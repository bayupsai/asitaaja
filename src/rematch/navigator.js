import createReactNavigationPlugin from '@rematch/react-navigation';
import Routes from '../Routes';
import { dispatch } from '@rematch/core';
import { isSignedIn } from '../services/Auth';

export const { Navigator, reactNavigationPlugin } = createReactNavigationPlugin(
  {
    Routes,
    initialScreen: 'Splash',
    // initialScreen: 'Login',
  }
);
