import React from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native';
// import { Grid, Row, Col } from 'react-native-easy-grid'
import MultiSwitch from 'rn-slider-switch';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const FormSearchFlight = props => (
  <View style={styles.container}>
    <Text>Book your flight</Text>
    <View>
      <MultiSwitch
        currentStatus={'Open'}
        disableScroll={value => {
          // this.scrollView.setNativeProps({
          //     scrollEnabled: value
          // });
        }}
        // isParentScrollEnabled={false}
        onStatusChanged={text => {
          // alert('XXXX')
        }}
        // disableSwitch={true}
      />
    </View>
  </View>
);

export default FormSearchFlight;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
  },
  pageTitle: {
    color: '#FFFFFF',
    fontSize: 24,
    fontWeight: '400',
  },
});
