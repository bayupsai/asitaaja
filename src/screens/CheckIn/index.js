import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon, Button } from 'react-native-elements';
import Dash from 'react-native-dash';

//Component
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { fontExtraBold, fontReguler, fontBold } from '../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class CheckIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
    };
  }

  handleBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage title="Check In" callback={this.handleBack} />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ paddingBottom: 10 }}
        >
          <SubHeaderPage title="Check in for your flight now!" />
          <Grid style={{ flex: 0 }}>
            <Row style={styles.content}>
              <Row style={styles.cardBox}>
                <Text style={styles.textBody}>
                  Booking ID <Text style={styles.textBold}>434219200</Text>
                </Text>
              </Row>
            </Row>
          </Grid>

          <Grid
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 22,
              paddingRight: 22,
              paddingBottom: 20,
            }}
          >
            <Col size={4.5} style={styles.cardBoxRequest}>
              <Row style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.textRequest}>Request Refund</Text>
              </Row>
            </Col>
            <Col style={1}></Col>
            <Col size={4.5} style={styles.cardBoxRequest}>
              <Row style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.textRequest}>Request Reschedule</Text>
              </Row>
            </Col>
          </Grid>

          <Grid
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 25,
              paddingRight: 25,
              paddingBottom: 20,
            }}
          >
            <Col style={styles.cardBoxBooking}>
              <Row style={{ paddingTop: 5, paddingBottom: 6 }}>
                <Text style={styles.textTitle}>Booking Details</Text>
              </Row>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: '#BDBDBD',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    borderBottomWidth: 2,
                    borderColor: '#009688',
                    width: '25%',
                    marginLeft: 15,
                  }}
                />
              </View>
              <Row>
                <Text style={styles.textBold}>Booked by</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Amir Revon</Text>
              </Row>
              <Row>
                <Text style={styles.textBold}>Booking Date Departure</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Mon, 21 January 2019</Text>
              </Row>
              <Row>
                <Text style={styles.textBold}>Contact's Email Address</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Revonchaniago@gmail.com</Text>
              </Row>
            </Col>
          </Grid>

          <Grid
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 25,
              paddingRight: 25,
              paddingBottom: 20,
            }}
          >
            <Col style={styles.cardBoxPassenger}>
              <Row style={{ paddingTop: 5, paddingBottom: 6 }}>
                <Text style={styles.textTitle}>Passenger(s)</Text>
              </Row>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: '#BDBDBD',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    borderBottomWidth: 2,
                    borderColor: '#009688',
                    width: '25%',
                    marginLeft: 15,
                  }}
                />
              </View>
              <Row>
                <Text style={styles.textBold}>Mr. Amir Revon</Text>
              </Row>
              <Row style={{ paddingLeft: 6 }}>
                <Col size={1}>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#424242',
                      paddingBottom: 5,
                      paddingLeft: 10,
                    }}
                  >
                    CGK
                  </Text>
                </Col>
                <Col size={1}>
                  <Icon
                    name="arrowright"
                    color="black"
                    type="antdesign"
                    size={15}
                  />
                </Col>
                <Col size={2}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '800',
                      color: '#424242',
                    }}
                  >
                    DPS
                  </Text>
                </Col>
                <Col size={3}></Col>
                <Col size={2}></Col>
              </Row>
              <Row>
                <Col size={2}>
                  <Icon name="shopping-bag" color="black" type="foundation" />
                </Col>
                <Col size={8}>
                  <Text>Baggage 20 kg</Text>
                </Col>
              </Row>
              <Row style={{ paddingLeft: 10, paddingRight: 10 }}>
                <Dash style={{ width: 345, height: 1, paddingTop: 20 }} />
              </Row>
              <Row>
                <Col size={9}>
                  <Text style={styles.textBody}>See all passengers</Text>
                </Col>
                <Col size={1}>
                  <Row>
                    <Icon name="chevron-right" color="#424242" type="feather" />
                  </Row>
                </Col>
              </Row>
            </Col>
          </Grid>

          <Grid
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 25,
              paddingRight: 25,
              paddingBottom: 20,
            }}
          >
            <Col style={styles.cardBoxFlight}>
              <Row style={{ paddingTop: 5, paddingBottom: 6 }}>
                <Text style={styles.textTitle}>Flight Details</Text>
              </Row>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: '#BDBDBD',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    borderBottomWidth: 2,
                    borderColor: '#009688',
                    width: '25%',
                    marginLeft: 15,
                  }}
                />
              </View>
              <Row style={{ paddingTop: 7 }}>
                <Text style={styles.textBody}>
                  PNR Code <Text style={styles.textBold}>VMSNGP</Text>
                </Text>
              </Row>
              <Row>
                <Col size={2} style={{ paddingLeft: 18, paddingBottom: 18 }}>
                  <Image
                    source={require('../../assets/chat/assets/ic_garuda.png')}
                    style={styles.image}
                  />
                </Col>
                <Col size={8}>
                  <Row>
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: '800',
                        color: '#424242',
                      }}
                    >
                      Garuda (GA-652)
                    </Text>
                  </Row>
                  <Row>
                    <Text style={{ fontSize: 14, color: '#424242' }}>
                      Eco Flexible (Subclass Y)
                    </Text>
                  </Row>
                </Col>
              </Row>
              <Row style={{ paddingTop: 10 }}>
                <Text style={styles.textBold}>Departure</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Jakarta (CGK)</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Monday, 21 January 2019</Text>
              </Row>
              <Row style={{ paddingBottom: 8 }}>
                <Text style={styles.textBody}>20:20</Text>
              </Row>
              <Row>
                <Text style={styles.textBold}>Departure</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Denpasar - Bali (DPS)</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>Monday, 21 January 2019</Text>
              </Row>
              <Row style={{ paddingBottom: 10 }}>
                <Text style={styles.textBody}>23:20</Text>
              </Row>
            </Col>
          </Grid>

          <Grid
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: 25,
              paddingRight: 25,
              paddingBottom: 20,
            }}
          >
            <Col style={styles.cardBoxFrequent}>
              <Row style={{ paddingTop: 8, paddingBottom: 6 }}>
                <Text style={styles.textTitle}>Frequent Flyer</Text>
              </Row>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: '#BDBDBD',
                  justifyContent: 'center',
                }}
              >
                <View
                  style={{
                    borderBottomWidth: 2,
                    borderColor: '#009688',
                    width: '25%',
                    marginLeft: 15,
                  }}
                />
              </View>
              <Row style={{ paddingTop: 5 }}>
                <Text style={styles.textBold}>No. Number</Text>
              </Row>
              <Row>
                <Text style={styles.textBold}>7233099112 - Platinum</Text>
              </Row>
            </Col>
          </Grid>
          <View style={{ padding: 10 }}>
            <Button
              title="Avaiable for Checkin"
              titleStyle={{ color: '#FFFFFF' }}
              buttonStyle={styles.button}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default CheckIn;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  content: {
    paddingBottom: -10,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    transform: [{ translateY: -45 }],
  },
  cardBox: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 70,
    borderRadius: 7,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardBoxRequest: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 45,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardBoxBooking: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: deviceHeight / 3.13,
    borderRadius: 7,
  },
  cardBoxPassenger: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: deviceHeight / 4,
    borderRadius: 7,
  },
  cardBoxFlight: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: deviceHeight / 1.8,
    borderRadius: 7,
  },
  cardBoxFrequent: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: deviceHeight / 5.2,
    borderRadius: 7,
  },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    backgroundColor: '#BDBDBD',
    alignSelf: 'center',
  },
  image: {
    width: 60,
    height: 50,
    resizeMode: 'center',
  },
  button: {
    height: 45,
    borderRadius: 4,
    backgroundColor: '#57ae5a',
    marginLeft: 10,
    width: 370,
    paddingRight: 5,
  },
  textBold: {
    fontSize: 14,
    fontFamily: fontExtraBold,
    color: '#424242',
    paddingTop: 5,
    paddingLeft: 15,
  },
  textBody: {
    fontSize: 14,
    color: '#424242',
    paddingBottom: 5,
    paddingLeft: 15,
    fontFamily: fontReguler,
  },
  textRequest: { fontSize: 14, color: '#57ae5a', fontFamily: fontBold },
  textTitle: {
    fontSize: 16,
    color: '#18a6b9',
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
  },
});
