import React from 'react';
import { View } from 'react-native';
import Tabs from './Component/Tab/DisplayTabs';

class Order extends React.Component {
  //Main Render
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Tabs {...this.props} />
      </View>
    );
  }
}

export default Order;
