import React from 'react';
import { View, ScrollView, Text, StyleSheet } from 'react-native';
import numeral from 'numeral';
import HeaderPage from '../../../components/HeaderPage';
import SubHeader from '../../../components/SubHeaderPage';
import {
  thameColors,
  fontReguler,
  fontBold,
  asitaColor,
} from '../../../base/constant';
import {
  ButtonRounded,
  ButtonRoundedNonActive,
} from '../../../elements/Button';

const PLNPost = () => {
  return (
    <ScrollView style={styles.container}>
      <HeaderPage title="Waiting for Payment" />
      <SubHeader />
      <View style={styles.card}>
        <View style={styles.item}>
          <Text style={styles.textRegular}>
            Please Complete this payment in
          </Text>
          <Text style={styles.textTeal}>14:09 AM</Text>
        </View>
        <View style={styles.hr} />
        <View style={styles.item}>
          <Text style={styles.textGray}>No. Meter</Text>
          <Text style={styles.textBold}>214000168311</Text>
        </View>
        <View style={styles.item}>
          <Text style={styles.textGray}>Tarif/Daya</Text>
          <Text style={styles.textBold}>R1/900</Text>
        </View>
        <View style={styles.item}>
          <Text style={styles.textGray}>Nominal Tagihan</Text>
          <Text style={styles.textBold}>
            Rp{' '}
            {numeral(62000)
              .format('0,0')
              .replace(/,/g, '.')}
          </Text>
        </View>
        <View style={styles.item}>
          <Text style={styles.textGray}>Price You Pay</Text>
          <Text style={styles.textTeal}>
            Rp{' '}
            {numeral(64500)
              .format('0,0')
              .replace(/,/g, '.')}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionButton, { marginVertical: 20 }]}>
        <ButtonRounded label="PAYMENT INSTRUCTIONS" />
        <View style={{ marginTop: 5 }}>
          <ButtonRoundedNonActive label="CANCEL TRANSACTION" />
        </View>
      </View>
    </ScrollView>
  );
};

export default PLNPost;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  card: {
    backgroundColor: thameColors.white,
    borderRadius: 10,
    marginHorizontal: 20,
    padding: 20,
    alignItems: 'center',
    marginTop: -25,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  textGray: {
    fontFamily: fontReguler,
    color: thameColors.gray,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textTeal: {
    fontFamily: fontBold,
    color: asitaColor.tealBlue,
    fontSize: 16,
  },
  hr: {
    width: '100%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    marginVertical: 20,
  },
  item: {
    alignItems: 'center',
    marginVertical: 10,
  },
  sectionButton: {
    marginHorizontal: 20,
  },
});
