import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import HeaderPage from '../../components/HeaderPage';
import { ButtonPlus } from '../../elements/ButtonPlus';
import { ButtonRounded } from '../../elements/Button';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  fontRegulerItalic,
  thameColors,
} from '../../base/constant/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import BarcodeModal from './Modal/BarcodeModal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class OrderDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalBarcode: false,
    };
  }
  goBack = () => {
    this.props.navigation.goBack();
  };
  packageAlreadyDropped() {
    this.setState({ alreadyDropped: true });
  }

  render() {
    const { alreadyDropped } = this.state;
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Published Package"
          callback={this.goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          <Grid style={styles.section}>
            <Col style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <View>
                <Text style={styles.textInfo}>Booking ID</Text>
              </View>
              <View>
                <Text style={styles.textTitle}>201912345</Text>
              </View>
            </Col>
            <Col style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
              <TouchableOpacity
                onPress={() => this.setState({ modalBarcode: true })}
              >
                <Text style={styles.textBarcode}>BARCODE</Text>
              </TouchableOpacity>
            </Col>
          </Grid>

          <Grid
            style={{
              padding: 10,
              paddingLeft: 0,
              paddingRight: 0,
              marginTop: 10,
            }}
          >
            <Col style={styles.center}>
              <View style={[styles.center, { marginBottom: 15 }]}>
                <Text style={styles.textLabel}>
                  Please confirm using this button below, After
                </Text>
                <Text style={styles.textLabel}>
                  finishing dropping your package into Dropbox
                </Text>
              </View>
              <View style={styles.sectionButton}>
                {alreadyDropped === true ? (
                  <ButtonRounded
                    label="TRACKING MY ORDER"
                    onClick={() =>
                      this.props.navigation.navigate('CargoTracking')
                    }
                  />
                ) : (
                  <ButtonRounded
                    label="PACKAGE ALREADY DROPPED"
                    onClick={() => this.packageAlreadyDropped()}
                  />
                )}
              </View>
            </Col>
          </Grid>

          <Grid style={styles.section}>
            <Col>
              <Row>
                <Col size={2} style={{ borderRadius: 15 }}>
                  <Image
                    source={require('../../assets/cargo/icon_pickup_selected_new.png')}
                    resizeMode="center"
                    style={{ width: 45, height: 45 }}
                  />
                </Col>
                <Col size={8}>
                  <View>
                    <Text style={styles.textInfo}>PICKUP ADDRESS</Text>
                  </View>
                  <View style={{ paddingTop: 5 }}>
                    <Text style={styles.textlabelBold}>
                      SPBU Pertamina Kebun Jeruk
                    </Text>
                  </View>
                </Col>
              </Row>
              <Row style={{ marginBottom: 10, marginTop: 10 }}>
                <Col
                  size={2}
                  style={{ justifyContent: 'center', alignItems: 'flex-start' }}
                >
                  <Image
                    source={require('../../assets/icons/icon_dot_vertical.png')}
                    resizeMode="center"
                    style={{ width: 40, height: 35, marginLeft: 2 }}
                  />
                </Col>
                <Col size={8}></Col>
              </Row>
              <Row>
                <Col size={2}>
                  <Image
                    source={require('../../assets/cargo/icon_destination_selected_new.png')}
                    resizeMode="center"
                    style={{ width: 45, height: 45 }}
                  />
                </Col>
                <Col size={8}>
                  <View>
                    <Text style={styles.textInfo}>DESTINATION ADDRESS</Text>
                  </View>
                  <View style={{ paddingTop: 5 }}>
                    <Text style={styles.textlabelBold}>
                      SPBU Pertamina Kebun Jeruk
                    </Text>
                  </View>
                </Col>
              </Row>
            </Col>
          </Grid>

          <Grid style={styles.section}>
            <Col style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Row style={{ paddingBottom: 15 }}>
                <Col>
                  <Text style={styles.textSender}>Sender Contact</Text>
                </Col>
              </Row>
              <Row style={{ paddingBottom: 5 }}>
                <Col>
                  <Text style={styles.textlabelBold}>Revon</Text>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.textLabel}>0912312312312</Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={styles.textInfo}>Estimated Pickup</Text>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.textlabelBold}>21 Jan, 4PM - 6PM</Text>
                </Col>
              </Row>
            </Col>
          </Grid>

          <Grid style={styles.section}>
            <Col style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Row style={{ paddingBottom: 15 }}>
                <Col>
                  <Text style={styles.textRecipient}>Recipient Contact</Text>
                </Col>
              </Row>
              <Row style={{ paddingBottom: 5 }}>
                <Col>
                  <Text style={styles.textlabelBold}>Eka Hasibuan</Text>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.textLabel}>0912312312312</Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={styles.textInfo}>Estimated Pickup</Text>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.textlabelBold}>22 Jan, 4PM - 6PM</Text>
                </Col>
              </Row>
            </Col>
          </Grid>

          {/* PACKAGE DETAIL */}
          <Grid style={styles.marginTop20}>
            <Row>
              <Text style={styles.textTitle}>Package Detail</Text>
            </Row>
            <Row style={[styles.section, { paddingTop: 10, paddingBottom: 0 }]}>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>Books & Documents</Text>
                  </Col>
                </Row>
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>Small Size</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textLabel}>20x11x7</Text>
                  </Col>
                </Row>
                <Row style={{ padding: 10, paddingLeft: 0 }}>
                  <Col>
                    <Text style={styles.textLabel}>This item is Fragile</Text>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>

          {/* TOTAL PAYMENT */}
          <Grid style={styles.marginTop20}>
            <Row>
              <Text style={styles.textTitle}>Total Payment</Text>
            </Row>
            <Row style={[styles.section, { paddingTop: 10, paddingBottom: 0 }]}>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>Shipping Cost</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textLabel}>Rp5.000</Text>
                  </Col>
                </Row>
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>Small Package</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textLabel}>Rp19.000</Text>
                  </Col>
                </Row>
                <Row style={{ padding: 10, paddingLeft: 0, paddingBottom: 30 }}>
                  <Col>
                    <Text style={styles.textlabelBold}>Total Price</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textPrice}>Rp. 24.000</Text>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
          {/* <Grid>
                        <Row >
                            <Col style={{ justifyContent: "center", alignItems: "center" }}>
                                <View style={styles.sectionButton}>
                                    <ButtonRounded label="PACKAGE DROPED" onClick={() => alert("xxx") } />
                                </View>
                            </Col>
                        </Row>
                    </Grid> */}

          {/* <Grid>
                        <Row >
                            <Col style={{ justifyContent: "center", alignItems: "center" }}>
                                <View style={styles.sectionButton}>
                                    <ButtonRounded label="ORDER TRACKING" onClick={() => this.props.navigation.navigate("CargoTracking") } />
                                </View>
                            </Col>
                        </Row>
                    </Grid> */}
        </ScrollView>

        {/* MODAL */}
        <BarcodeModal
          visible={this.state.modalBarcode}
          closeModal={() => this.setState({ modalBarcode: false })}
          onBackdropPress={() => this.setState({ modalBarcode: false })}
        />
      </View>
    );
  }
}

export default OrderDetails;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#FFFFFF' },
  content: { marginTop: 0, marginLeft: 20, marginRight: 20 },
  textTitle: { color: '#222222', fontFamily: fontExtraBold, fontSize: 18 },
  textBarcode: {
    color: thameColors.primary,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textSender: {
    color: thameColors.primary,
    fontFamily: fontBold,
    fontSize: 18,
  },
  textRecipient: { color: '#a2195b', fontFamily: fontBold, fontSize: 18 },
  section: {
    padding: 20,
    paddingLeft: 0,
    paddingRight: 0,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 0.5,
  },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 16 },
  textInfo: { color: '#828282', fontFamily: fontReguler, fontSize: 16 },
  textlabelBold: { color: '#222222', fontFamily: fontBold, fontSize: 16 },
  textWarning: {
    color: '#d30000',
    fontFamily: fontRegulerItalic,
    fontSize: 14,
    paddingBottom: 10,
  },
  marginTop20: { marginTop: 20 },
  textPrice: { color: thameColors.primary, fontFamily: fontBold, fontSize: 18 },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  center: { justifyContent: 'center', alignItems: 'center' },
});
