import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import HeaderPage from '../../components/HeaderPage';
import { ButtonPlus } from '../../elements/ButtonPlus';
import { ButtonRounded } from '../../elements/Button';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  fontRegulerItalic,
  thameColors,
} from '../../base/constant/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import BarcodeModal from './Modal/BarcodeModal';
import Dash from 'react-native-dash';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class CargoTracking extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalBarcode: false,
      trackingData: [
        {
          status: 'Waiting Pickup',
          time: '21 Jan 2019, 14:23',
          location: 'SPBU Pertamina MT. Haryono',
        },
        {
          status: 'Picked by Agent',
          time: '21 Jan 2019, 16:00',
          location: 'SPBU Pertamina MT. Haryono',
        },
        {
          status: 'Drop in Forwarder',
          time: '22 Jan 2019, 07:00',
          location: 'Bandars Soekarno Hatta, Cengkareng',
        },
        {
          status: 'In Delivery to Destination',
          time: '22 Jan 2019, 11:03',
          location: 'Bandara Soekarno Hatta, Cengkareng',
        },
        {
          status: 'Transit in Forwarder',
          time: '23 Jan 2019, 08:33',
          location: 'Bandara Ngurah Rai, Denpasar',
        },
        {
          status: 'Delivery by Agent to Destination',
          time: '23 Jan 2019, 12:23',
          location: 'SPBU Pertamina Kuta Bali',
        },
        {
          status: 'In Box',
          time: '23 Jan 2019, 12:33',
          location: 'SPBU Pertamina Kuta Bali',
        },
      ],
    };
  }
  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Order Tracking"
          callback={this.goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          {this.state.trackingData.map((data, index) => {
            return (
              <Grid key={index}>
                <Col size={1.5}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: 30,
                      height: 30,
                      backgroundColor: '#FFFFFF',
                      borderRadius: 15,
                      borderColor: thameColors.primary,
                      borderWidth: 1,
                    }}
                  >
                    <View
                      style={{
                        width: 15,
                        height: 15,
                        backgroundColor: thameColors.primary,
                        borderRadius: 7.5,
                      }}
                    ></View>
                  </View>
                  <Dash
                    dashColor="#e2e2e2"
                    style={{
                      width: 0.5,
                      height: 50,
                      flexDirection: 'column',
                      paddingLeft: 13,
                      paddingTop: 10,
                      paddingBottom: 5,
                    }}
                  />
                </Col>
                <Col
                  size={8.5}
                  style={{
                    paddingTop: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <View>
                    <Text style={styles.textTitle}>{data.status}</Text>
                  </View>
                  <View>
                    <Text style={styles.textLabel}>{data.time}</Text>
                  </View>
                  <View>
                    <Text style={styles.textLabel}>{data.location}</Text>
                  </View>
                </Col>
              </Grid>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

export default CargoTracking;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#FFFFFF' },
  content: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    paddingBottom: 50,
  },
  textTitle: { color: '#222222', fontFamily: fontExtraBold, fontSize: 18 },
  textBarcode: { color: '#a2195b', fontFamily: fontBold, fontSize: 16 },
  textSender: {
    color: thameColors.primary,
    fontFamily: fontBold,
    fontSize: 18,
  },
  textRecipient: { color: '#a2195b', fontFamily: fontBold, fontSize: 18 },
  section: {
    padding: 20,
    paddingLeft: 0,
    paddingRight: 0,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 0.5,
  },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 16 },
  textInfo: { color: '#828282', fontFamily: fontReguler, fontSize: 16 },
  textlabelBold: { color: '#222222', fontFamily: fontBold, fontSize: 16 },
  textWarning: {
    color: '#d30000',
    fontFamily: fontRegulerItalic,
    fontSize: 14,
    paddingBottom: 10,
  },
  marginTop20: { marginTop: 20 },
  textPrice: { color: '#a2195b', fontFamily: fontExtraBold, fontSize: 18 },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
});
