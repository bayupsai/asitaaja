import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  InteractionManager,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import {
  failedState,
  actionFlightSelected,
  actionSetPassenger,
} from '../../redux/actions/FlightAction';
import HeaderPage from '../../components/HeaderPage';
import HeaderPageResults from '../../components/HeaderPageResults';
import SubHeaderPage from '../../components/SubHeaderPage';
import { ButtonRounded, ButtonRoundedNonActive } from '../../elements/Button';

import DetailFlight from './ComponentFlight/DetailFlight';
// import ReturnFlight from './ComponentFlight/ReturnFlight'
import PassengerFlight from './ComponentFlight/PassengerFlight';
import AlertModal from '../../components/AlertModal';
import { fontBold, fontReguler, thameColors } from '../../base/constant';

// let plane = {
//     data: {
//         departure_code: "Jakarta",
//         arrival_code: "Padang",
//     },
// }

class OrderDetailFlight extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      logoImage: [
        { id: 1, img: require('../../assets/icons/logo-airlines.png') },
        { id: 2, img: require('../../assets/logos/train.png') },
      ],
    };
  }

  _goBack = () => this.props.navigation.goBack();
  alert = message => alert(message);
  _alertOk = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ visibleModal: null }, () =>
        alert('The order was cancel')
      );
    });
  };

  //Modal
  _toggleModal = modal => this.setState({ visibleModal: modal });
  _setNullModal = () => this.setState({ visibleModal: null });
  //Modal

  //handle image logo
  _imgCore = () => {
    return require('../../assets/icons/logo-airlines.png');
    // return require('../../assets/logos/train.png')
  };
  _imgSingle = () => {
    return require('../../assets/icons/flight_path.png');
    // return require('../../assets/icons/icon_train_right.png')
  };
  _imgReturn = () => {
    return require('../../assets/icons/icon_flight2.png');
    // return require('../../assets/icons/icon_train_left.png')
  };
  //handle image logo

  //Header Conditions
  headerType = () => {
    let { selectOrder } = this.props;
    let statusName = '';
    let plane = {
      data: {
        departure_code:
          selectOrder.flight_data[0].flight_info.detail[0].departure_city_name,
        arrival_code:
          selectOrder.flight_data[0].flight_info.detail[0].arrival_city_name,
      },
    };
    if (selectOrder.status == 'BOOKED') {
      statusName = 'Booking Details';
      return <HeaderPage title={statusName} callback={this._goBack} />;
    } else if (selectOrder.status == 'WAITING_PAYMENT') {
      statusName = 'Waiting Payment';
      return <HeaderPage title={statusName} callback={this._goBack} />;
    } else if (selectOrder.status == 'PAID') {
      return (
        <View>
          <HeaderPageResults
            bookingId={selectOrder.partner_trx_id}
            page="orderFlight"
            callback={this._goBack}
            flight={plane}
          />
          <View
            style={{
              backgroundColor: thameColors.white,
              alignItems: 'center',
              justifyContent: 'center',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              marginTop: -20,
              padding: 20,
              paddingBottom: 10,
            }}
          />
        </View>
      );
    }
  };

  subHeaderText = () => {
    let { selectOrder } = this.props;
    let destination =
      selectOrder.flight_data[0].flight_info.detail[0].departure_city_name;
    let arrival =
      selectOrder.flight_data[0].flight_info.detail[0].arrival_city_name;
    let destinationCode =
      selectOrder.flight_data[0].flight_info.detail[0].departure_city;
    let arrivalCode =
      selectOrder.flight_data[0].flight_info.detail[0].arrival_city;
    return `${destination} (${destinationCode}) - ${arrival} (${arrivalCode})`;
    // return "Complete payment within 17:28 PM"
  };

  finishBook = () => {
    return (
      <View
        style={{
          backgroundColor: thameColors.white,
          alignItems: 'center',
          justifyContent: 'center',
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          marginTop: -20,
          padding: 20,
          paddingBottom: 10,
        }}
      >
        <Text
          style={{
            fontFamily: fontBold,
            color: thameColors.green,
            fontSize: 22,
            marginBottom: 15,
          }}
        >
          Your E-Tiket has been issued!
        </Text>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 25,
          }}
        >
          <Text
            style={{
              fontFamily: fontReguler,
              color: thameColors.superBack,
              textAlign: 'center',
            }}
          >
            Check your email inbox/spam folder revonchaniago@gmail.com to obtain
            E-tiket.
          </Text>
        </View>
        <View style={[styles.sectionHr, { marginBottom: 10 }]} />
        <Text style={{ fontFamily: fontReguler, marginBottom: 10 }}>
          Or download your E-Tiket here
        </Text>
        <ButtonRounded
          label="DOWNLOAD E-TICKET"
          onClick={() => alert('E-Tiket has been downloaded')}
        />
        <View style={[styles.sectionHr, { marginTop: 10 }]} />
      </View>
    );
  };
  //Header Conditions

  cancelPayment = () => {
    this.props.dispatch(actionFlightSelected({}, null));
    this.props.dispatch(actionSetPassenger([]));
    this.props.dispatch(failedState('bookingFlight', {}));
    //then
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Tab' })],
      })
    );
  };

  //Button for Status Payment
  _statusPayment = () => {
    let { status, partner_trx_id } = this.props.selectOrder;
    if (status == 'WAITING_PAYMENT') {
      return (
        <View style={styles.buttonPossition}>
          <ButtonRounded
            label="PAYMENT INSTRUCTIONS"
            onClick={() =>
              this._navigating(status, 'PaymentTransfer', partner_trx_id)
            }
          />
          <View style={{ marginTop: 5 }}>
            <ButtonRoundedNonActive
              label="CANCEL TRANSACTION"
              onClick={() => this.setState({ visibleModal: 1 })}
            />
          </View>
        </View>
      );
    } else if (status == 'BOOKED') {
      return (
        <View style={styles.buttonPossition}>
          <ButtonRounded
            label="SELECT PAYMENT METHOD"
            onClick={() =>
              this.props.navigation.navigate('PaymentOrder', {
                pages: 'orderFlight',
              })
            }
          />
        </View>
      );
    }
  };

  //Navigation
  _navigating = (type, route, trx_id) => {
    let { navigation } = this.props;
    // if (type == 'WAITING_PAYMENT') {
    //     navigation.navigate(route, { invoiceNumber: trx_id })
    // } else {
    //     navigation.navigate(route, { pages: "orderFlight" })
    // }
    InteractionManager.runAfterInteractions(() => {
      navigation.navigate(
        route,
        type === 'WAITING_PAYMENT'
          ? { invoiceNumber: trx_id }
          : { pages: 'orderFlight' }
      );
    });
  };

  //Main Render
  render() {
    let { status } = this.props.selectOrder;
    return (
      <View style={styles.container}>
        {/* <HeaderPage title={typeOrder.headerTitle} callback={this._goBack} /> */}
        {this.headerType()}

        {/* {
                    typeOrder.type == "finish" ?
                        <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, paddingLeft: 20, paddingRight: 20, backgroundColor: "#FFFFFF" }} />
                        : <View style={{ width: 0, height: 0 }} />
                } */}
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* //Sub Header */}
          {status == 'PAID' ? (
            this.finishBook()
          ) : (
            <SubHeaderPage label={this.subHeaderText()} />
          )}
          {/* //Sub Header */}

          {/* <DetailFlight {...this.props} {...details[0]} /> */}
          <DetailFlight
            {...this.props}
            imgCore={this._imgCore()}
            condition={'Flight'}
          />
          {/* {
                        flight_data.length > 1 ? <ReturnFlight {...this.props} imgCore={this._imgCore()} condition={'Flight'} /> : null
                    } */}
          {/* <PassengerFlight {...this.props} {...this.props.booking.data} /> */}
          <PassengerFlight
            {...this.props}
            imgSingle={this._imgSingle()}
            imgReturn={this._imgReturn()}
          />

          {/* {
                        Object.keys(this.props.booking).length !== 0 && this.props.booking.constructor == Object ?
                            <View style={styles.buttonPossition}>
                                <ButtonRounded label="PAYMENT INSTRUCTIONS" onClick={() => this.props.navigation.navigate('PaymentOrder', { pages: "flight" })} />
                                <View style={{ marginTop: 5 }}><ButtonRoundedNonActive label="CANCEL TRANSACTION" onClick={() => this.setState({ visibleModal: 1 })} /></View>
                            </View>
                            : <View style={{ width: 0, height: 0 }} />
                    } */}
          {this._statusPayment()}
        </ScrollView>

        {/* ================== Alert ================== */}

        {/* Modal Alert - Cancel Transaction */}
        <AlertModal
          type="qna"
          isVisible={this.state.visibleModal === 1}
          titleColor={thameColors.superBack}
          title="Cancel Purcase"
          contentText="After being canceled, this order cannot be returned. Are you sure want to continue?"
          labelOk="YES, CANCEL ORDER"
          labelCancel="NO, EXIT"
          onPress={this._alertOk}
          onDismiss={this._setNullModal}
        />
        {/* Modal Alert - Cancel Transaction */}

        {/* ================== Alert ================== */}
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    booking: state.flight.dataBookingFlight,
    passenger: state.flight.passengerData,
    flight: state.flight,
    selectOrder: state.ordersHistory.selectOrder,
  };
}

export default connect(stateToProps)(OrderDetailFlight);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonPossition: {
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
});
