import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontBold, fontReguler } from '../../../../../base/constant';

//Components
import OrderFlight from './OrderFlight';
import OrderTrain from './OrderTrain';
import OrderCargo from './OrderCargo';
import OrderPulsa from './OrderPulsa';
import OrderDataInternet from './OrderDataInternet';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class OrderFinished extends React.PureComponent {
  render() {
    return (
      <ScrollView style={{ marginBottom: 20 }}>
        <OrderFlight {...this.props} />
        <OrderTrain {...this.props} />
        <OrderCargo />
        <OrderPulsa />
        <OrderDataInternet />
      </ScrollView>
    );
  }
}

export default OrderFinished;
