import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';

//Components
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class OrderPulsa extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arrs: [1],
    };
  }
  render() {
    return (
      <ScrollView>
        {this.state.arrs.map((data, index) => {
          return (
            <Grid
              key={index}
              style={styles.cardBox}
              onPress={() => alert('Detail Order')}
            >
              <Col size={2} style={{ alignItems: 'center' }}>
                <Image
                  source={require('../../../../../assets/icons/icon_pulsa.png')}
                  style={styles.image}
                />
              </Col>
              <Col size={8} style={{ padding: 10 }}>
                <Grid>
                  <Col
                    style={{
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                    size={4}
                  >
                    <Text style={styles.textTitle}>+62 812 7072 3917</Text>
                  </Col>
                </Grid>
                <Grid style={{ paddingTop: 5 }}>
                  <Col>
                    <Text style={styles.textBody}>Pulsa Telkomsel</Text>
                  </Col>
                </Grid>
                <Grid style={{ paddingTop: 5 }}>
                  <Col>
                    <Text style={styles.textPrice}>Rp. 100.000</Text>
                  </Col>
                </Grid>
                <Grid style={{ paddingTop: 10 }}>
                  <Col size={8} style={styles.col}>
                    <Text style={styles.purchase}>Purchase Successful</Text>
                  </Col>
                  <Col
                    size={2}
                    onPress={() => alert('Continue Payment')}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Icon
                      name="navigate-next"
                      type="materialicon"
                      color="#0066b3"
                      size={30}
                    />
                  </Col>
                </Grid>
              </Col>
            </Grid>
          );
        })}
      </ScrollView>
    );
  }
}
export default OrderPulsa;

const styles = StyleSheet.create({
  image: {
    marginTop: 15,
    width: 50,
    height: 45,
    resizeMode: 'center',
    tintColor: thameColors.primary,
  },
  cardBox: {
    borderRadius: 5,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#ffffff',
  },
  col: {
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  purchase: {
    borderRadius: 20,
    backgroundColor: '#efefef',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
    color: '#008d00',
    fontFamily: fontBold,
    fontSize: 16,
  },
  textCode: { fontSize: 16, color: '#222222', fontFamily: fontReguler },
  textBody: { fontSize: 16, color: '#222222', fontFamily: fontReguler },
  textTitle: { fontSize: 16, color: '#222222', fontFamily: fontBold },
  textPrice: { fontSize: 16, color: '#222222', fontFamily: fontBold },
  icon: {
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
});
