import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import {
  fontBold,
  fontReguler,
  asitaColor,
  fontExtraBold,
  thameColors,
} from '../../../../../base/constant';
// import { ButtonRounded } from '../../../../../elements/Button';

const window = Dimensions.get('window');

const OrderEmpty = props => (
  <View style={styles.cardBox}>
    <Grid style={{ marginTop: 30, flex: 0 }}>
      <Col style={{ alignItems: 'center' }}>
        <Image
          style={styles.image}
          resizeMode={'contain'}
          source={require('../../../../../assets/img/order_empty.png')}
        />
      </Col>
    </Grid>
    <Grid style={{ paddingTop: 30 }}>
      <Col style={{ alignItems: 'center' }}>
        <View>
          <Text
            style={{
              fontFamily: fontBold,
              color: asitaColor.black,
              fontSize: 22,
            }}
          >
            No Purchase Found
          </Text>
        </View>
        <View style={{ paddingTop: 5 }}>
          <Text style={{ fontSize: 16, fontFamily: fontReguler }}>
            You haven’t made any purchases
          </Text>
        </View>
        <View style={{ paddingTop: 20 }}>
          <TouchableOpacity
            onPress={props.goHome}
            style={{
              backgroundColor: asitaColor.orange,
              borderRadius: 20,
              padding: 10,
              paddingLeft: 30,
              paddingRight: 30,
            }}
          >
            <Text
              style={{
                color: thameColors.white,
                fontSize: 16,
                fontFamily: fontExtraBold,
              }}
            >
              ORDER NOW
            </Text>
          </TouchableOpacity>
          {/* <ButtonRounded label="ORDER NOW" /> */}
        </View>
      </Col>
    </Grid>
  </View>
);
export default OrderEmpty;

const styles = StyleSheet.create({
  image: {
    width: window.height / 2,
    height: window.height / 3,
    resizeMode: 'cover',
  },
  cardBox: {
    width: window.width,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
});
