import React from 'react';
import { View, ScrollView } from 'react-native';
import FlightOrderList from '../../Orders/Flight';
import CargoOrderList from '../../Orders/Cargo';
import { connect } from 'react-redux';
import _ from 'lodash';
import HotelOrderList from '../../Orders/Hotel';

class BookingList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arrs: [
        {
          id: 1,
          condition: 'flight',
          type: 'booking',
          message: '',
          headerTitle: 'Booking Details',
        },
        {
          id: 2,
          condition: 'flight',
          type: 'payment',
          message: 'Waiting Payment',
          headerTitle: 'Waiting Payment',
        },
      ],
    };
  }

  checkEmptyObj = obj => {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  renderFlight = () => {
    let { flightSelected, passengerData, dataBookingFlight } = this.props;
    if (
      Object.keys(dataBookingFlight).length !== 0 &&
      dataBookingFlight.constructor == Object
    ) {
      return (
        <FlightOrderList
          onPress={() => this._navigate('OrderDetailFlight')}
          {...this.props}
        />
      );
    } else if (passengerData.length !== 0) {
      return (
        <FlightOrderList
          onPress={() => this._navigate('FormOrder')}
          {...this.props}
        />
      );
    } else if (
      Object.keys(flightSelected).length !== 0 &&
      flightSelected.constructor == Object
    ) {
      return (
        <FlightOrderList
          onPress={() => this._navigate('FormOrder')}
          {...this.props}
        />
      );
    } else {
      return <View style={{ width: 0, height: 0 }} />;
    }
  };

  //navigating
  _navigate = (params, passing) => {
    this.props.navigation.navigate(params, passing);
  };

  render() {
    return (
      <ScrollView style={{ paddingBottom: 20 }}>
        {this.state.arrs.map((data, index) => {
          return <CargoOrderList key={index} {...this.props} />;
        })}

        {/* {this.renderFlight()} */}

        {this.state.arrs.map((data, index) => {
          return (
            <FlightOrderList
              key={index}
              typeOrder={data.message}
              onPress={() =>
                this._navigate('OrderDetailFlight', { typeOrder: data })
              }
              {...this.props}
            />
          );
        })}

        {this.state.arrs.map((data, index) => (
          <HotelOrderList key={index} index={index} {...this.props} />
        ))}
      </ScrollView>
    );
  }
}

function stateToProps(state) {
  return {
    flightSelected: state.flight.flightSelected,
    passengerData: state.flight.passengerData,
    dataBookingFlight: state.flight.dataBookingFlight,
  };
}
export default connect(stateToProps)(BookingList);
