import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { fontExtraBold } from '../../../../base/constant';

const Back = props => {
  return (
    <TouchableOpacity>
      <Icon name="dots-three-vertical" color="#FFFFFF" type="entypo" />
    </TouchableOpacity>
  );
};

const Title = props => {
  return (
    <Text style={{ color: '#FFFFFF', fontSize: 18, fontFamily: fontExtraBold }}>
      {props.title}
    </Text>
  );
};

const Heading = props => (
  <Header
    leftComponent={<Text></Text>}
    rightComponent={<Back goBack={props.backButton} />}
    centerComponent={<Title title={props.title} />}
    containerStyle={{
      backgroundColor: '#00275d',
      borderBottomColor: 'transparent',
    }}
  ></Header>
);

export default Heading;
