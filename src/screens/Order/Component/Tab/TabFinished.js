import React from 'react';
import { View, ScrollView, InteractionManager } from 'react-native';

// import OrderFinished from './Finished/OrderFinished'
import OrderEmpty from './Finished/OrderEmpty';
import { thameColors } from '../../../../base/constant';

class TabActive extends React.Component {
  _goHome = () => {
    const { navigate } = this.props.navigation;
    InteractionManager.runAfterInteractions(() => {
      navigate('Home');
    });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <OrderEmpty/> */}
        <ScrollView style={{ backgroundColor: thameColors.white }}>
          <OrderEmpty goHome={this._goHome} />
          {/* <OrderFinished {...this.props} /> */}
        </ScrollView>
      </View>
    );
  }
}

export default TabActive;
