import * as React from 'react';
import { View, StyleSheet, StatusBar, Platform } from 'react-native';
import { asitaColor } from '../../../../base/constant';

// import Heading from './Header'
import Header from '../../../../components/HeaderLogin';
import Tabs from './Tabs';
import TabActive from './TabActive';
import TabFinished from './TabFinished';

const isAndroid = Platform.OS === 'android';

class TabsOffers extends React.PureComponent {
  constructor() {
    super();
  }
  componentDidMount() {
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      isAndroid && StatusBar.setBackgroundColor(asitaColor.marineBlue);
    });
  }
  componentWillUnmount() {
    this._navListener.remove();
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          {...this.props}
          title="My Orders"
          backButton={this.props.modalin}
        />

        <Tabs>
          <TabActive {...this.props} title="Active" />
          <TabFinished {...this.props} title="Finished" />
        </Tabs>
      </View>
    );
  }
}

export default TabsOffers;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
});
