import React from 'react';
import { View, InteractionManager } from 'react-native';
import _ from 'lodash';

// import BookingList from './Active/BookingList'
import BookingEmpty from './Active/BookingEmpty';
import { thameColors, asitaColor } from '../../../../base/constant';

import { actionOrdersHistory } from '../../../../redux/actions/OrdersHistoryAction';
import { connect } from 'react-redux';
import FlightOrderList from '../Orders/Flight';
import { makeDataProfile } from '../../../../redux/selectors/ProfileSelector';
import { makeOrderHistory } from '../../../../redux/selectors/OrderHistorySelector';

class TabActive extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  componentDidMount() {
    this._hitOrdersHistory();
  }

  //Hit Orders History
  _hitOrdersHistory = () => {
    let { dispatch, getUser, token } = this.props;
    if (getUser) {
      this.setState({ loading: true });
      dispatch(actionOrdersHistory(token))
        .then(res => {
          if (res.type == 'ORDERS_HISTORY_SUCCESS') {
            this.setState({ loading: false });
          } else {
            this.setState({ loading: false });
          }
        })
        .catch(err => {
          this.setState({ loading: false });
        });
    } else {
      this.setState({ loading: false });
    }
  };

  //Refresh Function
  _onRefresh = () => {
    this._hitOrdersHistory();
  };

  _backHome = () => {
    const { navigate } = this.props.navigation;
    InteractionManager.runAfterInteractions(() => navigate('Home'));
  };

  //Main Render
  render() {
    let { orderData, token, isLogin } = this.props;
    let { loading } = this.state;
    let bgColor = thameColors.white;
    if (isLogin) {
      if (orderData !== null) {
        bgColor = thameColors.backWhite;
      } else {
        bgColor = thameColors.white;
      }
    } else {
      bgColor = thameColors.white;
    }
    const dataReal = _.reject(orderData, ['flight_data', null]);
    return (
      <View style={{ flex: 1, backgroundColor: bgColor }}>
        {/* <ScrollView style={{  paddingBottom: 10 }}> */}
        {/* <BookingEmpty /> */}
        <FlightOrderList
          data={orderData !== null && token !== '' ? dataReal : new Array(0)}
          loading={loading}
          refreshing={loading}
          onRefresh={this._onRefresh}
          listEmptyComponent={<BookingEmpty goHome={this._backHome} />}
          {...this.props}
        />
        {/* </ScrollView> */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    orderData: makeOrderHistory(state),
    getUser: makeDataProfile(state),
    token: state.profile.token,
    isLogin: state.profile.isLogin,
    loading: state.ordersHistory.fetching,
    // flightData: state.ordersHistory.data[0].flight_data
  };
}

export default connect(mapStateToProps)(TabActive);
