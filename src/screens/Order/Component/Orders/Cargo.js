import React from 'react';
import { Text, StyleSheet, Image } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../../../base/constant/index';

const CargoOrderList = props => {
  const navigation = props.navigation;
  return (
    <Grid
      onPress={() => navigation.navigate('OrderDetails')}
      style={styles.cardBox}
    >
      <Col size={2} style={{ alignItems: 'center', paddingTop: 15 }}>
        <Image
          source={require('../../../../assets/cargo/cargo_orders_icon.png')}
          style={styles.image}
        />
      </Col>
      <Col size={8} style={{ padding: 10 }}>
        <Grid>
          <Col
            style={{ alignItems: 'flex-start', justifyContent: 'center' }}
            size={4.5}
          >
            <Text style={styles.textTitle}>Books & Documents</Text>
          </Col>
        </Grid>
        <Grid style={{ paddingTop: 5 }}>
          <Col>
            <Text style={styles.textBody}>Small Size (20x11x17)</Text>
          </Col>
        </Grid>
        <Grid style={{ paddingTop: 5 }}>
          <Col>
            <Text style={styles.textTitle}>Rp24.000</Text>
          </Col>
        </Grid>
        <Grid style={{ paddingTop: 10 }}>
          <Col size={8} style={styles.col}>
            <Text style={styles.purchase}>Published Package</Text>
          </Col>
          <Col
            size={2}
            style={{ justifyContent: 'center', alignItems: 'center' }}
          >
            <Icon
              name="navigate-next"
              type="materialicon"
              color="#0066b3"
              size={30}
            />
          </Col>
        </Grid>
      </Col>
    </Grid>
  );
};

export default CargoOrderList;

const styles = StyleSheet.create({
  image: {
    width: 40,
    height: 40,
    resizeMode: 'center',
    tintColor: thameColors.primary,
  },
  cardBox: {
    borderRadius: 5,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#ffffff',
  },
  col: {
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  purchase: {
    borderRadius: 20,
    backgroundColor: '#008d00',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
    fontFamily: fontBold,
    fontSize: 16,
  },
  textCode: { fontSize: 16, color: '#222222', fontFamily: fontExtraBold },
  textBody: { fontSize: 16, color: '#222222', fontFamily: fontReguler },
  textTitle: { fontSize: 16, color: '#222222', fontFamily: fontBold },
  textPrice: { fontSize: 20, color: '#a2195b', fontFamily: fontExtraBold },
  icon: {
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
});
