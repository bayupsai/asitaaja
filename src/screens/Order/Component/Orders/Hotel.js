import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../../../base/constant/index';
import AlertModal from '../../../../components/AlertModal';
import numeral from 'numeral';

const Dot = () => (
  <Icon type="entypo" name="dot-single" color={thameColors.textBlack} />
);

class HotelOrderList extends React.PureComponent {
  render() {
    let { flightSelected, dataBookingFlight, passengerData } = this.props;
    return (
      <Grid onPress={this.props.onPress} style={styles.cardBox}>
        <Col size={2} style={{ alignItems: 'center' }}>
          {/* <Image source={{ uri: flightSelected.detail[0].img_src }} style={styles.image} /> */}
          <Image
            source={require('../../../../assets/icons/sleep_icon.png')}
            style={styles.image}
          />
        </Col>
        <Col size={8}>
          <Grid>
            <Col
              style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              size={4.5}
            >
              <Text style={styles.textTitle}>TS Suites Bali & Villas</Text>
            </Col>
          </Grid>
          <Grid style={{ paddingTop: 5 }}>
            <Col>
              {this.props.index === 0 ? (
                <Text style={styles.textBody}>Order ID 211789300</Text>
              ) : (
                <Text style={styles.textBody}>Voucher ID AABDCR</Text>
              )}
            </Col>
          </Grid>
          <Grid style={{ paddingTop: 5 }}>
            <Col>
              {this.props.index === 0 ? (
                <Text style={styles.textPrice}>
                  Rp
                  {numeral(3515400)
                    .format('0,0')
                    .replace(/,/g, '.')}
                </Text>
              ) : (
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.textPrice}>1 Guest</Text>
                  <Dot />
                  <Text style={styles.textPrice}>1 Nights</Text>
                  <Dot />
                  <Text style={styles.textPrice}>7 Rooms</Text>
                </View>
              )}
            </Col>
          </Grid>
          <Grid style={{ paddingTop: 10 }}>
            <Col size={8} style={styles.col}>
              {this.props.index === 0 ? (
                <Text style={[styles.purchase, { textAlign: 'center' }]}>
                  Waiting for Payment - 00:22:20
                </Text>
              ) : (
                <Text
                  style={[
                    styles.purchase,
                    { textAlign: 'center', backgroundColor: thameColors.green },
                  ]}
                >
                  Voucher has been Published
                </Text>
              )}
            </Col>
            <Col
              size={2}
              onPress={() => alert('Continue Payment')}
              style={{ justifyContent: 'center', alignItems: 'center' }}
            >
              <Icon
                name="navigate-next"
                type="materialicon"
                color={thameColors.oceanBlue}
                size={30}
              />
            </Col>
          </Grid>
        </Col>
      </Grid>
    );
  }
}

export default HotelOrderList;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    resizeMode: 'center',
  },
  cardBox: {
    borderRadius: 5,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: thameColors.white,
    padding: 10,
  },
  col: { alignItems: 'flex-start', justifyContent: 'center' },
  purchase: {
    borderRadius: 20,
    backgroundColor: thameColors.oceanBlue,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
  },
  textCode: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontExtraBold,
  },
  textBody: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
  },
  textTitle: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontBold,
  },
  textPrice: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontBold,
  },
  icon: {
    backgroundColor: thameColors.white,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
});
