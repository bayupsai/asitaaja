import React from 'react';
import {
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  InteractionManager,
  Alert,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
// import { DotIndicator } from 'react-native-indicators'
import { Icon } from 'react-native-elements';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../../../base/constant/index';
import numeral from 'numeral';
import { actionSelectOrder } from '../../../../redux/actions/OrdersHistoryAction';
import { scale } from '../../../../Const/ScaleUtils';

class FlightOrderList extends React.PureComponent {
  //Loading
  // _loading = () => {
  //     return (
  //         <View style={{ flex: 1, justifyContent: "center", alignItems: "center", alignSelf: "center", alignContent: "center" }}>
  //             <DotIndicator size={12} color={thameColors.darkBlue} />
  //         </View>
  //     )
  // }
  _loading = () => {
    return null;
  };

  //Render Item
  _renderItem = ({ item, index }) => {
    // let totalPrice = item.flight_data[0].flight_info.price_adult + item.flight_data[0].flight_info.price_child + item.flight_data[0].flight_info.price_infant
    let statusName = '';
    let colorStatus = thameColors.green;
    // let returnStatus = []
    let imgFlight = require('../../../../assets/icons/logo-garuda.png');
    // let lastIndex = this.props.loading ? new Array(0) : this.props.data.length - 1
    if (item.status === 'BOOKED') {
      statusName = 'Continue Payment';
      colorStatus = thameColors.yellowFlash;
    } else if (item.status === 'WAITING_PAYMENT') {
      statusName = 'Waiting Payment';
      colorStatus = thameColors.darkBlue;
    } else if (item.status === 'PAID') {
      statusName = 'E-Ticket was Issued';
      colorStatus = thameColors.green;
    } else {
      statusName = item.status;
      colorStatus = thameColors.redVelvet;
    }
    // if (item.flight_data !== null ? item.flight_data[0].flight_info.name === 'GARUDA' : 'SJ') {
    //     imgFlight = require('../../../../assets/icons/logo-garuda.png')
    // }

    return (
      <TouchableOpacity
        onPress={() => this._onPressCard('flight', item)}
        style={index === 0 ? { marginTop: 20 } : undefined}
      >
        <Grid style={styles.cardBox}>
          <Col size={2} style={{ alignItems: 'center' }}>
            {/* <Image source={{ uri: flightSelected.detail[0].img_src }} style={styles.image} /> */}
            <Image
              source={imgFlight}
              style={styles.image}
              resizeMode="contain"
            />
          </Col>
          <Col size={8} style={{ padding: 10 }}>
            <Grid>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
                size={4.5}
              >
                <Text style={styles.textTitle}>
                  {item.flight_data !== null
                    ? item.flight_data[0].flight_info.detail[0]
                        .departure_city_name
                    : null}{' '}
                  (
                  {item.flight_data !== null
                    ? item.flight_data[0].flight_info.detail[0].departure_city
                    : null}
                  )
                </Text>
              </Col>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
                size={1}
              >
                <Icon
                  name="arrow-right"
                  type="feather"
                  color={thameColors.textBlack}
                  size={15}
                />
              </Col>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
                size={4.5}
              >
                <Text style={styles.textTitle}>
                  {item.flight_data
                    ? item.flight_data[0].flight_info.detail[0]
                        .arrival_city_name
                    : null}{' '}
                  (
                  {item.flight_data
                    ? item.flight_data[0].flight_info.detail[0].arrival_city
                    : null}
                  )
                </Text>
              </Col>
            </Grid>
            <Grid style={{ paddingTop: 5 }}>
              {/* {
                            dataBookingFlight ?
                                <Col>
                                    <Text style={styles.textBody}>Booking ID: <Text style={styles.textCode}>{dataBookingFlight.partner_trxid}</Text></Text>
                                </Col>
                                : <View style={{ width: 0, height: 0 }} />
                        } */}
              <Col>
                <Text style={styles.textBody}>
                  Booking ID:{' '}
                  <Text style={styles.textCode}>{item.partner_trx_id}</Text>
                </Text>
              </Col>
            </Grid>
            <Grid style={{ paddingTop: 5 }}>
              <Col>
                {/* <Text style={styles.textBody}>{flightSelected.flight_number} - Eco {flightSelected.stop} (Subclass {flightSelected.detail[0].class})</Text> */}
                {/* <Text style={styles.textBody}>{item.flight_data[0].flight_info.flight_number} - {item.flight_data[0].flight_info.detail[0].service_class}</Text> */}
              </Col>
            </Grid>
            <Grid style={{ paddingTop: 5 }}>
              <Col>
                {/* <Text style={styles.textPrice}>Rp. {
                                numeral(dataBookingFlight ? dataBookingFlight.total : flightSelected.price_adult).format('0,0').replace(/,/g, '.')
                            }</Text> */}
                <Text style={styles.textPrice}>
                  Rp{' '}
                  {numeral(item.total_amount)
                    .format('0,0')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={{ paddingTop: 10 }}>
              <Col size={8} style={styles.col}>
                <Text
                  style={[styles.purchase, { backgroundColor: colorStatus }]}
                >
                  {statusName}
                </Text>
              </Col>
              <Col
                size={2}
                style={{ justifyContent: 'center', alignItems: 'center' }}
              >
                <Icon
                  name="navigate-next"
                  type="materialicon"
                  color={thameColors.oceanBlue}
                  size={30}
                />
              </Col>
            </Grid>
          </Col>
        </Grid>
      </TouchableOpacity>
    );
  };

  //Select The Order Card
  _onPressCard = (type, item) => {
    let { navigation, dispatch } = this.props;
    if (type === 'flight') {
      if (item.flight_data !== null) {
        InteractionManager.runAfterInteractions(() => {
          dispatch(actionSelectOrder('selectOrder', item));
          navigation.navigate('OrderDetailFlight');
        });
      } else {
        Alert.alert('Alert', 'Oops, something is wrong!');
      }
    }
  };

  //Main Render
  render() {
    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={this.props.loading ? new Array(0) : this.props.data}
        renderItem={this.props.loading ? this._loading : this._renderItem}
        removeClippedSubviews={true}
        getItemLayout={(data, index) => ({ length: 100, offset: 100, index })}
        refreshControl={<RefreshControl onRefresh={this.props.onRefresh} />}
        ListEmptyComponent={
          this.props.loading ? this._loading : this.props.listEmptyComponent
        }
      />
    );
  }
}

export default FlightOrderList;

const styles = StyleSheet.create({
  image: {
    width: scale(50),
    height: scale(50),
  },
  cardBox: {
    borderRadius: 5,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: thameColors.white,
  },
  col: { alignItems: 'flex-start', justifyContent: 'center' },
  purchase: {
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
  },
  textCode: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontExtraBold,
  },
  textBody: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
  },
  textTitle: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontBold,
  },
  textPrice: {
    fontSize: 20,
    color: thameColors.secondary,
    fontFamily: fontExtraBold,
  },
  icon: {
    backgroundColor: thameColors.white,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
});
