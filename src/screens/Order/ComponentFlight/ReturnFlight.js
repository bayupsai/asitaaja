import React, { PureComponent } from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import moment from 'moment';
//Components
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const ReturnFlight = props => {
  let { flight_data } = props.selectOrder;
  return (
    <Grid
      style={{
        height: deviceHeight / 3,
        marginVertical: 15,
        borderBottomColor: thameColors.gray,
        borderBottomWidth: 0.5,
        marginHorizontal: 25,
      }}
    >
      <Col>
        <Grid style={{ flex: 0, marginBottom: 25 }}>
          <Col style={{ alignItems: 'flex-start' }}>
            <Text style={styles.textOrange}>
              Return{' '}
              {props.condition.charAt(0).toUpperCase() +
                props.condition.slice(1)}
            </Text>
          </Col>
          <Col style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textBold}>
              {moment(flight_data[1].departure_date).format('ddd, DD MMM')}
              {/* Wed, 21 Jan */}
            </Text>
          </Col>
        </Grid>
        <Grid style={{ flex: 0 }}>
          <Col size={1.5} style={{ alignItems: 'flex-start' }}>
            <Grid>
              <Col>
                <Text style={styles.textBold}>
                  {flight_data[1].departure_time}
                  {/* 05:35 */}
                </Text>
                <Text style={styles.textInfo}>
                  {moment(flight_data[1].departure_date).format('DD MMM')}
                  {/* 21 Jan */}
                </Text>
              </Col>
            </Grid>
            <Grid>
              <Col>
                <Text style={styles.textBold}>
                  {/* 2h 5m */}
                  {flight_data[1].flight_info.duration}
                </Text>
              </Col>
            </Grid>
            <Grid>
              <Col>
                <Text style={styles.textBold}>
                  {flight_data[1].arrival_time}
                  {/* 08:40 */}
                </Text>
                <Text style={styles.textInfo}>
                  {moment(flight_data[1].arrival_date).format('DD MMM')}
                  {/* 21 Jan */}
                </Text>
              </Col>
            </Grid>
          </Col>
          <Col size={1} style={{ alignItems: 'center' }}>
            <Icon
              size={18}
              color={thameColors.gray}
              name="ios-radio-button-off"
              type="ionicon"
            />
            <View style={styles.gridRow}></View>
            <Icon
              name="ios-radio-button-on"
              color={thameColors.gray}
              size={18}
              type="ionicon"
            />
          </Col>
          <Col size={7.5} style={{ alignItems: 'flex-start' }}>
            <Grid>
              <Col>
                <Text style={styles.textBold}>
                  {flight_data[1].flight_info.detail[0].departure_city_name} (
                  {flight_data[1].departure_name}){/* Padang (PDG) */}
                </Text>
                <Text style={styles.textInfo}>
                  {flight_data[1].flight_info.detail[0].departure_airport_name}
                  Ngurah Ray
                </Text>
              </Col>
            </Grid>
            <Grid style={styles.colStart}>
              <Col
                size={3}
                style={{ justifyContent: 'flex-start', marginTop: -15 }}
              >
                <Image
                  style={{ width: 50, height: 50 }}
                  resizeMode="contain"
                  source={props.imgCore}
                />
              </Col>
              <Col size={7}>
                <Text style={styles.textItinerary}>
                  ({flight_data[1].flight_info.flight_number}) •{' '}
                  {flight_data[1].flight_info.detail[0].service_class}
                </Text>
              </Col>
            </Grid>
            <Grid>
              <Col>
                <Text style={styles.textBold}>
                  {flight_data[1].flight_info.detail[0].arrival_city_name} (
                  {flight_data[1].arrival_name}){/* Jakarta (CGK) */}
                </Text>
                <Text style={styles.textInfo}>
                  {flight_data[1].flight_info.detail[0].arrival_airport_name}
                  {/* Soekarno Hatta International Airport */}
                </Text>
              </Col>
            </Grid>
          </Col>
        </Grid>
      </Col>
    </Grid>
  );
};

export default ReturnFlight;

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  gridRow: {
    height: 60 + '%',
    width: 0.8,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  colStart: { justifyContent: 'center', alignItems: 'flex-start' },
  textItinerary: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    fontSize: 14,
    marginLeft: 3,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 14,
  },
  textOrange: {
    color: thameColors.primary,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textInfo: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 14,
  },
});
