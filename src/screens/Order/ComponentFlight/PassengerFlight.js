import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  FlatList,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import numeral from 'numeral';
//Components
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../base/constant/index';
import { TouchableOpacity } from 'react-native-gesture-handler';

const PassengerFlight = props => {
  let { travelers, flight_data, total_amount } = props.selectOrder;
  return (
    <Grid style={{ marginRight: 25, marginLeft: 25 }}>
      <Col>
        <Row style={{ marginBottom: 20 }}>
          <Text style={styles.textBold}>Passenger</Text>
        </Row>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={travelers}
          renderItem={({ item, index }) => {
            let typeMan = 'Adult';
            if (item.cabin_type == 'ADULT') {
              typeMan = 'Adult';
            } else if (item.cabin_type == 'CHILD') {
              typeMan = 'Child';
            } else {
              typeMan = 'Infant';
            }
            return (
              <Row
                key={index}
                style={{
                  marginBottom: 10,
                  paddingBottom: 10,
                  borderBottomColor: thameColors.gray,
                  borderBottomWidth: 0.5,
                }}
              >
                <Col size={7}>
                  <Text style={styles.textBold}>
                    {index + 1}. {item.first_name} {item.last_name}
                  </Text>
                </Col>
                <Col size={3} style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.textLabel}>{typeMan}</Text>
                </Col>
              </Row>
            );
          }}
        />

        {/* <Row style={{ marginBottom: 20 }}>
                    <Col size={7}>
                        <Text style={styles.textBody}>Departure Baggage</Text>
                    </Col>
                    <Col size={3} style={{ alignItems: 'flex-end' }}>
                        <Text style={styles.textLabel}>20 kg</Text>
                    </Col>
                </Row> */}

        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Text style={styles.textBold}>Policy</Text>
        </Row>
        <Row style={{ marginBottom: 10 }}>
          <Col size={7}>
            <Text style={styles.textGreen}>Refundable</Text>
          </Col>
          <Col size={3} style={{ alignItems: 'flex-end' }}>
            <TouchableOpacity>
              <Text style={styles.textBlue}>INFO</Text>
            </TouchableOpacity>
          </Col>
        </Row>
        <Row style={{ marginBottom: 20 }}>
          <Col size={7}>
            <Text style={styles.textGreen}>Reschedule Available</Text>
          </Col>
          <Col size={3} style={{ alignItems: 'flex-end' }}>
            <TouchableOpacity>
              <Text style={styles.textBlue}>INFO</Text>
            </TouchableOpacity>
          </Col>
        </Row>
        <Row style={styles.sectionHr}></Row>

        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Text style={styles.textBold}>Price Departure & Return</Text>
        </Row>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={new Array(3)}
          renderItem={({ item, index }) => {
            let type = 'Adult';
            let priceType = '';
            let amount = {
              adult: flight_data[0].cabin_info.adult,
              child: flight_data[0].cabin_info.child,
              infant: flight_data[0].cabin_info.infant,
            };
            let totalType = 1;
            if (index == 0) {
              type = 'Adult';
              priceType = flight_data[0].flight_info.price_adult;
              totalType = amount.adult;
            } else if (index == 1) {
              type = 'Child';
              priceType = flight_data[0].flight_info.price_child;
              totalType = amount.child;
            } else if (index == 2) {
              type = 'Infant';
              priceType = flight_data[0].flight_info.price_infant;
              totalType = amount.infant;
            }
            return (
              <Row
                key={index}
                style={{
                  marginBottom: 20,
                  borderBottomWidth: 0.5,
                  borderBottomColor: thameColors.gray,
                  paddingBottom: 10,
                }}
              >
                <Col size={2} style={{ alignItems: 'center' }}>
                  <Image
                    source={index == 0 ? props.imgSingle : props.imgReturn}
                    style={[
                      styles.iconFlight,
                      { tintColor: thameColors.primary },
                    ]}
                    resizeMode="stretch"
                  />
                </Col>
                <Col size={2}>
                  <Text style={styles.textLabel}>
                    {type} x{totalType}
                  </Text>
                </Col>
                <Col size={6} style={{ alignItems: 'flex-end' }}>
                  <Text style={styles.textLabel}>
                    Rp{' '}
                    {numeral(priceType)
                      .format('0,0')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Row>
            );
          }}
        />

        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Text style={styles.textBold}>Total Payment</Text>
        </Row>
        <Row
          style={{
            marginBottom: 10,
            borderBottomWidth: 0.5,
            borderBottomColor: thameColors.gray,
            paddingBottom: 10,
          }}
        >
          <Col size={7}>
            <Text style={styles.textLabel}>Baggage Charge</Text>
          </Col>
          <Col size={3} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textLabel}>Rp. 0</Text>
          </Col>
        </Row>
        <Row style={{ marginBottom: 30, marginTop: 10 }}>
          <Col size={4}>
            <Text
              style={[
                styles.textBody,
                { fontSize: 16, color: thameColors.superBack },
              ]}
            >
              Price You Pay
            </Text>
          </Col>
          {/* <Col size={6} style={{ alignItems: 'flex-end' }}><Text style={styles.textPrice}>Rp {numeral(props.amount).format("0,00").replace(/,/g, '.')}</Text></Col> */}
          <Col size={6} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textPrice}>
              Rp{' '}
              {numeral(total_amount)
                .format('0,0')
                .replace(/,/g, '.')}
            </Text>
          </Col>
        </Row>
      </Col>
    </Grid>
  );
};

export default PassengerFlight;

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  textLabel: {
    color: thameColors.textBlack,
    fontFamily: fontReguler,
    fontSize: 14,
  },
  textBody: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 14,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textBlue: {
    fontSize: 14,
    fontFamily: fontReguler,
    color: thameColors.oceanBlue,
  },
  textGreen: { color: thameColors.green, fontFamily: fontBold, fontSize: 14 },
  textPrice: { color: thameColors.primary, fontFamily: fontBold, fontSize: 18 },
  iconFlight: {
    width: 35,
    height: 25,
  },
});
