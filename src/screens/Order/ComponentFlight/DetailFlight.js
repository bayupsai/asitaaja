import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  FlatList,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../base/constant/index';

const window = Dimensions.get('window');

class DetailFlight extends React.PureComponent {
  _renderItem = ({ item, index }) => {
    // let { flight_data } = props.selectOrder
    // let lastIndex = flight_data.length - 1
    let typeFlight = '';
    if (index == 0) {
      typeFlight = 'Departure';
    } else {
      typeFlight = 'Return';
    }
    return (
      <Grid
        key={index}
        style={{
          height: window.height / 3,
          marginVertical: 15,
          borderBottomColor: thameColors.gray,
          borderBottomWidth: 0.5,
          marginHorizontal: 25,
        }}
      >
        <Col>
          <Grid style={{ flex: 0, marginBottom: 10 }}>
            <Col style={{ alignItems: 'flex-start' }}>
              <Text style={styles.textOrange}>
                {typeFlight}{' '}
                {this.props.condition.charAt(0).toUpperCase() +
                  this.props.condition.slice(1)}
              </Text>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <Text style={styles.textBold}>
                {/* {moment(props.departure_date).format("ddd, DD MMM")} */}
                {moment(item.departure_date).format('ddd, DD MMM')}
              </Text>
            </Col>
          </Grid>
          <Grid style={{ flex: 0 }}>
            <Col size={1.5} style={{ alignItems: 'flex-start' }}>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>
                    {item.departure_time}
                    {/* 05:35 */}
                  </Text>
                  <Text style={styles.textInfo}>
                    {moment(item.departure_date).format('DD MMM')}
                    {/* 21 Jan */}
                  </Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={[styles.textBold, { fontSize: 12 }]}>
                    {/* 2h 5m */}
                    {item.flight_info.duration}
                  </Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>
                    {item.arrival_time}
                    {/* 08:40 */}
                  </Text>
                  <Text style={styles.textInfo}>
                    {moment(item.arrival_date).format('DD MMM')}
                    {/* 21 Jan */}
                  </Text>
                </Col>
              </Grid>
            </Col>
            <Col size={1} style={{ alignItems: 'center' }}>
              <Icon
                size={18}
                color={thameColors.gray}
                name="ios-radio-button-off"
                type="ionicon"
              />
              <View style={styles.gridRow}></View>
              <Icon
                name="ios-radio-button-on"
                color={thameColors.gray}
                size={18}
                type="ionicon"
              />
            </Col>
            <Col size={7.5} style={{ alignItems: 'flex-start' }}>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>
                    {item.flight_info.detail[0].departure_city_name} (
                    {item.flight_info.detail[0].departure_city})
                    {/* Jakarta (CGK) */}
                  </Text>
                  <Text style={styles.textInfo}>
                    {item.flight_info.detail[0].departure_airport_name}
                    {/* Soekarno Hatta International Airport */}
                  </Text>
                </Col>
              </Grid>
              <Grid style={styles.colStart}>
                <Col
                  size={3}
                  style={{ justifyContent: 'flex-start', marginTop: -15 }}
                >
                  <Image
                    style={{ width: 50, height: 50 }}
                    resizeMode="contain"
                    source={this.props.imgCore}
                  />
                </Col>
                <Col size={7}>
                  <Text style={styles.textItinerary}>
                    ({item.flight_info.flight_number}) •{' '}
                    {item.flight_info.detail[0].service_class}
                  </Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>
                    {item.flight_info.detail[0].arrival_city_name} (
                    {item.flight_info.detail[0].arrival_city})
                    {/* Padang (PDG) */}
                  </Text>
                  <Text style={styles.textInfo}>
                    {item.flight_info.detail[0].arrival_airport_name}
                    {/* Ngurah Ray */}
                  </Text>
                </Col>
              </Grid>
            </Col>
          </Grid>
          {/* <Grid><Col style={styles.sectionHr}></Col></Grid> */}
        </Col>
      </Grid>
    );
  };

  //Main Render
  render() {
    let { flight_data } = this.props.selectOrder;
    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={flight_data ? flight_data : new Array(0)}
        renderItem={this._renderItem}
        ListEmptyComponent={
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text>DATA Empty</Text>
          </View>
        }
        removeClippedSubviews={true}
      />
    );
  }
}

export default DetailFlight;

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  gridRow: {
    height: 60 + '%',
    width: 0.8,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  colStart: { justifyContent: 'center', alignItems: 'flex-start' },
  textItinerary: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    fontSize: 14,
    marginLeft: 3,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 14,
  },
  textOrange: {
    color: thameColors.primary,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textInfo: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 14,
  },
});
