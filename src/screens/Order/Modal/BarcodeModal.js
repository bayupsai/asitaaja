import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Modal from 'react-native-modal';
import numeral from 'numeral';
import { InputText } from '../../../elements/TextInput';

//local component
import { Button } from '../../../elements/Button';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const BarcodeModal = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visible}
      onBackButtonPress={props.closeModal}
      onBackdropPress={props.closeModal}
      style={styles.bottomModal}
    >
      <View style={styles.modalPassenger}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#e2e2e2',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={props.closeModal}
          >
            <Col style={{ alignItems: 'center' }}>
              <Text
                style={{ color: '#000', fontSize: 16, fontFamily: fontBold }}
              >
                {' '}
                Locker QR Code
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid style={{ marginTop: 10 }}>
          <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Image
              source={require('../../../assets/cargo/dummy_qrcode.png')}
              resizeMode="center"
              style={{ width: 100, height: 100 }}
            />
          </Col>
        </Grid>
        {/* <Grid>
                    <Col style={{ alignItems: "center", justifyContent: "center" }}>
                        <Text style={{ color: "#000", fontSize: 16, fontFamily: fontReguler }}>or</Text>
                    </Col>
                </Grid> */}
        <Grid>
          <Col style={{ alignItems: 'center', justifyContent: 'flex-start' }}>
            <View style={{ flexDirection: 'row', marginTop: 15 }}>
              <Col
                size={4.5}
                style={{
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                  width: deviceWidth,
                  height: 0.5,
                  backgroundColor: '#838383',
                }}
              />
              <Col
                size={1}
                style={{
                  marginTop: -15,
                  position: 'relative',
                  width: 150,
                  height: 40,
                  borderRadius: 152 / 2,
                  borderWidth: 0.5,
                  borderColor: '#838383',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Text
                  style={{
                    marginTop: -6,
                    fontSize: 16,
                    fontFamily: fontBold,
                    padding: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    color: '#828282',
                  }}
                >
                  or
                </Text>
              </Col>
              <Col
                size={4.5}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: deviceWidth,
                  height: 0.5,
                  backgroundColor: '#838383',
                }}
              />
            </View>
            <View style={{ paddingTop: 15 }}>
              <Text
                style={{ color: '#000', fontSize: 16, fontFamily: fontReguler }}
              >
                Use Locker PIN Number
              </Text>
            </View>
            <View style={{ paddingTop: 10 }}>
              <Text
                style={{ color: '#000', fontSize: 22, fontFamily: fontBold }}
              >
                144099
              </Text>
            </View>
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default BarcodeModal;

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
