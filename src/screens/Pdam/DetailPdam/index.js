/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { ScrollView, View, Text } from 'react-native';
import moment from 'moment';
import numeral from 'numeral';
import { Grid, Col } from 'react-native-easy-grid';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import styles from './styles';
import { ButtonRounded } from '../../../elements/Button';

class DetailPdam extends React.PureComponent {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
      region: navigation.getParam('region'),
      customerNumber: navigation.getParam('customerNumber'),
    };
  }

  goBack = () => {
    const {
      navigation: { goBack },
    } = this.props;
    goBack();
  };

  // Main Render
  render() {
    const { region, customerNumber } = this.state;
    return (
      <ScrollView style={styles.container}>
        <HeaderPage title="Payment Details" callback={this.goBack} />
        <SubHeaderPage />
        <View style={styles.toTop}>
          <View style={[styles.card, styles.alignCenter, styles.cardHeader]}>
            <Text style={styles.textCard}>
              You will not be able to change your details below once you proceed
              to payment
            </Text>
          </View>

          <View style={styles.card}>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Nama PDAM</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>{region.product}</Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>No Pelanggan</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>{customerNumber}</Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Nama</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>Budi Setiawan</Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Total Tagihan</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>
                  Rp{' '}
                  {numeral(156984)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Periode</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>
                  {moment().format('MMMM YYYY')}
                </Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Pemakain</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>0 M3</Text>
              </Col>
            </Grid>
            <View style={{ marginTop: 20 }}>
              <Text style={styles.textBold}>Detail Tagihan</Text>
              <Grid style={styles.itemCard}>
                <Col>
                  <Text style={styles.textGray}>Jumlah Tagihan</Text>
                </Col>
                <Col>
                  <Text style={styles.textSemiBold}>
                    Rp{' '}
                    {numeral(154484)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Grid>
              <Grid style={styles.itemCard}>
                <Col>
                  <Text style={styles.textGray}>Biaya Admin</Text>
                </Col>
                <Col>
                  <Text style={styles.textSemiBold}>
                    Rp{' '}
                    {numeral(2750)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Grid>
              <Grid style={[styles.itemCard, { borderBottomWidth: 0 }]}>
                <Col>
                  <Text style={styles.textGray}>Denda</Text>
                </Col>
                <Col>
                  <Text style={styles.textSemiBold}>
                    Rp{' '}
                    {numeral(0)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Grid>
            </View>
          </View>

          <View style={[styles.card, { paddingVertical: 20 }]}>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Harga</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>
                  Rp{' '}
                  {numeral(156984)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={[styles.itemCard, { borderBottomWidth: 0 }]}>
              <Col>
                <Text style={styles.textGray}>Price You Pay</Text>
              </Col>
              <Col>
                <Text style={styles.textPrice}>
                  Rp{' '}
                  {numeral(156984)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
          </View>
        </View>
        <View style={{ margin: 20 }}>
          <ButtonRounded
            buttonStyle={styles.btnColor}
            label="CONTINUE TO PAYMENT"
          />
        </View>
      </ScrollView>
    );
  }
}

export default DetailPdam;
