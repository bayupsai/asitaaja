import { StyleSheet } from 'react-native';
import {
  thameColors,
  fontReguler,
  fontBold,
  fontSemiBold,
  asitaColor,
} from '../../../base/constant';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: thameColors.white,
    borderRadius: 5,
  },
  cardHeader: {
    paddingVertical: 25,
    paddingHorizontal: 25,
  },
  itemCard: {
    flex: 0,
    borderBottomColor: thameColors.gray,
    borderBottomWidth: 0.5,
    paddingBottom: 5,
    marginVertical: 5,
  },
  alignCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textCard: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    textAlign: 'center',
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
    fontSize: 18,
    alignSelf: 'flex-end',
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 20,
  },
  textGray: {
    fontFamily: fontReguler,
    color: thameColors.gray,
  },
  textPrice: {
    fontFamily: fontBold,
    color: asitaColor.tealBlue,
    fontSize: 20,
    alignSelf: 'flex-end',
  },
  toTop: {
    marginTop: -50,
  },
  btnColor: {
    backgroundColor: asitaColor.tealBlue,
  },
});
