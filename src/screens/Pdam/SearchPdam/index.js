/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity as Touch,
  FlatList,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { Grid, Col, Row } from 'react-native-easy-grid';
import AlertModal from '../../../components/AlertModal';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import styles from './styles';
import { SearchInput, InputText } from '../../../elements/TextInput';
import { ButtonRounded } from '../../../elements/Button';
import Modalin from './components/Modal';
import { region } from './data';
import { thameColors } from '../../../base/constant';
import { makeSearchPDAM } from '../../../redux/selectors/PpobSelector';
import { actionSearchPDAM } from '../../../redux/actions/PpobAction';

class Pdam extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modal: null,
      region: '',
      customerNumber: 30008997,
    };
  }

  componentDidMount() {
    this.getSearchPdam('');
  }

  // Search PDAM
  getSearchPdam = text => {
    const { dispatch } = this.props;
    const payload = {
      searchString: text,
    };
    dispatch(actionSearchPDAM(payload))
      .then(() => {
        null;
      })
      .catch(err => {
        Alert('Alert', err.message);
      });
  };

  // Modal
  showModal = id => {
    requestAnimationFrame(() => {
      this.setState({ modal: id });
    });
  };

  modalContent1 = () => {
    const { modalContent, toTop, itemRegion } = styles;
    const { searchPdam } = this.props;
    const renderItem = ({ item, index }) => (
      <Touch
        onPress={() => this.selectRegion(item)}
        key={index}
        style={[
          itemRegion,
          index === region.length - 1 ? { borderBottomWidth: 0 } : null,
        ]}
      >
        <Text style={styles.textRegion}>{item.product}</Text>
      </Touch>
    );

    return (
      <View style={[modalContent]}>
        <HeaderPage
          title="Choose Region"
          callback={() => this.showModal(null)}
        />
        <SubHeaderPage />
        <View style={toTop}>
          <View style={{ marginHorizontal: 10 }}>
            <SearchInput
              placeholder="Search Region"
              returnKeyType="search"
              onSubmitEditing={event =>
                this.getSearchPdam(event.nativeEvent.text)
              }
            />
          </View>
        </View>
        <ScrollView>
          <View style={styles.cardRegion}>
            <FlatList
              data={searchPdam}
              keyExtractor={(__, index) => index.toString()}
              renderItem={renderItem}
              removeClippedSubviews={true}
              ListEmptyComponent={
                <View style={{ alignItems: 'center' }}>
                  <Text>Oopps, we couldn't find your region.</Text>
                </View>
              }
            />
          </View>
        </ScrollView>
      </View>
    );
  };

  modalContent2 = () => {
    const { customerNumber } = this.state;
    return (
      <View style={styles.modalSeatClass}>
        <Grid style={styles.modalContent2}>
          <Touch style={styles.row1} onPress={() => this.showModal(null)}>
            <Col style={styles.center}>
              <Text style={styles.textBold}>Customer Number</Text>
            </Col>
          </Touch>
        </Grid>
        <Grid>
          <Col style={{ backgroundColor: thameColors.white }}>
            <Grid style={styles.topPadding}>
              <Row style={styles.bottom20}>
                <Col size={7} style={styles.left0}>
                  <InputText
                    label="Enter Customer ID"
                    required="*"
                    value={customerNumber}
                    editable
                    onChangeText={data =>
                      this.setState({ customerNumber: data })
                    }
                  />
                </Col>
              </Row>
              <Row style={styles.bottom20}>
                <Col size={6} style={styles.sectionButtonArea}>
                  <ButtonRounded
                    label="SAVE"
                    onClick={() => this.showModal(null)}
                  />
                </Col>
              </Row>
            </Grid>
          </Col>
        </Grid>
      </View>
    );
  };

  // Function
  goBack = () => {
    const {
      navigation: { goBack },
    } = this.props;
    goBack();
  };

  selectRegion = city => {
    requestAnimationFrame(() => {
      this.setState({ region: city }, () => this.showModal(null));
    });
  };

  payPdam = () => {
    const { customerNumber } = this.state;
    if (this.state.region) {
      requestAnimationFrame(() => {
        this.props.navigation.navigate('DetailPdam', {
          region: this.state.region,
          customerNumber,
        });
      });
    } else {
      this.showModal(404);
    }
  };

  render() {
    const { container, button, toTop, modal1, modal2 } = styles;
    const { modal, customerNumber } = this.state;
    return (
      <ScrollView style={container}>
        <HeaderPage title="Pay PDAM Bills" callback={this.goBack} />
        <SubHeaderPage />
        <View style={toTop}>
          <Grid>
            <Col>
              <Touch
                onPress={() => this.showModal(1)}
                activeOpacity={0.7}
                style={[styles.card, { alignItems: 'center' }]}
              >
                <Text style={styles.textLabel}>Region</Text>
                <Text style={[styles.textBold, { textAlign: 'center' }]}>
                  {this.state.region.product}
                </Text>
              </Touch>
            </Col>
          </Grid>
          <Grid>
            <Col>
              <Touch
                onPress={() => this.showModal(2)}
                activeOpacity={0.7}
                style={[styles.card, { alignItems: 'center' }]}
              >
                <Text style={styles.textLabel}>Customer Number</Text>
                <Text style={styles.textBold}>{customerNumber}</Text>
              </Touch>
            </Col>
          </Grid>
          <Grid>
            <View style={button}>
              <ButtonRounded
                buttonStyle={styles.btnColor}
                label="PAY PDAM"
                onClick={this.payPdam}
              />
            </View>
          </Grid>
        </View>

        {/* ======================= Modal ======================= */}
        <Modalin
          isVisible={modal}
          dismiss={() => this.showModal(null)}
          style={modal === 1 ? modal1 : modal2}
          children={modal === 1 ? this.modalContent1() : this.modalContent2()}
        />

        <AlertModal
          isVisible={modal === 404}
          onDismiss={() => this.showModal(null)}
          type="normal"
          title="Alert"
          contentText="Please select Region first"
          onPress={() => this.showModal(null)}
        />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  searchPdam: makeSearchPDAM(state),
});

export default connect(mapStateToProps)(Pdam);
