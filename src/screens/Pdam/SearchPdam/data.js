export const region = [
  { title: 'Kota Bandung' },
  { title: 'Palyja Jakarta' },
  { title: 'Aetra Jakarta' },
  { title: 'Kota Manado' },
  { title: 'Kota Bogor' },
  { title: 'Kota Surabaya' },
  { title: 'Kota Banjarmasin' },
  { title: 'Kabupaten Bogor' },
  { title: 'Kota Balikpapan' },
  { title: 'Kota Solo' },
  { title: 'Banyumas' },
  { title: 'Cilacap' },
];
