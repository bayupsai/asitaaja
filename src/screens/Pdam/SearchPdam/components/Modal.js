import React from 'react';
import { StyleSheet, ViewPropTypes } from 'react-native';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    margin: 0,
  },
});

const Modalin = props => {
  const { container } = styles;
  return (
    <Modal
      isVisible={props.isVisible}
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      onBackButtonPress={props.dismiss}
      onBackdropPress={props.dismiss}
      children={props.children}
      style={[container, props.style]}
      // backdropColor={thameColors.white}
    />
  );
};

export default Modalin;

Modalin.propTypes = {
  isVisible: PropTypes.any.isRequired,
  dismiss: PropTypes.func.isRequired,
  children: PropTypes.any.isRequired,
  style: ViewPropTypes.style,
};
