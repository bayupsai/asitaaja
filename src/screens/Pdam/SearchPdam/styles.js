import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import {
  thameColors,
  fontReguler,
  fontSemiBold,
  asitaColor,
} from '../../../base/constant';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  row1: { flex: 1, flexDirection: 'row' },
  center: { alignItems: 'center', justifyContent: 'center' },
  topPadding: { paddingTop: 20 },
  bottom20: { flex: 0, marginBottom: 20 },
  left0: { marginLeft: 0 },
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  button: {
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
  btnColor: {
    backgroundColor: asitaColor.tealBlue,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: thameColors.white,
    borderRadius: 5,
  },
  cardRegion: {
    backgroundColor: thameColors.white,
    marginTop: 20,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    padding: 20,
  },
  itemRegion: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: thameColors.gray,
    width: '100%',
    alignItems: 'center',
  },
  toTop: {
    marginTop: -50,
  },
  modal1: {
    backgroundColor: thameColors.backWhite,
  },
  modal2: {
    justifyContent: 'flex-end',
  },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.25,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: deviceHeight,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent2: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  textLabel: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 16,
    paddingBottom: 5,
  },
  textBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
    fontSize: 20,
    paddingBottom: 4,
  },
  textRegion: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    fontSize: 18,
    textAlign: 'center',
  },
});
