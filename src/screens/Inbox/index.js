import React from 'react';
import { View, StyleSheet, StatusBar, Platform } from 'react-native';

//IMPORT COMPONENT
import Heading from '../../components/HeaderLogin';
import Card from './Component/Card';
import AlertModal from '../../components/AlertModal';
import InboxEmpty from './Component/InboxEmpty';
import { thameColors, asitaColor } from '../../base/constant';

const isAndroid = Platform.OS === 'android';

class Inbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataCard: [
        {
          id: 1,
          title: 'Your flight is gonna be ready Tomorrow',
          type: 'noread',
          time: 'Update 2 hours ago',
        },
        {
          id: 2,
          title: 'E-Ticket has been published',
          type: 'noread',
          time: 'Update 2 hours ago',
        },
        {
          id: 3,
          title: 'Please pay for your order',
          type: 'read',
          time: 'Update 3 hours ago',
        },
      ],
      visible: null,
    };
  }
  componentDidMount() {
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      isAndroid && StatusBar.setBackgroundColor(asitaColor.marineBlue);
    });
  }
  componentWillUnmount() {
    this._navListener.remove();
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  // Modal
  _setNullModal = () => this.setState({ visible: null });
  _setModal1 = () => this.setState({ visible: 1 });
  // Modal
  _renderItem = ({ item, i }) => (
    <Card key={i} title={item.title} time={item.time} type={item.type} />
  );

  render() {
    return (
      <View style={styles.container}>
        <Heading
          title="My Inbox"
          {...this.props}
          callback={this._goBack}
          callmenu={this._setModal1}
        />
        <InboxEmpty />
        {/* <FlatList
                    data={this.state.dataCard}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                /> */}

        {/* Alert */}
        <AlertModal
          type="normal"
          isVisible={this.state.visible === 1}
          title="Alert"
          contentText="Menu"
          onPress={this._setNullModal}
          onDismiss={this._setNullModal}
        />
        {/* Alert */}
      </View>
    );
  }
}

export default Inbox;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.white,
  },
});
