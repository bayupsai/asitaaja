import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { fontReguler, thameColors } from '../../../base/constant';
import AlertModal from '../../../components/AlertModal';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class Card extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: null,
      errorMessage: '',
    };
  }

  render() {
    return (
      <Grid style={styles.card} {...this.props}>
        {/* Alert */}
        <AlertModal
          type="normal"
          isVisible={this.state.visible === 1}
          title="Alert"
          contentText="Alert"
          onPress={() => this.setState({ visible: null })}
          onDismiss={() => this.setState({ visible: null })}
        />
        {/* Alert */}

        <Col size={1.5} style={{ marginRight: 10, alignItems: 'center' }}>
          <View style={{ justifyContent: 'flex-start' }}>
            {this.props.type == 'noread' ? (
              <Icon
                type="zocial"
                name="email"
                color={thameColors.secondary}
                size={35}
              />
            ) : this.props.type == 'read' ? (
              <Image
                source={require('../../../assets/icons/icon-email-open.png')}
                style={{
                  tintColor: thameColors.secondary,
                  width: 35,
                  height: 32,
                  resizeMode: 'center',
                  marginTop: 5,
                }}
              />
            ) : (
              <View></View>
            )}
          </View>
          <View></View>
        </Col>
        <Col size={7.3} style={{ marginTop: 3, marginBottom: 3 }}>
          <View>
            <Text style={styles.textTitle}>{this.props.title}</Text>
          </View>
          <View>
            <Text style={styles.textTime}>{this.props.time}</Text>
          </View>
        </Col>
        <Col size={1.2} style={styles.col}>
          <TouchableOpacity onPress={() => this.setState({ visible: 1 })}>
            <Icon
              name="navigate-next"
              type="materialicon"
              color="#0066b3"
              size={30}
            />
          </TouchableOpacity>
        </Col>
        {/* <Col size={1.5} style={styles.col}>
                    <Icon
                        type={
                            this.props.type == "cargo" ? (
                                "zocial"
                            ) : (
                                    "material-community"
                                )
                        }
                        name={
                            this.props.type == "cargo" ? (
                                "email"
                            ) : (
                                    this.props.type == "flight" ? (
                                        "email-open"
                                    ) : (
                                            this.props.type == "hotel" ? (
                                                "email-open"
                                            ) : (
                                                    ""
                                                )
                                        )
                                )
                        }
                        color='#008c9a'
                        size={35}
                    />
                </Col>
                <Col size={7.5}>
                    <Text style={styles.textTitle}>{this.props.title}</Text>
                    <Text>{this.props.time}</Text>
                </Col> */}
        {/* <Col size={1} style={styles.col}> */}
        {/* {
                        this.props.type !== "cargo" ? (
                            <TouchableOpacity  onPress={() => alert('Alert')}>
                                <Icon name="navigate-next" type="materialicon" color="#0066b3" size={30} />
                            </TouchableOpacity>
                        ) : (
                                <View />
                            )
                    } */}
        {/* <TouchableOpacity  onPress={() => alert('Alert')}>
                        <Icon name="navigate-next" type="materialicon" color="#0066b3" size={30} />
                    </TouchableOpacity>
                </Col> */}
      </Grid>
    );
  }
}

export default Card;

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    padding: 10,
    height: deviceHeight / 7.5,
    borderRadius: 5,
    backgroundColor: '#FFF',
  },
  col: { justifyContent: 'center', alignItems: 'center' },
  button: {
    width: 75,
    height: 25,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#009688',
  },
  textTitle: {
    fontSize: 16,
    color: '#222222',
    fontFamily: 'NunitoSans',
  },
  textTime: {
    fontSize: 14,
    color: '#222222',
    fontFamily: fontReguler,
  },
});
