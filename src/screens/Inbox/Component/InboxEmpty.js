import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontBold, fontReguler, thameColors } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class InboxEmpty extends React.Component {
  render() {
    return (
      <View style={styles.cardBox}>
        <Grid style={{ marginTop: 30, flex: 0 }}>
          <Col style={{ alignItems: 'center' }}>
            <Image
              style={styles.image}
              resizeMode={'contain'}
              source={require('../../../assets/img/empty_inbox.png')}
            />
          </Col>
        </Grid>
        <Grid style={{ paddingTop: 30 }}>
          <Col style={{ alignItems: 'center' }}>
            <View>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.superBack,
                  fontSize: 22,
                }}
              >
                No Inbox Found
              </Text>
            </View>
            <View style={{ paddingTop: 5 }}>
              <Text style={{ fontSize: 16, fontFamily: fontReguler }}>
                You haven’t made any purchases
              </Text>
            </View>
          </Col>
        </Grid>
      </View>
    );
  }
}
export default InboxEmpty;

const styles = StyleSheet.create({
  image: {
    width: deviceHeight / 2,
    height: deviceHeight / 3,
    resizeMode: 'center',
  },
  cardBox: {
    width: deviceWidth,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
});
