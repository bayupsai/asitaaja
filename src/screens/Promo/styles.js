import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  card: {
    // marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  imgPromo: {
    width: '100%',
    height: 200,
    borderRadius: 5,
  },
});
