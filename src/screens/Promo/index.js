import React, { PureComponent } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native';

//import component
import styles from './styles';
import HeaderPage from '../../components/HeaderPage';

class Promo extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataPromo: [
        { id: 1, img: require('../../assets/offers/offers-1.png') },
        { id: 2, img: require('../../assets/offers/offers-2.png') },
        { id: 3, img: require('../../assets/offers/offers-3.png') },
        { id: 4, img: require('../../assets/offers/offers-1.png') },
        { id: 5, img: require('../../assets/offers/offers-2.png') },
        { id: 6, img: require('../../assets/offers/offers-3.png') },
        { id: 7, img: require('../../assets/offers/offers-1.png') },
        { id: 8, img: require('../../assets/offers/offers-2.png') },
        { id: 9, img: require('../../assets/offers/offers-3.png') },
      ],
    };
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _renderItem = ({ item, i }) => (
    <TouchableOpacity key={i} style={styles.card}>
      <Image source={item.img} style={styles.imgPromo} resizeMode="center" />
    </TouchableOpacity>
  );
  render() {
    return (
      <View style={styles.container}>
        <HeaderPage title="Promo" callback={this._goBack} {...this.props} />
        <FlatList
          data={this.state.dataPromo}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text>Nothing Promo Today</Text>
            </View>
          }
        ></FlatList>
      </View>
    );
  }
}

export default Promo;
