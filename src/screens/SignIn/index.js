import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  Platform,
  InteractionManager,
  StatusBar,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
import HeaderLogin from '../../components/HeaderLogin';
import { InputLogin, InputPassword } from '../../elements/TextInput';
import { ScrollView } from 'react-native-gesture-handler';
import { DotIndicator } from 'react-native-indicators';
import { connect } from 'react-redux';
import {
  actionGetToken,
  actionLogOutSocmed,
  actionLoginSocmed,
} from '../../redux/actions/SocmedAction';
import { actionLoginMail } from '../../redux/actions/LoginAction';
import { failedState as failedRegister } from '../../redux/actions/RegisterAction';
import { actionGetProfile } from '../../redux/actions/ProfileAction';
import MyAccount from './MyAccount/MyAccount';
import {
  fontBold,
  fontReguler,
  fontRegulerItalic,
  fontExtraBold,
  asitaColor,
  thameColors,
} from '../../base/constant';
import { validateEmailFormat } from '../../utilities/helpers';
import AlertModal from '../../components/AlertModal';
import { google, facebook } from 'react-native-simple-auth';
import {
  googleAndroid,
  googleIos,
  facebookConf,
} from '../../services/SignInProvider';
import { makeDataLogin } from '../../redux/selectors/LoginSelector';

const window = Dimensions.get('window');
var deviceWidth = window.width;

const isAndroid = Platform.OS === 'android';

class LoginPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isValidEmail: true,
      modalVisible: null,
      error: '',
      googleAuth: '',
      facebookAuth: '',
      facebookToken: '',
      googleToken: '',
      logFacebook: this.props.statusFacebook
        ? this.props.statusFacebook
        : false,
      logGoogle: this.props.statusGoogle ? this.props.statusGoogle : false,
      authUser: '',
      secureText: true,
    };
  }

  componentDidMount() {
    let { dispatch } = this.props;
    dispatch(failedRegister('registerApply', {}, ''));
    dispatch(failedRegister('registerConfirm', {}, ''));
    dispatch(failedRegister('registerNow', {}, ''));
    dispatch(failedRegister('mobileApply', {}, ''));
    dispatch(failedRegister('mobileConfirm', {}, ''));
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      isAndroid && StatusBar.setBackgroundColor(asitaColor.marineBlue);
    });
  }
  componentWillUnmount() {
    this._navListener.remove();
  }

  // ===================== Login Google =====================
  _googleTros = () => {
    let { dispatch } = this.props;
    Platform.OS === 'ios'
      ? google(googleIos)
          .then(info => {
            dispatch(actionGetToken('google', info.credentials.id_token));
            this.actionGoogle(info.credentials.id_token);
          })
          .catch(err => {})
      : google(googleAndroid)
          .then(info => {
            dispatch(actionGetToken('google', info.credentials.id_token));
            this.actionGoogle(info.credentials.id_token);
          })
          .catch(err => {});
  };

  actionGoogle = token => {
    let { dispatch } = this.props;
    let payloadData = {
      email: '',
      password: '',
      code: token,
      authType: 'google',
    };
    dispatch(actionLoginSocmed('google', payloadData)) // <- Handle login after get Google Token
      .then(res => {
        if (res.type == 'LOGIN_GOOGLE_SUCCESS') {
          let { access_token } = res.data;
          dispatch(actionGetProfile(access_token)) // <- Handle Get Profile data after Login aeroaja with google
            .then(res => {})
            .catch(err => {
              // this._logoutGoogle()
              this._setError(err, 2);
            });
        } else {
          // this._logoutGoogle()
          this._setError(res.message, 2);
        }
      })
      .catch(err => {
        // this._logoutGoogle()
        this._setError(err, 2);
      });
  };
  // ===================== Login Google =====================

  // ===================== Login Facebook =====================
  _facebookTros = () => {
    let { dispatch } = this.props;
    facebook(facebookConf)
      .then(info => {
        dispatch(actionGetToken('facebook', info.credentials.access_token));
        this.actionFacebook(info.credentials.access_token);
      })
      .catch(err => {
        // console.log(err)
      });
  };

  actionFacebook = token => {
    let { dispatch } = this.props;
    let payloadData = {
      email: '',
      password: '',
      code: token,
      authType: 'facebook',
    };
    dispatch(actionLoginSocmed('facebook', payloadData)) // Handle Login using Token from Facebook
      .then(res => {
        if (res.type == 'LOGIN_FACEBOOK_SUCCESS') {
          dispatch(actionGetProfile(res.data.access_token)) // <- Handle Get Profile after Success Login Socmed Facebook
            .then(res => {})
            .catch(err => {
              dispatch(actionLogOutSocmed('facebook'));
              this._setError(err, 2);
            });
        } else {
          dispatch(actionLogOutSocmed('facebook'));
          this._setError(res.message, 2);
        }
      })
      .catch(err => {
        dispatch(actionLogOutSocmed('facebook'));
        this._setError(err, 2);
      });
  };

  // ===================== Login Facebook =====================

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  goToMenu = () => {
    this.props.navigation.goMenu();
  };

  goNavigate = route => {
    const { navigate } = this.props.navigation;
    navigate(route);
  };
  navigateSignUp = () => {
    const { navigate } = this.props.navigation;
    InteractionManager.runAfterInteractions(() => {
      navigate('SignUp');
    });
  };

  doLogin = () => {
    if (this.props.loading == false) {
      if (this.state.email == '' || this.state.password == '') {
        this.setState({ modalVisible: 1 });
      } else {
        let payload = {
          email: this.state.email,
          password: this.state.password,
        };
        this.props
          .dispatch(actionLoginMail(payload))
          .then(res => {
            if (res.type == 'LOGIN_MAIL_SUCCESS') {
              this.props
                .dispatch(actionGetProfile(this.props.dataLogin.access_token))
                .then(res => {})
                .catch(err => {
                  // console.log(err)
                });
            } else {
              this.setState({ modalVisible: 2, error: res.message });
            }
          })
          .catch(err => {
            this.setState({ error: err, modalVisible: 99 });
          });
      }
    }
  };

  handleInputEmail = email => {
    let checkEmail = validateEmailFormat(email);
    if (checkEmail) {
      this.setState({ email: email, isValidEmail: true });
    } else {
      this.setState({ email: '', isValidEmail: false });
    }
  };

  errorAlert = (err, id) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.modalVisible === id}
        onDismiss={this.resetModal()}
        onPress={this.resetModal()}
        title="Alert"
        contentText={err}
      />
    );
  };
  _setError = (err, id) => {
    this.setState({ error: err, modalVisible: id });
  };

  resetModal = () => {
    this.setState({ modalVisible: null });
  };

  _showPassword = () => this.setState({ secureText: !this.state.secureText });

  //Main Render
  render() {
    return this.props.isLogin === false ? (
      <View style={{ flex: 1 }}>
        {/* ==================== modal ================= */}
        <AlertModal
          type="normal"
          isVisible={this.state.modalVisible === 1}
          onDismiss={this.resetModal}
          onPress={this.resetModal}
          title="Warning"
          contentText="Please enter your email and password correctly"
        />
        <AlertModal
          type="normal"
          isVisible={this.state.modalVisible === 2}
          onDismiss={this.resetModal}
          onPress={this.resetModal}
          title="Login Failed"
          contentText={this.state.error}
        />
        <AlertModal
          type="normal"
          isVisible={this.state.modalVisible === 3}
          onDismiss={this.resetModal}
          onPress={this.resetModal}
          title="Alert"
          contentText={this.state.error}
        />
        <Modal
          useNativeDriver={true}
          isVisible={this.props.loadingProfile || this.props.loadingSocmed}
        >
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <View
              style={{
                width: '95%',
                padding: 10,
                paddingVertical: 50,
                borderRadius: 5,
                backgroundColor: asitaColor.white,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text
                style={{
                  fontFamily: fontRegulerItalic,
                  color: asitaColor.black,
                  fontSize: 18,
                }}
              >
                Wait a moment....
              </Text>
            </View>
          </View>
        </Modal>
        {/* ==================== modal ================= */}

        <HeaderLogin
          title="Log In"
          callback={this.goBackHeader}
          {...this.props}
        />
        <ScrollView style={{ flex: 1, backgroundColor: 'ffffff' }}>
          <Grid style={{ marginTop: 30, flex: 0 }}>
            <Col style={styles.col}>
              <View>
                <Text style={[styles.textBold, { fontSize: 18 }]}>
                  Hi, There
                </Text>
              </View>
              <View style={{ paddingTop: 5 }}>
                <Text style={[styles.textBody, { fontSize: 17 }]}>
                  Log In to your account here.
                </Text>
              </View>
            </Col>
          </Grid>
          <Grid
            style={{ flex: 0, marginTop: 30, marginLeft: 20, marginRight: 20 }}
          >
            <Col size={4.5}>
              <Button
                buttonStyle={{
                  backgroundColor: asitaColor.white,
                  borderColor: asitaColor.greyish,
                  borderWidth: 1,
                }}
                icon={
                  <Image
                    style={styles.image}
                    source={require('../../assets/icons/icon_google.png')}
                  />
                }
                title="Google"
                containerStyle={{ height: 50 }}
                titleStyle={{ color: asitaColor.black, paddingRight: 30 }}
                onPress={this._googleTros}
              />
            </Col>
            <Col size={0.5} />
            <Col size={4.5}>
              <Button
                buttonStyle={{
                  backgroundColor: '#3b5998',
                  borderColor: '#3B5998',
                  borderWidth: 1,
                }}
                icon={
                  <Icon
                    name="facebook-f"
                    type="FontAwesome"
                    color="white"
                    size={25}
                    style={{ paddingRight: 6 }}
                  />
                }
                title="Facebook"
                type="clear"
                containerStyle={{ height: 50 }}
                titleStyle={{
                  paddingLeft: 5,
                  paddingRight: 20,
                  color: asitaColor.white,
                }}
                onPress={this._facebookTros}
              />
            </Col>
          </Grid>

          <Grid style={{ marginTop: 10, marginBottom: 0, flex: 0 }}>
            <Col
              size={4.5}
              style={{
                alignItems: 'flex-end',
                justifyContent: 'center',
                paddingTop: 5,
              }}
            >
              <View
                style={{
                  width: 50,
                  height: 1,
                  backgroundColor: asitaColor.brownGrey,
                }}
              />
            </Col>
            <Col
              size={1}
              style={{ alignItems: 'center', justifyContent: 'center' }}
            >
              <Text style={{ fontSize: 16, fontFamily: fontBold }}>or</Text>
            </Col>
            <Col
              size={4.5}
              style={{
                alignItems: 'flex-start',
                justifyContent: 'center',
                paddingTop: 5,
              }}
            >
              <View
                style={{
                  width: 50,
                  height: 1,
                  backgroundColor: asitaColor.brownGrey,
                }}
              />
            </Col>
          </Grid>

          <Grid style={{ marginTop: -5, flex: 0 }}>
            <Col>
              <View>
                <View>
                  <InputLogin
                    testID={'username'}
                    placeholder="Email address"
                    editable={true}
                    autoCapitalize="none"
                    onChangeText={data => this.handleInputEmail(data)}
                  />
                </View>
                <View>
                  {this.state.isValidEmail == false ? (
                    <View>
                      <Text
                        style={{
                          fontFamily: fontBold,
                          color: 'red',
                          fontSize: 12,
                          paddingLeft: 20,
                        }}
                      >
                        Please enter a valid email address
                      </Text>
                    </View>
                  ) : null}
                </View>
              </View>
              <View>
                <InputPassword
                  testID={'password'}
                  placeholder="Password"
                  onChangeText={data => this.setState({ password: data })}
                  showPassword={this.state.secureText}
                  onShowPassword={this._showPassword}
                />
              </View>
              <View style={{ alignSelf: 'center', marginTop: 10 }}>
                <TouchableOpacity>
                  <Text style={styles.textBody}>Forgot your Password?</Text>
                </TouchableOpacity>
              </View>
            </Col>
          </Grid>

          <Grid style={{ marginTop: 20, flex: 0 }}>
            <Col style={styles.col}>
              <TouchableOpacity
                testID={'pressSignIn'}
                onPress={this.doLogin}
                style={{
                  marginTop: 10,
                  backgroundColor: asitaColor.oceanBlue,
                  width: deviceWidth - 40,
                  borderRadius: 20,
                  padding: 10,
                  color: 'fff',
                  fontSize: 16,
                  alignItems: 'center',
                }}
              >
                {this.props.loading ? (
                  <DotIndicator
                    size={8}
                    style={{ flex: 1, padding: 2, marginTop: 10 }}
                    color={thameColors.white}
                  />
                ) : (
                  <Text
                    style={{
                      color: thameColors.white,
                      fontSize: 16,
                      fontFamily: fontExtraBold,
                    }}
                  >
                    LOG IN
                  </Text>
                )}
              </TouchableOpacity>
            </Col>
          </Grid>

          <Grid style={{ marginTop: 20, flex: 0 }}>
            <Col style={styles.col}>
              <View>
                <Text
                  style={[
                    styles.textBody,
                    { fontSize: 16, fontFamily: fontBold },
                  ]}
                >
                  New Member? Create your account.
                </Text>
              </View>

              <TouchableOpacity
                onPress={this.navigateSignUp}
                style={{ marginBottom: 25 }}
              >
                <View>
                  <Text style={styles.textRegister}>REGISTER</Text>
                </View>
              </TouchableOpacity>
            </Col>
          </Grid>
        </ScrollView>
      </View>
    ) : (
      // JIKA SUDAH LOGIN
      <MyAccount {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.login.fetchingLogin,
    loadingProfile: state.profile.loadingGet,
    loadingSocmed: state.socmed.fetching,
    dataLogin: makeDataLogin(state),
    isLogin: state.profile.isLogin,
  };
}

export default connect(mapStateToProps)(LoginPage);

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  col: { justifyContent: 'center', alignItems: 'center' },
  textBold: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontBold,
  },
  textBody: {
    fontSize: 14,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
  },
  textRegister: {
    fontSize: 16,
    color: asitaColor.orange,
    fontFamily: fontBold,
    paddingLeft: 10,
    paddingTop: 10,
  },
  image: {
    width: 30,
    height: 25,
    resizeMode: 'center',
    backgroundColor: 'white',
    marginRight: 15,
    marginLeft: 5,
  },
});
