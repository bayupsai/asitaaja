import React, { PureComponent } from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { Col, Grid } from 'react-native-easy-grid';

//redux
import { connect } from 'react-redux';

//Component
import HeaderPage from '../../../components/HeaderPage';
import SubHeader from '../../../components/SubHeaderPage';
import HeaderEditProfile from './Component/EditHeader';
import EditDetail from './Component/EditDetail';
import EditNumber from './Component/EditNumber';
import SosialMedia from './Component/SosialMedia';

class EditProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.stste = {};
  }

  _goBack = () => this.props.navigation.goBack();

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <HeaderPage callback={this._goBack} title="Edit Profile" />
          <SubHeader />
          <View style={{ marginTop: -65, marginBottom: 60 }}>
            <Grid style={{ marginBottom: 30 }}>
              <Col style={{ alignItems: 'center' }}>
                <HeaderEditProfile {...this.props} />
              </Col>
            </Grid>
            <Grid style={{ marginBottom: 30 }}>
              <Col style={{ alignItems: 'center' }}>
                <EditDetail {...this.props} />
              </Col>
            </Grid>
            <Grid style={{ marginBottom: 30 }}>
              <Col style={{ alignItems: 'center' }}>
                <EditNumber {...this.props} />
              </Col>
            </Grid>
            <Grid style={{ marginBottom: 30, marginBottom: 30 }}>
              <Col style={{ alignItems: 'center' }}>
                <SosialMedia {...this.props} />
              </Col>
            </Grid>
          </View>
        </ScrollView>
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    loading: state.profile.loadingGet,
  };
}

export default connect(stateToProps)(EditProfile);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#f0f0f0' },
});
