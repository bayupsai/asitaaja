import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Switch,
  Linking,
} from 'react-native';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { actionChangePasswordLogin } from '../../../redux/actions/ProfileAction';
//Component
import HeaderPage from '../../../components/HeaderPage';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { fontReguler, fontBold, thameColors } from '../../../base/constant';
import { InputPassword } from '../../../elements/TextInput';
import { ButtonRounded } from '../../../elements/Button';
import AlertModal from '../../../components/AlertModal';

let window = Dimensions.get('window');
let deviceHeight = window.height;
let deviceWidth = window.width;

class ChangePassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      login: false,
      loginText:
        'Send me a verification code every time I log in from a new device',
      visibleModal: null,
      errorMessage: '',

      //Password
      currentPassword: '',
      stateCurrentPassword: true,
      newPassword: '',
      stateNewPassword: true,
      confirmPassword: '',
      stateConfirmPassword: true,
    };
  }

  // Modal
  toggleSwitch = () => {
    this.setState({ login: !this.state.login });
  };
  resetModal = () => {
    this.setState({ visibleModal: null });
  };
  showModal = () => {
    this.setState({ visibleModal: 1 });
  };
  //Modal

  shareWA = () => {
    Linking.openURL(`whatsapp://send?text=Halo&phone=+6285319724999`);
  };

  handleBack = () => {
    this.props.navigation.goBack();
  };

  //Action Redux
  savePassword = () => {
    let { currentPassword, newPassword, confirmPassword } = this.state;
    let { dispatch, token } = this.props;
    let payload = {
      current_password: currentPassword,
      password: newPassword,
      confirmation_password: confirmPassword,
    };
    if (
      currentPassword !== '' &&
      newPassword !== '' &&
      confirmPassword !== ''
    ) {
      if (newPassword == confirmPassword) {
        if (currentPassword !== newPassword) {
          dispatch(actionChangePasswordLogin(token, payload))
            .then(res => {
              if (res.type == 'CHANGE_PASSWORD_LOGIN_SUCCESS') {
                this.setState({
                  currentPassword: '',
                  newPassword: '',
                  confirmPassword: '',
                  visibleModal: 99,
                  errorMessage: res.data.message,
                });
              } else {
                this.setState({ visibleModal: 404, errorMessage: res.data });
              }
            })
            .catch(err => {
              this.setState({ visibleModal: 404, errorMessage: err.message });
            });
        } else {
          this.setState({
            visibleModal: 404,
            errorMessage:
              'Your Current Password & New Password is same, please input different New Password',
          });
        }
      } else {
        this.setState({
          visibleModal: 404,
          errorMessage: 'New Password and Confirm Password are not match',
        });
      }
    } else {
      this.setState({
        visibleModal: 404,
        errorMessage: 'Please enter all Field',
      });
    }
  };
  //Action Redux

  render() {
    return (
      <View style={{ flex: 1 }} {...this.props}>
        <HeaderPage
          title="Password & Security"
          callback={this.handleBack}
          {...this.props}
        />
        <ScrollView style={{ backgroundColor: thameColors.backWhite }}>
          <View style={{ margin: 20 }}>
            <Grid style={styles.card}>
              <Row style={{ marginBottom: 8 }}>
                <Col size={7}>
                  <Text style={styles.textBold}>Password</Text>
                </Col>
                <Col size={3} style={styles.paddingIcon}>
                  {this.props.loadingChange ? (
                    <Text style={styles.textRegular}>Please wait...</Text>
                  ) : (
                    <TouchableOpacity onPress={this.showModal}>
                      <Text
                        style={[
                          styles.textRegular,
                          { color: thameColors.primary },
                        ]}
                      >
                        Change
                      </Text>
                    </TouchableOpacity>
                  )}
                </Col>
              </Row>
            </Grid>
            <Grid style={[styles.card, { marginTop: 0 }]}>
              <Row>
                <Col size={7}>
                  <Text style={styles.textBold}>
                    Enable log in verification
                  </Text>
                </Col>
                <Col size={3}>
                  <Switch
                    onValueChange={this.toggleSwitch}
                    value={this.state.login}
                  />
                </Col>
              </Row>
              <Row>
                <Col size={8}>
                  <Text style={styles.textRegular}>{this.state.loginText}</Text>
                </Col>
                <Col size={2} />
              </Row>
            </Grid>
          </View>
        </ScrollView>

        {/* =============================== Modal Change Password ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          style={styles.bottomModal}
          onBackButtonPress={this.resetModal}
          onBackdropPress={this.resetModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.buttonGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.superBack,
                      fontSize: 16,
                      fontFamily: fontBold,
                    }}
                  >
                    Change Password
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: thameColors.white }}>
                <ScrollView>
                  <Text style={[styles.textRegular, { margin: 10 }]}>
                    Enter the old password and the new password you want
                  </Text>
                  <InputPassword
                    placeholder="Current Password"
                    onChangeText={currentPassword =>
                      this.setState({ currentPassword })
                    }
                    showPassword={this.state.stateCurrentPassword}
                    onShowPassword={() =>
                      this.setState({
                        stateCurrentPassword: !this.state.stateCurrentPassword,
                      })
                    }
                  />
                  <InputPassword
                    placeholder="New Password"
                    onChangeText={newPassword => this.setState({ newPassword })}
                    showPassword={this.state.stateNewPassword}
                    onShowPassword={() =>
                      this.setState({
                        stateNewPassword: !this.state.stateNewPassword,
                      })
                    }
                  />
                  <InputPassword
                    placeholder="Confirm Password"
                    onChangeText={confirmPassword =>
                      this.setState({ confirmPassword })
                    }
                    showPassword={this.state.stateConfirmPassword}
                    onShowPassword={() =>
                      this.setState({
                        stateConfirmPassword: !this.state.stateConfirmPassword,
                      })
                    }
                  />
                </ScrollView>
              </Col>
            </Grid>
            <Grid style={{ flex: 0, paddingBottom: 10 }}>
              <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                <ButtonRounded
                  label="SAVE"
                  onClick={this.props.loadingChange ? '' : this.savePassword}
                />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== Modal Change Password ===================================== */}

        <AlertModal
          isVisible={this.state.visibleModal == 404}
          onDismiss={() => this.setState({ visibleModal: 1 })}
          onPress={() => this.setState({ visibleModal: 1 })}
          type="normal"
          title="Failed"
          contentText={this.state.errorMessage}
        />
        <AlertModal
          isVisible={this.state.visibleModal == 99}
          onDismiss={() => this.resetModal()}
          onPress={() => this.resetModal()}
          type="normal"
          title="Success"
          contentText={this.state.errorMessage}
        />
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    token: state.profile.token,
    loadingChange: state.profile.isLoadingChangePassword,
  };
}

export default connect(stateToProps)(ChangePassword);

const styles = StyleSheet.create({
  card: {
    marginVertical: 15,
    paddingLeft: 20,
    paddingTop: 15,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: thameColors.white,
    width: '100%',
    borderRadius: 5,
  },
  textBold: {
    fontSize: 16,
    color: thameColors.textBlack,
    alignItems: 'center',
    fontFamily: fontBold,
  },
  textRegular: { color: thameColors.textBlack, fontFamily: fontReguler },
  sectionHr: {
    width: 100 + '%',
    height: 0.3,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  paddingIcon: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15,
  },
  paddingLanguage: {
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  indonesia: {
    color: thameColors.secondary,
    fontFamily: fontReguler,
    borderRadius: 8,
    backgroundColor: thameColors.semiGray,
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  english: {
    color: thameColors.white,
    fontFamily: fontReguler,
    borderRadius: 8,
    backgroundColor: thameColors.secondary,
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 45,
    width: deviceWidth,
  },
  modalSearch: {
    flex: 0,
    height: 70,
    width: deviceWidth,
    marginTop: -35,
    marginBottom: 10,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    color: thameColors.white,
    fontFamily: fontBold,
    fontSize: 18,
  },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 1.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
