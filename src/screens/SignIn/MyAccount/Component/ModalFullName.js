import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { Avatar } from 'react-native-elements';
import Modal from 'react-native-modal';
import { fontBold, fontReguler } from '../../../../base/constant/index';
import { ButtonRounded } from '../../../../elements/Button';
import { InputText } from '../../../../elements/TextInput';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

const ModalFullName = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visible}
      style={styles.bottomModal}
    >
      <View style={styles.modalHeader}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#e2e2e2',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onClick={props.closeModal}
          >
            <Col style={{ alignItems: 'center' }}>
              <Text
                style={{ color: '#000', fontSize: 16, fontFamily: fontBold }}
              >
                Full Name
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid style={{ marginTop: 20 }}>
          <Col>
            <InputText placeholder="Full Name" />
          </Col>
        </Grid>
        <Grid style={{ flex: 0, paddingBottom: 10 }}>
          <Col>
            <Button label="SAVE" onClick={props.onClick} />
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalHeader: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

export default ModalFullName;
