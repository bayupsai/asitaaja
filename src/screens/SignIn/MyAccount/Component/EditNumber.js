import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  InteractionManager,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { DotIndicator } from 'react-native-indicators';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../base/constant/index';
import { styles } from './styles';
import { connect } from 'react-redux';
import {
  actionUpdateProfile,
  actionGetProfile,
} from '../../../../redux/actions/ProfileAction';

import Modal from 'react-native-modal';
import { ButtonRounded } from '../../../../elements/Button';
import { InputText } from '../../../../elements/TextInput';
import AlertModal from '../../../../components/AlertModal';
import { makeDataProfile } from '../../../../redux/selectors/ProfileSelector';

const window = Dimensions.get('window');

class EditNumber extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      visibleModal: null,
      errorMessage: '',
      mobileNoState: '',
      mobileNoPrefixState: '',
    };
  }
  componentDidMount() {
    let { mobileNo } = this.props.userData;
    mobileNo ? this.setState({ mobileNoState: mobileNo }) : null; // <-- Check if mobile number is exist
  }

  //modal
  toggleModal = modal => this.setState({ visibleModal: modal });

  setModal1 = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ visibleModal: 1 });
    });
  };

  resetModal = () => this.setState({ visibleModal: null });

  alertModal = (id, idOk, idNo, message) => {
    const { visibleModal } = this.state;
    return (
      <AlertModal
        type="normal"
        isVisible={visibleModal === id}
        title="Alert"
        contentText={message}
        onPress={() => this.toggleModal(idOk)}
        onDismiss={() => this.toggleModal(idNo)}
      />
    );
  };
  loadingConsole = () => '';
  //modal

  save = () => {
    let { fullname, email, photo } = this.props.userData;
    let { mobileNoState, mobileNoPrefixState } = this.state;
    let payload = {
      fullname: fullname,
      email: email,
      mobileNoPrefix: mobileNoPrefixState == '' ? '62' : mobileNoPrefixState,
      mobileNo: mobileNoState,
      photo: photo,
      countryID: 100,
      salutation: 'Mr',
      birthDate: '1992-01-02',
      address: 'Jakarta Pusat',
    };
    if (mobileNoState !== '') {
      this.props
        .dispatch(actionUpdateProfile(this.props.token, payload))
        .then(res => {
          if (res.type == 'UPDATE_PROFILE_SUCCESS') {
            this.props.dispatch(actionGetProfile(this.props.token));
            this.setState({ visibleModal: null });
          } else {
            this.setState({
              visibleModal: 404,
              errorMessage: this.props.userData,
            });
          }
        })
        .catch(err => {
          this.setState({ visibleModal: 404, errorMessage: err.message });
        });
    } else {
      this.setState({
        visibleModal: 404,
        errorMessage: 'Please enter Fullname filed to continue update',
      });
    }
  };

  //Main Render
  render() {
    let { mobileNoPrefix, mobileNo } = this.props.userData;
    let { loading } = this.props;
    return (
      <View style={styles.cardUser}>
        <Grid style={styles.gridUser}>
          <Row style={{ marginTop: 10, marginBottom: 10 }}>
            <Col size={5.5}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.textBlack,
                  fontSize: 20,
                }}
              >
                Mobile Number
              </Text>
              {/* <Text style={{ fontFamily: fontReguler, color: thameColors.textBlack }}>You may use up to 3 numbers</Text> */}
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.textBlack,
                }}
              >
                You may use up to 3 numbers
              </Text>
            </Col>
            <Col size={4.5} style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity>
                <Text
                  style={{
                    fontFamily: fontBold,
                    color: thameColors.oceanBlue,
                    fontSize: 16,
                    textAlign: 'right',
                  }}
                >
                  Add Mobile Number
                </Text>
              </TouchableOpacity>
            </Col>
          </Row>
          {!mobileNo ? (
            <Row>
              <Col>
                <Text
                  style={{ color: thameColors.red, fontFamily: fontReguler }}
                >
                  * Required Mobile Number
                </Text>
              </Col>
            </Row>
          ) : null}
          <Row style={{ marginBottom: 15, marginTop: 15 }}>
            <Col style={styles.sectionHr} />
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col size={7} onPress={this.setModal1}>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.textBlack,
                  fontSize: 16,
                }}
              >{`+${mobileNoPrefix} ${mobileNo}`}</Text>
              <Text style={{ fontFamily: fontReguler, fontStyle: 'italic' }}>
                Primary Mobile Number
              </Text>
            </Col>
          </Row>
        </Grid>

        {/* =============================== MODAL EDIT NAME ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={loading ? this.loadingConsole : this.resetModal}
          onBackdropPress={loading ? this.loadingConsole : this.resetModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalHeader}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                onPress={this.resetModal}
                style={{ flex: 1, flexDirection: 'row' }}
              >
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                  {loading ? (
                    <DotIndicator color={thameColors.secondary} />
                  ) : (
                    <Text
                      style={{
                        color: thameColors.superBack,
                        fontFamily: fontBold,
                        fontSize: 16,
                      }}
                    >
                      Mobile Number
                    </Text>
                  )}
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col>
                <Row style={{ marginLeft: 20, marginTop: 15 }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontReguler,
                    }}
                  >
                    Mobile number will be used to log in & reset password
                  </Text>
                </Row>
                <Row style={{ marginBottom: 30 }}>
                  {/* <Col size={3}>
                                        <InputText placeholder={mobileNoPrefix} onChangeText={(noPrefix) => this.setState({ mobileNoPrefixState: noPrefix })} />
                                    </Col> */}
                  <Col size={7}>
                    <InputText
                      placeholder={mobileNo}
                      value={mobileNo ? this.state.mobileNoState : ''}
                      maxLength={13}
                      keyboardType="phone-pad"
                      onChangeText={no => this.setState({ mobileNoState: no })}
                    />
                  </Col>
                </Row>
              </Col>
            </Grid>
            <Grid style={{ flex: 0 }}>
              <Col
                style={{
                  marginBottom: 25,
                  marginTop: 25,
                  marginRight: 5,
                  marginLeft: 20,
                }}
              >
                <ButtonRounded
                  label={loading ? 'loading...' : 'SAVE'}
                  onClick={loading ? this.loadingConsole : this.save}
                  children={
                    loading ? (
                      <DotIndicator color={thameColors.secondary} />
                    ) : null
                  }
                />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL EDIT NAME ===================================== */}

        {this.alertModal(404, 1, 1, this.state.errorMessage)}
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    userData: makeDataProfile(state),
    loading: state.profile.isLoading,
    token: state.profile.token,
  };
}
export default connect(stateToProps)(EditNumber);
