import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  InteractionManager,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { Avatar } from 'react-native-elements';
import Modal from 'react-native-modal';
import { DotIndicator } from 'react-native-indicators';
import ImagePicker from 'react-native-image-picker';
import AlertModal from '../../../../components/AlertModal';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../base/constant/index';
import { styles } from './styles';
import { ButtonRounded } from '../../../../elements/Button';
import { InputText } from '../../../../elements/TextInput';
import { connect } from 'react-redux';
import {
  actionUpdateProfile,
  actionGetProfile,
} from '../../../../redux/actions/ProfileAction';
import { makeDataProfile } from '../../../../redux/selectors/ProfileSelector';

const window = Dimensions.get('window');

class HeaderEditProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      visibleModal: null,
      fullnameState: '',
      photoState: null,
      extensions: '',
      errorMessage: '',
    };
  }
  componentDidMount() {
    let { fullname } = this.props.userData;
    fullname ? this.setState({ fullnameState: fullname }) : null; // <-- check if fullname is not empty
  }

  // Get Picture
  handleChoosePicture = () => {
    const options = {
      title: 'Select Avatar',
      cancelButtonTitle: 'Cancel',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, res => {
      const checkExt = res.fileName;
      if (res.didCancel) {
      } else if (res.error) {
      } else {
        if (checkExt.endsWith('.jpeg')) {
          const source = `data:image/jpeg;base64,${res.data}`;
          this.setState({ photoState: source }, () => this.handlePic());
        } else if (checkExt.endsWith('.jpg')) {
          const source = `data:image/jpg;base64,${res.data}`;
          this.setState({ photoState: source }, () => this.handlePic());
        } else if (checkExt.endsWith('.png')) {
          const source = `data:image/png;base64,${res.data}`;
          this.setState({ photoState: source }, () => this.handlePic());
        } else {
          this.setState({
            visibleModal: 99,
            errorMessage: 'Format File must be .jpg, .jpeg, and .png',
          });
        }
      }
    });
  };
  handlePic = () => {
    let { email, mobileNo, mobileNoPrefix, fullname } = this.props.userData;
    let { photoState } = this.state;
    let payload = {
      fullname: fullname,
      email: email,
      mobileNoPrefix: mobileNoPrefix,
      mobileNo: mobileNo,
      photo: photoState,
      countryID: 100,
      salutation: 'Mr',
      birthDate: '1992-01-02',
      address: 'Jakarta Pusat',
    };
    this.props
      .dispatch(actionUpdateProfile(this.props.token, payload))
      .then(res => {
        if (res.type == 'UPDATE_PROFILE_SUCCESS') {
          this.props.dispatch(actionGetProfile(this.props.token));
          this.setState({ visibleModal: null });
        } else {
          this.setState({
            visibleModal: 404,
            errorMessage: this.props.userData,
          });
        }
      })
      .catch(err => {
        this.setState({ visibleModal: 404, errorMessage: err });
      });
  };
  // Get Picture

  //modal
  toggleModal = modal => this.setState({ visibleModal: modal });

  setModal1 = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ visibleModal: 1 });
    });
  };

  resetModal = () => this.setState({ visibleModal: null });

  alertModal = (id, idOk, idNo, message) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visibleModal === id}
        title="Alert"
        contentText={message}
        onPress={() => this.toggleModal(idOk)}
        onDismiss={() => this.resetModal(idNo)}
      />
    );
  };
  loadingConsole = () => '';
  //modal

  save = () => {
    let { email, mobileNo, photo, mobileNoPrefix } = this.props.userData;
    let { fullnameState } = this.state;
    let payload = {
      fullname: fullnameState,
      email: email,
      mobileNoPrefix: mobileNoPrefix,
      mobileNo: mobileNo,
      photo: photo,
      countryID: 100,
      salutation: 'Mr',
      birthDate: '1992-01-02',
      address: 'Jakarta Pusat',
    };
    // alert(this.props.token)
    if (fullnameState !== '') {
      this.props
        .dispatch(actionUpdateProfile(this.props.token, payload))
        .then(res => {
          if (res.type == 'UPDATE_PROFILE_SUCCESS') {
            this.props.dispatch(actionGetProfile(this.props.token));
            this.setState({ visibleModal: null });
          } else {
            this.setState({
              visibleModal: 404,
              errorMessage: this.props.userData,
            });
          }
        })
        .catch(err => {
          this.setState({ visibleModal: 404, errorMessage: err.message });
        });
    } else {
      this.setState({
        visibleModal: 404,
        errorMessage: 'Please enter Fullname filed to continue update',
      });
    }
  };

  //Main Render
  render() {
    let { fullname, photo } = this.props.userData;
    let { loading } = this.props;
    return (
      <View style={styles.cardUser}>
        <Grid style={styles.gridUser}>
          <Col size={3} style={{ alignItems: 'flex-start' }}>
            <Avatar
              rounded
              size="large"
              source={
                photo == '' || photo == null
                  ? require('../../../../assets/icons/avatar.png')
                  : { uri: `${photo}` }
              }
              onPress={this.handleChoosePicture}
              showEditButton
            />
          </Col>
          <Col
            size={7}
            style={{ justifyContent: 'center', alignItems: 'center' }}
          >
            <Text style={{ fontFamily: fontReguler }}>
              Customize your profile picture
            </Text>
          </Col>
        </Grid>
        <Grid>
          <Col style={styles.sectionHr}></Col>
        </Grid>
        <Grid style={styles.gridName}>
          <Row style={{ marginTop: 10 }} onPress={this.setModal1}>
            <Col size={5}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.superBack,
                  fontSize: 20,
                }}
              >
                {' '}
                Full Name
              </Text>
            </Col>
            <Col size={5} style={{ alignItems: 'flex-end' }}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.oceanBlue,
                  fontSize: 16,
                }}
              >
                Change
              </Text>
            </Col>
          </Row>
          <Row style={{ marginBottom: 15, marginTop: 15 }}>
            <Col style={styles.sectionHr}></Col>
          </Row>
          <Row style={{ marginBottom: 20 }}>
            <Col>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.darkGray,
                  fontSize: 18,
                }}
              >
                {' '}
                {fullname ? fullname : 'Undefined'}
              </Text>
            </Col>
          </Row>
        </Grid>

        {/* =============================== MODAL EDIT NAME ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={loading ? this.loadingConsole : this.resetModal}
          onBackdropPress={loading ? this.loadingConsole : this.resetModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalHeader}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                onPress={this.resetModal}
                style={{ flex: 1, flexDirection: 'row' }}
              >
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                  {loading ? (
                    <DotIndicator color={thameColors.secondary} />
                  ) : (
                    <Text
                      style={{
                        color: thameColors.superBack,
                        fontFamily: fontBold,
                        fontSize: 16,
                      }}
                    >
                      Full Name
                    </Text>
                  )}
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col>
                <View style={{ marginLeft: 20, marginTop: 15 }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontReguler,
                    }}
                  >
                    The name you enter will be saved as contact
                  </Text>
                </View>
                <InputText
                  placeholder={fullname ? fullname : 'Full Name'}
                  value={fullname ? this.state.fullnameState : ''}
                  onChangeText={text => this.setState({ fullnameState: text })}
                />
              </Col>
            </Grid>
            <Grid style={{ flex: 0 }}>
              <Col
                style={{
                  marginBottom: 30,
                  marginTop: 20,
                  marginRight: 5,
                  marginLeft: 20,
                }}
              >
                <ButtonRounded
                  label={loading ? 'loading...' : 'SAVE'}
                  onClick={loading ? this.loadingConsole : this.save}
                  children={
                    loading ? (
                      <DotIndicator color={thameColors.secondary} />
                    ) : null
                  }
                />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL EDIT NAME ===================================== */}

        {this.alertModal(404, 1, 1, this.state.errorMessage)}
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    userData: makeDataProfile(state),
    loading: state.profile.isLoading,
    token: state.profile.token,
  };
}
export default connect(stateToProps)(HeaderEditProfile);
