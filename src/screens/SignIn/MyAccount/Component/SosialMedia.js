import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../base/constant/index';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { styles } from './styles';

class SosialMedia extends PureComponent {
  constructor(props) {
    super(props);
  }

  renderAccounts = () => {
    let { authType } = this.props;
    if (authType == 'google') {
      return (
        <Row style={{ margin: 10 }}>
          <Col
            size={1}
            style={{
              borderColor: thameColors.darkGray,
              borderWidth: 0.3,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: 5,
              padding: 5,
            }}
          >
            <Image
              style={{ width: 30, height: 30, resizeMode: 'center' }}
              source={require('../../../../assets/icons/icon_google.png')}
            />
          </Col>
          <Col size={7} style={{ marginLeft: 15 }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 18,
              }}
            >
              Google
            </Text>
            <Text
              style={{
                fontFamily: fontReguler,
                color: thameColors.darkGray,
                fontStyle: 'italic',
              }}
            >
              Logged in with Google
            </Text>
          </Col>
          <Col size={2}></Col>
        </Row>
      );
    } else if (authType == 'facebook') {
      return (
        <Row style={{ marginBottom: 20 }}>
          <Col
            size={1.8}
            style={{ justifyContent: 'center', alignItems: 'center' }}
          >
            <Icon
              name="facebook"
              type="entypo"
              color={thameColors.oceanBlue}
              size={38}
            />
          </Col>
          <Col size={6} style={{ justifyContent: 'center', marginLeft: 5 }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 18,
              }}
            >
              Facebook
            </Text>
            <Text
              style={{
                fontFamily: fontReguler,
                color: thameColors.darkGray,
                fontStyle: 'italic',
              }}
            >
              Logged in with Google
            </Text>
          </Col>
          {/* <Col size={2.2} style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => alert('In Development')}>
                            <Text style={{ fontFamily: fontBold, color: '#0066b3', fontSize: 16 }}>Connect</Text>
                        </TouchableOpacity>
                    </Col> */}
        </Row>
      );
    } else {
      return null;
    }
  };

  render() {
    return (
      <View style={styles.cardUser}>
        <Grid style={styles.gridUser}>
          <Col style={{ marginTop: 5, marginBottom: 5 }}>
            <View>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.textBlack,
                  fontSize: 20,
                }}
              >
                Linked Accounts
              </Text>
            </View>
            <View style={{ marginTop: 3 }}>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.textBlack,
                }}
              >
                For an easier login, connect your social accounts
              </Text>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.textBlack,
                }}
              >
                with Aeroaja.com
              </Text>
            </View>
          </Col>
        </Grid>
        <Grid>
          <Col style={styles.sectionHr}></Col>
        </Grid>
        <Grid style={styles.gridUser}>
          {this.renderAccounts()}
          {/* <Row style={{ marginBottom: 15, marginTop: 15 }}><Col style={styles.sectionHr} ></Col></Row> */}
        </Grid>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    authType: state.socmed.authType,
  };
}

export default connect(mapStateToProps)(SosialMedia);
