import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  InteractionManager,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { DotIndicator } from 'react-native-indicators';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../base/constant/index';
import { connect } from 'react-redux';
import {
  actionUpdateProfile,
  actionGetProfile,
} from '../../../../redux/actions/ProfileAction';
import { validateEmailFormat } from '../../../../utilities/helpers';
import { styles } from './styles';

import Modal from 'react-native-modal';
import AlertModal from '../../../../components/AlertModal';
import { ButtonRounded } from '../../../../elements/Button';
import { InputText } from '../../../../elements/TextInput';
import { makeDataProfile } from '../../../../redux/selectors/ProfileSelector';

const window = Dimensions.get('window');

class EditDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      visibleModal: null,
      emailState: '',
      isValidEmail: true,
      errorMessage: '',
    };
  }
  componentDidMount() {
    let { email } = this.props.userData;
    email ? this.setState({ emailState: email }) : null; // <-- Check if email is exists
  }

  //modal
  toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  setModal1 = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ visibleModal: 1 });
    });
  };

  resetModal = () => this.setState({ visibleModal: null });

  alertModal = (id, idOk, idNo, message) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visibleModal === id}
        title="Alert"
        contentText={message}
        onPress={() => this.toggleModal(idOk)}
        onDismiss={() => this.resetModal(idNo)}
      />
    );
  };
  loadingConsole = () => 'loading';
  //modal

  //handle email
  handleInputEmail = email => {
    let checkEmail = validateEmailFormat(email);
    if (checkEmail) {
      this.setState({ emailState: email, isValidEmail: true });
    } else {
      this.setState({ email: '', isValidEmail: false });
    }
  };

  //handle email
  save = () => {
    let { fullname, mobileNo, photo } = this.props.userData;
    let { emailState } = this.state;
    let payload = {
      fullname: fullname,
      email: emailState,
      mobileNoPrefix: '62',
      mobileNo: mobileNo,
      photo: photo,
      countryID: 100,
      salutation: 'Mr',
      birthDate: '1992-01-02',
      address: 'Jakarta Pusat',
    };
    if (emailState !== '') {
      this.props
        .dispatch(actionUpdateProfile(this.props.token, payload))
        .then(res => {
          if (res.type == 'UPDATE_PROFILE_SUCCESS') {
            this.props.dispatch(actionGetProfile(this.props.token));
            this.setState({ visibleModal: null });
          } else {
            this.setState({
              visibleModal: 404,
              errorMessage: this.props.userData,
            });
          }
        })
        .catch(err => {
          this.setState({ visibleModal: 404, errorMessage: err.message });
        });
    } else {
      this.setState({
        visibleModal: 404,
        errorMessage: 'Please enter Fullname filed to continue update',
      });
    }
  };

  render() {
    let { email } = this.props.userData;
    let { loading } = this.props;
    return (
      <View style={styles.cardUser}>
        <Grid style={styles.gridUser}>
          <Row style={{ marginTop: 10 }}>
            <Col size={7}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.textBlack,
                  fontSize: 20,
                }}
              >
                Email
              </Text>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.textBlack,
                }}
              >
                You may use to 2 emails
              </Text>
            </Col>
            <Col size={3} style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity>
                <Text
                  style={{
                    fontFamily: fontBold,
                    color: thameColors.oceanBlue,
                    fontSize: 16,
                    textAlign: 'right',
                  }}
                >
                  {' '}
                  Add Email{' '}
                </Text>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ marginBottom: 15, marginTop: 15 }}>
            <Col style={styles.sectionHr} />
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col size={7} onPress={this.setModal1}>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.textBlack,
                  fontSize: 18,
                }}
              >
                {email ? email : 'Undefined'}
              </Text>
              <Text style={{ fontFamily: fontReguler, fontStyle: 'italic' }}>
                Primary email address
              </Text>
            </Col>
            <Col size={3} style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity>
                <Image
                  source={require('../../../../assets/icons/icon-trash.png')}
                  style={{ width: 30, height: 25 }}
                  resizeMode="center"
                />
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>

        {/* =============================== MODAL EDIT EMAIL ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={loading ? this.loadingConsole : this.resetModal}
          onBackdropPress={loading ? this.loadingConsole : this.resetModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalHeader}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                onPress={this.resetModal}
                style={{ flex: 1, flexDirection: 'row' }}
              >
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                  {loading ? (
                    <DotIndicator color={thameColors.secondary} />
                  ) : (
                    <Text
                      style={{
                        color: thameColors.superBack,
                        fontFamily: fontBold,
                        fontSize: 16,
                      }}
                    >
                      Email
                    </Text>
                  )}
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col>
                <View style={{ marginLeft: 20, marginTop: 15 }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontReguler,
                    }}
                  >
                    Email will be used to log in & reset password
                  </Text>
                </View>
                <InputText
                  placeholder={email ? email : 'Email Address'}
                  onChangeText={text => this.handleInputEmail(text)}
                />
                <View>
                  {this.props.isValidEmail == false ? (
                    <View>
                      <Text
                        style={{
                          fontFamily: fontBold,
                          color: 'red',
                          fontSize: 12,
                          paddingLeft: 20,
                        }}
                      >
                        Please enter a valid email address
                      </Text>
                    </View>
                  ) : null}
                </View>
              </Col>
            </Grid>
            <Grid style={{ flex: 0 }}>
              <Col
                style={{
                  marginBottom: 30,
                  marginTop: 20,
                  marginRight: 5,
                  marginLeft: 20,
                }}
              >
                <ButtonRounded
                  label={loading ? 'loading...' : 'SAVE'}
                  onClick={loading ? this.loadingConsole : this.save}
                  children={
                    loading ? (
                      <DotIndicator color={thameColors.secondary} />
                    ) : null
                  }
                />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL EDIT EMAIL ===================================== */}

        {this.alertModal(404, 1, 1, this.state.errorMessage)}
      </View>
    );
  }
}
function stateToProps(state) {
  return {
    userData: makeDataProfile(state),
    loading: state.profile.isLoading,
    token: state.profile.token,
  };
}
export default connect(stateToProps)(EditDetails);
