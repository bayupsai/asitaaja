import { StyleSheet, Dimensions } from 'react-native';
import { thameColors } from '../../../../base/constant';

const window = Dimensions.get('window');

export const styles = StyleSheet.create({
  cardUser: {
    flex: 1,
    backgroundColor: thameColors.white,
    width: window.width / 1.11,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridUser: {
    paddingRight: 25,
    paddingLeft: 25,
    marginTop: 5,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gridName: {
    paddingRight: 25,
    paddingLeft: 25,
    paddingTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.backWhite,
    alignSelf: 'center',
  },
  // STYLE MODAL
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalHeader: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // STYLE MODAL
});
