import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  InteractionManager,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { fontBold, thameColors } from '../../../base/constant';
import { failedState } from '../../../redux/actions/LoginAction';
import { failedState as failedProfile } from '../../../redux/actions/ProfileAction';
import { failedState as failedOrder } from '../../../redux/actions/OrdersHistoryAction';
import { connect } from 'react-redux';
import AlertModal from '../../../components/AlertModal';
import { actionLogOutSocmed } from '../../../redux/actions/SocmedAction';
import { scale } from '../../../Const/ScaleUtils';

const window = Dimensions.get('window');

class CardUser extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: null,
      message: '',
    };
  }

  doLogout = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ modalVisible: 1 });
    });
  };

  // Navigation
  _navigateOrder = () => {
    const { navigation } = this.props;
    InteractionManager.runAfterInteractions(() => {
      navigation.navigate('Order');
    });
  };
  _navigateChangePassword = () => {
    const { navigation } = this.props;
    InteractionManager.runAfterInteractions(() => {
      navigation.navigate('ChangePassword');
    });
  };
  // Navigation

  //=========== Logout Socmed =============
  _logoutGoogle = async () => {
    // <- Logout Google
    let { dispatch } = this.props;
    dispatch(actionLogOutSocmed('google')); // <- Destroy data from redux
  };
  _logoutFacebook = async () => {
    // <- Logout Facebook
    const { dispatch } = this.props;
    dispatch(actionLogOutSocmed('facebook')); // <- Destroy data from redux
  };
  logOutSocmed = () => {
    let { authType } = this.props;
    if (authType == 'google') {
      this._logoutGoogle();
    } else if (authType == 'facebook') {
      this._logoutFacebook();
    }
  };
  //=========== Logout Socmed =============

  alert = id => {
    const { dispatch } = this.props;
    return (
      <AlertModal
        type="qna"
        isVisible={this.state.modalVisible === id}
        onPress={() => {
          this.logOutSocmed();
          dispatch(failedState('loginMail', ''));
          dispatch(failedProfile('getProfile', null, null, null));
          dispatch(failedOrder('ordersHistory', [], ''));
        }}
        onDismiss={() => this.setState({ modalVisible: null, message: '' })}
        title="My Account"
        titleColor={thameColors.superBack}
        contentText="Are you sure you want to log out?"
        labelOk="YES"
        labelCancel="NO"
      />
    );
  };

  //Main Render
  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.alert(1)}
        <View style={styles.cardUser}>
          <TouchableOpacity
            style={{ flexDirection: 'row', marginBottom: 10 }}
            onPress={this._navigateOrder}
          >
            <Image
              style={[styles.icon, { marginRight: scale(30) }]}
              source={require('../../../assets/icons/garuda-mall.png')}
            />
            <View style={styles.orderCard}>
              <Text style={styles.textBody}>My Orders</Text>
              <Icon
                name="navigate-next"
                type="materialicon"
                color={thameColors.oceanBlue}
                size={30}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ flexDirection: 'row' }}
            onPress={this._navigateChangePassword}
          >
            <Image
              style={[styles.icon, { marginRight: scale(30) }]}
              source={require('../../../assets/icons/lock.png')}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '85%',
              }}
            >
              <Text style={styles.textBody}>Password & Security</Text>
              <Icon
                name="navigate-next"
                type="materialicon"
                color={thameColors.oceanBlue}
                size={30}
              />
            </View>
          </TouchableOpacity>
          {/* <Grid style={styles.gridMenu}>
                        <Col size={2}>
                            <TouchableOpacity style={{ flex: 0 }} onPress={this.props.onPress}>
                                <Image style={styles.icon} source={require('../../../assets/icons/garuda-mall.png')} />
                            </TouchableOpacity>
                        </Col>
                        <Col size={7} style={{ justifyContent: 'center' }}>
                            <TouchableOpacity style={{ flex: 0 }} onPress={this.props.onPress}>
                                <Text style={styles.textBody}>My Orders</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col size={1} style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                            <TouchableOpacity style={{ flex: 0 }} onPress={this.props.onPress}>
                                <Icon name="navigate-next" type="materialicon" color={thameColors.oceanBlue} size={30} />
                            </TouchableOpacity>
                        </Col>
                    </Grid> */}
        </View>
        <TouchableOpacity onPress={this.doLogout} style={styles.cardUser}>
          <Grid style={styles.gridMenu}>
            <Col size={2}>
              <View style={{ flex: 0 }}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/icons/icon_logout.png')}
                />
              </View>
            </Col>
            <Col size={7} style={{ justifyContent: 'center' }}>
              <View style={{ flex: 0 }}>
                <Text style={styles.textBody}>Log Out</Text>
              </View>
            </Col>
          </Grid>
        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    authType: state.socmed.authType,
    tokenFB: state.socmed.tokenFacebook,
  };
}
export default connect(mapStateToProps)(CardUser);

const styles = StyleSheet.create({
  cardUser: {
    flex: 0,
    backgroundColor: thameColors.white,
    width: window.width / 1.11,
    borderRadius: 7,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  textBold: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontBold,
    paddingBottom: 5,
  },
  textBody: {
    fontSize: 16,
    color: thameColors.textBlack,
    alignItems: 'center',
    fontFamily: fontBold,
  },
  icon: {
    width: 30,
    height: 30,
    resizeMode: 'center',
    tintColor: thameColors.textBlack,
  },
  orderCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '85%',
    borderBottomWidth: 0.5,
    borderBottomColor: thameColors.gray,
    paddingBottom: 10,
  },
});
