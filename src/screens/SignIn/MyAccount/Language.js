import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Switch,
  Linking,
} from 'react-native';
import { Icon } from 'react-native-elements';
//Component
import HeaderPage from '../../../components/HeaderPage';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { fontReguler, fontBold, thameColors } from '../../../base/constant';

//resolution
const window = Dimensions.get('window');
let deviceHeight = window.height;

class Language extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      notification: false,
    };
  }

  toggleSwitch = () => {
    this.setState({ notification: !this.state.notification });
  };

  shareWA = () => {
    Linking.openURL(`whatsapp://send?text=Halo&phone=+6285319724999`);
  };

  handleBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={{ flex: 1 }} {...this.props}>
        <HeaderPage
          title="Settings"
          callback={this.handleBack}
          {...this.props}
        />
        <ScrollView style={{ backgroundColor: thameColors.backWhite }}>
          <View style={{ margin: 20 }}>
            {/* <Grid style={styles.card}>
                            <Col size={3}>
                                <Text style={styles.textBold}>Language</Text>
                            </Col>
                            <Col size={3.5}  style={{alignItems:'flex-end'}}>
                                <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={styles.indonesia}>Indonesia</Text>
                                </TouchableOpacity>
                            </Col>
                            <Col size={3.5} style={{alignItems:'flex-end'}}>
                                <TouchableOpacity style={styles.paddingLanguage}>
                                    <Text style={styles.english}>English</Text>
                                </TouchableOpacity>
                            </Col>
                        </Grid> */}
            <Grid style={styles.card}>
              <Row style={{ marginBottom: 8 }}>
                <Col size={9} style={{ justifyContent: 'center' }}>
                  <Text style={styles.textBold}>Notification</Text>
                </Col>
                <Col size={1} style={styles.paddingIcon}>
                  <Switch
                    onValueChange={this.toggleSwitch}
                    value={this.state.notification}
                  />
                </Col>
              </Row>
              <Row style={styles.sectionHr}></Row>
              <Row
                style={{ marginTop: 5, marginBottom: 5 }}
                onPress={() => this.props.navigation.navigate('About')}
              >
                <Col size={9}>
                  <Text style={styles.textBold}>About Us</Text>
                </Col>
                <Col size={1} style={styles.paddingIcon}>
                  <Icon
                    name="navigate-next"
                    type="materialicon"
                    color={thameColors.oceanBlue}
                    size={30}
                  />
                </Col>
              </Row>
            </Grid>
            {/* <Grid style={[styles.card, { height: deviceHeight / 6 }]}>
                            <Row style={{ marginBottom: 5 }} onPress={() => this.props.navigation.navigate("Terms")}>
                                <Col size={9}>
                                    <Text style={styles.textBold}>Terms & Conditions</Text>
                                </Col>
                                <Col size={1} style={styles.paddingIcon}>
                                    <Icon name="navigate-next" type="materialicon" color={thameColors.oceanBlue} size={30} />
                                </Col>
                            </Row>
                            <Row style={styles.sectionHr}></Row>
                            <Row style={{ marginTop: 5 }} onPress={() => this.props.navigation.navigate("Privacy")}>
                                <Col size={9}>
                                    <Text style={styles.textBold}>Privacy Policy</Text>
                                </Col>
                                <Col size={1} style={styles.paddingIcon}>
                                    <Icon name="navigate-next" type="materialicon" color={thameColors.oceanBlue} size={30} />
                                </Col>
                            </Row>
                        </Grid> */}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Language;

const styles = StyleSheet.create({
  card: {
    marginTop: 15,
    marginBottom: 15,
    paddingLeft: 20,
    paddingTop: 15,
    paddingBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: thameColors.white,
    width: '100%',
    borderRadius: 5,
  },
  textBold: {
    fontSize: 16,
    color: thameColors.textBlack,
    alignItems: 'center',
    fontFamily: fontBold,
  },
  sectionHr: {
    width: 100 + '%',
    height: 0.3,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  paddingIcon: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15,
  },
  paddingLanguage: {
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  indonesia: {
    color: thameColors.secondary,
    fontFamily: fontReguler,
    borderRadius: 8,
    backgroundColor: thameColors.semiGray,
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  english: {
    color: thameColors.white,
    fontFamily: fontReguler,
    borderRadius: 8,
    backgroundColor: thameColors.secondary,
    padding: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
});
