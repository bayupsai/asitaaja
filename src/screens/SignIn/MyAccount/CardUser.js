import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon, Avatar } from 'react-native-elements';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  thameColors,
} from '../../../base/constant';

const window = Dimensions.get('window');

const CardUser = props => (
  <View style={styles.cardUser}>
    <Grid style={styles.gridUser}>
      <Col size={3} style={{ alignItems: 'center' }}>
        {props.photo ? (
          <Avatar rounded size="large" source={{ uri: props.photo }} />
        ) : (
          <Avatar
            rounded
            size="large"
            source={require('../../../assets/icons/avatar.png')}
          />
        )}
      </Col>
      <Col size={7} style={{ paddingTop: 10, paddingLeft: 0 }}>
        <View style={{ paddingLeft: 7 }}>
          <Text style={styles.textBold}>{props.fullname}</Text>
        </View>
        <View style={{ paddingLeft: 7 }}>
          <Text style={styles.textAccount}>{props.email}</Text>
        </View>
      </Col>
    </Grid>
    <Grid style={styles.gridCard}>
      <Col size={2}>
        <Image
          style={styles.icon}
          source={require('../../../assets/icons/icon_card.png')}
        />
      </Col>
      <Col size={7}>
        <TouchableOpacity onPress={props.onPress}>
          <Text style={styles.textBody}>Account Setting</Text>
        </TouchableOpacity>
      </Col>
      <Col
        size={1}
        style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}
      >
        <TouchableOpacity onPress={props.onPress}>
          <Icon
            name="navigate-next"
            type="materialicon"
            color={thameColors.oceanBlue}
            size={30}
          />
        </TouchableOpacity>
      </Col>
    </Grid>
  </View>
);

export default CardUser;

const styles = StyleSheet.create({
  cardUser: {
    flex: 1,
    backgroundColor: thameColors.white,
    width: window.width / 1.11,
    height: window.height / 4,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageDefault: {
    width: 100,
    height: 90,
    resizeMode: 'center',
    backgroundColor: 'white',
    marginRight: 15,
    marginLeft: 5,
    paddingBottom: 10,
  },
  image: {
    width: 100,
    height: 90,
    resizeMode: 'center',
    backgroundColor: 'white',
    marginRight: 15,
    marginLeft: 5,
    paddingBottom: 10,
    borderRadius: 10,
  },
  gridUser: { paddingRight: 20, paddingLeft: 10, paddingTop: 15 },
  gridCard: {
    borderTopColor: thameColors.buttonGray,
    borderTopWidth: 0.5,
    paddingRight: 20,
    paddingLeft: 20,
    marginTop: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
  },
  textBold: {
    fontSize: 18,
    color: thameColors.superBack,
    fontFamily: fontExtraBold,
  },
  textBody: {
    fontSize: 16,
    color: thameColors.superBack,
    fontFamily: fontBold,
  },
  textAccount: {
    fontSize: 16,
    color: thameColors.darkGray,
    fontFamily: fontReguler,
  },
  icon: { width: 30, height: 30, resizeMode: 'center' },
});
