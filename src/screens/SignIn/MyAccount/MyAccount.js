import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  InteractionManager,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import {
  thameColors,
  fontRegulerItalic,
  asitaColor,
} from '../../../base/constant';

//redux
import { connect } from 'react-redux';

//IMPORT COMPONENT
import Heading from '../../../components/HeaderLogin';
import SubHeaderPage from '../../../components/SubHeaderPage';
import CardUser from './CardUser';
import CardMenu from './CardMenu';
import { makeDataProfile } from '../../../redux/selectors/ProfileSelector';

class MyAccount extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fullname: this.props.dataUser.fullname,
      email: this.props.dataUser.email,
      photo: this.props.dataUser.photo,
    };
  }

  _navigateEdit = () => {
    const { navigation } = this.props;
    InteractionManager.runAfterInteractions(() => {
      navigation.navigate('EditProfile');
    });
  };

  render() {
    let { fullname, email, photo } = this.props.dataUser;
    return (
      <View style={styles.container}>
        <Modal isVisible={this.props.loading}>
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <View
              style={{
                width: '95%',
                padding: 10,
                paddingVertical: 50,
                borderRadius: 5,
                backgroundColor: asitaColor.white,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text
                style={{
                  fontFamily: fontRegulerItalic,
                  color: asitaColor.superBack,
                  fontSize: 18,
                }}
              >
                Wait a moment....
              </Text>
            </View>
          </View>
        </Modal>

        <ScrollView>
          <Heading title="My Account" {...this.props} />
          <SubHeaderPage />
          <View style={{ marginTop: -65, marginBottom: 60 }}>
            <Grid style={{ marginBottom: 30 }}>
              <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                <CardUser
                  {...this.props}
                  onPress={this._navigateEdit}
                  photo={photo}
                  fullname={fullname}
                  email={email}
                />
              </Col>
            </Grid>
            <Grid>
              <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                <CardMenu {...this.props} />
              </Col>
            </Grid>
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataUser: makeDataProfile(state),
    loading: state.profile.loadingGet,
  };
}

export default connect(mapStateToProps)(MyAccount);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: asitaColor.backWhite },
  content: {
    paddingBottom: -10,
    alignItems: 'center',
    justifyContent: 'center',
    transform: [{ translateY: -160 }],
  },
  contentMenu: {
    flex: 1,
  },
});
