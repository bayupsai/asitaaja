import React from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';
import { verifyForgot } from '../../../redux/actions/ForgotAction';

class VerifyForgot extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      isVisible: null,
      message: '',
    };
  }
  _onModal = item => {
    this.setState({ isVisible: item });
  };
  _changeText = data => {
    this.setState({ data });
  };
  _onPress = () => {
    const { data } = this.state;
    let { dispatch, navigation, forgotData } = this.props;
    let payload = {
      data: forgotData.data,
      otp: data,
    };
    dispatch(verifyForgot(payload))
      .then(res => {
        if (res.type == 'VERIFY_FORGOT_SUCCESS') {
          navigation.navigate('ChangeForgot');
        } else {
          this.setState({ message: res.message, isVisible: 1 }); // <-Error 1
        }
      })
      .catch(err => {
        this.setState({ message: err, isVisible: 2 }); // <-Error 2
      });
  };

  //Main Render
  render() {
    const { container } = styles;
    const { isVisible, message } = this.state;
    return (
      <View style={container}>
        <Text>Enter the OTP Code to continue</Text>
        <TextInput
          autoCapitalize="characters"
          placeholder="Enter OTP Code from your email / mobile no"
          onChangeText={data => this._changeText(data)}
        />
        <Button title="SEND OTP CODE" onPress={this._onPress} />
        {/* ============================= Modal Alert ============================= */}
        <AlertModal
          type="normal"
          isVisible={isVisible === 1}
          onDismiss={() => this._onModal(null)}
          onPress={() => this._onModal(null)}
          title="Warning"
          contentText={message}
        />
        <AlertModal
          type="normal"
          isVisible={isVisible === 2}
          onDismiss={() => this._onModal(null)}
          onPress={() => this._onModal(null)}
          title="Warning"
          contentText={message}
        />
        {/* ============================= Modal Alert ============================= */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    forgotData: state.forgot.payloadForgot,
  };
}

export default connect(mapStateToProps)(VerifyForgot);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
