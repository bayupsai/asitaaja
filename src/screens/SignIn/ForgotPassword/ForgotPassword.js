import React from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';
import { forgot } from '../../../redux/actions/ForgotAction';
import AlertModal from '../../../components/AlertModal';

class ForgotPassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      isVisible: null,
      message: '',
    };
  }
  _onModal = item => {
    this.setState({ isVisible: item });
  };
  _changeText = data => {
    this.setState({ data });
  };
  _onPress = () => {
    const { data } = this.state;
    let { dispatch, navigation } = this.props;
    let payload = { data: data };
    dispatch(forgot(payload))
      .then(res => {
        if (res.type == 'FORGOT_PASSWORD_SUCCESS') {
          navigation.navigate('VerifyForgot');
        } else {
          this.setState({ message: res.message, isVisible: 1 }); // <-Error 1
        }
      })
      .catch(err => {
        this.setState({ message: err, isVisible: 2 }); // <-Error 2
      });
  };

  //Main Render
  render() {
    const { container } = styles;
    const { isVisible, message } = this.state;
    return (
      <View style={container}>
        <Text>Enter your Email / Mobile Number for verify your accounts</Text>
        <TextInput
          autoCapitalize="none"
          keyboardType="email-address"
          placeholder="Email / Mobile Number"
          onChangeText={data => this._changeText(data)}
        />
        <Button title="REQUEST" onPress={this._onPress} />

        {/* ============================= Modal Alert ============================= */}
        <AlertModal
          type="normal"
          isVisible={isVisible === 1}
          onDismiss={() => this._onModal(null)}
          onPress={() => this._onModal(null)}
          title="Warning"
          contentText={message}
        />
        <AlertModal
          type="normal"
          isVisible={isVisible === 2}
          onDismiss={() => this._onModal(null)}
          onPress={() => this._onModal(null)}
          title="Warning"
          contentText={message}
        />
        {/* ============================= Modal Alert ============================= */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    forgot: state.forgot,
  };
}

export default connect(mapStateToProps)(ForgotPassword);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
