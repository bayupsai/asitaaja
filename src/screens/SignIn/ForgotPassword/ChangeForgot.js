import React from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';
import { changeForgot } from '../../../redux/actions/ForgotAction';
import AlertModal from '../../../components/AlertModal';

class ChangeForgot extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      isVisible: null,
      message: '',
    };
  }
  _onModal = item => {
    this.setState({ isVisible: item });
  };
  _changeText = data => {
    this.setState({ data });
  };
  _onPress = () => {
    const { data } = this.state;
    let { dispatch, navigation, verifyData } = this.props;
    let payload = {
      data: verifyData.data,
      otp: verifyData.otp,
      password: data,
    };
    dispatch(changeForgot(payload))
      .then(res => {
        if (res.type == 'CHANGE_FORGOT_SUCCESS') {
          navigation.navigate('LoginPage');
        } else {
          this.setState({ message: res.message, isVisible: 1 }); // <-Error 1
        }
      })
      .catch(err => {
        this.setState({ message: err, isVisible: 2 }); // <-Error 2
      });
  };

  //Main Render
  render() {
    const { container } = styles;
    const { isVisible, message } = this.state;
    return (
      <View style={container}>
        <Text>Enter Your new password for your accounts</Text>
        <TextInput
          autoCapitalize="none"
          secureTextEntry={true}
          placeholder="Enter new password"
          onChangeText={data => this._changeText(data)}
        />
        <Button title="CHANGE" onPress={this._onPress} />
        {/* ============================= Modal Alert ============================= */}
        <AlertModal
          type="normal"
          isVisible={isVisible === 1}
          onDismiss={() => this._onModal(null)}
          onPress={() => this._onModal(null)}
          title="Warning"
          contentText={message}
        />
        <AlertModal
          type="normal"
          isVisible={isVisible === 2}
          onDismiss={() => this._onModal(null)}
          onPress={() => this._onModal(null)}
          title="Warning"
          contentText={message}
        />
        {/* ============================= Modal Alert ============================= */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    verifyData: state.forgot.payloadVerify,
  };
}

export default connect(mapStateToProps)(ChangeForgot);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
