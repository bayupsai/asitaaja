import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Dimensions,
  Modal,
  ScrollView,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import HeaderPage from '../../components/HeaderPage';
import { fontReguler, thameColors } from '../../base/constant';
import { verticalScale, scale } from '../../Const/ScaleUtils';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class MainMenu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: this.props.stateVisible,
    };
  }

  navigateTo = page => {
    const { navigation } = this.props;
    navigation.push(page);
  };

  openPage = page => {
    this.setState(this.props.stateVisible, () => {
      this.navigateTo(page);
    });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
          style={[
            styles.section,
            {
              width: '100%',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
          ]}
        >
          <Image
            source={require('../../assets/img/comming_soon.png')}
            style={{ width: '100%', height: '92.5%', marginTop: 25 }}
            resizeMode="contain"
          />
        </View>
        {/* <Grid style={{ width: deviceWidth }}>
                         <Row style={[styles.section, { marginTop: 10 }]}>
                              <Col style={styles.column}>
                                   <Image source={require('../../assets/img/comming_soon.png')} style={{ width: '100%', height: verticalScale(100) }} resizeMode={'contain'} />
                              </Col>
                         </Row>
                    </Grid> */}
        {/* <Grid>
                         <Row style={[styles.section, { marginTop: 10 }]}>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => alert('Bus')} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/icon_car_rent.png')}
                                                  style={styles.icons}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Bus</Text>
                                   </TouchableOpacity>
                              </Col>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => this.openPage('EMoney')} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/icon_emoney.png')}
                                                  style={styles.icons}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>E-Money</Text>
                                   </TouchableOpacity>
                              </Col>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => console.log("Coming Soon . . .")} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/holiday_icon.png')}
                                                  style={styles.icons40}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Holiday</Text>
                                   </TouchableOpacity>
                              </Col>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => console.log("Coming Soon . . .")} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/icon_umrah.png')}
                                                  style={styles.icons40}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Umrah</Text>
                                   </TouchableOpacity>
                              </Col>
                         </Row>

                         <Row style={[styles.section]}>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => this.openPage("Cargo")} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/icon_cargo.png')}
                                                  style={styles.icons}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Cargo</Text>
                                   </TouchableOpacity>
                              </Col>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => console.log("Coming Soon . . .")} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/rent_car_icon.png')}
                                                  style={styles.icons}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Car Rent</Text>
                                   </TouchableOpacity>
                              </Col>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => console.log("Coming Soon . . .")} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/icon_baggage.png')}
                                                  style={styles.icons40}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Baggage</Text>
                                   </TouchableOpacity>
                              </Col>
                              <Col style={styles.column}>
                                   <TouchableOpacity onPress={() => console.log("Coming Soon . . .")} style={{ alignItems: "center" }}>
                                        <View style={styles.menu}>
                                             <Image
                                                  source={require('../../assets/icons/icon_group.png')}
                                                  style={styles.icons40}
                                             />
                                        </View>
                                        <Text style={styles.titleMenu}>Group</Text>
                                   </TouchableOpacity>
                              </Col>
                         </Row>

                    </Grid> */}
      </View>
    );
  }
}

export default MainMenu;

const styles = StyleSheet.create({
  mt15: {
    marginTop: 15,
  },
  mb15: {
    marginBottom: 15,
  },
  pt20: {
    paddingTop: 20,
  },
  section: {
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#FFFFFF',
  },
  column: {
    alignItems: 'center',
  },
  menu: {
    backgroundColor: thameColors.secondary,
    borderRadius: 15,
    height: deviceWidth / 6,
    width: deviceWidth / 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icons: {
    resizeMode: 'center',
    height: deviceWidth / 9,
    width: deviceWidth / 9,
  },
  icons40: {
    resizeMode: 'center',
    height: deviceWidth / 10,
    width: deviceWidth / 10,
  },
  icons50: {
    resizeMode: 'center',
    height: 30,
    width: 30,
  },
  titleMenu: {
    color: '#534e4c',
    fontFamily: fontReguler,
    paddingTop: 5,
    fontSize: 14,
  },

  // === Style Modal
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 3.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // === Style Modal
});
