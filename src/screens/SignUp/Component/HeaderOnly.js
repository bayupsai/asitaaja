import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontReguler,
  fontRegulerItalic,
  thameColors,
  asitaColor,
} from '../../../base/constant';
import { statusHeight } from '../../../elements/BarStyle';

const BackButton = props => {
  return (
    <TouchableOpacity
      onPress={() => props.goBack()}
      style={{ paddingRight: 20 }}
    >
      <Icon
        name="ios-arrow-round-back"
        color={asitaColor.white}
        size={42}
        type="ionicon"
      />
    </TouchableOpacity>
  );
};

const TitlePage = props => {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Text
        style={{
          color: asitaColor.white,
          fontSize: 18,
          fontFamily: fontExtraBold,
        }}
      >
        {props.title}
      </Text>
    </View>
  );
};

const Menu = props => {
  return (
    <TouchableOpacity>
      <Icon name="dots-three-vertical" color={asitaColor.white} type="entypo" />
    </TouchableOpacity>
  );
};

const HeaderPage = props => (
  <Header
    leftComponent={<BackButton goBack={props.callback} />}
    centerComponent={<TitlePage title={props.title} />}
    // rightComponent={<Menu /> }
    containerStyle={{
      backgroundColor: asitaColor.marineBlue,
      paddingTop: 0,
      borderBottomWidth: 0,
      borderBottomColor: 'transparent',
      height: 50 + statusHeight,
    }}
  />
);

export default HeaderPage;
