import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Tabs from './Tabs';
import MobileTab from './MobileTab';
import EmailTab from './EmailTab';
import { asitaColor } from '../../../base/constant';

export default class ScreenTab extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Tabs>
          <MobileTab onPress={this.props.onPressMobile} />
          <EmailTab onPress={this.props.onPressEmail} />
        </Tabs>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: asitaColor.backWhite },
});
