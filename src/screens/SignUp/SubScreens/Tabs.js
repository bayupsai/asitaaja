import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  asitaColor,
} from '../../../base/constant';

export default class Tabs extends Component {
  state = {
    activeTab: 0,
    activeText: 0,
  };

  // Pull children out of props passed from App component
  render({ children } = this.props) {
    return (
      <View style={styles.container}>
        {/* Tabs row */}
        <View style={styles.tabsContainer}>
          {/* Pull props out of children, and pull title out of props */}
          {children.map(({ props: { title } }, index) => (
            <TouchableOpacity
              style={[
                // Default style for every tab
                styles.tabContainer,
                // Merge default style with styles.tabContainerActive for active tab
                index === this.state.activeTab ? styles.tabContainerActive : [],
              ]}
              // Change active tab
              onPress={() => this.setState({ activeTab: index })}
              // Required key prop for components generated returned by map iterator
              key={index}
            >
              <Text
                style={[
                  // Default style for every tab
                  styles.tabTextDefault,
                  // Merge default style with styles.tabContainerActive for active tab
                  index === this.state.activeTab ? styles.tabText : [],
                ]}
                // style={styles.tabText}
                // style={[
                //     // Default style for every text
                //     styles.textContainer,
                //     //  Merge default style with styles.tabtext for active text
                //     index === this.state.activeText ? styles.tabText : []
                //     ]}
                //     // Change active Text
                //     onChange={() => this.setState({activeText: index })}
                //     key={index}
              >
                {title}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        {/* Content */}
        <View style={styles.contentContainer}>
          {children[this.state.activeTab]}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  // Component container
  container: {
    flex: 1, // Take up all available space
  },
  // Tabs row container
  tabsContainer: {
    flexDirection: 'row',
  },
  // Individual tab container
  tabContainer: {
    flex: 1, // Take up equal amount of space for each tab
    paddingVertical: 15, // Vertical padding
    borderBottomWidth: 2, // Add thick border at the bottom
    borderBottomColor: 'rgba(0, 0, 0, 0.11)', // Transparent border for inactive tabs
  },
  // Active tab container
  tabContainerActive: {
    borderBottomColor: asitaColor.orange,
    borderBottomWidth: 2,
  },
  textContainer: {
    color: 'rgba(0, 0, 0, 0.11)',
    fontFamily: fontBold,
    textAlign: 'center',
  },
  // Tab text
  tabTextDefault: {
    color: asitaColor.superBlack,
    fontSize: 16,
    fontFamily: fontBold,
    textAlign: 'center',
  },

  tabText: {
    color: asitaColor.orange,
    fontSize: 16,
    fontFamily: fontExtraBold,
    textAlign: 'center',
  },
  // Content container
  contentContainer: {
    flex: 1,
  },
});
