import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Button, Icon } from 'react-native-elements';
import { DotIndicator } from 'react-native-indicators';
import { InputLogin } from '../../../elements/TextInput';
import { fontReguler } from '../../../base/constant';
import { styles } from './styles';

// export default class MobileTab extends React.PureComponent {
//     render() {
//         return
//     }
// }

const MobileTab = props => (
  <View style={styles.content} title={props.title}>
    <Grid style={{ flex: 0 }}>
      <Col size={3}>
        <InputLogin
          placeholder="+62"
          maxLength={4}
          keyboardType="numeric"
          onChangeText={props.changePrefix}
          style={{ textAlign: 'center' }}
        />
      </Col>
      <Col size={7}>
        <InputLogin
          placeholder="Mobile Number"
          keyboardType="numeric"
          onChangeText={props.onChangeText}
        />
      </Col>
    </Grid>
    <Grid style={{ flex: 0, marginTop: 20, marginRight: 40, marginLeft: 40 }}>
      <Col size={1} style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Icon type="ionicon" name="ios-lock" size={20} />
      </Col>
      <Col size={9} style={{ justifyContent: 'center' }}>
        <Text style={styles.textBody}>
          Your data will be protected and very safe.
        </Text>
      </Col>
    </Grid>
    <Grid style={{ flex: 0, marginLeft: 20, marginRight: 20, marginTop: 20 }}>
      <Col>
        {props.loading ? (
          <DotIndicator size={8} style={{ flex: 1 }} color="#FFFFFF" />
        ) : (
          <TouchableOpacity onPress={props.onPress} style={styles.button}>
            <Text style={styles.textRegister}>REGISTER</Text>
          </TouchableOpacity>
        )}
      </Col>
    </Grid>

    <Grid style={{ marginTop: 20, marginBottom: 0, flex: 0 }}>
      <Col
        size={3.5}
        style={{
          alignItems: 'flex-end',
          justifyContent: 'center',
          paddingTop: 5,
        }}
      >
        <View style={{ width: 50, height: 0.75, backgroundColor: '#828282' }} />
      </Col>
      <Col size={3} style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 14, fontFamily: fontReguler }}>
          or register with
        </Text>
      </Col>
      <Col
        size={3.5}
        style={{
          alignItems: 'flex-start',
          justifyContent: 'center',
          paddingTop: 5,
        }}
      >
        <View style={{ width: 50, height: 0.75, backgroundColor: '#828282' }} />
      </Col>
    </Grid>

    <Grid style={{ flex: 0, marginTop: 40, marginLeft: 20, marginRight: 20 }}>
      <Col size={4.5}>
        <Button
          buttonStyle={{ backgroundColor: '#fff' }}
          icon={
            <Image
              style={styles.image}
              source={require('../../../assets/icons/icon_google.png')}
            />
          }
          title="Google"
          type="outline"
          containerStyle={{ height: 50 }}
          onPress={props.google}
          titleStyle={{ color: '#222222', paddingRight: 30 }}
        />
      </Col>
      <Col size={0.5} />
      <Col size={4.5}>
        <Button
          buttonStyle={{ backgroundColor: '#3b5998' }}
          icon={
            <Image
              style={styles.image}
              source={require('../../../assets/icons/icon_facebook.png')}
            />
          }
          title="Facebook"
          containerStyle={{ height: 50 }}
          onPress={props.facebook}
          titleStyle={{ paddingLeft: 5, paddingRight: 20 }}
        />
      </Col>
    </Grid>
    <Grid style={{ marginBottom: 40 }}>
      <Col style={{ justifyContent: 'flex-end', alignContent: 'center' }}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text
            style={{
              textAlign: 'center',
              color: '#222222',
              fontFamily: 'NunitoSans-Regular',
            }}
          >
            By Registering, I agree to Garuda Mall{' '}
            <Text style={styles.textUnderline}>Terms &</Text>
          </Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text>
            <Text style={styles.textUnderline}>Conditions</Text>{' '}
            <Text style={styles.textNormal}>and</Text>{' '}
            <Text
              style={styles.textUnderline}
              onPress={() => alert('Privacy Policy')}
            >
              {' '}
              Privacy Policy
            </Text>
          </Text>
        </View>
      </Col>
    </Grid>
  </View>
);

export default MobileTab;
