import { StyleSheet, Dimensions } from 'react-native';
import {
  thameColors,
  fontReguler,
  fontExtraBold,
  fontBold,
  asitaColor,
} from '../../../base/constant';

const window = Dimensions.get('window');

export const styles = StyleSheet.create({
  content: {
    backgroundColor: thameColors.white,
    padding: 0,
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
  },
  image: {
    width: 30,
    height: 25,
    resizeMode: 'center',
    marginRight: 15,
    marginLeft: 5,
  },
  button: {
    width: window.width - 50,
    backgroundColor: asitaColor.orange,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  colIcon: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginTop: 10,
    marginBottom: 10,
  },
  textBody: {
    fontSize: 14,
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  textRegister: {
    color: thameColors.white,
    fontSize: 16,
    fontFamily: fontExtraBold,
  },
  textUnderline: {
    textDecorationLine: 'underline',
    color: asitaColor.marineBlue,
    fontFamily: fontBold,
  },
  textNormal: { color: thameColors.textBlack, fontFamily: fontReguler },
});
