import React, { PureComponent } from 'react';
import { View, Platform, InteractionManager } from 'react-native';
import { connect } from 'react-redux';
import {
  actionRegisterApply,
  actionMobileApply,
} from '../../redux/actions/RegisterAction';
import {
  actionGetToken,
  actionLogOutSocmed,
  actionLoginSocmed,
} from '../../redux/actions/SocmedAction';
import { actionGetProfile } from '../../redux/actions/ProfileAction';
import styles from './styles';
import HeaderOnly from './Component/HeaderOnly';
import Tabs from './SubScreens/Tabs';
import MobileTab from './SubScreens/MobileTab';
import EmailTab from './SubScreens/EmailTab';
import { validateEmailFormat } from '../../utilities/helpers';
import AlertModal from '../../components/AlertModal';
import { google, facebook } from 'react-native-simple-auth';
import {
  googleAndroid,
  googleIos,
  facebookConf,
} from '../../services/SignInProvider';

class SignUp extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isValidEmail: true,
      mobileNo: '',
      mobileNoPrefix: '',
      modalVisible: null,
      error: '',
    };
  }

  _goBack = () => this.props.navigation.goBack();

  changeEmail = o => {
    this.setState({ email: o });
  };
  changeMobile = (type, number) => {
    if (type == 'no') {
      this.setState({ mobileNo: number });
    } else if (type == 'prefix') {
      this.setState({ mobileNoPrefix: number });
    }
  };

  //handle email
  handleInputEmail = email => {
    let checkEmail = validateEmailFormat(email);
    if (checkEmail) {
      this.setState({ email: email, isValidEmail: true });
    } else {
      this.setState({ email: '', isValidEmail: false });
    }
  };
  goVerifyEmail = () => {
    let checkEmail = validateEmailFormat(this.state.email);
    InteractionManager.runAfterInteractions(() => {
      if (checkEmail == true || this.state.email !== '') {
        this.setState({ isValidEmail: true });
        let payloadData = {
          email: this.state.email,
        };
        this.props
          .dispatch(actionRegisterApply(payloadData))
          .then(res => {
            if (res.type == 'REGISTER_APPLY_SUCCESS') {
              this.props.navigation.navigate('SignUpVerify', {
                tab: 'email',
                data: res,
              });
            } else {
              this.setState({ modalVisible: 3, error: res.data.message });
            }
          })
          .catch(err => {
            this.setState({ modalVisible: 2, error: err.message });
          });
      } else {
        this.setState({ isValidEmail: false, modalVisible: 1 });
      }
    });
  };
  //handle email

  //handle mobile
  goVerifyMobile = () => {
    let payload = {
      mobileNoPrefix: '62',
      mobileNo: this.state.mobileNo,
    };
    InteractionManager.runAfterInteractions(() => {
      if (this.state.mobileNo !== '') {
        this.props
          .dispatch(actionMobileApply(payload))
          .then(res => {
            if (res.type == 'MOBILE_APPLY_SUCCESS') {
              this.props.navigation.navigate('SignUpVerify', {
                tab: 'mobile',
                data: res,
              });
            } else {
              this.setState({ modalVisible: 3, error: res.data });
              // alert(this.props.mobileApply.message)
            }
          })
          .catch(err => {
            this.setState({ modalVisible: 2, error: err });
            // this.errorAlert(err)
          });
      } else {
        this.setState({ modalVisible: 1 });
      }
    });
  };
  //handle mobile

  // ===================== Login Google =====================
  _googleTros = () => {
    let { dispatch } = this.props;
    InteractionManager.runAfterInteractions(() => {
      Platform.OS === 'ios'
        ? google(googleIos)
            .then(info => {
              dispatch(actionGetToken('google', info.credentials.id_token));
              this.actionGoogle(info.credentials.id_token);
            })
            .catch(err => {})
        : google(googleAndroid)
            .then(info => {
              dispatch(actionGetToken('google', info.credentials.id_token));
              this.actionGoogle(info.credentials.id_token);
            })
            .catch(err => {});
    });
  };

  actionGoogle = token => {
    let { dispatch, navigation } = this.props;
    let payloadData = {
      email: '',
      password: '',
      code: token,
      authType: 'google',
    };
    dispatch(actionLoginSocmed('google', payloadData)) // <- Handle login after get Google Token
      .then(res => {
        if (res.type == 'LOGIN_GOOGLE_SUCCESS') {
          let { access_token } = res.data;
          dispatch(actionGetProfile(access_token)) // <- Handle Get Profile data after Login aeroaja with google
            .then(res => {
              navigation.navigate('LoginPage');
            })
            .catch(err => {
              // this._logoutGoogle()
              this._setError(err, 2);
            });
        } else {
          // this._logoutGoogle()
          this._setError(res.message, 2);
        }
      })
      .catch(err => {
        // this._logoutGoogle()
        this._setError(err, 2);
      });
  };
  // ===================== Login Google =====================

  // ===================== Login Facebook =====================
  _facebookTros = () => {
    let { dispatch } = this.props;
    InteractionManager.runAfterInteractions(() => {
      facebook(facebookConf)
        .then(info => {
          dispatch(actionGetToken('facebook', info.credentials.access_token));
          this.actionFacebook(info.credentials.access_token);
        })
        .catch(err => {});
    });
  };

  actionFacebook = token => {
    let { dispatch, navigation } = this.props;
    let payloadData = {
      email: '',
      password: '',
      code: token,
      authType: 'facebook',
    };
    dispatch(actionLoginSocmed('facebook', payloadData)) // Handle Login using Token from Facebook
      .then(res => {
        if (res.type == 'LOGIN_FACEBOOK_SUCCESS') {
          dispatch(actionGetProfile(res.data.access_token)) // <- Handle Get Profile after Success Login Socmed Facebook
            .then(res => {
              navigation.navigate('LoginPage');
            })
            .catch(err => {
              dispatch(actionLogOutSocmed('facebook'));
              this._setError(err, 2);
            });
        } else {
          dispatch(actionLogOutSocmed('facebook'));
          this._setError(res.message, 2);
        }
      })
      .catch(err => {
        dispatch(actionLogOutSocmed('facebook'));
        this._setError(err, 2);
      });
  };

  // ===================== Login Facebook =====================

  resetModal = () => {
    this.setState({ modalVisible: null });
  };

  render() {
    let { modalVisible } = this.state;
    return (
      <View style={styles.container}>
        <HeaderOnly title="New Member" callback={this._goBack} />
        <Tabs>
          <MobileTab
            title="Mobile Number"
            {...this.props}
            onPress={this.goVerifyMobile}
            loading={this.props.isFetchingRegister}
            onChangeText={value => this.changeMobile('no', value)}
            changePrefix={value => this.changeMobile('prefix', value)}
            google={this._googleTros}
            facebook={this._facebookTros}
          />
          <EmailTab
            title="Email"
            isFetchingRegister={this.props.isFetchingRegister}
            isValidEmail={this.state.isValidEmail}
            email={text => this.handleInputEmail(text)}
            registerByEmail={this.goVerifyEmail}
            {...this.props}
            google={this._googleTros}
            facebook={this._facebookTros}
          />
        </Tabs>

        {/* ======================= Modal ===================== */}
        <AlertModal
          isVisible={modalVisible === 1}
          type="normal"
          title="Alert"
          contentText="Please enter all field"
          onPress={this.resetModal}
          onDismiss={this.resetModal}
        />
        <AlertModal
          isVisible={modalVisible === 2}
          type="normal"
          title="Alert"
          contentText={this.state.error}
          onPress={this.resetModal}
          onDismiss={this.resetModal}
        />
        {/* Email */}
        <AlertModal
          isVisible={modalVisible === 3}
          type="normal"
          title="Alert"
          contentText={this.state.error}
          onPress={this.resetModal}
          onDismiss={this.resetModal}
        />
        {/* ======================= Modal ===================== */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isFetchingRegister: state.register.fetchingRegister,
  };
}

export default connect(mapStateToProps)(SignUp);
