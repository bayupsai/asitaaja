import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon, Button } from 'react-native-elements';
import { DotIndicator } from 'react-native-indicators';
import { connect } from 'react-redux';
import {
  actionRegisterConfirm,
  actionMobileConfirm,
} from '../../../redux/actions/RegisterAction';
import styles from '../styles';
import HeaderOnly from '../Component/HeaderOnly';
import { InputText } from '../../../elements/TextInput';
import { fontReguler, fontBold, asitaColor } from '../../../base/constant';
import AlertModal from '../../../components/AlertModal';
import {
  makePayloadRegisterEmail,
  makePayloadRegisterMobile,
} from '../../../redux/selectors/RegisterSelector';

const window = Dimensions.get('window');

const opt_length = new Array(6).fill(0);

class SignUpVerify extends PureComponent {
  input = React.createRef();

  constructor(props) {
    super(props);

    this.state = {
      typeScreen: this.props.navigation.state.params.tab,
      // typeScreen: "mobile",
      image: [
        { id: 1, src: require('../../../assets/img/email-verify.png') },
        { id: 2, src: require('../../../assets/img/phone-verify.png') },
      ],
      confirmEmail: '',
      confirmMobile: '',
      modalVisible: null,
      error: '',
      focused: false,
    };
  }

  //Handle OTP
  handlePress = () => {
    this.input.current.focus();
  };
  handleFocus = () => {
    this.setState({ focused: true });
  };
  handleBlur = () => {
    this.setState({ focused: false });
  };
  handleChange = value => {
    if (this.state.typeScreen == 'mobile') {
      this.setState(state => {
        if (state.confirmMobile.length >= opt_length.length) return null;
        return {
          confirmMobile: (state.confirmMobile + value).slice(
            0,
            opt_length.length
          ),
        };
      });
    } else {
      this.setState(state => {
        if (state.confirmEmail.length >= opt_length.length) return null;
        return {
          confirmEmail: (state.confirmEmail + value).slice(
            0,
            opt_length.length
          ),
        };
      });
    }
  };
  handleKeyPres = e => {
    if (e.nativeEvent.key === 'Backspace') {
      if (this.state.typeScreen == 'mobile') {
        this.setState(state => {
          return {
            confirmMobile: state.confirmMobile.slice(
              0,
              state.confirmMobile.length - 1
            ),
          };
        });
      } else {
        this.setState(state => {
          return {
            confirmEmail: state.confirmEmail.slice(
              0,
              state.confirmEmail.length - 1
            ),
          };
        });
      }
    }
  };
  //

  _goBack = () => {
    this.props.navigation.goBack();
  };

  goForm = i => {
    this.props.navigation.navigate('SignUpForm', {
      tab: i,
    });
  };

  //modal
  resetModal = () => {
    this.setState({ modalVisible: null });
  };
  errorAlert = (err, id) => {
    return (
      <AlertModal
        isVisible={this.state.modalVisible === id}
        type="normal"
        onDismiss={this.resetModal}
        onPress={this.resetModal}
        title="Alert"
        contentText={err}
      />
    );
  };
  //modal

  onConfirm = type => {
    //mobile screen
    if (type == 'mobile') {
      let payloadData = {
        mobileNoPrefix: '62',
        mobileNo: this.props.dataMobile.mobileNo,
        otp: this.state.confirmMobile,
      };
      this.props
        .dispatch(actionMobileConfirm(payloadData))
        .then(res => {
          if (res.type == 'MOBILE_CONFIRM_SUCCESS') {
            this.props.navigation.navigate('SignUpForm', { tab: type });
          } else {
            this.setState({
              modalVisible: 404,
              error: 'The OTP Code is not valid, Please enter valid code',
            });
          }
        })
        .catch(err => {
          this.setState({ modalVisible: 404, error: err });
        });
      //email screen
    } else if (type == 'email') {
      let payloadData = {
        email: this.props.dataApply.email,
        otp: this.state.confirmEmail,
      };
      this.props
        .dispatch(actionRegisterConfirm(payloadData))
        .then(res => {
          if (res.type == 'REGISTER_CONFIRM_SUCCESS') {
            this.props.navigation.navigate('SignUpForm', { tab: type });
          } else {
            this.setState({
              modalVisible: 404,
              error: 'The OTP Code is not valid, Please enter valid code',
            });
          }
        })
        .catch(err => {
          this.setState({ modalVisible: 404, error: err });
        });
    }
  };

  // OnPress
  _sendCOde = () => {
    const { confirmMobile, confirmEmail, typeScreen } = this.state;
    confirmEmail !== '' || confirmMobile !== ''
      ? this.onConfirm(typeScreen)
      : this.setState({ error: 'Please enter the OTP Code', modalVisible: 3 });
  };
  // OnPress

  render() {
    const {
      confirmEmail,
      confirmMobile,
      typeScreen,
      image,
      error,
      focused,
    } = this.state;
    const values =
      typeScreen == 'mobile' ? confirmMobile.split('') : confirmEmail.split('');
    const selectedIndex =
      values.length < opt_length.length ? values.length : opt_length.length - 1;
    const hideInput = !(values.length < opt_length.length);
    return (
      <View style={styles.container}>
        {/* Alert */}
        {this.errorAlert(error, 404)}
        {/* Alert */}

        <HeaderOnly title="Verfication" callback={this._goBack} />
        <ScrollView>
          <Grid style={{ flex: 0, marginTop: 25 }}>
            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image
                source={typeScreen == 'mobile' ? image[1].src : image[0].src}
                style={{ width: window.width - 80, height: window.height / 3 }}
                resizeMode="center"
              />
            </Col>
          </Grid>
          <Grid style={{ flex: 0, marginTop: 30 }}>
            <Col
              style={[
                { justifyContent: 'center', alignItems: 'center' },
                styles.marginSide,
              ]}
            >
              <Text
                style={{
                  alignItems: 'center',
                  fontFamily: fontReguler,
                  fontSize: 16,
                  textAlign: 'center',
                }}
              >
                {typeScreen == 'mobile'
                  ? 'We have sent a verification code to your number. Please enter the code to verify your account.'
                  : 'We have sent verification to your email. Please enter the code to verify your account.'}
              </Text>
            </Col>
          </Grid>
          <Grid style={{ flex: 0, marginBottom: 20 }}>
            <Col>
              {/* Input Style like Tokopedia OTP */}
              {/* <TouchableWithoutFeedback onPress={this.handlePress}>
                <View style={styles.wrap}>
                  {
                    opt_length.map((item, index) => {
                      const selected = values.length === index
                      const filled = values.length === opt_length.length && index === opt_length.length - 1
                      const removeBorder = index === opt_length.length - 1 ? styles.noBorder : undefined
                      return (
                        <View style={[styles.display, removeBorder]} key={index}>
                          <Text style={styles.text}>{values[index] || ""}</Text>
                          {(selected || filled) && focused && <View style={styles.shadows} />}
                        </View>
                      )
                    })
                  }
                </View>
              </TouchableWithoutFeedback>
              <View style={styles.wrap}>
                <TextInput value="" ref={this.input} onFocus={this.handleFocus} onBlur={this.handleBlur}
                  onChangeText={this.handleChange} onKeyPress={this.handleKeyPres}
                  style={[styles.input, { left: selectedIndex * 32, opacity: hideInput ? 0 : 1 }]} />
              </View> */}
              <InputText
                onChangeText={i =>
                  typeScreen == 'mobile'
                    ? this.setState({ confirmMobile: i })
                    : this.setState({ confirmEmail: i })
                }
                keyboardType="default"
                autoCapitalize="characters"
                placeholder={
                  typeScreen == 'mobile'
                    ? 'Verification Mobile Number'
                    : 'Verification Email Address'
                }
                style={{ width: '80%' }}
              />
            </Col>
          </Grid>

          <Grid style={[{ flex: 0 }, styles.marginSide]}>
            <Col>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Icon
                  type="feather"
                  size={14}
                  name="refresh-cw"
                  color={asitaColor.superBlack}
                />
                <Text
                  style={{
                    marginLeft: 10,
                    fontFamily: fontBold,
                    color: asitaColor.black,
                    fontSize: 16,
                  }}
                >
                  Resend Verification Code?
                </Text>
              </TouchableOpacity>
            </Col>
          </Grid>
          <Grid style={[{ flex: 0, marginTop: 20 }, styles.marginSide]}>
            <Col>
              {this.props.loadingRegister ? (
                <DotIndicator
                  size={10}
                  style={{ flex: 1 }}
                  color={asitaColor.orange}
                />
              ) : (
                <Button
                  title="SEND CODE"
                  buttonStyle={{
                    borderRadius: 25,
                    backgroundColor: asitaColor.orange,
                  }}
                  onPress={this._sendCOde}
                />
              )}
            </Col>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataApply: makePayloadRegisterEmail(state),
    loadingRegister: state.register.fetchingRegister,
    //mobile
    dataMobile: makePayloadRegisterMobile(state),
  };
}

export default connect(mapStateToProps)(SignUpVerify);
