import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Button } from 'react-native-elements';
import { InputText, InputPassword } from '../../../../elements/TextInput';
import { fontReguler, fontBold } from '../../../../base/constant';
import { validateEmailFormat } from '../../../../utilities/helpers';

class Content extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      typeScreen: this.props.navigation.state.params.tab,
      validEmail: false,
      secureText1: true,
      secureText2: true,
    };
  }

  render() {
    let { typeScreen, validEmail } = this.state;
    return (
      <View style={styles.content}>
        <Grid style={{ flex: 0 }}>
          <Col>
            <Text style={{ textAlign: 'center', fontFamily: fontReguler }}>
              One more step! Enter your full name {'\n'}
              and password to securely log in to this account.
            </Text>
          </Col>
        </Grid>
        <Grid style={{ flex: 0 }}>
          <Col>
            <InputText
              placeholder="Name as ID card or passport"
              onChangeText={this.props.fullname}
            />
            <InputText
              placeholder={this.props.email}
              onChangeText={this.props.changeTextEmail}
              editable={typeScreen == 'email' ? false : true}
            />
            {validEmail == this.props.validationEmail ? (
              <View>
                <Text
                  style={{
                    fontFamily: fontBold,
                    color: 'red',
                    fontSize: 12,
                    paddingLeft: 20,
                  }}
                >
                  Please enter a valid email address
                </Text>
              </View>
            ) : null}
            <InputText
              placeholder={this.props.mobile}
              onChangeText={this.props.changeTextMobile}
              keyboardType="numeric"
              editable={typeScreen == 'mobile' ? false : true}
            />
            <InputText placeholder="Nationality" onChangeText={i => i} />
            <InputPassword
              placeholder="Password"
              onChangeText={this.props.password}
              showPassword={this.state.secureText1}
              onShowPassword={() =>
                this.setState({ secureText1: !this.state.secureText1 })
              }
            />
            <InputPassword
              placeholder="Confirm Password"
              onChangeText={this.props.confirmPassword}
              showPassword={this.state.secureText2}
              onShowPassword={() =>
                this.setState({ secureText2: !this.state.secureText2 })
              }
            />
            {this.props.alertPassword}
            {this.props.onPress}
          </Col>
        </Grid>
      </View>
    );
  }
}

export default Content;

const styles = StyleSheet.create({
  content: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 25,
  },
});
