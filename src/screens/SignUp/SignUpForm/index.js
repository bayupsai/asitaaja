import React, { PureComponent } from 'react';
import { View, ScrollView, InteractionManager } from 'react-native';
import { Button } from 'react-native-elements';
import { DotIndicator } from 'react-native-indicators';

//redux
import { connect } from 'react-redux';
import { actionRegisterNow } from '../../../redux/actions/RegisterAction';

//local component
import styles from '../styles';
import HeaderOnly from '../Component/HeaderOnly';
import Content from './Component/Content';
import { validateEmailFormat } from '../../../utilities/helpers';
import AlertModal from '../../../components/AlertModal';
import { asitaColor } from '../../../base/constant';
import {
  makePayloadRegisterEmail,
  makeRegisterNow,
  makePayloadRegisterMobile,
} from '../../../redux/selectors/RegisterSelector';

class SignUpForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fullname: '',
      email: '',
      mobile: '',
      password: '',
      confirmPassword: '',
      typeScreen: this.props.navigation.state.params.tab,
      isValidEmail: true,
      visible: null,
      err: '',
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  navigateLogin = () => {
    const { navigation } = this.props;
    InteractionManager.runAfterInteractions(() => {
      navigation.navigate('LoginPage');
    });
  };

  handleEmail = email => {
    let checkEmail = validateEmailFormat(email);
    if (checkEmail) {
      this.setState({ email, isValidEmail: true });
    } else {
      this.setState({ email, isValidEmail: false });
    }
  };

  //modal
  _toggleModal = modal => {
    this.setState({ visible: modal });
  };
  setNullModal = () => this.setState({ visible: null });

  _alertModal = (idModal, message) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visible === idModal}
        title="Failed"
        contentText={message}
        onPress={this.setNullModal}
        onDismiss={this.setNullModal}
      />
    );
  };
  //modal

  onRegisterEmail = () => {
    let payloadData = {
      fullname: this.state.fullname,
      passwd: this.state.password,
      mobileNoPrefix: '62',
      mobileNo:
        this.state.typeScreen == 'mobile'
          ? this.props.dataMobile.mobileNo
          : this.state.mobile,
      email:
        this.state.typeScreen == 'email'
          ? this.props.dataApply.email
          : this.state.email,
      countryID: 100,
    };
    this.props
      .dispatch(actionRegisterNow(payloadData))
      .then(res => {
        if (res.type == 'REGISTER_NOW_SUCCESS') {
          this.setState({ visible: 1 });
        } else {
          this.setState({ visible: 404, err: this.props.register });
        }
      })
      .catch(err => {
        this.setState({ visible: 404, err: err.message });
      });
  };

  render() {
    let { email, typeScreen, isValidEmail } = this.state;
    return (
      <View style={styles.container}>
        {/* Alert Modal */}
        <AlertModal
          type="normal"
          isVisible={this.state.visible === 1}
          title="Success"
          contentText="Your have successfully registered, Please login."
          onPress={this.navigateLogin}
          onDismiss={this.navigateLogin}
        />
        {this._alertModal(404, this.state.err)}
        {/* AlertModal */}

        <HeaderOnly title="New Member" callback={this.goBack} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Content
            {...this.props}
            fullname={i => this.setState({ fullname: i })}
            email={typeScreen == 'email' ? this.props.dataApply.email : 'Email'}
            validationEmail={isValidEmail}
            mobile={
              typeScreen == 'mobile'
                ? this.props.dataMobile.mobileNo
                : 'Mobile Number'
            }
            changeTextEmail={email => this.setState({ email })}
            changeTextMobile={mobile => this.setState({ mobile })}
            password={i => this.setState({ password: i })}
            confirmPassword={i => this.setState({ confirmPassword: i })}
            onPress={
              this.props.loading ? (
                <DotIndicator
                  size={10}
                  style={{ flex: 1 }}
                  color={asitaColor.orange}
                />
              ) : (
                <Button
                  title="SUBMIT"
                  buttonStyle={{
                    borderRadius: 25,
                    backgroundColor: asitaColor.orange,
                    marginTop: 20,
                    marginLeft: 20,
                    marginRight: 20,
                  }}
                  disabled={
                    this.state.fullname &&
                    this.state.email &&
                    this.state.password &&
                    this.state.confirmPassword == ''
                      ? true
                      : false
                  }
                  onPress={this.onRegisterEmail}
                />
              )
            }
          />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataApply: makePayloadRegisterEmail(state),
    register: makeRegisterNow(state),
    loading: state.register.fetchingRegister,
    //mobile
    dataMobile: makePayloadRegisterMobile(state),
  };
}

export default connect(mapStateToProps)(SignUpForm);
