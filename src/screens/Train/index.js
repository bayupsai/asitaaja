/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Image,
} from 'react-native';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { Grid, Row, Col } from 'react-native-easy-grid';
import MultiSwitch from 'rn-slider-switch';
import { SearchInput, InputTextRevamp } from '../../elements/TextInput';
import { Button, ButtonRounded } from '../../elements/Button';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Dates from 'react-native-dates';
import moment from 'moment';
//redux
import { connect } from 'react-redux';
import {
  actionStationSearch,
  actionSearchTrain,
  failedState,
} from '../../redux/actions/TrainAction';

//redux
import styles from './styles';
import { DotIndicator } from 'react-native-indicators';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../base/constant';

import dataStation from './dataStation';

var deviceWidth = Dimensions.get('window').width;

class Train extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      focus: 'startDate',
      startDate: null,
      endDate: null,
      flightType: 'single',
      return: false,
      visibleModal: null,
      seatClassSelected: 1,
      seatClassType: [
        { index: 1, title: 'Economy' },
        { index: 2, title: 'Business' },
        { index: 3, title: 'First Class' },
        { index: 4, title: 'Premium Economy' },
      ],
      destinationPopular: [
        { code: 'AKB', name: 'Aekloba' },
        { code: 'AWN', name: 'Arjawinangun' },
        { code: 'AW', name: 'Awipari' },
        { code: 'BBK', name: 'Babakan' },
        { code: 'BBT', name: 'Babat' },
        { code: 'BGR', name: 'Bagor' },
        { code: 'BJL', name: 'Bajalinggei' },
        { code: 'BAP', name: 'Bandarkalipah' },
        { code: 'BDT', name: 'Bandartinggi' },
        { code: 'BD', name: 'Bandung' },
        { code: 'BG', name: 'Bangil' },
        { code: 'BDW', name: 'Bangoduwa' },
        { code: 'BJR', name: 'Banjar' },
        { code: 'BJI', name: 'Banjarsari' },
        { code: 'BW', name: 'Banyuwangibaru' },
        { code: 'BAT', name: 'Barat' },
        { code: 'BRN', name: 'Baron' },
        { code: 'BTG', name: 'Batang' },
        { code: 'BTK', name: 'Batangkuis' },
        { code: 'BTT', name: 'Batu Tulis' },
        { code: 'BTA', name: 'Baturaja' },
        { code: 'BKS', name: 'Bekasi' },
        { code: 'BKI', name: 'Bekri' },
        { code: 'BIJ', name: 'Binjai' },
        { code: 'BBU', name: 'Blambanganumpu' },
        { code: 'BMG', name: 'Blimbing' },
        { code: 'BIB', name: 'Blimbingpendopo' },
        { code: 'BL', name: 'Blitar' },
        { code: 'BOO', name: 'Bogor' },
        { code: 'BJ', name: 'Bojonegoro' },
        { code: 'BJG', name: 'Bojong' },
        { code: 'BBN', name: 'Brambanan' },
        { code: 'BB', name: 'Brebes' },
        { code: 'BBG', name: 'Brumbung' },
        { code: 'BKA', name: 'Bulakamba' },
        { code: 'BMA', name: 'Bumiayu' },
        { code: 'BGM', name: 'Bungamas' },
        { code: 'CRB', name: 'Caruban' },
        { code: 'CU', name: 'Cepu' },
        { code: 'CI', name: 'Ciamis' },
        { code: 'CJ', name: 'Cianjur' },
        { code: 'CAW', name: 'Ciawi' },
        { code: 'CBD', name: 'Cibadak' },
        { code: 'CB', name: 'Cibatu' },
        { code: 'CBB', name: 'Cibeber' },
        { code: 'CCL', name: 'Cicalengka' },
        { code: 'CCR', name: 'Cicurug' },
        { code: 'CGB', name: 'Cigombong' },
        { code: 'CD', name: 'Cikadongdong' },
        { code: 'CKP', name: 'Cikampek' },
        { code: 'CP', name: 'Cilacap' },
        { code: 'CLK', name: 'Cilaku' },
        { code: 'CLD', name: 'Ciledug' },
        { code: 'CLH', name: 'Cilegeh' },
        { code: 'CLG', name: 'Cilegon' },
        { code: 'CPI', name: 'Cipari' },
        { code: 'CPD', name: 'Cipeundeuy' },
        { code: 'CRA', name: 'Cipunegara' },
        { code: 'CN', name: 'Cirebon' },
        { code: 'CNP', name: 'Cirebonprujakan' },
        { code: 'CRG', name: 'Cireungas' },
        { code: 'CSA', name: 'Cisaat' },
        { code: 'CO', name: 'Comal' },
        { code: 'DEN', name: 'Denpasar' },
        { code: 'DMR', name: 'Dolokmerangir' },
        { code: 'DPL', name: 'Doplang' },
        { code: 'GMR', name: 'Gambir' },
        { code: 'GDS', name: 'Gandasoli' },
        { code: 'GDM', name: 'Gandrungmangun' },
        { code: 'GRM', name: 'Garum' },
        { code: 'GDG', name: 'Gedangan' },
        { code: 'GDB', name: 'Gedebage' },
        { code: 'GG', name: 'Geneng' },
        { code: 'GHM', name: 'Giham' },
        { code: 'GLM', name: 'Glenmore' },
        { code: 'GB', name: 'Gombong' },
        { code: 'GM', name: 'Gumilir' },
        { code: 'GD', name: 'Gundih' },
        { code: 'GNM', name: 'Gunungmegang' },
        { code: 'HGL', name: 'Haurgeulis' },
        { code: 'HRP', name: 'Haurpugur' },
        { code: 'HL', name: 'Hengelo' },
        { code: 'IJ', name: 'Ijo' },
        { code: 'JAKK', name: 'Jakarta Kota' },
        { code: 'JTB', name: 'Jatibarang' },
        { code: 'JNG', name: 'Jatinegara' },
        { code: 'JTR', name: 'Jatiroto' },
        { code: 'JR', name: 'Jember' },
        { code: 'JN', name: 'Jenar' },
        { code: 'JRL', name: 'Jeruklegi' },
        { code: 'JG', name: 'Jombang' },
        { code: 'KAB', name: 'Kadokangabus' },
        { code: 'KBR', name: 'Kalibaru' },
        { code: 'KBD', name: 'Kalibodri' },
        { code: 'KLT', name: 'Kalisat' },
        { code: 'KSL', name: 'Kalisetail' },
        { code: 'KIT', name: 'Kalitidu' },
        { code: 'KLN', name: 'Kaliwungu' },
        { code: 'KE', name: 'Karang Tengah' },
        { code: 'KA', name: 'Karanganyar' },
        { code: 'KNE', name: 'Karangasem' },
        { code: 'KGT', name: 'Karangjati' },
        { code: 'KRR', name: 'Karangsari' },
        { code: 'KRW', name: 'Karangsuwung' },
        { code: 'KW', name: 'Karawang' },
        { code: 'KWG', name: 'Kawunganten' },
        { code: 'KBS', name: 'Kebasen' },
        { code: 'KRO', name: 'Kebonromo' },
        { code: 'KM', name: 'Kebumen' },
        { code: 'KD', name: 'Kediri' },
        { code: 'KDB', name: 'Kedungbanteng' },
        { code: 'KG', name: 'Kedunggalar' },
        { code: 'KEJ', name: 'Kedungjati' },
        { code: 'KJ', name: 'Kemranjen' },
        { code: 'KPN', name: 'Kepanjen' },
        { code: 'KPT', name: 'Kertapati' },
        { code: 'KTM', name: 'Kertasemaya' },
        { code: 'KTS', name: 'Kertosono' },
        { code: 'KSB', name: 'Kesamben' },
        { code: 'KGG', name: 'Ketanggungan' },
        { code: 'KGB', name: 'Ketanggungan Barat' },
        { code: 'KAC', name: 'Kiaracondong' },
        { code: 'KIS', name: 'Kisaran' },
        { code: 'KK', name: 'Klakah' },
        { code: 'KT', name: 'Klaten' },
        { code: 'KB', name: 'Kotabumi' },
        { code: 'KOP', name: 'Kotapadang' },
        { code: 'KRT', name: 'Kretek' },
        { code: 'KRN', name: 'Krian' },
        { code: 'KYA', name: 'Kroya' },
        { code: 'KTA', name: 'Kutoarjo' },
        { code: 'KWN', name: 'Kutowinangun' },
        { code: 'LT', name: 'Lahat' },
        { code: 'LMG', name: 'Lamongan' },
        { code: 'LP', name: 'Lampegan' },
        { code: 'LN', name: 'Langen' },
        { code: 'LRA', name: 'Larangan' },
        { code: 'LW', name: 'Lawang' },
        { code: 'LBJ', name: 'Lebakjero' },
        { code: 'LBG', name: 'Lebeng' },
        { code: 'LL', name: 'Leles' },
        { code: 'LPN', name: 'Lempuyangan' },
        { code: 'LMP', name: 'Limapuluh' },
        { code: 'LG', name: 'Linggapura' },
        { code: 'LOS', name: 'Losari' },
        { code: 'LLG', name: 'Lubuk Linggau' },
        { code: 'LBP', name: 'Lubukpakam' },
        { code: 'LWG', name: 'Luwung' },
        { code: 'MN', name: 'Madiun' },
        { code: 'ML', name: 'Malang' },
        { code: 'MLK', name: 'Malang Kotalama' },
        { code: 'MRI', name: 'Manggarai' },
        { code: 'MNJ', name: 'Manonjaya' },
        { code: 'MA', name: 'Maos' },
        { code: 'MP', name: 'Martapura' },
        { code: 'MSR', name: 'Masaran' },
        { code: 'MSG', name: 'Maseng' },
        { code: 'MDN', name: 'Medan' },
        { code: 'MLW', name: 'Meluwung' },
        { code: 'MBM', name: 'Membangmuda' },
        { code: 'MER', name: 'Merak' },
        { code: 'MBU', name: 'Merbau' },
        { code: 'MR', name: 'Mojokerto' },
        { code: 'ME', name: 'Muara Enim' },
        { code: 'MSL', name: 'Muarasaling' },
        { code: 'NG', name: 'Nagreg' },
        { code: 'NJ', name: 'Nganjuk' },
        { code: 'NBO', name: 'Ngrombo' },
        { code: 'NT', name: 'Ngunut' },
        { code: 'NTG', name: 'Notog' },
        { code: 'PDL', name: 'Padalarang' },
        { code: 'PHA', name: 'Padanghalaban' },
        { code: 'PME', name: 'Pamingke' },
        { code: 'PNW', name: 'Paninjawan' },
        { code: 'PPR', name: 'Papar' },
        { code: 'PA', name: 'Paron' },
        { code: 'PRK', name: 'Parungkuda' },
        { code: 'PRP', name: 'Parungpanjang' },
        { code: 'PSE', name: 'Pasar Senen' },
        { code: 'PAT', name: 'Patuguran' },
        { code: 'PGB', name: 'Pegadenbaru' },
        { code: 'PK', name: 'Pekalongan' },
        { code: 'PML', name: 'Pemalang' },
        { code: 'PBA', name: 'Perbaungan' },
        { code: 'PRA', name: 'Perlanaan' },
        { code: 'PTA', name: 'Petarukan' },
        { code: 'PLB', name: 'Plabuan' },
        { code: 'PLD', name: 'Plered' },
        { code: 'PBM', name: 'Prabumulih' },
        { code: 'PRB', name: 'Prembun' },
        { code: 'PB', name: 'Probolinggo' },
        { code: 'PPK', name: 'Prupuk' },
        { code: 'PUR', name: 'Puluraja' },
        { code: 'PWK', name: 'Purwakarta' },
        { code: 'PWT', name: 'Purwokerto' },
        { code: 'PWS', name: 'Purwosari' },
        { code: 'RJP', name: 'Rajapolah' },
        { code: 'RBP', name: 'Rambipuji' },
        { code: 'RPH', name: 'Rampah' },
        { code: 'RCK', name: 'Rancaekek' },
        { code: 'RBG', name: 'Randublatung' },
        { code: 'RK', name: 'Rangkasbitung' },
        { code: 'RAP', name: 'Rantau Prapat' },
        { code: 'RH', name: 'Rendeh' },
        { code: 'RGP', name: 'Rojogampi' },
        { code: 'SRD', name: 'Saradan' },
        { code: 'SNA', name: 'Saungnaga' },
        { code: 'SBJ', name: 'Sei Bejangkar' },
        { code: 'SMC', name: 'Semarangponcol' },
        { code: 'SMT', name: 'Semarangtawang' },
        { code: 'SMB', name: 'Sembung' },
        { code: 'STL', name: 'Sentolo' },
        { code: 'SPJ', name: 'Sepanjang' },
        { code: 'SG', name: 'Serang' },
        { code: 'SIR', name: 'Siantar' },
        { code: 'SDR', name: 'Sidareja' },
        { code: 'SDA', name: 'Sidoarjo' },
        { code: 'SKP', name: 'Sikampuh' },
        { code: 'SDU', name: 'Sindanglaut' },
        { code: 'SLW', name: 'Slawi' },
        { code: 'SLO', name: 'Solobalapan' },
        { code: 'SK', name: 'Solojebres' },
        { code: 'SGG', name: 'Songgom' },
        { code: 'SR', name: 'Sragen' },
        { code: 'SRI', name: 'Sragi' },
        { code: 'SWT', name: 'Srowot' },
        { code: 'SRW', name: 'Sruweng' },
        { code: 'SI', name: 'Sukabumi' },
        { code: 'SBP', name: 'Sumberpucung' },
        { code: 'SRJ', name: 'Sumberrejo' },
        { code: 'SWD', name: 'Sumberwadung' },
        { code: 'SPH', name: 'Sumpiuh' },
        { code: 'SGU', name: 'Surabaya Gubeng' },
        { code: 'SBI', name: 'Surabaya Pasarturi' },
        { code: 'TAL', name: 'Talun' },
        { code: 'TBK', name: 'Tambak' },
        { code: 'THB', name: 'Tanah Abang' },
        { code: 'TNG', name: 'Tangerang' },
        { code: 'TGL', name: 'Tanggul' },
        { code: 'TGN', name: 'Tanjung' },
        { code: 'TNK', name: 'Tanjung Karang' },
        { code: 'TPK', name: 'Tanjung Priuk' },
        { code: 'TNB', name: 'Tanjungbalai' },
        { code: 'TJS', name: 'Tanjungrasa' },
        { code: 'TSM', name: 'Tasikmalaya' },
        { code: 'TBI', name: 'Tebing Tinggi' },
        { code: 'TI', name: 'Tebingtinggi' },
        { code: 'TG', name: 'Tegal' },
        { code: 'TW', name: 'Telawa' },
        { code: 'TGR', name: 'Temuguruh' },
        { code: 'TIS', name: 'Terisi' },
        { code: 'TA', name: 'Tulungagung' },
        { code: 'TLY', name: 'Tulungbuyut' },
        { code: 'UJM', name: 'Ujanmas' },
        { code: 'UJN', name: 'Ujungnegoro' },
        { code: 'WK', name: 'Walikukun' },
        { code: 'WR', name: 'Waru' },
        { code: 'WDW', name: 'Waruduwur' },
        { code: 'WB', name: 'Warungbandrek' },
        { code: 'WT', name: 'Wates' },
        { code: 'WLR', name: 'Weleri' },
        { code: 'WIL', name: 'Wilangan' },
        { code: 'WG', name: 'Wlingi' },
        { code: 'WO', name: 'Wonokromo' },
        { code: 'WNS', name: 'Wonosari' },
        { code: 'YK', name: 'Yogyakarta' },
      ],
      destinationResult: [],
      origin: { code: 'GMR', name: 'Gambir' },
      destination: { code: 'BD', name: 'Bandung' },
      departureDate: moment(new Date())
        .add(1, 'days')
        .format('YYYY-MM-DD'),
      returnDate: moment()
        .add(2, 'days')
        .format('YYYY-MM-DD'),
      adult: 1,
      child: 0,
      infant: 0,
      seatClass: 'Economy',
      stationTrain: [],
    };
  }

  componentDidMount() {
    this.props.dispatch(failedState('searchTrain'));
    // this.fetchListOfStation();
  }

  rotateData = () => {
    const { origin, destination } = this.state;
    let data1 = origin;
    let data2 = destination;
    this.setState({ origin: data2, destination: data1 });
  };

  // GET DATA STATION LIST
  fetchListOfStation = () => {
    this.props
      .dispatch(actionStationSearch())
      .then(res => {
        console.log(res);
        this.setState({ stationTrain: res.data });
      })
      .catch(err => {
        console.log(err);
        console.log('Error at XXXX');
      });
  };
  // GET DATA STATION LIST

  // ========================= SET DATA
  setOriginDestination = (modalType, data) => {
    if (modalType === 1) {
      // departure
      this.setState({
        visibleModal: null,
        origin: data,
        destinationResult: [],
      });
    } else if (modalType === 2) {
      // destination
      this.setState({
        visibleModal: null,
        destination: data,
        destinationResult: [],
      });
    }
  };
  setPassengerData = (passengerType, value) => {
    console.log(passengerType);
    console.log(value);
    if (passengerType === 'adult') {
      if (value > 0) {
        this.setState({ adult: value });
      }
    } else if (passengerType === 'infant') {
      if (value === '-') {
        this.setState({ infant: 0 });
      } else {
        this.setState({ infant: value });
      }
    }
  };

  setSeatClass = seatClass => {
    this.setState({
      seatClassSelected: seatClass.index,
      seatClass: seatClass.title,
      visibleModal: null,
    });
  };

  setDepartureDate = ({ date }) => {
    if (this.state.departureDate === this.state.returnDate) {
      this.setState({
        departureDate: moment(date).format('YYYY-MM-DD'),
        returnDate: moment(date)
          .add('1', 'days')
          .format('YYYY-MM-DD'),
        visibleModal: null,
        focus: date,
      });
    } else if (this.state.returnDate >= this.state.departureDate) {
      this.setState({
        departureDate: moment(date).format('YYYY-MM-DD'),
        returnDate: moment(date)
          .add('1', 'days')
          .format('YYYY-MM-DD'),
        visibleModal: null,
        focus: date,
      });
    } else {
      this.setState({
        departureDate: moment(date).format('YYYY-MM-DD'),
        visibleModal: null,
        focus: date,
      });
    }
  };

  setReturnDate = ({ date }) => {
    if (this.state.returnDate === moment(new Dates()).format('YYYY-MM-DD')) {
      alert('Could Not Set Return Date');
    } else if (this.state.returnDate === this.state.departureDate) {
      this.setState({
        returnDate: moment(date).format('YYYY-MM-DD'),
        departureDate: moment(date)
          .add('-1', 'days')
          .format('YYYY-MM-DD'),
        visibleModal: null,
      });
    } else if (date === this.state.departureDate) {
      this.setState({
        returnDate: moment(date).format('YYYY-MM-DD'),
        departureDate: moment(date)
          .add('-1', 'days')
          .format('YYYY-MM-DD'),
        visibleModal: null,
      });
    } else {
      this.setState({
        returnDate: moment(date).format('YYYY-MM-DD'),
        visibleModal: null,
      });
    }
  };
  //  ==================== SET DATA

  // ==================== GLOBAL FUNC
  // searchTrain = () => {
  //     this.props.navigation.navigate('SearchTrain')
  // }

  resetModal = () => {
    this.setState({
      visibleModal: null,
      departureResult: [],
      destinationResult: [],
    });
  };

  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  // ========================== GLOBAL FUNC

  // ============================= SEARCHING AUTOCOMPLETE
  searchingDeparture = val => {
    const { visibleModal } = this.state;
    const type =
      visibleModal === 1
        ? this.props.dataStation.destination
        : this.props.dataStation.origin;
    if (val !== '') {
      this.setState(
        {
          destinationResult: [],
        },
        () => {
          let data = this.filterValuePart(type, val);
          console.log(data);
          this.setState({
            destinationResult: data,
          });
        }
      );
    } else {
      this.setState({
        destinationResult: [],
      });
    }
  };

  filterValuePart = (arr, part) => {
    return arr.filter(function(item) {
      const itemData = item.code.toUpperCase() + ' ' + item.name.toUpperCase();
      const textData = part.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
  };
  // // ===================================== SEARCHING AUTOCOMPLETE

  searchTrain = () => {
    // this.setState({ visibleModal: 99 })
    let payloadData = {
      org: this.state.origin.code,
      des: this.state.destination.code,
      departure_date: moment(this.state.departureDate).format('DD/MM/YYYY'),
      adult_quantity: this.state.adult,
      infant_quantity: this.state.infant,
    };
    // let payloadData = {
    //   command: 'SCHEDULE',
    //   product: 'KERETA',
    //   data: {
    //     departure_code: this.state.origin.code,
    //     arrival_code: this.state.destination.code,
    //     date: this.state.departureDate,
    //     adult: this.state.adult,
    //     class: ''
    //   }
    // };

    // if (this.state.flightType == 'return') {
    //   payloadData.data.return_date = this.state.returnDate;
    // }

    // if (this.state.infant > 0) {
    //   payloadData.data.infant = this.state.infant;
    // }

    this.props.dispatch(actionSearchTrain(payloadData));
    this.props.navigation.navigate('SearchTrain', {
      origin: this.state.origin,
      arrival: this.state.destination,
      return: this.state.return,
      payloadData: payloadData,
      type: 'train',
    });

    // .then((res) => {
    //     this.setState({ visibleModal: null })
    //     console.log(res)
    //     if (res.type == "SEARCH_TRAIN_SUCCESS") {
    //         this.props.navigation.navigate('SearchTrain', {
    //             origin: this.state.origin,
    //             arrival: this.state.destination,
    //             return: this.state.return
    //         })
    //     } else {
    //         Alert.alert(
    //             'Flight',
    //             'Flight not available, please choose another date.',
    //             [
    //                 { text: 'Cancel', onPress: () => console.log('OK Pressed') },
    //                 { text: 'Try Again', onPress: () => console.log('OK Pressed') },
    //             ],
    //             { cancelable: false },
    //         );
    //     }
    // })
  };

  loadingBooking = () => (
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#FFFFFF',
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        height: 'auto',
        flex: 0,
      }}
    >
      {/* <Col size={3} style={{ alignItems: "center", justifyContent: "center", paddingLeft: 0 }}>
                <Image source={require('../../assets/icons/icon_trains.png')} style={{ width: 75, height: 75 }} resizeMode="center" />
            </Col> */}
      <Col style={{ alignItems: 'center' }}>
        <Row style={{ flex: 0 }}>
          <Text
            style={{ fontFamily: fontExtraBold, fontSize: 16, color: '#000' }}
          >
            Please Wait.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5 }}>
          <Text
            style={{ fontFamily: fontReguler, fontSize: 16, color: '#000' }}
          >
            We still process your request.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5, alignItems: 'center' }}>
          <DotIndicator
            size={10}
            style={{ flex: 0, backgroundColor: '#FFFFFF' }}
            color={thameColors.secondary}
          />
        </Row>
      </Col>
    </Grid>
  );

  _keyExtractor = item => item.code;

  // Main Render
  render() {
    let passengerAdult = this.state.adult + ' Adult';
    let passengerInfant =
      this.state.infant > 0 ? ', ' + this.state.infant + ' Infant' : '';
    const passengerData = passengerAdult + passengerInfant;
    const isDateBlocked = date => date.isBefore(moment(), 'day');
    const mytextorigin = this.state.origin.name;
    const maytextdestination = this.state.destination.name;
    const maxlimit = 13;

    return (
      <View style={{ backgroundColor: '#f4f4f4', flex: 1 }}>
        <ScrollView style={{ marginBottom: 15 }}>
          {/* HEADER COMPONENT */}
          <HeaderPage
            title="Search Train"
            callback={() => this.goBackHeader()}
          />
          <SubHeaderPage />
          {/* HEADER COMPONENT */}

          {/* SWITCH ONE WAY OR RETURN */}
          <Grid style={styles.sectionToggle}>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              <MultiSwitch
                currentStatus={'Open'}
                disableScroll={() => {}}
                onStatusChanged={text => {
                  if (text === 'Single') {
                    this.setState({ flightType: 'single', return: false });
                  } else if (text === 'Return') {
                    this.setState({ flightType: 'return', return: true });
                  }
                }}
              />
            </Col>
          </Grid>
          {/* SWITCH ONE WAY OR RETURN */}

          {/* LIST FORM */}
          <Grid style={[styles.card, { paddingTop: 10 }]}>
            <Col
              size={4}
              onPress={() => this._toggleModal(1)}
              style={{ justifyContent: 'center', alignItems: 'flex-start' }}
            >
              <View style={{ paddingBottom: 10 }}>
                <Text style={styles.textLabel}>From</Text>
              </View>
              <View onChangeText={data => this.handleCHange(data)}>
                <Text style={styles.textBold}>
                  {mytextorigin.length > maxlimit
                    ? mytextorigin.substring(0, maxlimit - 3) + '...'
                    : mytextorigin}
                </Text>
              </View>
              <View>
                <Text style={styles.textInitial}>
                  ({this.state.origin.code})
                </Text>
              </View>
            </Col>
            <Col
              size={2}
              style={{ justifyContent: 'center', alignItems: 'center' }}
            >
              <TouchableOpacity onPress={this.rotateData}>
                <View>
                  <Image
                    source={require('../../assets/icons/icon_trains.png')}
                    style={{
                      width: 20,
                      height: 25,
                      marginBottom: -13,
                      marginLeft: 12,
                      tintColor: thameColors.secondary,
                    }}
                    resizeMode="center"
                  />
                </View>
                <View>
                  <Image
                    source={require('../../assets/icons/flight_return.png')}
                    style={{
                      width: 45,
                      height: 40,
                      marginTop: -20,
                      tintColor: thameColors.secondary,
                    }}
                    resizeMode="contain"
                  />
                </View>
              </TouchableOpacity>
            </Col>
            <Col
              size={4}
              onPress={() => this._toggleModal(2)}
              style={{ justifyContent: 'center', alignItems: 'flex-end' }}
            >
              <View style={{ paddingBottom: 10 }}>
                <Text style={styles.textLabel}>To</Text>
              </View>
              <View>
                <Text style={styles.textBold}>
                  {maytextdestination.length > maxlimit
                    ? maytextdestination.substring(0, maxlimit - 3) + '...'
                    : maytextdestination}
                </Text>
              </View>
              <View>
                <Text style={styles.textInitial}>
                  ({this.state.destination.code})
                </Text>
              </View>
            </Col>
          </Grid>
          <Grid style={styles.sectionInput}>
            <Col>
              <InputTextRevamp
                label="Departure Date"
                source={require('../../assets/icons/icon_departure.png')}
                editable={moment(this.state.departureDate).format(
                  'DD MMM YYYY'
                )}
                onPress={() => this._toggleModal(3)}
              />
            </Col>
          </Grid>
          {this.state.return === true ? (
            <Grid style={styles.sectionInput}>
              <Col>
                <InputTextRevamp
                  label="Return Date"
                  source={require('../../assets/icons/icon_return.png')}
                  editable={moment(this.state.returnDate).format('DD MMM YYYY')}
                  onPress={() => this._toggleModal(4)}
                />
              </Col>
            </Grid>
          ) : null}
          <Grid style={styles.sectionInput}>
            <Col>
              <InputTextRevamp
                label="Passengers (Adult or Infant)"
                source={require('../../assets/icons/icon_total_passenger.png')}
                editable={passengerData}
                onPress={() => this._toggleModal(5)}
              />
            </Col>
          </Grid>
          {/* LIST FORM */}
          {/* BUTTON */}
          <Grid style={styles.sectionButtonArea}>
            <View style={styles.sectionButton}>
              <ButtonRounded label="SEARCH TRAINS" onClick={this.searchTrain} />
            </View>
          </Grid>
          {/* BUTTON */}
        </ScrollView>

        {/* ============================== START MODAL COMPONENT HERE ============================== */}

        {/* ========= MODAL LOADING ========= */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 99}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={250}
          animationOutTiming={250}
          backdropTransitionInTiming={250}
          backdropTransitionOutTiming={250}
        >
          {this.loadingBooking()}
        </Modal>
        {/* ========= MODAL LOADING ========= */}

        {/* =============================== MODAL DEPARTURE DATE & RETURN DATE ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={
            this.state.visibleModal === 3 || this.state.visibleModal === 4
          }
          style={styles.bottomModal}
          onBackButtonPress={this.resetModal}
          onBackdropPress={this.resetModal}
        >
          <View style={styles.modalContent}>
            <Grid style={styles.modalHeaderDatepicker}>
              <Row style={{ paddingBottom: 7.5 }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={() => this.resetModal()}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color="#FFFFFF"
                      size={28}
                    />
                  </Col>
                  <Col size={6.7} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 3
                        ? 'Departure Date'
                        : 'Return Date'}
                    </Text>
                  </Col>
                  <Col size={1.3} />
                </TouchableOpacity>
              </Row>
            </Grid>
            <Grid>
              <Col>
                <View style={{ flex: 1 }}>
                  <Dates
                    date={this.state.date}
                    onDatesChange={
                      this.state.visibleModal === 3
                        ? this.setDepartureDate
                        : this.setReturnDate
                    }
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    focusedInput={this.state.focus}
                    isDateBlocked={isDateBlocked}
                  />
                  <Text
                    style={{
                      paddingTop: 20,
                      fontSize: 16,
                      color: '#000',
                      paddingLeft: 20,
                    }}
                  >
                    {this.state.visibleModal === 3
                      ? 'Departure Date: ' +
                        moment(this.state.departureDate).format('DD MMM YYYY')
                      : 'Return Date: ' +
                        moment(this.state.returnDate).format('DD MMM YYYY')}
                  </Text>
                </View>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL DEPARTURE DATE & RETURN DATE ===================================== */}

        {/* =============================== MODAL DEPARTURE & DESTINATION ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={
            this.state.visibleModal === 1 || this.state.visibleModal === 2
          }
          onBackButtonPress={this.resetModal}
          onBackdropPress={this.resetModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalContent}>
            <Grid style={{ flex: 0 }}>
              <Row style={[styles.modalHeader, { paddingTop: 15 }]}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={() => this.resetModal()}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color="#FFFFFF"
                      size={28}
                    />
                  </Col>
                  <Col size={6.7} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 1
                        ? 'Choose Departure'
                        : 'Choose Destination'}
                    </Text>
                  </Col>
                  <Col size={1.3} />
                </TouchableOpacity>
              </Row>
              <SubHeaderPage />
              <Row style={styles.modalSearch}>
                <Col>
                  <SearchInput
                    placeholder="Search City or Stations"
                    onChangeText={data => this.searchingDeparture(data)}
                  />
                </Col>
              </Row>
            </Grid>
            <Grid>
              <Col
                style={{
                  backgroundColor: '#FFFFFF',
                  padding: 20,
                  paddingTop: 10,
                  borderWidth: 0.5,
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  borderColor: '#FFFFFF',
                }}
              >
                <ScrollView>
                  <Grid
                    style={{
                      flex: 1,
                      backgroundColor: '#FFFFFF',
                      padding: 10,
                      paddingBottom: 10,
                      paddingLeft: 0,
                    }}
                  >
                    <Col style={{ alignItems: 'flex-start' }}>
                      <Text
                        style={{
                          fontFamily: fontReguler,
                          color: '#828282',
                          fontSize: 16,
                          marginLeft: 5,
                        }}
                      >
                        Popular destination
                      </Text>
                    </Col>
                  </Grid>
                  <Grid>
                    <Col>
                      {this.props.loadStation ? (
                        <DotIndicator
                          size={10}
                          style={{ flex: 0, backgroundColor: '#FFFFFF' }}
                          color={thameColors.secondary}
                        />
                      ) : (
                        <FlatList
                          showsVerticalScrollIndicator={false}
                          keyExtractor={this._keyExtractor}
                          data={
                            this.state.destinationResult.length === 0
                              ? dataStation
                              : this.state.destinationResult
                          }
                          renderItem={({ item, i }) => (
                            <Grid
                              key={i}
                              style={{
                                flex: 1,
                                backgroundColor: '#FFFFFF',
                                padding: 10,
                              }}
                            >
                              <TouchableOpacity
                                style={{ flexDirection: 'row', flex: 1 }}
                                onPress={() =>
                                  this.setOriginDestination(
                                    this.state.visibleModal,
                                    item
                                  )
                                }
                              >
                                <Col style={{ alignItems: 'flex-start' }}>
                                  <Text
                                    style={{
                                      fontFamily: fontBold,
                                      color: '#222222',
                                      fontSize: 16,
                                    }}
                                  >
                                    {item.name}
                                  </Text>
                                </Col>
                                <Col style={{ alignItems: 'flex-end' }}>
                                  <View
                                    style={{
                                      backgroundColor: '#f8f8f8',
                                      width: 50,
                                      height: 25,
                                      alignItems: 'flex-end',
                                      justifyContent: 'center',
                                      borderRadius: 5,
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontFamily: fontReguler,
                                        color: '#838383',
                                        fontSize: 12,
                                        alignSelf: 'center',
                                      }}
                                    >
                                      {item.code}
                                    </Text>
                                  </View>
                                </Col>
                              </TouchableOpacity>
                            </Grid>
                          )}
                          ListEmptyComponent={
                            <Text>Please type your destination.</Text>
                          }
                        />
                      )}
                    </Col>
                  </Grid>
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL DEPARTURE & DESTINATION ===================================== */}

        {/* =============================== MODAL PASSENGER ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 5}
          onBackButtonPress={this.resetModal}
          onBackdropPress={this.resetModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#838383',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col
                  size={2}
                  style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                >
                  <Icon
                    name="close"
                    type="evilIcon"
                    color="#008195"
                    size={20}
                  />
                </Col>
                <Col
                  size={8}
                  style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                >
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Passenger
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: '#000',
                      fontSize: 14,
                    }}
                  >
                    Adult
                  </Text>
                  <Text style={{ color: '#828282', fontSize: 12 }}>Age 3+</Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={[0, 1, 2, 3, 4]}
                    selectedIndex={this.state.adult}
                    onValueChange={data => this.setPassengerData('adult', data)}
                    wrapperHeight={210}
                    wrapperBackground={'#FFFFFF'}
                    itemHeight={60}
                    highlightColor={'#d8d8d8'}
                    highlightBorderWidth={0.5}
                    activeItemColor={'#222121'}
                    itemColor={'#ed6d00'}
                  />
                </View>
              </Col>

              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{ fontWeight: '500', color: '#000', fontSize: 14 }}
                  >
                    Infant
                  </Text>
                  <Text style={{ color: '#828282', fontSize: 12 }}>
                    Bellow age 3
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={['-', 0, 1, 2, 3, 4]}
                    selectedIndex={this.state.infant + 1}
                    renderItem={() => {}}
                    onValueChange={data =>
                      this.setPassengerData('infant', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={'#FFFFFF'}
                    itemHeight={60}
                    highlightColor={'#d8d8d8'}
                    highlightBorderWidth={0.5}
                    activeItemColor={'#222121'}
                    itemColor={'#ed6d00'}
                  />
                </View>
              </Col>
            </Grid>
            <Grid style={{ flex: 0, paddingBottom: 10 }}>
              <Col>
                <Button
                  label="Done"
                  onClick={() => this.setState({ visibleModal: null })}
                />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL PASSENGER ===================================== */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataStation: state.train.stationSearch,
    dataTrain: state.train.searchTrain,
    loadSearchTrain: state.train.fetchingTrain,
    loadStation: state.train.fetchingStation,

    payloads: state.train.payloadTrain,
  };
}
export default connect(mapStateToProps)(Train);
