const data = [
  {
    code: 'KTG',
    name: 'KETAPANG',
  },
  {
    code: 'MAS',
    name: 'MASON PINE',
  },
  {
    code: 'MSP',
    name: 'MASON PINE BANDUNG',
  },
  {
    code: 'MPII',
    name: 'MPII',
  },
  {
    code: 'MPIESS',
    name: 'MPIESSS',
  },
  {
    code: 'MPIES',
    name: 'MPIESS',
  },
  {
    code: 'MPIE',
    name: 'MPIE',
  },
  {
    code: 'MP',
    name: 'MPIE',
  },
  {
    code: 'BST',
    name: 'BANDARA SOEKARNO HATTA',
  },
  {
    code: 'LLG',
    name: 'LUBUK LINGGAU',
  },
  {
    code: 'TI',
    name: 'TEBING TINGGI',
  },
  {
    code: 'LT',
    name: 'LAHAT',
  },
  {
    code: 'ME',
    name: 'MUARA ENIM',
  },
  {
    code: 'PBM',
    name: 'PRABUMULIH',
  },
  {
    code: 'IDR',
    name: 'INDRALAYA',
  },
  {
    code: 'JG',
    name: 'JOMBANG',
  },
  {
    code: 'TESTT',
    name: 'TESTERR',
  },
  {
    code: 'PWT',
    name: 'PURWOKERTO',
  },
  {
    code: 'SDR',
    name: 'SIDAREJA',
  },
  {
    code: 'CI',
    name: 'CIAMIS',
  },
  {
    code: 'MNJ',
    name: 'MANONJAYA',
  },
  {
    code: 'BMW',
    name: 'BUMIWALUYA',
  },
  {
    code: 'PSE',
    name: 'PASAR SENEN',
  },
  {
    code: 'JNG',
    name: 'JATINEGARA',
  },
  {
    code: 'BKS',
    name: 'BEKASI',
  },
  {
    code: 'BOO',
    name: 'BOGOR',
  },
  {
    code: 'WJ',
    name: 'WOJO',
  },
  {
    code: 'SBX',
    name: 'SURABAYA KOTA',
  },
  {
    code: 'KPT',
    name: 'KERTAPATI',
  },
  {
    code: 'GMR',
    name: 'GAMBIR',
  },
  {
    code: 'KTN',
    name: 'KAYUTANAM',
  },
  {
    code: 'SCN',
    name: 'SICINCIN',
  },
  {
    code: 'PDX',
    name: 'PADANGX',
  },
  {
    code: 'BIM',
    name: 'BANDARA INTERNASIONAL MINANGKABAU',
  },
  {
    code: 'PMN',
    name: 'PARIAMAN',
  },
  {
    code: 'KI',
    name: 'KURAITAJI',
  },
  {
    code: 'PAK',
    name: 'PAUH KAMBAR',
  },
  {
    code: 'LA',
    name: 'LUBUK ALUNG',
  },
  {
    code: 'DUK',
    name: 'DUKU',
  },
  {
    code: 'LBY',
    name: 'LUBUK BUAYA',
  },
  {
    code: 'TAB',
    name: 'TABING',
  },
  {
    code: 'ATR',
    name: 'AIR TAWAR',
  },
  {
    code: 'AI',
    name: 'PASAR ALAI',
  },
  {
    code: 'PD',
    name: 'PADANG',
  },
  {
    code: 'LDO',
    name: 'LEDOKOMBO',
  },
  {
    code: 'SGJ',
    name: 'SINGOJURUH',
  },
  {
    code: 'AGO',
    name: 'ARGOPURO',
  },
  {
    code: 'WO',
    name: 'WONOKROMO',
  },
  {
    code: 'WR',
    name: 'WARU',
  },
  {
    code: 'TLN',
    name: 'TULANGAN',
  },
  {
    code: 'TRK',
    name: 'TARIK',
  },
  {
    code: 'TGA',
    name: 'TANGGULANGIN',
  },
  {
    code: 'TES',
    name: 'TANDES',
  },
  {
    code: 'SB',
    name: 'SURABAYA KOTA',
  },
  {
    code: 'SGU',
    name: 'SURABAYA GUBENG',
  },
  {
    code: 'SRJ',
    name: 'SUMBEREJO',
  },
  {
    code: 'SGS',
    name: 'SINGOSARI',
  },
  {
    code: 'BJK',
    name: 'BANJAR KEMANTREN',
  },
  {
    code: 'PWJ',
    name: 'PAGERWOJO',
  },
  {
    code: 'SBP',
    name: 'SUMBERPUCUNG',
  },
  {
    code: 'SDA',
    name: 'SIDOARJO',
  },
  {
    code: 'SPJ',
    name: 'SEPANJANG',
  },
  {
    code: 'PC',
    name: 'PUCUK',
  },
  {
    code: 'PR',
    name: 'PORONG',
  },
  {
    code: 'PGJ',
    name: 'POGAJIH',
  },
  {
    code: 'NB',
    name: 'NGEBRUK',
  },
  {
    code: 'MR',
    name: 'MOJOKERTO',
  },
  {
    code: 'LW',
    name: 'LAWANG',
  },
  {
    code: 'LMG',
    name: 'LAMONGAN',
  },
  {
    code: 'KRN',
    name: 'KRIAN',
  },
  {
    code: 'KSB',
    name: 'KESAMBEN',
  },
  {
    code: 'KPS',
    name: 'KAPAS',
  },
  {
    code: 'KDA',
    name: 'KANDANGAN',
  },
  {
    code: 'GDG',
    name: 'GEDANGAN',
  },
  {
    code: 'DD',
    name: 'DUDUK',
  },
  {
    code: 'CME',
    name: 'CERME',
  },
  {
    code: 'BWO',
    name: 'BOWERNO',
  },
  {
    code: 'BJ',
    name: 'BOJONEGORO',
  },
  {
    code: 'BNW',
    name: 'BENOWO',
  },
  {
    code: 'BG',
    name: 'BANGIL',
  },
  {
    code: 'BBT',
    name: 'BABAT',
  },
  {
    code: 'BW',
    name: 'BANYUWANGI BARU',
  },
  {
    code: 'KNE',
    name: 'KARANGASEM',
  },
  {
    code: 'RGP',
    name: 'ROGOJAMPI',
  },
  {
    code: 'TGR',
    name: 'TEMGURUH',
  },
  {
    code: 'KSL',
    name: 'KALISETAIL',
  },
  {
    code: 'SWD',
    name: 'SUMBER WADUNG',
  },
  {
    code: 'GLM',
    name: 'GLENMORE',
  },
  {
    code: 'KBR',
    name: 'KALIBARU',
  },
  {
    code: 'KLT',
    name: 'KALISAT',
  },
  {
    code: 'AJ',
    name: 'ARJASA',
  },
  {
    code: 'JR',
    name: 'JEMBER',
  },
  {
    code: 'TAL',
    name: 'TALUN',
  },
  {
    code: 'GRM',
    name: 'GARUM',
  },
  {
    code: 'RJ',
    name: 'REJOTANGAN',
  },
  {
    code: 'NT',
    name: 'NGUNUT',
  },
  {
    code: 'SBL',
    name: 'SUMBERGEMPOL',
  },
  {
    code: 'KRS',
    name: 'KRAS',
  },
  {
    code: 'NDL',
    name: 'NGADILUWIH',
  },
  {
    code: 'PPR',
    name: 'PAPAR',
  },
  {
    code: 'CRM',
    name: 'CURAHMALANG',
  },
  {
    code: 'SBO',
    name: 'SUMOBITO',
  },
  {
    code: 'PTR',
    name: 'PETERONGAN',
  },
  {
    code: 'SMB',
    name: 'SEMBUNG',
  },
  {
    code: 'WLT',
    name: 'WALANTAKA',
  },
  {
    code: 'TOJB',
    name: 'TONJONG BARU',
  },
  {
    code: 'KEN',
    name: 'KRENCENG',
  },
  {
    code: 'KRA',
    name: 'KARANGANTU',
  },
  {
    code: 'JBU',
    name: 'JAMBU BARU',
  },
  {
    code: 'CKL',
    name: 'CIKEUSAL',
  },
  {
    code: 'CT',
    name: 'CATANG',
  },
  {
    code: 'SG',
    name: 'SERANG',
  },
  {
    code: 'CLG',
    name: 'CILEGON',
  },
  {
    code: 'MER',
    name: 'MERAK',
  },
  {
    code: 'RK',
    name: 'RANGKASBITUNG',
  },
  {
    code: 'SBI',
    name: 'SURABAYA PASARTURI',
  },
  {
    code: 'TG',
    name: 'TEGAL',
  },
  {
    code: 'CU',
    name: 'CEPU',
  },
  {
    code: 'WNG',
    name: 'WONOGIRI',
  },
  {
    code: 'SK',
    name: 'SOLOJEBRES',
  },
  {
    code: 'ABR',
    name: 'AMBARAWA',
  },
  {
    code: 'GD',
    name: 'GUNDIH',
  },
  {
    code: 'TW',
    name: 'TELAWA',
  },
  {
    code: 'KEJ',
    name: 'KEDUNGJATI',
  },
  {
    code: 'SLM',
    name: 'SALEM',
  },
  {
    code: 'SMT',
    name: 'SEMARANG TAWANG',
  },
  {
    code: 'SMC',
    name: 'SEMARANG PONCOL',
  },
  {
    code: 'SDI',
    name: 'SEDADI',
  },
  {
    code: 'NBO',
    name: 'NGROMBO',
  },
  {
    code: 'KGT',
    name: 'KARANGJATI',
  },
  {
    code: 'GUB',
    name: 'GUBUG',
  },
  {
    code: 'BBG',
    name: 'BRUMBUNG',
  },
  {
    code: 'ATA',
    name: 'ALASTUA',
  },
  {
    code: 'KRG',
    name: 'KRUENGGEUKEUH',
  },
  {
    code: 'BKH',
    name: 'BUNGKAIH',
  },
  {
    code: 'KRM',
    name: 'KRUENGMANE',
  },
  {
    code: 'SIR',
    name: 'SIANTAR',
  },
  {
    code: 'CKP',
    name: 'CIKAMPEK',
  },
  {
    code: 'DWN',
    name: 'DAWUAN',
  },
  {
    code: 'KOS',
    name: 'KOSAMBI',
  },
  {
    code: 'KLI',
    name: 'KLARI',
  },
  {
    code: 'KW',
    name: 'KARAWANG',
  },
  {
    code: 'KDH',
    name: 'KEDUNGGEDEH',
  },
  {
    code: 'LMB',
    name: 'LEMAHABANG',
  },
  {
    code: 'CKR',
    name: 'CIKARANG',
  },
  {
    code: 'TB',
    name: 'TAMBUN',
  },
  {
    code: 'KMO',
    name: 'KEMAYORAN',
  },
  {
    code: 'TPK',
    name: 'TANJUNGPRIUK',
  },
  {
    code: 'CBR',
    name: 'CIBUNGUR',
  },
  {
    code: 'GDR',
    name: 'GEDUNG RATU',
  },
  {
    code: 'BTI',
    name: 'BERANTI',
  },
  {
    code: 'TGI',
    name: 'TEGINENENG',
  },
  {
    code: 'RGS',
    name: 'RENGAS',
  },
  {
    code: 'HJP',
    name: 'HAJI PEMANGGILAN',
  },
  {
    code: 'BBA',
    name: 'BLAMBANGAN PAGAR',
  },
  {
    code: 'KAG',
    name: 'KALIBALANGAN',
  },
  {
    code: 'CMS',
    name: 'CANDIMAS',
  },
  {
    code: 'KTP',
    name: 'KETAPANG',
  },
  {
    code: 'KB',
    name: 'KOTABUMI',
  },
  {
    code: 'SLS',
    name: 'SULUSUBAN',
  },
  {
    code: 'BKI',
    name: 'BEKRI',
  },
  {
    code: 'RJS',
    name: 'REJOSARI',
  },
  {
    code: 'LAR',
    name: 'LABUANRATU',
  },
  {
    code: 'TNK',
    name: 'TANJUNGKARANG',
  },
  {
    code: 'STA',
    name: 'SOLOKOTA',
  },
  {
    code: 'PNT',
    name: 'PASARNGUTER',
  },
  {
    code: 'SKH',
    name: 'SUKOHARJO',
  },
  {
    code: 'PWS',
    name: 'PURWOSARI',
  },
  {
    code: 'KT',
    name: 'KLATEN',
  },
  {
    code: 'BBN',
    name: 'BRAMBANAN',
  },
  {
    code: 'MGW',
    name: 'MAGUWO',
  },
  {
    code: 'LPN',
    name: 'LEMPUYANGAN',
  },
  {
    code: 'WT',
    name: 'WATES',
  },
  {
    code: 'JN',
    name: 'JENAR',
  },
  {
    code: 'CN',
    name: 'CIREBON',
  },
  {
    code: 'SI',
    name: 'SUKABUMI',
  },
  {
    code: 'GDS',
    name: 'GANDASOLI',
  },
  {
    code: 'LP',
    name: 'LAMPEGAN',
  },
  {
    code: 'CRG',
    name: 'CIREUNGAS',
  },
  {
    code: 'CBB',
    name: 'CIBEBER',
  },
  {
    code: 'CJ',
    name: 'CIANJUR',
  },
  {
    code: 'BIJ',
    name: 'BINJAI',
  },
  {
    code: 'MDN',
    name: 'MEDAN',
  },
  {
    code: 'SR',
    name: 'SRAGEN',
  },
  {
    code: 'SPH',
    name: 'SUMPIUH',
  },
  {
    code: 'BMG',
    name: 'BLIMBING',
  },
  {
    code: 'ML',
    name: 'MALANG',
  },
  {
    code: 'MLK',
    name: 'MALANG KOTALAMA',
  },
  {
    code: 'KPN',
    name: 'KEPANJEN',
  },
  {
    code: 'WG',
    name: 'WLINGI',
  },
  {
    code: 'BL',
    name: 'BLITAR',
  },
  {
    code: 'TA',
    name: 'TULUNGAGUNG',
  },
  {
    code: 'KD',
    name: 'KEDIRI',
  },
  {
    code: 'KTS',
    name: 'KERTOSONO',
  },
  {
    code: 'NJ',
    name: 'NGANJUK',
  },
  {
    code: 'MN',
    name: 'MADIUN',
  },
  {
    code: 'PA',
    name: 'PARON',
  },
  {
    code: 'WK',
    name: 'WALIKUKUN',
  },
  {
    code: 'SLO',
    name: 'SOLOBALAPAN',
  },
  {
    code: 'YK',
    name: 'YOGYAKARTA',
  },
  {
    code: 'KTA',
    name: 'KUTOARJO',
  },
  {
    code: 'KM',
    name: 'KEBUMEN',
  },
  {
    code: 'GB',
    name: 'GOMBONG',
  },
  {
    code: 'KYA',
    name: 'KROYA',
  },
  {
    code: 'BJR',
    name: 'BANJAR',
  },
  {
    code: 'TSM',
    name: 'TASIKMALAYA',
  },
  {
    code: 'CPD',
    name: 'CIPEUNDEUY',
  },
  {
    code: 'CB',
    name: 'CIBATU',
  },
  {
    code: 'LO',
    name: 'LEUWIGOONG',
  },
  {
    code: 'KRAI',
    name: 'KARANGSARI',
  },
  {
    code: 'LL',
    name: 'LELES',
  },
  {
    code: 'HRP',
    name: 'HAURPUGUR',
  },
  {
    code: 'CMK',
    name: 'CIMEKAR',
  },
  {
    code: 'CTH',
    name: 'CIKUDAPATEUH',
  },
  {
    code: 'CLE',
    name: 'CILAME',
  },
  {
    code: 'SKT',
    name: 'SASAKSAAT',
  },
  {
    code: 'MSI',
    name: 'MASWATI',
  },
  {
    code: 'RH',
    name: 'RENDEH',
  },
  {
    code: 'SUT',
    name: 'SUKATANI',
  },
  {
    code: 'CA',
    name: 'CIGANEA',
  },
  {
    code: 'PLD',
    name: 'PLERED',
  },
  {
    code: 'PWK',
    name: 'PURWAKARTA',
  },
  {
    code: 'CMD',
    name: 'CIMINDI',
  },
  {
    code: 'NG',
    name: 'NAGREG',
  },
  {
    code: 'CIR',
    name: 'CIROYOM',
  },
  {
    code: 'CD',
    name: 'CIKADONGDONG',
  },
  {
    code: 'GK',
    name: 'GADOBANGKONG',
  },
  {
    code: 'PDL',
    name: 'PADALARANG',
  },
  {
    code: 'CMI',
    name: 'CIMAHI',
  },
  {
    code: 'CCL',
    name: 'CICALENGKA',
  },
  {
    code: 'KAC',
    name: 'KIARACONDONG',
  },
  {
    code: 'RCK',
    name: 'RANCAEKEK',
  },
  {
    code: 'BD',
    name: 'BANDUNG',
  },
  {
    code: 'KP',
    name: 'KANTOR PUSAT',
  },
];

export default data;
