import { StyleSheet, Dimensions } from 'react-native';
import {
  fontReguler,
  fontExtraBold,
  fontBold,
  thameColors,
  asitaColor,
} from '../../base/constant';
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  textLabel: { fontFamily: fontReguler, color: '#828282', fontSize: 16 },
  textBold: {
    fontFamily: fontExtraBold,
    color: '#222222',
    fontSize: 18,
    fontWeight: '500',
    paddingBottom: 5,
  },
  textInitial: {
    fontFamily: fontBold,
    color: '#222222',
    fontSize: 14,
    fontWeight: '500',
  },

  sectionToggle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -40,
    paddingBottom: 10,
  },
  sectionInput: {
    paddingBottom: 10,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 20,
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#f0f0f0',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 7,
  },

  // MODAL DEPARTURE
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 45,
    width: deviceWidth,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontFamily: fontBold,
    fontSize: 18,
  },
  modalSearch: {
    flex: 0,
    height: 70,
    width: deviceWidth,
    marginTop: -35,
    marginBottom: 10,
  },
  // MODAL DEPARTURE

  // MODAL PASSENGER
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // MODAL PASSENGER

  // MODAL SEAT CLASS
  modalSeatClass: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // MODAL SEAT CLASS

  AutoCompleteClose: {
    padding: 5,
    color: '#999',
    fontWeight: '900',
    elevation: 5,
    fontSize: 18,
  },
});
