import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import numeral from 'numeral';

import { ButtonRounded } from '../../../../elements/Button';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../../base/constant';

export default class TrainDetails extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ marginLeft: 25, marginRight: 25, marginTop: 25 }}>
          <Grid style={{ marginTop: 10 }}>
            <Col>
              <View>
                <Text style={styles.textBody}>Adult Fare (x1)</Text>
              </View>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <View>
                <Text style={styles.textBold}>
                  Rp
                  {numeral(this.props.price)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </View>
            </Col>
          </Grid>
          <Grid style={{ marginTop: 20, marginBottom: 20 }}>
            <Col style={styles.sectionHr}></Col>
          </Grid>

          <Grid style={{ marginTop: 10, alignItems: 'flex-end' }}>
            <Col>
              <Grid style={{ marginBottom: 15 }}>
                <Col style={styles.sectionHr}></Col>
              </Grid>
              <Grid style={{ flex: 0 }}>
                <Col>
                  <Text style={styles.textBold}>Tax and other fees</Text>
                </Col>
              </Grid>
              <Grid style={{ flex: 0 }}>
                <Col>
                  <View>
                    <Text style={styles.textBody}>Service Charge</Text>
                  </View>
                  <View>
                    <Text style={styles.textBody}>Tax</Text>
                  </View>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <View>
                    <Text style={styles.textGreen}>Free</Text>
                  </View>
                  <View>
                    <Text style={styles.textBold}>Included</Text>
                  </View>
                </Col>
              </Grid>
            </Col>
          </Grid>
          <Grid style={{ marginTop: 20, marginBottom: 20 }}>
            <Col style={styles.sectionHr}></Col>
          </Grid>
        </ScrollView>

        <Grid
          style={{ justifyContent: 'center', alignItems: 'center', flex: 0 }}
        >
          <Col>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid
              style={{
                flex: 0,
                marginBottom: 10,
                marginLeft: 25,
                marginRight: 25,
              }}
            >
              <Col>
                <Text style={styles.textBold}>Total Payment</Text>
              </Col>
              <Col style={{ alignItems: 'flex-end' }}>
                <Text style={styles.textPrice}>
                  Rp.{' '}
                  {numeral(this.props.price)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={{ flex: 0, padding: 10, marginLeft: 9 }}>
              <ButtonRounded
                onClick={this.props.modalPress}
                label="SELECT TRAIN"
              />
            </Grid>
          </Col>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: thameColors.white, flex: 1 },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  textBold: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textGreen: { fontFamily: fontBold, color: thameColors.green, fontSize: 14 },
  textBody: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 14,
  },
  textPrice: {
    fontFamily: fontExtraBold,
    color: asitaColor.orange,
    fontSize: 16,
  },
});
