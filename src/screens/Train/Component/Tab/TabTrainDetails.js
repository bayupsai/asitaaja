import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Grid, Col } from 'react-native-easy-grid';
import moment from 'moment';
import numeral from 'numeral';
import { ButtonRounded } from '../../../../elements/Button';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../../base/constant/index';

export default class TrainDetails extends React.PureComponent {
  render() {
    return (
      <View title={this.props.title} style={styles.content}>
        <ScrollView>
          <Grid style={{ marginTop: 25 }}>
            <Col size={4.5} style={styles.colEnd}>
              <Text style={styles.textHeader}>
                {this.props.departureLocation}
              </Text>
            </Col>
            <Col size={1} style={styles.colCenter}>
              <Image
                resizeMode="center"
                style={{
                  width: 40,
                  height: 25,
                  tintColor: asitaColor.orange,
                }}
                source={require('../../../../assets/icons/icon_trains.png')}
              />
            </Col>
            <Col size={4.5} style={styles.colStart}>
              <Text style={styles.textHeader}>
                {this.props.arrivalLocation}
              </Text>
            </Col>
          </Grid>

          <Grid style={{ marginTop: 25, marginLeft: 25, marginRight: 10 }}>
            <Col size={2} style={styles.colStart}>
              <Grid style={{ flex: 0, marginBottom: 10 }}>
                <Col>
                  <View>
                    <Text style={styles.textBold}>
                      {this.props.departureTime}
                    </Text>
                  </View>
                  <View>
                    <Text style={styles.textDate}>
                      {moment(this.props.departure_date).format('DD MMM')}
                    </Text>
                  </View>
                </Col>
              </Grid>
              <Grid style={{ flex: 0, marginBottom: 30, marginTop: 30 }}>
                <Col>
                  <View>
                    <Text style={styles.textDate}>Durations</Text>
                  </View>
                </Col>
              </Grid>
              <Grid style={{ flex: 0, marginBottom: 20 }}></Grid>
              <Grid style={{ flex: 0, marginBottom: 20 }}>
                <Col>
                  <View>
                    <Text style={styles.textBold}>
                      {this.props.arrivalTime}
                    </Text>
                  </View>
                  <View>
                    <Text style={styles.textDate}>
                      {moment(this.props.arrival_date).format('DD MMM')}
                    </Text>
                  </View>
                </Col>
              </Grid>
            </Col>

            <Col
              size={1.5}
              style={{ justifyContent: 'flex-start', alignItems: 'center' }}
            >
              <Icon
                size={18}
                color={thameColors.gray}
                name="ios-radio-button-off"
                type="ionicon"
              />
              <View style={styles.gridRow}></View>
              <Icon
                name="ios-radio-button-on"
                color={thameColors.gray}
                size={18}
                type="ionicon"
              />
            </Col>

            <Col size={7.5} style={styles.colStart}>
              <Grid style={{ flex: 0, marginBottom: 15, marginTop: -10 }}>
                <Col>
                  <Text style={styles.textBold}>
                    {this.props.departureLocation} ({this.props.departure_code})
                  </Text>
                </Col>
              </Grid>
              <Grid style={{ flex: 0, marginBottom: 25, marginTop: 15 }}>
                <Col>
                  <Text>{this.props.train}</Text>
                </Col>
              </Grid>
              <Grid style={{ flex: 0, marginBottom: 35 }}>
                <Col size={3} style={styles.colStart}>
                  <Image
                    style={{ width: 60, height: 40 }}
                    resizeMode="center"
                    source={require('../../../../assets/logos/train.png')}
                  />
                </Col>
                <Col size={7} style={styles.colStart}>
                  <Text style={styles.textItinerary}>
                    Ekonomi • (Sub Class {this.props.subClass})
                  </Text>
                </Col>
              </Grid>
              <Grid style={{ flex: 0, marginBottom: 25 }}>
                <Col
                  style={{ justifyContent: 'center', alignItems: 'flex-start' }}
                >
                  <Text style={styles.textBold}>
                    {this.props.arrivalLocation} ({this.props.arrival_code})
                  </Text>
                </Col>
              </Grid>
            </Col>
          </Grid>
        </ScrollView>

        <Grid
          style={{ justifyContent: 'center', alignItems: 'center', flex: 0 }}
        >
          <Col>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid
              style={{
                flex: 0,
                marginBottom: 10,
                marginLeft: 25,
                marginRight: 25,
              }}
            >
              <Col>
                <Text style={styles.textBold}>Total Payment</Text>
              </Col>
              <Col style={{ alignItems: 'flex-end' }}>
                <Text style={styles.textPrice}>
                  Rp.{' '}
                  {numeral(this.props.price)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid style={{ flex: 0, padding: 10, marginLeft: 9 }}>
              <ButtonRounded
                onClick={this.props.modalPress}
                label="SELECT TRAIN"
              />
            </Grid>
          </Col>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: thameColors.white,
    flex: 1,
  },
  textHeader: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 18,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textItinerary: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 14,
    marginLeft: 3,
  },
  textDate: { fontFamily: fontReguler, color: thameColors.darkGray },
  textPrice: {
    fontFamily: fontExtraBold,
    color: asitaColor.orange,
    fontSize: 16,
  },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  gridRow: {
    height: 65 + '%',
    width: 0.8,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  gridTop: {
    width: 100 + '%',
    height: 0.4,
    backgroundColor: thameColors.gray,
    justifyContent: 'center',
    alignItems: 'flex-end',
    flex: 1,
  },
  gridBottom: {
    width: 100 + '%',
    height: 0.8,
    backgroundColor: thameColors.gray,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flex: 1,
  },
  colCenter: { justifyContent: 'center', alignItems: 'center' },
  colEnd: { justifyContent: 'center', alignItems: 'flex-end' },
  colStart: { justifyContent: 'center', alignItems: 'flex-start' },
});
