import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  FlatList,
  StyleSheet,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import numeral from 'numeral';
import { connect } from 'react-redux';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import CardResult from '../Component/SearchTrain/CardResult';
// import TrainInfo from '../../FormOrderTrain/TrainInfo';
import { actionTrainSelected } from '../../../redux/actions/TrainAction';
import { ButtonRounded } from '../../../elements/Button';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
} from '../../../base/constant/index';
import DateSlider from '../../../components/CalendarSlider/index';

const window = Dimensions.get('window');
let deviceHeight = window.height;
let deviceWidth = window.width;

class SearchTrainReturn extends PureComponent {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      dating: new Date(),
      departure_code: this.props.train.payloadTrain.data.arrival_code,
      arrival_code: this.props.train.payloadTrain.data.departure_code,
      departureDate: this.props.train.payloadTrain.data.return_date,
      adult: this.props.payloadTrain.data.adult,
      data: [
        {
          id: 0,
          depart: 'BDG',
          arrive: 'GMBR',
          timedepart: ' 09:40',
          timearrive: '13:15',
          train: 'Argo Parahyangan',
          price: '110.000',
        },
        {
          id: 1,
          depart: 'BDG',
          arrive: 'GMBR',
          timedepart: ' 09:40',
          timearrive: '13:10',
          train: 'Argo Parahyangan',
          price: '150.000',
        },
        {
          id: 2,
          depart: 'BDG',
          arrive: 'GMBR',
          timedepart: ' 07:35',
          timearrive: '13:15',
          train: 'Connecting Trains',
          price: '155.000',
        },
      ],
      dataReturnTrain: this.props.dataTrain.returns,
      trainSelected: this.props.navigation.getParam('dataTrainDepartures'),
      arrayCalendar: [1],
      arrayShimerCalendar: [1, 2, 3, 4, 5, 6, 7],
    };
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  trainSelected = payload => {
    console.log(payload);
    this.props.dispatch(actionTrainSelected(payload, 'return'));
    this.props.navigation.navigate('FormOrderTrain');
  };

  _renderItem = ({ item, i }) => (
    <CardResult
      {...this.props}
      key={i}
      depart={this.state.departure_code}
      arrive={this.state.arrival_code}
      timedepart={item.arrival_time}
      timearrive={item.departure_time}
      subClass={item.sub_class}
      train={item.name}
      price={numeral(item.price_adult).format('0,0')}
      onPress={() => this.trainSelected(item)}
      modalPress={() => this.trainSelected(item)}
    />
  );

  loadingCalendar = index => {
    return (
      <Grid key={index} style={[styles.cardCalendar, { padding: 10 }]}>
        {this.state.arrayShimerCalendar.map((item, i) => (
          <Col
            style={{ marginLeft: 15, marginRight: 15, alignItems: 'center' }}
            key={i}
          >
            <ShimmerPlaceHolder
              autoRun={true}
              visible={false}
              height={25}
              width={25}
              style={{ paddingBottom: 5 }}
            />
            <ShimmerPlaceHolder
              autoRun={true}
              visible={false}
              height={25}
              width={28}
              style={{ paddingBottom: 10 }}
            />
            <ShimmerPlaceHolder
              autoRun={true}
              visible={false}
              height={15}
              width={10}
              style={{ paddingBottom: 5 }}
            />
          </Col>
        ))}
      </Grid>
    );
  };

  emptyResults = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <View>
          <Image
            resizeMode="center"
            style={{ height: deviceHeight / 2.5, width: deviceWidth - 40 }}
            source={require('../../../assets/icons/search_not_found.png')}
          />
        </View>
        <View style={{ paddingTop: 10, alignItems: 'center' }}>
          <Text
            style={{ color: '#222222', fontFamily: fontBold, fontSize: 22 }}
          >
            No Schedules Available
          </Text>
        </View>
        <View
          style={{
            paddingTop: 20,
            paddingLeft: 30,
            paddingRight: 30,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: '#222222',
              fontFamily: fontReguler,
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            Please search for another date or you may
          </Text>
        </View>
        <View
          style={{
            paddingTop: 3,
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 10,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: '#222222',
              fontFamily: fontReguler,
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            click the Change Search button below
          </Text>
        </View>
        <View style={{ marginTop: 30 }}>
          <ButtonRounded
            onClick={() => this.props.navigation.goBack()}
            label="CHANGE SEARCH"
          />
        </View>
      </View>
    );
  };

  render() {
    console.log(this.state);
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <HeaderPage title="Return" callback={this.goBackHeader} />
        <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
          <SubHeaderPage />

          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: -60,
            }}
          >
            {/* <TrainInfo
              {...this.props.train.trainSelected}
              {...this.props.train}
              title="Departure Train"
            /> */}
          </View>
          <View
            style={{
              backgroundColor: '#f0f0f0',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 0,
            }}
          >
            <Text style={{ color: '#000', fontSize: 14, fontWeight: '500' }}>
              Select Your Return From{' '}
              {this.state.dataReturnTrain.length > 0
                ? this.state.dataReturnTrain.length
                : '0'}{' '}
              Schedules
            </Text>
          </View>
          <Grid>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              {this.props.loading ? (
                this.state.arrayCalendar.map((item, i) =>
                  this.loadingCalendar(i)
                )
              ) : this.props.dataTrain.returns.length !== 0 ? (
                <DateSlider {...this.props.navigation.state.params} />
              ) : (
                <View></View>
              )}
            </Col>
          </Grid>
          <Grid style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Col>
              <FlatList
                data={this.state.dataReturnTrain}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={this.emptyResults()}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
              />
            </Col>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardCalendar: {
    backgroundColor: '#FFFFFF',
    width: deviceWidth / 1.05,
    height: 80,
    borderRadius: 6,
    marginBottom: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
  },
});

function mapStateToProps(state) {
  return {
    dataTrain: state.train.searchTrain,
    payloadTrain: state.train.payloadTrain,
    train: state.train,
  };
}

export default connect(mapStateToProps)(SearchTrainReturn);
