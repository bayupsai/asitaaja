import React, { PureComponent } from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  FlatList,
  Text,
  Image,
  StyleSheet,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import numeral from 'numeral';
import { connect } from 'react-redux';
import {
  Placeholder,
  PlaceholderLine,
  Fade,
  PlaceholderMedia,
} from 'rn-placeholder';
import HeaderPageResults from '../../../components/HeaderPageResults';
import { actionTrainSelected } from '../../../redux/actions/TrainAction';
import CardResult from './SearchTrain/CardResult';
import { fontBold, fontReguler, asitaColor } from '../../../base/constant';
import { ButtonRounded } from '../../../elements/Button';
import DateSlider from '../../../components/CalendarSlider/index';
import moment from 'moment';

const window = Dimensions.get('window');
let deviceHeight = window.height;
let deviceWidth = window.width;

class SearchTrain extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dating: new Date(),
      departureLocation: this.props.navigation.state.params.origin.name,
      arrivalLocation: this.props.navigation.state.params.arrival.name,
      departure_code: this.props.navigation.state.params.origin.code,
      arrival_code: this.props.navigation.state.params.arrival.code,
      departureDate: this.props.payloads.departure_date,
      adult: 1,
      return: this.props.navigation.state.params.return,
      arrayDummy: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      arrayCalendar: [1],
      arrayShimerCalendar: [1, 2, 3, 4, 5, 6, 7],
      loading: this.props.loading,
      formPayload: this.props.navigation.state.params
        ? this.props.navigation.state.params.payload
        : '',
    };
  }

  searchReturn = () => {
    this.props.navigation.navigate('SearchTrainReturn');
  };
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  trainSelected = payload => {
    // const isReturn = this.props.payloads.data.return_date
    //   ? this.props.payloads.data.return_date
    //   : false;
    const isReturn = false;
    this.props.dispatch(actionTrainSelected(payload));
    if (isReturn != false) {
      this.props.navigation.navigate('SearchTrainReturn');
    } else {
      this.props.navigation.navigate('FormOrderTrain');
    }
  };

  _renderItem = ({ item, i }) =>
    this.props.loading ? (
      <View
        key={i}
        style={{
          marginTop: 25,
          marginLeft: 10,
          marginRight: 10,
          justifyContent: 'center',
          alignItems: 'flex-start',
          padding: 10,
          backgroundColor: '#fff',
        }}
      >
        {/* <ShimmerPlaceHolder autoRun={true} visible={false} height={20} width={deviceWidth / 1.10} style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }} />
                <ShimmerPlaceHolder autoRun={true} visible={false} width={100} style={{ marginTop: 20 }} />
                <ShimmerPlaceHolder autoRun={true} visible={false} width={deviceWidth / 2} style={{ marginTop: 10 }} />
                <ShimmerPlaceHolder autoRun={true} visible={false} width={deviceWidth / 1.10} height={20} style={{ marginTop: 20, borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }} /> */}
      </View>
    ) : (
      <CardResult
        {...this.props}
        key={i}
        onPress={() => this.trainSelected(item)}
        modalPress={() => this.trainSelected(item)}
        depart={this.state.arrival_code}
        arrive={this.state.departure_code}
        departureLocation={this.state.departureLocation}
        arrivalLocation={this.state.arrivalLocation}
        subClass={item.subclass}
        timedepart={item.dep_datetime.substring(13, item.dep_datetime.length)}
        timearrive={item.arv_datetime.substring(13, item.arv_datetime.length)}
        train={item.transporter_name}
        price={numeral(item.adult_fare).format('0,0')}
      />
    );

  _renderLoading = i => {
    return (
      <Placeholder
        Animation={Fade}
        style={{
          backgroundColor: asitaColor.white,
          marginTop: 20,
          borderRadius: 5,
          marginLeft: 10,
          marginRight: 10,
          justifyContent: 'center',
          alignItems: 'flex-start',
          padding: 10,
        }}
      >
        <PlaceholderLine width={80} />
        <PlaceholderLine />
        <PlaceholderLine width={30} />
      </Placeholder>
    );
  };

  loadingCalendar = index => {
    return (
      <Grid key={index} style={[styles.cardCalendar, { padding: 10 }]}>
        {this.state.arrayShimerCalendar.map((item, i) => (
          <Col
            style={{ marginLeft: 15, marginRight: 15, alignItems: 'center' }}
            key={i}
          >
            {/* <ShimmerPlaceHolder autoRun={true} visible={false} height={25} width={25} style={{paddingBottom: 5}}/>
                        <ShimmerPlaceHolder autoRun={true} visible={false} height={25} width={28} style={{paddingBottom: 10}}/>
                        <ShimmerPlaceHolder autoRun={true} visible={false} height={15} width={10} style={{paddingBottom: 5}}/> */}
          </Col>
        ))}
      </Grid>
    );
  };

  emptyResults = () => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: 30,
        }}
      >
        <View>
          <Image
            resizeMode="center"
            style={{ height: deviceHeight / 2.5, width: deviceWidth - 40 }}
            source={require('../../../assets/icons/search_not_found.png')}
          />
        </View>
        <View style={{ paddingTop: 10, alignItems: 'center' }}>
          <Text
            style={{ color: '#222222', fontFamily: fontBold, fontSize: 22 }}
          >
            No Schedules Available
          </Text>
        </View>
        <View
          style={{
            paddingTop: 20,
            paddingLeft: 30,
            paddingRight: 30,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: '#222222',
              fontFamily: fontReguler,
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            Please search for another date or you may
          </Text>
        </View>
        <View
          style={{
            paddingTop: 3,
            paddingLeft: 30,
            paddingRight: 30,
            paddingBottom: 10,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: '#222222',
              fontFamily: fontReguler,
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            click the Change Search button below
          </Text>
        </View>
        <View style={{ marginTop: 30 }}>
          <ButtonRounded
            onClick={() => this.props.navigation.goBack()}
            label="CHANGE SEARCH"
          />
        </View>
      </View>
    );
  };

  // Main Render
  render() {
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <HeaderPageResults
          page="train"
          callback={this.goBackHeader}
          train={this.state}
        />
        <View
          style={{
            backgroundColor: '#f0f0f0',
            alignItems: 'center',
            justifyContent: 'center',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
            padding: 10,
            paddingBottom: 10,
          }}
        >
          {this.props.loading == false ? (
            // || this.props.dataTrain.departures !== undefined || this.props.dataTrain.departures !== null || this.props.dataTrain.departures.length !== 0
            <Text style={{ color: '#000', fontSize: 14, fontFamily: fontBold }}>
              {this.props.dataTrain.schedule
                ? this.props.dataTrain.schedule !== null
                  ? `Select Departure Train From ${this.props.dataTrain.schedule.length} Schedules`
                  : 'No schedule available'
                : 'No schedule availables'}
            </Text>
          ) : (
            <Text />
          )}
        </View>

        <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
          <Grid>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              {this.props.loading ? (
                this.state.arrayCalendar.map((item, i) =>
                  this.loadingCalendar(i)
                )
              ) : this.props.dataTrain.schedule !== null &&
                this.props.dataTrain.schedule !== undefined ? (
                this.props.dataTrain.schedule.length > 0 ? (
                  <DateSlider type="train" {...this.props} />
                ) : null
              ) : null}
            </Col>
          </Grid>
          <Grid style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Col>
              {this.props.loading ? (
                this.state.arrayDummy.map((item, i) => this._renderLoading(i))
              ) : (
                <FlatList
                  data={
                    this.props.dataTrain.schedule !== undefined
                      ? this.props.dataTrain.schedule
                      : []
                  }
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  renderItem={this._renderItem}
                  keyExtractor={(i, index) => index.toString()}
                  ListEmptyComponent={this.emptyResults()}
                />
              )}
            </Col>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardCalendar: {
    backgroundColor: '#FFFFFF',
    width: deviceWidth / 1.05,
    height: 80,
    borderRadius: 6,
    marginBottom: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
  },
});

mapStateToProps = state => {
  const departArray = {
    departures: [],
  };
  return {
    dataTrain: state.train.searchTrain,
    payloads: state.train.payloadTrain,
    loading: state.train.fetchingTrain,
  };
};

export default connect(mapStateToProps)(SearchTrain);
