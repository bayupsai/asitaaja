import React, { PureComponent } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

//Component
import { fontBold, fontReguler } from '../../../../base/constant/index';
import { ButtonRounded } from '../../../../elements/Button';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const Header = props => {
  return (
    <Grid style={{ marginBottom: 25 }}>
      <Col>
        <View style={{ marginTop: 25 }}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.textGreen}>Your E-tiket has been issued!</Text>
          </View>
          <View
            style={{ marginTop: 15, alignItems: 'center', marginBottom: 25 }}
          >
            <Text style={styles.textLabel}>
              Check your email inbox/spam folder
            </Text>
            <Text style={styles.textLabel}>
              <Text style={{ fontFamily: fontBold }}>{props.email} </Text>to
              obtain E-tiket.
            </Text>
          </View>
          {/* <View style={styles.sectionHr}></View> */}
        </View>
        {/* <View style={{alignItems: 'center', marginTop: 15}}>
                    <View><Text style={styles.textInfo}> Or download your E-tiket here </Text></View>
                    <View style={{marginBottom: 25, marginTop: 20}}>
                        <ButtonRounded size="small" label="DOWNLOAD E-TICKET" onClick={() => alert('Download')}/>
                    </View>
                    <View style={styles.sectionHr}></View>
                </View> */}
      </Col>
    </Grid>
  );
};
export default Header;

const styles = StyleSheet.create({
  sectionHr: {
    width: 90 + '%',
    height: 0.5,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 14 },
  textGreen: { color: '#008d00', fontFamily: fontBold, fontSize: 16 },
  textInfo: { color: '#828282', fontFamily: fontReguler, fontSize: 14 },
});
