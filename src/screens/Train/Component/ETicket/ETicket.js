import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import { fontReguler, fontExtraBold } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

const EticketTrain = props => (
  <View style={styles.card}>
    <View style={styles.cardBox}>
      <Grid
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: 80,
          paddingBottom: 15,
          paddingTop: 15,
        }}
      >
        <Row>
          <Image
            source={require('../../../../assets/icons/issued-ticket.png')}
            style={styles.img}
          />
        </Row>
      </Grid>
      <Grid>
        <Row></Row>
        <Row>
          <Text style={styles.textH1}>Your e-ticket has been issued!</Text>
        </Row>
        <Row>
          <Text style={styles.textH5}>BOOKING ID</Text>
        </Row>
      </Grid>
      <Grid>
        <Row>
          <Text style={styles.textH1}>446592409</Text>
        </Row>
        <Row>
          <Text style={styles.textH5}>Jakarta - Bandung</Text>
        </Row>
        <Row>
          <Text style={styles.textH5}>1 Adult</Text>
        </Row>
      </Grid>
      <Grid>
        <Row>
          <Text style={styles.textH5}>Train</Text>
        </Row>
        <Row>
          <Text style={styles.textH1}>Cirebon Ekpress (68) | Economy (C)</Text>
        </Row>
        <Row>
          <Text style={styles.textH5}>Mon. 21 Jan 2019</Text>
        </Row>
      </Grid>
      <Grid>
        <Row>
          <Text style={styles.textH1}>Go Paperless</Text>
        </Row>
        <Row>
          <Text style={styles.textH2}>
            Show your e-ticket from your smartphone upon check-in
          </Text>
        </Row>
        <Row>
          <Text style={styles.textH2}>
            To access your e-ticket on Garuda Mall app or mobile web,
          </Text>
        </Row>
        <Row>
          <Text style={styles.textH2}>
            log in to your account using revonchaniago@gmail.com
          </Text>
        </Row>
      </Grid>
    </View>
  </View>
);

export default EticketTrain;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 5,
  },
  cardBox: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: deviceHeight / 1.2,
    borderRadius: 7,
    padding: 20,
  },
  grid: { padding: 20, alignItems: 'center', justifyContent: 'center' },
  img: { height: 150, width: 300, resizeMode: 'contain', paddingLeft: 20 },
  textH5: {
    fontSize: 14,
    color: '#6f6f6f',
    fontFamily: fontReguler,
  },
  textH2: {
    fontSize: 12,
    color: '#424242',
    fontFamily: fontReguler,
  },
  textH1: {
    fontSize: 16,
    fontFamily: fontExtraBold,
    color: '#424242',
  },
});
