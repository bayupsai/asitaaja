import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import numeral from 'numeral';

//redux
import { connect } from 'react-redux';

//Components
import { fontBold, fontReguler } from '../../../../base/constant/index';
import { TouchableOpacity } from 'react-native-gesture-handler';

const image = [
  { id: require('../../../../assets/icons/icon_train_right.png') },
  { id: require('../../../../assets/icons/icon_train_left.png') },
];

function upperCase(str) {
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

const Passenger = props => {
  let { details } = props.dataBookingTrain.data;
  let { passengers } = props.payloadBook.data;
  console.log(passengers);
  return (
    <Grid style={{ marginRight: 25, marginLeft: 25 }}>
      <Col>
        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Text style={styles.textBold}>Passenger</Text>
        </Row>
        {passengers.map((item, index) => {
          return (
            <Row key={index} style={{ marginBottom: 10 }}>
              <Col size={7}>
                <Text style={styles.textBold}>
                  {index + 1}. {item.first_name + ' ' + item.last_name}
                </Text>
              </Col>
              <Col size={3} style={{ alignItems: 'flex-end' }}>
                <Text style={styles.textLabel}>{upperCase(item.type)}</Text>
              </Col>
            </Row>
          );
        })}
        <Row style={{ marginBottom: 20 }}>
          <Col size={7}>
            <Text style={styles.textBody}>Departure Baggage</Text>
          </Col>
          <Col size={3} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textLabel}>20 kg</Text>
          </Col>
        </Row>
        <Row style={styles.sectionHr}></Row>

        {/* =================== Price ====================== */}
        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Text style={styles.textBold}>Price Departure & Return</Text>
        </Row>
        {details.map((item, index) => {
          return (
            <Row style={{ marginBottom: 20 }} key={index}>
              <Col size={1.2} style={{ marginTop: -5 }}>
                <Image
                  source={image[index].id}
                  style={{ width: 30, height: 35, resizeMode: 'center' }}
                />
              </Col>
              <Col
                size={8.8}
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}
              >
                <Text style={styles.textLabel}>
                  {upperCase(passengers[0].type)} x
                  {props.payloadBook.data.adult}
                </Text>
                <Text style={styles.textLabel}>
                  Rp {numeral(item.adult_price).format('0,0')}
                </Text>
              </Col>
            </Row>
          );
        })}
        <Row style={styles.sectionHr}></Row>
        {/* =================== Price ====================== */}

        <Row style={{ marginTop: 20 }}>
          <Text style={styles.textBold}>Total Payment</Text>
        </Row>
        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Col size={4}>
            <Text style={styles.textLabel}>Baggage Charge</Text>
          </Col>
          <Col size={6} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textLabel}>Rp 0 </Text>
          </Col>
        </Row>
        <Row style={styles.sectionHr}></Row>
        <Row style={{ marginBottom: 30, marginTop: 20 }}>
          <Col size={4}>
            <Text style={styles.textLabel}>Total Price</Text>
          </Col>
          <Col size={6} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textPrice}>
              Rp {numeral(props.dataBookingTrain.total).format('0,0')}
            </Text>
          </Col>
        </Row>
      </Col>
    </Grid>
  );
};

function mapStateToProps(state) {
  return {
    dataBookingTrain: state.train.bookTrain,
    payloadBook: state.train.payloadBook,
  };
}

export default connect(mapStateToProps)(Passenger);

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 14 },
  textBody: { color: '#828282', fontFamily: fontReguler, fontSize: 14 },
  textBold: { color: '#222222', fontFamily: fontBold, fontSize: 16 },
  textBlue: { fontSize: 14, fontFamily: fontReguler, color: '#0066b3' },
  textGreen: { color: '#008d00', fontFamily: fontBold, fontSize: 14 },
  textPrice: { color: '#a2195b', fontFamily: fontBold, fontSize: 16 },
});
