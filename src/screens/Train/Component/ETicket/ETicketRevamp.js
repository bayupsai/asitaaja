import React, { PureComponent } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontBold, fontReguler } from '../../../../base/constant/index';

//redux
import { connect } from 'react-redux';

//Component
import HeaderTrain from '../../../../components/HeaderTrain';
import SubHeaderPage from '../../../../components/SubHeaderPage';
import Header from './Header';
import Departure from './Departure';
import Return from './Return';
import Passanger from './Passenger';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

class EticketTrain extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataTrain: 'this.props.navigation.state.params.dataBookingTrain',
    };
  }

  goBack = () => {
    this.props.navigation.navigate('Tab');
  };

  render() {
    console.log(this.props.dataBookingTrain.data.details);
    let {
      departure_name,
      arrival_name,
    } = this.props.dataBookingTrain.data.details[0];
    let { data } = this.props.payloadBook;
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <HeaderTrain
          origin={departure_name}
          destination={arrival_name}
          callback={this.goBack}
          {...this.props}
        />
        <SubHeaderPage
          title="Booking ID"
          label={this.props.dataBookingTrain.partner_trxid}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          horizontal={false}
          style={styles.content}
        >
          <View>
            <Header {...this.props} email={data.passengers[0].email} />

            {/* ============ Detail Trains (Departure &/ Return) ============ */}
            <Departure {...this.props} />
            {/* ============ Detail Trains (Departure &/ Return) ============ */}

            <Passanger {...this.props} />
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataBookingTrain: state.train.bookTrain,
    payloadBook: state.train.payloadBook,
  };
}
export default connect(mapStateToProps)(EticketTrain);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    marginTop: 0,
  },
  content: {
    backgroundColor: '#FFFFFF',
    marginTop: -40,
    borderRadius: 10,
    paddingBottom: 25,
  },
  sectionHr: {
    width: 90 + '%',
    height: 0.5,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  textBold: { color: '#222222', fontFamily: fontBold, fontSize: 14 },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 14 },
  textInfo: { color: '#828282', fontFamily: fontReguler, fontSize: 16 },
  textGreen: { color: '#008d00', fontFamily: fontBold, fontSize: 16 },
});
