import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import moment from 'moment';

//redux
import { connect } from 'react-redux';

//Components
import { fontBold, fontReguler } from '../../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const TrainDirection = props => {
  let { details } = props.dataBookingTrain.data;
  return details.map((item, index) => {
    return (
      <Grid key={index} style={{ height: deviceHeight / 2.5 }}>
        <Col style={{ marginLeft: 25, marginRight: 25 }}>
          <Grid>
            <Col style={styles.sectionHr}></Col>
          </Grid>
          <Grid style={{ flex: 0, marginBottom: 25, marginTop: 20 }}>
            <Col style={{ alignItems: 'flex-start' }}>
              <Text style={styles.textOrange}>
                {index == 0 ? 'Departure' : 'Return'} Train
              </Text>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <Text style={styles.textBold}>
                {moment(item.date).format('ddd, D MMM')}
              </Text>
            </Col>
          </Grid>
          <Grid style={{ flex: 0 }}>
            <Col size={1.5} style={{ alignItems: 'flex-start' }}>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>{item.departure_time}</Text>
                  <Text style={styles.textInfo}>
                    {moment(item.departure_date).format('D MMM')}
                  </Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>2h 5m</Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>{item.arrival_time}</Text>
                  <Text style={styles.textInfo}>
                    {moment(item.arrival_date).format('D MMM')}
                  </Text>
                </Col>
              </Grid>
            </Col>
            <Col size={1} style={{ alignItems: 'center' }}>
              <Icon
                size={18}
                color="#c1c1c1"
                name="ios-radio-button-off"
                type="ionicon"
              />
              <View style={styles.gridRow}></View>
              <Icon
                name="ios-radio-button-on"
                color="#c1c1c1"
                size={18}
                type="ionicon"
              />
            </Col>
            <Col size={7.5} style={{ alignItems: 'flex-start' }}>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>
                    {item.departure_name} ({item.departure_code})
                  </Text>
                  <Text style={styles.textInfo}>{item.departure_name}</Text>
                </Col>
              </Grid>
              <Grid style={styles.colStart}>
                <Col
                  size={3}
                  style={{ justifyContent: 'flex-start', marginTop: -15 }}
                >
                  <Image
                    style={{ width: 50, height: 50 }}
                    resizeMode="contain"
                    source={require('../../../../assets/logos/train.png')}
                  />
                </Col>
                <Col size={7}>
                  <View>
                    <Text style={styles.textItinerary}>{item.name}</Text>
                    <Text style={styles.textItinerary}>{item.seat}</Text>
                  </View>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>
                    {item.arrival_name} ({item.arrival_code})
                  </Text>
                  <Text style={styles.textInfo}>{item.arrival_name}</Text>
                </Col>
              </Grid>
            </Col>
          </Grid>
          <Grid>
            <Col style={styles.sectionHr}></Col>
          </Grid>
        </Col>
      </Grid>
    );
  });
};

function mapStateToProps(state) {
  return {
    dataBookingTrain: state.train.bookTrain,
  };
}

export default connect(mapStateToProps)(TrainDirection);

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  gridRow: {
    height: 60 + '%',
    width: 0.8,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  colStart: { justifyContent: 'center', alignItems: 'flex-start' },
  textItinerary: {
    fontFamily: fontReguler,
    color: '#222222',
    fontSize: 14,
    marginLeft: 3,
  },
  textBold: { color: '#222222', fontFamily: fontBold, fontSize: 14 },
  textOrange: { color: '#a2195b', fontFamily: fontBold, fontSize: 16 },
  textInfo: { color: '#828282', fontFamily: fontReguler, fontSize: 14 },
});
