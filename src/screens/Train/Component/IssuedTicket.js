import React, { PureComponent } from 'react';
import { View, Dimensions, ScrollView, Text, StyleSheet } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';

// COMPONENT
import HeaderPage from '../../../components/HeaderPage';
import { Button } from '../../../elements/Button';
import Eticket from './ETicket/ETicket';
import { fontReguler, thameColors } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

export default class SearchTrain extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      // dating: new Date(),
      // data: [
      //     { id: 0, depart: "GMBR", arrive: "BDG", timedepart:' 09:40', timearrive: '13:15', train: 'Argo Parahyangan', price: '110.000' },
      //     { id: 1, depart: "GMBR", arrive: "BDG", timedepart:' 09:40', timearrive: '13:10', train: 'Argo Parahyangan', price: '150.000' },
      //     { id: 2, depart: "GMBR", arrive: "BDG", timedepart:' 07:35', timearrive: '13:15', train: 'Connecting Trains', price: '155.000' },
      // ]
    };
  }

  // searchReturn = () => { this.props.navigation.navigate('SearchTrainReturn') }
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <HeaderPage title="Train" callback={this.goBackHeader} />
        <ScrollView horizontal={false}>
          <Grid style={{ paddingBottom: 20, paddingTop: 20 }}>
            <Eticket />
          </Grid>

          <Grid style={styles.card}>
            <Col style={styles.cardBox}>
              <Row style={{ paddingLeft: 20, paddingTop: 20 }}>
                <Text style={styles.textDownload}>
                  Have you got cheap hotel or a car rental around Bandung Area?
                </Text>
              </Row>
              <Row style={{ padding: 20 }}>
                <Col size={4}>
                  <Row style={styles.colButton}>
                    <Text style={styles.textButton}>See Car Rental</Text>
                  </Row>
                </Col>
                <Col size={4}>
                  <Row style={styles.colButton}>
                    <Text style={styles.textButton}>See Hotel</Text>
                  </Row>
                </Col>
                <Col size={2}></Col>
              </Row>
            </Col>
          </Grid>

          <Grid style={styles.grid}>
            <Row>
              <Text style={styles.textDownload}>
                or download your e-ticket here
              </Text>
            </Row>
          </Grid>

          <Grid>
            <Button
              label="Download E-Ticket"
              onClick={() => alert('Download E-Ticket')}
            />
          </Grid>

          <Grid style={styles.grid}>
            <Row>
              <Text style={styles.textBooking}>
                see departure booking details
              </Text>
            </Row>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 5,
  },
  cardBox: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: deviceHeight / 5,
    borderRadius: 7,
  },
  grid: { padding: 20, alignItems: 'center', justifyContent: 'center' },
  textDownload: { fontSize: 14, color: '#424242', fontFamily: fontReguler },
  textBooking: {
    fontSize: 14,
    color: thameColors.primary,
    fontFamily: fontReguler,
  },
  textButton: {
    fontSize: 14,
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: fontReguler,
  },
  colButton: {
    borderRadius: 5,
    height: 35,
    width: 120,
    backgroundColor: thameColors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 1,
    paddingRight: 1,
  },
});
