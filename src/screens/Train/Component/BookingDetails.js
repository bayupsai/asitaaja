import React, { PureComponent } from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { DotIndicator } from 'react-native-indicators';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import numeral from 'numeral';

//redux
import { connect } from 'react-redux';
import { actionBookTrain } from '../../../redux/actions/TrainAction';

// COMPONENT
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import { Button } from '../../../elements/Button';
import DepartureDetails from './Booking/DepartureDetails';
import ReturnDetails from './Booking/ReturnDetails';
import PersonaInformation from './Booking/Personalnformation';
import Passenger from './Booking/Passenger';
import Price from './Booking/PriceBooking';
import Insurance from './Booking/Insurance';
import SeatNumber from './Booking/SeatNumber';
import AlertModal from '../../../components/AlertModal';

//Modalin dong...
import ModalContact from './Booking/ModalContact';
import ModalPassenger from './Booking/ModalPassenger';
import ModalSatuation from './Booking/ModalSatuation';
import {
  fontExtraBold,
  fontReguler,
  thameColors,
} from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
let deviceWidth = window.width;

class BookingDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dating: new Date(),
      adult: this.props.payloadTrain.data.adult,
      visibleModal: null,
      messageAlert: '',
      date: null,
      focus: 'start_date',
      listTitle: [{ title: 'Mr' }, { title: 'Mrs' }, { title: 'Ms' }],

      //from screen before
      dataTrainDepartures: this.props.navigation.state.params
        .dataTrainDepartures,
      dataTrainReturns: this.props.navigation.state.params.dataTrainReturns,
      departure_code: this.props.navigation.state.params.departure_code,
      departure_name: this.props.navigation.state.params.departure_name,
      arrival_code: this.props.navigation.state.params.arrival_code,
      arrival_name: this.props.navigation.state.params.arrival_name,
      return: this.props.navigation.state.params.return,

      price: '',
      insurance: false,

      //dataContact
      fullnameContact: '',
      phoneContact: '',
      emailContact: '',

      //dataPassenger
      dataPassenger: [
        {
          satuation: '',
          fullnamePassenger: '',
          emailPassenger: '',
          phonePassenger: '',
          birth_date: '',
          id_card: '',
        },
      ],
      satuation: '',
      fullnamePassenger: '',
      emailPassenger: '',
      phonePassenger: '',
      birth_date: '',
      id_card: '',
    };
  }

  searchReturn = () => {
    this.props.navigation.navigate('SearchTrainReturn');
  };
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  //modal
  resetModal = () => {
    this.setState({ visibleModal: null });
  };
  satuation = i => {
    this.setState({ visibleModal: i });
  };
  _alertModal = (idModal, message) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visibleModal === idModal}
        title="Alert"
        contentText={message}
        onPress={() => this.resetModal()}
        onDismiss={() => this.resetModal()}
      />
    );
  };
  //modal
  handleInput = (type, data) => {
    if (type == 'fullNameContact') {
      this.setState({ fullnameContact: data });
    } else if (type == 'phoneNumberContact') {
      this.setState({ phoneContact: data });
    } else if (type == 'emailContact') {
      this.setState({ emailContact: data });
    }
  };
  changeContact = () => {
    console.log('data passeenger');
    console.log(this.state);
    if (
      this.state.fullnameContact == '' ||
      this.state.phoneContact == '' ||
      this.state.emailContact == ''
    ) {
      this.setState({ visibleModal: 91 });
      // Alert.alert(
      //     'Warning',
      //     'Please enter data correctly!',
      //     [
      //         { text: 'OK', onPress: () => console.log('OK Pressed') },
      //     ],
      //     { cancelable: false },
      // );
    } else {
      this.setState({ visibleModal: null });
    }
  };
  changePassenger = () => {
    console.log('data passeenger');
    console.log(this.state);
    if (
      this.state.fullnamePassenger == '' ||
      this.state.phonePassenger == '' ||
      this.state.emailPassenger == '' ||
      this.state.birth_date == ''
    ) {
      this.setState({ visibleModal: 91 });
      // Alert.alert(
      //     'Warning',
      //     'Please enter data correctly!',
      //     [
      //         { text: 'OK', onPress: () => console.log('OK Pressed') },
      //     ],
      //     { cancelable: false },
      // );
    } else {
      this.setState({ visibleModal: null });
    }
  };

  insuracePress = () => {
    this.setState({ insurance: !this.state.insurance });
  };

  payment = () => {
    let splitName = this.state.fullnamePassenger.split(' ', 2);
    let payloadData = {
      command: 'BOOKING',
      product: 'KERETA',
      partner_trxid: 'ga-mall-023003003002',
      data: {
        departure_code: this.state.departure_code,
        arrival_code: this.state.arrival_code,
        date: moment(this.state.dataTrainDepartures.departure_date).format(
          'YYYY-MM-DD'
        ),
        adult: this.state.adult,
        schedule_id: this.state.dataTrainDepartures.schedule_id,
        class: this.state.dataTrainDepartures.class,
        sub_class: this.state.dataTrainDepartures.sub_class,
        passengers: [
          {
            first_name: splitName[0],
            last_name: splitName[1],
            email: this.state.emailPassenger,
            phone: this.state.phonePassenger,
            primary: true,
            salutation: this.state.satuation,
            type: 'adult',
            birth_date: this.state.birth_date,
            id_card_number: this.state.id_card,
          },
        ],
      },
    };
    // let payloadData = {
    //     command: "BOOKING"
    //     , product: "KERETA"
    //     , data: {
    //         departure_code: this.state.departure_code
    //         , arrival_code: this.state.arrival_code
    //         , departure_date: moment(this.state.dataTrainDepartures.departure_date).format("YYYY-MM-DD")
    //         , adult: 1
    //         , class: this.state.dataTrainDepartures.class
    //         , schedule_id: this.state.dataTrainDepartures.schedule_id
    //         , sub_class: this.state.dataTrainDepartures.sub_class
    //         , passengers: [{
    //             salutation: this.state.satuation
    //             , first_name: splitName[0]
    //             , last_name: splitName[1]
    //             , email: this.state.emailPassenger
    //             , phone: this.state.phonePassenger
    //             , birth_date: this.state.birth_date
    //             , type: "adult"
    //             , id_card_number: this.state.id_card
    //             , primary: true
    //         }
    //         ]
    //     }
    // }
    this.props
      .dispatch(actionBookTrain(payloadData))
      .then(res => {
        console.log(res);
        if (res.type === '"BOOK_TRAIN_SUCCESS"') {
          this.setState({ visibleModal: 92, messageAlert: res.type });
          // alert(res.type)
        } else {
          this.setState({ visibleModal: 92, messageAlert: res.errorMessage });
          // alert(res.errorMessage)
        }

        // alert(JSON.stringify(this.props.dataBook))
        // this.props.dataBook.length !== 0 ? (
        //     alert(JSON.stringify(this.props.dataBook))
        // ) : (
        //         alert(`${this.props.dataBook.error_msg}`)
        //     )
      })
      .catch(err => {
        console.log(err);
        this.setState({ visibleModal: 92, messageAlert: err });
        // alert('Error : ' + err)
      });
  };

  render() {
    const isDateBlocked = date => date.isAfter(moment(), 'day');
    let name = this.state.fullnameContact.split(' ', 2);
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        {/* Alert Modal */}
        {this._alertModal(91, 'Please enter data correctly')}
        {this._alertModal(92, this.state.messageAlert)}
        {/* Alert Modal */}

        <HeaderPage title="Train" callback={this.goBackHeader} />
        <ScrollView horizontal={false}>
          <SubHeaderPage title="Booking Details" />
          <Grid
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: -10,
            }}
          >
            <DepartureDetails
              trainName={this.state.dataTrainDepartures.name}
              subClass={this.state.dataTrainDepartures.sub_class}
              departureDate={this.state.dataTrainDepartures.departure_date}
              departureTime={this.state.dataTrainDepartures.departure_time}
              arrivalDate={this.state.dataTrainDepartures.arrival_date}
              arrivalTime={this.state.dataTrainDepartures.arrival_time}
              departureName={this.state.departure_name}
              arrivalName={this.state.arrival_name}
            />
          </Grid>
          {this.state.return == true ? (
            <Grid
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: -10,
              }}
            >
              <ReturnDetails
                trainName={this.state.dataTrainReturns.name}
                subClass={this.state.dataTrainReturns.sub_class}
                departureDate={this.state.dataTrainReturns.departure_date}
                departureTime={this.state.dataTrainReturns.departure_time}
                arrivalDate={this.state.dataTrainReturns.arrival_date}
                arrivalTime={this.state.dataTrainReturns.arrival_time}
                departureName={this.state.arrival_name}
                arrivalName={this.state.departure_name}
              />
            </Grid>
          ) : (
            <Text></Text>
          )}

          {/* <Grid style={{ paddingTop: 20 }}>
                        <Row style={{ paddingLeft: 20, paddingBottom: -10 }}><Text style={{ color: '#424242', fontSize: 16, fontWeight: '600' }}>Contact Information (for E-Ticket)</Text></Row>
                        <PersonaInformation
                            fullnameContact={this.state.fullnameContact == "" ? "Insert Data, Please..." : this.state.fullnameContact}
                            onPress={() => this.setState({ visibleModal: 1 })}
                        />
                    </Grid> */}

          <Grid style={{ paddingTop: 10, paddingTop: -20 }}>
            <Row style={{ paddingLeft: 20, paddingBottom: -10 }}>
              <Text
                style={{
                  color: '#424242',
                  fontSize: 16,
                  fontFamily: fontExtraBold,
                }}
              >
                Passenger Details
              </Text>
            </Row>
            <Passenger
              fullnamePassenger={
                this.state.fullnamePassenger == ''
                  ? 'Insert Data Passenger, Please...'
                  : this.state.fullnamePassenger
              }
              onPress={() => this.setState({ visibleModal: 2 })}
            />
            {this.state.fullnamePassenger &&
            this.state.satuation &&
            this.state.emailPassenger &&
            this.state.phonePassenger &&
            this.state.birth_date &&
            this.state.id_card !== '' ? (
              <Text></Text>
            ) : (
              <Row style={{ paddingLeft: 20, paddingTop: -30 }}>
                <Text style={{ color: 'red', fontFamily: fontReguler }}>
                  Field is Required *
                </Text>
              </Row>
            )}
          </Grid>

          <Grid style={{ paddingTop: 30 }}>
            <Row style={{ paddingLeft: 20, paddingBottom: -10 }}>
              <Text
                style={{
                  color: '#424242',
                  fontSize: 16,
                  fontFamily: fontExtraBold,
                }}
              >
                Important
              </Text>
            </Row>
            <Insurance
              onPress={this.insuracePress}
              icon={this.state.insurance}
            />
          </Grid>

          <Grid style={{ paddingTop: 20 }}>
            <Row style={{ paddingLeft: 20, paddingBottom: -10 }}>
              <Text
                style={{
                  color: '#424242',
                  fontSize: 16,
                  fontFamily: fontExtraBold,
                }}
              >
                Seat Number
              </Text>
            </Row>
            <SeatNumber />
          </Grid>

          <Grid style={{ paddingTop: 10 }}>
            <Price
              {...this.props}
              {...this.state}
              train_name_departure={this.state.dataTrainDepartures.name}
              sub_class_departure={this.state.dataTrainDepartures.sub_class}
              price_depart={this.state.dataTrainDepartures.price_adult}
              // train_name_return={this.state.dataTrainReturns.name}
              // sub_class_return={this.state.dataTrainReturns.sub_class}
              // price_return={this.state.dataTrainReturns.price_adult}
              return={
                this.state.return == true ? (
                  <Row style={{ paddingTop: -10 }}>
                    <Col size={6}>
                      <Row>
                        <Text
                          style={{
                            color: '#424242',
                            fontSize: 14,
                            fontFamily: fontExtraBold,
                          }}
                        >
                          Return Train
                        </Text>
                      </Row>
                      <Row>
                        <Text
                          style={{
                            fontSize: 14,
                            color: '#424242',
                            fontFamily: fontReguler,
                          }}
                        >
                          {this.state.dataTrainReturns.name}
                        </Text>
                      </Row>
                      <Row style={{ alignItems: 'flex-end' }}>
                        <Text
                          style={{
                            fontSize: 14,
                            color: '#424242',
                            fontFamily: fontReguler,
                          }}
                        >
                          (Sub-Class {this.state.dataTrainReturns.sub_class})
                        </Text>
                      </Row>
                    </Col>
                    <Col
                      size={4}
                      style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                      }}
                    >
                      <Row></Row>
                      <Row>
                        <Text
                          style={{
                            color: '#424242',
                            fontSize: 16,
                            fontFamily: fontReguler,
                          }}
                        >
                          Rp
                          {numeral(
                            this.state.dataTrainReturns.price_adult
                          ).format('0,0')}
                        </Text>
                      </Row>
                      <Row></Row>
                    </Col>
                  </Row>
                ) : (
                  <Row style={{ paddingTop: -10 }}>
                    <Col size={6}>
                      <Row>
                        <Text
                          style={{
                            color: '#424242',
                            fontSize: 14,
                            fontFamily: fontExtraBold,
                          }}
                        >
                          Return Train
                        </Text>
                      </Row>
                      <Row>
                        <Text>-</Text>
                      </Row>
                    </Col>
                    <Col size={4}>
                      <Row></Row>
                      <Row>
                        <Text>-</Text>
                      </Row>
                    </Col>
                  </Row>
                )
              }
              totalPrice={
                this.state.return == true
                  ? this.state.dataTrainDepartures.price_adult +
                    this.state.dataTrainReturns.price_adult
                  : this.state.dataTrainDepartures.price_adult
              }
            />
          </Grid>

          <Grid style={styles.cardBox}>
            <Col style={{ padding: 15 }}>
              <Row>
                <Text style={styles.textBody}>
                  By clicking this button, you acknowledfe that you
                </Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>
                  have read and agreed to the{' '}
                  <Text style={styles.textDate}>Terms & Conditions</Text>
                </Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>
                  and <Text style={styles.textDate}>Privacy Policy </Text>of
                  Garuda Mall
                </Text>
              </Row>
            </Col>
          </Grid>

          <Grid style={{ paddingTop: 20, paddingBottom: 50 }}>
            {this.props.loadBook ? (
              <DotIndicator />
            ) : this.state.fullnamePassenger &&
              this.state.satuation &&
              this.state.emailPassenger &&
              this.state.phonePassenger &&
              this.state.birth_date &&
              this.state.id_card !== '' ? (
              <Button label="Continue Payment" onClick={this.payment} />
            ) : (
              <Button
                label="Continue Payment"
                onClick={() => console.log('nothing')}
                isActive={false}
              />
            )}
          </Grid>
        </ScrollView>

        {/* Modal */}
        {/* <ModalContact
                    visibleModal={this.state.visibleModal}
                    closeModal={this.resetModal}
                    fullnameContact={this.state.fullnameContact}
                    phoneContact={this.state.phoneContact}
                    emailContact={this.state.emailContact}
                    changeFullnameContact={(i) => this.setState({ fullnameContact: i })}
                    changePhoneContact={(i) => this.setState({ phoneContact: i })}
                    changeEmailContact={(i) => this.setState({ emailContact: i })}
                    changeContact={this.changeContact}
                /> */}
        <ModalPassenger
          onDismiss={this.resetModal}
          visibleModal={this.state.visibleModal}
          closeModal={this.resetModal}
          clickSatuation={() => this.setState({ visibleModal: 3 })}
          satuation={this.state.satuation == 0 ? 'Mr.' : this.state.satuation}
          fullnamePassenger={this.state.fullnamePassenger}
          phonePassenger={this.state.phonePassenger}
          emailPassenger={this.state.emailPassenger}
          birthDate={this.state.birth_date}
          id_card={this.state.id_card}
          changeFullnamePassenger={i => this.setState({ fullnamePassenger: i })}
          changePhonePassenger={i => this.setState({ phonePassenger: i })}
          changeEmailPassenger={i => this.setState({ emailPassenger: i })}
          changeBirthDate={i => this.setState({ birth_date: i })}
          changePassenger={this.changePassenger}
          changeIdNumber={i => this.setState({ id_card: i })}
        />
        <ModalSatuation
          onDismiss={this.resetModal}
          visibleModal={this.state.visibleModal}
          closeModal={() => this.setState({ visibleModal: 2 })}
          content={this.state.listTitle.map((item, i) => {
            return (
              <TouchableOpacity
                key={i}
                style={{
                  flex: 1,
                  backgroundColor: '#FFFFFF',
                  padding: 10,
                  borderBottomColor: '#d5d5d5',
                  borderBottomWidth: 0.5,
                  flexDirection: 'row',
                }}
                style={{ flex: 1 }}
                onPress={() =>
                  this.setState({ satuation: item.title, visibleModal: 2 })
                }
              >
                <Text
                  style={{ fontWeight: '400', color: '#000', fontSize: 16 }}
                >
                  {item.title}
                </Text>
              </TouchableOpacity>
            );
          })}
        />
        {/* Modal */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataTrain: state.train.searchTrain,
    dataBook: state.train.bookTrain,
    loadBook: state.train.fetchingBook,
    payloadBook: state.train.payloadBook,
    payloadTrain: state.train.payloadTrain,
    failedBook: state.train.failedBook,
  };
}

export default connect(mapStateToProps)(BookingDetails);

const styles = StyleSheet.create({
  cardBox: {
    width: deviceWidth / 1.1,
    height: 90,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    backgroundColor: '#fff',
    borderRadius: 5,
  },
  textDate: { fontSize: 14, color: '#0066b3', fontFamily: fontReguler },
  textBody: { fontSize: 14, color: '#424242', fontFamily: fontReguler },

  //modal
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
