import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import Dash from 'react-native-dash';
import numeral from 'numeral';
import DisplayModal from '../SearchTrain/DisplayModal';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  thameColors,
  asitaColor,
} from '../../../../base/constant';

class CardResult extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  modalPress = modalin => {
    this.setState({ modalVisible: modalin });
    this.props.modalPress();
  };

  render() {
    return (
      <View {...this.props}>
        <TouchableOpacity onPress={this.props.onPress} style={[styles.card]}>
          <DisplayModal
            {...this.props}
            display={this.state.modalVisible}
            close={() => {
              this.handleModal(!this.state.modalVisible);
            }}
            modalin={() => {
              this.handleModal(!this.state.modalVisible);
            }}
            departureTime={this.props.timedepart}
            departureLocation={this.props.departureLocation}
            departure_code={this.props.depart}
            arrivalTime={this.props.timearrive}
            arrivalLocation={this.props.arrivalLocation}
            arrival_code={this.props.arrive}
            modalPress={() => this.modalPress(!this.state.modalVisible)}
          />
          <Grid style={styles.cardBody}>
            <Col style={{ justifyContent: 'center' }}>
              <Text
                style={{ color: '#222222', fontFamily: fontBold, fontSize: 14 }}
              >
                {this.props.train}
              </Text>
            </Col>
          </Grid>
          <Grid style={styles.cardBody}>
            <Col size={2} style={[styles.col, { alignItems: 'flex-start' }]}>
              <Text style={styles.textTime}>{this.props.timearrive}</Text>
              <Text style={styles.textDepart}>{this.props.arrive}</Text>
            </Col>
            <Col size={5} style={[styles.col]}>
              <Row
                style={{
                  marginLeft: -15,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 5,
                }}
              >
                <Col size={4}>
                  <View style={{ alignItems: 'flex-end' }}>
                    <Dash
                      dashColor="#dadada"
                      style={{ width: 40, height: 0.8 }}
                    />
                  </View>
                </Col>
                <Col
                  size={2}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: -15,
                  }}
                >
                  <Image
                    source={require('../../../../assets/icons/icon_trains.png')}
                    style={{
                      width: 25,
                      height: 20,
                      marginBottom: -13,
                      tintColor: '#222222',
                    }}
                    resizeMode="center"
                  />
                </Col>
                <Col size={4}>
                  <View style={{ alignItems: 'flex-start' }}>
                    <Dash
                      dashColor="#dadada"
                      style={{ width: 40, height: 0.8 }}
                    />
                  </View>
                </Col>
              </Row>
              <Row>
                <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontFamily: fontReguler, fontSize: 12 }}>
                    2h 5m direct
                  </Text>
                </Col>
              </Row>
            </Col>
            <Col size={2} style={styles.col}>
              <Text style={styles.textTime}>{this.props.timedepart}</Text>
              <Text style={styles.textDepart}>{this.props.depart}</Text>
            </Col>
            <Col size={1} style={[styles.col, { marginTop: -20 }]}>
              <TouchableOpacity
                onPress={() => {
                  this.handleModal(true);
                }}
                style={styles.icon}
              >
                <Icon
                  name="ios-arrow-down"
                  size={25}
                  color={asitaColor.orange}
                />
              </TouchableOpacity>
            </Col>
          </Grid>

          <Grid style={styles.cardBody}>
            <Col
              size={4}
              style={{ justifyContent: 'center', alignItems: 'flex-start' }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color: '#838383',
                  fontFamily: fontReguler,
                }}
              >
                Sub Class ({this.props.subClass}){' '}
              </Text>
            </Col>
            <Col
              size={6}
              style={{ alignItems: 'flex-end', justifyContent: 'center' }}
            >
              <Text
                style={{
                  fontSize: 14,
                  color: thameColors.secondary,
                  fontFamily: fontReguler,
                }}
              >
                Available
              </Text>
            </Col>
          </Grid>

          <Grid style={[styles.cardBody, { marginBottom: 10 }]}>
            <Col size={4}>
              <Image
                style={{ width: 60, height: 40 }}
                resizeMode="center"
                source={require('../../../../assets/logos/train.png')}
              />
            </Col>
            <Col
              size={6}
              style={{ alignItems: 'flex-end', justifyContent: 'center' }}
            >
              <Text style={styles.prices}>
                Rp.{' '}
                {numeral(this.props.price)
                  .format('0,00')
                  .replace(/,/g, '.')}
              </Text>
            </Col>
          </Grid>
        </TouchableOpacity>
      </View>
    );
  }
}

export default CardResult;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    flex: 1,
    height: 'auto',
    borderRadius: 5,
    margin: 10,
  },
  cardBody: { marginLeft: 15, marginTop: 10, marginRight: 10 },
  cardFooter: {
    paddingTop: 0,
    paddingBottom: 0,
    padding: 10,
    borderTopWidth: 0.25,
    borderTopColor: '#e0e0e0',
  },
  prices: { color: asitaColor.orange, fontFamily: fontBold, fontSize: 18 },
  dash: { width: 150, height: 0.5 },
  col: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTime: { fontFamily: fontExtraBold, color: '#222222', fontSize: 14 },
  textDepart: {
    borderRadius: 5,
    fontFamily: fontBold,
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
});
