import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import { fontReguler, fontBold } from '../../../../base/constant';
import { Icon } from 'react-native-elements';

// const yester3 = moment(new Date()).subtract(3, 'days').format("D ddd")
// const currentDay = moment(new Date()).format("D ddd")
// const tomorr1 = moment(new Date()).add(1, 'days').format("D ddd")
// const tomorr2 = moment(new Date()).add(2, 'days').format("D ddd")
const toMonth = moment(new Date())
  .format('MMM')
  .toUpperCase();

let dateDay = [
  {
    id: 1,
    date: moment(new Date()).format('D'),
    day: moment(new Date())
      .add(0, 'days')
      .format('ddd'),
  },
  {
    id: 2,
    date: moment(new Date())
      .add(1, 'days')
      .format('D'),
    day: moment(new Date())
      .add(1, 'days')
      .format('ddd'),
  },
  {
    id: 3,
    date: moment(new Date())
      .add(2, 'days')
      .format('D'),
    day: moment(new Date())
      .add(2, 'days')
      .format('ddd'),
  },
  {
    id: 4,
    date: moment(new Date())
      .add(3, 'days')
      .format('D'),
    day: moment(new Date())
      .add(3, 'days')
      .format('ddd'),
  },
  {
    id: 5,
    date: moment(new Date())
      .add(4, 'days')
      .format('D'),
    day: moment(new Date())
      .add(4, 'days')
      .format('ddd'),
  },
  {
    id: 6,
    date: moment(new Date())
      .add(5, 'days')
      .format('D'),
    day: moment(new Date())
      .add(5, 'days')
      .format('ddd'),
  },
  {
    id: 7,
    date: moment(new Date())
      .add(6, 'days')
      .format('D'),
    day: moment(new Date())
      .add(6, 'days')
      .format('ddd'),
  },
];

export default class Calendar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      buttonColor: '#979797',
      selectedItems: [1],
    };
  }

  datingNow = () => {
    moment().format('MMMM Do YYYY, h:mm:ss a');
  };

  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginLeft: 20,
          marginRight: 15,
          alignItems: 'center',
        }}
      >
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {/* <Text style={styles.month}>{toMonth}</Text> */}
          {dateDay.map((item, key) => (
            <TouchableOpacity
              onPress={() => this.setState({ selectedItems: item.id })}
              key={key}
            >
              {this.state.selectedItems == item.id ? (
                <View style={styles.today}>
                  <Text style={styles.textDateActive}>{item.date}</Text>
                  <Text style={styles.textDayActive}>{item.day}</Text>
                  <Icon
                    name="dot-single"
                    type="entypo"
                    size={25}
                    color="#a2195b"
                  />
                </View>
              ) : (
                <View style={styles.day}>
                  <Text style={styles.textDate}>{item.date}</Text>
                  <Text style={styles.textDay}>{item.day}</Text>
                  <Icon
                    name="dot-single"
                    type="entypo"
                    size={25}
                    color="#ffffff"
                  />
                </View>
              )}
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sectionMont: {
    backgroundColor: '#f6f6f6',
  },
  month: {
    color: '#767676',
    fontFamily: fontBold,
    transform: [{ rotate: '-90deg' }, { translateY: 15 }],
    fontSize: 14,
    paddingBottom: 15,
    fontFamily: fontReguler,
  },
  day: {
    alignItems: 'center',
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
    // backgroundColor: '#FFF',
    // fontSize: 14,
    // borderRadius: 10,
  },
  today: {
    alignItems: 'center',
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
    // fontSize: 14,
    // borderBottomColor: "#ed6d00",
    // borderBottomWidth: 2,
  },
  textDate: { fontFamily: fontBold, color: '#222222', fontSize: 16 },
  textDateActive: { fontFamily: fontBold, color: '#a2195b', fontSize: 16 },
  textDay: { fontFamily: fontReguler, color: '#222222' },
  textDayActive: { fontFamily: fontBold, color: '#a2195b' },
});
