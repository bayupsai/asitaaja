import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontReguler,
  thameColors,
} from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

const SubHeaderOnly = props => (
  <View style={styles.headerContainer}>
    <ImageBackground
      source={require('../../../../assets/icons/maps.png')}
      style={styles.imageBackground}
    >
      <Grid style={{ marginBottom: 60, marginTop: 50 }}>
        <Col size={3.5}>
          <Row>
            <Text style={styles.textH5}>Origin From</Text>
          </Row>
          <Row>
            <Text style={styles.textH1}>{props.origin}</Text>
          </Row>
          <Row>
            <Text style={styles.textH2}>{props.departure_name}</Text>
          </Row>
        </Col>
        <Col
          size={3}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <Row></Row>
          <Row style={{ paddingRight: 50 }}>
            <Image
              source={require('../../../../assets/icons/return.png')}
              style={{
                height: 25,
                width: 200,
                resizeMode: 'center',
                paddingRight: 60,
              }}
            />
          </Row>
          <Row></Row>
        </Col>
        <Col size={3.5}>
          <Row>
            <Text style={styles.textH5}>Arrival At</Text>
          </Row>
          <Row>
            <Text style={styles.textH1}>{props.arrival}</Text>
          </Row>
          <Row>
            <Text style={styles.textH2}>{props.arrival_name}</Text>
          </Row>
        </Col>
      </Grid>
    </ImageBackground>
  </View>
);

export default SubHeaderOnly;

const styles = StyleSheet.create({
  textH5: {
    fontSize: 14,
    color: '#fff',
    fontFamily: fontExtraBold,
  },
  textH2: {
    fontSize: 14,
    color: '#fff',
    fontFamily: fontReguler,
  },
  textH1: {
    fontSize: 20,
    fontFamily: fontExtraBold,
    color: '#fff',
  },
  dash: {
    width: 1,
    height: deviceHeight / 6,
    flexDirection: 'column',
    transform: [{ translateY: 0 }, { translateX: 0 }],
  },
  imageBackground: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: deviceWidth,
    height: deviceHeight / 3.5,
  },

  circleDeparture: {
    flex: 0,
    marginLeft: -3,
    marginBottom: 4,
  },
  circleDestination: {
    flex: 0,
    marginLeft: -3,
    marginTop: 0,
  },
  departureTitle: {
    color: '#decbfb',
    fontSize: 14,
    fontWeight: '500',
    paddingBottom: 2,
  },
  departureCode: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  departureCity: {
    color: '#ffffff',
    fontSize: 14,
    fontWeight: '400',
  },
  headerContainer: {
    backgroundColor: thameColors.primary,
    height: deviceHeight / 3.8,
    borderBottomLeftRadius: 50,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 20,
    paddingTop: 0,
  },
  headerContent: {
    paddingLeft: 50,
  },
});
