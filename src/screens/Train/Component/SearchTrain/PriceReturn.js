import React, { PureComponent } from 'react';
import { Text, StyleSheet, Dimensions, Image } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontExtraBold, thameColors } from '../../../../base/constant';

const window = Dimensions.get('window');
var deviceWidth = window.width;

export default class Calendar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      buttonColor: '#979797',
      selectedItems: [1],
    };
  }

  render() {
    return (
      <Grid style={styles.content}>
        <Row style={styles.cardBox}>
          <Col size={2} style={{ justifyContent: 'flex-start' }}>
            <Image
              source={require('../../../../assets/icons/train.png')}
              style={styles.img}
            />
          </Col>
          <Col size={6}>
            <Text style={styles.textBody}>Cirebon Express</Text>
            <Text style={styles.textPrice}>IDR 1.250.000</Text>
          </Col>
          <Col size={2} style={styles.colButton}>
            <Row style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={styles.textButton}>Ubah</Text>
            </Row>
          </Col>
        </Row>
      </Grid>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    paddingBottom: -10,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    flex: 0,
    padding: 5,
    transform: [{ translateY: -10 }],
  },
  cardBox: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 60,
    borderRadius: 7,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  textButton: {
    fontSize: 14,
    color: '#fff',
    fontFamily: fontExtraBold,
    justifyContent: 'center',
  },
  colButton: {
    borderRadius: 10,
    height: 30,
    width: 80,
    backgroundColor: thameColors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 2,
    paddingRight: 2,
  },
  img: {
    height: 40,
    width: 60,
    resizeMode: 'contain',
    tintColor: '#23900a',
    paddingLeft: 20,
  },
  textBody: {
    fontSize: 14,
    color: '#424242',
    fontFamily: fontExtraBold,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  textPrice: {
    color: '#ed6d00',
    fontFamily: fontExtraBold,
    fontSize: 14,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});
