import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Calendar from './Calendar';

const window = Dimensions.get('window');
let deviceWidth = window.width;

const DateScroll = props => (
  <View style={styles.card}>
    <Calendar />
  </View>
);

export default DateScroll;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.05,
    height: 80,
    borderRadius: 6,
    marginBottom: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
    // box shadow
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 5,
  },
});
