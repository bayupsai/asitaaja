import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';

import Heading from './Header';
import Tabs from './Tabs';

//Route Tab
import TabOne from '../Tab/TabTrainDetails';
import TabTwo from '../Tab/TabFacilities';
import TabThree from '../Tab/TabPriceDetails';

//component imported
const window = Dimensions.get('window');

//Master Modal
class DisplayModalTrain extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      page: 0,
      modalVisible: true,
    };

    this._onTabChange = this._onTabChange.bind(this);
  }

  _onTabChange(tabIndex) {
    this.setState({ page: tabIndex });
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  resetModal = () => {
    this.setState({ modalVisible: false });
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.display}
        hardwareAccelerated={false}
        onRequestClose={this.props.close}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackButtonPress={this.resetModal}
        onBackdropPress={this.resetModal}
        style={{ margin: 0, padding: 0 }}
      >
        <View style={styles.container}>
          <Heading title="Details Trains" backButton={this.props.modalin} />
          <Tabs>
            <TabOne
              {...this.props}
              title="Train Details"
              departureTime={this.props.departureTime}
              departureLocation={this.props.departureLocation}
              departure_code={this.props.departure_code}
              arrivalTime={this.props.arrivalTime}
              arrivalLocation={this.props.arrivalLocation}
              arrival_code={this.props.arrival_code}
              modalPress={this.props.modalPress}
            />
            <TabTwo
              {...this.props}
              title="Facilities"
              modalPress={this.props.modalPress}
            />
            {/* <TabThree title="Price Details" /> */}
          </Tabs>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
    backgroundColor: '#FFF', // Background color
  },
  // Tab content container
  content: {
    flex: 1, // Take up all available space
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
    backgroundColor: '#C2185B', // Darker background for content area
  },
  // Content header
  header: {
    margin: 5, // Add margin
    color: '#FFFFFF', // White color
    fontFamily: 'Avenir', // Change font family
    fontSize: 26, // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: 'Avenir',
    fontSize: 18,
  },
});

export default DisplayModalTrain;
