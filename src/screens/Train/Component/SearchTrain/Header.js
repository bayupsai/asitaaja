import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { thameColors, fontBold, asitaColor } from '../../../../base/constant';

const Back = props => {
  return (
    <TouchableOpacity onPress={props.goBack} style={{ paddingRight: 20 }}>
      <Icon name="ios-arrow-round-back" color="#fff" size={42} type="ionicon" />
    </TouchableOpacity>
  );
};

const Title = props => {
  return (
    <Text
      style={{
        color: '#FFFFFF',
        fontSize: 18,
        fontFamily: fontBold,
        letterSpacing: 0.5,
        marginLeft: -10,
      }}
    >
      {props.title}
    </Text>
  );
};

const Heading = props => (
  <Header
    leftComponent={<Back goBack={props.backButton} />}
    centerComponent={<Title title={props.title} />}
    rightComponent={<Text />}
    containerStyle={{
      backgroundColor: asitaColor.marineBlue,
      paddingTop: 0,
      borderBottomWidth: 0,
      borderBottomColor: 'transparent',
      height: 50,
    }}
  ></Header>
);

export default Heading;
