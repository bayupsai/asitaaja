import React from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  ScrollView,
  Switch,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import { InputText } from '../../../../elements/TextInput';
import { Button, ButtonRounded } from '../../../../elements/Button';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { actionSetPassenger } from '../../../../redux/actions/TrainAction';
import { getFirstNameLastname } from '../../../../utilities/helpers';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import {
  fontReguler,
  fontExtraBold,
  fontBold,
  thameColors,
  fontRegulerItalic,
} from '../../../../base/constant';
import AlertModal from '../../../../components/AlertModal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 0 },
  title: { color: '#424242', fontFamily: fontExtraBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 20,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'space-between', flexDirection: 'row' },
  contactName: { fontSize: 16, color: '#000', fontFamily: fontReguler },
  contactNamePlaceholder: {
    fontSize: 16,
    color: '#838383',
    fontFamily: fontReguler,
  },
  titleChecklist: { fontSize: 14, color: '#434a5e', fontFamily: fontReguler },
  sectionButton: { alignItems: 'flex-end' },
  sectionButtonArea: { justifyContent: 'center', alignItems: 'center' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalSeatClassAdult: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.05,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalDob: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

class Baggage extends React.PureComponent {
  render() {
    return (
      <Grid style={styles.container}>
        <Row>
          <Text style={styles.title}>Additional Baggage</Text>
        </Row>
        <View>
          <View>
            <TouchableOpacity
              onPress={() => alert('Additional Baggage')}
              style={styles.section}
            >
              <Col style={styles.sectionName}>
                <Text style={styles.contactName}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontWeight: '100',
                      fontFamily: fontReguler,
                    }}
                  >
                    Baggage (0 Kg)
                  </Text>
                </Text>
                <ButtonPlus
                  size="small"
                  colorButton="buttonColor"
                  onClick={() => alert('Additional Baggage')}
                />
              </Col>
            </TouchableOpacity>
            <Row>
              <Col>
                <Text
                  style={{
                    color: thameColors.darkGray,
                    fontSize: 13,
                    fontFamily: fontRegulerItalic,
                  }}
                >
                  {' '}
                  Add baggage to lighten your load
                </Text>
              </Col>
            </Row>
          </View>
        </View>
      </Grid>
    );
  }
}

export default Baggage;
