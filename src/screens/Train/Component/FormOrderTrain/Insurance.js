import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { CheckBox } from 'react-native-elements';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
} from '../../../../base/constant';

const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 10 },
  title: { color: '#424242', fontFamily: fontExtraBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 5,
    flexDirection: 'column',
  },
  sectionName: { justifyContent: 'center' },
  contactName: { fontSize: 16, color: '#434a5e', fontFamily: fontReguler },
  sectionButton: { alignItems: 'flex-end' },
  subtitle: {
    color: '#424242',
    fontFamily: fontBold,
    fontSize: 16,
    fontFamily: fontReguler,
  },
  desc: {
    color: '#424242',
    fontSize: 16,
    paddingTop: 10,
    fontFamily: fontReguler,
  },
  price: {
    color: '#424242',
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: fontReguler,
  },
});

const Insurance = state => {
  return (
    <Grid style={styles.container}>
      <Row>
        <Text style={styles.title}>Insurance</Text>
      </Row>
      <Row style={styles.section}>
        <Grid>
          <Row>
            <Text style={styles.subtitle}>Important</Text>
          </Row>
          <Row style={{ marginTop: 5 }}>
            <Text style={styles.decs}>
              Up to Rp300 million coverage for accidents, up to Rp20 million for
              trip cancellation, and up to Rp7.5 million for flight and baggage
              delay.
            </Text>
          </Row>
        </Grid>
        <Grid style={{ marginTop: 10 }}>
          <Col style={{}}>
            <CheckBox
              containerStyle={{
                backgroundColor: '#ffffff',
                padding: 0,
                margin: 0,
                marginLeft: 0,
                borderWidth: 0,
              }}
              title="Yes Insurance"
              textStyle={{ color: '#434a5e', fontSize: 16 }}
              checked={state.usingInsurance}
              iconType="feather"
              checkedIcon="check"
              uncheckedIcon="square"
              checkedColor="#434a5e"
              uncheckedColor="#434a5e"
              onPress={() => state.onPress()}
            />
          </Col>
          <Col style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
            <Text style={styles.price}>IDR 25.000/Pax</Text>
          </Col>
        </Grid>
      </Row>
    </Grid>
  );
};

export default Insurance;
