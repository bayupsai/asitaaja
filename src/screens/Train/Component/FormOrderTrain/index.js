import React from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import styles from '../../../Flight/SearchFlight/styles';
import { DotIndicator } from 'react-native-indicators';
import Modal from 'react-native-modal';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Button, ButtonRounded } from '../../../../elements/Button';
import HeaderPage from '../../../../components/HeaderPage';
import SubHeaderPage from '../../../../components/SubHeaderPage';
import GlobalInsurance from '../../../../components/GlobalInsurance';
import TrainInfo from './TrainInfo';
import Contact from './Contact';
import Passenger from './Passenger';
import Baggage from './Baggage';
import SpecialRequest from './SpecialRequest';
import Insurance from './Insurance';
import Prices from './Prices';
import AlertModal from '../../../../components/AlertModal';
import _ from 'lodash';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

import {
  actionBookTrain,
  failedState,
} from '../../../../redux/actions/TrainAction';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
} from '../../../../base/constant';
import PriceTicket from './PriceTicket';
import { formatDateTrain } from '../../../../utilities/helpers';

class FormOrderTrain extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      usingInsurance: false,
      insurancePrice: 0,
      visibleModal: null,
      validationStatusData: false,
      trainSelected: this.props.navigation.getParam('dataTrainDepartures'),
      errorMessage: '',
      insurance: false,
    };

    // this.props.dispatch(failedState("bookingFlight"))
  }

  // ==== GLOBAL FUNC
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  //modal
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };
  _alertModal = (idModal, message) => {
    const { visibleModal } = this.state;
    return (
      <AlertModal
        type="normal"
        isVisible={visibleModal === idModal}
        title={visibleModal == 91 ? 'Failed' : 'Warning'}
        contentText={message}
        onPress={() => this._toggleModal(null)}
        onDismiss={() => this._toggleModal(null)}
      />
    );
  };
  //modal
  checkUsingInsurane = () => {
    if (this.state.usingInsurance === true) {
      this.setState({ usingInsurance: false, insurancePrice: 0 });
    } else {
      this.setState({ usingInsurance: true, insurancePrice: 25000 });
    }
  };
  // ==== GLOBA FUNC
  validateAllData = () => {
    const payloadTrain = this.props.train.payloadTrain;
    const passengerAdult = parseInt(payloadTrain.adult_quantity);
    const passengerInfant = payloadTrain.infant_quantity
      ? parseInt(payloadTrain.infant_quantity)
      : 0;
    const totalPassenger = passengerAdult + passengerInfant;

    const reduxPassengers = this.props.train.passengerData;

    console.log('Tot Pass -> ' + totalPassenger);
    console.log('Tot Pas in Redux ' + reduxPassengers.length);

    if (reduxPassengers.length < totalPassenger) {
      console.log('still errors');
    } else {
      console.log('valid');
      let checkValidation = false;
      reduxPassengers.map((data, index) => {
        console.log('Inside redux map ' + index);
        console.log(data);
        if (
          data.first_name == '' ||
          data.last_name == '' ||
          (data.type == 'adult' && data.id_card_number == '')
        ) {
          checkValidation = false;
        } else {
          checkValidation = true;
        }
      });
      console.log('Outsiede redux map ' + checkValidation);
      if (checkValidation) {
        this.setState({ validationStatusData: true });
      } else {
        console.log('still error');
      }
    }
  };

  paymentOrder = () => {
    console.log('VALIDATION STATUS -> ' + this.state.validationStatusData);
    console.log(this.props.contact);
    console.log(this.props.train.passengerData);
    if (
      this.state.validationStatusData &&
      this.props.contact.title &&
      this.props.contact.fullName &&
      this.props.contact.emailAddress &&
      this.props.contact.phoneNumber
    ) {
      let payloadTrain = this.props.train.payloadTrain.data;
      let selectedTrain = this.props.train.trainSelected;
      let selectedReturnTrain = this.props.train.trainReturnSelected;
      // this.setState({ visibleModal: 4 });
      const { trainSelected, contact, passenger } = this.props;
      // const trainSelected = this.props.trainSelected;
      // const contact = this.props.contact;
      // const payload = {
      //   command: 'BOOKING',
      //   product: 'KERETA',
      //   partner_trxid: 'ga-mall-023003003002',
      //   data: {
      //     // departure_code: payloadTrain.departure_code,
      //     arrival_code: payloadTrain.arrival_code,
      //     date: payloadTrain.date,
      //     // return_date: payloadTrain.return_date ? payloadTrain.return_date : '',
      //     adult: payloadTrain.adult,
      //     infant: payloadTrain.infant ? payloadTrain.infant : '',
      //     schedule_id: selectedTrain.schedule_id,
      //     // return_schedule_id: payloadTrain.return_date
      //     //   ? selectedReturnTrain.schedule_id
      //     //   : '',
      //     class: '7076',
      //     sub_class: 'A',
      //     passengers: this.props.train.passengerData
      //   }
      // };
      const dateTrain = formatDateTrain(trainSelected.dep_datetime).replace(
        /-/g,
        ''
      );
      const dataAdult = _.filter(passenger, { type: 'adult' });
      const dataInfant = _.filter(passenger, { type: 'infant' });

      const passengerAdult = _.map(dataAdult, a =>
        _.omit(
          a,
          'email',
          'phone',
          'primary',
          'type',
          'salutation',
          'birth_date'
        )
      );
      const passengerChild = _.map(dataInfant, a =>
        _.omit(
          a,
          'email',
          'phone',
          'primary',
          'type',
          'salutation',
          'birth_date',
          'identity'
        )
      );

      const payload = {
        data_schedule_depart: {
          org: trainSelected.org,
          des: trainSelected.des,
          dep_date: dateTrain,
          transporter_no: trainSelected.transporter_no,
          subclass: trainSelected.subclass,
          vendor_id: trainSelected.vendor_id,
        },
        data_caller: {
          title: contact.title,
          name: contact.fullName,
          phone: contact.countryCode + contact.phoneNumber,
        },
        data_passenger: {
          adult: passengerAdult,
          infant: passengerChild,
        },
      };
      this.props.dispatch(actionBookTrain(payload)).then(res => {
        this.setState({ visibleModal: null });
        console.log(res);
        if (res.type == 'BOOK_TRAIN_SUCCESS') {
          // this.props.navigation.navigate('PaymentOrder', { pages: 'train' });
          alert('Success');
        } else {
          this.setState({
            visibleModal: 91,
            errorMessage: res.errorMessage
              ? res.errorMessage
              : 'Booking failed, please try again later',
          });
          // Alert.alert(
          //     'Failed.',
          //     (res.errorMessage) ? res.errorMessage : 'Booking failed, please try again later.',
          //     [
          //       {text: 'OK', onPress: () => console.log('OK Pressed')},
          //     ],
          //     { cancelable: false },
          // );
        }
      });
    } else {
      this.setState({
        visibleModal: 92,
        errorMessage:
          'Please enter contact details and passenger details correctly',
      });
      // Alert.alert(
      //     'Warning.',
      //     'Please enter your contact details and passenger details correctly.',
      //     [
      //       {text: 'OK', onPress: () => console.log('OK Pressed')},
      //     ],
      //     { cancelable: false },
      // );
    }
  };

  loadingBooking = () => (
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor: '#FFFFFF',
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        height: 'auto',
        flex: 0,
      }}
    >
      <Row style={{ flex: 0 }}>
        <Col
          size={3}
          style={{ alignItems: 'center', justifyContent: 'center' }}
        >
          <Image
            source={require('../../../../assets/img/loading3x.png')}
            style={{ width: 175, height: 175 }}
            resizeMode="center"
          />
        </Col>
      </Row>
      <Row style={{ flex: 0 }}>
        <Col size={7}>
          <Row
            style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}
          >
            <Text
              style={{ fontFamily: fontExtraBold, fontSize: 16, color: '#000' }}
            >
              Relaxed for a moment
            </Text>
          </Row>
          <Row
            style={{
              flex: 0,
              paddingTop: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{ fontFamily: fontReguler, fontSize: 14, color: '#000' }}
            >
              We are currently processing your booking.
            </Text>
          </Row>
          <Row
            style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}
          >
            <Text
              style={{ fontFamily: fontReguler, fontSize: 14, color: '#000' }}
            >
              This may take a few minutes.
            </Text>
          </Row>
          <Row
            style={{
              flex: 0,
              paddingTop: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <DotIndicator
              size={10}
              style={{ flex: 0, backgroundColor: '#FFFFFF' }}
              color="#a2195b"
            />
          </Row>
        </Col>
      </Row>
    </Grid>
  );

  // Main Render
  render() {
    console.log(this.props);
    let { errorMessage } = this.state;
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 0 }}>
        {this._alertModal(91, errorMessage)}
        {this._alertModal(92, errorMessage)}
        <ScrollView>
          <HeaderPage
            title="Order Summary"
            callback={() => this.goBackHeader()}
            {...this.props}
          />
          <SubHeaderPage />

          <View style={{ marginTop: -50 }}>
            <TrainInfo
              {...this.props.trainSelected}
              departure_code={'this.props.isReturn.departure_code'}
              arrival_code={'this.props.isReturn.arrival_code'}
              title="Departure Train"
            />
            {/* {this.props.isReturn.return_date ? (
              <TrainInfo
                {...this.props.trainReturnSelected}
                departure_code={"this.props.isReturn.arrival_code"}
                arrival_code={"this.props.isReturn.departure_code"}
                title="Return Train"
              />
            ) : null} */}
          </View>

          <Contact {...this.props.train} {...this.props} />

          <Passenger
            onChangeData={() => this.validateAllData()}
            {...this.props}
          />

          {/* Baggage */}
          {/* <Baggage /> */}
          {/* Baggage */}

          {/* Insurance */}
          {/* <GlobalInsurance
            title="Travel Insurance"
            price={39000}
            value={this.state.insurance}
            onChange={() => this.setState({ insurance: !this.state.insurance })}
          /> */}
          {/* Insurance */}

          {/* Price Ticket whene return is true */}
          {/* {this.props.isReturn.return_date ? (
            <PriceTicket />
          ) : (
              <View style={{ width: 0, height: 0 }} />
            )} */}
          {/* Price Ticket whene return is true */}

          <Prices {...this.props.train} />

          <View style={styles.sectionButtonArea}>
            <View style={styles.sectionButton}>
              <ButtonRounded
                label="Continue Payment"
                onClick={() => this._toggleModal(5)}
                isActive={true}
              />
            </View>
          </View>
        </ScrollView>

        {/* LOADING MODAL */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 4}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={200}
          animationOutTiming={200}
          backdropTransitionInTiming={200}
          backdropTransitionOutTiming={200}
        >
          {this.loadingBooking()}
        </Modal>
        {/* LOADING MODAL */}

        {/* Booking Summary Alert */}
        <AlertModal
          type="qna"
          isVisible={this.state.visibleModal === 5}
          titleColor="#000"
          title="Are your booking details correct?"
          contentText="You will not be able to change your booking details once you proceed to payment. Do you want to continue?"
          labelOk="YES, CONTINUE"
          labelCancel="CHECK AGAIN"
          onPress={() => this.paymentOrder()}
          onDismiss={() => this._toggleModal(null)}
        />
        {/* Booking Summary Alert */}

        {/* Alert when Back Button Pressed */}
        <AlertModal
          type="qna"
          isVisible={this.state.visibleModal === 6}
          titleColor="#000"
          title="Are you sure want to back to Transaction?"
          contentText="You can continue your transaction again in My Order List"
          labelOk="YES, CONTINUE"
          labelCancel="NO, THANKS"
          onPress={() => this.props.navigation.navigate('Home')}
          onDismiss={() => this._toggleModal(null)}
        />
        {/* Alert when Back Button Pressed */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    train: state.train,
    isReturn: state.train.payloadTrain.data,
    trainSelected: state.train.trainSelected,
    trainReturnSelected: state.train.trainReturnSelected,
    contact: state.train.contactDetail,
    passenger: state.train.passengerData,
  };
}
export default connect(mapStateToProps)(FormOrderTrain);

{
  /* <View style={{ marginTop: 0 }}>
    <SpecialRequest title="Booking Hotel" subtitle="Combine your Flight with Hotel"/>
    <SpecialRequest title="Pickup" subtitle="Combine your Flight with Pickup" />
</View> */
}
{
  /* <Insurance  {...this.state } onPress={() => this.checkUsingInsurane() } /> */
}
