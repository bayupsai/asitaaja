import React from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  ScrollView,
  Switch,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import { InputText } from '../../../../elements/TextInput';
import { Button, ButtonRounded } from '../../../../elements/Button';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { actionSetPassenger } from '../../../../redux/actions/TrainAction';
import { getFirstNameLastname } from '../../../../utilities/helpers';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import {
  fontReguler,
  fontExtraBold,
  fontBold,
  thameColors,
} from '../../../../base/constant';
import AlertModal from '../../../../components/AlertModal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 0 },
  title: { color: '#424242', fontFamily: fontExtraBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 20,
    borderRadius: 5,
  },
  sectionName: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 5,
  },
  contactName: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
    paddingLeft: 5,
  },
  contactNamePlaceholder: {
    fontSize: 16,
    color: '#838383',
    fontFamily: fontReguler,
  },
  titleChecklist: { fontSize: 14, color: '#434a5e', fontFamily: fontReguler },
  sectionButton: { alignItems: 'flex-end' },
  sectionButtonArea: { justifyContent: 'center', alignItems: 'center' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalSeatClassAdult: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.05,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalDob: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

class Passenger extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      titleSelected: 2,
      listTitle: [
        { index: 1, title: 'Mr' },
        { index: 2, title: 'Mrs' },
        { index: 3, title: 'Ms' },
        { index: 4, title: 'Mstr' },
      ],
      title: 'Mr',
      fullName: '',
      countryCode: '+62',
      phoneNumber: '',
      emailAddress: '',
      adults: this.props.train.adult_quantity,
      infants: this.props.train.infant_quantity,
      dataPassenger: {
        adults: [],
        children: [],
        infants: [],
      },
      payloadPassengers: [],
      passengersAdults: [],
      passengersChilds: [],
      passengersInfants: [],
      modalActiveForm: '',
      modalActiveIndex: '',
      modalActiveData: '',

      sameAsContact: false,
      sameContactPerson: false,
    };
  }

  componentDidMount() {
    if (this.state.adults > 0)
      this.generateStatePassenger(this.state.adults, 'adults');
    // if (this.state.childs > 0) this.generateStatePassenger(this.state.childs, "children")
    if (this.state.infants > 0)
      this.generateStatePassenger(this.state.infants, 'infants');
    this.setState({
      modalActiveForm: 'adults',
      modalActiveIndex: 0,
      modalActiveData: null,
    });
  }

  //modal
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };
  _alertModal = (idModal, title, description) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visibleModal === idModal}
        title={title}
        contentText={description}
        onPress={() => this._toggleModal(1)}
        onDismiss={() => this._toggleModal(1)}
      />
    );
  };
  //modal

  setTitlePassenger = data => {
    const { dataPassenger, modalActiveForm, modalActiveIndex } = this.state;
    const field = 'title';
    const fieldIndex = 'titleIndex';
    if (dataPassenger[modalActiveForm]) {
      dataPassenger[modalActiveForm][modalActiveIndex][field] = data.title;
      dataPassenger[modalActiveForm][modalActiveIndex][fieldIndex] = data.index;
    }
    this.setState({
      dataPassenger,
      visibleModal: 1,
    });
  };

  handleInput = (type, data) => {
    let { dataPassenger, modalActiveForm, modalActiveIndex } = this.state;

    if (type == 'fullName') {
      const field = 'fullName';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
        // this.setState({ fullName: data });
      }
    } else if (type == 'IDCard') {
      const field = 'IdCardNumber';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
      }
    } else if (type == 'sameContact') {
      const field = 'fullName';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
      }
      this.generatePayloadPassengers('adult', 'adults');
    } else if (type == 'blank') {
      const field1 = 'fullName';
      const field2 = 'title';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field1] = data;
        // dataPassenger[modalActiveForm][modalActiveIndex][field2] = data
      }
      this.generatePayloadPassengers('adult', 'adults');
      this.setState({ visibleModal: 1 });
    }
    this.setState({
      dataPassenger,
    });
  };

  generateStatePassenger(total, field) {
    let { dataPassenger } = this.state;
    for (let i = 0; i < total; i++) {
      dataPassenger[field].push({
        titleIndex: 1,
        title: 'Mr',
        fullName: '',
        IdCardNumber: '',
      });
    }
    this.setState({ dataPassenger });
  }

  renderFormPassenger(form, titleForm) {
    const { dataPassenger, dataTitle } = this.state;

    console.log(form);
    if (!dataPassenger[form]) return null;

    return (
      <View>
        {dataPassenger[form].map((data, index) => {
          console.log(dataPassenger[form][index].IdCardNumber);
          return (
            <View key={index}>
              <TouchableOpacity
                onPress={() => this.openModalPassenger(form, index, null)}
                style={styles.section}
              >
                <Col style={styles.sectionName}>
                  <Text style={styles.contactName}>
                    {dataPassenger[form][index].fullName ? (
                      dataPassenger[form][index].title +
                      ' ' +
                      dataPassenger[form][index].fullName
                    ) : (
                      <Text
                        style={{
                          color: thameColors.textBlack,
                          fontFamily: fontReguler,
                          fontFamily: fontReguler,
                        }}
                      >
                        {titleForm + ' #' + parseInt(index + 1)}
                      </Text>
                    )}
                  </Text>
                  <ButtonPlus
                    size="small"
                    colorButton="buttonColor"
                    onClick={() => this.openModalPassenger(form, index, null)}
                  />
                </Col>
              </TouchableOpacity>
              {dataPassenger[form][index].fullName == '' ||
              (form == 'adults' &&
                (dataPassenger[form][index].IdCardNumber == '' ||
                  dataPassenger[form][index].IdCardNumber.length > 16)) ? (
                <Row>
                  <Col>
                    <Text
                      style={{
                        color: '#d30000',
                        fontSize: 13,
                        fontFamily: fontExtraBold,
                      }}
                    >
                      *Please fill in passenger details{' '}
                    </Text>
                  </Col>
                </Row>
              ) : null}
            </View>
          );
        })}
      </View>
    );
  }

  openModalPassenger = (form, index, currentData) => {
    this.setState({
      visibleModal: 1,
      modalActiveForm: form,
      modalActiveIndex: index,
      modalActiveData: currentData,
    });
  };

  generatePayloadPassengers = (passengerType, objectField) => {
    let { dataPassenger } = this.state;
    let payloadPass = [];
    let tempArr = [];
    let contactDetails = this.props.contactDetails;

    // let dataEmail = ""
    // let dataPhone = ""
    // let dataPrimary = true

    let dataEmail = contactDetails.emailAddress;
    let dataPhone = contactDetails.phoneNumber;
    let dataPrimary = true;
    let dataIDCard = '';
    let isValid = true;
    dataPassenger[objectField].map((data, index) => {
      console.log('Data Passenger Index :' + index);
      let tempObj = {};
      tempObj.name = data.fullName;
      // getFirstNameLastname(data.fullName, res => {
      //   (tempObj.first_name = res.firstName),
      //     (tempObj.last_name = res.lastName);
      // });

      if (tempObj.name !== '') {
        if (
          objectField == 'adults' &&
          (data.IdCardNumber == '' || data.IdCardNumber.length > 16)
        ) {
          isValid = false;
          this.setState({ visibleModal: 91 });
        } else {
          if (index == 0 && passengerType == 'adult') {
            console.log('ADULT AND O ARRAY');
            dataEmail = contactDetails.emailAddress;
            dataPhone = contactDetails.phoneNumber;
            dataPrimary = true;
            dataIDCard = data.IdCardNumber;
          } else {
            dataEmail = 'mandatory@abc.xyz';
            dataPhone = '0855101010101' + index;
            dataPrimary = false;
            dataIDCard = data.IdCardNumber + index;
          }
          (tempObj.email = dataEmail),
            (tempObj.phone = dataPhone),
            (tempObj.primary = dataPrimary),
            (tempObj.identity = dataIDCard),
            (tempObj.type = passengerType),
            (tempObj.salutation = data.title),
            (tempObj.birth_date = '1964-12-03'),
            tempArr.push(tempObj);

          isValid = true;
        }
      } else {
        isValid = false;
        this.setState({ visibleModal: 92 });
      }
    });

    if (objectField == 'adults') {
      console.log('in Adult');
      this.setState({ passengersAdults: tempArr });
    } else if (objectField == 'infants') {
      console.log('in Infants');
      this.setState({ passengersInfants: tempArr });
    }

    setTimeout(() => {
      payloadPass = this.state.passengersAdults
        .concat(this.state.passengersChilds)
        .concat(this.state.passengersInfants);
      console.log('Set payload');
      if (isValid) {
        this.setState({ payloadPassengers: payloadPass, visibleModal: null });
        this.props.dispatch(actionSetPassenger(payloadPass));
        this.props.onChangeData();
      }
    }, 100);
  };

  toggleContactPerson = value => {
    console.log(value + ' -- ' + this.props.contactDetails.fullName);
    if (this.props.contactDetails.fullName) {
      this.setState(
        {
          sameContactPerson: value,
          modalActiveForm: 'adults',
          modalActiveIndex: 0,
          modalActiveData: null,
        },
        value
          ? this.handleInput('sameContact', this.props.contactDetails.fullName)
          : this.handleInput('blank', ' ')
      );
    } else {
      this.setState({ visibleModal: 999, sameContactPerson: false });
    }
  };

  savePassengers = () => {
    const { modalActiveForm } = this.state;
    let typePassenger = 'adult';
    let typeFiled = 'adults';
    if (modalActiveForm == 'children') {
      typePassenger = 'child';
      typeFiled = 'children';
    } else if (modalActiveForm == 'infants') {
      typePassenger = 'infant';
      typeFiled = 'infants';
    }
    this.generatePayloadPassengers(typePassenger, typeFiled);
  };

  render() {
    let {
      dataPassenger,
      modalActiveForm,
      modalActiveIndex,
      sameAsContact,
    } = this.state;
    const titleIndex = 'titleIndex';
    const title = 'title';
    const fullname = 'fullName';
    const IdCardNumber = 'IdCardNumber';

    let currentPopupPassengers = '';
    let currentPopupPassengersTitle = '';
    let currentPopupPassengersFullname = '';
    let currentPopupPassengersIDCard = '';

    if (modalActiveForm) {
      currentPopupPassengers =
        dataPassenger[modalActiveForm][modalActiveIndex][titleIndex];
      currentPopupPassengersTitle =
        dataPassenger[modalActiveForm][modalActiveIndex][title];
      currentPopupPassengersFullname =
        dataPassenger[modalActiveForm][modalActiveIndex][fullname];
      currentPopupPassengersIDCard =
        dataPassenger[modalActiveForm][modalActiveIndex][IdCardNumber];
    }

    console.log(`Data Passenger : ${JSON.stringify(dataPassenger)}`);

    return (
      <Grid style={styles.container}>
        {/* ================= Modal Alert ================= */}
        {this._alertModal(91, 'Next Step', 'Please enter your ID card number')}
        {this._alertModal(
          92,
          'Warning',
          'Please enter Fullname (first name & last name) correctly'
        )}
        {this._alertModal(
          999,
          'Warning',
          'Make sure you already fill your contact details'
        )}
        {/* ================= Modal Alert ================= */}

        <Row style={{ marginBottom: -5 }}>
          <Text style={styles.title}>Passenger Details</Text>
        </Row>
        <Row style={{ paddingTop: 10, marginBottom: -5 }}>
          <Col>
            <Text style={styles.titleContactPerson}>
              Same as contact detail
            </Text>
          </Col>
          <Col>
            <Switch
              onValueChange={this.toggleContactPerson}
              value={this.state.sameContactPerson}
            />
          </Col>
        </Row>

        {this.renderFormPassenger('adults', 'Adult')}
        {/* { this.renderFormPassenger("children", "Child") } */}
        {this.renderFormPassenger('infants', 'Infant')}

        {/* =============================== MODAL FORM ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={() => this._toggleModal(null)}
          onBackdropPress={() => this._toggleModal(null)}
          style={styles.bottomModal}
        >
          <View
            style={
              this.state.modalActiveForm == 'adults'
                ? styles.modalSeatClassAdult
                : styles.modalSeatClass
            }
          >
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#dadada',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col size={8} style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontExtraBold,
                      fontSize: 16,
                    }}
                  >
                    {this.state.modalActiveForm == 'adults'
                      ? 'ADULT ' + (parseInt(this.state.modalActiveIndex) + 1)
                      : ''}
                    {this.state.modalActiveForm == 'children'
                      ? 'CHILD ' + (parseInt(this.state.modalActiveIndex) + 1)
                      : ''}
                    {this.state.modalActiveForm == 'infants'
                      ? 'INFANT ' + (parseInt(this.state.modalActiveIndex) + 1)
                      : ''}
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: '#FFFFFF' }}>
                <Grid style={{ paddingTop: 20 }}>
                  {/* <Row style={{ flex: 0, marginBottom: 20 }}>
                                        {
                                            this.props.contactDetails && modalActiveForm == 'adults' && modalActiveIndex === 0 ?
                                                <Col style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end' }}>
                                                    <Text style={{ fontFamily: fontBold }}>Same as Contact Person</Text>
                                                    <Switch value={sameAsContact} onValueChange={() => this.setValueSame("sameContact", this.props.contactDetails.fullName)} />
                                                </Col>
                                                : <View style={{ width: 0, height: 0 }} />
                                        }
                                    </Row> */}
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col size={3}>
                      <TouchableOpacity onPress={() => this._toggleModal(2)}>
                        <InputText
                          size="small"
                          placeholder={
                            currentPopupPassengersTitle != ''
                              ? currentPopupPassengersTitle
                              : 'Mr.'
                          }
                          label="Title"
                          required="*"
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col size={7} style={{ marginLeft: 0 }}>
                      <InputText
                        placeholder={currentPopupPassengersFullname}
                        label="Fullname"
                        required="*"
                        editable={true}
                        onChangeText={data =>
                          this.handleInput('fullName', data)
                        }
                      />
                    </Col>
                  </Row>

                  {this.state.modalActiveForm == 'adults' ? (
                    <Row style={{ flex: 0, marginBottom: 20 }}>
                      <Col style={{ marginLeft: 0 }}>
                        <InputText
                          placeholder={currentPopupPassengersIDCard}
                          label="ID Number (KTP/passport/others)"
                          keyboardType="number-pad"
                          required="*"
                          editable={true}
                          onChangeText={data =>
                            this.handleInput('IDCard', data)
                          }
                        />
                      </Col>
                    </Row>
                  ) : null}
                  <Row style={{ flex: 0, marginBottom: 10 }}>
                    <Col size={6} style={styles.sectionButtonArea}>
                      <ButtonRounded
                        label="SAVE"
                        onClick={this.savePassengers}
                      />
                    </Col>
                  </Row>
                </Grid>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL FORM ===================================== */}

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 2}
          onBackButtonPress={() => this._toggleModal(1)}
          onBackdropPress={() => this._toggleModal(1)}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#838383',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col
                  size={2}
                  style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                >
                  <Icon
                    name="close"
                    type="evilIcon"
                    color="#008195"
                    size={20}
                  />
                </Col>
                <Col
                  size={8}
                  style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                >
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontExtraBold,
                      fontSize: 16,
                    }}
                  >
                    Choose Title
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ paddingLeft: 10 }}>
                {this.state.listTitle.map((data, index) => {
                  return (
                    <Grid
                      key={index}
                      style={{
                        flex: 1,
                        backgroundColor: '#FFFFFF',
                        padding: 10,
                        borderBottomColor: '#d5d5d5',
                        borderBottomWidth: 0.5,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setTitlePassenger(data)}
                        style={{ flex: 1 }}
                      >
                        <Col style={{ alignItems: 'flex-start' }}>
                          <Text
                            style={{
                              fontFamily: fontReguler,
                              color: '#000',
                              fontSize: 16,
                            }}
                          >
                            {data.title}
                          </Text>
                        </Col>
                        <Col
                          style={{
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}
                        >
                          {currentPopupPassengers != '' &&
                          currentPopupPassengers == data.index ? (
                            <Icon
                              name="check"
                              type="feather"
                              color="#008195"
                              size={22}
                            />
                          ) : (
                            <Text></Text>
                          )}
                        </Col>
                      </TouchableOpacity>
                    </Grid>
                  );
                })}
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    train: state.train.payloadTrain,
    contactDetails: state.train.contactDetail,
  };
}
export default connect(mapStateToProps)(Passenger);
