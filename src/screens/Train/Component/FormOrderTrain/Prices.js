import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Numeral from 'numeral';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../../../base/constant';

const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 10 },
  title: { color: '#424242', fontFamily: fontExtraBold, fontSize: 16 },
  titlePrice: { color: '#424242', fontFamily: fontExtraBold, fontSize: 18 },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 20,
    borderRadius: 5,
    flexDirection: 'column',
  },
  sectionName: { justifyContent: 'center' },
  contactName: {
    fontSize: 16,
    color: thameColors.darkGray,
    fontFamily: fontReguler,
  },
  detailPrices: {
    fontSize: 16,
    color: thameColors.primary,
    fontFamily: fontReguler,
  },
  totalPrices: {
    fontSize: 18,
    color: thameColors.primary,
    fontFamily: fontBold,
  },
  sectionButton: { alignItems: 'flex-end' },
  pt5: { paddingTop: 5 },
});

const Prices = props => {
  // const isReturn = props.payloadTrain.data.return_date;
  const isReturn = false;
  const departureData = props.trainSelected;
  console.log(departureData);

  const totalAdults =
    parseInt(departureData.adult_fare) * props.payloadTrain.adult_quantity;
  const passInfant = props.payloadTrain.infant_quantity
    ? props.payloadTrain.infant_quantity
    : 0;
  // const totalInfants = parseInt(departureData.price_infant) * passInfant;
  const totalInfants = 0;
  const totalPriceDeparture = totalAdults + totalInfants;
  let totalPriceReturn = 0;
  let returnData = {};
  if (isReturn) {
    // returnData = props.trainReturnSelected;
    // const totalReturnAdults =
    //   parseInt(returnData.price_adult) * props.payloadTrain.data.adult;
    // const totalReturnInfants = parseInt(returnData.price_infant) * passInfant;
    // totalPriceReturn = totalReturnAdults + totalReturnInfants;
  } else {
    totalPriceReturn = 0;
  }
  return (
    <Grid style={styles.container}>
      <Row>
        <Text style={[styles.title, { color: '#000' }]}>Total Payment</Text>
      </Row>
      <Row style={styles.section}>
        {/* If not return */}
        {isReturn ? (
          <View style={{ width: 0, height: 0 }} />
        ) : (
          <Grid
            style={{
              borderBottomColor: thameColors.gray,
              borderBottomWidth: 1,
              paddingBottom: 10,
            }}
          >
            <Col>
              <Text
                style={[styles.contactName, { color: thameColors.textBlack }]}
              >
                Adult x1
              </Text>
            </Col>
            <Col style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
              <Text
                style={[styles.contactName, { color: thameColors.textBlack }]}
              >
                Rp
                {Numeral(totalPriceDeparture)
                  .format('0,00')
                  .replace(/,/g, '.')}
              </Text>
            </Col>
          </Grid>
        )}
        {/* If not return */}

        {/* Travel Insurance */}
        {/* <Grid
          style={{
            borderBottomColor: thameColors.gray,
            borderBottomWidth: 1,
            paddingBottom: 10,
            paddingTop: 10
          }}
        >
          <Col>
            <Text
              style={[styles.contactName, { color: thameColors.textBlack }]}
            >
              Travel Insurance
            </Text>
          </Col>
          <Col style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
            <Text
              style={[styles.contactName, { color: thameColors.textBlack }]}
            >
              Rp 0
            </Text>
          </Col>
        </Grid> */}

        {/* Price Total */}
        <Grid style={{ paddingTop: 10 }}>
          <Col>
            <Text
              style={[styles.contactName, { color: thameColors.textBlack }]}
            >
              Price you pay
            </Text>
          </Col>
          <Col style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
            <Text style={styles.totalPrices}>
              Rp
              {Numeral(totalPriceDeparture)
                .format('0,00')
                .replace(/,/g, '.')}
            </Text>
          </Col>
        </Grid>
      </Row>
    </Grid>
  );
};

export default Prices;
