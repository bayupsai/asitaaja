import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import Numeral from 'numeral';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
  asitaColor,
} from '../../../../base/constant';
import { formatDateTrain } from '../../../../utilities/helpers';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    paddingBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    flex: 0,
    marginBottom: 10,
  },
  cardBox: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    backgroundColor: '#fff',
    width: deviceWidth - 40,
    paddingBottom: 0,
    borderRadius: 10,
  },
  title: {
    color: asitaColor.orange,
    fontFamily: fontExtraBold,
    fontSize: 18,
  },
  sectionBox: { flex: 0, paddingTop: 10, paddingBottom: 0 },
  rute: { fontSize: 16, color: '#424242', fontFamily: fontExtraBold },
  departureDate: { fontFamily: fontReguler, fontSize: 14, color: '#424242' },
  classInfo: { fontFamily: fontBold, fontSize: 14, color: '#b9bcb9' },
  buttonDetail: {
    flex: 0,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
  sectionTime: {
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  departureTime: {
    fontFamily: fontExtraBold,
    color: thameColors.primary,
    fontSize: 14,
  },
  departureCode: {
    fontSize: 14,
    fontFamily: fontReguler,
    paddingLeft: 5,
    color: '#a3a3a3',
  },
  prices: { color: '#ed6d00', fontFamily: fontExtraBold, fontSize: 16 },
});

const subStr = string => {
  return `${string.substring(0, 20)}...`;
};

const uptoLow = string => {
  return `${string.charAt(0).toUpperCase() + string.substr(1).toLowerCase()}`;
};

const FlightInfo = props => {
  const dateDep = props.dep_datetime.substring(0, 10);
  const valueDateDep = dateDep.replace(/\//g, '-');
  const dateArv = props.arv_datetime.substring(0, 10);
  const valueDateArv = dateArv.replace(/\//g, '-');
  return (
    <Grid style={styles.container}>
      <Col style={styles.cardBox}>
        <Row style={{ flex: 0 }}>
          <Col>
            <Text style={styles.title}>{props.title}</Text>
          </Col>
          <Col style={{ alignItems: 'flex-end' }}>
            <Text
              style={[
                styles.title,
                {
                  color: '#222222',
                  fontFamily: fontReguler,
                  textAlign: 'right',
                },
              ]}
            >
              {/* {moment(valueDateDep).format('dd, DD MMM YYYY')} */}
              {moment(formatDateTrain(valueDateDep)).format('ddd, DD MMM YYYY')}
            </Text>
          </Col>
        </Row>
        <Row style={{ flex: 0, marginTop: 20 }}>
          <Col>
            <View>
              <Text
                style={{
                  fontFamily: fontExtraBold,
                  color: '#222222',
                  fontSize: 18,
                }}
              >
                {/* {moment(valueDate).format('ddd, DD MMM')} */}
                {moment(formatDateTrain(valueDateDep)).format('DD MMM')}
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: '#222222',
                  fontSize: 16,
                }}
              >
                {/* {payloadTrain.departure_code} */}
                {props.org}
              </Text>
            </View>
          </Col>
          <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Dash
                style={{ width: deviceWidth / 10, height: 1, marginRight: 5 }}
                dashGap={3}
                dashLength={6}
                dashColor={thameColors.gray}
              />
              <Image
                source={require('../../../../assets/icons/icon_trains.png')}
                style={{
                  width: 20,
                  height: 25,
                  margin: 5,
                  tintColor: '#222222',
                }}
                resizeMode="center"
              />
              <Dash
                style={{ width: deviceWidth / 10, height: 1 }}
                dashGap={3}
                dashLength={6}
                dashColor={thameColors.gray}
              />
            </View>
            <View style={{ marginTop: 3 }}>
              <Text>2h 5m direct</Text>
            </View>
          </Col>
          <Col style={{ alignItems: 'flex-end' }}>
            <View>
              <Text
                style={{
                  fontFamily: fontExtraBold,
                  color: '#222222',
                  fontSize: 18,
                }}
              >
                {moment(formatDateTrain(valueDateArv)).format('DD MMM')}
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: '#222222',
                  fontSize: 16,
                }}
              >
                {/* {payloadTrain.arrival_code} */}
                {props.des}
              </Text>
            </View>
          </Col>
        </Row>
        <Row style={{ flex: 0, marginTop: 10 }}>
          <Col>
            <Image
              style={{ width: 75, height: 75 }}
              resizeMode="center"
              source={require('../../../../assets/logos/train.png')}
            />
          </Col>
          <Col style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text
              style={{
                color: '#222222',
                fontSize: 16,
                fontFamily: fontReguler,
              }}
            >
              {uptoLow(subStr(props.transporter_name))}
            </Text>
          </Col>
        </Row>
      </Col>
    </Grid>
  );
};

export default FlightInfo;
