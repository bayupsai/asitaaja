import React from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import { InputText } from '../../../../elements/TextInput';
import { Button, ButtonRounded } from '../../../../elements/Button';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { actionSetPassenger } from '../../../../redux/actions/TrainAction';
import { getFirstNameLastname } from '../../../../utilities/helpers';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import {
  fontReguler,
  fontExtraBold,
  fontBold,
  thameColors,
  fontRegulerItalic,
} from '../../../../base/constant';
import AlertModal from '../../../../components/AlertModal';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 0 },
  title: { color: '#424242', fontFamily: fontExtraBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 20,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'space-around', flexDirection: 'row' },
  contactName: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
  },
  contactNamePlaceholder: {
    fontSize: 16,
    color: '#838383',
    fontFamily: fontReguler,
  },
  titleChecklist: { fontSize: 14, color: '#434a5e', fontFamily: fontReguler },
  sectionButton: { alignItems: 'flex-end' },
  sectionButtonArea: { justifyContent: 'center', alignItems: 'center' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalSeatClassAdult: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.05,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalDob: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

const borderBottomWidth = () => {
  if (index == 0) {
    `${0.5}`;
  }
};

class PriceTicket extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dummy: [
        {
          id: 1,
          img: require('../../../../assets/icons/icon_train_right.png'),
          price: 110000,
        },
        {
          id: 2,
          img: require('../../../../assets/icons/icon_train_left.png'),
          price: 100000,
        },
      ],
    };
  }
  render() {
    return (
      <Grid style={styles.container}>
        <Row>
          <Text style={styles.title}>Price Departure & Return</Text>
        </Row>
        <View>
          <View>
            <View style={styles.section}>
              {this.state.dummy.map((item, index) => {
                return (
                  <View
                    key={index}
                    style={[
                      [
                        styles.sectionName,
                        {
                          borderBottomColor: thameColors.gray,
                          borderBottomWidth: index == 0 ? 0.5 : 0,
                          paddingBottom: 10,
                          paddingTop: 10,
                        },
                      ],
                    ]}
                  >
                    <Col
                      size={7}
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                      }}
                    >
                      <Image
                        source={item.img}
                        style={{
                          width: deviceWidth / 10,
                          height: deviceHeight / 50,
                          marginRight: 10,
                          tintColor: thameColors.primary,
                        }}
                      />
                      <Text style={styles.contactName}>Adult x1</Text>
                    </Col>
                    <Col size={3} style={{ justifyContent: 'flex-end' }}>
                      <Text style={styles.contactName}>Rp{item.price}</Text>
                    </Col>
                  </View>
                );
              })}
            </View>
          </View>
        </View>
      </Grid>
    );
  }
}

export default PriceTicket;
