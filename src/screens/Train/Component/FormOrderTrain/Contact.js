import React from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import {
  Button,
  ButtonLogin,
  ButtonRounded,
} from '../../../../elements/Button';
import { InputText } from '../../../../elements/TextInput';
import { connect } from 'react-redux';
import { actionContactDetail } from '../../../../redux/actions/FlightAction';
import Modal from 'react-native-modal';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
} from '../../../../base/constant';
import { validateEmailFormat } from '../../../../utilities/helpers';
import AlertModal from '../../../../components/AlertModal';
import LoginModal from '../../../../components/LoginModal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 10 },
  title: { color: '#424242', fontFamily: fontExtraBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 20,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'space-between', flexDirection: 'row' },
  contactName: { fontSize: 16, color: '#000' },
  sectionButton: { alignItems: 'flex-end' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 1.75,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  sectionButtonArea: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class Contact extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      titleSelected: 1,
      listTitle: [
        { index: 1, title: 'Mr' },
        { index: 2, title: 'Mrs' },
        { index: 3, title: 'Ms' },
        { index: 4, title: 'Mstr' },
      ],
      title: 'Mr',
      fullName: '',
      countryCode: '+62',
      phoneNumber: '',
      emailAddress: '',
      isValidEmail: true,
    };
  }

  componentDidMount() {
    this.handleLogin();
  }

  handleLogin = () => {
    let { fullname, mobileNo, email } = this.props.dataProfile;
    this.setState(
      { fullName: fullname, phoneNumber: mobileNo, emailAddress: email },
      () => this.informationPassengers()
    );
  };

  //modal
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };
  _alertModal = idModal => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visibleModal === idModal}
        title="Warning"
        contentText="Please enter data correctly"
        onPress={() => this._toggleModal(1)}
        onDismiss={() => this._toggleModal(1)}
      />
    );
  };
  //modal

  setTitlePassenger = data => {
    this.setState({
      title: data.title,
      titleSelected: data.index,
      visibleModal: 1,
    });
  };
  handleInput = (type, data) => {
    if (type == 'fullName') {
      this.setState({ fullName: data });
    } else if (type == 'phoneNumber') {
      this.setState({ phoneNumber: data });
    } else if (type == 'emailAddress') {
      let checkEmail = validateEmailFormat(data);
      if (checkEmail) {
        this.setState({ emailAddress: data, isValidEmail: true });
      } else {
        this.setState({ emailAddress: '', isValidEmail: false });
      }
    }
  };
  informationPassengers = () => {
    if (
      this.state.fullName == '' ||
      this.state.phoneNumber == '' ||
      this.state.emailAddress == ''
    ) {
      this.setState({ visibleModal: 999 });
    } else {
      const payload = {
        title: `${this.state.title}.`,
        fullName: this.state.fullName,
        countryCode: this.state.countryCode,
        phoneNumber: this.state.phoneNumber,
        emailAddress: this.state.emailAddress,
      };
      this.props.dispatch(actionContactDetail(payload));
      this.setState({ visibleModal: null });
    }
  };

  render() {
    return (
      <Grid style={styles.container}>
        {this._alertModal(999)}

        {/* ===================== loginButton ===================== */}
        {this.props.isLogin === false ? (
          <ButtonLogin onPress={() => this._toggleModal(99999)} />
        ) : (
          <View style={{ width: 0, height: 0 }} />
        )}
        {/* ===================== loginButton ===================== */}

        <Row style={{ marginTop: 15 }}>
          <Text style={styles.title}>Contact Details</Text>
        </Row>
        <TouchableOpacity
          onPress={() => this._toggleModal(1)}
          style={styles.section}
        >
          <Col style={styles.sectionName}>
            <Text style={styles.contactName}>
              {this.state.fullName ? (
                this.state.title + '. ' + this.state.fullName
              ) : (
                <Text
                  style={{
                    color: thameColors.textBlack,
                    fontFamily: fontReguler,
                  }}
                >
                  Contact Person
                </Text>
              )}
            </Text>
            <ButtonPlus
              size="small"
              colorButton="buttonColor"
              onClick={() => this._toggleModal(1)}
            />
          </Col>
        </TouchableOpacity>
        {this.state.fullName == '' ||
        this.state.phoneNumber == '' ||
        this.state.emailAddress == '' ? (
          <Row>
            <Text
              style={{
                color: '#d30000',
                fontSize: 13,
                fontFamily: fontExtraBold,
              }}
            >
              *Please fill in contact details
            </Text>
          </Row>
        ) : (
          <View></View>
        )}

        {/* =============================== MODAL FORM ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={() => this._toggleModal(null)}
          onBackdropPress={() => this._toggleModal(null)}
          style={styles.bottomModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#d5d5d5',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col size={8} style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Contact Details
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: '#FFFFFF' }}>
                <Grid style={{ paddingTop: 20 }}>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col size={3}>
                      <TouchableOpacity onPress={() => this._toggleModal(2)}>
                        <InputText
                          size="small"
                          placeholder={this.state.title}
                          label="Title"
                          required="*"
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col size={7} style={{ marginLeft: 0 }}>
                      <InputText
                        label="Fullname"
                        required="*"
                        value={this.state.fullName}
                        editable={true}
                        onChangeText={value =>
                          this.handleInput('fullName', value)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col>
                      <InputText
                        label="Phone Number"
                        required="*"
                        value={this.state.phoneNumber}
                        maxLength={13}
                        keyboardType="number-pad"
                        editable={true}
                        onChangeText={value =>
                          this.handleInput('phoneNumber', value)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col>
                      <View>
                        <InputText
                          label="Email Address"
                          required="*"
                          value={this.state.emailAddress}
                          editable={true}
                          onChangeText={value =>
                            this.handleInput('emailAddress', value)
                          }
                        />
                      </View>
                      {this.state.isValidEmail == false ? (
                        <View>
                          <Text
                            style={{
                              fontFamily: fontBold,
                              color: 'red',
                              fontSize: 12,
                              paddingLeft: 20,
                            }}
                          >
                            Please enter a valid email address
                          </Text>
                        </View>
                      ) : null}
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 10 }}>
                    <Col size={6} style={styles.sectionButtonArea}>
                      <ButtonRounded
                        label="SAVE"
                        onClick={this.informationPassengers}
                      />
                    </Col>
                  </Row>
                </Grid>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL FORM ===================================== */}

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 2}
          onBackButtonPress={() => this._toggleModal(1)}
          onBackdropPress={() => this._toggleModal(1)}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#d5d5d5',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col size={8} style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Choose Title
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ paddingLeft: 10 }}>
                {this.state.listTitle.map((data, index) => {
                  return (
                    <Grid
                      key={index}
                      style={{
                        flex: 1,
                        backgroundColor: '#FFFFFF',
                        padding: 10,
                        borderBottomColor: '#d5d5d5',
                        borderBottomWidth: 0.5,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setTitlePassenger(data)}
                        style={{ flex: 1 }}
                      >
                        <Col style={{ alignItems: 'flex-start' }}>
                          <Text
                            style={{
                              fontFamily: fontReguler,
                              color: '#000',
                              fontSize: 16,
                            }}
                          >
                            {data.title}
                          </Text>
                        </Col>
                        <Col
                          style={{
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}
                        >
                          {this.state.titleSelected == data.index ? (
                            <Icon
                              name="check"
                              type="feather"
                              color="#008195"
                              size={22}
                            />
                          ) : (
                            <Text></Text>
                          )}
                        </Col>
                      </TouchableOpacity>
                    </Grid>
                  );
                })}
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}

        {/* =============================== LOGIN MODAL ===================================== */}
        <LoginModal
          isVisible={this.state.visibleModal === 99999}
          onDismiss={() => this.setState({ visibleModal: null })}
          goRegister={() =>
            this.setState({ visibleModal: null }, () =>
              this.props.navigation.navigate('SignUp')
            )
          }
          callbackLogin={() => this.handleLogin()}
        />
        {/* =============================== LOGIN MODAL ===================================== */}
      </Grid>
    );
  }
}
function mapStateToProps(state) {
  return {
    contact: state.flight.contactDetail,
    singleSearchFetch: state.flight.singleSearchFetch,
    loading: state.login.fetchingLogin,
    dataLogin: state.login.data,
    isLogin: state.login.isLogin,
    dataProfile: state.profile.data,
  };
}
export default connect(mapStateToProps)(Contact);
