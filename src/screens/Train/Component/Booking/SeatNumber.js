import React from 'react';
import { Text, StyleSheet, Dimensions, View, Image } from 'react-native';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontReguler } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class Recipient extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  _pressed = () => {
    alert('Seat Number');
  };

  render() {
    return (
      <View style={styles.content}>
        <Grid style={styles.card}>
          <Col
            size={3}
            style={{ justifyContent: 'center', aligmentItem: 'center' }}
          >
            <Row style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Image
                source={require('../../../../assets/icons/seat.png')}
                style={{ height: 35, width: 45 }}
              />
            </Row>
          </Col>
          <Col size={5}>
            <Row>
              <Text style={styles.textTitle}>Choose the best seat for the</Text>
            </Row>
            <Row>
              <Text style={styles.textTitle}>comfort of your trip!</Text>
            </Row>
          </Col>
          <Col size={2}>
            <Row>
              <ButtonPlus onClick={this._pressed} />
            </Row>
          </Col>
        </Grid>
      </View>
    );
  }
}

export default Recipient;

const styles = StyleSheet.create({
  content: {
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20,
    flex: 1,
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
    marginTop: 15,
  },
  textTitle: {
    color: '#424242',
    fontSize: 14,
    fontFamily: fontReguler,
  },
});
