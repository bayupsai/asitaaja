import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { fontExtraBold, fontReguler } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class ReturnDetails extends React.PureComponent {
  render() {
    return (
      <View style={styles.card}>
        <View style={{ padding: 10 }}>
          <Text style={{ color: '#009688', fontSize: 18 }}>Return Details</Text>
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderColor: '#BDBDBD',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              borderBottomWidth: 2,
              borderColor: '#009688',
              width: '25%',
              marginLeft: 15,
            }}
          />
        </View>
        <Grid style={{ padding: 10 }}>
          <Row style={{ marginTop: 10 }}>
            <Col>
              <Text style={styles.textBold}>Train Details</Text>
              <Text style={styles.textBody}>
                {this.props.trainName} (Sub-Class {this.props.subClass})
              </Text>
            </Col>
          </Row>
          <Row style={{ marginTop: 10 }}>
            <Col>
              <Text style={styles.textBold}>Return Details</Text>
              <Text style={styles.textBody}>
                {moment(this.props.departureDate).format('ddd, D MMMM YYYY ')}
              </Text>
            </Col>
          </Row>

          <Row
            style={{ marginTop: 10 }}
            style={{
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              paddingTop: 10,
              paddingBottom: 10,
            }}
          >
            <Col
              size={1}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row style={{ flex: 0, flexDirection: 'row', marginBottom: 5 }}>
                <Icon name="ios-radio-button-on" size={18} color="#0387cc" />
              </Row>
              <Row style={{ flex: 0, flexDirection: 'row' }}>
                <Dash style={styles.dash} dashColor="#0387cc" />
              </Row>
              <Row style={{ flex: 0, flexDirection: 'row', marginTop: 5 }}>
                <Icon name="ios-radio-button-off" size={18} color="#0387cc" />
              </Row>
            </Col>
            <Col
              size={2}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row>
                <Text style={styles.textTime}>{this.props.departureTime}</Text>
              </Row>
              <Row>
                <Text style={styles.textDate}>
                  {moment(this.props.departureDate).format('DD MMM')}
                </Text>
              </Row>
              <Row></Row>
              <Row></Row>
              <Row></Row>
              <Row>
                <Text style={styles.textTime}>{this.props.arrivalTIme}</Text>
              </Row>
              <Row>
                <Text style={styles.textDate}>
                  {moment(this.props.arrivalDate).format('DD MMM')}
                </Text>
              </Row>
            </Col>
            <Col
              size={1}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row>
                <Text style={styles.textTime}>-</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>-</Text>
              </Row>
              <Row></Row>
              <Row></Row>
              <Row></Row>
              <Row>
                <Text style={styles.textTime}>-</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>-</Text>
              </Row>
            </Col>
            <Col
              size={3}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row>
                <Text style={styles.textTime}>{this.props.departureName}</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>
                  {this.props.departureName} Station
                </Text>
              </Row>
              <Row></Row>
              <Row></Row>
              <Row></Row>
              <Row>
                <Text style={styles.textTime}>{this.props.arrivalName}</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>
                  {this.props.arrivalName} Station
                </Text>
              </Row>
            </Col>
            <Col size={3}></Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

export default ReturnDetails;

const styles = StyleSheet.create({
  card: {
    width: deviceWidth / 1.1,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    backgroundColor: '#fff',
    borderRadius: 5,
    transform: [{ translateY: -45 }],
  },
  textBold: { color: '#424242', fontSize: 16, fontFamily: fontExtraBold },
  textDate: { fontSize: 10, color: '#0066b3', fontFamily: fontReguler },
  textTime: { fontSize: 14, fontFamily: fontExtraBold, color: '#424242' },
  textBody: { fontSize: 14, color: '#424242', fontFamily: fontReguler },
  dash: {
    marginLeft: 7,
    width: 1,
    height: 100,
    flexDirection: 'column',
    transform: [{ translateY: 0 }, { translateX: 0 }],
  },
});
