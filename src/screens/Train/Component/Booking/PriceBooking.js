import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import numeral from 'numeral';
import { fontReguler, fontExtraBold } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class PriceBookingTrain extends React.PureComponent {
  render() {
    return (
      <View style={styles.card}>
        <View style={{ padding: 10 }}>
          <Text style={{ color: '#009688', fontSize: 18 }}>Price Details</Text>
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderColor: '#BDBDBD',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              borderBottomWidth: 2,
              borderColor: '#009688',
              width: '25%',
              marginLeft: 15,
            }}
          />
        </View>
        <Grid style={{ paddingLeft: 20, paddingRight: 20, paddingBottom: 10 }}>
          <Row style={{ paddingTop: 10 }}>
            <Col size={6}>
              <Row>
                <Text style={styles.textBold}>Departure Train</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>
                  {this.props.train_name_departure}
                </Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>
                  (Sub-Class {this.props.sub_class_departure})
                </Text>
              </Row>
            </Col>
            <Col
              size={4}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row></Row>
              <Row>
                <Text style={styles.textPrice}>
                  Rp{numeral(this.props.price_depart).format('0,0')}
                </Text>
              </Row>
              <Row></Row>
            </Col>
          </Row>

          <Row>
            <Col>
              <Dash
                style={{ width: '100%', height: 1, marginTop: 30 }}
                dashColor="#BDBDBD"
              />
            </Col>
          </Row>

          {this.props.return}

          <Row>
            <Col>
              <Dash
                style={{ width: '100%', height: 1, marginTop: 30 }}
                dashColor="#BDBDBD"
              />
            </Col>
          </Row>
          <Row>
            <Col size={6}>
              <Row>
                <Text style={styles.textBold}>Travel Insurance</Text>
              </Row>
              <Row>
                <Text style={styles.textBody}>1x</Text>
              </Row>
              <Row></Row>
            </Col>
            <Col
              size={4}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row></Row>
              <Row>
                <Text style={styles.textPrice}>
                  Rp{numeral(39000).format('0,0')}
                </Text>
              </Row>
              <Row></Row>
            </Col>
          </Row>
          <Row>
            <Col>
              <Dash
                style={{ width: '100%', height: 1, marginTop: 40 }}
                dashColor="#BDBDBD"
              />
            </Col>
          </Row>
          <Row style={{ paddingBottom: 20 }}>
            <Col size={6}>
              <Row>
                <Text style={styles.textBold}>Price You Pay</Text>
              </Row>
              <Row></Row>
            </Col>
            <Col
              size={4}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Row>
                <Text style={styles.textTotalPrice}>
                  Rp{numeral(this.props.totalPrice).format('0,0')}
                </Text>
              </Row>
              <Row></Row>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

export default PriceBookingTrain;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    width: deviceWidth / 1.1,
    height: deviceHeight / 1.5,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    backgroundColor: '#fff',
    borderRadius: 5,
  },
  textBody: { fontSize: 14, color: '#424242', fontFamily: fontReguler },
  textBold: { color: '#424242', fontSize: 14, fontFamily: fontExtraBold },
  textPrice: { color: '#424242', fontSize: 16, fontFamily: fontReguler },
  textTotalPrice: { fontSize: 16, color: '#FFA000', fontFamily: fontExtraBold },
});
