import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';

//local component
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import { fontReguler } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

const Passenger = props => {
  return (
    <View style={styles.content}>
      <Grid style={styles.card}>
        <Col size={6}>
          <Text style={styles.textTitle}>{props.fullnamePassenger}</Text>
        </Col>
        <Col size={4} style={{ alignItems: 'flex-end' }}>
          <ButtonPlus onClick={props.onPress} />
        </Col>
      </Grid>
    </View>
    // <View style={{ backgroundColor: "#f0f0f0", flex: 1 }}>
    //     <Grid style={{ paddingLeft: 20, paddingBottom: 20 }}>
    //         <Row><Text style={styles.textBold}>Contact Information (for E-Ticket)</Text></Row>
    //     </Grid>
    //     <Grid style={styles.sectionInput}>
    //         <Col size={3}>
    //             <TouchableOpacity onPress={() => alert('Modal')}>
    //                 <InputText placeholder="Mrs" label="Title*" editable={false} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //         <Col size={7}>
    //             <TouchableOpacity onPress={() => alert('Modal')}>
    //                 <InputText placeholder="e.g Amir Revon" label="Fullname*" editable={false} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //     </Grid>
    //     <Grid style={styles.sectionInput}>
    //         <Col size={3}>
    //             <TouchableOpacity onPress={() => alert('Modal')}>
    //                 <InputText placeholder="(+62)" label="Code*" editable={false} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //         <Col size={7}>
    //             <TouchableOpacity onPress={() => alert('Modal')}>
    //                 <InputText placeholder="e.g 8218375003" label="Phone Number*" editable={false} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //     </Grid>
    //     <Grid>
    //         <Col>
    //             <TouchableOpacity onPress={() => alert('Input')}>
    //                 <InputText placeholder="e.g youremail@email.com" label="Email Address" editable={true} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //     </Grid>

    //     <Grid style={{ padding: 20 }}>
    //         <Row style={{ paddingTop: 15 }}><Text style={styles.textBold}>Passenger Details</Text></Row>
    //     </Grid>
    //     <Grid style={styles.sectionInput}>
    //         <Col size={3}>
    //             <TouchableOpacity onPress={() => alert('Modal')}>
    //                 <InputText placeholder="Mrs" label="Title*" editable={false} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //         <Col size={7}>
    //             {/* <TouchableOpacity onPress={() => alert('Modal') }> */}
    //             <InputText placeholder="e.g Amir Revon" label="Fullname*" editable={true} onChangeText={this.props.passFullname} />
    //             {/* </TouchableOpacity> */}
    //         </Col>
    //     </Grid>
    //     <Grid style={styles.sectionInput}>
    //         <Col size={3}>
    //             <TouchableOpacity onPress={() => alert('Modal')}>
    //                 <InputText placeholder="(+62)" label="Code*" editable={false} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //         <Col size={7}>
    //             {/* <TouchableOpacity onPress={() => alert('Modal')}> */}
    //             <InputText placeholder="e.g 8218375003" label="Phone Number*" editable={true} keyboardType="number-pad" onChangeText={this.props.passPhone} />
    //             {/* </TouchableOpacity> */}
    //         </Col>
    //     </Grid>
    //     <Grid>
    //         <Col>
    //             <TouchableOpacity onPress={() => alert('Inputaan')}>
    //                 <InputText placeholder="1234 - 567 - 890" label="Id Numbers" editable={true} onChangeText={(data) => this.handleCHange(data)} />
    //             </TouchableOpacity>
    //         </Col>
    //     </Grid>
    //     <Grid>
    //         <Col>
    //             {/* <TouchableOpacity onPress={() => alert('Inputaan')}> */}
    //             <InputText placeholder="e.g yourname@email.com" label="email" editable={true} keyboardType="email-address" onChangeText={this.props.passEmail} />
    //             {/* </TouchableOpacity> */}
    //         </Col>
    //     </Grid>
    //     <Grid>
    //         <Col>
    //             <TouchableOpacity onPress={this.props.pressBirthDate}>
    //                 <InputText placeholder={this.props.passBirthDate} label="Birth Date" editable={false} onChangeText={(i)=> console.log(i)} />
    //             </TouchableOpacity>
    //         </Col>
    //     </Grid>
    // </View>
  );
};

export default Passenger;

const styles = StyleSheet.create({
  content: {
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20,
    flex: 1,
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
    marginTop: 15,
  },
  textTitle: {
    color: '#424242',
    fontSize: 14,
    fontFamily: fontReguler,
  },
});
