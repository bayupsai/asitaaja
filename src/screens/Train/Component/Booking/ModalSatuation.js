import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { TextInputMask } from 'react-native-masked-text';
import Modal from 'react-native-modal';

//local component
import { InputText } from '../../../../elements/TextInput';
import { Button } from '../../../../elements/Button';
import { fontBold } from '../../../../base/constant';

const ModalSatuation = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visibleModal === 3}
      onBackButtonPress={props.onDismiss}
      onBackdropPress={props.onDismiss}
      style={styles.bottomModal}
    >
      <View style={styles.modalSeatClass}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#dadada',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={props.closeModal}
          >
            <Col size={2} style={{ alignItems: 'flex-start', paddingLeft: 10 }}>
              <Icon name="close" type="evilIcon" color="#008195" size={20} />
            </Col>
            <Col
              size={8}
              style={{ alignItems: 'flex-start', paddingLeft: '25%' }}
            >
              <Text
                style={{ color: '#000', fontFamily: fontBold, fontSize: 16 }}
              >
                Satuation
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid style={{ padding: 10 }}>{props.content}</Grid>
      </View>
    </Modal>
  );
};

export default ModalSatuation;

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 0 },
  title: { color: '#424242', fontWeight: 'bold', fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center' },
  contactName: { fontSize: 16, color: '#434a5e' },
  titleChecklist: { fontSize: 14, color: '#434a5e', fontWeight: '400' },
  sectionButton: { alignItems: 'flex-end' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
