import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { fontReguler } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class Insurance extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Grid style={styles.card}>
          <Col size={1}>
            <Row style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <TouchableOpacity onPress={this.props.onPress}>
                <Icon
                  type="ionicon"
                  name={
                    this.props.icon == true
                      ? 'ios-checkmark-circle'
                      : 'ios-remove-circle-outline'
                  }
                  color="#009688"
                />
              </TouchableOpacity>
            </Row>
          </Col>
          <Col size={9}>
            <Row>
              <Col size={6}>
                <Text style={{ color: '#000', fontFamily: fontReguler }}>
                  Travel Insurance
                </Text>
              </Col>
              <Col size={4}>
                <Text style={{ color: '#4CAF50', fontFamily: fontReguler }}>
                  Rp. 39.000 <Text style={{ color: '#BDBDBD' }}>/pax</Text>
                </Text>
              </Col>
            </Row>
            <Row>
              <Text style={{ fontFamily: fontReguler }}>
                Up to Rp300 million coverage for accidents,
              </Text>
            </Row>
            <Row>
              <Text style={{ fontFamily: fontReguler }}>
                up top Rp20 million for trip cancellation,
              </Text>
            </Row>
            <Row>
              <Text style={{ fontFamily: fontReguler }}>
                and up to Rp7.5 million for flight and
              </Text>
            </Row>
            <Row>
              <Text style={{ fontFamily: fontReguler }}>baggage delay</Text>
            </Row>
            <Row>
              <Text style={{ color: '#2196F3', fontFamily: fontReguler }}>
                More info
              </Text>
            </Row>

            <Row>
              <Col size={9}>
                <Text style={{ color: '#FFC107', fontFamily: fontReguler }}>
                  Including five others protection
                </Text>
              </Col>
              <Col size={1}>
                <Icon type="ionicon" name="ios-arrow-down" color="#FFC107" />
              </Col>
            </Row>
          </Col>
        </Grid>
      </View>
    );
  }
}

export default Insurance;

const styles = StyleSheet.create({
  card: {
    width: deviceWidth / 1.1,
    height: deviceHeight / 3.5,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
  },
});
