import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { TextInputMask } from 'react-native-masked-text';
import Modal from 'react-native-modal';

//local component
import { InputText } from '../../../../elements/TextInput';
import { Button } from '../../../../elements/Button';
import { fontBold } from '../../../../base/constant';

const ModalPassenger = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visibleModal === 2}
      onBackButtonPress={props.onDismiss}
      onBackdropPress={props.onDismiss}
      style={styles.bottomModal}
    >
      <View style={styles.modalSeatClass}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Grid
            style={{
              width: deviceWidth,
              height: 40,
              flex: 0,
              justifyContent: 'center',
              alignItems: 'center',
              borderBottomColor: '#dadada',
              borderBottomWidth: 0.5,
            }}
          >
            <TouchableOpacity
              style={{ flex: 1, flexDirection: 'row' }}
              onPress={props.closeModal}
            >
              <Col
                size={2}
                style={{ alignItems: 'flex-start', paddingLeft: 10 }}
              >
                <Icon name="close" type="evilIcon" color="#008195" size={20} />
              </Col>
              <Col
                size={8}
                style={{ alignItems: 'flex-start', paddingLeft: '25%' }}
              >
                <Text
                  style={{ color: '#000', fontFamily: fontBold, fontSize: 16 }}
                >
                  Passenger
                </Text>
              </Col>
            </TouchableOpacity>
          </Grid>
          <Grid>
            <Col style={{ backgroundColor: '#FFFFFF' }}>
              <Grid style={{ paddingTop: 20 }}>
                <Row style={{ flex: 0, marginBottom: 20 }}>
                  <Col size={3}>
                    <TouchableOpacity onPress={props.clickSatuation}>
                      <InputText
                        size="small"
                        placeholder={props.satuation}
                        label="Title *"
                        editable={false}
                      />
                    </TouchableOpacity>
                  </Col>
                  <Col size={7} style={{ marginLeft: 0 }}>
                    <InputText
                      placeholder="e.g Amir Budi"
                      label="Fullname *"
                      value={props.fullnamePassenger}
                      editable={true}
                      onChangeText={props.changeFullnamePassenger}
                    />
                  </Col>
                </Row>
                <Row style={{ flex: 0, marginBottom: 20 }}>
                  <Col>
                    <InputText
                      placeholder="082111xxxxx"
                      label="Phone Number *"
                      value={props.phonePassenger}
                      keyboardType="phone-pad"
                      editable={true}
                      onChangeText={props.changePhonePassenger}
                    />
                  </Col>
                </Row>
                <Row style={{ flex: 0, marginBottom: 20 }}>
                  <Col>
                    <InputText
                      placeholder="yourmail@mail.com"
                      label="Email Address *"
                      value={props.emailPassenger}
                      keyboardType="email-address"
                      editable={true}
                      onChangeText={props.changeEmailPassenger}
                    />
                  </Col>
                </Row>
                <Row style={{ flex: 0, marginBottom: 20 }}>
                  <Col>
                    <TextInputMask
                      type={'datetime'}
                      options={{
                        format: 'YYYY-MM-DD',
                      }}
                      placeholder="1989-11-17"
                      value={props.birthDate}
                      placeholderTextColor="#838383"
                      onChangeText={props.changeBirthDate}
                      style={{
                        padding: 10,
                        paddingTop: 6,
                        paddingBottom: 6,
                        backgroundColor: '#FFFFFF',
                        marginLeft: 20,
                        marginRight: 20,
                        borderRadius: 5,
                        borderWidth: 0.5,
                        borderColor: '#e6e6e6',
                      }}
                    />
                  </Col>
                </Row>
                <Row style={{ flex: 0, marginBottom: 20 }}>
                  <Col>
                    <InputText
                      placeholder="Your ID Card in KTP / Passport"
                      label="ID Card (KTP / Passport) *"
                      value={props.id_card}
                      keyboardType="numeric"
                      editable={true}
                      onChangeText={props.changeIdNumber}
                    />
                  </Col>
                </Row>
                <Row style={{ flex: 0, marginBottom: 10 }}>
                  <Col size={6} style={styles.sectionButtonArea}>
                    <Button label="DONE" onClick={props.changePassenger} />
                  </Col>
                </Row>
              </Grid>
            </Col>
          </Grid>
        </ScrollView>
      </View>
    </Modal>
  );
};

export default ModalPassenger;

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 0 },
  title: { color: '#424242', fontWeight: 'bold', fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center' },
  contactName: { fontSize: 16, color: '#434a5e' },
  titleChecklist: { fontSize: 14, color: '#434a5e', fontWeight: '400' },
  sectionButton: { alignItems: 'flex-end' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 1.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
