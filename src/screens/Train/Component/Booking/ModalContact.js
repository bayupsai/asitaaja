import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';

//local component
import { InputText } from '../../../../elements/TextInput';
import { Button } from '../../../../elements/Button';
import { fontBold } from '../../../../base/constant';
//local component

const ModalContact = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visibleModal === 1}
      style={styles.bottomModal}
    >
      <View style={styles.modalSeatClass}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#838383',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={props.closeModal}
          >
            <Col size={2} style={{ alignItems: 'flex-start', paddingLeft: 10 }}>
              <Icon name="close" type="evilIcon" color="#008195" size={20} />
            </Col>
            <Col
              size={8}
              style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
            >
              <Text
                style={{ color: '#000', fontFamily: fontBold, fontSize: 16 }}
              >
                Contact Details
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid>
          <Col style={{ backgroundColor: '#FFFFFF' }}>
            <Grid style={{ paddingTop: 20 }}>
              <Row style={{ flex: 0, marginBottom: 20 }}>
                {/* <Col size={3}>
                                    <TouchableOpacity onPress={() => this._toggleModal(2)}>
                                        <InputText size="small" placeholder={(this.props.contact.title) ? this.props.contact.title : "Mr"} label="Title *" editable={false} />
                                    </TouchableOpacity>
                                </Col> */}
                <Col size={7} style={{ marginLeft: 0 }}>
                  <InputText
                    placeholder={'e.g Amir Budi'}
                    label="Fullname *"
                    editable={true}
                    value={props.fullnameContact}
                    onChangeText={props.changeFullnameContact}
                  />
                </Col>
              </Row>
              <Row style={{ flex: 0, marginBottom: 20 }}>
                <Col>
                  <InputText
                    placeholder={'082111xxxxx'}
                    label="Phone Number *"
                    keyboardType="phone-pad"
                    editable={true}
                    value={props.phoneContact}
                    onChangeText={props.changePhoneContact}
                  />
                </Col>
              </Row>
              <Row style={{ flex: 0, marginBottom: 20 }}>
                <Col>
                  <InputText
                    placeholder={'e.g yourname@email.com'}
                    label="Email Address *"
                    keyboardType="email-address"
                    editable={true}
                    value={props.emailContact}
                    onChangeText={props.changeEmailContact}
                  />
                </Col>
              </Row>
              <Row style={{ flex: 0, marginBottom: 10 }}>
                <Col size={6} style={styles.sectionButtonArea}>
                  <Button label="DONE" onClick={props.changeContact} />
                </Col>
              </Row>
            </Grid>
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default ModalContact;

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 10 },
  title: { color: '#424242', fontWeight: 'bold', fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center' },
  contactName: { fontSize: 16, color: '#434a5e' },
  sectionButton: { alignItems: 'flex-end' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 1.75,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
