import React, { PureComponent } from 'react';
import { View, StyleSheet, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import { thameColors } from '../../../base/constant';
import { scale } from '../../../Const/ScaleUtils';
import HeaderSearch from '../../../components/Header/HeaderSearch';
import MovieCarousel from '../Component/MovieCarousel';
import MovieList from '../Component/MovieList';
import FabButton from '../Component/FabButton';
import { BarStyle } from '../../../elements/BarStyle';

const actions = [
  {
    text: '',
    icon: require('../../../assets/icons/activities_icon.png'),
    name: 'Activities',
    position: 1,
    color: thameColors.secondary,
  },
  {
    text: '',
    icon: require('../../../assets/icons/attraction_icon.png'),
    name: 'Events',
    position: 2,
    color: thameColors.secondary,
  },
];

class AttractionMovies extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      placeholder: 'Jakarta',
    };
  }
  goBack = () => {
    const { goBack } = this.props.navigation;
    goBack();
  };
  _onDropDown = () => {
    alert('Find Movies');
  };

  //Main Render
  render() {
    const { container, imgIcon } = styles;
    const { placeholder } = this.state;
    return (
      <View style={container}>
        <BarStyle />
        <ScrollView showsVerticalScrollIndicator={false}>
          <HeaderSearch
            placeholder={placeholder}
            onChangeText={text => this.setState({ search: text })}
            onPress={this.goBack}
            onPressRight={this._onDropDown}
            dropDown={true}
            onDropDown={this._onDropDown}
          >
            <Image
              source={require('../../../assets/icons/theater_icon.png')}
              style={imgIcon}
            />
          </HeaderSearch>
          <MovieCarousel {...this.props} />
          <MovieList {...this.props} />
        </ScrollView>
        <FabButton actions={actions} onPressItem={name => alert(name)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(AttractionMovies);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.white,
  },
  imgIcon: {
    width: scale(25),
    height: scale(25),
  },
});
