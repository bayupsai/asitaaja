const slide = [
  { id: 1, img: require('../../assets/attraction/attraction.png') },
  { id: 2, img: require('../../assets/attraction/attraction.png') },
  { id: 3, img: require('../../assets/attraction/attraction.png') },
];

const offers = [
  {
    id: 1,
    img: require('../../assets/icons/activities_rounded.png'),
    title: 'Activities',
    navigate: 'AttractionActivities',
  },
  {
    id: 2,
    img: require('../../assets/icons/movies_rounded.png'),
    title: 'Movies',
    navigate: 'AttractionMovies',
  },
  {
    id: 3,
    img: require('../../assets/icons/events_rounded.png'),
    title: 'Events',
    navigate: 'AttractionEvents',
  },
];

const activities = [
  {
    id: 1,
    img: require('../../assets/attraction/popular1.png'),
    title: 'Taman Safari Bogor',
    location: 'Bogor, Indonesia',
    price: '343274',
  },
  {
    id: 2,
    img: require('../../assets/attraction/popular2.png'),
    title: 'Jungleland Bogor',
    location: 'Bogor, Indonesia',
    price: '120120',
  },
  {
    id: 3,
    img: require('../../assets/attraction/popular3.png'),
    title: 'Bali Safari Park',
    location: 'Bali, Indonesia',
    price: '250000',
  },
  {
    id: 4,
    img: require('../../assets/attraction/popular4.png'),
    title: 'Underwater Park',
    location: 'Bali, Indonesia',
    price: '550000',
  },
];

const movies = [
  {
    id: 1,
    img: require('../../assets/attraction/movie1.png'),
    title: 'John Wick',
    genre: 'Action',
  },
  {
    id: 2,
    img: require('../../assets/attraction/movie2.png'),
    title: 'Moonlight',
    genre: 'Drama',
  },
  {
    id: 3,
    img: require('../../assets/attraction/movie3.png'),
    title: 'Alita: Battle Angle ',
    genre: 'Action, Fantasy',
  },
  {
    id: 4,
    img: require('../../assets/attraction/movie4.png'),
    title: 'Aquaman',
    genre: 'Action, Fantasy',
  },
  {
    id: 5,
    img: require('../../assets/attraction/movie5.png'),
    title: 'Deadpool 2',
    genre: 'Action, Comedy',
  },
  {
    id: 6,
    img: require('../../assets/attraction/movie6.jpg'),
    title: 'Spider-Man: Far From Home',
    genre: 'Action, Fantasy',
  },
];

const events = [
  {
    id: 1,
    img: require('../../assets/attraction/event1.png'),
    title: 'Jakarta Jazz Festival',
    dateStart: '22 Jul 19',
    dateEnd: '26 Jul 19',
    price: 800000,
  },
  {
    id: 2,
    img: require('../../assets/attraction/event2.png'),
    title: 'Lanny Concert',
    dateStart: '27 Jul 19',
    dateEnd: '29 Jul 19',
    price: 703144,
  },
  {
    id: 3,
    img: require('../../assets/attraction/event3.png'),
    title: 'Night Music Live',
    dateStart: '27 Jul 19',
    dateEnd: '29 Jul 19',
    price: 553124,
  },
  {
    id: 4,
    img: require('../../assets/attraction/event4.png'),
    title: 'Sound Music Fest',
    dateStart: '27 Jul 19',
    dateEnd: '29 Jul 19',
    price: 268999,
  },
];

export { slide, offers, activities, movies, events };
