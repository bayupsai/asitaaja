import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { thameColors, fontExtraBold } from '../../base/constant';
import HeaderSearch from '../../components/Header/HeaderSearch';
import { BarStyle } from '../../elements/BarStyle';
import Menu from './Component/Menu';
import Banners from './Component/Banners';
import PopularActivities from './Component/PopularActivities';
import PopularMovies from './Component/PopularMovies';
import PopularEvents from './Component/PopularEvents';

class Attraction extends PureComponent {
  _onDropDown = () => {
    alert('Attraction');
  };
  backHeader = () => {
    const { goBack } = this.props.navigation;
    goBack();
  };
  render() {
    const { container } = styles;
    return (
      <View style={container}>
        <BarStyle />
        <ScrollView showsVerticalScrollIndicator={false}>
          <HeaderSearch
            placeholder="Find places or activities"
            onChangeText={text => console.log(text)}
            onPress={this.backHeader}
            onPressRight={this._onDropDown}
          >
            <Icon
              type="feather"
              name="bookmark"
              color={thameColors.white}
              size={30}
            />
          </HeaderSearch>
          <Banners {...this.props} />
          <Menu {...this.props} />
          <PopularActivities {...this.props} />
          <PopularMovies {...this.props} />
          <PopularEvents {...this.props} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(Attraction);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  textExtraBold: {
    fontFamily: fontExtraBold,
    fontSize: 20,
    color: thameColors.superBack,
  },
});
