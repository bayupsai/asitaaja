import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import moment from 'moment';
import _ from 'lodash';
import HeaderPage from '../../../components/HeaderPage';
import styles from '../Component/style';
import SubHeaderPage from '../../../components/SubHeaderPage';
import { InputTextRevamp } from '../../../elements/TextInput';
import CardBooking from '../Component/CardBooking';
import { activities } from '../data';
import DetailFooter from '../Component/DetailFooter';
import ModalDate from '../Component/ModalDate';
import { connect } from 'react-redux';
import { actionDateAttraction } from '../../../redux/actions/AttractionAction';
import { thameColors } from '../../../base/constant';

class OrderAttractionActivities extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      qty: 0,
      isVisible: null,
      date: null,
      startDate: null,
      data: [],
    };
  }
  componentDidMount() {
    const data = [];
    data.push(activities);
    this.setState({ data });
  }
  onDismiss = () => {
    this.setState({ isVisible: null });
  };
  openDate = () => {
    // Show Date Picker
    this.setState({ isVisible: 1 });
  };
  onChangeDate = ({ date }) => {
    //Search Date for Ticket
    this.setState(
      {
        startDate: moment(date).format('YYYY-MM-DD'),
        isVisible: null,
      },
      () => {
        this.props.dispatch(
          actionDateAttraction(this.state.startDate, 'setDateActivities')
        );
      }
    );
  };
  action = (type, value) => {
    //Add quantity Ticket
    if (type == '-') {
      if (this.state.qty === 0) {
        this.setState({ qty: 0 });
      } else {
        this.setState({ qty: this.state.qty - value });
      }
    } else if (type == '+') {
      if (this.state.qty == 7) {
        this.setState({ qty: 7 });
      } else {
        this.setState({ qty: this.state.qty + value });
      }
    }
  };
  addData = (title, value) => {
    let addData = [];
    addData.push([{ title, id: value }]);
    this.setState({
      data: addData,
    });
  };

  //Main Render
  render() {
    const { container, textBold } = styles;
    let { isVisible, date, startDate, data } = this.state;
    return (
      <View style={container}>
        <HeaderPage title="Ticket" callback={this.goBack} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <InputTextRevamp
            style={{ marginTop: -32.5 }}
            source={require('../../../assets/icons/icon_departure.png')}
            onPress={this.openDate}
            contentInput={
              <Text style={textBold}>
                {startDate == null
                  ? 'Select Date'
                  : moment(startDate).format('ddd, D MMM YYYY')}
              </Text>
            }
          />
          {startDate === null ? (
            <View style={{ width: 0, height: 0 }} />
          ) : (
            activities.map((item, index) => {
              return (
                <CardBooking
                  key={index}
                  title={item.title}
                  qty={this.state.qty}
                  minus={() => this.addData('-', item.title, item)}
                  plus={() => this.addData(item.title, index)}
                />
              );
            })
          )}
        </ScrollView>
        <DetailFooter
          label="BOOK NOW"
          type="ticket"
          labelType={`${data} Ticket`}
          active={data.length !== 0 ? true : false}
          onPress={
            data.length !== 0
              ? () => alert(JSON.stringify(data))
              : () => console.log('Data is 0')
          }
        />

        {/* Modal */}
        <ModalDate
          isVisible={isVisible === 1}
          dismiss={this.onDismiss}
          title="Select Date"
          date={date}
          startDate={startDate}
          selectedDate={startDate}
          changeDate={this.onChangeDate}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(OrderAttractionActivities);
