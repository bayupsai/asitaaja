import React, { Component } from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Animated,
} from 'react-native';
import { connect } from 'react-redux';
import { actionTicketAttraction } from '../../../redux/actions/AttractionAction';
import { thameColors } from '../../../base/constant';
import DetailCarousel from '../Component/DetailCarousel';
import { activities } from '../data';
import DetailItem from '../Component/DetailItem';
import styles from '../Component/style';
import { scale, verticalScale } from '../../../Const/ScaleUtils';
import DetailFooter from '../Component/DetailFooter';
import DetailModal from '../Component/DetailModal';

class DetailActivities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      visibleModal: null,
    };
  }
  hideModal = () => {
    this.setState({ visibleModal: null });
  };
  pressModal = value => {
    this.setState({ visibleModal: value });
  };
  buyTicket = (to, item) => {
    const { navigation, dispatch } = this.props;
    alert(`${to} ticket was sold out!`);
  };

  render() {
    const { container } = styles;
    const { scrollY, visibleModal } = this.state;
    const headerHeight = Animated.diffClamp(
      scrollY,
      0,
      verticalScale(300)
    ).interpolate({
      inputRange: [0, verticalScale(300)],
      outputRange: [0, -verticalScale(20)],
      extrapolate: 'clamp',
    });
    let { detailActivities } = this.props.attraction; //call redux data
    return (
      <View style={container}>
        <StatusBar
          barStyle={'default'}
          backgroundColor={'transparent'}
          translucent={true}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          // onScroll={Animated.event(
          //     [{
          //         nativeEvent:
          //             { contentOffset: { y: this.state.scrollY } }
          //     }], { useNativeDriver: true })
          // }
        >
          <DetailCarousel data={activities} />
          <DetailItem
            title={detailActivities.title}
            subTitle={detailActivities.location}
            onPressDescription={() => this.pressModal(1)}
            onPressTerms={() => this.pressModal(2)}
          />
        </ScrollView>
        {/* Buy Ticket */}
        <DetailFooter
          onPress={() => this.buyTicket('Activities')}
          label="SELECT TICKETS"
        />
        {/* Modal */}
        <DetailModal
          isVisible={visibleModal === 1}
          onDismiss={this.hideModal}
          onPress={this.hideModal}
          title="Description"
          content={contentText[0].text}
        />
        <DetailModal
          isVisible={visibleModal === 2}
          onDismiss={this.hideModal}
          onPress={this.hideModal}
          title="Terms and Conditions"
          content={
            contentText[1].text1 + contentText[1].text2 + contentText[1].text3
          }
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(DetailActivities);

const contentText = [
  {
    text:
      'Bali Safari and Marine Park is a 400,000-square-metre conservation centre that serves as the home of more than 60 species of animals. These include Himalayan bears, baboons, blue wildebeests, African hippos, and Indian white tigers. Apart from seeing them in shelters that mimic their natural habitats, you can also watch them do tricks in multiple talent shows. To explore the park on a short period of time, join the Safari Journey on-board an air-conditioned tram to see the wildlife',
  },
  {
    text1: 'Price Includes : Admission fee',
    text2: 'Price Excludes : Tips, Gratuities, Personal Expenses, Meals',
    text3:
      'Warnings : For Indonesian visitors: Please book the option for Indonesian citizens, as the management reserves the right to deny admission when your KTP eligibility doesn’t correspond to the voucher details. During inclement weather, outdoor shows will be cancelled for the safety of the staff and guests. Children ages 11 and below must be accompanied by a paying adult at all times. Restaurants may change due to unforeseen circumstances.',
  },
];
