import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Icon } from 'react-native-elements';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import styles from '../Component/style';
import HeaderSearch from '../../../components/Header/HeaderSearch';
import { thameColors } from '../../../base/constant';
import SubMenu from '../Component/SubMenu';
import Card from '../Component/Card';
import { activities } from '../data';
import FabButton from '../Component/FabButton';
import { BarStyle } from '../../../elements/BarStyle';

const actions = [
  {
    text: '',
    icon: require('../../../assets/icons/movies_icon.png'),
    name: 'Movies',
    position: 2,
    color: thameColors.secondary,
    animated: false,
  },
  {
    text: '',
    icon: require('../../../assets/icons/attraction_icon.png'),
    name: 'Events',
    position: 1,
    color: thameColors.secondary,
    animated: false,
  },
];

class AttractionActivities extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      dataSubMenu: [
        { id: 1, title: 'Nature & Animals' },
        { id: 2, title: 'Waterparks' },
        { id: 3, title: 'Amusement Parks' },
        { id: 4, title: 'Danger Zone' },
      ],
      selectedSubMenu: 1,
    };
  }
  backHeader = () => {
    const { goBack } = this.props.navigation;
    goBack();
  };
  selectSubMenu = item => {
    this.setState({ selectedSubMenu: item });
  };
  _onDropDown = () => {
    alert('Activities');
  };
  navigateCard = (to, item) => {
    //Navigate to Detail Card
    const { navigation, dispatch } = this.props;
    dispatch(actionDetailAttraction(item, 'activities'));
    navigation.navigate(to);
  };

  //Render Card Content
  renderItem = (item, index) => (
    <Card
      key={index}
      source={item.img}
      onPress={() => this.navigateCard('DetailActivities', item)}
    >
      <Text style={styles.textBold}>{item.title}</Text>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 10,
          marginBottom: 10,
          alignItems: 'center',
        }}
      >
        <Icon
          type="material"
          name="location-on"
          color={thameColors.gray}
          size={20}
          iconStyle={{ marginRight: 7 }}
        />
        <Text style={[styles.textRegular, { color: thameColors.darkGray }]}>
          {item.location}
        </Text>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={[styles.textRegular, { color: thameColors.gray }]}>
          Start from
        </Text>
        <Text style={[styles.textBold, { color: thameColors.primary }]}>
          Rp
          {numeral(item.price)
            .format('0,0')
            .replace(/,/g, '.')}
        </Text>
      </View>
    </Card>
  );

  //Main Render
  render() {
    let { dataSubMenu, selectedSubMenu } = this.state;
    return (
      <View style={styles.container}>
        <BarStyle />
        <ScrollView showsVerticalScrollIndicator={false}>
          <HeaderSearch
            placeholder="Activities in Indonesia"
            onChangeText={text => this.setState({ search: text })}
            onPress={this.backHeader}
            onPressRight={this._onDropDown}
            dropDown={true}
            onDropDown={this._onDropDown}
          >
            <Icon
              type="feather"
              name="bookmark"
              color={thameColors.white}
              size={30}
            />
          </HeaderSearch>

          {/* Sub Menu */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {dataSubMenu.map((item, index) => (
              <SubMenu
                key={index}
                onPress={() => this.selectSubMenu(item.id)}
                label={item.title}
                selected={item.id === selectedSubMenu ? true : false}
                style={
                  index === 0 ? { marginLeft: 20 } : { position: 'relative' }
                }
              />
            ))}
          </ScrollView>

          {/* List Item */}
          <View style={{ marginTop: 15, marginBottom: 50 }}>
            {activities.map((item, index) => this.renderItem(item, index))}
          </View>
        </ScrollView>

        {/* Floating Button */}
        <FabButton actions={actions} onPressItem={name => alert(name)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(AttractionActivities);
