import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import Dates from 'react-native-dates';
import moment from 'moment';
import { thameColors, fontBold } from '../../../base/constant';

const window = Dimensions.get('window');

const ModalDate = props => {
  const isDateBlocked = date => date.isBefore(moment(), 'day');
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.isVisible}
      style={styles.bottomModal}
      onBackButtonPress={props.dismiss}
      onBackdropPress={props.dismiss}
    >
      <View style={styles.modalContent}>
        <Grid style={styles.modalHeaderDatepicker}>
          <Row style={{ paddingBottom: 7.5 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', flex: 1 }}
              onPress={props.dismiss}
            >
              <Col
                size={2}
                style={{ alignItems: 'center', justifyContent: 'center' }}
              >
                <Icon
                  name="close"
                  type="evilIcon"
                  color={thameColors.white}
                  size={28}
                />
              </Col>
              <Col size={6.5} style={styles.modalTitleSection}>
                <Text style={styles.modalTitle}>{props.title}</Text>
              </Col>
              <Col size={1.5} />
            </TouchableOpacity>
          </Row>
        </Grid>
        <Grid>
          <Col>
            <View style={{ flex: 1 }}>
              <Dates
                date={props.date}
                onDatesChange={props.changeDate}
                startDate={props.startDate}
                endDate={props.endDate}
                focusedInput={'startDate'}
                isDateBlocked={isDateBlocked}
                focusedInput={'startDate'}
              />
              <Text
                style={{
                  paddingTop: 20,
                  fontSize: 16,
                  color: thameColors.textBlack,
                  paddingLeft: 20,
                }}
              >
                {moment(props.selectedDate).format('DD MMM YYYY')}
              </Text>
            </View>
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default ModalDate;

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: window.height,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 45,
    width: window.width,
  },
  modalSearch: {
    flex: 0,
    height: 70,
    width: window.width,
    marginTop: -35,
    marginBottom: 10,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: window.width,
  },
  modalTitleSection: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    color: thameColors.white,
    fontFamily: fontBold,
    fontSize: 18,
  },
});
