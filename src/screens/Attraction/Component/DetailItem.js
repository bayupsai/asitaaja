import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Icon } from 'react-native-elements';
import style from './style';
import { thameColors } from '../../../base/constant';
import { scale, verticalScale } from '../../../Const/ScaleUtils';
import PopularActivities from './PopularActivities';

const window = Dimensions.get('window');

class DetailItem extends React.PureComponent {
  render() {
    const { container } = styles;
    const { textBold, textRegular, textSemiBold } = style;
    return (
      <View>
        {/* Title  */}
        <View
          style={[
            container,
            { flexDirection: 'row', justifyContent: 'space-between' },
          ]}
        >
          <View>
            <Text style={[textBold, { fontSize: 20 }]}>{this.props.title}</Text>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <Icon
                type="material"
                name="location-on"
                color={thameColors.darkGray}
                size={20}
                iconStyle={{ marginRight: 5 }}
              />
              <Text style={[textRegular, { color: thameColors.darkGray }]}>
                {this.props.subTitle}
              </Text>
            </View>
          </View>
          <TouchableOpacity onPress={this.props.onPressBookmark}>
            <Icon
              type="feather"
              name="bookmark"
              color={thameColors.textBlack}
              size={25}
              iconStyle={{ transform: [{ translateX: 7.5 }] }}
            />
          </TouchableOpacity>
        </View>
        {/* Time */}
        <View style={[container, { backgroundColor: thameColors.backWhite }]}>
          <View style={{ marginBottom: 10, flexDirection: 'row' }}>
            <Icon
              type="material-community"
              name="calendar-check"
              color={thameColors.textBlack}
              size={20}
              iconStyle={{ marginRight: 5 }}
            />
            <Text style={textSemiBold}>
              {this.props.date ? this.props.date : 'Available Today'}
            </Text>
          </View>
          <View style={{ marginBottom: 10, flexDirection: 'row' }}>
            <Image
              source={require('../../../assets/icons/time_icon.png')}
              style={{ width: scale(15), height: scale(15), marginRight: 5 }}
              resizeMode={'contain'}
            />
            <Text style={textSemiBold}>Opening Hours: 09:00 - 18:00</Text>
          </View>
          <TouchableOpacity
            onPress={this.props.onPressSchedule}
            style={{ marginLeft: 20 }}
          >
            <Text style={[textSemiBold, { color: thameColors.primary }]}>
              See All Opening Hours
            </Text>
          </TouchableOpacity>
        </View>
        {/* Description */}
        <TouchableOpacity
          onPress={this.props.onPressDescription}
          style={[
            container,
            {
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              borderBottomColor: thameColors.gray,
              borderBottomWidth: 0.5,
            },
          ]}
        >
          <Text style={textRegular}>Description</Text>
          <Icon
            type="ionicon"
            name="ios-arrow-down"
            color={thameColors.textBlack}
            size={25}
          />
        </TouchableOpacity>
        {/* Terms and Conditions */}
        <TouchableOpacity
          onPress={this.props.onPressTerms}
          style={[
            container,
            {
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              borderBottomColor: thameColors.gray,
              borderBottomWidth: 0.5,
            },
          ]}
        >
          <Text style={textRegular}>Terms and Conditions</Text>
          <Icon
            type="ionicon"
            name="ios-arrow-down"
            color={thameColors.textBlack}
            size={25}
          />
        </TouchableOpacity>
        {/* Location */}
        <View>
          <View style={container}>
            <Text
              style={[textBold, { color: thameColors.primary, fontSize: 20 }]}
            >
              Location Details
            </Text>
          </View>
          <Image
            source={require('../../../assets/attraction/maps.png')}
            style={{ width: window.width, height: verticalScale(250) }}
            resizeMode="cover"
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
              transform: [{ translateY: -40 }],
            }}
          >
            <View style={{ alignItems: 'center' }}>
              <Image
                source={require('../../../assets/icons/direction_blue_icon.png')}
                style={{ width: scale(50), height: scale(50) }}
                resizeMode="center"
              />
              <Text style={[textSemiBold, { marginTop: 10 }]}>
                Show Directions
              </Text>
            </View>
            <View style={{ alignItems: 'center' }}>
              <Image
                source={require('../../../assets/icons/location_blue_icon.png')}
                style={{ width: scale(50), height: scale(50) }}
                resizeMode="center"
              />
              <Text style={[textSemiBold, { marginTop: 10 }]}>Show Map</Text>
            </View>
          </View>
        </View>
        {/* More Activities */}
        <PopularActivities />
      </View>
    );
  }
}

export default DetailItem;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: thameColors.white,
  },
});
