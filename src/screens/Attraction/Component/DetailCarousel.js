import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { verticalScale, scale } from '../../../Const/ScaleUtils';
import { thameColors } from '../../../base/constant';

const window = Dimensions.get('window');

class DetailCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeDotIndex: 0,
    };
  }
  renderImg = (item, index) => (
    <View
      key={index}
      style={[styles.img, { alignItems: 'center', justifyContent: 'center' }]}
    >
      <Image
        source={item.img}
        style={[
          styles.img,
          index !== 0
            ? styles.firstImg
            : { position: 'relative', marginRight: 5 },
        ]}
        resizeMode={'cover'}
      />
    </View>
  );

  render() {
    return (
      <View>
        <ScrollView horizontal={true} indicatorStyle="white">
          {this.props.data.map((item, index) => this.renderImg(item, index))}
        </ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            position: 'absolute',
            width: window.width,
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: thameColors.white,
              borderRadius: 20,
              padding: 5,
              top: 40,
              left: 20,
            }}
          >
            <Image
              source={require('../../../assets/icons/arrow_left_icon.png')}
              style={{ width: scale(25), height: scale(20), marginRight: 5 }}
              resizeMode="cover"
            />
          </TouchableOpacity>
          <TouchableOpacity style={{ top: 41, right: 20 }}>
            <Image
              source={require('../../../assets/icons/share_icon.png')}
              style={{ width: scale(30), height: scale(30) }}
              resizeMode="center"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default DetailCarousel;

const styles = StyleSheet.create({
  dotStyle: {
    width: 200,
    height: 3,
    borderRadius: 5,
  },
  img: {
    width: window.width,
    height: verticalScale(300),
  },
  firstImg: {
    marginLeft: 5,
    marginRight: 5,
    width: '95%',
    height: '95%',
  },
});
