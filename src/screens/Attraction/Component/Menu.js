import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import { fontExtraBold, thameColors } from '../../../base/constant';
import { offers } from '../data';
import style from './style';
import { scale, verticalScale } from '../../../Const/ScaleUtils';

const window = Dimensions.get('window');

class Offers extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  coloring = index => {
    if (index === 0) {
      return thameColors.primary;
    } else if (index === 1) {
      return thameColors.secondary;
    } else if (index === 2) {
      return thameColors.yellow;
    }
  };
  cardTicket = (item, index) => {
    return (
      <TouchableOpacity
        key={index}
        onPress={() => this.navigate(item.navigate)}
        style={[
          styles.cardTicket,
          index === 0 ? { marginLeft: 20 } : { position: 'relative' },
        ]}
      >
        <View style={{ alignItems: 'center' }}>
          <View
            style={{
              width: scale(40),
              height: scale(40),
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 7,
            }}
          >
            <Image
              source={item.img}
              style={{ width: scale(40), height: scale(40) }}
            />
          </View>
          <Text style={style.textSemiBold}>{item.title}</Text>
        </View>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            position: 'absolute',
            width: window.width - 225,
          }}
        >
          <View style={[styles.circle, { left: -35 }]} />
          <View style={[styles.circle, { right: -35 }]} />
        </View>
      </TouchableOpacity>
    );
  };
  navigate = item => {
    const { push } = this.props.navigation;
    push(item);
  };

  render() {
    const { container, textExtraBold } = styles;
    return (
      <View style={container}>
        <Text style={[textExtraBold, { marginBottom: 10 }]}>
          Get Attractive Offers
        </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {offers.map((item, index) => this.cardTicket(item, index))}
        </ScrollView>
      </View>
    );
  }
}

export default Offers;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    alignItems: 'center',
  },
  textExtraBold: {
    fontFamily: fontExtraBold,
    fontSize: 20,
    color: thameColors.textBlack,
  },
  cardTicket: {
    width: scale(175),
    backgroundColor: thameColors.white,
    borderRadius: 10,
    margin: 7,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    borderRadius: 100,
    width: 35,
    height: 35,
    backgroundColor: thameColors.backWhite,
  },
});
