import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import { thameColors } from '../../../base/constant';
import { scale, verticalScale } from '../../../Const/ScaleUtils';
import style from './style';
import { movies } from '../data';

class MovieList extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  navigateCard = (to, item) => {
    //Navigate to Detail Card
    const { navigation, dispatch } = this.props;
    dispatch(actionDetailAttraction(item, 'movies'));
  };

  //render Card Content
  renderImg = (item, index) => {
    return (
      <TouchableOpacity
        onPress={() => this.navigateCard('DetailMovies', item)}
        key={index}
        style={[
          { margin: 5 },
          index === 0 ? { marginLeft: 20 } : { position: 'relative' },
        ]}
      >
        <View>
          <Image
            source={item.img}
            style={[
              style.imagePopular,
              {
                width: scale(200),
                height: verticalScale(300),
                flex: 1,
                borderRadius: 10,
              },
            ]}
            resizeMode="cover"
          />
          <View
            style={[
              {
                justifyContent: 'space-between',
                marginTop: 10,
                width: scale(200),
              },
              style.imagePopular.width,
            ]}
          >
            <Text style={[style.textBold, { marginBottom: 5 }]}>
              {item.title}
            </Text>
            <Text style={[style.textRegular, { color: thameColors.darkGray }]}>
              {item.genre}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  //Main Render
  render() {
    const { container } = styles;
    const { textExtraBold } = style;
    return (
      <View style={container}>
        <View style={style.header}>
          <Text style={textExtraBold}>In Theaters Soon</Text>
          <TouchableOpacity onPress={this.moreMovie}>
            <Icon
              type="material"
              name="keyboard-arrow-right"
              color={thameColors.primary}
              size={30}
            />
          </TouchableOpacity>
        </View>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {movies.map((item, index) => {
            return this.renderImg(item, index);
          })}
        </ScrollView>
      </View>
    );
  }
}

export default MovieList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 70,
  },
  img: {
    width: scale(175),
    height: verticalScale(250),
    borderRadius: 10,
    margin: 5,
  },
});
