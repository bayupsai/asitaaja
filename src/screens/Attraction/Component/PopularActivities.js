import React from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import Dash from 'react-native-dash';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import styles from './style';
import { thameColors } from '../../../base/constant';
import { activities } from '../data';
import { scale } from '../../../Const/ScaleUtils';

class PopularActivities extends React.Component {
  constructor(props) {
    super(props);
  }
  navigateTo = () => {
    //Navigate to more info
    const { push } = this.props.navigation;
    push('AttractionActivities');
  };
  navigateCard = (to, item) => {
    //Navigate to Detail Card
    const { navigation, dispatch } = this.props;
    navigation.navigate(to);
    dispatch(actionDetailAttraction(item, 'activities'));
  };

  //Render Card Content
  CardContent = props => {
    return (
      <TouchableOpacity
        onPress={props.onPress}
        style={[{ margin: 7 }, props.style]}
      >
        <View>
          <Image
            source={props.source}
            style={styles.imagePopular}
            resizeMode="cover"
          />
          <View
            style={{
              width: scale(200),
              backgroundColor: thameColors.white,
              borderRadius: 10,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              transform: [{ translateY: -50 }],
            }}
          >
            <View style={{ padding: 15 }}>
              <Text style={styles.textBold}>{props.title}</Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  alignItems: 'center',
                }}
              >
                <Icon
                  type="material"
                  name="location-on"
                  color={thameColors.gray}
                  size={20}
                  iconStyle={{ marginRight: 5 }}
                />
                <Text style={[styles.textRegular, { color: thameColors.gray }]}>
                  {props.location}
                </Text>
              </View>
            </View>
            <Dash dashColor={thameColors.gray} dashThickness={1} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 10,
              }}
            >
              <Text
                style={[
                  styles.textRegular,
                  { color: thameColors.gray, fontSize: 14 },
                ]}
              >
                Start from
              </Text>
              <Text style={[styles.textBold, { color: thameColors.primary }]}>
                Rp
                {numeral(props.price)
                  .format('0,0')
                  .replace(/,/g, '.')}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  //Main Render
  render() {
    let { detailActivities } = this.props.attraction;
    return (
      <View>
        {/* Heading Menu */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 20,
            marginBottom: 10,
            alignItems: 'center',
          }}
        >
          <View>
            <TouchableOpacity
              onPress={() => alert(JSON.stringify(detailActivities))}
            >
              <Text style={styles.textExtraBold}>Popular Activities</Text>
            </TouchableOpacity>
            <Text style={styles.textRegular}>
              Many activities are waiting for you!
            </Text>
          </View>
          <TouchableOpacity onPress={this.navigateTo}>
            <Icon
              type="ionicon"
              name="ios-arrow-forward"
              color={thameColors.primary}
              size={25}
            />
          </TouchableOpacity>
        </View>
        {/* Card Content */}
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {activities.map((item, index) => {
            return (
              <this.CardContent
                key={index}
                onPress={() => this.navigateCard('DetailActivities', item)}
                source={item.img}
                title={item.title}
                location={item.location}
                price={item.price}
                style={
                  index === 0 ? { marginLeft: 20 } : { position: 'relative' }
                }
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}
export default connect(mapStateToProps)(PopularActivities);
