import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { thameColors, fontExtraBold } from '../../../base/constant';
import numeral from 'numeral';
import styles from './style';

const DetailFooter = props => {
  return (
    <View style={style.bottomFix}>
      <View style={{ width: '50%', justifyContent: 'center' }}>
        <Text style={[styles.textRegular, { color: thameColors.gray }]}>
          {props.type == 'ticket' ? props.labelType : 'Start From'}
        </Text>
        <Text
          style={[
            styles.textBold,
            { color: thameColors.primary, fontSize: 22 },
          ]}
        >
          Rp
          {numeral(343274)
            .format('0,0')
            .replace(/,/g, '.')}
        </Text>
      </View>
      <View style={{ justifyContent: 'center' }}>
        <TouchableOpacity
          onPress={props.onPress}
          style={[
            style.button,
            props.active === true ? style.buttonActive : style.buttonNonActive,
          ]}
        >
          <Text
            style={[
              style.text,
              props.active === true ? style.textActive : style.textNonActive,
            ]}
          >
            {props.label}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DetailFooter;

const style = StyleSheet.create({
  bottomFix: {
    padding: 20,
    backgroundColor: thameColors.white,
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: window.width,
  },
  button: {
    width: '100%',
    padding: 10,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  buttonActive: {
    backgroundColor: thameColors.secondary,
  },
  buttonNonActive: {
    backgroundColor: thameColors.white,
    borderWidth: 2,
    borderColor: thameColors.buttonGray,
  },
  text: {
    fontFamily: fontExtraBold,
    color: thameColors.white,
    fontSize: 16,
  },
  textActive: {
    color: thameColors.white,
  },
  textNonActive: {
    color: thameColors.buttonGray,
  },
});
