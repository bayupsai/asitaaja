import React from 'react';
import { View, StyleSheet, Image, ScrollView, Dimensions } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { slide } from '../data';
import { scale, verticalScale } from '../../../Const/ScaleUtils';

const window = Dimensions.get('window');

//carousel image setting
const marginHorizontal = 5;
const widthImg = scale(300) + marginHorizontal * 2;

class Slide extends React.PureComponent {
  renderBanner = ({ item, index }) => {
    const { imageCard } = styles;
    return (
      <View
        style={[
          imageCard,
          {
            paddingHorizontal: marginHorizontal,
            width: widthImg,
            borderRadius: 0,
          },
        ]}
      >
        <Image
          key={index}
          source={item.img}
          style={[imageCard, { flex: 1 }]}
          resizeMode="center"
        />
      </View>
    );
  };
  render() {
    const { container } = styles;
    return (
      <View style={container}>
        {/* {
                        slide.map((item, index) => {
                            return (
                            )
                        })
                    } */}
        <Carousel
          data={slide}
          ref={c => (this._slideRef = c)}
          renderItem={this.renderBanner}
          sliderWidth={window.width}
          itemWidth={widthImg}
          loop={true}
          enableSnap={true}
          autoplay={true}
          autoplayDelay={2000}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
        />
      </View>
    );
  }
}

export default Slide;

const styles = StyleSheet.create({
  container: {
    marginTop: 0,
  },
  imageCard: {
    width: scale(300),
    height: verticalScale(175),
    borderRadius: 10,
    // margin: 7,
    // marginRight: 10,
  },
});
