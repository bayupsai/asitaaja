import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { fontBold, thameColors, fontReguler } from '../../../base/constant';
import { verticalScale } from '../../../Const/ScaleUtils';

const window = Dimensions.get('window');

const DetailModal = props => {
  return (
    <Modal
      useNativeDriver={true}
      isVisible={props.isVisible}
      onBackButtonPress={props.onDismiss}
      onBackdropPress={props.onDismiss}
      style={{
        justifyContent: 'flex-end',
        margin: 0,
      }}
    >
      <View style={styles.modalContent}>
        <TouchableOpacity
          style={{
            flex: 1.5,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: 'rgba(0,0,0,0.1)',
            borderBottomWidth: 0.5,
          }}
        >
          <Text
            style={{
              fontFamily: fontBold,
              color: thameColors.superBack,
              fontSize: 18,
            }}
          >
            {props.title}
          </Text>
        </TouchableOpacity>
        <View style={{ flex: 6, width: window.width, padding: 20 }}>
          <Text
            style={{ fontFamily: fontReguler, color: thameColors.textBlack }}
          >
            {props.content}
          </Text>
        </View>
        <View style={{ flex: 2.5 }}>
          <TouchableOpacity
            style={{ flex: 0, justifyContent: 'center', alignItems: 'center' }}
            onPress={props.onPress}
          >
            <View
              style={{
                width: 50,
                height: 50,
                borderRadius: 25,
                borderWidth: 1.5,
                padding: 10,
                borderColor: thameColors.darkGray,
              }}
            >
              <Icon
                name="arrowdown"
                type="antdesign"
                color={thameColors.superBack}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default DetailModal;

const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: verticalScale(400),
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
