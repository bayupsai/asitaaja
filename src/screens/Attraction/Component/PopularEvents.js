import React from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import Dash from 'react-native-dash';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import styles from './style';
import { thameColors } from '../../../base/constant';
import { events } from '../data';
import { scale } from '../../../Const/ScaleUtils';

class PopularEvents extends React.Component {
  constructor(props) {
    super(props);
  }
  navigateTo = () => {
    //Navigate to more info
    const { navigation } = this.props;
    navigation.navigate('AttractionEvents');
  };
  navigateCard = (to, item) => {
    //Navigate to Detail Card
    const { navigation, dispatch } = this.props;
    navigation.navigate(to);
    dispatch(actionDetailAttraction(item, 'events'));
  };

  //render Card
  CardContent = props => {
    return (
      <TouchableOpacity
        onPress={props.onPress}
        style={[{ margin: 7 }, props.style]}
      >
        <View>
          <Image
            source={props.source}
            style={styles.imagePopular}
            resizeMode="cover"
          />
          <View
            style={{
              width: scale(200),
              backgroundColor: thameColors.white,
              borderRadius: 10,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              transform: [{ translateY: -50 }],
            }}
          >
            <View style={{ padding: 15 }}>
              <Text style={styles.textBold}>{props.title}</Text>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                {/* <Image source={require('../../../assets/icons/location_icon.png')} resizeMode="stretch" style={{ width: window.width / 20, height: window.height / 30, marginRight: 5 }} /> */}
                <Icon
                  type="material-community"
                  name="calendar-check"
                  color={thameColors.gray}
                  size={20}
                  iconStyle={{ marginRight: 5 }}
                />
                <Text
                  style={[
                    styles.textRegular,
                    { color: thameColors.gray, fontSize: 14 },
                  ]}
                >
                  {props.dateStart} - {props.dateEnd}
                </Text>
              </View>
            </View>
            <Dash dashColor={thameColors.gray} dashThickness={1} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 10,
              }}
            >
              <Text
                style={[
                  styles.textRegular,
                  { color: thameColors.gray, fontSize: 14 },
                ]}
              >
                Start from
              </Text>
              <Text style={[styles.textBold, { color: thameColors.primary }]}>
                Rp
                {numeral(props.price)
                  .format('0,0')
                  .replace(/,/g, '.')}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  //Main Render
  render() {
    return (
      <View>
        {/* Heading Menu */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 20,
            marginBottom: 10,
            alignItems: 'center',
          }}
        >
          <View>
            <Text style={styles.textExtraBold}>Popular Events</Text>
            <Text style={styles.textRegular}>
              Don't miss the events we offer!
            </Text>
          </View>
          <TouchableOpacity onPress={this.navigateTo}>
            <Icon
              type="ionicon"
              name="ios-arrow-forward"
              color={thameColors.primary}
              size={25}
            />
          </TouchableOpacity>
        </View>
        {/* Card Content */}
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {events.map((item, index) => {
            return (
              <this.CardContent
                onPress={() => this.navigateCard('DetailEvents', item)}
                key={index}
                source={item.img}
                title={item.title}
                dateStart={item.dateStart}
                dateEnd={item.dateEnd}
                price={item.price}
                style={
                  index === 0 ? { marginLeft: 20 } : { position: 'relative' }
                }
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(PopularEvents);
