import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Animated,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { Icon } from 'react-native-elements';
import _ from 'lodash';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import { thameColors } from '../../../base/constant';
import { scale, verticalScale } from '../../../Const/ScaleUtils';
import style from './style';
import { movies } from '../data';

const height = verticalScale(35);
const heightOffset = 300;
const widthDevice = Dimensions.get('window').width;

class MovieCarousel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0,
      imgCenter: {},
      scrollY: new Animated.Value(0),
    };
  }
  componentDidMount() {
    this.findingImg(0);
  }
  findingImg = index => {
    const find = _.find(movies, { id: index + 1 });
    this.setState({ imgCenter: find });
  };
  moreMovie = () => {
    alert('More Movies');
  };
  navigateCard = (to, item) => {
    //Navigate to Card
    const { navigation, dispatch } = this.props;
    dispatch(actionDetailAttraction(item, 'movies'));
  };

  //render Card Content
  renderMovie = ({ item, index }) => {
    const horizontalMargin = 10;
    return (
      <TouchableOpacity
        onPress={() => this.navigateCard('DetailMovies', item)}
        key={index}
        style={{
          width: scale(200) + horizontalMargin * 2,
          height: verticalScale(300),
          paddingHorizontal: horizontalMargin,
          borderRadius: 10,
        }}
      >
        <Image
          source={item.img}
          style={{
            width: scale(200),
            height: verticalScale(300),
            flex: 1,
            borderRadius: 10,
          }}
          resizeMode="cover"
        />
      </TouchableOpacity>
    );
  };

  //mainRender
  render() {
    const { container, img } = styles;
    const { textExtraBold } = style;
    const diffScrollTranslate = Animated.diffClamp(
      this.state.scrollY,
      0,
      heightOffset
    ).interpolate({
      inputRange: [0, heightOffset],
      outputRange: [0, -height],
      extrapolate: 'clamp',
    });
    const horizontalMargin = 10;
    return (
      <View style={container}>
        <View style={style.header}>
          <Text style={textExtraBold}>Now Playing</Text>
          <TouchableOpacity onPress={this.moreMovie}>
            <Icon
              type="material"
              name="keyboard-arrow-right"
              color={thameColors.primary}
              size={30}
            />
          </TouchableOpacity>
        </View>
        <View style={{ alignItems: 'center' }}>
          <Carousel
            ref={c => (this._slider1Ref = c)}
            data={movies}
            renderItem={this.renderMovie}
            sliderWidth={widthDevice}
            itemWidth={scale(200) + horizontalMargin * 2}
            loop={true}
            enableSnap={true}
            onSnapToItem={index =>
              this.setState({ activeSlide: index }, () => {
                this.findingImg(index);
              })
            }
            onScroll={Animated.event([
              { nativeEvent: { contentOffset: { y: this.state.scrollY } } },
            ])}
            autoplay={true}
            autoplayDelay={2000}
            inactiveSlideScale={1}
            inactiveSlideOpacity={1}
          />
          <View
            style={{
              alignItems: 'center',
              width: scale(200) + horizontalMargin * 2,
              height: verticalScale(65),
              paddingHorizontal: 10,
            }}
          >
            <Animated.View
              style={[
                styles.ticketCenter,
                { transform: [{ translateY: diffScrollTranslate }] },
              ]}
            >
              <Text style={[style.textBold, { color: thameColors.white }]}>
                Buy Ticket
              </Text>
            </Animated.View>
            <View style={{ alignItems: 'center' }}>
              <Text style={[style.textExtraBold, { textAlign: 'center' }]}>
                {this.state.imgCenter.title}
              </Text>
              <Text style={style.textRegular}>
                {this.state.imgCenter.genre}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(MovieCarousel);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    width: scale(200),
    height: verticalScale(300),
    borderRadius: 5,
  },
  ticketCenter: {
    width: '100%',
    height: verticalScale(35),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: thameColors.secondary,
    borderRadius: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: -34,
  },
});
