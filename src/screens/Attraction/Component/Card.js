import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { thameColors } from '../../../base/constant';
import { scale } from '../../../Const/ScaleUtils';

class Card extends React.PureComponent {
  render({ children } = this.props) {
    const { container, imageStyle, cardStyle } = styles;
    return (
      <View style={container}>
        <TouchableOpacity style={cardStyle} onPress={this.props.onPress}>
          <View style={{ width: '30%' }}>
            <Image source={this.props.source} style={imageStyle} />
          </View>
          <View style={{ padding: 20, width: '70%' }}>{children}</View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Card;

const styles = StyleSheet.create({
  container: {
    margin: 20,
    alignItems: 'flex-end',
  },
  imageStyle: {
    borderRadius: 25,
    width: scale(120),
    height: scale(120),
    position: 'absolute',
    transform: [{ translateY: -20 }, { translateX: -20 }],
  },
  cardStyle: {
    backgroundColor: thameColors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '95%',
    borderRadius: 10,
  },
});
