import React from 'react';
import { StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import { FloatingAction } from 'react-native-floating-action';
import { thameColors } from '../../../base/constant';

class FabButton extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      float: false,
    };
  }
  floatPress = item => {
    this.setState({ float: !item });
  };

  render() {
    const { float } = this.state;
    return (
      <FloatingAction
        actions={this.props.actions}
        onPressItem={this.props.onPressItem}
        color={thameColors.secondary}
        iconHeight={25}
        iconWidth={25}
        shadow={styles.shadow}
        overlayColor="rgba(0,0,0,0)"
        distanceToEdge={20}
        floatingIcon={
          float == false ? (
            <Icon
              type="feather"
              name="plus"
              color={thameColors.white}
              size={30}
            />
          ) : (
            <Icon
              type="material-community"
              name="window-close"
              color={thameColors.white}
              size={30}
            />
          )
        }
        onPressMain={() => this.floatPress(float)}
        onClose={() => this.floatPress(float)}
      />
    );
  }
}

export default FabButton;

const styles = StyleSheet.create({
  shadow: {
    shadowOpacity: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: 'rgba(0,0,0, 0)',
    shadowRadius: 0,
  },
});
