import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { thameColors } from '../../../base/constant';
import style from './style';
import { scale } from '../../../Const/ScaleUtils';

const window = Dimensions.get('window');

const SubMenu = props => {
  return (
    <TouchableOpacity
      style={[
        props.style,
        styles.subMenu,
        props.selected === true ? styles.selected : styles.nonSelected,
      ]}
      onPress={props.onPress}
    >
      <Text
        style={[
          style.textSemiBold,
          props.selected === true ? styles.selected : styles.nonSelected,
        ]}
      >
        {props.label}
      </Text>
    </TouchableOpacity>
  );
};

export default SubMenu;

const styles = StyleSheet.create({
  subMenu: {
    padding: 15,
    margin: 10,
    width: scale(150),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginRight: 5,
  },
  selected: {
    backgroundColor: thameColors.secondary,
    color: thameColors.white,
  },
  nonSelected: {
    backgroundColor: thameColors.white,
    color: thameColors.textBlack,
  },
});
