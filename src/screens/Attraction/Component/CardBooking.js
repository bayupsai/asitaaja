import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import Dash from 'react-native-dash';
import numeral from 'numeral';
import { scale } from '../../../Const/ScaleUtils';
import { thameColors } from '../../../base/constant';
import style from './style';

const CardBooking = props => {
  const { card, buttonCircle } = styles;
  return (
    <View style={card}>
      <View style={{ padding: 20 }}>
        <Text style={[style.textSemiBold, { fontSize: 18 }]}>
          {props.title}
        </Text>
        <Text style={[style.textRegular, { color: thameColors.gray }]}>
          Category: Adult
        </Text>
        <TouchableOpacity
          onPress={() => alert('Details')}
          style={{ marginTop: 10 }}
        >
          <Text
            style={[
              style.textRegular,
              { fontSize: 18, color: thameColors.secondary },
            ]}
          >
            View Detail
          </Text>
        </TouchableOpacity>
      </View>
      <Dash
        dashThickness={1}
        dashColor={thameColors.gray}
        style={{ width: '99%' }}
      />
      <View style={{ padding: 20 }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Text
            style={[
              style.textBold,
              { color: thameColors.primary, fontSize: 22 },
            ]}
          >
            Rp
            {numeral(343274)
              .format('0,0')
              .replace(/,/g, '.')}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: thameColors.semiGray,
              borderRadius: 25,
              width: scale(100),
              justifyContent: 'space-between',
              paddingHorizontal: 3,
              paddingVertical: 5,
            }}
          >
            <TouchableOpacity
              onPress={props.minus}
              style={[
                buttonCircle,
                { backgroundColor: thameColors.buttonGray },
              ]}
            >
              <Icon
                type="feather"
                name="minus"
                color={thameColors.white}
                size={20}
              />
            </TouchableOpacity>
            <Text style={style.textRegular}>{props.qty}</Text>
            <TouchableOpacity
              onPress={props.plus}
              style={[buttonCircle, { backgroundColor: thameColors.secondary }]}
            >
              <Icon
                type="feather"
                name="plus"
                color={thameColors.white}
                size={20}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CardBooking;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    backgroundColor: thameColors.white,
    borderRadius: 5,
  },
  buttonCircle: {
    borderRadius: 25,
    width: scale(30),
    height: scale(30),
    justifyContent: 'center',
  },
});
