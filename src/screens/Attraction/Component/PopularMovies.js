import React from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { actionDetailAttraction } from '../../../redux/actions/AttractionAction';
import styles from './style';
import { thameColors } from '../../../base/constant';
import { movies } from '../data';
import { scale } from '../../../Const/ScaleUtils';

class PopularMovies extends React.Component {
  constructor(props) {
    super(props);
  }
  navigateTo = () => {
    const { push } = this.props.navigation;
    push('AttractionMovies');
  };
  navigateCard = (to, item) => {
    const { dispatch } = this.props;
    dispatch(actionDetailAttraction(item, 'movies'));
  };

  CardContent = props => {
    return (
      <TouchableOpacity
        onPress={props.onPress}
        style={[{ margin: 7 }, props.style]}
      >
        <View>
          <Image
            source={props.source}
            style={styles.imagePopular}
            resizeMode="cover"
          />
          <View
            style={{
              width: scale(200),
              justifyContent: 'space-between',
              marginTop: 10,
            }}
          >
            <Text style={[styles.textBold, { marginBottom: 5 }]}>
              {props.title}
            </Text>
            <Text style={[styles.textRegular, { color: thameColors.gray }]}>
              {props.genre}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={{ marginTop: -40 }}>
        {/* Heading Menu */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 20,
            marginBottom: 10,
            alignItems: 'center',
          }}
        >
          <View>
            <Text style={styles.textExtraBold}>Movies Near You</Text>
            <Text style={styles.textRegular}>
              There are many theaters around you!
            </Text>
          </View>
          <TouchableOpacity onPress={this.navigateTo}>
            <Icon
              type="ionicon"
              name="ios-arrow-forward"
              color={thameColors.primary}
              size={25}
            />
          </TouchableOpacity>
        </View>
        {/* Card Content */}
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {movies.map((item, index) => {
            return (
              <this.CardContent
                onPress={() => this.navigateCard('DetailMovies', item)}
                key={index}
                source={item.img}
                title={item.title}
                genre={item.genre}
                style={
                  index === 0 ? { marginLeft: 20 } : { position: 'relative' }
                }
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    attraction: state.attraction,
  };
}

export default connect(mapStateToProps)(PopularMovies);
