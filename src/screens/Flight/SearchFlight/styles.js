import { StyleSheet, Dimensions, Platform } from 'react-native';
import {
  fontReguler,
  fontBold,
  fontExtraBold,
  thameColors,
  asitaColor,
} from '../../../base/constant';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  sectionToggle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -40,
    paddingBottom: 10,
  },
  sectionInput: {
    paddingBottom: 10,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: thameColors.backWhite,
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: thameColors.white,
    borderRadius: 5,
    height: deviceHeight / 7,
  },
  textLabel: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 16,
  },
  textBold: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 18,
    fontWeight: '500',
    paddingBottom: 5,
  },
  textInitial: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 14,
    fontWeight: '500',
  },

  // MODAL DEPARTURE
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 45,
    width: deviceWidth,
  },
  modalSearch: {
    flex: 0,
    // height: 70,
    width: deviceWidth,
    marginTop: Platform.OS === 'android' ? -60 : -70,
    marginBottom: 10,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    color: thameColors.white,
    fontFamily: fontBold,
    fontSize: 18,
  },
  // MODAL DEPARTURE

  // MODAL PASSENGER
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // MODAL PASSENGER

  // MODAL SEAT CLASS
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // MODAL SEAT CLASS

  AutoCompleteClose: {
    padding: 5,
    color: thameColors.darkGray,
    fontWeight: '900',
    elevation: 5,
    fontSize: 18,
  },
});
