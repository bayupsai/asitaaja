import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Image,
  RefreshControl,
  InteractionManager,
  Platform,
  StatusBar,
} from 'react-native';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import { Grid, Row, Col } from 'react-native-easy-grid';
import MultiSwitch from 'rn-slider-switch';
import _ from 'lodash';
import { SearchInput, InputTextRevamp } from '../../../elements/TextInput';
import { ButtonRounded } from '../../../elements/Button';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Dates from 'react-native-dates';
import moment from 'moment';
import { connect } from 'react-redux';
import styles from './styles';
import {
  actionSearchFlight,
  failedState,
  actionListAirport,
} from '../../../redux/actions/FlightAction';
import { makeGetAirport } from '../../../redux/selectors/FlightSelector';
import { DotIndicator } from 'react-native-indicators';
import DestinationPopular from './Data/destinationPopular';
import {
  fontBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../base/constant';

var deviceWidth = Dimensions.get('window').width;
const isAndroid = Platform.OS === 'android';

class SearchFlight extends PureComponent {
  constructor(props) {
    super(props);
    let departureDate = moment().add(1, 'days');
    let returnDate = moment(departureDate).add(1, 'days');
    this.state = {
      date: null,
      focus: 'startDate',
      startDate: null,
      endDate: null,
      flightType: 'single', // single or return
      visibleModal: null,
      seatClassSelected: 1,
      seatClassSelectedName: 'ECONOMY',
      seatClassType: [
        { index: 1, title: 'ECONOMY' },
        { index: 2, title: 'BUSINESS' },
        { index: 3, title: 'FIRST' },
      ],
      destinationPopular: [
        {
          code: 'CGK',
          name: 'Jakarta',
          namex: 'Soekarno-Hatta International Airport',
        },
        {
          code: 'DPS',
          name: 'Denpasar',
          namex: 'Ngurah Rai International Airport',
        },
        { code: 'LOP', name: 'Lombok', namex: 'Lombok International Airport' },
        {
          code: 'JOG',
          name: 'Yogya',
          namex: 'Adisutjipto International Airport',
        },
        { code: 'MLG', name: 'Malang', namex: 'Abdul Rahman Saleh' },
        {
          code: 'UPG',
          name: 'Makassar',
          namex: 'Hasanuddin International Airport',
        },
        {
          code: 'KNO',
          name: 'Medan',
          namex: 'Kualanamu International Airport',
        },
        { code: 'AMQ', name: 'Ambon', namex: 'Pattimura Airport' },
        { code: 'MDC', name: 'Manado', namex: 'Sam Ratulangi Airport' },
        {
          code: 'SUB',
          name: 'Surabaya',
          namex: 'Juanda International Airport ',
        },
      ],
      departureList: [
        { code: 'AEG', name: 'Padang Sidempuan', namex: 'Aek Godang Airport' },
        { code: 'AMQ', name: 'Ambon', namex: 'Pattimura Airport, Ambon' },
        {
          code: 'BDO',
          name: 'Bandung',
          namex: 'Husein Sastranegara International Airport',
        },
        { code: 'BEJ', name: 'Tanjung Redeb', namex: 'Kalimarau Airport' },
        { code: 'BIK', name: 'Biak', namex: 'Frans Kaisiepo Airport' },
        { code: 'BKS', name: 'Bengkulu', namex: 'Fatmawati Soekarno Airport' },
        {
          code: 'BTH',
          name: 'Batam',
          namex: 'Hang Nadim International Airport',
        },
        {
          code: 'BTJ',
          name: 'Banda Aceh',
          namex: 'Sultan Iskandar Muda International Airport',
        },
        { code: 'BXB', name: 'Babo-Papua Island', namex: 'Babo Airport' },
        {
          code: 'CGK',
          name: 'Jakarta',
          namex: 'Soekarno-Hatta International Airport',
        },
        { code: 'CXP', name: 'Cilacap', namex: 'Tunggul Wulung Airport' },
        {
          code: 'DJJ',
          name: 'Jayapura',
          namex: 'Sentani International Airport',
        },
        {
          code: 'DPS',
          name: 'Denpasar',
          namex: 'Ngurah Rai (Bali) International Airport',
        },
        { code: 'DUM', name: 'Dumai', namex: 'Pinang Kampai Airport' },
        { code: 'FKQ', name: 'Fakfak', namex: 'Fakfak Airport' },
        { code: 'GNS', name: 'Gunung Sitoli', namex: 'Binaka Airport' },
        {
          code: 'HLP',
          name: 'Jakarta',
          namex: 'Halim Perdanakusuma International Airport',
        },
        { code: 'KNG', name: 'Kaimana', namex: 'Kaimana Airport' },
        {
          code: 'KTG',
          name: 'Ketapang',
          namex: 'Ketapang(Rahadi Usman) Airport',
        },
        { code: 'LOP', name: 'Mataram', namex: 'Lombok International Airport' },
        { code: 'LSX', name: 'Lhok Sukon', namex: 'Lhok Sukon Airport' },
        { code: 'LUV', name: 'Langgur', namex: 'Dumatumbun Airport' },
        {
          code: 'LUW',
          name: 'Luwok',
          namex: 'Syukuran Aminuddin Amir Airport',
        },
        { code: 'MDC', name: 'Manado', namex: 'Sam Ratulangi Airport' },
        { code: 'MES', name: 'Medan', namex: 'Soewondo Air Force Base' },
        { code: 'MKQ', name: 'Merauke', namex: 'Mopah Airport' },
        { code: 'MKW', name: 'Manokwari', namex: 'Rendani Airport' },
        { code: 'NAH', name: 'Tahuna', namex: 'Naha Airport' },
        { code: 'NBX', name: 'Nabire', namex: 'Nabire Airport' },
        { code: 'NPO', name: 'Nanga Pinoh', namex: 'Nanga Pinoh Airport' },
        { code: 'NTX', name: 'Ranai', namex: 'Ranai Airport' },
        { code: 'OTI', name: 'Gotalalamo', namex: 'Pitu Airport' },
        {
          code: 'PDG',
          name: 'Ketaping/Padang',
          namex: 'Minangkabau International Airport',
        },
        { code: 'PDO', name: 'Talang Gudang', namex: 'Pendopo Airport' },
        {
          code: 'PKU',
          name: 'Pekanbaru',
          namex: 'Sultan Syarif Kasim Ii (Simpang Tiga) Airport',
        },
        {
          code: 'PLM',
          name: 'Palembang',
          namex: 'Sultan Mahmud Badaruddin II Airport',
        },
        { code: 'PLW', name: 'Palu', namex: 'Mutiara Airport' },
        { code: 'PNK', name: 'Pontianak', namex: 'Supadio Airport' },
        { code: 'PSJ', name: 'Poso', namex: 'Kasiguncu Airport' },
        { code: 'PSU', name: 'Putussibau', namex: 'Pangsuma Airport' },
        { code: 'RGT', name: 'Rengat', namex: 'Japura Airport' },
        { code: 'SOQ', name: 'Sorong', namex: 'Dominique Edward Osok Airport' },
        { code: 'SQG', name: 'Sintang', namex: 'Sintang(Susilo) Airport' },
        { code: 'SRI', name: 'Samarinda', namex: 'Temindung Airport' },
        {
          code: 'SUB',
          name: 'Surabaya',
          namex: 'Juanda International Airport',
        },
        { code: 'TIM', name: 'Timika', namex: 'Moses Kilangin Airport' },
        { code: 'TJG', name: 'Tanta-Tabalong', namex: 'Warukin Airport' },
        {
          code: 'TKG',
          name: 'Bandar Lampung',
          namex: 'Radin Inten II (Branti) Airport',
        },
        { code: 'TRK', name: 'Tarakan', namex: 'Juwata Airport' },
        {
          code: 'TTE',
          name: 'Sango',
          namex: 'Sultan Khairun Babullah Airport',
        },
        { code: 'UGU', name: 'Sugapa', namex: 'Bilogai-Sugapa Airport' },
        {
          code: 'UPG',
          name: 'Ujung Pandang',
          namex: 'Hasanuddin International Airport',
        },
        { code: 'WMX', name: 'Wamena', namex: 'Wamena Airport' },
      ],
      departureResult: [],
      destinationList: [
        { code: 'AEG', name: 'Padang Sidempuan', namex: 'Aek Godang Airport' },
        { code: 'AMQ', name: 'Ambon', namex: 'Pattimura Airport, Ambon' },
        {
          code: 'BDO',
          name: 'Bandung',
          namex: 'Husein Sastranegara International Airport',
        },
        { code: 'BEJ', name: 'Tanjung Redeb', namex: 'Kalimarau Airport' },
        { code: 'BIK', name: 'Biak', namex: 'Frans Kaisiepo Airport' },
        { code: 'BKS', name: 'Bengkulu', namex: 'Fatmawati Soekarno Airport' },
        {
          code: 'BTH',
          name: 'Batam',
          namex: 'Hang Nadim International Airport',
        },
        {
          code: 'BTJ',
          name: 'Banda Aceh',
          namex: 'Sultan Iskandar Muda International Airport',
        },
        { code: 'BXB', name: 'Babo-Papua Island', namex: 'Babo Airport' },
        {
          code: 'CGK',
          name: 'Jakarta',
          namex: 'Soekarno-Hatta International Airport',
        },
        { code: 'CXP', name: 'Cilacap', namex: 'Tunggul Wulung Airport' },
        {
          code: 'DJJ',
          name: 'Jayapura',
          namex: 'Sentani International Airport',
        },
        {
          code: 'DPS',
          name: 'Denpasar',
          namex: 'Ngurah Rai (Bali) International Airport',
        },
        { code: 'DUM', name: 'Dumai', namex: 'Pinang Kampai Airport' },
        { code: 'FKQ', name: 'Fakfak', namex: 'Fakfak Airport' },
        { code: 'GNS', name: 'Gunung Sitoli', namex: 'Binaka Airport' },
        {
          code: 'HLP',
          name: 'Jakarta',
          namex: 'Halim Perdanakusuma International Airport',
        },
        { code: 'KNG', name: 'Kaimana', namex: 'Kaimana Airport' },
        {
          code: 'KTG',
          name: 'Ketapang',
          namex: 'Ketapang(Rahadi Usman) Airport',
        },
        { code: 'LOP', name: 'Mataram', namex: 'Lombok International Airport' },
        { code: 'LSX', name: 'Lhok Sukon', namex: 'Lhok Sukon Airport' },
        { code: 'LUV', name: 'Langgur', namex: 'Dumatumbun Airport' },
        {
          code: 'LUW',
          name: 'Luwok',
          namex: 'Syukuran Aminuddin Amir Airport',
        },
        { code: 'MDC', name: 'Manado', namex: 'Sam Ratulangi Airport' },
        { code: 'MES', name: 'Medan', namex: 'Soewondo Air Force Base' },
        { code: 'MKQ', name: 'Merauke', namex: 'Mopah Airport' },
        { code: 'MKW', name: 'Manokwari', namex: 'Rendani Airport' },
        { code: 'NAH', name: 'Tahuna', namex: 'Naha Airport' },
        { code: 'NBX', name: 'Nabire', namex: 'Nabire Airport' },
        { code: 'NPO', name: 'Nanga Pinoh', namex: 'Nanga Pinoh Airport' },
        { code: 'NTX', name: 'Ranai', namex: 'Ranai Airport' },
        { code: 'OTI', name: 'Gotalalamo', namex: 'Pitu Airport' },
        {
          code: 'PDG',
          name: 'Ketaping/Padang',
          namex: 'Minangkabau International Airport',
        },
        { code: 'PDO', name: 'Talang Gudang', namex: 'Pendopo Airport' },
        {
          code: 'PKU',
          name: 'Pekanbaru',
          namex: 'Sultan Syarif Kasim Ii (Simpang Tiga) Airport',
        },
        {
          code: 'PLM',
          name: 'Palembang',
          namex: 'Sultan Mahmud Badaruddin II Airport',
        },
        { code: 'PLW', name: 'Palu', namex: 'Mutiara Airport' },
        { code: 'PNK', name: 'Pontianak', namex: 'Supadio Airport' },
        { code: 'PSJ', name: 'Poso', namex: 'Kasiguncu Airport' },
        { code: 'PSU', name: 'Putussibau', namex: 'Pangsuma Airport' },
        { code: 'RGT', name: 'Rengat', namex: 'Japura Airport' },
        { code: 'SOQ', name: 'Sorong', namex: 'Dominique Edward Osok Airport' },
        { code: 'SQG', name: 'Sintang', namex: 'Sintang(Susilo) Airport' },
        { code: 'SRI', name: 'Samarinda', namex: 'Temindung Airport' },
        {
          code: 'SUB',
          name: 'Surabaya',
          namex: 'Juanda International Airport',
        },
        { code: 'TIM', name: 'Timika', namex: 'Moses Kilangin Airport' },
        { code: 'TJG', name: 'Tanta-Tabalong', namex: 'Warukin Airport' },
        {
          code: 'TKG',
          name: 'Bandar Lampung',
          namex: 'Radin Inten II (Branti) Airport',
        },
        { code: 'TRK', name: 'Tarakan', namex: 'Juwata Airport' },
        {
          code: 'TTE',
          name: 'Sango',
          namex: 'Sultan Khairun Babullah Airport',
        },
        { code: 'UGU', name: 'Sugapa', namex: 'Bilogai-Sugapa Airport' },
        {
          code: 'UPG',
          name: 'Ujung Pandang',
          namex: 'Hasanuddin International Airport',
        },
        { code: 'WMX', name: 'Wamena', namex: 'Wamena Airport' },
      ],
      destinationResult: [],
      // MAIN DATA
      origin: {
        airport_code: 'CGK',
        area_code: 'JKTA',
        city_name: 'Jakarta',
        country_code: '360',
        icao_code: 'WIII',
        international_airport_name: 'Soekarno Hatta Intl Airport',
        timezone: 'Asia/Pontianak',
      },
      destination: {
        airport_code: 'JOG',
        area_code: 'YKIA',
        city_name: 'Yogyakarta',
        country_code: '360',
        icao_code: 'WARJ',
        international_airport_name: 'Adi Sutjipto',
        timezone: 'Asia/Pontianak',
      },
      departureDate: departureDate.format('YYYY-MM-DD'),
      returnDate: returnDate.format('YYYY-MM-DD'),
      adult: 1,
      child: 0,
      infant: 0,
      seatClass: 'ECONOMY',
    };
  }

  componentDidMount() {
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      isAndroid && StatusBar.setBackgroundColor(asitaColor.marineBlue);
    });
    InteractionManager.runAfterInteractions(() => {
      this.props.dispatch(failedState('searchFlight'));
      const { getDestination } = this.props.navigation.state.params;
      if (getDestination) {
        let data = _.find(DestinationPopular, { city_name: getDestination });
        this.setOriginDestination(2, data);
      } else {
        alert('Nothing Parameters');
      }
      this.getAirport();
    });
  }
  componentWillUnmount() {
    this._navListener.remove();
  }

  getAirport = () => {
    let { dispatch, listAirport } = this.props;
    let payload = {};
    if (listAirport.length === 0) {
      dispatch(actionListAirport(payload));
    }
  };

  setOriginDestination = (modalType, data) => {
    if (modalType == 1) {
      // departure
      this.setState({
        visibleModal: null,
        origin: data,
      });
    } else if (modalType == 2) {
      // destination
      this.setState({
        visibleModal: null,
        destination: data,
      });
    }
  };
  setPassengerData = (passengerType, value) => {
    if (passengerType == 'adult') {
      if (value > 0) {
        this.setState({ adult: value });
      }
    } else if (passengerType == 'child') {
      if (value == '-') {
        this.setState({ child: 0 });
      } else {
        this.setState({ child: value });
      }
    } else if (passengerType == 'infant') {
      if (value != '-') {
        this.setState({ infant: value });
      }
    }
  };

  setSeatClass = seatClass => {
    this.setState({
      seatClassSelected: seatClass.index,
      seatClass: seatClass.title, // Used for Search Flight later
      seatClassSelectedName: seatClass.title,
      visibleModal: null,
    });
  };

  setDepartureDate = ({ date }) => {
    const { returnDate } = this.state;
    let selectDate = moment(date).format('YYYY-MM-DD');
    if (selectDate >= returnDate) {
      this.setState({
        departureDate: selectDate,
        returnDate: moment(date)
          .add(1, 'days')
          .format('YYYY-MM-DD'),
      });
    }

    this.setState({
      startDate: date,
      visibleModal: null,
      focus: date,
      departureDate: selectDate,
      returnDate: moment(date)
        .add(1, 'days')
        .format('YYYY-MM-DD'),
    });
  };
  setReturnDate = ({ date }) => {
    const { departureDate } = this.state;
    let selectDate = moment(date).format('YYYY-MM-DD');
    if (selectDate <= departureDate) {
      this.setState({
        departureDate: moment(date)
          .add(-1, 'days')
          .format('YYYY-MM-DD'),
        returnDate: selectDate,
        visibleModal: null,
      });
    } else {
      this.setState({
        returnDate: moment(date).format('YYYY-MM-DD'),
        visibleModal: null,
      });
    }
  };
  //  ==================== SET DATA

  // ==================== GLOBAL FUNC
  loadingBooking = () => (
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: asitaColor.white,
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        height: 'auto',
        flex: 0,
      }}
    >
      <Col
        size={3}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          paddingLeft: 0,
        }}
      >
        <Image
          source={require('../../../assets/loading/search-flight.png')}
          style={{ width: 75, height: 75 }}
          resizeMode="center"
        />
      </Col>
      <Col size={7} style={{ paddingLeft: 10 }}>
        <Row style={{ flex: 0 }}>
          <Text
            style={{
              fontFamily: fontBold,
              fontSize: 16,
              color: asitaColor.superBack,
            }}
          >
            Please Wait.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5 }}>
          <Text
            style={{
              fontFamily: fontReguler,
              fontSize: 16,
              color: asitaColor.superBack,
            }}
          >
            We still process your request.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5, alignItems: 'center' }}>
          <DotIndicator
            size={10}
            style={{ flex: 0, backgroundColor: asitaColor.white }}
            color={asitaColor.tealBlue}
          />
        </Row>
      </Col>
    </Grid>
  );
  searchFlight = () => {
    // this.setState({visibleModal: 99})
    let payloadData = {
      command: 'SCHEDULE',
      product: 'FLIGHT',
      data: {
        departure_code: this.state.origin.airport_code,
        arrival_code: this.state.destination.airport_code,
        departure_date: this.state.departureDate,
        adult: this.state.adult,
        child: this.state.child,
        infant: this.state.infant,
        seatClass: this.state.seatClass,
      },
    };

    if (this.state.flightType == 'return') {
      payloadData.data.return_date = this.state.returnDate;
    }
    this.props.dispatch(actionSearchFlight(payloadData));
    InteractionManager.runAfterInteractions(() => {
      this.props.navigation.navigate('SearchResult', {
        payload: payloadData,
        type: 'flight',
      });
    });
  };

  // Modal Function
  resetModal = () => {
    this.setState({
      visibleModal: null,
      departureResult: [],
      destinationResult: [],
    });
  };
  _toggleModal = modal => {
    // for open modal : 1 = departure, 2 = destination, 3 = passenger, 4 = seat class
    this.setState({ visibleModal: modal });
  };
  _setModal1 = () => this.setState({ visibleModal: 1 });
  _setModal2 = () => this.setState({ visibleModal: 2 });
  _setModal3 = () => this.setState({ visibleModal: 3 });
  _setModal4 = () => this.setState({ visibleModal: 4 });
  _setModal5 = () => this.setState({ visibleModal: 5 });
  _setModal6 = () => this.setState({ visibleModal: 6 });
  _setNullModal = () => {
    this.setState({ visibleModal: null });
  };
  // Modal Function

  // SeatClass FlatList Function
  _renderSeatClass = ({ item, index }) => {
    return (
      <Grid
        key={index}
        style={{
          flex: 1,
          backgroundColor: thameColors.white,
          padding: 10,
          borderBottomColor: thameColors.buttonGray,
          borderBottomWidth: 0.5,
        }}
      >
        <TouchableOpacity
          onPress={() => this.setSeatClass(item)}
          style={{ flex: 1 }}
        >
          <Row>
            <Col style={{ alignItems: 'flex-start' }}>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.superBack,
                  fontSize: 16,
                }}
              >
                {item.title == 'First' ? 'First Class' : item.title}
              </Text>
            </Col>
            <Col style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
              {this.state.seatClassSelected == item.index ? (
                <Icon
                  name="ios-checkmark-circle"
                  type="ionicon"
                  color={asitaColor.green}
                  size={22}
                />
              ) : (
                <Text></Text>
              )}
            </Col>
          </Row>
        </TouchableOpacity>
      </Grid>
    );
  };
  // SeatClass FlatList Function

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  handleCHange = data => {
    data;
  };
  _keyExtractor = (item, index) => index.toString();

  // ========================== GLOBAL FUNC

  // ============================= SEARCHING AUTOCOMPLETE
  searchingDeparture = val => {
    if (val !== '') {
      this.setState({ departureResult: '' }, () => {
        let data = this.filterValuePart(this.props.listAirport, val);
        this.setState({ departureResult: data });
      });
    } else {
      this.setState({ departureResult: [] });
    }
  };

  filterValuePart = (arr, part) => {
    return arr.filter(function(item) {
      const itemData =
        item.airport_code.toUpperCase() +
        ' ' +
        item.international_airport_name.toUpperCase() +
        '  ' +
        item.city_name.toUpperCase();
      const textData = part.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
  };
  _switchDestination = () => {
    const { origin, destination } = this.state;
    let or = [];
    let des = [];
    or.push(destination);
    des.push(origin);
    if (or.length !== 0 && des.length !== 0) {
      this.setState({ origin: or[0], destination: des[0] });
    }
  };
  // ===================================== SEARCHING AUTOCOMPLETE

  //Main Render
  render() {
    let passengerAdult = this.state.adult + ' Adult';
    let passengerChild =
      this.state.child > 0 ? ', ' + this.state.child + ' Child' : '';
    let passengerInfant =
      this.state.infant > 0 ? ', ' + this.state.infant + ' Infant' : '';
    const passengerData = passengerAdult + passengerChild + passengerInfant;
    const isDateBlocked = date => date.isBefore(moment(), 'day');
    let mytextorigin = this.state.origin.city_name;
    let maytextdestination = this.state.destination.city_name;
    let { loadingAirport } = this.props;
    return (
      <View style={{ backgroundColor: asitaColor.backWhite, flex: 1 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <HeaderPage
            title="Booking Your Flights"
            callback={this.goBackHeader}
            {...this.props}
          />
          <SubHeaderPage />

          <Grid style={styles.sectionToggle}>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              <MultiSwitch
                currentStatus={'Open'}
                disableScroll={value => {}}
                onStatusChanged={text => {
                  if (text == 'Single') {
                    this.setState({ flightType: 'single' });
                  } else if (text == 'Return') {
                    let departureDate = this.state.departureDate;
                    this.setState({
                      flightType: 'return',
                      returnDate: moment(departureDate)
                        .add(1, 'days')
                        .format('YYYY-MM-DD'),
                    });
                  } else {
                    this.setSeatClass({ flightType: 'single' });
                  }
                }}
              />
            </Col>
          </Grid>

          {/* =============================== REVAMP FLIGHT SEARCH ===================================== */}
          <Grid style={styles.card}>
            <Col
              size={4}
              onPress={this._setModal1}
              style={{ justifyContent: 'center', alignItems: 'flex-start' }}
            >
              <View style={{ paddingBottom: 10 }}>
                <Text style={styles.textLabel}>From</Text>
              </View>
              <View>
                <Text style={styles.textBold}>
                  {loadingAirport
                    ? 'Loading....'
                    : mytextorigin.length > 13
                    ? mytextorigin.substring(0, 13 - 3) + '...'
                    : mytextorigin}
                </Text>
              </View>
              <View>
                <Text style={styles.textInitial}>
                  ({this.state.origin.airport_code})
                </Text>
              </View>
            </Col>
            <Col
              size={2}
              style={{ justifyContent: 'center', alignItems: 'center' }}
            >
              <TouchableOpacity
                onPress={this._switchDestination}
                style={{ justifyContent: 'center', alignItems: 'center' }}
              >
                <View>
                  <Image
                    source={require('../../../assets/icons/flight_path.png')}
                    style={{
                      width: 25,
                      height: 30,
                      marginBottom: -16,
                      tintColor: asitaColor.orange,
                    }}
                    resizeMode="center"
                  />
                </View>
                <View>
                  <Image
                    source={require('../../../assets/icons/flight_return.png')}
                    style={{
                      width: 45,
                      height: 40,
                      marginTop: -20,
                      tintColor: asitaColor.marineBlue,
                    }}
                    resizeMode="contain"
                  />
                </View>
              </TouchableOpacity>
            </Col>
            <Col
              size={4}
              onPress={this._setModal2}
              style={{ justifyContent: 'center', alignItems: 'flex-end' }}
            >
              <View style={{ paddingBottom: 10 }}>
                <Text style={styles.textLabel}>To</Text>
              </View>
              <View>
                <Text style={styles.textBold}>
                  {loadingAirport
                    ? 'Loading....'
                    : maytextdestination.length > 13
                    ? maytextdestination.substring(0, 13 - 3) + '...'
                    : maytextdestination
                  // maytextdestination
                  }
                </Text>
              </View>
              <View>
                <Text style={styles.textInitial}>
                  ({this.state.destination.airport_code})
                </Text>
              </View>
            </Col>
          </Grid>
          <Grid>
            <Col>
              <InputTextRevamp
                onPress={this._setModal5}
                editable={moment(this.state.departureDate).format(
                  'DD MMM YYYY'
                )}
                label="Departure Date"
                source={require('../../../assets/icons/icon_departure.png')}
              />
            </Col>
          </Grid>
          {this.state.flightType == 'return' ? (
            <Grid>
              <Col>
                <InputTextRevamp
                  onPress={this._setModal6}
                  editable={moment(this.state.returnDate).format('DD MMM YYYY')}
                  label="Return Date"
                  source={require('../../../assets/icons/icon_return.png')}
                />
              </Col>
            </Grid>
          ) : null}

          <Grid>
            <Col>
              <InputTextRevamp
                onPress={this._setModal3}
                editable={passengerData}
                label="Total Passenger"
                source={require('../../../assets/icons/icon_total_passenger.png')}
              />
            </Col>
          </Grid>
          <Grid>
            <Col>
              <InputTextRevamp
                onPress={this._setModal4}
                editable={this.state.seatClass}
                label="Cabin Class"
                source={require('../../../assets/icons/icon_cabin_class.png')}
              />
            </Col>
          </Grid>

          <Grid style={[styles.sectionButtonArea, { marginVertical: 25 }]}>
            <View style={styles.sectionButton}>
              <ButtonRounded
                label="SEARCH FLIGHTS"
                onClick={this.searchFlight}
              />
            </View>
          </Grid>
          {/* =============================== REVAMP FLIGHT SEARCH ===================================== */}
        </ScrollView>

        {/* ========= MODAL LOADING ========= */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 99}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={250}
          animationOutTiming={250}
          backdropTransitionInTiming={250}
          backdropTransitionOutTiming={250}
        >
          {this.loadingBooking()}
        </Modal>
        {/* ========= MODAL LOADING ========= */}

        {/* =============================== MODAL DEPARTURE DATE & RETURN DATE ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={
            this.state.visibleModal === 5 || this.state.visibleModal === 6
          }
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View
            style={[
              styles.modalContent,
              { paddingTop: Platform.OS === 'ios' ? 30 : 0 },
            ]}
          >
            <Grid style={styles.modalHeaderDatepicker}>
              <Row style={{ paddingBottom: 7.5 }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={this._setNullModal}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color={asitaColor.white}
                      size={28}
                    />
                  </Col>
                  <Col size={6.5} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 5
                        ? 'Departure Date'
                        : 'Return Date'}
                    </Text>
                  </Col>
                  <Col size={1.5} />
                </TouchableOpacity>
              </Row>
            </Grid>
            <Grid>
              <Col>
                <View style={{ flex: 1 }}>
                  <Dates
                    date={this.state.date}
                    onDatesChange={
                      this.state.visibleModal === 5
                        ? this.setDepartureDate
                        : this.setReturnDate
                    }
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    focusedInput={this.state.focus}
                    isDateBlocked={isDateBlocked}
                    focusedInput={this.state.focus}
                  />
                  <Text
                    style={{
                      paddingTop: 20,
                      fontSize: 16,
                      color: asitaColor.black,
                      paddingLeft: 20,
                    }}
                  >
                    {this.state.visibleModal === 5
                      ? 'Departure Date: ' +
                        moment(this.state.departureDate).format('DD MMM YYYY')
                      : 'Return Date: ' +
                        moment(this.state.returnDate).format('DD MMM YYYY')}
                  </Text>
                </View>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL DEPARTURE DATE & RETURN DATE ===================================== */}

        {/* =============================== MODAL DEPARTURE & DESTINATION ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={
            this.state.visibleModal === 1 || this.state.visibleModal === 2
          }
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View
            style={[
              styles.modalContent,
              {
                paddingTop: Platform.OS === 'ios' ? 30 : 0,
                backgroundColor: asitaColor.marineBlue,
              },
            ]}
          >
            <Grid style={{ flex: 0 }}>
              <Row
                style={[
                  styles.modalHeader,
                  { paddingTop: Platform.OS === 'ios' ? 15 : 0 },
                ]}
              >
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={this._setNullModal}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color={asitaColor.white}
                      size={28}
                    />
                  </Col>
                  <Col size={6.5} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 1
                        ? 'Select Departure'
                        : 'Select Destination'}
                    </Text>
                  </Col>
                  <Col size={1.5} />
                </TouchableOpacity>
              </Row>
              <SubHeaderPage />
              <Row style={styles.modalSearch}>
                <Col>
                  <SearchInput
                    placeholder="Search City or Airport"
                    onChangeText={data => this.searchingDeparture(data)}
                  />
                </Col>
              </Row>
            </Grid>
            <Grid>
              <Col
                style={{
                  backgroundColor: asitaColor.white,
                  padding: 20,
                  paddingTop: 10,
                  borderWidth: 0.5,
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  borderColor: asitaColor.white,
                }}
              >
                <ScrollView showsVerticalScrollIndicator={false}>
                  <Grid
                    style={{
                      flex: 1,
                      backgroundColor: asitaColor.white,
                      padding: 10,
                      paddingBottom: 10,
                      paddingLeft: 10,
                    }}
                  >
                    <Col style={{ alignItems: 'flex-start' }}>
                      <Text
                        style={{
                          fontFamily: fontReguler,
                          color: asitaColor.brownGrey,
                          fontSize: 16,
                        }}
                      >
                        Popular Destination
                      </Text>
                    </Col>
                  </Grid>
                  <Grid>
                    <Col>
                      <FlatList
                        showsVerticalScrollIndicator={false}
                        keyExtractor={this._keyExtractor}
                        data={
                          this.state.departureResult.length > 0
                            ? this.state.departureResult
                            : this.props.listAirport
                        }
                        removeClippedSubviews={true}
                        renderItem={({ item, index }) => (
                          <Grid
                            key={index}
                            style={{
                              flex: 1,
                              backgroundColor: thameColors.white,
                              padding: 10,
                              paddingBottom: 0,
                            }}
                          >
                            <TouchableOpacity
                              style={{
                                flexDirection: 'row',
                                flex: 1,
                                borderBottomWidth: 0.5,
                                borderBottomColor: thameColors.gray,
                                paddingBottom: 10,
                              }}
                              onPress={() =>
                                this.setOriginDestination(
                                  this.state.visibleModal,
                                  item
                                )
                              }
                            >
                              <Col style={{ alignItems: 'flex-start' }}>
                                <Text
                                  style={{
                                    fontFamily: fontBold,
                                    color: thameColors.textBlack,
                                    fontSize: 16,
                                  }}
                                >
                                  {item.city_name}
                                </Text>
                                <Text
                                  style={{
                                    fontFamily: fontReguler,
                                    color: thameColors.darkGray,
                                  }}
                                >
                                  {item.airport_code} -{' '}
                                  {item.international_airport_name}
                                </Text>
                              </Col>
                            </TouchableOpacity>
                          </Grid>
                        )}
                        ListEmptyComponent={
                          <View
                            style={{
                              flex: 1,
                              backgroundColor: thameColors.white,
                              padding: 10,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: fontBold,
                                color: thameColors.textBlack,
                                fontSize: 16,
                              }}
                            >
                              Oops, failed to load Airport List!.
                            </Text>
                            <TouchableOpacity
                              onPress={this.getAirport}
                              style={{ marginTop: 10 }}
                            >
                              <Text
                                style={{
                                  fontFamily: fontBold,
                                  color: thameColors.primary,
                                  fontSize: 16,
                                }}
                              >
                                Try again
                              </Text>
                            </TouchableOpacity>
                          </View>
                        }
                        refreshControl={
                          <RefreshControl
                            onRefresh={this.getAirport}
                            refreshing={this.props.loadingAirport}
                          />
                        }
                      />
                    </Col>
                  </Grid>
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL DEPARTURE & DESTINATION ===================================== */}

        {/* =============================== MODAL PASSENGER ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 3}
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: asitaColor.brownishGrey,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._setNullModal}
              >
                <Col style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      color: asitaColor.superBlack,
                      fontSize: 16,
                      fontFamily: fontBold,
                    }}
                  >
                    Passenger
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      color: asitaColor.superBlack,
                      fontSize: 14,
                      fontFamily: fontBold,
                      textAlign: 'center',
                    }}
                  >
                    Adult
                  </Text>
                  <Text
                    style={{
                      color: asitaColor.brownGrey,
                      fontSize: 12,
                      fontFamily: fontReguler,
                    }}
                  >
                    Age 12+
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={[0, 1, 2, 3, 4, 5, 6, 7]}
                    selectedIndex={this.state.adult}
                    onValueChange={(data, selectedIndex) =>
                      this.setPassengerData('adult', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={asitaColor.white}
                    itemHeight={60}
                    highlightColor={asitaColor.brownishGrey}
                    highlightBorderWidth={0}
                    activeItemColor={asitaColor.textBlack}
                    itemColor={asitaColor.orange}
                  />
                </View>
              </Col>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      color: thameColors.superBack,
                      fontSize: 14,
                      fontFamily: fontBold,
                      textAlign: 'center',
                    }}
                  >
                    Child
                  </Text>
                  <Text
                    style={{
                      color: thameColors.darkGray,
                      fontSize: 12,
                      fontFamily: fontReguler,
                    }}
                  >
                    Age 2 - 11
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={['-', 0, 1, 2, 3, 4, 5, 6]}
                    selectedIndex={this.state.child + 1}
                    renderItem={(data, index, isSelected) => {}}
                    onValueChange={(data, selectedIndex) =>
                      this.setPassengerData('child', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.buttonGray}
                    highlightBorderWidth={0}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      color: thameColors.superBack,
                      fontSize: 14,
                      fontFamily: fontBold,
                      textAlign: 'center',
                    }}
                  >
                    Infant
                  </Text>
                  <Text
                    style={{
                      color: thameColors.darkGray,
                      fontSize: 12,
                      fontFamily: fontReguler,
                    }}
                  >
                    Bellow age 2
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={['-', 0, 1, 2, 3, 4]}
                    selectedIndex={this.state.infant + 1}
                    renderItem={(data, index, isSelected) => {}}
                    onValueChange={(data, selectedIndex) =>
                      this.setPassengerData('infant', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.buttonGray}
                    highlightBorderWidth={0}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
            </Grid>
            <Grid style={{ flex: 0, paddingBottom: 10 }}>
              <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                <ButtonRounded label="DONE" onClick={this._setNullModal} />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL PASSENGER ===================================== */}

        {/* =============================== MODAL SEAT CLASS ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 4}
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.buttonGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._setNullModal}
              >
                <Col style={{ alignItems: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.superBack,
                      fontSize: 16,
                      fontFamily: fontBold,
                    }}
                  >
                    Seat Class
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: thameColors.white }}>
                <ScrollView>
                  <FlatList
                    keyExtractor={this._keyExtractor}
                    data={this.state.seatClassType}
                    renderItem={this._renderSeatClass}
                    removeClippedSubviews={true}
                    getItemLayout={(data, index) => ({
                      length: 3,
                      offset: 3 * index,
                      index,
                    })}
                  />
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL SEAT CLASS ===================================== */}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    listAirport: makeGetAirport(state),
    loadingAirport: state.flight.loadingAirport,
  };
};
export default connect(mapStateToProps)(SearchFlight);
