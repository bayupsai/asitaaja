export default DepartureList = [
  {
    code: 'APW',
    name: ' Faleolo',
  },
  {
    code: 'KLO',
    name: ' Kalibo',
  },
  {
    code: 'AAL',
    name: 'Aalborg ',
  },
  {
    code: 'AAR',
    name: 'Aarhus',
  },
  {
    code: 'JIM',
    name: 'Aba Segud ',
  },
  {
    code: 'DIR',
    name: 'Aba Tenna Dejazmach Yilma ',
  },
  {
    code: 'MLG',
    name: 'Abdul Rachman Saleh',
  },
  {
    code: 'ZNZ',
    name: 'Abeid Amani Karume ',
  },
  {
    code: 'ABZ',
    name: 'Aberdeen ',
  },
  {
    code: 'ABI',
    name: 'Abilene ',
  },
  {
    code: 'AUH',
    name: 'Abu Dhabi ',
  },
  {
    code: 'SRG',
    name: 'Achmad Yani',
  },
  {
    code: 'ADA',
    name: 'Adana Sakirpasa ',
  },
  {
    code: 'ADD',
    name: 'Addis Ababa Bole ',
  },
  {
    code: 'ADL',
    name: 'Adelaide',
  },
  {
    code: 'ADE',
    name: 'Aden',
  },
  {
    code: 'MGQ',
    name: 'Aden Adde',
  },
  {
    code: 'JOG',
    name: 'Adi Sutjipto',
  },
  {
    code: 'SOC',
    name: 'Adisumarmo',
  },
  {
    code: 'ADF',
    name: 'Adiyaman',
  },
  {
    code: 'AEG',
    name: 'Aek Godang ',
  },
  {
    code: 'CWB',
    name: 'Afonso Pena ',
  },
  {
    code: 'AJI',
    name: 'Agri',
  },
  {
    code: 'AGU',
    name: 'Aguascalientes ',
  },
  {
    code: 'AIT',
    name: 'Aitutaki  Airport',
  },
  {
    code: 'ENU',
    name: 'Akanu Ibiam ',
  },
  {
    code: 'AXT',
    name: 'Akita ',
  },
  {
    code: 'CAK',
    name: 'Akron Canton ',
  },
  {
    code: 'AKU',
    name: 'Aksu ',
  },
  {
    code: 'AAN',
    name: 'Al Ain ',
  },
  {
    code: 'DWC',
    name: 'Al Maktoum ',
  },
  {
    code: 'NJF',
    name: 'Al Najaf International',
  },
  {
    code: 'NOV',
    name: 'Albano Machado ',
  },
  {
    code: 'ALB',
    name: 'Albany ',
  },
  {
    code: 'ALH',
    name: 'Albany Airport',
  },
  {
    code: 'OAJ',
    name: 'Albert J. Ellis ',
  },
  {
    code: 'ABQ',
    name: 'Albuquerque ',
  },
  {
    code: 'ABX',
    name: 'Albury ',
  },
  {
    code: 'LEG',
    name: 'Aleg ',
  },
  {
    code: 'AES',
    name: 'Alesund Airportn Vigra',
  },
  {
    code: 'AEX',
    name: 'Alexandria ',
  },
  {
    code: 'ALC',
    name: 'Alicanteelche',
  },
  {
    code: 'ASP',
    name: 'Alice Springs',
  },
  {
    code: 'LHE',
    name: 'Allama Iqbal ',
  },
  {
    code: 'ALA',
    name: 'Almaty ',
  },
  {
    code: 'LEI',
    name: 'Almeria ',
  },
  {
    code: 'ARD',
    name: 'Alor Island',
  },
  {
    code: 'AAT',
    name: 'Altay',
  },
  {
    code: 'MQX',
    name: 'Alula Aba Nega ',
  },
  {
    code: 'ASJ',
    name: 'Amami ',
  },
  {
    code: 'MZH',
    name: 'Amasya Merzifon',
  },
  {
    code: 'MJN',
    name: 'Amborovy ',
  },
  {
    code: 'SAL',
    name: 'Amilcar Cabral',
  },
  {
    code: 'AQG',
    name: 'Anqing Tianzhushan ',
  },
  {
    code: 'AOG',
    name: 'Anshan Tengao ',
  },
  {
    code: 'AVA',
    name: 'Anshun Huangguoshu',
  },
  {
    code: 'AYT',
    name: 'Antalya ',
  },
  {
    code: 'GUM',
    name: 'Antonio B. Won Pat',
  },
  {
    code: 'VQS',
    name: 'Antonio Rivera Rodriguez',
  },
  {
    code: 'ANM',
    name: 'Antsirabato ',
  },
  {
    code: 'AOJ',
    name: 'Aomori ',
  },
  {
    code: 'ATW',
    name: 'Appleton ',
  },
  {
    code: 'AMH',
    name: 'Arba Minch ',
  },
  {
    code: 'STO',
    name: 'Arlanda ',
  },
  {
    code: 'ARM',
    name: 'Armidale ',
  },
  {
    code: 'DIE',
    name: 'Arrachart ',
  },
  {
    code: 'AKJ',
    name: 'Asahikawa',
  },
  {
    code: 'AVL',
    name: 'Asheville Regional Airport',
  },
  {
    code: 'ASB',
    name: 'Ashgabat ',
  },
  {
    code: 'ASO',
    name: 'Asosa ',
  },
  {
    code: 'TSE',
    name: 'Astana',
  },
  {
    code: 'OVD',
    name: 'Asturias ',
  },
  {
    code: 'IST',
    name: 'Ataturk',
  },
  {
    code: 'PXA',
    name: 'Atung Bungsu',
  },
  {
    code: 'AKL',
    name: 'Auckland',
  },
  {
    code: 'AGS',
    name: 'Augusta Regional ',
  },
  {
    code: 'MGA',
    name: 'Augusto C Sandino ',
  },
  {
    code: 'AUS',
    name: 'Austin bergstrom ',
  },
  {
    code: 'AVV',
    name: 'Avalon',
  },
  {
    code: 'CBO',
    name: 'Awang',
  },
  {
    code: 'AXU',
    name: 'Axum ',
  },
  {
    code: 'ADB',
    name: 'Azmir Adnan Menderes',
  },
  {
    code: 'BXB',
    name: 'Babo ',
  },
  {
    code: 'PEW',
    name: 'Bacha Khan ',
  },
  {
    code: 'BCD',
    name: 'Bacolodasilay',
  },
  {
    code: 'BJZ',
    name: 'Badajoz ',
  },
  {
    code: 'BGW',
    name: 'Baghdad ',
  },
  {
    code: 'BHV',
    name: 'Bahawalpur ',
  },
  {
    code: 'BJR',
    name: 'Bahir Dar',
  },
  {
    code: 'BAH',
    name: 'Bahrain ',
  },
  {
    code: 'BKM',
    name: 'Bakelalan ',
  },
  {
    code: 'BNK',
    name: 'Ballina Byron Gateway',
  },
  {
    code: 'BWI',
    name: 'Baltimoreawashington ',
  },
  {
    code: 'BKO',
    name: 'Bamakoasenou',
  },
  {
    code: 'BXU',
    name: 'Bancasi ',
  },
  {
    code: 'CMB',
    name: 'Bandaranaike ',
  },
  {
    code: 'BLR',
    name: 'Bangalore',
  },
  {
    code: 'BGR',
    name: 'Bangor ',
  },
  {
    code: 'BGF',
    name: 'Bangui M Poko ',
  },
  {
    code: 'BJL',
    name: 'Banjul ',
  },
  {
    code: 'BSD',
    name: 'Baoshan Yunrui',
  },
  {
    code: 'BAV',
    name: 'Baotou ',
  },
  {
    code: 'BCI',
    name: 'Barcaldine ',
  },
  {
    code: 'BCN',
    name: 'Barcelona',
  },
  {
    code: 'BRI',
    name: 'Bari Karol Wojtyaa ',
  },
  {
    code: 'BBN',
    name: 'Bario ',
  },
  {
    code: 'NBB',
    name: 'Barranco Minas ',
  },
  {
    code: 'BSO',
    name: 'Basco ',
  },
  {
    code: 'BSR',
    name: 'Basra',
  },
  {
    code: 'BAL',
    name: 'Batman Airport',
  },
  {
    code: 'BTR',
    name: 'Baton Rouge Metropolitan',
  },
  {
    code: 'BTC',
    name: 'Batticaloa ',
  },
  {
    code: 'BTW',
    name: 'Batu Licin',
  },
  {
    code: 'BUS',
    name: 'Batumi ',
  },
  {
    code: 'BUW',
    name: 'Baubau',
  },
  {
    code: 'VLI',
    name: 'Bauerfield ',
  },
  {
    code: 'BHY',
    name: 'Beihai Fucheng ',
  },
  {
    code: 'PEK',
    name: 'Beijing Capital',
  },
  {
    code: 'BEY',
    name: 'Beirutrafic Hariri ',
  },
  {
    code: 'BEG',
    name: 'Belgrade Nikola Tesla',
  },
  {
    code: 'TLV',
    name: 'Ben Gurion ',
  },
  {
    code: 'ISB',
    name: 'Benazir Bhutto ',
  },
  {
    code: 'BEN',
    name: 'Benina International Airport ',
  },
  {
    code: 'MEX',
    name: 'Benito Ju¡rez',
  },
  {
    code: 'BGO',
    name: 'Bergen',
  },
  {
    code: 'MTW',
    name: 'Beringin',
  },
  {
    code: 'TXL',
    name: 'Berlin Tegel',
  },
  {
    code: 'BFJ',
    name: 'Bijie Feixiong ',
  },
  {
    code: 'BIO',
    name: 'Bilbao ',
  },
  {
    code: 'BLL',
    name: 'Billund',
  },
  {
    code: 'ZBL',
    name: 'Biloela ',
  },
  {
    code: 'GNS',
    name: 'Binaka',
  },
  {
    code: 'BGG',
    name: 'Bingol Airport',
  },
  {
    code: 'BTU',
    name: 'Bintulu',
  },
  {
    code: 'BHX',
    name: 'Birmingham ',
  },
  {
    code: 'BHM',
    name: 'Birmingham shuttlesworth ',
  },
  {
    code: 'FNT',
    name: 'Bishop ',
  },
  {
    code: 'BKQ',
    name: 'Blackall ',
  },
  {
    code: 'BWX',
    name: 'Blimbingsari',
  },
  {
    code: 'BFN',
    name: 'Bloemfontein ',
  },
  {
    code: 'LEX',
    name: 'Blue Grass ',
  },
  {
    code: 'BOI',
    name: 'Boise ',
  },
  {
    code: 'BOD',
    name: 'Bordeaux Merignac',
  },
  {
    code: 'HBE',
    name: 'Borg El Arab',
  },
  {
    code: 'KBP',
    name: 'Boryspil ',
  },
  {
    code: 'ALG',
    name: 'Boumedienne ',
  },
  {
    code: 'BDL',
    name: 'Bradley ',
  },
  {
    code: 'SWQ',
    name: 'Brang Biji',
  },
  {
    code: 'BSB',
    name: 'Brasilia',
  },
  {
    code: 'BRE',
    name: 'Bremen ',
  },
  {
    code: 'BNE',
    name: 'Brisbane',
  },
  {
    code: 'BRS',
    name: 'Bristol ',
  },
  {
    code: 'BME',
    name: 'Broome ',
  },
  {
    code: 'BWN',
    name: 'Brunei',
  },
  {
    code: 'BQK',
    name: 'Brunswick Golden Isles Airport',
  },
  {
    code: 'BRU',
    name: 'Brussels',
  },
  {
    code: 'OTP',
    name: 'Bucharest Henri Coanda',
  },
  {
    code: 'ROK',
    name: 'Budapest Ferenc Liszt ',
  },
  {
    code: 'BUF',
    name: 'Buffalo Niagara ',
  },
  {
    code: 'BJM',
    name: 'Bujumbura',
  },
  {
    code: 'BUA',
    name: 'Buka ',
  },
  {
    code: 'WUB',
    name: 'Buli',
  },
  {
    code: 'BUL',
    name: 'Bulolo ',
  },
  {
    code: 'BDB',
    name: 'Bundaberg ',
  },
  {
    code: 'BMV',
    name: 'Buon Ma Thuot',
  },
  {
    code: 'BTV',
    name: 'Burlington',
  },
  {
    code: 'BQB',
    name: 'Busselton Regional Airport',
  },
  {
    code: 'CAB',
    name: 'Cabinda  Aeroporto De Cabinda',
  },
  {
    code: 'COO',
    name: 'Cadjehoun  ',
  },
  {
    code: 'CNS',
    name: 'Cairns',
  },
  {
    code: 'CAI',
    name: 'Cairo ',
  },
  {
    code: 'CYP',
    name: 'Calbayog ',
  },
  {
    code: 'YYC',
    name: 'Calgary ',
  },
  {
    code: 'CCJ',
    name: 'Calicut ',
  },
  {
    code: 'CKZ',
    name: 'Canakkale Airport',
  },
  {
    code: 'CBR',
    name: 'Canberra',
  },
  {
    code: 'CUN',
    name: 'Cancun ',
  },
  {
    code: 'CPT',
    name: 'Cape Town ',
  },
  {
    code: 'CWL',
    name: 'Cardiff',
  },
  {
    code: 'MVD',
    name: 'Carrasco ',
  },
  {
    code: 'CXI',
    name: 'Cassidy ',
  },
  {
    code: 'HPH',
    name: 'Cat Bi',
  },
  {
    code: 'IGU',
    name: 'Cataratas ',
  },
  {
    code: 'CRM',
    name: 'Catarman National',
  },
  {
    code: 'CBT',
    name: 'Catumbela ',
  },
  {
    code: 'KCO',
    name: 'Cengiz Topel Naval',
  },
  {
    code: 'BMI',
    name: 'Central Illinois Regional Airport',
  },
  {
    code: 'GRI',
    name: 'Central Nebraska Regional Airport',
  },
  {
    code: 'CWA',
    name: 'Central Wisconsin ',
  },
  {
    code: 'NBS',
    name: 'Changbaishan ',
  },
  {
    code: 'CGQ',
    name: 'Changchun Longjia ',
  },
  {
    code: 'CGD',
    name: 'Changde Taohuayuan ',
  },
  {
    code: 'SIN',
    name: 'Changi',
  },
  {
    code: 'CSX',
    name: 'Changsha Huanghua ',
  },
  {
    code: 'CIH',
    name: 'Changzhi Wangcun ',
  },
  {
    code: 'CZX',
    name: 'Changzhou Benniu ',
  },
  {
    code: 'STS',
    name: 'Charles M. Schulz',
  },
  {
    code: 'CHS',
    name: 'Charleston ',
  },
  {
    code: 'CTL',
    name: 'Charleville',
  },
  {
    code: 'CLT',
    name: 'Charlotte Douglas ',
  },
  {
    code: 'CHO',
    name: 'Charlottesville albemarle',
  },
  {
    code: 'CHA',
    name: 'Chattanooga Metropolitan',
  },
  {
    code: 'LKO',
    name: 'Chaudhary Charan Singh',
  },
  {
    code: 'CTU',
    name: 'Chengdu Shuangliu',
  },
  {
    code: 'MAA',
    name: 'Chennai',
  },
  {
    code: 'CJJ',
    name: 'Cheongju ',
  },
  {
    code: 'TVC',
    name: 'Cherry Capital ',
  },
  {
    code: 'BOM',
    name: 'Chhatrapati Shivaji',
  },
  {
    code: 'CNX',
    name: 'Chiang Mai',
  },
  {
    code: 'CEI',
    name: 'Chiang Rai',
  },
  {
    code: 'BLZ',
    name: 'Chileka ',
  },
  {
    code: 'CMU',
    name: 'Chimbu ',
  },
  {
    code: 'TRR',
    name: 'China Bay ',
  },
  {
    code: 'RMQ',
    name: 'Ching Chuan Kang Air Base',
  },
  {
    code: 'ULN',
    name: 'Chinggis Khaan',
  },
  {
    code: 'KIV',
    name: 'Chisinau International',
  },
  {
    code: 'CJL',
    name: 'Chitral ',
  },
  {
    code: 'JUH',
    name: 'Chizhou Jiuhuashan ',
  },
  {
    code: 'CKG',
    name: 'Chongqing Jiangbei',
  },
  {
    code: 'CHC',
    name: 'Christchurch',
  },
  {
    code: 'XCH',
    name: 'Christmas Island ',
  },
  {
    code: 'NGO',
    name: 'Chubu Centrair',
  },
  {
    code: 'STI',
    name: 'Cibao ',
  },
  {
    code: 'CGU',
    name: 'Ciudad Guayana',
  },
  {
    code: 'CRK',
    name: 'Clark',
  },
  {
    code: 'CFE',
    name: 'Clermont ferrand Auvergne',
  },
  {
    code: 'CLE',
    name: 'Cleveland Hopkins ',
  },
  {
    code: 'LIT',
    name: 'Clinton ',
  },
  {
    code: 'CNJ',
    name: 'Cloncurry ',
  },
  {
    code: 'EWN',
    name: 'Coastal Carolina Regional Airport',
  },
  {
    code: 'CCK',
    name: 'Cocos Island',
  },
  {
    code: 'CFS',
    name: 'Coffs Harbour',
  },
  {
    code: 'CJB',
    name: 'Coimbatore ',
  },
  {
    code: 'CGN',
    name: 'Cologne Bonn ',
  },
  {
    code: 'COS',
    name: 'Colorado Springs',
  },
  {
    code: 'COU',
    name: 'Columbia Regional Airport',
  },
  {
    code: 'CAE',
    name: 'Columbian Metropolitan',
  },
  {
    code: 'CSG',
    name: 'Columbus Metropolitan',
  },
  {
    code: 'FTE',
    name: 'Comandante Armando Tola',
  },
  {
    code: 'SCL',
    name: 'Comodoro Arturo Merino Benitez',
  },
  {
    code: 'CKY',
    name: 'Conakry ',
  },
  {
    code: 'CPH',
    name: 'Copenhagen ',
  },
  {
    code: 'ORK',
    name: 'Cork ',
  },
  {
    code: 'CRP',
    name: 'Corpus Christi ',
  },
  {
    code: 'NCE',
    name: 'Cote Dazur ',
  },
  {
    code: 'CZM',
    name: 'Cozumel ',
  },
  {
    code: 'GRU',
    name: 'Cumbica ',
  },
  {
    code: 'CUR',
    name: 'Curacao',
  },
  {
    code: 'MEQ',
    name: 'Cut Nyak Dien',
  },
  {
    code: 'STT',
    name: 'Cyril E King ',
  },
  {
    code: 'DAD',
    name: 'Da Nang',
  },
  {
    code: 'TAE',
    name: 'Daegu ',
  },
  {
    code: 'DLM',
    name: 'Dalaman ',
  },
  {
    code: 'DBA',
    name: 'Dalbandin ',
  },
  {
    code: 'DLU',
    name: 'Dali ',
  },
  {
    code: 'DLC',
    name: 'Dalian Zhoushuizi ',
  },
  {
    code: 'DAL',
    name: 'Dallas Love Field',
  },
  {
    code: 'DDG',
    name: 'Dandong Langtou ',
  },
  {
    code: 'MSN',
    name: 'Dane County Regional Airport',
  },
  {
    code: 'TAC',
    name: 'Daniel Z Romualdez',
  },
  {
    code: 'DQA',
    name: 'Daqing Sartu ',
  },
  {
    code: 'DAU',
    name: 'Daru ',
  },
  {
    code: 'DRW',
    name: 'Darwin',
  },
  {
    code: 'DAT',
    name: 'Datong Yungang ',
  },
  {
    code: 'DAY',
    name: 'Dayton ',
  },
  {
    code: 'DAB',
    name: 'Daytona Beach ',
  },
  {
    code: 'BJX',
    name: 'Del Bajio',
  },
  {
    code: 'DNZ',
    name: 'Denizli Cardak',
  },
  {
    code: 'DEN',
    name: 'Denver ',
  },
  {
    code: 'PGK',
    name: 'Depati Amir',
  },
  {
    code: 'KRC',
    name: 'Depati Parbo',
  },
  {
    code: 'SSA',
    name: 'Deputado Luis Eduardo Magalhaes',
  },
  {
    code: 'DEA',
    name: 'Dera Ghazi Khan ',
  },
  {
    code: 'DSK',
    name: 'Dera Ismail Khan ',
  },
  {
    code: 'DSM',
    name: 'Des Moines ',
  },
  {
    code: 'VPS',
    name: 'Destin fort Walton Beach ',
  },
  {
    code: 'DTW',
    name: 'Detroit Metropolitan ',
  },
  {
    code: 'DPO',
    name: 'Devonport ',
  },
  {
    code: 'DAC',
    name: 'Dhaka',
  },
  {
    code: 'NIM',
    name: 'Diori Hamani ',
  },
  {
    code: 'DPL',
    name: 'Dipolog ',
  },
  {
    code: 'DIG',
    name: 'Diqing Shangri la ',
  },
  {
    code: 'DIY',
    name: 'Diyarbakr',
  },
  {
    code: 'JIB',
    name: 'Djibouti ambouli ',
  },
  {
    code: 'DNK',
    name: 'Dnipropetrovsk International',
  },
  {
    code: 'DOB',
    name: 'Dobo',
  },
  {
    code: 'SOQ',
    name: 'Dominique Edward Osok',
  },
  {
    code: 'DME',
    name: 'Domodedovo ',
  },
  {
    code: 'DMK',
    name: 'Don Mueang',
  },
  {
    code: 'DOK',
    name: 'Donetsk International',
  },
  {
    code: 'VDH',
    name: 'Dong Hoi',
  },
  {
    code: 'TBB',
    name: 'Dong Tac',
  },
  {
    code: 'DHN',
    name: 'Dothan',
  },
  {
    code: 'DLA',
    name: 'Douala ',
  },
  {
    code: 'DOM',
    name: 'Douglas charles ',
  },
  {
    code: 'DRS',
    name: 'Dresden ',
  },
  {
    code: 'DXB',
    name: 'Dubai ',
  },
  {
    code: 'XNB',
    name: 'Dubai City Bus Station',
  },
  {
    code: 'DBO',
    name: 'Dubbo City Regional Airport',
  },
  {
    code: 'DUB',
    name: 'Dublin',
  },
  {
    code: 'DBV',
    name: 'Dubrovnik ',
  },
  {
    code: 'DBQ',
    name: 'Dubuque Regional Airport',
  },
  {
    code: 'LUV',
    name: 'Dumatubun ',
  },
  {
    code: 'DUD',
    name: 'Dunedin',
  },
  {
    code: 'DNH',
    name: 'Dunhuang ',
  },
  {
    code: 'DRO',
    name: 'Durangola Plata County ',
  },
  {
    code: 'MME',
    name: 'Durham Tees Valley',
  },
  {
    code: 'DYU',
    name: 'Dushanbe I',
  },
  {
    code: 'DUS',
    name: 'Dusselfdorf ',
  },
  {
    code: 'DZA',
    name: 'Dzaoudzi pamandzi ',
  },
  {
    code: 'KNX',
    name: 'East Kimberley Regional Airport',
  },
  {
    code: 'ELS',
    name: 'East London ',
  },
  {
    code: 'GGG',
    name: 'East Texas Regional Airport',
  },
  {
    code: 'CLL',
    name: 'Easterwood ',
  },
  {
    code: 'EDI',
    name: 'Edinburgh ',
  },
  {
    code: 'YEG',
    name: 'Edmonton ',
  },
  {
    code: 'MAO',
    name: 'Eduardo Gomes ',
  },
  {
    code: 'LPB',
    name: 'El Alto ',
  },
  {
    code: 'BOG',
    name: 'El Dorado ',
  },
  {
    code: 'ELP',
    name: 'El Paso ',
  },
  {
    code: 'KOE',
    name: 'El Tari',
  },
  {
    code: 'PMC',
    name: 'El Tepual ',
  },
  {
    code: 'EZS',
    name: 'ElazÄ±g',
  },
  {
    code: 'EDL',
    name: 'Eldoret ',
  },
  {
    code: 'ATH',
    name: 'Eleftherios Venizelos',
  },
  {
    code: 'ELM',
    name: 'Elmira corning Regional Airport',
  },
  {
    code: 'EMD',
    name: 'Emerald ',
  },
  {
    code: 'ENH',
    name: 'Enshi Xujiaping ',
  },
  {
    code: 'EBB',
    name: 'Entebbe ',
  },
  {
    code: 'OMA',
    name: 'Eppley Airfield',
  },
  {
    code: 'EBL',
    name: 'Erbil ',
  },
  {
    code: 'ECN',
    name: 'Ercan ',
  },
  {
    code: 'ERI',
    name: 'Erie ',
  },
  {
    code: 'ASR',
    name: 'Erkilet International Airport ',
  },
  {
    code: 'ERC',
    name: 'Erzincan',
  },
  {
    code: 'ERZ',
    name: 'Erzurum',
  },
  {
    code: 'ESB',
    name: 'Esenboga',
  },
  {
    code: 'AOE',
    name: 'Eskiehir Hasan Polatkan Airport ',
  },
  {
    code: 'EPR',
    name: 'Esperance ',
  },
  {
    code: 'EUG',
    name: 'Eugene ',
  },
  {
    code: 'BSL',
    name: 'Euroa Basel Mulhouse Freiburg',
  },
  {
    code: 'EVV',
    name: 'Evansville Regional Airport',
  },
  {
    code: 'GGT',
    name: 'Exuma ',
  },
  {
    code: 'PPT',
    name: 'Faaa ',
  },
  {
    code: 'LYP',
    name: 'Faisalabad ',
  },
  {
    code: 'FKQ',
    name: 'Fakfak',
  },
  {
    code: 'FAO',
    name: 'Faro Airport ',
  },
  {
    code: 'NOS',
    name: 'Fascene ',
  },
  {
    code: 'BKS',
    name: 'Fatmawati Soekarno',
  },
  {
    code: 'FAY',
    name: 'Fayetteville Regional Airport',
  },
  {
    code: 'FCO',
    name: 'Fiumicino',
  },
  {
    code: 'FLG',
    name: 'Flagstaff Pulliam',
  },
  {
    code: 'BON',
    name: 'Flamingo',
  },
  {
    code: 'FLR',
    name: 'Florence Peretola',
  },
  {
    code: 'FLO',
    name: 'Florence Regional Airport',
  },
  {
    code: 'FLL',
    name: 'Fort Lauderdale hollywood ',
  },
  {
    code: 'YMM',
    name: 'Fort Mcmurray',
  },
  {
    code: 'FSM',
    name: 'Fort Smith Regional Airport',
  },
  {
    code: 'FWA',
    name: 'Fort Wayne ',
  },
  {
    code: 'DFW',
    name: 'Fort Worth ',
  },
  {
    code: 'USU',
    name: 'Francisco B Reyes',
  },
  {
    code: 'DVO',
    name: 'Francisco Bangoy',
  },
  {
    code: 'OPO',
    name: 'Francisco Sa Carneiro',
  },
  {
    code: 'TRC',
    name: 'Francisco Sarabia ',
  },
  {
    code: 'HOG',
    name: 'Frank Pauads Airport',
  },
  {
    code: 'FRA',
    name: 'Frankfurt ',
  },
  {
    code: 'BIK',
    name: 'Frans Kaisiepo',
  },
  {
    code: 'RTG',
    name: 'Frans Sales Lega',
  },
  {
    code: 'FAT',
    name: 'Fresno Yosemite ',
  },
  {
    code: 'FDH',
    name: 'Friedrichshafen',
  },
  {
    code: 'TBU',
    name: 'Fuaamotu',
  },
  {
    code: 'FUE',
    name: 'Fuerteventura ',
  },
  {
    code: 'FUJ',
    name: 'Fukue',
  },
  {
    code: 'FUK',
    name: 'Fukuoka',
  },
  {
    code: 'FKS',
    name: 'Fukushima',
  },
  {
    code: 'FUG',
    name: 'Fuyang Xiguan ',
  },
  {
    code: 'FOC',
    name: 'Fuzhou Changle ',
  },
  {
    code: 'GNV',
    name: 'Gainesville Regional Airport',
  },
  {
    code: 'GIG',
    name: 'Galeao ',
  },
  {
    code: 'GLX',
    name: 'Gamarmalamo',
  },
  {
    code: 'GMB',
    name: 'Gambela ',
  },
  {
    code: 'KVD',
    name: 'Ganja ',
  },
  {
    code: 'KOW',
    name: 'Ganzhou Huangjin ',
  },
  {
    code: 'GCK',
    name: 'Garden City Regional Airport',
  },
  {
    code: 'GZT',
    name: 'Gaziantep Oguzeli',
  },
  {
    code: 'GZP',
    name: 'Gazipasa',
  },
  {
    code: 'GDN',
    name: 'Gdansk Lech Waesa ',
  },
  {
    code: 'ZCL',
    name: 'General Leobardo C Ruiz ',
  },
  {
    code: 'MTY',
    name: 'General Mariano Escobedo',
  },
  {
    code: 'MKE',
    name: 'General Mitchell ',
  },
  {
    code: 'MZT',
    name: 'General Rafael Buelna ',
  },
  {
    code: 'CUU',
    name: 'General Roberto Fierro Villalobos',
  },
  {
    code: 'GES',
    name: 'General Santos',
  },
  {
    code: 'PIA',
    name: 'General Wayne A Downing Peoria',
  },
  {
    code: 'GVA',
    name: 'Geneve',
  },
  {
    code: 'GRJ',
    name: 'George ',
  },
  {
    code: 'BHD',
    name: 'George Best Belfast City',
  },
  {
    code: 'IAH',
    name: 'George Bush Intercontinental ',
  },
  {
    code: 'GRR',
    name: 'Gerald R Ford',
  },
  {
    code: 'GET',
    name: 'Geraldton ',
  },
  {
    code: 'LKA',
    name: 'Gewayantana',
  },
  {
    code: 'GIB',
    name: 'Gibraltar ',
  },
  {
    code: 'GIL',
    name: 'Gilgit ',
  },
  {
    code: 'PUS',
    name: 'Gimhae',
  },
  {
    code: 'GMP',
    name: 'Gimpo ',
  },
  {
    code: 'PNP',
    name: 'Girua ',
  },
  {
    code: 'GIS',
    name: 'Gisborne ',
  },
  {
    code: 'GLT',
    name: 'Gladstone ',
  },
  {
    code: 'GLA',
    name: 'Glasgow',
  },
  {
    code: 'GOA',
    name: 'Goa International',
  },
  {
    code: 'GDE',
    name: 'Gode Iddiole ',
  },
  {
    code: 'MPH',
    name: 'Godofredo P Ramos',
  },
  {
    code: 'OOL',
    name: 'Gold Coast',
  },
  {
    code: 'SKB',
    name: 'Golden Rock ',
  },
  {
    code: 'GTR',
    name: 'Golden Triangle Regional ',
  },
  {
    code: 'GDQ',
    name: 'Gondar ',
  },
  {
    code: 'GKA',
    name: 'Goroka ',
  },
  {
    code: 'GOV',
    name: 'Gove ',
  },
  {
    code: 'LPA',
    name: 'Gran Canaria ',
  },
  {
    code: 'GRX',
    name: 'Granada ',
  },
  {
    code: 'FPO',
    name: 'Grand Bahama ',
  },
  {
    code: 'GJT',
    name: 'Grand Junction Regional Airport',
  },
  {
    code: 'BGI',
    name: 'Grantley Adams ',
  },
  {
    code: 'BGM',
    name: 'Greater Binghamton ',
  },
  {
    code: 'ROC',
    name: 'Greater Rochester ',
  },
  {
    code: 'GRB',
    name: 'Green Bayaustin Straubel',
  },
  {
    code: 'GSP',
    name: 'Greenville spartanburg ',
  },
  {
    code: 'POP',
    name: 'Gregorio Luperon',
  },
  {
    code: 'GDL',
    name: 'Guadalajara ',
  },
  {
    code: 'CAN',
    name: 'Guangzhou Baiyun',
  },
  {
    code: 'BLQ',
    name: 'Guglielmo Marconi ',
  },
  {
    code: 'KWL',
    name: 'Guilin Liangjiang',
  },
  {
    code: 'KWE',
    name: 'Guiyang Longdongbao ',
  },
  {
    code: 'GPT',
    name: 'Gulfport biloxi ',
  },
  {
    code: 'GUR',
    name: 'Gurney ',
  },
  {
    code: 'GWD',
    name: 'Gwadar ',
  },
  {
    code: 'KWJ',
    name: 'Gwangju ',
  },
  {
    code: 'YKR',
    name: 'H Aroeppala',
  },
  {
    code: 'SMQ',
    name: 'H Asan Sampit',
  },
  {
    code: 'ENE',
    name: 'H Hasan Aroeboesman',
  },
  {
    code: 'TJQ',
    name: 'H a s  Hanandjoeddin',
  },
  {
    code: 'HAC',
    name: 'Hachijojima ',
  },
  {
    code: 'HAK',
    name: 'Haikou',
  },
  {
    code: 'HKD',
    name: 'Hakodate',
  },
  {
    code: 'YHZ',
    name: 'Halifax Stanfield',
  },
  {
    code: 'HLP',
    name: 'Halim Perdanakusuma',
  },
  {
    code: 'ABU',
    name: 'Haliwen',
  },
  {
    code: 'KDI',
    name: 'Haluoleo',
  },
  {
    code: 'DOH',
    name: 'Hamad ',
  },
  {
    code: 'HAM',
    name: 'Hamburg',
  },
  {
    code: 'HMI',
    name: 'Hami ',
  },
  {
    code: 'KBL',
    name: 'Hamid Karzai ',
  },
  {
    code: 'HLZ',
    name: 'Hamilton Airport ',
  },
  {
    code: 'HTI',
    name: 'Hamilton Island',
  },
  {
    code: 'HNA',
    name: 'Hanamaki',
  },
  {
    code: 'HDG',
    name: 'Handan',
  },
  {
    code: 'HND',
    name: 'Haneda',
  },
  {
    code: 'BTH',
    name: 'Hang Nadim',
  },
  {
    code: 'HGH',
    name: 'Hangzhou Xiaoshan',
  },
  {
    code: 'HAJ',
    name: 'Hannover',
  },
  {
    code: 'HRE',
    name: 'Harare ',
  },
  {
    code: 'HRB',
    name: 'Harbin Taiping ',
  },
  {
    code: 'HGA',
    name: 'Hargeisa Egal ',
  },
  {
    code: 'MDT',
    name: 'Harrisburg ',
  },
  {
    code: 'LVI',
    name: 'Harry Mwanga Nkumbula',
  },
  {
    code: 'ATL',
    name: 'Hartsfield jackson Atlanta ',
  },
  {
    code: 'HDY',
    name: 'Hat Yai',
  },
  {
    code: 'HTY',
    name: 'Hatay ',
  },
  {
    code: 'HIS',
    name: 'Hayman Island',
  },
  {
    code: 'FAR',
    name: 'Hector ',
  },
  {
    code: 'HFE',
    name: 'Hefei Xinqiao ',
  },
  {
    code: 'HEK',
    name: 'Heihe Aihui',
  },
  {
    code: 'HEL',
    name: 'Helsinki vantaa ',
  },
  {
    code: 'BUH',
    name: 'Henri Coanda',
  },
  {
    code: 'STX',
    name: 'Henry E Rohlsen ',
  },
  {
    code: 'HER',
    name: 'Heraklion ',
  },
  {
    code: 'HMO',
    name: 'Hermosillo ',
  },
  {
    code: 'HVB',
    name: 'Hervey Bay ',
  },
  {
    code: 'UVF',
    name: 'Hewanorra ',
  },
  {
    code: 'GYD',
    name: 'Heydar Aliyev ',
  },
  {
    code: 'WLS',
    name: 'Hihifo ',
  },
  {
    code: 'ITO',
    name: 'Hilo ',
  },
  {
    code: 'HHH',
    name: 'Hilton Head ',
  },
  {
    code: 'HIJ',
    name: 'Hiroshima ',
  },
  {
    code: 'HBA',
    name: 'Hobart',
  },
  {
    code: 'HET',
    name: 'Hohhot Baita ',
  },
  {
    code: 'HKK',
    name: 'Hokitika Airport',
  },
  {
    code: 'BUR',
    name: 'Hollywood Burbank',
  },
  {
    code: 'HKG',
    name: 'Hong Kong',
  },
  {
    code: 'HIR',
    name: 'Honiara ',
  },
  {
    code: 'HNL',
    name: 'Honolulu',
  },
  {
    code: 'HID',
    name: 'Horn Island ',
  },
  {
    code: 'WDH',
    name: 'Hosea Kutako ',
  },
  {
    code: 'HKN',
    name: 'Hoskins ',
  },
  {
    code: 'HTN',
    name: 'Hotan ',
  },
  {
    code: 'HJJ',
    name: 'Huaihua Zhijiang ',
  },
  {
    code: 'TXN',
    name: 'Huangshan Tunxi',
  },
  {
    code: 'HUY',
    name: 'Humberside',
  },
  {
    code: 'HSV',
    name: 'Huntsville ',
  },
  {
    code: 'HRG',
    name: 'Hurghada ',
  },
  {
    code: 'BDO',
    name: 'Husein Sastranegara',
  },
  {
    code: 'HYD',
    name: 'Hyderabad',
  },
  {
    code: 'IBZ',
    name: 'Ibiza ',
  },
  {
    code: 'IGD',
    name: 'IgdÄ±r ',
  },
  {
    code: 'IKI',
    name: 'Iki ',
  },
  {
    code: 'ILO',
    name: 'Iloilo',
  },
  {
    code: 'JUL',
    name: 'Inca Manco Capac',
  },
  {
    code: 'JUL',
    name: 'Inca Manco Capac',
  },
  {
    code: 'ICN',
    name: 'Incheon',
  },
  {
    code: 'IND',
    name: 'Indianapolis ',
  },
  {
    code: 'DEL',
    name: 'Indira Gandhi',
  },
  {
    code: 'IVC',
    name: 'Invercargill Airport',
  },
  {
    code: 'IFN',
    name: 'Isfahan',
  },
  {
    code: 'IOM',
    name: 'Isle Of Man Airport',
  },
  {
    code: 'ISE',
    name: 'Isparta Suleyman Demirel',
  },
  {
    code: 'ITH',
    name: 'Ithaca Tompkins Regional Airport',
  },
  {
    code: 'TNR',
    name: 'Ivato ',
  },
  {
    code: 'IWK',
    name: 'Iwakuni ',
  },
  {
    code: 'IWJ',
    name: 'Iwami ',
  },
  {
    code: 'ZIH',
    name: 'Ixtapa-zihuatanejo ',
  },
  {
    code: 'IZO',
    name: 'Izumo ',
  },
  {
    code: 'BPT',
    name: 'Jack Brooks Regional Airport',
  },
  {
    code: 'JAN',
    name: 'Jacksonâ??evers ',
  },
  {
    code: 'POM',
    name: 'Jacksons ',
  },
  {
    code: 'JAX',
    name: 'Jacksonville ',
  },
  {
    code: 'JAI',
    name: 'Jaipur ',
  },
  {
    code: 'GTO',
    name: 'Jalaluddin',
  },
  {
    code: 'MLW',
    name: 'James Spriggs Payne ',
  },
  {
    code: 'CJU',
    name: 'Jeju ',
  },
  {
    code: 'JMU',
    name: 'Jiamusi Dongjiao ',
  },
  {
    code: 'TNA',
    name: 'Jinan Yaoqiang ',
  },
  {
    code: 'JGS',
    name: 'Jinggangshan',
  },
  {
    code: 'JNG',
    name: 'Jining Qufu ',
  },
  {
    code: 'KHI',
    name: 'Jinnah ',
  },
  {
    code: 'JIU',
    name: 'Jiujiang Lushan ',
  },
  {
    code: 'JXA',
    name: 'Jixi Xingkaihu ',
  },
  {
    code: 'PBM',
    name: 'Johan Adolf Pengel',
  },
  {
    code: 'JFK',
    name: 'John F Kennedy ',
  },
  {
    code: 'SNA',
    name: 'John Wayne ',
  },
  {
    code: 'JOL',
    name: 'Jolo',
  },
  {
    code: 'NBO',
    name: 'Jomo Kenyatta',
  },
  {
    code: 'JLN',
    name: 'Joplin Regional Airport',
  },
  {
    code: 'LIM',
    name: 'Jorge Chavez ',
  },
  {
    code: 'GYE',
    name: 'Jose Joaquin De Olmedo ',
  },
  {
    code: 'MDE',
    name: 'Jose Maria Cordova',
  },
  {
    code: 'HAV',
    name: 'Jose Marti ',
  },
  {
    code: 'RTB',
    name: 'Juan Manuel Galvez',
  },
  {
    code: 'SJO',
    name: 'Juan Santamaria ',
  },
  {
    code: 'SUB',
    name: 'Juanda',
  },
  {
    code: 'JUB',
    name: 'Juba ',
  },
  {
    code: 'DAR',
    name: 'Julius Nyerere ',
  },
  {
    code: 'JNU',
    name: 'Juneau ',
  },
  {
    code: 'TRK',
    name: 'Juwata',
  },
  {
    code: 'KOJ',
    name: 'Kagoshima',
  },
  {
    code: 'KCM',
    name: 'Kahramanmaras',
  },
  {
    code: 'OGG',
    name: 'Kahului ',
  },
  {
    code: 'AZO',
    name: 'Kalamazoo battle Creek ',
  },
  {
    code: 'KGI',
    name: 'Kalgoorlie boulder ',
  },
  {
    code: 'BEJ',
    name: 'Kalimarau',
  },
  {
    code: 'KIX',
    name: 'Kansai',
  },
  {
    code: 'MCI',
    name: 'Kansas City ',
  },
  {
    code: 'KHH',
    name: 'Kaohsiung ',
  },
  {
    code: 'KRY',
    name: 'Karamay ',
  },
  {
    code: 'KTA',
    name: 'Karratha ',
  },
  {
    code: 'KSY',
    name: 'Kars Harakani ',
  },
  {
    code: 'KHG',
    name: 'Kashgar ',
  },
  {
    code: 'KFS',
    name: 'Kastamonu',
  },
  {
    code: 'SPI',
    name: 'Kaunas ',
  },
  {
    code: 'KVG',
    name: 'Kavieng ',
  },
  {
    code: 'KZN',
    name: 'Kazan ',
  },
  {
    code: 'LUN',
    name: 'Kenneth Kaunda ',
  },
  {
    code: 'KSH',
    name: 'Kermanshah ',
  },
  {
    code: 'KTE',
    name: 'Kerteh',
  },
  {
    code: 'KTN',
    name: 'Ketchikan ',
  },
  {
    code: 'EYW',
    name: 'Key West ',
  },
  {
    code: 'KHV',
    name: 'Khabarovsk Novy ',
  },
  {
    code: 'KRT',
    name: 'Khartoum ',
  },
  {
    code: 'KHS',
    name: 'Khasab ',
  },
  {
    code: 'KKC',
    name: 'Khon Kaen ',
  },
  {
    code: 'LBD',
    name: 'Khudjand ',
  },
  {
    code: 'KGL',
    name: 'Kigali ',
  },
  {
    code: 'JRO',
    name: 'Kilimanjaro ',
  },
  {
    code: 'GRK',
    name: 'Killeen fort Hood Regional Airport',
  },
  {
    code: 'JED',
    name: 'King Abdulaziz',
  },
  {
    code: 'DMM',
    name: 'King Fahd ',
  },
  {
    code: 'AQJ',
    name: 'King Hussein International Airport',
  },
  {
    code: 'DUR',
    name: 'King Shaka ',
  },
  {
    code: 'KIS',
    name: 'Kisumu ',
  },
  {
    code: 'KKJ',
    name: 'Kitakyushu ',
  },
  {
    code: 'UNG',
    name: 'Kiunga ',
  },
  {
    code: 'KRS',
    name: 'Kjevik',
  },
  {
    code: 'UKB',
    name: 'Kobe ',
  },
  {
    code: 'COK',
    name: 'Kochi',
  },
  {
    code: 'KCZ',
    name: 'Kochi Ryoma ',
  },
  {
    code: 'KCT',
    name: 'Koggala ',
  },
  {
    code: 'SVX',
    name: 'Koltsovo',
  },
  {
    code: 'KMQ',
    name: 'Komatsu',
  },
  {
    code: 'LBJ',
    name: 'Komodo',
  },
  {
    code: 'KOA',
    name: 'Kona ',
  },
  {
    code: 'KYA',
    name: 'Konya Havaliman',
  },
  {
    code: 'KRL',
    name: 'Korla ',
  },
  {
    code: 'BKI',
    name: 'Kota Kinabalu',
  },
  {
    code: 'KBU',
    name: 'Kotabaru',
  },
  {
    code: 'ACC',
    name: 'Kotoka ',
  },
  {
    code: 'KBV',
    name: 'Krabi',
  },
  {
    code: 'KUL',
    name: 'Kuala Lumpur ',
  },
  {
    code: 'KNO',
    name: 'Kuala Namu',
  },
  {
    code: 'KCH',
    name: 'Kuching',
  },
  {
    code: 'KUD',
    name: 'Kudat',
  },
  {
    code: 'SVP',
    name: 'Kuito ',
  },
  {
    code: 'KMJ',
    name: 'Kumamoto',
  },
  {
    code: 'KMG',
    name: 'Kunming Wujiaba',
  },
  {
    code: 'KCA',
    name: 'Kuqa Qiuci ',
  },
  {
    code: 'KUH',
    name: 'Kushiro ',
  },
  {
    code: 'KWI',
    name: 'Kuwait ',
  },
  {
    code: 'BDA',
    name: 'L f Wade ',
  },
  {
    code: 'GUA',
    name: 'La Aurora',
  },
  {
    code: 'MAR',
    name: 'La Chinita',
  },
  {
    code: 'LCG',
    name: 'La Coruna ',
  },
  {
    code: 'LSE',
    name: 'La Crosse Regional Airport',
  },
  {
    code: 'LRM',
    name: 'La Romana ',
  },
  {
    code: 'NOU',
    name: 'La Tontouta',
  },
  {
    code: 'OZC',
    name: 'Labo',
  },
  {
    code: 'LBU',
    name: 'Labuan ',
  },
  {
    code: 'LAH',
    name: 'Labuha',
  },
  {
    code: 'LAE',
    name: 'Lae Nadzab ',
  },
  {
    code: 'LFT',
    name: 'Lafayette Regional Airport',
  },
  {
    code: 'LLO',
    name: 'Lagaligo ',
  },
  {
    code: 'LGA',
    name: 'Laguardia ',
  },
  {
    code: 'CGY',
    name: 'Laguindingan',
  },
  {
    code: 'LDU',
    name: 'Lahad Datu',
  },
  {
    code: 'LCH',
    name: 'Lake Charles Regional Airport',
  },
  {
    code: 'LLI',
    name: 'Lalibela ',
  },
  {
    code: 'STL',
    name: 'Lambert St louis ',
  },
  {
    code: 'LPT',
    name: 'Lampang',
  },
  {
    code: 'LNY',
    name: 'Lanai ',
  },
  {
    code: 'GOT',
    name: 'Landvetter',
  },
  {
    code: 'LGK',
    name: 'Langkawi',
  },
  {
    code: 'ACE',
    name: 'Lanzarote ',
  },
  {
    code: 'LHW',
    name: 'Lanzhou Zhongchuan ',
  },
  {
    code: 'LAO',
    name: 'Laoag ',
  },
  {
    code: 'LRD',
    name: 'Laredo ',
  },
  {
    code: 'LCA',
    name: 'Larnaca ',
  },
  {
    code: 'SDQ',
    name: 'Las Americas ',
  },
  {
    code: 'LST',
    name: 'Launceston',
  },
  {
    code: 'LWY',
    name: 'Lawas',
  },
  {
    code: 'LAW',
    name: 'Lawton fort Sill Regional Airport',
  },
  {
    code: 'LEA',
    name: 'Learmonth ',
  },
  {
    code: 'LBA',
    name: 'Leeds Bradford ',
  },
  {
    code: 'LGP',
    name: 'Legapzi',
  },
  {
    code: 'ABE',
    name: 'Lehigh Valley ',
  },
  {
    code: 'LEJ',
    name: 'Leipzig/halle ',
  },
  {
    code: 'RTI',
    name: 'Lekunik',
  },
  {
    code: 'DKR',
    name: 'Leopold Sedar Senghor ',
  },
  {
    code: 'YYZ',
    name: 'Lester B Pearson',
  },
  {
    code: 'LXA',
    name: 'Lhasa Gonggar ',
  },
  {
    code: 'LSW',
    name: 'Lhokseumawe',
  },
  {
    code: 'LYG',
    name: 'Lianyungang Baitabu ',
  },
  {
    code: 'LIR',
    name: 'Liberia ',
  },
  {
    code: 'LLB',
    name: 'Libo',
  },
  {
    code: 'LBV',
    name: 'Libreville Leon Mba ',
  },
  {
    code: 'PVR',
    name: 'Lic Gustavo Diaz Ordaz',
  },
  {
    code: 'LNV',
    name: 'Lihir Island ',
  },
  {
    code: 'LIH',
    name: 'Lihue ',
  },
  {
    code: 'LJG',
    name: 'Lijiang Sanyi ',
  },
  {
    code: 'LLW',
    name: 'Lilongwe ',
  },
  {
    code: 'LMN',
    name: 'Limbang',
  },
  {
    code: 'MIL',
    name: 'Linate ',
  },
  {
    code: 'LNJ',
    name: 'Lincang ',
  },
  {
    code: 'LPI',
    name: 'Linkoping City',
  },
  {
    code: 'HZH',
    name: 'Liping ',
  },
  {
    code: 'LIS',
    name: 'Lisbon ',
  },
  {
    code: 'BUD',
    name: 'Liszt Ferenc ',
  },
  {
    code: 'LZH',
    name: 'Liuzhou Bailian ',
  },
  {
    code: 'LJU',
    name: 'Ljubljana Joze Pucnik ',
  },
  {
    code: 'BOS',
    name: 'Logan ',
  },
  {
    code: 'LOP',
    name: 'Lombok',
  },
  {
    code: 'LFW',
    name: 'Lome tokoin ',
  },
  {
    code: 'LCY',
    name: 'London City ',
  },
  {
    code: 'LGW',
    name: 'London Gatwick',
  },
  {
    code: 'LHR',
    name: 'London Heathrow',
  },
  {
    code: 'LKH',
    name: 'Long Akah',
  },
  {
    code: 'LBP',
    name: 'Long Banga',
  },
  {
    code: 'LGB',
    name: 'Long Beach ',
  },
  {
    code: 'ISP',
    name: 'Long Island Macarthur ',
  },
  {
    code: 'LGL',
    name: 'Long Lellang',
  },
  {
    code: 'ODN',
    name: 'Long Seridan',
  },
  {
    code: 'LRE',
    name: 'Longreach ',
  },
  {
    code: 'LDH',
    name: 'Lord Howe Island ',
  },
  {
    code: 'LAX',
    name: 'Los Angeles ',
  },
  {
    code: 'SJD',
    name: 'Los Cabos ',
  },
  {
    code: 'MSY',
    name: 'Louis Armstrong New Orleans',
  },
  {
    code: 'SDF',
    name: 'Louisville ',
  },
  {
    code: 'LPQ',
    name: 'Luang Prabang',
  },
  {
    code: 'SDD',
    name: 'Lubango',
  },
  {
    code: 'LBB',
    name: 'Lubbock Preston Smith ',
  },
  {
    code: 'FBM',
    name: 'Lubumbashi ',
  },
  {
    code: 'LUO',
    name: 'Luena ',
  },
  {
    code: 'LUG',
    name: 'Lugano',
  },
  {
    code: 'SJU',
    name: 'Luis Munoz Marin ',
  },
  {
    code: 'FNA',
    name: 'Lungi ',
  },
  {
    code: 'LYA',
    name: 'Luoyang Beijiao ',
  },
  {
    code: 'LUX',
    name: 'Luxembourg Findel ',
  },
  {
    code: 'LZO',
    name: 'Luzhou Lantian ',
  },
  {
    code: 'LWO',
    name: 'Lviv Danylo Halytskyi',
  },
  {
    code: 'LYH',
    name: 'Lynchburg Regional Airport',
  },
  {
    code: 'NAS',
    name: 'Lynden Pindling ',
  },
  {
    code: 'LYS',
    name: 'Lyon saint exupery',
  },
  {
    code: 'MFM',
    name: 'Macau',
  },
  {
    code: 'MKY',
    name: 'Mackay',
  },
  {
    code: 'CEB',
    name: 'Mactan cebu',
  },
  {
    code: 'MAG',
    name: 'Madang ',
  },
  {
    code: 'FNC',
    name: 'Madeira ',
  },
  {
    code: 'MAD',
    name: 'Madrid ',
  },
  {
    code: 'IXM',
    name: 'Madurai',
  },
  {
    code: 'SSG',
    name: 'Malabo ',
  },
  {
    code: 'MKZ',
    name: 'Malacca',
  },
  {
    code: 'AGP',
    name: 'Malaga ',
  },
  {
    code: 'MEG',
    name: 'Malanje ',
  },
  {
    code: 'MLX',
    name: 'Malatya ',
  },
  {
    code: 'MLE',
    name: 'Male',
  },
  {
    code: 'ARD',
    name: 'Mali',
  },
  {
    code: 'MYD',
    name: 'Malindi ',
  },
  {
    code: 'KAN',
    name: 'Mallam Aminu Kano International ',
  },
  {
    code: 'MXP',
    name: 'Malpensa ',
  },
  {
    code: 'MLA',
    name: 'Malta ',
  },
  {
    code: 'MMH',
    name: 'Mammoth Yosemite ',
  },
  {
    code: 'WMR',
    name: 'Mananara Nord',
  },
  {
    code: 'FRU',
    name: 'Manas ',
  },
  {
    code: 'MAN',
    name: 'Manchester ',
  },
  {
    code: 'MHT',
    name: 'Manchester boston Regional Airport',
  },
  {
    code: 'MDL',
    name: 'Mandalay',
  },
  {
    code: 'MHK',
    name: 'Manhattan Regional Airport',
  },
  {
    code: 'MPM',
    name: 'Maputo ',
  },
  {
    code: 'VCE',
    name: 'Marco Polo',
  },
  {
    code: 'MQM',
    name: 'Mardin ',
  },
  {
    code: 'SLZ',
    name: 'Marechal Cunha Machado',
  },
  {
    code: 'MBX',
    name: 'Maribor Edvard Rusjan ',
  },
  {
    code: 'RJM',
    name: 'Marinda',
  },
  {
    code: 'MGF',
    name: 'Maringa Regional Airport',
  },
  {
    code: 'UIO',
    name: 'Mariscal Sucre ',
  },
  {
    code: 'WMN',
    name: 'Maroantsetra ',
  },
  {
    code: 'RAK',
    name: 'Marrakesh Menara',
  },
  {
    code: 'RMF',
    name: 'Marsa Alam ',
  },
  {
    code: 'MRS',
    name: 'Marseille Provence ',
  },
  {
    code: 'MHH',
    name: 'Marsh Harbour',
  },
  {
    code: 'FDF',
    name: 'Martinique Aime Cesaire ',
  },
  {
    code: 'MUR',
    name: 'Marudi ',
  },
  {
    code: 'MHD',
    name: 'Mashhad ',
  },
  {
    code: 'WNI',
    name: 'Matahora',
  },
  {
    code: 'WGI',
    name: 'Matahora',
  },
  {
    code: 'MWK',
    name: 'Matak',
  },
  {
    code: 'MYJ',
    name: 'Matsuyama',
  },
  {
    code: 'HRI',
    name: 'Mattala Rajapaksa ',
  },
  {
    code: 'GND',
    name: 'Maurice Bishop ',
  },
  {
    code: 'DIW',
    name: 'Mawella Lagoon ',
  },
  {
    code: 'BZV',
    name: 'Maya maya ',
  },
  {
    code: 'MZR',
    name: 'Mazar I Sharif ',
  },
  {
    code: 'MFE',
    name: 'Mcallen Miller ',
  },
  {
    code: 'LAS',
    name: 'Mccarran ',
  },
  {
    code: 'TYS',
    name: 'Mcghee Tyson ',
  },
  {
    code: 'BFL',
    name: 'Meadows Field ',
  },
  {
    code: 'MED',
    name: 'Medina ',
  },
  {
    code: 'MXZ',
    name: 'Meixian ',
  },
  {
    code: 'MLK',
    name: 'Melalan',
  },
  {
    code: 'MNA',
    name: 'Melanguane',
  },
  {
    code: 'MEL',
    name: 'Melbourne',
  },
  {
    code: 'VIZ',
    name: 'Melbourne all Airports',
  },
  {
    code: 'MMB',
    name: 'Memanbetsu ',
  },
  {
    code: 'MEM',
    name: 'Memphis ',
  },
  {
    code: 'MDU',
    name: 'Mendi ',
  },
  {
    code: 'SPP',
    name: 'Menongue ',
  },
  {
    code: 'MIA',
    name: 'Miami ',
  },
  {
    code: 'MIG',
    name: 'Mianyang Nanjiao ',
  },
  {
    code: 'MAF',
    name: 'Midland ',
  },
  {
    code: 'MDW',
    name: 'Midway',
  },
  {
    code: 'CND',
    name: 'Mihail Kogalniceanu International Airport',
  },
  {
    code: 'YGJ',
    name: 'Miho yonago ',
  },
  {
    code: 'LIN',
    name: 'Milan Linate ',
  },
  {
    code: 'BJV',
    name: 'Milas bodrum Airport',
  },
  {
    code: 'MQL',
    name: 'Mildura ',
  },
  {
    code: 'PDG',
    name: 'Minangkabau',
  },
  {
    code: 'EZE',
    name: 'Ministro Pistarini ',
  },
  {
    code: 'MSP',
    name: 'Minneapolis saint Paul',
  },
  {
    code: 'MSQ',
    name: 'Minsk',
  },
  {
    code: 'MYY',
    name: 'Miri',
  },
  {
    code: 'MSJ',
    name: 'Misawa ',
  },
  {
    code: 'MIS',
    name: 'Misima Island ',
  },
  {
    code: 'MRA',
    name: 'Misurata ',
  },
  {
    code: 'MMY',
    name: 'Miyako ',
  },
  {
    code: 'KMI',
    name: 'Miyazaki A',
  },
  {
    code: 'MOB',
    name: 'Mobile Regional Airport',
  },
  {
    code: 'MJD',
    name: 'Moenjodaro ',
  },
  {
    code: 'CMN',
    name: 'Mohammed V ',
  },
  {
    code: 'OHE',
    name: 'Mohe Gulian ',
  },
  {
    code: 'MBA',
    name: 'Moi ',
  },
  {
    code: 'MBT',
    name: 'Moises R Espinosa',
  },
  {
    code: 'MKK',
    name: 'Molokai ',
  },
  {
    code: 'MAS',
    name: 'Momote',
  },
  {
    code: 'MCM',
    name: 'Monaco Heliport',
  },
  {
    code: 'MBE',
    name: 'Monbetsu ',
  },
  {
    code: 'LWE',
    name: 'Monopito ',
  },
  {
    code: 'MLU',
    name: 'Monroe Regional Airport louisiana',
  },
  {
    code: 'MRY',
    name: 'Monterey Regional Airport',
  },
  {
    code: 'MGM',
    name: 'Montgomery Regional Airport',
  },
  {
    code: 'YUL',
    name: 'Montreal pierre Elliott Trudeau',
  },
  {
    code: 'MKQ',
    name: 'Mopah',
  },
  {
    code: 'MOV',
    name: 'Moranbah',
  },
  {
    code: 'MRZ',
    name: 'Moree ',
  },
  {
    code: 'MLM',
    name: 'Morelia',
  },
  {
    code: 'MXH',
    name: 'Moro ',
  },
  {
    code: 'MOQ',
    name: 'Morondava ',
  },
  {
    code: 'OTI',
    name: 'Morotai',
  },
  {
    code: 'OSM',
    name: 'Mosul',
  },
  {
    code: 'HGU',
    name: 'Mount ',
  },
  {
    code: 'ISA',
    name: 'Mount Isa ',
  },
  {
    code: 'TIM',
    name: 'Mozes Kilangin',
  },
  {
    code: 'MWX',
    name: 'Muan ',
  },
  {
    code: 'MRB',
    name: 'Muara Bungo',
  },
  {
    code: 'MDG',
    name: 'Mudanjiang Hailang ',
  },
  {
    code: 'BMU',
    name: 'Muhammad Salahuddin',
  },
  {
    code: 'MKM',
    name: 'Mukah ',
  },
  {
    code: 'MUX',
    name: 'Multan ',
  },
  {
    code: 'MZV',
    name: 'Mulu ',
  },
  {
    code: 'MUC',
    name: 'Munich ',
  },
  {
    code: 'LOS',
    name: 'Murtala Muhammed ',
  },
  {
    code: 'MSR',
    name: 'Mus, Turkey',
  },
  {
    code: 'MCT',
    name: 'Muskat',
  },
  {
    code: 'PLW',
    name: 'Mutiara',
  },
  {
    code: 'MWZ',
    name: 'Mwanza ',
  },
  {
    code: 'MYR',
    name: 'Myrtle Beach ',
  },
  {
    code: 'FIH',
    name: 'N Djili ',
  },
  {
    code: 'NBX',
    name: 'Nabire',
  },
  {
    code: 'NAN',
    name: 'Nadi',
  },
  {
    code: 'WNP',
    name: 'Naga ',
  },
  {
    code: 'NGS',
    name: 'Nagasaki ',
  },
  {
    code: 'NAH',
    name: 'Naha',
  },
  {
    code: 'OKA',
    name: 'Naha',
  },
  {
    code: 'SHB',
    name: 'Nakashibetsu ',
  },
  {
    code: 'NAJ',
    name: 'Nakhchivan ',
  },
  {
    code: 'KOP',
    name: 'Nakhon Phanom',
  },
  {
    code: 'NST',
    name: 'Nakhon Si Thammarat',
  },
  {
    code: 'NLT',
    name: 'Nalati ',
  },
  {
    code: 'MSZ',
    name: 'Namibe ',
  },
  {
    code: 'APL',
    name: 'Nampula ',
  },
  {
    code: 'KHN',
    name: 'Nanchang Changbei ',
  },
  {
    code: 'NAO',
    name: 'Nanchong Gaoping',
  },
  {
    code: 'NKG',
    name: 'Nanjing',
  },
  {
    code: 'SHM',
    name: 'Nanki shirahama ',
  },
  {
    code: 'NNG',
    name: 'Nanning Wuxu',
  },
  {
    code: 'NTE',
    name: 'Nantes Atlantique',
  },
  {
    code: 'NTG',
    name: 'Nantong Xingdong ',
  },
  {
    code: 'NNY',
    name: 'Nanyang Jiangying ',
  },
  {
    code: 'NAP',
    name: 'Naples International',
  },
  {
    code: 'APF',
    name: 'Naples Municipal ',
  },
  {
    code: 'NAW',
    name: 'Narathiwat',
  },
  {
    code: 'ASF',
    name: 'Narimanovo Airport',
  },
  {
    code: 'NRT',
    name: 'Narita',
  },
  {
    code: 'BNA',
    name: 'Nashville',
  },
  {
    code: 'NDJ',
    name: 'Ndjamena ',
  },
  {
    code: 'RAI',
    name: 'Nelson Mandela ',
  },
  {
    code: 'CCU',
    name: 'Netaji Subhas Chandra Bosen',
  },
  {
    code: 'NAV',
    name: 'Nevsehir Kapadokya',
  },
  {
    code: 'CTS',
    name: 'New Chitose',
  },
  {
    code: 'ISG',
    name: 'New Ishigaki ',
  },
  {
    code: 'EWR',
    name: 'Newark Liberty ',
  },
  {
    code: 'NTL',
    name: 'Newcastle ',
  },
  {
    code: 'NCL',
    name: 'Newcastle ',
  },
  {
    code: 'ZNE',
    name: 'Newman ',
  },
  {
    code: 'PHF',
    name: 'Newport News williamsburg ',
  },
  {
    code: 'DPS',
    name: 'Ngurah Rai',
  },
  {
    code: 'CXR',
    name: 'Nha Trang',
  },
  {
    code: 'KIJ',
    name: 'Niigata ',
  },
  {
    code: 'NGB',
    name: 'Ningbo Lishe',
  },
  {
    code: 'MNL',
    name: 'Ninoy Aquino',
  },
  {
    code: 'IUE',
    name: 'Niue International Airport ',
  },
  {
    code: 'ABV',
    name: 'Nnamdi Azikiwe ',
  },
  {
    code: 'HAN',
    name: 'Noi Bai ',
  },
  {
    code: 'DEX',
    name: 'Nop Goliat',
  },
  {
    code: 'ORF',
    name: 'Norfolk ',
  },
  {
    code: 'KIN',
    name: 'Norman Manley ',
  },
  {
    code: 'ELH',
    name: 'North Eleuthera ',
  },
  {
    code: 'CVG',
    name: 'Northern Kentucky ',
  },
  {
    code: 'XNA',
    name: 'Northwest Arkansas Regional Airport',
  },
  {
    code: 'ECP',
    name: 'Northwest Florida Beaches',
  },
  {
    code: 'NWI',
    name: 'Norwich',
  },
  {
    code: 'NTQ',
    name: 'Noto Satoyama ',
  },
  {
    code: 'JBB',
    name: 'Notohadinegoro',
  },
  {
    code: 'NKC',
    name: 'Nouakchott International',
  },
  {
    code: 'OVB',
    name: 'Novosibirsk Tolmachevo ',
  },
  {
    code: 'NNX',
    name: 'Nunukan',
  },
  {
    code: 'NUE',
    name: 'Nuremberg ',
  },
  {
    code: 'JNB',
    name: 'O R Tambo ',
  },
  {
    code: 'OAK',
    name: 'Oakland ',
  },
  {
    code: 'ONJ',
    name: 'Odate noshiro ',
  },
  {
    code: 'ODS',
    name: 'Odessa International',
  },
  {
    code: 'ORD',
    name: 'Ohare',
  },
  {
    code: 'OIT',
    name: 'Oita',
  },
  {
    code: 'OKJ',
    name: 'Okayama ',
  },
  {
    code: 'OLB',
    name: 'Olbia Costa Smeralda ',
  },
  {
    code: 'SXK',
    name: 'Olilit',
  },
  {
    code: 'OLP',
    name: 'Olympic Dam ',
  },
  {
    code: 'VPE',
    name: 'Ondjiva Pereira ',
  },
  {
    code: 'ONT',
    name: 'Ontario ',
  },
  {
    code: 'ORN',
    name: 'Oran Es Senia',
  },
  {
    code: 'DSN',
    name: 'Ordos Ejin Horo ',
  },
  {
    code: 'MCO',
    name: 'Orlando ',
  },
  {
    code: 'MLB',
    name: 'Orlando Melbourne ',
  },
  {
    code: 'ITM',
    name: 'Osaka ',
  },
  {
    code: 'OSS',
    name: 'Osh ',
  },
  {
    code: 'OIM',
    name: 'Oshima ',
  },
  {
    code: 'OSL',
    name: 'Oslo',
  },
  {
    code: 'OXB',
    name: 'Osvaldo Vieira ',
  },
  {
    code: 'YOW',
    name: 'Ottawa Macdonald cartier',
  },
  {
    code: 'OUA',
    name: 'Ouagadougou ',
  },
  {
    code: 'HME',
    name: 'Oued Irara rim Belkacem ',
  },
  {
    code: 'GCM',
    name: 'Owen Roberts ',
  },
  {
    code: 'PPG',
    name: 'Pago Pago ',
  },
  {
    code: 'PBI',
    name: 'Palm Beach ',
  },
  {
    code: 'PSP',
    name: 'Palm Springs ',
  },
  {
    code: 'PMI',
    name: 'Palma De Mallorca',
  },
  {
    code: 'CLO',
    name: 'Palmaseca ',
  },
  {
    code: 'PKN',
    name: 'Pangkalan Bun',
  },
  {
    code: 'PJG',
    name: 'Panjgur',
  },
  {
    code: 'PBO',
    name: 'Paraburdoo ',
  },
  {
    code: 'ORY',
    name: 'Paris Orly',
  },
  {
    code: 'CDG',
    name: 'Paris charles De Gaulle ',
  },
  {
    code: 'AMQ',
    name: 'Pattimura',
  },
  {
    code: 'POL',
    name: 'Pemba mozambique',
  },
  {
    code: 'PEN',
    name: 'Penang',
  },
  {
    code: 'PNS',
    name: 'Pensacola ',
  },
  {
    code: 'PER',
    name: 'Perth',
  },
  {
    code: 'PHL',
    name: 'Philadelphia ',
  },
  {
    code: 'BZE',
    name: 'Philip S W Goldson ',
  },
  {
    code: 'PNH',
    name: 'Phnom Penh',
  },
  {
    code: 'PHX',
    name: 'Phoenix Sky Harbor ',
  },
  {
    code: 'HUI',
    name: 'Phu Bai',
  },
  {
    code: 'UIH',
    name: 'Phu Cat',
  },
  {
    code: 'PQC',
    name: 'Phu Quoc',
  },
  {
    code: 'HKT',
    name: 'Phuket',
  },
  {
    code: 'POS',
    name: 'Piarco ',
  },
  {
    code: 'GSO',
    name: 'Piedmont Triad ',
  },
  {
    code: 'DUM',
    name: 'Pinang Kampai ',
  },
  {
    code: 'PSA',
    name: 'Pisa ',
  },
  {
    code: 'PGV',
    name: 'Pitta greenville ',
  },
  {
    code: 'PIT',
    name: 'Pittsburgh ',
  },
  {
    code: 'TGD',
    name: 'Podgorica',
  },
  {
    code: 'UOL',
    name: 'Pogugol',
  },
  {
    code: 'KPO',
    name: 'Pohang ',
  },
  {
    code: 'PTP',
    name: 'Pointe A Pitre ',
  },
  {
    code: 'PNR',
    name: 'Pointe Noire ',
  },
  {
    code: 'FUT',
    name: 'Pointe Vele ',
  },
  {
    code: 'SLP',
    name: 'Ponciano Arriaga',
  },
  {
    code: 'TTR',
    name: 'Pongtiku ',
  },
  {
    code: 'ABJ',
    name: 'Port Bouet',
  },
  {
    code: 'CMH',
    name: 'Port Columbus ',
  },
  {
    code: 'PHC',
    name: 'Port Harcourt',
  },
  {
    code: 'PHE',
    name: 'Port Hedland ',
  },
  {
    code: 'PLO',
    name: 'Port Lincoln ',
  },
  {
    code: 'PQQ',
    name: 'Port Macquarie ',
  },
  {
    code: 'PDX',
    name: 'Portland',
  },
  {
    code: 'PWM',
    name: 'Portland ',
  },
  {
    code: 'PXO',
    name: 'Porto Santo ',
  },
  {
    code: 'BPS',
    name: 'Porto Seguro',
  },
  {
    code: 'PSJ',
    name: 'Poso Kasiguncu',
  },
  {
    code: 'PRG',
    name: 'Prague',
  },
  {
    code: 'PUQ',
    name: 'Presidente Carlos Ibanez Del Campo ',
  },
  {
    code: 'DIL',
    name: 'Presidente Nicolau Lobato',
  },
  {
    code: 'ELQ',
    name: 'Prince Nayef Bin Abdulaziz Regional Airport',
  },
  {
    code: 'HAH',
    name: 'Prince Said Ibrahim ',
  },
  {
    code: 'SXM',
    name: 'Princess Juliana ',
  },
  {
    code: 'PRN',
    name: 'Pristina',
  },
  {
    code: 'PLS',
    name: 'Providenciales ',
  },
  {
    code: 'PBC',
    name: 'Puebla ',
  },
  {
    code: 'SYM',
    name: 'Puer Simao ',
  },
  {
    code: 'PPS',
    name: 'Puerto Princesa',
  },
  {
    code: 'LED',
    name: 'Pulkovo',
  },
  {
    code: 'PUJ',
    name: 'Punta Cana ',
  },
  {
    code: 'PSU',
    name: 'Putussibau',
  },
  {
    code: 'JIQ',
    name: 'Qianjiang Wulingshan ',
  },
  {
    code: 'TAO',
    name: 'Qingdao',
  },
  {
    code: 'NDG',
    name: 'Qiqihar Sanjianzi',
  },
  {
    code: 'MLI',
    name: 'Quad City ',
  },
  {
    code: 'JJN',
    name: 'Quanzhou Jinjiang',
  },
  {
    code: 'LAD',
    name: 'Quatro De Fevereiro ',
  },
  {
    code: 'YQB',
    name: 'Quebec City Jean Lesage',
  },
  {
    code: 'AMM',
    name: 'Queen Alia ',
  },
  {
    code: 'AUA',
    name: 'Queen Beatrix ',
  },
  {
    code: 'ZQN',
    name: 'Queenstown',
  },
  {
    code: 'QRO',
    name: 'Queretaro ',
  },
  {
    code: 'UET',
    name: 'Quetta ',
  },
  {
    code: 'DCN',
    name: 'Raaf Base Curtin',
  },
  {
    code: 'RAB',
    name: 'Rabaul ',
  },
  {
    code: 'TKG',
    name: 'Radin Inten Ii',
  },
  {
    code: 'KTG',
    name: 'Rahadi Oesman',
  },
  {
    code: 'TNJ',
    name: 'Raja Haji Fisabilillah',
  },
  {
    code: 'RDU',
    name: 'Raleigh durham ',
  },
  {
    code: 'SAP',
    name: 'Ramon Villeda Morales ',
  },
  {
    code: 'NTX',
    name: 'Ranai',
  },
  {
    code: 'RAP',
    name: 'Rapid City Regional Airport',
  },
  {
    code: 'RAR',
    name: 'Rarotonga',
  },
  {
    code: 'RVT',
    name: 'Ravensthorpe',
  },
  {
    code: 'REC',
    name: 'Recife guararapes gilberto Freyre ',
  },
  {
    code: 'YQR',
    name: 'Regina',
  },
  {
    code: 'YKF',
    name: 'Region Of Waterloo ',
  },
  {
    code: 'MKW',
    name: 'Rendani',
  },
  {
    code: 'RNO',
    name: 'Reno tahoe ',
  },
  {
    code: 'RIC',
    name: 'Richmond ',
  },
  {
    code: 'AMA',
    name: 'Rick Husband Amarillo ',
  },
  {
    code: 'RIX',
    name: 'Riga',
  },
  {
    code: 'ROA',
    name: 'Roanoke blacksburg Regional Airport',
  },
  {
    code: 'MLN',
    name: 'Robert Atty Bessing',
  },
  {
    code: 'ROB',
    name: 'Roberts ',
  },
  {
    code: 'RDM',
    name: 'Roberts Field',
  },
  {
    code: 'RST',
    name: 'Rochester ',
  },
  {
    code: 'AQP',
    name: 'Rodriguez Ballon ',
  },
  {
    code: 'MFR',
    name: 'Rogue Valleyl medford ',
  },
  {
    code: 'RUN',
    name: 'Roland Garros',
  },
  {
    code: 'RMA',
    name: 'Roma ',
  },
  {
    code: 'ROR',
    name: 'Roman Tmetuchi',
  },
  {
    code: 'DCA',
    name: 'Ronald Reagan Washington ',
  },
  {
    code: 'ROV',
    name: 'Rostov-on-don',
  },
  {
    code: 'ROW',
    name: 'Roswell International Air Center',
  },
  {
    code: 'RTM',
    name: 'Rotterdam-den Haag',
  },
  {
    code: 'RXS',
    name: 'Roxas',
  },
  {
    code: 'SBG',
    name: 'Sabang',
  },
  {
    code: 'SAW',
    name: 'Sabiha GokÃ§en International',
  },
  {
    code: 'HIN',
    name: 'Sacheon ',
  },
  {
    code: 'SMF',
    name: 'Sacramento ',
  },
  {
    code: 'HSG',
    name: 'Saga ',
  },
  {
    code: 'SMS',
    name: 'Sainte Marie ',
  },
  {
    code: 'SPN',
    name: 'Saipan',
  },
  {
    code: 'SLL',
    name: 'Salalah  ',
  },
  {
    code: 'POA',
    name: 'Salgado Filho ',
  },
  {
    code: 'SLC',
    name: 'Salt Lake City ',
  },
  {
    code: 'SZG',
    name: 'Salzburg',
  },
  {
    code: 'MDC',
    name: 'Sam Ratulangi',
  },
  {
    code: 'AZS',
    name: 'Saman¡ El Catey International Airport',
  },
  {
    code: 'SVB',
    name: 'Sambava ',
  },
  {
    code: 'USM',
    name: 'Samui',
  },
  {
    code: 'SJT',
    name: 'San Angelo Regional Airport',
  },
  {
    code: 'SAT',
    name: 'San Antonio',
  },
  {
    code: 'SCY',
    name: 'San Cristobal',
  },
  {
    code: 'SAN',
    name: 'San Diego ',
  },
  {
    code: 'SFO',
    name: 'San Fransisco ',
  },
  {
    code: 'SJC',
    name: 'San Jose ',
  },
  {
    code: 'SBP',
    name: 'San Luis Obispo County Regional Airport',
  },
  {
    code: 'SAH',
    name: 'Sanaa',
  },
  {
    code: 'SDK',
    name: 'Sandakan',
  },
  {
    code: 'TRF',
    name: 'Sandefjord',
  },
  {
    code: 'PUM',
    name: 'Sangia Nibandera',
  },
  {
    code: 'MBJ',
    name: 'Sangster ',
  },
  {
    code: 'GNY',
    name: 'Sanlurfa Gap',
  },
  {
    code: 'SBA',
    name: 'Santa Barbara Municipal',
  },
  {
    code: 'SAF',
    name: 'Santa Fe Municipal',
  },
  {
    code: 'STM',
    name: 'Santarem Maestro Wilson Fonseca',
  },
  {
    code: 'SCQ',
    name: 'Santiago De Compostela',
  },
  {
    code: 'SYX',
    name: 'Sanya Phoenix',
  },
  {
    code: 'TMS',
    name: 'Sao Tome ',
  },
  {
    code: 'SJJ',
    name: 'Sarajevo ',
  },
  {
    code: 'SRQ',
    name: 'Sarasota Bradenton ',
  },
  {
    code: 'AMD',
    name: 'Sardar Vallabhbhai Patel',
  },
  {
    code: 'VHC',
    name: 'Saurimo ',
  },
  {
    code: 'SAV',
    name: 'Savannah Hilton Head',
  },
  {
    code: 'MQT',
    name: 'Sawyer',
  },
  {
    code: 'AMS',
    name: 'Schiphol',
  },
  {
    code: 'SEA',
    name: 'Seattle Tacoma',
  },
  {
    code: 'SZE',
    name: 'Semera ',
  },
  {
    code: 'JHB',
    name: 'Senai',
  },
  {
    code: 'SDJ',
    name: 'Sendai ',
  },
  {
    code: 'DJJ',
    name: 'Sentani',
  },
  {
    code: 'BPN',
    name: 'Sepinggan',
  },
  {
    code: 'SVQ',
    name: 'Seville ',
  },
  {
    code: 'SEZ',
    name: 'Seychelles ',
  },
  {
    code: 'CGP',
    name: 'Shah Amanat',
  },
  {
    code: 'RYK',
    name: 'Shaikh Zayed ',
  },
  {
    code: 'SHA',
    name: 'Shanghai Hongqiao ',
  },
  {
    code: 'PVG',
    name: 'Shanghai Pudong',
  },
  {
    code: 'SWA',
    name: 'Shantou Jieyang',
  },
  {
    code: 'SHJ',
    name: 'Sharjah ',
  },
  {
    code: 'SSH',
    name: 'Sharm El Sheikh',
  },
  {
    code: 'SHE',
    name: 'Shenyang',
  },
  {
    code: 'SZX',
    name: 'Shenzhen',
  },
  {
    code: 'SVO',
    name: 'Sheremetyevo',
  },
  {
    code: 'SJW',
    name: 'Shijiazhuang Zhengding ',
  },
  {
    code: 'SYZ',
    name: 'Shiraz ',
  },
  {
    code: 'SHC',
    name: 'Shire',
  },
  {
    code: 'FSZ',
    name: 'Shizuoka ',
  },
  {
    code: 'SYO',
    name: 'Shonai ',
  },
  {
    code: 'SHV',
    name: 'Shreveport Regional Airport',
  },
  {
    code: 'SKT',
    name: 'Sialkot ',
  },
  {
    code: 'FLZ',
    name: 'Sibolga',
  },
  {
    code: 'SBW',
    name: 'Sibu',
  },
  {
    code: 'DGT',
    name: 'Sibulan ',
  },
  {
    code: 'REP',
    name: 'Siem Reap',
  },
  {
    code: 'GIU',
    name: 'Sigiriya ',
  },
  {
    code: 'LLG',
    name: 'Silampari',
  },
  {
    code: 'DTB',
    name: 'Silangit',
  },
  {
    code: 'ASU',
    name: 'Silvio Pettirossi ',
  },
  {
    code: 'SMG',
    name: 'Simeuleu',
  },
  {
    code: 'CCS',
    name: 'Simon Bolivar ',
  },
  {
    code: 'NLA',
    name: 'Simon Mwansa Kapwepwe',
  },
  {
    code: 'NOP',
    name: 'Sinop',
  },
  {
    code: 'FSD',
    name: 'Sioux Falls Regional Airport',
  },
  {
    code: 'SUX',
    name: 'Sioux Gateway ',
  },
  {
    code: 'MRU',
    name: 'Sir Seewoosagur Ramgoolam ',
  },
  {
    code: 'GBE',
    name: 'Sir Seretse Khama  ',
  },
  {
    code: 'SIT',
    name: 'Sitka Rocky Gutierrez',
  },
  {
    code: 'VAS',
    name: 'Sivas',
  },
  {
    code: 'KDU',
    name: 'Skardu A',
  },
  {
    code: 'SKP',
    name: 'Skopje ',
  },
  {
    code: 'BJW',
    name: 'Soa',
  },
  {
    code: 'AER',
    name: 'Sochi International Airport',
  },
  {
    code: 'CGK',
    name: 'Soekarno Hatta',
  },
  {
    code: 'SOF',
    name: 'Sofia ',
  },
  {
    code: 'SVG',
    name: 'Sola ',
  },
  {
    code: 'SBN',
    name: 'South Bend International',
  },
  {
    code: 'RSW',
    name: 'Southwest Florida ',
  },
  {
    code: 'ABY',
    name: 'Southwest Georgia',
  },
  {
    code: 'SZA',
    name: 'Soyo ',
  },
  {
    code: 'SPU',
    name: 'Split ',
  },
  {
    code: 'GEG',
    name: 'Spokane',
  },
  {
    code: 'SGF',
    name: 'Springfield branson',
  },
  {
    code: 'ATQ',
    name: 'Sri Guru Ram Das Jee ',
  },
  {
    code: 'YYT',
    name: 'St Johns',
  },
  {
    code: 'STW',
    name: 'Stavropol Shpakovskoye',
  },
  {
    code: 'SWF',
    name: 'Stewart ',
  },
  {
    code: 'ARN',
    name: 'Stockholm Arlanda',
  },
  {
    code: 'BRA',
    name: 'Stockholm Bromma',
  },
  {
    code: 'BMA',
    name: 'Stockholm Bromma Airport',
  },
  {
    code: 'SXB',
    name: 'Strasbourg ',
  },
  {
    code: 'STR',
    name: 'Stuttgart ',
  },
  {
    code: 'RAQ',
    name: 'Sugimanuru',
  },
  {
    code: 'SKZ',
    name: 'Sukkur ',
  },
  {
    code: 'ISU',
    name: 'Sulaimaniyah ',
  },
  {
    code: 'SZB',
    name: 'Sultan Abdul Aziz Shah',
  },
  {
    code: 'AOR',
    name: 'Sultan Abdul Halim',
  },
  {
    code: 'IPH',
    name: 'Sultan Azlan Shah',
  },
  {
    code: 'TTE',
    name: 'Sultan Babullah',
  },
  {
    code: 'TLI',
    name: 'Sultan Bantilan',
  },
  {
    code: 'KUA',
    name: 'Sultan Haji Ahmad Shah',
  },
  {
    code: 'UPG',
    name: 'Sultan Hasanuddin',
  },
  {
    code: 'BTJ',
    name: 'Sultan Iskandar Muda',
  },
  {
    code: 'KBR',
    name: 'Sultan Ismail Petra',
  },
  {
    code: 'TGG',
    name: 'Sultan Mahmud',
  },
  {
    code: 'PLM',
    name: 'Sultan Mahmud Badaruddin Ii',
  },
  {
    code: 'PKU',
    name: 'Sultan Syarif Kasim Ii',
  },
  {
    code: 'DJB',
    name: 'Sultan Thaha Syaifuddin',
  },
  {
    code: 'WUX',
    name: 'Sunan Shuofang ',
  },
  {
    code: 'MCY',
    name: 'Sunshine Coast',
  },
  {
    code: 'PNK',
    name: 'Supadio',
  },
  {
    code: 'URT',
    name: 'Surat Thani',
  },
  {
    code: 'SUG',
    name: 'Surigao',
  },
  {
    code: 'SQG',
    name: 'Susilo',
  },
  {
    code: 'BKK',
    name: 'Suvarnabhumi',
  },
  {
    code: 'BDJ',
    name: 'Syamsuddin Noor',
  },
  {
    code: 'SYD',
    name: 'Sydney ',
  },
  {
    code: 'SYR',
    name: 'Syracuse Hancock ',
  },
  {
    code: 'LUW',
    name: 'Syukuran Aminuddin Amir',
  },
  {
    code: 'PVD',
    name: 'T F Green ',
  },
  {
    code: 'TBZ',
    name: 'Tabriz ',
  },
  {
    code: 'TBG',
    name: 'Tabubil ',
  },
  {
    code: 'TCG',
    name: 'Tacheng ',
  },
  {
    code: 'TAG',
    name: 'Tagbilaran',
  },
  {
    code: 'TIF',
    name: 'Taif Regional',
  },
  {
    code: 'TNN',
    name: 'Tainan',
  },
  {
    code: 'TSA',
    name: 'Taipei Songshan',
  },
  {
    code: 'TPE',
    name: 'Taiwan Taoyuan',
  },
  {
    code: 'TYN',
    name: 'Taiyuan Wuxu',
  },
  {
    code: 'TAK',
    name: 'Takamatsu ',
  },
  {
    code: 'TXE',
    name: 'Takengon Rembele',
  },
  {
    code: 'TLH',
    name: 'Tallahassee ',
  },
  {
    code: 'TLL',
    name: 'Tallinn ',
  },
  {
    code: 'TMC',
    name: 'Tambolaka',
  },
  {
    code: 'TPA',
    name: 'Tampa ',
  },
  {
    code: 'MJU',
    name: 'Tampa Padang ',
  },
  {
    code: 'TMP',
    name: 'Tampere Pirkkala',
  },
  {
    code: 'TMW',
    name: 'Tamworth ',
  },
  {
    code: 'SGN',
    name: 'Tan Son Nhat',
  },
  {
    code: 'CNF',
    name: 'Tancredo Neves ',
  },
  {
    code: 'VPM',
    name: 'Tanjung Api',
  },
  {
    code: 'TJS',
    name: 'Tanjung Harapan',
  },
  {
    code: 'TGC',
    name: 'Tanjung Manis',
  },
  {
    code: 'TIZ',
    name: 'Tari ',
  },
  {
    code: 'TAS',
    name: 'Tashkent ',
  },
  {
    code: 'TWU',
    name: 'Tawau',
  },
  {
    code: 'TBS',
    name: 'Tbilisi ',
  },
  {
    code: 'ANC',
    name: 'Ted Stevens Anchorage ',
  },
  {
    code: 'IKA',
    name: 'Tehran Imam Khomeini ',
  },
  {
    code: 'TEQ',
    name: 'Tekirdag Corlu',
  },
  {
    code: 'SRI',
    name: 'Temindung',
  },
  {
    code: 'TFN',
    name: 'Tenerife North ',
  },
  {
    code: 'TFS',
    name: 'Tenerife South ',
  },
  {
    code: 'EIS',
    name: 'Terrance B Lettsome ',
  },
  {
    code: 'TXK',
    name: 'Texarkana Regional Airport',
  },
  {
    code: 'CID',
    name: 'The Eastern Iowa',
  },
  {
    code: 'SKG',
    name: 'Thessaloniki ',
  },
  {
    code: 'TRV',
    name: 'Thivandrum',
  },
  {
    code: 'THD',
    name: 'Tho Xuan',
  },
  {
    code: 'TSN',
    name: 'Tianjin',
  },
  {
    code: 'TIA',
    name: 'Tirana International Nene Tereza ',
  },
  {
    code: 'TRZ',
    name: 'Tiruchilapalli',
  },
  {
    code: 'PKY',
    name: 'Tjilik Riwut',
  },
  {
    code: 'TMM',
    name: 'Toamasina ',
  },
  {
    code: 'KAZ',
    name: 'Tobelo',
  },
  {
    code: 'PTY',
    name: 'Tocumen ',
  },
  {
    code: 'OBO',
    name: 'Tokachi Obihiro ',
  },
  {
    code: 'TKS',
    name: 'Tokushima',
  },
  {
    code: 'FTU',
    name: 'Tolanaro ',
  },
  {
    code: 'TOL',
    name: 'Toledo Express',
  },
  {
    code: 'TLE',
    name: 'Toliara ',
  },
  {
    code: 'TGU',
    name: 'Toncontin ',
  },
  {
    code: 'TGO',
    name: 'Tongliao ',
  },
  {
    code: 'TEN',
    name: 'Tongren Fenghuang ',
  },
  {
    code: 'TTJ',
    name: 'Tottori ',
  },
  {
    code: 'TLS',
    name: 'Toulouse Blagnac ',
  },
  {
    code: 'PAP',
    name: 'Toussaint L Ouverture ',
  },
  {
    code: 'TSV',
    name: 'Townsville',
  },
  {
    code: 'TOY',
    name: 'Toyama ',
  },
  {
    code: 'TZX',
    name: 'Trabzon',
  },
  {
    code: 'TST',
    name: 'Trang',
  },
  {
    code: 'TRI',
    name: 'Tri cities',
  },
  {
    code: 'HTS',
    name: 'Tri state ',
  },
  {
    code: 'KTM',
    name: 'Tribhuvan',
  },
  {
    code: 'TIP',
    name: 'Tripoli ',
  },
  {
    code: 'TRD',
    name: 'Trondheim Airport Vaernes',
  },
  {
    code: 'SUP',
    name: 'Trunojoyo',
  },
  {
    code: 'TSJ',
    name: 'Tsushima ',
  },
  {
    code: 'TUS',
    name: 'Tucson',
  },
  {
    code: 'TUG',
    name: 'Tuguegarao ',
  },
  {
    code: 'TUL',
    name: 'Tulsa ',
  },
  {
    code: 'TUN',
    name: 'Tunis carthage',
  },
  {
    code: 'TUK',
    name: 'Turbat ',
  },
  {
    code: 'BJW',
    name: 'Turelelo Soa',
  },
  {
    code: 'TRN',
    name: 'Turin',
  },
  {
    code: 'TKU',
    name: 'Turku',
  },
  {
    code: 'HVN',
    name: 'Tweed New Haven ',
  },
  {
    code: 'TYR',
    name: 'Tyler Pounds Regional',
  },
  {
    code: 'UBP',
    name: 'Ubon Ratchathani',
  },
  {
    code: 'UTH',
    name: 'Udonthani',
  },
  {
    code: 'UFA',
    name: 'Ufa International',
  },
  {
    code: 'USN',
    name: 'Ulsan ',
  },
  {
    code: 'AYQ',
    name: 'Uluru',
  },
  {
    code: 'CMI',
    name: 'University Of Illinois Williard',
  },
  {
    code: 'SCE',
    name: 'University Park ',
  },
  {
    code: 'URC',
    name: 'Urumqi Diwopu',
  },
  {
    code: 'USH',
    name: 'Ushuaia Malvinas',
  },
  {
    code: 'KNG',
    name: 'Utarom',
  },
  {
    code: 'ANU',
    name: 'V C Bird ',
  },
  {
    code: 'VAA',
    name: 'Vaasa',
  },
  {
    code: 'BEL',
    name: 'Val De Cans ',
  },
  {
    code: 'VLD',
    name: 'Valdosta Regional',
  },
  {
    code: 'VLC',
    name: 'Valencia ',
  },
  {
    code: 'VLL',
    name: 'Valladolid ',
  },
  {
    code: 'BRO',
    name: 'Valley ',
  },
  {
    code: 'VAN',
    name: 'Van Ferit Melen',
  },
  {
    code: 'YVR',
    name: 'Vancouver ',
  },
  {
    code: 'VAI',
    name: 'Vanimo ',
  },
  {
    code: 'VIE',
    name: 'Vienna',
  },
  {
    code: 'VGO',
    name: 'Vigo peinador ',
  },
  {
    code: 'VNO',
    name: 'Vilnius',
  },
  {
    code: 'CTA',
    name: 'Vincenzo Bellini',
  },
  {
    code: 'VII',
    name: 'Vinh',
  },
  {
    code: 'VIJ',
    name: 'Virgin Gorda ',
  },
  {
    code: 'VVI',
    name: 'Viru Viru ',
  },
  {
    code: 'VTZ',
    name: 'Visakhapatnam',
  },
  {
    code: 'VVO',
    name: 'Vladivostok ',
  },
  {
    code: 'VKO',
    name: 'Vnukovo',
  },
  {
    code: 'ACT',
    name: 'Waco Regional',
  },
  {
    code: 'WGA',
    name: 'Wagga Wagga ',
  },
  {
    code: 'MOF',
    name: 'Wai Oti',
  },
  {
    code: 'WGP',
    name: 'Waingapu',
  },
  {
    code: 'WKJ',
    name: 'Wakkanai ',
  },
  {
    code: 'WMX',
    name: 'Wamena',
  },
  {
    code: 'WXN',
    name: 'Wanzhou Wuqiao ',
  },
  {
    code: 'WBM',
    name: 'Wapenamanda ',
  },
  {
    code: 'WAW',
    name: 'Warsawa Frederic Chopin',
  },
  {
    code: 'TJG',
    name: 'Warukin',
  },
  {
    code: 'IAD',
    name: 'Washington Dulles',
  },
  {
    code: 'ALO',
    name: 'Waterloo ',
  },
  {
    code: 'ART',
    name: 'Watertown ',
  },
  {
    code: 'VTE',
    name: 'Wattay',
  },
  {
    code: 'WEI',
    name: 'Weipa ',
  },
  {
    code: 'WLG',
    name: 'Wellington',
  },
  {
    code: 'WNH',
    name: 'Wenshan Puzhehei',
  },
  {
    code: 'WNZ',
    name: 'Wenzhou Longwan',
  },
  {
    code: 'HPN',
    name: 'Westchester County',
  },
  {
    code: 'WWK',
    name: 'Wewak ',
  },
  {
    code: 'PPP',
    name: 'Whitsunday Coast Proserpine',
  },
  {
    code: 'ICT',
    name: 'Wichita Dwight D Eisenhower ',
  },
  {
    code: 'SPS',
    name: 'Wichita Falls Municipal',
  },
  {
    code: 'SBY',
    name: 'Wicomico Regional Airport',
  },
  {
    code: 'AVP',
    name: 'Wilkes barre',
  },
  {
    code: 'OKC',
    name: 'Will Rogers World ',
  },
  {
    code: 'HOU',
    name: 'William P. Hobby ',
  },
  {
    code: 'IPT',
    name: 'Williamsport ',
  },
  {
    code: 'ILM',
    name: 'Wilmington ',
  },
  {
    code: 'JIJ',
    name: 'Wilwal ',
  },
  {
    code: 'YWG',
    name: 'Winnipeg James Armstrong Richardson',
  },
  {
    code: 'CCC',
    name: 'Winterland Airport',
  },
  {
    code: 'TSY',
    name: 'Wiriadinata',
  },
  {
    code: 'WJU',
    name: 'Wonju ',
  },
  {
    code: 'BHE',
    name: 'Woodbourne Airport ',
  },
  {
    code: 'WUH',
    name: 'Wuhan Tianhe',
  },
  {
    code: 'WUS',
    name: 'Wuyishan ',
  },
  {
    code: 'XIY',
    name: 'Xi An Xianyang',
  },
  {
    code: 'XMN',
    name: 'Xiamen Gaoqi ',
  },
  {
    code: 'XFN',
    name: 'Xiangyang Liuji',
  },
  {
    code: 'XIL',
    name: 'Xilinhot ',
  },
  {
    code: 'ACX',
    name: 'Xingyi Wanfenglin ',
  },
  {
    code: 'XNN',
    name: 'Xining Caojiabao',
  },
  {
    code: 'JHG',
    name: 'Xishuangbanna Gasa ',
  },
  {
    code: 'JHG',
    name: 'Xishuangbanna Gasa ',
  },
  {
    code: 'XUZ',
    name: 'Xuzhou Guanyin ',
  },
  {
    code: 'GAJ',
    name: 'Yamagata ',
  },
  {
    code: 'UBJ',
    name: 'Yamaguchi Ube ',
  },
  {
    code: 'ENY',
    name: 'Yanan Ershilipu ',
  },
  {
    code: 'YNB',
    name: 'Yanbu',
  },
  {
    code: 'YNZ',
    name: 'Yancheng Nanyang ',
  },
  {
    code: 'RGN',
    name: 'Yangoon',
  },
  {
    code: 'YTY',
    name: 'Yangzhou Taizhou ',
  },
  {
    code: 'YNJ',
    name: 'Yanji Chaoyangchuan ',
  },
  {
    code: 'YNT',
    name: 'Yantai Penglai ',
  },
  {
    code: 'NSI',
    name: 'Yaounde Nsimalen ',
  },
  {
    code: 'CRW',
    name: 'Yeager ',
  },
  {
    code: 'YZF',
    name: 'Yellowknife ',
  },
  {
    code: 'RSU',
    name: 'Yeosu ',
  },
  {
    code: 'YIH',
    name: 'Yichang Sanxia',
  },
  {
    code: 'LDS',
    name: 'Yichun Lindu ',
  },
  {
    code: 'INC',
    name: 'Yinchuan Hedong',
  },
  {
    code: 'YIN',
    name: 'Yining ',
  },
  {
    code: 'YIW',
    name: 'Yiwu ',
  },
  {
    code: 'UYN',
    name: 'Yulin Yuyang ',
  },
  {
    code: 'YUM',
    name: 'Yuma ',
  },
  {
    code: 'YCU',
    name: 'Yuncheng Guangong',
  },
  {
    code: 'UUS',
    name: 'Yuzhno sakhalinsk ',
  },
  {
    code: 'KZR',
    name: 'Zafer ',
  },
  {
    code: 'ZAG',
    name: 'Zagreb ',
  },
  {
    code: 'ZAM',
    name: 'Zamboanga',
  },
  {
    code: 'ZAZ',
    name: 'Zaragoza ',
  },
  {
    code: 'DYG',
    name: 'Zhangjiajie ',
  },
  {
    code: 'ZHA',
    name: 'Zhanjiang ',
  },
  {
    code: 'ZAT',
    name: 'Zhaotong ',
  },
  {
    code: 'CGO',
    name: 'Zhengzhou Xinzheng ',
  },
  {
    code: 'PZH',
    name: 'Zhob ',
  },
  {
    code: 'HSN',
    name: 'Zhoushan Putuoshan ',
  },
  {
    code: 'ZUH',
    name: 'Zhuhai Jinwan ',
  },
  {
    code: 'ZYI',
    name: 'Zunyi Xinzhou ',
  },
  {
    code: 'ZRH',
    name: 'Zurich',
  },
];
