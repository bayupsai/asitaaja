import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import styles from '../styles';
import { thameColors } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceWidth = window.width;

const ModalSeatClass = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visible}
      style={styles.bottomModal}
    >
      <View style={styles.modalSeatClass}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: thameColors.darkGray,
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={() => this.setState({ modalVisible: true })}
          >
            <Col size={2} style={{ alignItems: 'flex-start', paddingLeft: 10 }}>
              <Icon
                name="close"
                type="evilIcon"
                color={thameColors.semiGreen}
                size={20}
              />
            </Col>
            <Col
              size={8}
              style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
            >
              <Text
                style={{
                  color: thameColors.superBack,
                  fontWeight: '500',
                  fontSize: 16,
                }}
              >
                Seat Class
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid>
          <Col style={{ backgroundColor: thameColors.white }}>
            <ScrollView>
              {this.state.seatClassType.map((data, index) => {
                return (
                  <Grid
                    key={index}
                    style={{
                      flex: 1,
                      backgroundColor: thameColors.white,
                      padding: 10,
                      borderBottomColor: thameColors.buttonGray,
                      borderBottomWidth: 0.5,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.setSeatClass(data)}
                      style={{ flex: 1 }}
                    >
                      <Row>
                        <Col style={{ alignItems: 'flex-start' }}>
                          <Text
                            style={{
                              fontWeight: '400',
                              color: thameColors.superBack,
                              fontSize: 16,
                            }}
                          >
                            {data.title}
                          </Text>
                        </Col>
                        <Col
                          style={{
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}
                        >
                          {this.state.seatClassSelected == data.index ? (
                            <Icon
                              name="check"
                              type="feather"
                              color={thameColors.semiGreen}
                              size={22}
                            />
                          ) : (
                            <Text></Text>
                          )}
                        </Col>
                      </Row>
                    </TouchableOpacity>
                  </Grid>
                );
              })}
            </ScrollView>
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default ModalSeatClass;
