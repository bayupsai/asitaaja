import React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { fontBold, fontReguler, thameColors } from '../../../../base/constant';

//Component
import HeaderFlight from '../../../../components/HeaderFlight';
import SubHeaderPage from '../../../../components/SubHeaderPage';
import Header from './ETicket/Header';
import DepartureFlight from './ETicket/DepartureFlight';
import ReturnFlight from './ETicket/RetrunFlight';
import Passenger from './ETicket/Passanger';
import { connect } from 'react-redux';

class ETicket extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  goBack = () => {
    this.props.navigation.navigate('Tab');
  };

  render() {
    return (
      <View style={{ backgroundColor: thameColors.backWhite, flex: 1 }}>
        <HeaderFlight
          origin={this.props.flight.origin}
          destination={this.props.flight.destination}
          callback={this.goBack}
          {...this.props}
        />
        <SubHeaderPage title={this.props.dataBookingFlight.partner_trxid} />
        <ScrollView
          showsVerticalScrollIndicator={false}
          horizontal={false}
          style={styles.content}
        >
          <View>
            <Header {...this.props.contactDetail} />
            <DepartureFlight {...this.props.flightSelected} />
            {this.props.flight.returnDate ? <ReturnFlight /> : null}

            <Passenger {...this.props.passengerData} />
          </View>
        </ScrollView>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    flight: state.flight,
    dataBookingFlight: state.flight.dataBookingFlight,
    contactDetail: state.flight.contactDetail,
    flightSelected: state.flight.flightSelected,
    flightReturnSelected: state.flight.flightReturnSelected,
    passengerData: state.flight,
  };
}
export default connect(mapStateToProps)(ETicket);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    marginTop: 0,
  },
  content: {
    backgroundColor: thameColors.white,
    marginTop: -40,
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 10,
  },
  sectionHr: {
    width: 90 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 14,
  },
  textLabel: {
    color: thameColors.textBlack,
    fontFamily: fontReguler,
    fontSize: 14,
  },
  textInfo: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 16,
  },
  textGreen: { color: thameColors.green, fontFamily: fontBold, fontSize: 16 },
});
