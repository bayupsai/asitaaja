import React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import MultiSwitch from 'rn-slider-switch';
import { Grid, Col } from 'react-native-easy-grid';

//Component
import HeaderPage from '../../../../components/HeaderPage';
import SubHeaderPage from '../../../../components/SubHeaderPage';
import styles from '../../SearchFlight/styles';
import { InputTextRevamp } from '../../../../elements/TextInput';
import {
  fontReguler,
  fontExtraBold,
  fontBold,
  thameColors,
} from '../../../../base/constant';

let deviceHeight = Dimensions.get('window').height;

class Flight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={{ backgroundColor: thameColors.backWhite, flex: 1 }}>
        <ScrollView style={{ marginBottom: 60 }}>
          <HeaderPage
            title="Booking Your Flight"
            callback={() => this.goBackHeader()}
            {...this.props}
          />
          <SubHeaderPage />

          <Grid style={styles.sectionToggle}>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              <MultiSwitch
                currentStatus={'Open'}
                disableScroll={value => {}}
                onStatusChanged={text => {
                  if (text == 'Single') {
                    this.setState({ flightType: 'single' });
                  } else if (text == 'Return') {
                    this.setState({ flightType: 'return' });
                  }
                }}
              />
            </Col>
          </Grid>

          <Grid style={styleInternal.card}>
            <Col
              size={4}
              style={{ justifyContent: 'center', alignItems: 'flex-start' }}
            >
              <View style={{ paddingBottom: 10 }}>
                <Text style={styleInternal.textLabel}>From</Text>
              </View>
              <View>
                <Text style={styleInternal.textBold}>Jakarta</Text>
              </View>
              <View>
                <Text style={styleInternal.textInitial}>(CGK)</Text>
              </View>
            </Col>
            <Col
              size={2}
              style={{ justifyContent: 'center', alignItems: 'center' }}
            >
              <View></View>
              <View></View>
              <View>
                <Image
                  source={require('../../../../assets/icons/flight_path.png')}
                  style={{ width: 25, height: 30, marginBottom: -10 }}
                  resizeMode="center"
                />
              </View>
              <View>
                <Image
                  source={require('../../../../assets/icons/flight_return.png')}
                  style={{ width: 45, height: 40, marginTop: -25 }}
                  resizeMode="contain"
                />
              </View>
            </Col>
            <Col
              size={4}
              style={{ justifyContent: 'center', alignItems: 'flex-end' }}
            >
              <View style={{ paddingBottom: 10 }}>
                <Text style={styleInternal.textLabel}>To</Text>
              </View>
              <View>
                <Text style={styleInternal.textInitial}>(DPS)</Text>
              </View>
            </Col>
          </Grid>
          <InputTextRevamp
            onPress={() => alert('aaa')}
            source={require('../../../assets/icons/icon_departure.png')}
            label="Departure Data"
            editable="Wed, 21 Jan 2019"
          />
          <InputTextRevamp
            source={require('../../../../assets/icons/icon_return.png')}
            label="Return Date"
            editable="Fri, 23 Jan 2019"
          />
          <InputTextRevamp
            source={require('../../../../assets/icons/icon_total_passenger.png')}
            label="Total Passenger"
            editable="1 Adult"
          />
          <InputTextRevamp
            source={require('../../../../assets/icons/icon_cabin_class.png')}
            label="Cabin Class"
            editable="Economy"
          />
        </ScrollView>
      </View>
    );
  }
}
export default Flight;

const styleInternal = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    paddingLeft: 25,
    paddingRight: 30,
    backgroundColor: thameColors.white,
    borderRadius: 5,
    height: deviceHeight / 7,
  },
  textLabel: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 14,
  },
  textBold: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 16,
    fontWeight: '500',
    paddingBottom: 4,
  },
  textInitial: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 14,
  },
});
