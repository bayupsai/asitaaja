import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { ButtonRounded } from '../../../../../elements/Button';

//Component
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../../base/constant';

const Header = props => {
  return (
    <Grid style={{ marginBottom: 25 }}>
      <Col>
        <View style={{ marginTop: 25 }}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.textGreen}>Your E-tiket has been issued!</Text>
          </View>
          <View
            style={{ marginTop: 15, alignItems: 'center', marginBottom: 25 }}
          >
            <Text style={styles.textLabel}>
              Check your email inbox/spam folder
            </Text>
            <Text style={styles.textLabel}>
              <Text style={{ fontFamily: fontBold }}>
                {props.emailAddress}{' '}
              </Text>
              to obtain E-tiket.
            </Text>
          </View>
          <View style={[styles.sectionHr, { marginBottom: 10 }]} />
          <Text style={{ fontFamily: fontReguler, marginBottom: 10 }}>
            Or download your E-Tiket here
          </Text>
          <ButtonRounded label="DOWNLOAD E-TICKET" />
          <View style={[styles.sectionHr, { marginTop: 10 }]} />
        </View>
      </Col>
    </Grid>
  );
};
export default Header;

const styles = StyleSheet.create({
  sectionHr: {
    width: 90 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  textLabel: {
    color: thameColors.textBlack,
    fontFamily: fontReguler,
    fontSize: 14,
  },
  textGreen: { color: thameColors.green, fontFamily: fontBold, fontSize: 16 },
  textInfo: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 14,
  },
});
