import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import numeral from 'numeral';
//Components
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../../base/constant';

const Passenger = props => {
  return (
    <Grid style={{ marginRight: 25, marginLeft: 25 }}>
      <Col>
        <Row style={{ marginBottom: 20 }}>
          <Text style={styles.textBold}>Passenger</Text>
        </Row>
        {props.passengerData.map((data, index) => {
          return (
            <Row key={index} style={{ marginBottom: 10 }}>
              <Col size={7}>
                <Text style={styles.textBold}>
                  {index + 1}. {data.first_name + ' ' + data.last_name}
                </Text>
              </Col>
              <Col size={3} style={{ alignItems: 'flex-end' }}>
                <Text style={styles.textLabel}>Adult</Text>
              </Col>
            </Row>
          );
        })}

        <Row style={{ marginBottom: 20 }}>
          <Col size={7}>
            <Text style={styles.textBody}>Departure Baggage</Text>
          </Col>
          <Col size={3} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textLabel}>20 kg</Text>
          </Col>
        </Row>
        <Row style={styles.sectionHr}></Row>
        <Row style={{ marginBottom: 20, marginTop: 20 }}>
          <Text style={styles.textBold}>Total Payment</Text>
        </Row>
        <Row style={{ marginBottom: 30, marginTop: 20 }}>
          <Col size={4}>
            <Text style={styles.textBold}>Total Price</Text>
          </Col>
          <Col size={6} style={{ alignItems: 'flex-end' }}>
            <Text style={styles.textPrice}>
              Rp{' '}
              {numeral(props.dataBookingFlight.total)
                .format('0,0')
                .replace(/,/g, '.')}
            </Text>
          </Col>
        </Row>
      </Col>
    </Grid>
  );
};

export default Passenger;

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  textLabel: {
    color: thameColors.textBlack,
    fontFamily: fontReguler,
    fontSize: 14,
  },
  textBody: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 14,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textBlue: {
    fontSize: 14,
    fontFamily: fontReguler,
    color: thameColors.oceanBlue,
  },
  textGreen: { color: thameColors.green, fontFamily: fontBold, fontSize: 14 },
  textPrice: {
    color: thameColors.secondary,
    fontFamily: fontBold,
    fontSize: 16,
  },
});
