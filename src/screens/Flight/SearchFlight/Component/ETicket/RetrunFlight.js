import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
//Components
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;

class ReturnFlight extends React.PureComponent {
  render() {
    return (
      <Grid style={{ height: deviceHeight / 2.5, marginTop: 15 }}>
        <Col style={{ marginLeft: 25, marginRight: 25 }}>
          <Grid style={{ flex: 0, marginBottom: 25 }}>
            <Col style={{ alignItems: 'flex-start' }}>
              <Text style={styles.textOrange}>Return Flight</Text>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <Text style={styles.textBold}>Fri, 23 Jan</Text>
            </Col>
          </Grid>
          <Grid style={{ flex: 0 }}>
            <Col size={1.5} style={{ alignItems: 'flex-start' }}>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>05:35</Text>
                  <Text style={styles.textInfo}>21 Jan</Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>2h 5m</Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>08:40</Text>
                  <Text style={styles.textInfo}>21 Jan</Text>
                </Col>
              </Grid>
            </Col>
            <Col size={1} style={{ alignItems: 'center' }}>
              <Icon
                size={18}
                color={thameColors.gray}
                name="ios-radio-button-off"
                type="ionicon"
              />
              <View style={styles.gridRow}></View>
              <Icon
                name="ios-radio-button-on"
                color={thameColors.gray}
                size={18}
                type="ionicon"
              />
            </Col>
            <Col size={7.5} style={{ alignItems: 'flex-start' }}>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>Bali / Denpasar (DPS)</Text>
                  <Text style={styles.textInfo}>
                    Ngurah Rai International Airport
                  </Text>
                </Col>
              </Grid>
              <Grid style={styles.colStart}>
                <Col
                  size={3}
                  style={{ justifyContent: 'flex-start', marginTop: -15 }}
                >
                  <Image
                    style={{ width: 50, height: 50 }}
                    resizeMode="contain"
                    source={require('../../../../../assets/icons/logo-airlines.png')}
                  />
                </Col>
                <Col size={7}>
                  <Text style={styles.textItinerary}>
                    (GA-652) • Economy Class
                  </Text>
                </Col>
              </Grid>
              <Grid>
                <Col>
                  <Text style={styles.textBold}>Jakarta (CGK)</Text>
                  <Text style={styles.textInfo}>
                    Soekarno Hatta International Airport
                  </Text>
                </Col>
              </Grid>
            </Col>
          </Grid>
          <Grid>
            <Col style={styles.sectionHr}></Col>
          </Grid>
        </Col>
      </Grid>
    );
  }
}

export default ReturnFlight;

const styles = StyleSheet.create({
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  gridRow: {
    height: 60 + '%',
    width: 0.8,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  colStart: { justifyContent: 'center', alignItems: 'flex-start' },
  textItinerary: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    fontSize: 14,
    marginLeft: 3,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 14,
  },
  textOrange: {
    color: thameColors.secondary,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textInfo: {
    color: thameColors.darkGray,
    fontFamily: fontReguler,
    fontSize: 14,
  },
});
