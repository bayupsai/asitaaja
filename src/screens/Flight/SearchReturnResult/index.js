import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { Grid, Col } from 'react-native-easy-grid';
// Component to Imported
import HeaderPage from '../../../components/HeaderPage';
import CardResult from './Component/CardResult';
import SubHeaderPage from '../../../components/SubHeaderPage';
import FlightInfo from '../FormOrder/FlightInfo';
import { fontBold, thameColors } from '../../../base/constant';
import { ButtonRounded } from '../../../elements/Button';
import DateSlider from '../../../components/CalendarSlider/index';
import {
  makeSearchReturnResult,
  makeFligthSelected,
} from '../../../redux/selectors/FlightSelector';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class SearchReturnResult extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dating: new Date(),
      data: [
        { id: 0, airline: 'Garuda Indonesia', price: '1.000.000' },
        { id: 1, airline: 'Sriwijaya', price: '1.000' },
        { id: 2, airline: 'Citilink', price: '1.500.000' },
        { id: 3, airline: 'Air Asia', price: '2.000.000' },
      ],
      arrayDummy: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      arrayCalendar: [1],
      arrayShimerCalendar: [1, 2, 3, 4, 5, 6, 7],
    };
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  // render loading
  loadingCalendar = index => {
    return (
      <Grid key={index} style={[styles.cardCalendar, { padding: 10 }]}>
        {this.state.arrayShimerCalendar.map((item, i) => (
          <Col
            style={{ marginLeft: 15, marginRight: 15, alignItems: 'center' }}
            key={i}
          ></Col>
        ))}
      </Grid>
    );
  };

  renderLoad = index => {
    return <View key={index} style={[styles.card, { padding: 10 }]}></View>;
  };
  // render loading

  // render loading null
  emptyResults = () => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: 20,
        }}
      >
        <View>
          <Image
            resizeMode="center"
            style={{ height: deviceHeight / 2.5, width: deviceWidth - 40 }}
            source={require('../../../assets/icons/search_not_found.png')}
          />
        </View>
        <View style={{ paddingTop: 10, alignItems: 'center' }}>
          <Text style={{ color: '#222222', fontWeight: '500', fontSize: 22 }}>
            No Flight Available
          </Text>
        </View>
        <View
          style={{
            paddingTop: 20,
            paddingLeft: 30,
            paddingRight: 30,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: '#222222',
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            Please search for another date
          </Text>
          {/* or you may */}
        </View>
        <View
          style={{
            paddingTop: 3,
            paddingLeft: 30,
            paddingRight: 30,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: '#222222',
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            click the Change Search button below
          </Text>
        </View>
        <View style={{ marginTop: 30 }}>
          <ButtonRounded
            onClick={() => this.props.navigation.goBack()}
            label="CHANGE SEARCH"
          />
        </View>
      </View>
    );
  };
  // render loading null

  render() {
    return (
      <View style={{ backgroundColor: thameColors.backWhite, flex: 1 }}>
        <HeaderPage title="Return Flight" callback={this.goBackHeader} />
        <ScrollView horizontal={false} removeClippedSubviews={true}>
          <SubHeaderPage />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: -60,
            }}
          >
            <FlightInfo
              {...this.props.flightSelected}
              title="Departure Flight"
              pageType="return"
            />
          </View>

          <View
            style={{
              backgroundColor: thameColors.backWhite,
              alignItems: 'center',
              justifyContent: 'center',
              padding: 0,
            }}
          >
            <Text
              style={{
                color: thameColors.superBack,
                fontSize: 14,
                fontFamily: fontBold,
              }}
            >
              Select return flight from{' '}
              {this.props.listResult ? this.props.listResult.length : '-'}{' '}
              schedules
            </Text>
          </View>
          <Grid>
            <Col
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 5,
                marginBottom: 5,
              }}
            >
              <DateSlider {...this.props.navigation.state.params} />
            </Col>
          </Grid>
          <Grid
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 10,
              marginLeft: 8,
              marginRight: 8,
            }}
          >
            <Col>
              {this.props.loading
                ? this.state.arrayDummy.map((item, index) =>
                    this.renderLoad(index)
                  )
                : this.props.listResult.length !== 0
                ? this.props.listResult
                    .filter(data => data.name != 'AIRASIA')
                    .map((item, i) => (
                      <CardResult {...this.props} {...item} key={i} />
                    ))
                : this.emptyResults()}
            </Col>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    justifyContent: 'center',
    marginTop: -10,
    alignItems: 'center',
    width: deviceWidth,
    height: 'auto',
    transform: [{ translateY: -10 }],
  },
  sectionContentScroll: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  //card
  card: {
    backgroundColor: thameColors.white,
    flex: 1,
    height: 'auto',
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 10,
  },
  cardCalendar: {
    backgroundColor: thameColors.white,
    width: deviceWidth / 1.05,
    height: 80,
    borderRadius: 6,
    marginBottom: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
  },
  cardBody: {
    paddingTop: 5,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  //card
});

function mapStateToProps(state) {
  return {
    flightSelected: makeFligthSelected(state),
    listResult: makeSearchReturnResult(state),
    loading: state.flight.singleSearchFetch,
  };
}
export default connect(mapStateToProps)(SearchReturnResult);
