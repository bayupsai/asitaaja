import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';

import Heading from './Header';
import Tabs from '../../../../components/Tabs';

//Route Tab
import TabOne from './Tab/TabOne';
import TabTwo from './Tab/TabTwo';
import { fontReguler, thameColors } from '../../../../base/constant';

//Master Modal
class DisplayModal extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      page: 0,
      modalVisible: true,
    };

    this._onTabChange = this._onTabChange.bind(this);
  }

  _onTabChange(tabIndex) {
    this.setState({ page: tabIndex });
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  resetModal = () => {
    this.setState({ modalVisible: false });
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.display}
        hardwareAccelerated={false}
        onRequestClose={this.props.close}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackButtonPress={this.resetModal}
        onBackdropPress={this.resetModal}
        style={{ margin: 0, padding: 0 }}
      >
        <View style={styles.container}>
          <Heading title="Details" backButton={this.props.close} />
          <Tabs>
            {/* First tab */}
            <TabOne
              modalPress={this.props.modalPress}
              {...this.props}
              title="Flight Details"
            />
            {/* Second tab */}
            <TabTwo {...this.props} title="Facilities" />
            {/* Third tab */}
            {/* <TabThree {...this.props} title="Price Details" /> */}
          </Tabs>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
    backgroundColor: thameColors.white, // Background color
  },
  // Tab content container
  content: {
    flex: 1, // Take up all available space
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
    backgroundColor: thameColors.secondary, // Darker background for content area
  },
  // Content header
  header: {
    margin: 5, // Add margin
    color: thameColors.white, // White color
    fontFamily: fontReguler, // Change font family
    fontSize: 26, // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: fontReguler,
    fontSize: 18,
  },
});

export default DisplayModal;
