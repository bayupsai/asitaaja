import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import Numeral from 'numeral';
import { ButtonRounded } from '../../../../../elements/Button';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../../../base/constant';

export default class TabOne extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ marginLeft: 25, marginRight: 25, marginTop: 25 }}>
          <Grid style={{ marginTop: 10 }}>
            <Col>
              <View>
                <Text style={styles.textGreen}>Reschedule Available</Text>
              </View>
              <View style={{ paddingTop: 20 }}>
                <Text style={styles.textBody}>Adult Fare (x1)</Text>
              </View>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <View>
                <Text style={styles.textGreen}>Refundable</Text>
              </View>
              <View style={{ paddingTop: 20 }}>
                <Text style={styles.textBold}>
                  Rp
                  {Numeral(this.props.price_adult)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </View>
            </Col>
          </Grid>
          <Grid style={{ marginTop: 20, marginBottom: 20 }}>
            <Col style={styles.sectionHr}></Col>
          </Grid>

          <Grid style={{ marginTop: 10 }}>
            <Col>
              <Grid style={{ flex: 0 }}>
                <Col>
                  <Text style={styles.textBold}>Tax and other fees</Text>
                </Col>
              </Grid>
              <Grid style={{ flex: 0, paddingTop: 10 }}>
                <Col>
                  <View>
                    <Text style={styles.textBody}>Service Charge</Text>
                  </View>
                  <View style={{ paddingTop: 10 }}>
                    <Text style={styles.textBody}>Tax</Text>
                  </View>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <View>
                    <Text style={styles.textGreen}>Free</Text>
                  </View>
                  <View style={{ paddingTop: 10 }}>
                    <Text style={styles.textBold}>Included</Text>
                  </View>
                </Col>
              </Grid>
            </Col>
          </Grid>
          <Grid style={{ marginTop: 20, marginBottom: 20 }}>
            <Col style={styles.sectionHr}></Col>
          </Grid>
        </ScrollView>

        <Grid
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 0,
            marginBottom: 15,
            marginTop: 20,
          }}
        >
          <Col>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid
              style={{
                flex: 0,
                marginBottom: 10,
                marginLeft: 25,
                marginRight: 25,
              }}
            >
              <Col>
                <Text style={styles.textBold}>Total Payment</Text>
              </Col>
              <Col style={{ alignItems: 'flex-end' }}>
                <Text style={styles.textPrice}>
                  Rp.{' '}
                  {Numeral(this.props.price_adult)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid
              style={{
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 15,
                marginBottom: 5,
              }}
            >
              <ButtonRounded
                onClick={this.props.modalPress}
                label="SELECT FLIGHT"
              />
            </Grid>
          </Col>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: thameColors.white, flex: 1 },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.halfWhite,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textGreen: { fontFamily: fontBold, color: thameColors.green, fontSize: 14 },
  textBody: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 14,
  },
  textPrice: { fontFamily: fontBold, color: thameColors.primary, fontSize: 16 },
});
