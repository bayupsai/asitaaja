import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { thameColors, fontBold, asitaColor } from '../../../../base/constant';
import { statusHeight } from '../../../../elements/BarStyle';

const Back = props => {
  return (
    <TouchableOpacity onPress={props.goBack} style={{ paddingRight: 20 }}>
      <Icon
        name="ios-arrow-round-back"
        color={thameColors.white}
        size={42}
        type="ionicon"
      />
    </TouchableOpacity>
  );
};

const Title = props => {
  return (
    <Text
      style={{
        color: thameColors.white,
        fontSize: 18,
        fontFamily: fontBold,
        letterSpacing: 0.5,
        marginLeft: -10,
      }}
    >
      {props.title}
    </Text>
  );
};

const Heading = props => (
  <Header
    leftComponent={<Back goBack={props.backButton} />}
    centerComponent={<Title title={props.title} />}
    rightComponent={<Text />}
    containerStyle={[
      {
        backgroundColor: asitaColor.marineBlue,
        paddingTop: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        height: 50 + statusHeight,
      },
    ]}
  ></Header>
);

export default Heading;
