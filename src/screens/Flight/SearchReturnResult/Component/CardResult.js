import React from 'react';
import { Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';
import Numeral from 'numeral';
import { connect } from 'react-redux';
import Dash from 'react-native-dash';
import { actionFlightSelected } from '../../../../redux/actions/FlightAction';
import AlertModal from '../../../../components/AlertModal';

//component imported
import DisplayModal from './DisplayModal';
import {
  fontReguler,
  fontBold,
  thameColors,
  asitaColor,
} from '../../../../base/constant';

class CardResult extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: null,
    };
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  modalPress = (data, modal) => {
    this.setState({ modalVisible: modal }, () => this.flightSelected(data));
  };

  setNullModal = () => {
    this.setState({ modalVisible: null });
  };

  flightSelected = data => {
    let payload = {
      arrival_date: data.arrival_date,
      arrival_time: data.arrival_time,
      departure_date: data.departure_date,
      departure_time: data.departure_time,
      duration: data.duration,
      flight_number: data.flight_number,
      full_via: data.full_via,
      detail: data.detail,
      name: data.name,
      price_adult: data.price_adult,
      price_child: data.price_child,
      price_infant: data.price_infant,
      schedule_id: data.schedule_id,
      stop: data.stop,
    };
    this.props.dispatch(actionFlightSelected(payload, 'returnPage'));
    this.props.navigation.navigate('FormOrder');
  };

  handlePress = () => {
    const data = this.props;
    requestAnimationFrame(() => {
      this.flightSelected(data);
    });
  };

  render() {
    const data = this.props;
    let lastIndex = this.props.detail.length - 1;
    return (
      <TouchableOpacity onPress={this.handlePress} style={[styles.card]}>
        <DisplayModal
          modalPress={() => this.modalPress(data, null)}
          {...this.props}
          display={this.state.modalVisible === 1}
          close={this.setNullModal}
          modalin={this.setNullModal}
        />
        {/* ========================== MODAL Error Name ========================== */}
        <AlertModal
          isVisible={this.state.modalVisible == 99}
          type="normal"
          contentText="Sorry for the inconvenience, currently flight only available for Garuda Indonesia. We are still working to make other airlines available, Thanks."
          onDismiss={this.setNullModal}
          onPress={this.setNullModal}
        />
        {/* ========================== MODAL Error Name ========================== */}

        <Grid style={[styles.cardBody]}>
          <Col
            size={2}
            style={{
              backgroundColor: 'white',
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 16,
              }}
            >
              {this.props.departure_time}
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontBold,
                paddingLeft: 5,
                color: thameColors.textBlack,
              }}
            >
              {this.props.detail[0].departure_city}
            </Text>
          </Col>
          <Col size={5}>
            <Grid
              style={{ alignItems: 'center', justifyContent: 'flex-start' }}
            >
              <Col>
                <Dash style={styles.dash} dashColor={thameColors.buttonGray} />
              </Col>
              <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Image
                  source={require('../../../../assets/icons/plane3x-new.png')}
                  style={{ width: 25, height: 20, resizeMode: 'center' }}
                />
              </Col>
              <Col>
                <Dash style={styles.dash} dashColor={thameColors.buttonGray} />
              </Col>
            </Grid>
            <Grid>
              <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: thameColors.gray,
                    fontFamily: fontReguler,
                  }}
                >
                  {this.props.duration}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: thameColors.gray,
                    fontFamily: fontReguler,
                  }}
                >
                  {this.props.stop === 'Langsung' ? 'Direct' : this.props.stop}
                </Text>
              </Col>
            </Grid>
          </Col>
          <Col
            size={2}
            style={{
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.lightBrown,
                fontSize: 16,
              }}
            >
              {this.props.arrival_time}
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontBold,
                paddingLeft: 5,
                color: thameColors.textBlack,
              }}
            >
              {this.props.detail[lastIndex].arrival_city}
            </Text>
          </Col>
          <Col
            size={1}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              paddingLeft: 10,
              paddingRight: 10,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.handleModal(1);
              }}
            >
              <Icon
                name="ios-arrow-down"
                size={25}
                color={asitaColor.oceanBlue}
              />
            </TouchableOpacity>
          </Col>
        </Grid>

        <Grid style={styles.cardFooter}>
          <Col size={6}>
            <Grid style={{ alignItems: 'center' }}>
              <Col>
                <Image
                  style={{ width: 50, height: 50 }}
                  resizeMode="center"
                  source={
                    this.props.name === 'GARUDA'
                      ? require('../../../../assets/icons/logo-airlines.png')
                      : { uri: this.props.detail[0].img_src }
                  }
                />
              </Col>
            </Grid>
          </Col>
          <Col
            size={4}
            style={{ alignItems: 'flex-end', justifyContent: 'center' }}
          >
            <Text style={styles.prices}>
              Rp.{' '}
              {Numeral(this.props.price_adult)
                .format('0,00')
                .replace(/,/g, '.')}
            </Text>
          </Col>
        </Grid>
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state) {
  return {
    flight: state.flight,
  };
}
export default connect(mapStateToProps)(CardResult);

const styles = StyleSheet.create({
  card: {
    backgroundColor: thameColors.white,
    flex: 1,
    height: 'auto',
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 0,
    marginBottom: 10,
  },
  cardHeader: {
    padding: 5,
  },
  cardBody: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  cardFooter: {
    paddingTop: 0,
    paddingBottom: 0,
    padding: 10,
    borderTopWidth: 0.25,
    borderTopColor: thameColors.halfWhite,
  },
  sectionCenterText: {
    alignSelf: 'center',
  },
  sectionAlignCenter: {
    alignItems: 'center',
    transform: [{ translateY: 7 }],
  },
  sectionAlignEnd: {
    alignItems: 'flex-end',
  },
  textBlue: {
    color: thameColors.darkBlue,
    fontFamily: fontReguler,
  },
  textGrey: {
    color: thameColors.lightBrown,
    fontFamily: fontReguler,
  },
  textOrage: {
    color: thameColors.orange,
    fontFamily: fontReguler,
  },
  textNote: {
    fontSize: 9,
    fontFamily: fontReguler,
  },
  text17: {
    fontSize: 17,
    fontFamily: fontReguler,
  },
  text19: {
    fontSize: 19,
    fontFamily: fontReguler,
  },
  airlines: {
    fontSize: 13,
    fontFamily: fontReguler,
  },
  prices: {
    color: thameColors.primary,
    fontFamily: fontBold,
    fontSize: 16,
  },
  dash: {},
});
