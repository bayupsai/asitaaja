import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Calendar from './Calendar';
import { thameColors } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceWidth = window.width;

const DateScroll = props => (
  <View style={styles.card}>
    <Calendar />
  </View>
);

export default DateScroll;

const styles = StyleSheet.create({
  card: {
    backgroundColor: thameColors.white,
    width: deviceWidth / 1.15,
    height: 'auto',
    borderRadius: 5,
    marginBottom: 25,
    // box shadow
    shadowColor: thameColors.superBack,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
});
