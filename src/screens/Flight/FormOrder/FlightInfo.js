import React from 'react';
import { Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../base/constant';
import Dash from 'react-native-dash';

var deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    paddingBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    flex: 0,
    marginBottom: 15,
  },
  cardBox: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    backgroundColor: thameColors.white,
    width: deviceWidth / 1.1,
    paddingBottom: 20,
    borderRadius: 5,
  },
  title: { color: asitaColor.orange, fontFamily: fontExtraBold, fontSize: 16 },
  sectionBox: { flex: 0, paddingTop: 10, paddingBottom: 0 },
  rute: {
    fontFamily: fontExtraBold,
    fontSize: 16,
    color: thameColors.textBlack,
  },
  departureDate: {
    fontFamily: fontReguler,
    fontSize: 16,
    color: thameColors.lightBrown,
  },
  classInfo: { fontFamily: fontBold, fontSize: 14, color: thameColors.gray },
  buttonDetail: {
    flex: 0,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
  duration: {
    color: thameColors.gray,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  departureTime: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  departureCode: {
    fontSize: 14,
    fontFamily: fontReguler,
    paddingLeft: 5,
    color: thameColors.gray,
  },
  prices: { color: asitaColor.orange, fontWeight: 'bold', fontSize: 16 },
});

const FlightInfo = props => (
  <Grid style={styles.container}>
    <Col style={styles.cardBox}>
      <Row style={{ flex: 0 }}>
        <Col size={5}>
          <Text style={styles.title}>{props.title}</Text>
        </Col>
        <Col size={5} style={{ alignItems: 'flex-end' }}>
          <Text style={styles.departureDate}>
            {props.detail[0].string_departure_date_short}
          </Text>
        </Col>
      </Row>
      <Row style={{ marginTop: 5, marginBottom: 5 }}>
        <Col size={2}>
          <Text style={styles.departureTime}>{props.departure_time}</Text>
        </Col>
        <Col size={6}>
          <Grid style={{ alignItems: 'center', justifyContent: 'flex-start' }}>
            <Col>
              <Dash dashColor={thameColors.buttonGray} />
            </Col>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={require('../../../assets/icons/plane3x-new.png')}
                style={{ width: 25, height: 20, resizeMode: 'center' }}
              />
            </Col>
            <Col>
              <Dash dashColor={thameColors.buttonGray} />
            </Col>
          </Grid>
        </Col>
        <Col size={2} style={{ alignItems: 'flex-end' }}>
          <Text style={styles.departureTime}>{props.arrival_time} </Text>
        </Col>
      </Row>
      <Row>
        <Col size={2}>
          <Text style={styles.rute}>{props.detail[0].departure_city}</Text>
        </Col>
        <Col size={6} style={{ alignItems: 'center' }}>
          <Text style={styles.duration}>{props.duration}</Text>
        </Col>
        <Col size={2} style={{ alignItems: 'flex-end' }}>
          <Text style={styles.rute}>{props.detail[0].arrival_city}</Text>
        </Col>
      </Row>
      <Row>
        <Col>
          {props.name == 'GARUDA' ? (
            <Image
              style={{ width: 50, height: 50 }}
              resizeMode="center"
              source={require('../../../assets/icons/logo-airlines.png')}
            />
          ) : (
            <Image
              style={{ width: 50, height: 50 }}
              resizeMode="center"
              source={{ uri: props.detail[0].img_src }}
            />
          )}
        </Col>
        <Col style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
          <Text style={styles.rute}>{props.name}</Text>
        </Col>
      </Row>
    </Col>
  </Grid>
);

export default FlightInfo;
