import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { ButtonPlus } from '../../../elements/ButtonPlus';
import { fontBold, fontReguler, thameColors } from '../../../base/constant';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: -20,
    flex: 0,
  },
  title: { color: thameColors.lightBrown, fontFamily: fontBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 10,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center' },
  subtitle: {
    fontSize: 16,
    color: thameColors.grayBrown,
    fontFamily: fontReguler,
  },
  sectionButton: { alignItems: 'flex-end' },
});

const SpecialRequest = props => {
  return (
    <Grid style={styles.container}>
      <Row style={styles.section}>
        <Col size={8} style={styles.sectionName}>
          <View>
            <Text style={styles.title}>{props.title}</Text>
          </View>
          <View>
            <Text style={styles.subtitle}>{props.subtitle}</Text>
          </View>
        </Col>
        <Col size={2} style={styles.sectionButton}>
          <ButtonPlus size="small" colorButton="buttonColor" />
        </Col>
      </Row>
    </Grid>
  );
};

export default SpecialRequest;
