import React from 'react';
import { Text, StyleSheet, Switch } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
  thameColors,
} from '../../../base/constant';

const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 15, marginTop: -15 },
  title: {
    color: thameColors.lightBrown,
    fontFamily: fontExtraBold,
    fontSize: 16,
  },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 15,
    borderRadius: 5,
    flexDirection: 'column',
  },
  sectionName: { justifyContent: 'center' },
  contactName: {
    fontSize: 16,
    color: thameColors.grayBrown,
    fontFamily: fontReguler,
  },
  sectionButton: { alignItems: 'flex-end' },
  subtitle: {
    color: thameColors.lightBrown,
    fontFamily: fontBold,
    fontSize: 16,
    fontFamily: fontReguler,
  },
  desc: {
    color: thameColors.lightBrown,
    fontSize: 16,
    paddingTop: 10,
    fontFamily: fontReguler,
  },
  price: {
    color: thameColors.lightBrown,
    fontWeight: 'bold',
    fontSize: 16,
    fontFamily: fontReguler,
  },
});

class Insurance extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeInsurance: false,
    };
  }

  toggleActiveInsurance = () => {
    this.setState({ activeInsurance: true });
  };

  render() {
    return (
      <Grid style={styles.container}>
        <Row>
          <Text style={styles.title}>Travel Insurance</Text>
        </Row>
        <Row style={styles.section}>
          <Grid>
            <Col size={5} style={{ paddingLeft: 5 }}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.primary,
                  fontSize: 16,
                }}
              >
                Rp. 39.000{' '}
                <Text
                  style={{
                    color: thameColors.textBlack,
                    fontFamily: fontReguler,
                  }}
                >
                  /pax
                </Text>
              </Text>
            </Col>
            <Col
              size={5}
              style={{ justifyContent: 'center', alignItems: 'flex-end' }}
            >
              <Switch
                onValueChange={this.toggleActiveInsurance}
                value={this.state.activeInsurance}
              />
            </Col>
          </Grid>
          <Grid style={{ marginTop: 10 }}>
            <Col size={8} style={{ paddingLeft: 5 }}>
              <Text
                style={{ fontFamily: fontReguler, color: thameColors.darkGray }}
              >
                Want to read Travel Insurance
              </Text>
              <Text
                style={{ fontFamily: fontReguler, color: thameColors.darkGray }}
              >
                information?{' '}
                <Text
                  onPress={() => alert('More Info')}
                  style={{ color: thameColors.primary, fontFamily: fontBold }}
                >
                  More Info
                </Text>{' '}
              </Text>
              {/* <Row><Text style={{fontFamily: fontBold, color: thameColors.darkGray, fontSize: 16}}>Want to read Travel Insurance</Text>  </Row>
                           <Row><Text style={{fontFamily: fontReguler, color: thameColors.darkGray}}>information? </Text></Row> */}
            </Col>
            <Col size={2} />
          </Grid>
        </Row>
      </Grid>
    );
  }
}

export default Insurance;
