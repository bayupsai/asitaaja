/* eslint-disable react-native/no-inline-styles */
// @flow
import React, { PureComponent } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  InteractionManager,
} from 'react-native';
import { connect } from 'react-redux';
import { DotIndicator } from 'react-native-indicators';
import Modal from 'react-native-modal';
import { Grid, Row, Col } from 'react-native-easy-grid';
import styles from '../SearchFlight/styles';
// COMPONENT
import { ButtonRounded } from '../../../elements/Button';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import FlightInfo from './FlightInfo';
import Contact from './Contact';
import Passenger from './Passenger';
import Prices from './Prices';
import AlertModal from '../../../components/AlertModal';
// COMPONENT

import {
  actionBookingFlight,
  failedState,
} from '../../../redux/actions/FlightAction';
import {
  fontExtraBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../base/constant';
import {
  makeAllFLight,
  makePassengerData,
  makeContactDetail,
  makeFligthSelected,
  makeFlightReturnSelected,
} from '../../../redux/selectors/FlightSelector';

// Static Type
type Props = {
  dispatch: Function,
  navigation: Function,
  flight: Object,
  flightSelected: Object,
  flightReturnSelected: Object,
  contact: Object,
  passengerData: any,
  token: String,
};

type State = {
  usingInsurance: boolean,
  insurancePrice: number,
  visibleModal: any,
  validationStatusData: boolean,
  errorMessage: string,
};

class FormOrder extends PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      usingInsurance: false,
      insurancePrice: 0,
      visibleModal: null,
      validationStatusData: false,
      errorMessage: '',
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.dispatch(failedState('bookingFlight'));
    });
  }

  // ==== GLOBAL FUNC
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  _goBackHome = () => {
    this.props.navigation.navigate('Home');
  };

  // Modal
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  _setNullModal = () => {
    this.setState({ visibleModal: null });
  };

  // Modal
  checkUsingInsurane = () => {
    if (this.state.usingInsurance === true) {
      this.setState({ usingInsurance: false, insurancePrice: 0 });
    } else {
      this.setState({ usingInsurance: true, insurancePrice: 25000 });
    }
  };

  // ==== GLOBA FUNC
  validateAllData = () => {
    const totalPassenger =
      parseInt(this.props.flight.adults, 14) +
      parseInt(this.props.flight.childs, 14) +
      parseInt(this.props.flight.infants, 14);
    const reduxPassengers = this.props.flight.passengerData;

    if (reduxPassengers.length !== totalPassenger) {
      this.setState({
        visibleModal: 91,
        errorMessage:
          'Please enter your contact details and passenger details correctly',
      });
    } else {
      let checkValidation = false;
      reduxPassengers.map((data, index) => {
        if (data.first_name === '' || data.salutation === '') {
          checkValidation = false;
        } else {
          checkValidation = true;
        }
      });

      if (checkValidation) {
        this.setState({ validationStatusData: true }, () => {
          this._toggleModal(5);
        });
      } else {
        this.setState({
          visibleModal: 91,
          errorMessage:
            'Please enter your contact details and passenger details correctly',
        });
      }
    }
  };

  checkAvailabilityData = () => {
    const { flight } = this.props;
    if (
      flight.contactDetail.title !== '' &&
      flight.contactDetail.fullName !== '' &&
      flight.contactDetail.emailAddress !== '' &&
      flight.contactDetail.phoneNumber.length >= 3
    ) {
      this.validateAllData();
    } else {
      this.setState({
        visibleModal: 91,
        errorMessage:
          'Please enter your contact details and passenger details correctly',
      });
    }
  };

  paymentOrder = () => {
    this.setState({ visibleModal: 4 });
    const payload = {
      id: 1,
      command: 'BOOKING',
      product: 'FLIGHT',
      data: {
        departure_code: this.props.flight.origin,
        partner_trxid: 'PARTNER-001',
        arrival_code: this.props.flight.destination,
        departure_date: this.props.flight.departureDate,
        return_date: this.props.flight.returnDate
          ? this.props.flight.returnDate
          : '',
        adult: this.props.flight.adults,
        child: this.props.flight.childs,
        infant: this.props.flight.infants,
        schedule_id: this.props.flight.flightSelected.schedule_id,
        return_schedule_id: this.props.flight.returnDate
          ? this.props.flightReturnSelected.schedule_id
          : '',
        class: '',
        sub_class: '',
        return_class: '',
        contact_detail: {
          salutation: this.props.contact.title,
          fullname: this.props.contact.fullName,
          email: this.props.contact.emailAddress,
          phone: this.props.contact.phoneNumber,
        },
        passengers: this.props.passengerData,
        // name: this.props.flight.name
      },
    };
    if (this.state.validationStatusData) {
      this.props
        .dispatch(
          actionBookingFlight(payload, this.props.token ? this.props.token : '')
        )
        .then(res => {
          this.setState({ visibleModal: null }, () => {
            if (res.type === 'BOOKING_FLIGHT_SUCCESS') {
              this.props.navigation.navigate('PaymentOrder', {
                pages: 'flight',
              });
            } else {
              this.setState({
                visibleModal: 91,
                errorMessage: 'Booking failed, please try again later',
              });
            }
          });
        })
        .catch(err => {
          this.setState({ visibleModal: 91, errorMessage: err.message });
        });
    } else {
      this.setState({
        visibleModal: 91,
        errorMessage:
          'Please enter your contact details and passenger details correctly',
      });
    }
  };

  _alertModal = (idModal, message) => (
    <AlertModal
      type="normal"
      isVisible={this.state.visibleModal === 91}
      title={this.state.visibleModal === 91 ? 'Failed' : 'Warning'}
      contentText={message}
      onPress={this._setNullModal}
      onDismiss={this._setNullModal}
      labelCancel=""
      labelOk=""
      titleColor=""
    />
  );

  loadingBooking = () => (
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor: thameColors.white,
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        height: 'auto',
        flex: 0,
      }}
    >
      <Row style={{ flex: 0 }}>
        <Col
          size={3}
          style={{ alignItems: 'center', justifyContent: 'center' }}
        >
          <Image
            source={require('../../../assets/img/loading3x.png')}
            style={{ width: 175, height: 175 }}
            resizeMode="center"
          />
        </Col>
      </Row>
      <Row style={{ flex: 0 }}>
        <Col size={7}>
          <Row
            style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}
          >
            <Text
              style={{
                fontFamily: fontExtraBold,
                fontSize: 16,
                color: thameColors.superBack,
              }}
            >
              Relaxed for a moment
            </Text>
          </Row>
          <Row
            style={{
              flex: 0,
              paddingTop: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: fontReguler,
                fontSize: 14,
                color: thameColors.superBack,
              }}
            >
              We are currently processing your booking.
            </Text>
          </Row>
          <Row
            style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}
          >
            <Text
              style={{
                fontFamily: fontReguler,
                fontSize: 14,
                color: thameColors.superBack,
              }}
            >
              This may take a few minutes.
            </Text>
          </Row>
          <Row
            style={{
              flex: 0,
              paddingTop: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <DotIndicator
              size={10}
              style={{ flex: 0, backgroundColor: thameColors.white }}
              color={asitaColor.tealBlue}
            />
          </Row>
        </Col>
      </Row>
    </Grid>
  );

  // Main Render
  render() {
    const { errorMessage } = this.state;
    return (
      <View style={{ backgroundColor: thameColors.backWhite, flex: 0 }}>
        <ScrollView>
          <HeaderPage
            title="Booking Summary"
            callback={this.goBackHeader}
            {...this.props}
          />
          <SubHeaderPage />

          <View style={{ marginTop: -50 }}>
            <FlightInfo
              {...this.props.flightSelected}
              title="Departure Flight"
            />
            {this.props.flight.returnDate !== '' ? (
              <FlightInfo
                {...this.props.flightReturnSelected}
                title="Return Flight"
              />
            ) : null}
          </View>

          <Contact {...this.props} />

          <Passenger onChangeData={this.validateAllData} />
          {/* <AdditionalBaggage /> */}

          {/* <View style={{ marginTop: 0 }}>
                        <SpecialRequest title="Booking Hotel" subtitle="Combine your Flight with Hotel"/>
                        <SpecialRequest title="Pickup" subtitle="Combine your Flight with Pickup" />
                    </View> */}
          {/* <Insurance  {...this.state } onPress={() => this.checkUsingInsurane() } /> */}

          <Prices {...this.props} />

          <View style={[styles.sectionButtonArea]}>
            <View style={[styles.sectionButton]}>
              <ButtonRounded
                label="CONTINUE PAYMENT"
                onClick={this.checkAvailabilityData}
              />
            </View>
          </View>
        </ScrollView>

        {/* LOADING MODAL */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 4}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={200}
          animationOutTiming={200}
          backdropTransitionInTiming={200}
          backdropTransitionOutTiming={200}
        >
          {this.loadingBooking()}
        </Modal>

        {/* Booking Summary Alert */}
        <AlertModal
          type="qna"
          isVisible={this.state.visibleModal === 5}
          titleColor={thameColors.superBack}
          title="Are your booking details correct?"
          contentText="You will not be able to change your booking details once you proceed to payment. Do you want to continue?"
          labelOk="YES, CONTINUE"
          labelCancel="CHECK AGAIN"
          onPress={this.paymentOrder}
          onDismiss={this._setNullModal}
        />
        {/* Booking Summary Alert */}

        {/* Alert when Back Button Pressed */}
        <AlertModal
          type="qna"
          isVisible={this.state.visibleModal === 6}
          titleColor={thameColors.superBack}
          title="Are you sure want to back to Transaction?"
          contentText="You can continue your transaction again in My Order List"
          labelOk="YES, CONTINUE"
          labelCancel="NO, THANKS"
          onPress={this._goBackHome}
          onDismiss={this._setNullModal}
        />
        {/* Alert when Back Button Pressed */}

        {this._alertModal(91, errorMessage)}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    flight: makeAllFLight(state),
    bookingFetching: state.flight.bookingFetch,
    token: state.profile.token,
    passengerData: makePassengerData(state),
    contact: makeContactDetail(state),
    flightSelected: makeFligthSelected(state),
    flightReturnSelected: makeFlightReturnSelected(state),
  };
}
export default connect(mapStateToProps)(FormOrder);
