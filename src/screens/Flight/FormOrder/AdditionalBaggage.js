import React from 'react';
import { Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { ButtonPlus } from '../../../elements/ButtonPlus';
import { fontReguler, fontBold, thameColors } from '../../../base/constant';

let deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 10, marginTop: -35 },
  title: { color: thameColors.lightBrown, fontFamily: fontBold, fontSize: 16 },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 15,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center', paddingLeft: 5 },
  contactName: { fontSize: 16, color: thameColors.superBack },
  sectionButton: { alignItems: 'flex-end' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 1.75,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

class AdditionalBaggage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Grid style={styles.container}>
        <Row style={{ marginTop: 25 }}>
          <Text style={styles.title}>Additional Baggage</Text>
        </Row>

        {/* Baggage */}
        <TouchableOpacity
          onPress={() => this.setState({ visibleModal: 1 })}
          style={styles.section}
        >
          <Grid>
            <Col style={styles.sectionName}>
              <Text style={[styles.contactName]}>
                {this.state.fullName ? (
                  this.state.title + '. ' + this.state.fullName
                ) : (
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontReguler,
                    }}
                  >
                    Baggage (0 kg)
                  </Text>
                )}
              </Text>
            </Col>
            <Col style={styles.sectionButton}>
              <ButtonPlus
                size="small"
                colorButton="buttonColor"
                onClick={() => this.setState({ visibleModal: 1 })}
              />
            </Col>
          </Grid>
        </TouchableOpacity>

        <Row>
          <Text
            style={{
              color: thameColors.darkGray,
              fontStyle: 'italic',
              fontFamily: fontReguler,
            }}
          >
            Add baggage to lighten your load
          </Text>
        </Row>
      </Grid>
    );
  }
}

export default AdditionalBaggage;
