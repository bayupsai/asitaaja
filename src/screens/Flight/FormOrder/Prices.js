import React from 'react';
import { Text, StyleSheet, Image, View } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Numeral from 'numeral';
import {
  fontExtraBold,
  fontReguler,
  fontBold,
  thameColors,
  asitaColor,
} from '../../../base/constant';

const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 10, flex: 1 },
  title: {
    color: thameColors.textBlack,
    fontFamily: fontExtraBold,
    fontSize: 16,
  },
  titlePrice: {
    color: thameColors.textBlack,
    fontFamily: fontExtraBold,
    fontSize: 18,
  },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 15,
    borderRadius: 5,
    flexDirection: 'column',
  },
  sectionName: { justifyContent: 'center' },
  contactName: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
  },
  detailPrices: {
    fontSize: 16,
    color: asitaColor.orange,
    fontFamily: fontBold,
  },
  totalPrices: { fontSize: 16, color: asitaColor.orange, fontFamily: fontBold },
  sectionButton: { alignItems: 'flex-end' },
  pt5: { paddingTop: 5, paddingLeft: 5, paddingRight: 5 },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    backgroundColor: thameColors.backWhite,
    alignSelf: 'center',
  },
  pl5: { paddingLeft: 5 },
});

const Prices = props => {
  const isReturn = props.flight.returnDate;
  const departureData = props.flight.flightSelected;
  const totalAdults = parseInt(departureData.price_adult) * props.flight.adults;
  const totalChilds = parseInt(departureData.price_child) * props.flight.childs;
  const totalInfants =
    parseInt(departureData.price_infant) * props.flight.infants;
  const totalPriceDeparture = totalAdults + totalChilds + +totalInfants;
  let totalPriceReturn = 0;
  let returnData = {};
  if (isReturn) {
    returnData = props.flight.flightReturnSelected;
    const totalReturnAdults =
      parseInt(returnData.price_adult) * props.flight.adults;
    const totalReturnChilds =
      parseInt(returnData.price_child) * props.flight.childs;
    const totalReturnInfants =
      parseInt(returnData.price_infant) * props.flight.infants;
    totalPriceReturn =
      totalReturnAdults + totalReturnChilds + totalReturnInfants;
  } else {
    totalPriceReturn = 0;
  }

  const totalPrice = parseInt(totalPriceDeparture) + parseInt(totalPriceReturn);
  return (
    <View style={styles.container}>
      {/* =============================== PRICE ONE WAY ===================================== */}
      {isReturn ? (
        <Grid style={{ flex: 0 }} />
      ) : (
        <Grid style={{ flex: 0, marginTop: -5 }}>
          <Row>
            <Text style={styles.title}>Total Payment</Text>
          </Row>
          <Row style={styles.section}>
            <Grid style={styles.pt5}>
              {departureData.price_adult > 0 && (
                <Row style={{ paddingBottom: 15 }}>
                  <Col style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Adult x{props.flight.adults}{' '}
                    </Text>
                  </Col>
                  <Col style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(departureData.price_adult)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              )}
              {departureData.price_child > 0 && props.flight.childs > 0 ? (
                <Row style={{ paddingBottom: 15 }}>
                  <Col style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Child x{props.flight.childs}{' '}
                    </Text>
                  </Col>
                  <Col style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(departureData.price_child)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              ) : null}
              {departureData.price_infant > 0 && props.flight.infants > 0 ? (
                <Row style={{ paddingBottom: 15 }}>
                  <Col style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Infant x{props.flight.infants}{' '}
                    </Text>
                  </Col>
                  <Col style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(departureData.price_infant)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              ) : null}
              {/* <Row style={styles.sectionHr} />
                            <Row style={{paddingBottom: 15, paddingTop: 15}}>
                                <Col style={styles.sectionName}>
                                    <Text style={styles.contactName}>Baggage Charge</Text>
                                </Col>
                                <Col style={styles.sectionButton}>
                                    <Text style={styles.contactName}>
                                        Rp. 0
                                    </Text>
                                </Col>
                            </Row> */}
              <Row style={styles.sectionHr} />
              <Row style={{ paddingBottom: 5, paddingTop: 15 }}>
                <Col style={styles.sectionName}>
                  <Text style={[styles.contactName, { fontFamily: fontBold }]}>
                    Price You Pay
                  </Text>
                </Col>
                <Col style={styles.sectionButton}>
                  <Text style={styles.totalPrices}>
                    IDR{' '}
                    {Numeral(totalPrice)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Row>
            </Grid>

            {props.insurancePrice > 0 && (
              <Grid style={styles.pt5}>
                <Row>
                  <Col style={styles.sectionName}>
                    <Text style={styles.contactName}>Insurance</Text>
                  </Col>
                  <Col style={styles.sectionButton}>
                    <Text style={styles.detailPrices}>
                      IDR{' '}
                      {Numeral(props.insurancePrice)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            )}
          </Row>
        </Grid>
      )}
      {/* =============================== PRICE ONE WAY ===================================== */}

      {/* =============================== PRICE RETURN ===================================== */}
      {isReturn ? (
        <Grid style={{ flex: 0, marginTop: -5 }}>
          <Row>
            <Text style={styles.title}>Price Departure & Return</Text>
          </Row>
          <Row style={styles.section}>
            {returnData.price_adult > 0 && (
              <Grid style={styles.pt5}>
                <Row style={{ paddingBottom: 15 }}>
                  <Col size={1} style={styles.pl5}>
                    <Image
                      source={require('../../../assets/icons/flight_path.png')}
                      style={{
                        width: 25,
                        height: 30,
                        tintColor: thameColors.primary,
                      }}
                      resizeMode="center"
                    />
                  </Col>
                  <Col size={3} style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Adult x{props.flight.adults}{' '}
                    </Text>
                  </Col>
                  <Col size={6} style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(departureData.price_adult)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
                <Row style={styles.sectionHr} />
                <Row style={{ paddingTop: 15 }}>
                  <Col size={1} style={styles.pl5}>
                    <Image
                      source={require('../../../assets/icons/icon_flight2.png')}
                      style={{
                        width: 25,
                        height: 30,
                        tintColor: thameColors.primary,
                      }}
                      resizeMode="center"
                    />
                  </Col>
                  <Col size={3} style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Adult x{props.flight.adults}{' '}
                    </Text>
                  </Col>
                  <Col size={6} style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(returnData.price_adult)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            )}

            {(returnData.price_child > 0) & (props.flight.childs > 0) ? (
              <Grid style={styles.pt5}>
                <Row style={styles.sectionHr} />
                <Row style={{ paddingBottom: 15, paddingTop: 15 }}>
                  <Col size={1} style={styles.pl5}>
                    <Image
                      source={require('../../../assets/icons/flight_path.png')}
                      style={{
                        width: 25,
                        height: 30,
                        tintColor: thameColors.primary,
                      }}
                      resizeMode="center"
                    />
                  </Col>
                  <Col size={3} style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Child x{props.flight.childs}{' '}
                    </Text>
                  </Col>
                  <Col size={6} style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(departureData.price_child)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
                <Row style={styles.sectionHr} />
                <Row style={{ paddingTop: 15 }}>
                  <Col size={1} style={styles.pl5}>
                    <Image
                      source={require('../../../assets/icons/icon_flight2.png')}
                      style={{
                        width: 25,
                        height: 30,
                        tintColor: thameColors.primary,
                      }}
                      resizeMode="center"
                    />
                  </Col>
                  <Col size={3} style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Child x{props.flight.childs}{' '}
                    </Text>
                  </Col>
                  <Col size={6} style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(returnData.price_child)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            ) : null}
            {(returnData.price_infant > 0) & (props.flight.infants > 0) ? (
              <Grid style={styles.pt5}>
                <Row style={styles.sectionHr} />
                <Row style={{ paddingBottom: 15, paddingTop: 15 }}>
                  <Col size={1} style={styles.pl5}>
                    <Image
                      source={require('../../../assets/icons/flight_path.png')}
                      style={{
                        width: 25,
                        height: 30,
                        tintColor: thameColors.primary,
                      }}
                      resizeMode="center"
                    />
                  </Col>
                  <Col size={3} style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Infant x{props.flight.infants}{' '}
                    </Text>
                  </Col>
                  <Col size={6} style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(departureData.price_infant)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
                <Row style={styles.sectionHr} />
                <Row style={{ paddingTop: 15 }}>
                  <Col size={1} style={styles.pl5}>
                    <Image
                      source={require('../../../assets/icons/icon_flight2.png')}
                      style={{
                        width: 25,
                        height: 30,
                        tintColor: thameColors.primary,
                      }}
                      resizeMode="center"
                    />
                  </Col>
                  <Col size={3} style={styles.sectionName}>
                    <Text style={styles.contactName}>
                      Infant x{props.flight.infants}{' '}
                    </Text>
                  </Col>
                  <Col size={6} style={styles.sectionButton}>
                    <Text style={styles.contactName}>
                      IDR{' '}
                      {Numeral(returnData.price_infant)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            ) : null}

            {props.insurancePrice > 0 && (
              <Grid style={styles.pt5}>
                <Row>
                  <Col style={styles.sectionName}>
                    <Text style={styles.contactName}>Insurance</Text>
                  </Col>
                  <Col style={styles.sectionButton}>
                    <Text style={styles.detailPrices}>
                      IDR{' '}
                      {Numeral(props.insurancePrice)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            )}
          </Row>
          <Row style={{ marginTop: 20 }}>
            <Text style={styles.title}>Total Payment</Text>
          </Row>
          <Row style={styles.section}>
            <Grid>
              {/* <Row style={{paddingBottom: 15, paddingTop: 15}}>
                                <Col style={styles.sectionName}>
                                    <Text style={styles.contactName}>Baggage Charge</Text>
                                </Col>
                                <Col style={styles.sectionButton}>
                                    <Text style={styles.contactName}>
                                        Rp. 0
                                    </Text>
                                </Col>
                            </Row> */}
              {/* <Row style={styles.sectionHr} /> */}
              <Row style={{ paddingBottom: 5, paddingTop: 5 }}>
                <Col style={styles.sectionName}>
                  <Text style={[styles.contactName, { fontFamily: fontBold }]}>
                    Price You Pay
                  </Text>
                </Col>
                <Col style={styles.sectionButton}>
                  <Text style={styles.totalPrices}>
                    IDR{' '}
                    {Numeral(totalPrice)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Row>
            </Grid>
          </Row>
        </Grid>
      ) : (
        <Grid style={{ flex: 0 }} />
      )}
      {/* =============================== PRICE RETURN ===================================== */}
    </View>
  );
};

export default Prices;
