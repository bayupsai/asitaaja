import { StyleSheet, Dimensions } from 'react-native';
import {
  fontExtraBold,
  fontReguler,
  thameColors,
} from '../../../../base/constant';

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 0 },
  title: {
    color: thameColors.textBlack,
    fontFamily: fontExtraBold,
    fontSize: 16,
  },
  titleContactPerson: {
    color: thameColors.lightBrown,
    fontFamily: fontReguler,
    fontSize: 16,
  },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 15,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center', paddingLeft: 5 },
  contactName: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontReguler,
  },
  contactNamePlaceholder: {
    fontSize: 16,
    color: thameColors.darkGray,
    fontFamily: fontReguler,
  },
  titleChecklist: {
    fontSize: 14,
    color: thameColors.grayBrown,
    fontFamily: fontReguler,
  },
  sectionButton: { alignItems: 'flex-end' },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 2.25,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalSeatClassAdult: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalDob: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

export default styles;
