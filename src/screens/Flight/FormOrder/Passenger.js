/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
// @flow
import React from 'react';
import {
  Text,
  Dimensions,
  View,
  TouchableOpacity,
  Switch,
  FlatList,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import { ButtonPlus } from '../../../elements/ButtonPlus';
import { InputText, InputDate } from '../../../elements/TextInput';
import { ButtonRounded } from '../../../elements/Button';
import { actionSetPassenger } from '../../../redux/actions/FlightAction';
import { getFirstNameLastname } from '../../../utilities/helpers';
import { fontReguler, thameColors, fontBold } from '../../../base/constant';
import AlertModal from '../../../components/AlertModal';
import {
  makeContactDetail,
  makeAllFLight,
  makePassengerData,
} from '../../../redux/selectors/FlightSelector';
import styles from './Component/passengerStyle';

const window = Dimensions.get('window');

// Static Type
type Props = {
  flight: Object,
  passenger: any,
  contact: Object,
  dispatch: Function,
};

type State = {
  visibleModal: any,
  errorMessage: string,
  titleSelected: number,
  listTitle: any,
  listTitleChildInfant: any,
  title: string,
  fullName: string,
  countryCode: string,
  phoneNumber: string,
  emailAddress: string,
  adults: number,
  childs: number,
  infants: number,
  dataPassenger: Object,
  payloadPassengers: any,
  passengersAdults: any,
  passengersChilds: any,
  passengersInfants: any,
  modalActiveForm: string,
  modalActiveIndex: any,
  modalActiveData: ?string,
  dobDay: number,
  dobMonth: any,
  dobYear: number,
  sameContactPerson: boolean,
  birth_date: string,
  dataMonth: any,
  dataDate: any,
  dataChildYear: any,
  dataInfantYear: any,
  dataYear: any,
};

class Passenger extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      errorMessage: '',
      titleSelected: 2,
      listTitle: [
        { index: 1, title: 'MR' },
        { index: 2, title: 'MRS' },
        { index: 3, title: 'MS' },
      ],
      listTitleChildInfant: [
        { index: 4, title: 'MSTR' },
        { index: 5, title: 'MISS' },
      ],
      title: 'MR',
      fullName: '',
      countryCode: '+62',
      phoneNumber: '',
      emailAddress: '',
      adults: this.props.flight.adults,
      childs: this.props.flight.childs,
      infants: this.props.flight.infants,
      dataPassenger: {
        adults: [],
        children: [],
        infants: [],
      },
      payloadPassengers: [],
      passengersAdults: [],
      passengersChilds: [],
      passengersInfants: [],
      modalActiveForm: '',
      modalActiveIndex: '',
      modalActiveData: '',
      dobDay: 1,
      dobMonth: 1,
      dobYear: 2008,
      sameContactPerson: false,
      birth_date: '',
      dataMonth: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ],
      dataDate: new Array(30),
      dataChildYear: [],
      dataInfantYear: [],
      dataYear: [],
    };
  }

  componentDidMount() {
    if (this.state.adults > 0) {
      this.generateStatePassenger(this.state.adults, 'adults');
    }
    if (this.state.childs > 0) {
      this.generateStatePassenger(this.state.childs, 'children');
    }
    if (this.state.infants > 0) {
      this.generateStatePassenger(this.state.infants, 'infants');
    }
    this.getPassenger();
    this._functionDate();
  }

  getPassenger = () => {
    this.setState({
      modalActiveForm: 'adults',
      modalActiveIndex: 0,
      modalActiveData: null,
    });
    this.props.passenger
      ? this.setState({
          fullName: this.props.passenger[0]
            ? `${this.props.passenger[0].first_name} ${this.props.passenger[0].last_name}`
            : '',
        })
      : null;
  };

  // ================== Function Date ================
  _functionDate = () => {
    const { dataDate, dataChildYear, dataInfantYear, dataYear } = this.state;
    const nowYear = new Date().getFullYear();
    for (let i = 1; i <= 31; i++) {
      dataDate.push(i);
    }
    for (let i = nowYear; i >= nowYear - 12; i--) {
      dataChildYear.push(i);
    }
    for (let i = nowYear; i >= nowYear - 2; i--) {
      dataInfantYear.push(i);
    }
    for (let i = nowYear; i >= nowYear - 75; i--) {
      dataYear.push(i);
    }
    this.setState({ dataDate, dataChildYear, dataInfantYear });
  };
  // ================== Function Date ================

  // Modal
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  _resetModal = () => {
    this.setState({ visibleModal: null });
  };

  _setModal1 = () => {
    this.setState({ visibleModal: 1 });
  };

  _setModal2 = () => {
    this.setState({ visibleModal: 2 });
  };

  _setModal3 = () => {
    this.setState({ visibleModal: 3 });
  };

  _alertModal = idModal => {
    let textMessage = '';
    if (idModal === 99) {
      textMessage = 'Please enter data correctly';
    } else if (idModal === 999) {
      textMessage = 'Make sure you already fill your contact details';
    }
    return (
      <AlertModal
        isVisible={this.state.visibleModal === idModal}
        type="normal"
        title="Warning"
        contentText={textMessage}
        onPress={this._resetModal}
        onDismiss={this._resetModal}
        labelCancel=""
        labelOk=""
        titleColor=""
      />
    );
  };
  // Modal

  // Flatlist Conf
  _keyExtractor = (item, index) => index.toString();

  _renderSalutation = ({ item, index }) => (
    <Grid
      key={index}
      style={{
        flex: 1,
        backgroundColor: thameColors.white,
        padding: 10,
        borderBottomColor: thameColors.gray,
        borderBottomWidth: 0.5,
      }}
    >
      <TouchableOpacity
        onPress={() => this.setTitlePassenger(item)}
        style={{ flex: 1 }}
      >
        <Col style={{ alignItems: 'flex-start' }}>
          <Text
            style={{
              fontFamily: fontReguler,
              color: thameColors.superBack,
              fontSize: 16,
            }}
          >
            {item.title}
          </Text>
        </Col>
        <Col style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
          <Icon
            name="check"
            type="feather"
            color={thameColors.semiGreen}
            size={22}
          />
        </Col>
      </TouchableOpacity>
    </Grid>
  );
  // Flatlist Conf

  setTitlePassenger = data => {
    const { dataPassenger, modalActiveForm, modalActiveIndex } = this.state;
    const field = 'title';
    const fieldIndex = 'titleIndex';
    if (dataPassenger[modalActiveForm]) {
      dataPassenger[modalActiveForm][modalActiveIndex][field] = data.title;
      dataPassenger[modalActiveForm][modalActiveIndex][fieldIndex] = data.index;
    }
    this.setState({ dataPassenger, visibleModal: 1 });
  };

  handleInput = (type, data) => {
    const { dataPassenger, modalActiveForm, modalActiveIndex } = this.state;
    if (type === 'fullName') {
      const field = 'fullName';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
      }
    } else if (type === 'dob') {
      const field = 'dateOfBirth';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
      }
    } else if (type === 'sameContact') {
      const field = 'fullName';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
        this.generatePayloadPassengers('adult', 'adults');
      }
      this.setState({ visibleModal: null });
    } else if (type === 'blank') {
      const field = 'fullName';
      if (dataPassenger[modalActiveForm]) {
        dataPassenger[modalActiveForm][modalActiveIndex][field] = data;
      }
      this.generatePayloadPassengers('adult', 'adults');
      this.setState({ visibleModal: 1 });
    }
    this.setState({ dataPassenger });
  };

  // Date Of Birth Function
  setDob = (type, value) => {
    if (type === 'day') {
      this.setState({ dobDay: value });
    } else if (type === 'month') {
      this.setState({ dobMonth: value });
    } else if (type === 'year') {
      this.setState({ dobYear: value });
    }
  };

  doneDob = () => {
    let month = '01';
    let dateOfBirth = '';
    if (this.state.dobMonth === 'January') {
      month = '01';
    } else if (this.state.dobMonth === 'February') {
      month = '02';
    } else if (this.state.dobMonth === 'March') {
      month = '03';
    } else if (this.state.dobMonth === 'April') {
      month = '04';
    } else if (this.state.dobMonth === 'May') {
      month = '05';
    } else if (this.state.dobMonth === 'June') {
      month = '06';
    } else if (this.state.dobMonth === 'July') {
      month = '07';
    } else if (this.state.dobMonth === 'August') {
      month = '08';
    } else if (this.state.dobMonth === 'September') {
      month = '09';
    } else if (this.state.dobMonth === 'October') {
      month = '10';
    } else if (this.state.dobMonth === 'November') {
      month = '11';
    } else if (this.state.dobMonth === 'December') {
      month = '12';
    }
    const day =
      this.state.dobDay < 10 ? `0${this.state.dobDay}` : this.state.dobDay;
    dateOfBirth = `${this.state.dobYear}-${month}-${day}`;
    this.setState({ visibleModal: 1 });
    this.handleInput('dob', dateOfBirth);
  };
  // Date Of Birth Function

  generateStatePassenger(total, field) {
    const { dataPassenger } = this.state;
    for (let i = 0; i < total; i++) {
      dataPassenger[field].push({
        titleIndex: 1,
        title: field === 'adults' ? 'MR' : 'MSTR',
        fullName: '',
        // nationality:'',
        dateOfBirth: '',
      });
    }
    this.setState({ dataPassenger });
  }

  renderFormPassenger(form, titleForm) {
    const { dataPassenger } = this.state;
    if (!dataPassenger[form]) {
      return null;
    }
    return (
      <View>
        {dataPassenger[form].map((data, index) => {
          return (
            <TouchableOpacity
              onPress={() => this.openModalPassenger(form, index, null)}
              key={index}
            >
              <Row style={styles.section}>
                <Col style={styles.sectionName}>
                  <Text style={styles.contactName}>
                    {dataPassenger[form][index].fullName ? (
                      `${dataPassenger[form][index].title} ${dataPassenger[form][index].fullName}`
                    ) : (
                      <Text style={styles.contactName}>
                        {`${titleForm} x${parseInt(index + 1)}`}
                      </Text>
                    )}
                  </Text>
                </Col>
                <Col style={styles.sectionButton}>
                  <ButtonPlus
                    size="small"
                    colorButton="buttonColor"
                    onClick={() => this.openModalPassenger(form, index, null)}
                  />
                </Col>
              </Row>
              {// dataPassenger[form][index].nationality == "" ||
              // ||  dataPassenger[form][index].dateOfBirth == ""
              dataPassenger[form][index].fullName === '' ? (
                <Row>
                  <Col>
                    <Text
                      style={{
                        color: thameColors.red,
                        fontSize: 14,
                        fontFamily: fontReguler,
                        fontStyle: 'italic',
                      }}
                    >
                      *Please fill in passenger details.
                    </Text>
                  </Col>
                </Row>
              ) : (
                <Text />
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }

  openModalPassenger = (form, index, currentData) => {
    this.setState({
      visibleModal: 1,
      modalActiveForm: form,
      modalActiveIndex: index,
      modalActiveData: currentData,
    });
  };

  generatePayloadPassengers = (passengerType, objectField) => {
    const { dataPassenger } = this.state;
    let payloadPass = [];
    let validDob = true;
    const tempArr = [];
    dataPassenger[objectField].map((data, index) => {
      const tempObj = {};
      if (data.fullName !== '') {
        if (objectField !== 'adults' && data.dateOfBirth === '') {
          validDob = false;
          this.setState({ visibleModal: 99 });
        } else {
          validDob = true;
          let primaryData = false;
          if (objectField === 'adults' && index === 0) {
            primaryData = true;
          }

          getFirstNameLastname(data.fullName, res => {
            (tempObj.first_name = res.firstName),
              (tempObj.last_name = res.lastName);
          });
          (tempObj.email = this.props.contact
            ? this.props.contact.emailAddress
            : 'mandatory@abc.xyz'),
            (tempObj.phone = this.props.contact
              ? `${this.props.contact.phoneNumber}`
              : '0855101010101'),
            (tempObj.birth_date =
              objectField === 'adults' ? '1988-04-08' : this.state.birth_date),
            (tempObj.primary = primaryData),
            (tempObj.salutation = data.title),
            (tempObj.type = passengerType),
            (tempObj.seat = ''),
            (tempObj.nationality = 'ID'),
            (tempObj.card_number = '123232323'),
            (tempObj.card_issue_date = '2017-01-01'),
            (tempObj.card_expire_date = '2022-01-01'),
            (tempObj.luggage = 1);
          tempArr.push(tempObj);
        }
      }
    });

    if (objectField === 'adults') {
      this.setState({ passengersAdults: tempArr });
    } else if (objectField === 'children') {
      this.setState({ passengersChilds: tempArr });
    } else if (objectField === 'infants') {
      this.setState({ passengersInfants: tempArr });
    }

    setTimeout(() => {
      payloadPass = this.state.passengersAdults
        .concat(this.state.passengersChilds)
        .concat(this.state.passengersInfants);
      if (validDob) {
        this.setState(
          {
            payloadPassengers: payloadPass,
            visibleModal: null,
            birth_date: '',
          },
          () => {
            this.props.dispatch(actionSetPassenger(payloadPass));
            // this.props.onChangeData()
          }
        );
      }
    }, 100);
  };

  savePassengers = () => {
    const { modalActiveForm } = this.state;
    let typePassenger = 'adult';
    let typeFiled = 'adults';
    if (modalActiveForm === 'children') {
      typePassenger = 'child';
      typeFiled = 'children';
    } else if (modalActiveForm === 'infants') {
      typePassenger = 'infant';
      typeFiled = 'infants';
    }
    this.generatePayloadPassengers(typePassenger, typeFiled);
  };

  toggleContactPerson = value => {
    if (this.props.contact.fullName) {
      this.setState(
        {
          sameContactPerson: value,
          modalActiveForm: 'adults',
          modalActiveIndex: 0,
          modalActiveData: null,
        },
        value
          ? this.handleInput('sameContact', this.props.contact.fullName)
          : this.handleInput('blank', '')
      );
    } else {
      this.setState({ visibleModal: 999, sameContactPerson: false });
    }
  };

  // Main Render
  render() {
    const { dataPassenger, modalActiveForm, modalActiveIndex } = this.state;
    const title = 'title';
    const fullname = 'fullName';
    // const nationality = "nationality"
    const dateOfBirth = 'dateOfBirth';
    let dataSalutation = [];
    let defaultSalutation = 'MR';
    let defaultYear = [];
    let currentPopupPassengersTitle = '';
    let currentPopupPassengersFullname = '';
    // let currentPopupPassengersNationality = ""
    let currentPopupPassengersDateOfBirth = '';

    if (modalActiveForm) {
      // currentPopupPassengers = dataPassenger[modalActiveForm][modalActiveIndex][titleIndex]
      currentPopupPassengersTitle =
        dataPassenger[modalActiveForm][modalActiveIndex][title];
      currentPopupPassengersFullname =
        dataPassenger[modalActiveForm][modalActiveIndex][fullname];
      // currentPopupPassengersNationality = dataPassenger[modalActiveForm][modalActiveIndex][nationality]
      currentPopupPassengersDateOfBirth =
        dataPassenger[modalActiveForm][modalActiveIndex][dateOfBirth];
    }

    if (modalActiveForm === 'children') {
      defaultYear = this.state.dataChildYear;
    } else if (modalActiveForm === 'infants') {
      defaultYear = this.state.dataInfantYear;
    }

    if (modalActiveForm !== 'adults') {
      dataSalutation = this.state.listTitleChildInfant;
      defaultSalutation = 'MSTR';
    } else {
      dataSalutation = this.state.listTitle;
      defaultSalutation = 'MR';
    }

    return (
      <Grid style={styles.container}>
        <Row>
          <Text style={styles.title}>Passenger Details</Text>
        </Row>
        <Row style={{ paddingTop: 10, marginBottom: -5 }}>
          <Col>
            <Text style={styles.titleContactPerson}>
              Same as contact detail
            </Text>
          </Col>
          <Col>
            <Switch
              onValueChange={this.toggleContactPerson}
              value={this.state.sameContactPerson}
            />
          </Col>
        </Row>
        {this._alertModal(99)}
        {this._alertModal(999)}

        {this.renderFormPassenger('adults', 'Adult')}
        {this.renderFormPassenger('children', 'Child')}
        {this.renderFormPassenger('infants', 'Infant')}

        {/* =============================== MODAL FORM ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={this._resetModal}
          onBackdropPress={this._resetModal}
          style={styles.bottomModal}
        >
          <View
            style={
              this.state.modalActiveForm === 'adults'
                ? styles.modalSeatClassAdult
                : styles.modalSeatClass
            }
          >
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.buttonGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._resetModal}
              >
                <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    {this.state.modalActiveForm === 'adults'
                      ? `ADULT ${parseInt(this.state.modalActiveIndex) + 1}`
                      : ''}
                    {this.state.modalActiveForm === 'children'
                      ? `CHILD ${parseInt(this.state.modalActiveIndex) + 1}`
                      : ''}
                    {this.state.modalActiveForm === 'infants'
                      ? `INFANT ${parseInt(this.state.modalActiveIndex) + 1}`
                      : ''}
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: thameColors.white }}>
                <Grid style={{ paddingTop: 20 }}>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col size={3}>
                      <TouchableOpacity onPress={this._setModal2}>
                        <InputText
                          size="small"
                          placeholder={
                            currentPopupPassengersTitle !== ''
                              ? currentPopupPassengersTitle
                              : defaultSalutation
                          }
                          label="Title"
                          required="*"
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col size={7} style={{ marginLeft: 0 }}>
                      <InputText
                        placeholder={currentPopupPassengersFullname}
                        label="Fullname"
                        required="*"
                        editable
                        onChangeText={data =>
                          this.handleInput('fullName', data)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col>
                      {this.state.modalActiveForm === 'infants' ||
                      this.state.modalActiveForm === 'children' ? (
                        <TouchableOpacity onPress={this._setModal3}>
                          <InputDate
                            placeholder={currentPopupPassengersDateOfBirth}
                            label="Birth Date (yyyy-mm-dd)"
                            required="*"
                            editable={false}
                            value={this.state.birth_date}
                            onChangeText={data => this.handleInput('dob', data)}
                          />
                        </TouchableOpacity>
                      ) : null}
                      {/* <TouchableOpacity onPress={this._setModal3}>
                              <InputText
                                placeholder={currentPopupPassengersDateOfBirth}
                                value={currentPopupPassengersDateOfBirth}
                                label="Birth Date"
                                required="*"
                                editable={false}
                              />
                            </TouchableOpacity> */}
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 10 }}>
                    <Col size={6} style={styles.sectionButtonArea}>
                      <ButtonRounded
                        label="SUBMIT"
                        onClick={this.savePassengers}
                      />
                    </Col>
                  </Row>
                </Grid>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL FORM ===================================== */}

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 2}
          onBackButtonPress={this._setModal1}
          onBackdropPress={this._setModal1}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._resetModal}
              >
                <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Choose Title
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ paddingLeft: 10 }}>
                <FlatList
                  keyExtractor={this._keyExtractor}
                  data={dataSalutation}
                  getItemLayout={(item, index) => ({
                    length: 5,
                    offset: 5 * index,
                    index,
                  })}
                  removeClippedSubviews
                  renderItem={this._renderSalutation}
                />
                {/* {
                                    dataSalutation.map((data, index) => {

                                        return (
                                            <Grid key={index} style={{ flex: 1, backgroundColor: thameColors.white, padding: 10, borderBottomColor: thameColors.gray, borderBottomWidth: 0.5 }}>
                                                <TouchableOpacity onPress={() => this.setTitlePassenger(data)} style={{ flex: 1 }}>
                                                    <Col style={{ alignItems: "flex-start" }}>
                                                        <Text style={{ fontFamily: fontReguler, color: thameColors.superBack, fontSize: 16 }}>{data.title}</Text>
                                                    </Col>
                                                    <Col style={{ alignItems: "flex-end", justifyContent: "center" }}>
                                                        {
                                                            (currentPopupPassengers != "" && currentPopupPassengers == data.index) ? <Icon name='check' type='feather' color={thameColors.semiGreen} size={22} /> : <Text></Text>
                                                        }
                                                    </Col>
                                                </TouchableOpacity>
                                            </Grid>
                                        )
                                    })
                                } */}
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}

        {/* =============================== MODAL PASSENGER ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 3}
          onBackButtonPress={this._setModal1}
          onBackdropPress={this._setModal1}
          style={styles.bottomModal}
        >
          <View style={styles.modalDob}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._setModal1}
              >
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Date Of Birth
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: thameColors.textBlack,
                      fontSize: 16,
                    }}
                  >
                    Day
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={[
                      1,
                      2,
                      3,
                      4,
                      5,
                      6,
                      7,
                      8,
                      9,
                      10,
                      11,
                      12,
                      13,
                      14,
                      15,
                      16,
                      17,
                      18,
                      19,
                      20,
                      21,
                      22,
                      23,
                      24,
                      25,
                      26,
                      27,
                      28,
                      29,
                      30,
                      31,
                    ]}
                    selectedIndex={1}
                    onValueChange={data => this.setDob('day', data)}
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.buttonGray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: thameColors.textBlack,
                      fontSize: 16,
                    }}
                  >
                    Month
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={[
                      'January',
                      'February',
                      'March',
                      'April',
                      'May',
                      'June',
                      'July',
                      'August',
                      'September',
                      'October',
                      'November',
                      'December',
                    ]}
                    selectedIndex={1}
                    renderItem={(data, index, isSelected) => {}}
                    onValueChange={(data, selectedIndex) =>
                      this.setDob('month', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.buttonGray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
              <Col style={{ alignItems: 'center' }}>
                <View>
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: thameColors.textBlack,
                      fontSize: 16,
                    }}
                  >
                    Year
                  </Text>
                </View>
                <View>
                  <ScrollPicker
                    style={{ fontSize: 10 }}
                    dataSource={defaultYear}
                    selectedIndex={1}
                    renderItem={(data, index, isSelected) => {}}
                    onValueChange={(data, selectedIndex) =>
                      this.setDob('year', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.buttonGray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
            </Grid>
            <Grid style={{ flex: 0, paddingBottom: 10 }}>
              <Col style={styles.sectionButtonArea}>
                <ButtonRounded label="SUBMIT" onClick={this.doneDob} />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL PASSENGER ===================================== */}
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    flight: makeAllFLight(state),
    contact: makeContactDetail(state),
    passenger: makePassengerData(state),
  };
}
export default connect(mapStateToProps)(Passenger);
