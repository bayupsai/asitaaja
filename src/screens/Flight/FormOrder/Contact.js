import React from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  InteractionManager,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { ButtonPlus } from '../../../elements/ButtonPlus';
import { ButtonLogin, ButtonRounded } from '../../../elements/Button';
import { InputText, SearchInput } from '../../../elements/TextInput';
import { connect } from 'react-redux';
import { actionContactDetail } from '../../../redux/actions/FlightAction';
import Modal from 'react-native-modal';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  thameColors,
  asitaColor,
} from '../../../base/constant';
import {
  validateEmailFormat,
  validatePhoneNumber,
} from '../../../utilities/helpers';
import AlertModal from '../../../components/AlertModal';
import LoginModal from '../../../components/LoginModal';
import SubHeaderPage from '../../../components/SubHeaderPage';
import _ from 'lodash';
import { actionListCountry } from '../../../redux/actions/ListCountryAction';
import { makeDataProfile } from '../../../redux/selectors/ProfileSelector';
import { makeListCountry } from '../../../redux/selectors/ListCountrySelector';

const window = Dimensions.get('window');
const styles = StyleSheet.create({
  container: { padding: 20, paddingTop: 15, marginTop: -35 },
  title: {
    color: thameColors.textBlack,
    fontFamily: fontExtraBold,
    fontSize: 16,
    paddingLeft: 5,
  },
  section: {
    marginTop: 10,
    backgroundColor: thameColors.white,
    padding: 15,
    borderRadius: 5,
  },
  sectionName: { justifyContent: 'center', paddingLeft: 5 },
  contactName: { fontSize: 16, color: thameColors.superBack },
  sectionButton: { alignItems: 'flex-end' },
  bottomModal: { justifyContent: 'flex-end', margin: 0 },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 1.6,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: window.height / 2.9,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: window.height,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 45,
    width: window.width,
  },
  modalSearch: {
    flex: 0,
    height: 70,
    width: window.width,
    marginTop: -35,
    marginBottom: 10,
  },
  modalTitleSection: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalTitle: {
    color: thameColors.white,
    fontFamily: fontBold,
    fontSize: 18,
  },
});

class Contact extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      titleSelected: 1,
      listTitle: [
        { index: 1, title: 'MR' },
        { index: 2, title: 'MRS' },
        { index: 3, title: 'MS' },
        { index: 4, title: 'MSTR' },
      ],
      title: 'MR',
      fullName: '',
      countryCode: '62',
      phoneNumber: '',
      emailAddress: '',
      isValidEmail: true,
      isValidPhone: true,
      country: [],

      errorMessage: '',
    };
  }

  componentDidMount() {
    this.handleLogin();
    this._checkCountry();
  }

  // Modal
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };
  _setNullModal = () => {
    this.setState({ visibleModal: null });
  };
  _setModal1 = () => {
    this.setState({ visibleModal: 1 });
  };
  _setModal2 = () => {
    this.setState({ visibleModal: 2 });
  };
  _setModal3 = () => {
    this.setState({ visibleModal: 3 });
  };
  //Modal

  // Flatlist Conf
  _keyExtractor = (item, index) => index.toString();
  _renderListCountry = ({ item, index }) => (
    <Grid
      key={index}
      style={{ flex: 1, backgroundColor: thameColors.white, padding: 10 }}
    >
      <TouchableOpacity
        style={{ flexDirection: 'row', flex: 1 }}
        onPress={() => this.setCountryCode(item.phonecode)}
      >
        <Col style={{ alignItems: 'flex-start' }}>
          <Text
            style={{
              fontFamily: fontBold,
              color: thameColors.textBlack,
              fontSize: 16,
            }}
          >
            {item.nicename}
          </Text>
          <Text
            style={{ fontFamily: fontReguler, color: thameColors.darkGray }}
          >
            {item.iso}
          </Text>
        </Col>
        <Col style={{ alignItems: 'flex-end' }}>
          <View
            style={{
              backgroundColor: thameColors.semiGray,
              width: 50,
              height: 25,
              alignItems: 'flex-end',
              justifyContent: 'center',
              borderRadius: 5,
            }}
          >
            <Text
              style={{
                fontFamily: fontReguler,
                color: thameColors.darkGray,
                fontSize: 12,
                alignSelf: 'center',
              }}
            >
              +{item.phonecode}
            </Text>
          </View>
        </Col>
      </TouchableOpacity>
    </Grid>
  );
  _renderListTitle = ({ data, index }) => (
    <Grid
      key={index}
      style={{
        flex: 1,
        backgroundColor: thameColors.white,
        padding: 10,
        borderBottomColor: thameColors.buttonGray,
        borderBottomWidth: 0.5,
      }}
    >
      <TouchableOpacity
        onPress={() => this.setTitlePassenger(data)}
        style={{ flex: 1 }}
      >
        <Col style={{ alignItems: 'flex-start' }}>
          <Text
            style={{
              fontFamily: fontReguler,
              color: thameColors.superBack,
              fontSize: 16,
            }}
          >
            {data.title}
          </Text>
        </Col>
        <Col style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
          {this.state.titleSelected == data.index ? (
            <Icon
              name="check"
              type="feather"
              color={thameColors.semiGreen}
              size={22}
            />
          ) : (
            <Text></Text>
          )}
        </Col>
      </TouchableOpacity>
    </Grid>
  );
  // Flatlist Conf

  //Check Data Country
  _checkCountry = () => {
    let { dispatch } = this.props;
    InteractionManager.runAfterInteractions(() => {
      dispatch(actionListCountry(1000))
        .then(res => {
          if (res.type == 'LIST_COUNTRY_SUCCESS') {
          }
        })
        .catch(err => {
          this.setState({ errorMessage: err.message });
        });
    });
  };

  // Login
  handleLogin = () => {
    let { fullname, mobileNo, email } = this.props.dataProfile;
    this.setState(
      { fullName: fullname, phoneNumber: mobileNo, emailAddress: email },
      () => this.informationPassengers()
    );
  };
  _onDismissLogin = () =>
    this.setState({ visibleModal: null }, () => this.informationPassengers);
  _goRegister = () =>
    this.setState({ visibleModal: null }, () =>
      this.props.navigation.navigate('SignUp')
    );
  // Login

  setTitlePassenger = data => {
    this.setState({
      title: data.title,
      titleSelected: data.index,
      visibleModal: 1,
    });
  };
  setCountryCode = code => {
    this.setState({ countryCode: code, visibleModal: 1 });
  };
  handleInput = (type, data) => {
    if (type == 'fullName') {
      this.setState({ fullName: data });
    } else if (type == 'phoneNumber') {
      let checkPhone = validatePhoneNumber(data);
      this.setState({
        phoneNumber: checkPhone === '0' ? '' : data,
        isValidPhone: checkPhone === '0' ? false : true,
      });
    } else if (type == 'emailAddress') {
      let checkEmail = validateEmailFormat(data);
      this.setState({
        emailAddress: checkEmail ? data : '',
        isValidEmail: checkEmail ? true : false,
      });
    }
  };

  informationPassengers = () => {
    if (this.state.fullName == '' || this.state.emailAddress == '') {
      this.setState({ visibleModal: 999 });
    } else {
      const payload = {
        title: this.state.title,
        fullName: this.state.fullName,
        // countryCode: this.state.countryCode,
        phoneNumber: `${this.state.countryCode}${this.state.phoneNumber}`,
        emailAddress: this.state.emailAddress,
      };
      this.props.dispatch(actionContactDetail(payload));
      this.setState({ visibleModal: null });
    }
  };

  //Main Render
  render() {
    return (
      <Grid style={styles.container}>
        {/* ===================== loginButton ===================== */}
        {this.props.isLogin === false ? (
          <ButtonLogin onPress={() => this._toggleModal(99999)} />
        ) : (
          <View style={{ width: 0, height: 0 }} />
        )}
        {/* ===================== loginButton ===================== */}

        <Row style={{ marginTop: 15 }}>
          <Text style={styles.title}>Contact Details</Text>
        </Row>
        {/* Contact Details */}
        <TouchableOpacity onPress={this._setModal1} style={styles.section}>
          <Grid>
            <Col style={styles.sectionName}>
              <Text style={[styles.contactName]}>
                {this.state.fullName ? (
                  this.state.title + '. ' + this.state.fullName
                ) : (
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontReguler,
                    }}
                  >
                    Contact Person
                  </Text>
                )}
              </Text>
            </Col>
            <Col style={styles.sectionButton}>
              <ButtonPlus
                size="small"
                colorButton="buttonColor"
                onClick={this._setModal1}
              />
            </Col>
          </Grid>
        </TouchableOpacity>
        {this.state.fullName == '' ||
        this.state.phoneNumber == '' ||
        this.state.emailAddress == '' ? (
          <Row>
            <Text
              style={{
                color: thameColors.red,
                fontSize: 13,
                fontFamily: fontExtraBold,
              }}
            >
              *Please fill in contact details.
            </Text>
          </Row>
        ) : (
          <View></View>
        )}
        {/* //Contact Details */}

        {/* =============================== MODAL FORM ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._setNullModal}
              >
                <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Contact Details
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: thameColors.white }}>
                <Grid style={{ paddingTop: 20 }}>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col size={3}>
                      <TouchableOpacity onPress={this._setModal2}>
                        <InputText
                          size="small"
                          placeholder={this.state.title}
                          label="Title"
                          required="*"
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col size={7} style={{ marginLeft: 0 }}>
                      <InputText
                        label="Fullname"
                        required="*"
                        value={this.state.fullName}
                        editable={true}
                        onChangeText={data =>
                          this.handleInput('fullName', data)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col size={4}>
                      <TouchableOpacity onPress={this._setModal3}>
                        <InputText
                          label="Country Code"
                          required="*"
                          value={'+' + this.state.countryCode}
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col size={6}>
                      <InputText
                        label="Phone Number"
                        required="*"
                        value={this.state.phoneNumber}
                        editable={true}
                        maxLength={13}
                        keyboardType="number-pad"
                        onChangeText={data =>
                          this.handleInput('phoneNumber', data)
                        }
                      />
                      {this.state.isValidPhone == false ? (
                        <View>
                          <Text
                            style={{
                              fontFamily: fontBold,
                              color: 'red',
                              fontSize: 12,
                              paddingLeft: 20,
                            }}
                          >
                            Character not allowed
                          </Text>
                        </View>
                      ) : null}
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 20 }}>
                    <Col>
                      <View>
                        <InputText
                          label="Email Address"
                          required="*"
                          value={
                            this.state.emailAddress
                              ? this.state.emailAddress
                              : ''
                          }
                          editable={true}
                          keyboardType={'email-address'}
                          autoCapitalize={'none'}
                          onChangeText={data =>
                            this.handleInput('emailAddress', data)
                          }
                        />
                      </View>
                      {this.state.isValidEmail == false ? (
                        <View>
                          <Text
                            style={{
                              fontFamily: fontBold,
                              color: 'red',
                              fontSize: 12,
                              paddingLeft: 20,
                            }}
                          >
                            Please enter a valid email address
                          </Text>
                        </View>
                      ) : null}
                    </Col>
                  </Row>
                  <Row style={{ flex: 0, marginBottom: 10 }}>
                    <Col size={6} style={styles.sectionButtonArea}>
                      <ButtonRounded
                        label="SAVE CONTACT"
                        onClick={this.informationPassengers}
                      />
                    </Col>
                  </Row>
                </Grid>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL FORM ===================================== */}

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 2}
          onBackButtonPress={this._setModal1}
          onBackdropPress={this._setModal1}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: window.width,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: thameColors.darkGray,
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this._setNullModal}
              >
                {/* <Col size={2} style={{ alignItems: "flex-start", paddingLeft: 10 }}>
                                    <Icon name='close' type='evilIcon' color='#008195' size={20} />
                                </Col> */}
                <Col style={{ alignItems: 'center', alignItems: 'center' }}>
                  <Text
                    style={{
                      color: thameColors.textBlack,
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Choose Title
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ paddingLeft: 10 }}>
                {this.state.listTitle.map((data, index) => {
                  return (
                    <Grid
                      key={index}
                      style={{
                        flex: 1,
                        backgroundColor: thameColors.white,
                        padding: 10,
                        borderBottomColor: thameColors.buttonGray,
                        borderBottomWidth: 0.5,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setTitlePassenger(data)}
                        style={{ flex: 1 }}
                      >
                        <Col style={{ alignItems: 'flex-start' }}>
                          <Text
                            style={{
                              fontFamily: fontReguler,
                              color: thameColors.superBack,
                              fontSize: 16,
                            }}
                          >
                            {data.title}
                          </Text>
                        </Col>
                        <Col
                          style={{
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}
                        >
                          {this.state.titleSelected == data.index ? (
                            <Icon
                              name="check"
                              type="feather"
                              color={thameColors.semiGreen}
                              size={22}
                            />
                          ) : (
                            <Text></Text>
                          )}
                        </Col>
                      </TouchableOpacity>
                    </Grid>
                  );
                })}
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}

        {/* =============================== MODAL COuntry COde ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 3}
          style={styles.bottomModal}
          onBackButtonPress={this._setModal1}
          onBackdropPress={this._setModal1}
        >
          <View style={styles.modalContent}>
            <Grid style={{ flex: 0 }}>
              <Row style={[styles.modalHeader, { paddingTop: 15 }]}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={this._setModal1}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color={thameColors.white}
                      size={28}
                    />
                  </Col>
                  <Col size={6.5} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>Search Country Code</Text>
                  </Col>
                  <Col size={1.5} />
                </TouchableOpacity>
              </Row>
              <SubHeaderPage />
              {/* <Row style={styles.modalSearch}>
                                <Col>
                                    <SearchInput placeholder="Search your Country Code" onChangeText={(data) => this.searchingCode(data)} />
                                </Col>
                            </Row> */}
            </Grid>
            <Grid>
              <Col
                style={{
                  backgroundColor: thameColors.white,
                  padding: 20,
                  paddingTop: 10,
                  borderWidth: 0.5,
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  borderColor: thameColors.white,
                }}
              >
                <ScrollView showsVerticalScrollIndicator={false}>
                  <Grid>
                    <Col>
                      <FlatList
                        showsVerticalScrollIndicator={false}
                        keyExtractor={this._keyExtractor}
                        data={
                          this.state.country.length != 0
                            ? this.state.country
                            : this.props.listCountry
                        }
                        renderItem={this._renderListCountry}
                        ListEmptyComponent={
                          <View style={{ alignItems: 'center', marginTop: 10 }}>
                            <TouchableOpacity onPress={this._checkCountry}>
                              <Text
                                style={{
                                  fontFamily: fontBold,
                                  color: thameColors.textBlack,
                                  fontSize: 16,
                                }}
                              >
                                Could not find your Country Code
                              </Text>
                            </TouchableOpacity>
                          </View>
                        }
                      />
                    </Col>
                  </Grid>
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL COuntry COde ===================================== */}

        {/* =============================== MODAL ALERT ===================================== */}
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 999}
          title="Warning."
          contentText="Please enter data correctly."
          onPress={this._setModal1}
          onDismiss={this._setModal1}
        />
        {/* =============================== MODAL ALERT ===================================== */}

        {/* =============================== LOGIN MODAL ===================================== */}
        <LoginModal
          isVisible={this.state.visibleModal === 99999}
          onDismiss={this._onDismissLogin}
          goRegister={this._goRegister}
          callbackLogin={this.handleLogin}
        />
        {/* =============================== LOGIN MODAL ===================================== */}

        {/* Error MODAL */}
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 404}
          title="Alert"
          contentText={this.state.errorMessage}
          onPress={this._setNullModal}
          onDismiss={this._setNullModal}
        />
        {/* Error MODAL */}
      </Grid>
    );
  }
}
function mapStateToProps(state) {
  return {
    loading: state.login.fetchingLogin,
    isLogin: state.profile.isLogin,
    dataProfile: makeDataProfile(state),
    listCountry: makeListCountry(state),
  };
}
export default connect(mapStateToProps)(Contact);
