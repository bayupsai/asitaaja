import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import {
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../../base/constant';

const toMonth = moment(new Date())
  .format('MMM')
  .toUpperCase();

let dateDay = [
  {
    id: 0,
    day: moment(new Date())
      .subtract(3, 'days')
      .format('D ddd'),
  },
  { id: 1, day: moment(new Date()).format('D ddd') },
  {
    id: 2,
    day: moment(new Date())
      .add(1, 'days')
      .format('D ddd'),
  },
  {
    id: 3,
    day: moment(new Date())
      .add(2, 'days')
      .format('D ddd'),
  },
  {
    id: 4,
    day: moment(new Date())
      .add(3, 'days')
      .format('D ddd'),
  },
];

export default class Calendar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      buttonColor: thameColors.darkGray,
      selectedItems: [1],
    };
  }

  datingNow = () => {
    moment().format('MMMM Do YYYY, h:mm:ss a');
  };

  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <Text style={styles.month}>{toMonth}</Text>
          {dateDay.map((item, key) => (
            <TouchableOpacity
              onPress={() => this.setState({ selectedItems: item.id })}
              key={key}
            >
              <Text
                style={
                  this.state.selectedItems == item.id
                    ? styles.today
                    : styles.day
                }
              >
                {item.day}
              </Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sectionMont: {
    backgroundColor: thameColors.backWhite,
  },
  month: {
    color: thameColors.grayBrown,
    fontWeight: '500',
    transform: [{ rotate: '-90deg' }, { translateY: 15 }],
    fontSize: 14,
    paddingBottom: 15,
  },
  day: {
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: thameColors.white,
    fontSize: 14,
    fontWeight: '400',
    color: thameColors.grayBrown,
    borderRadius: 10,
    fontFamily: fontReguler,
  },
  today: {
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
    color: asitaColor.orange,
    fontSize: 14,
    fontWeight: '400',
    borderBottomColor: asitaColor.orange,
    borderBottomWidth: 2,
    fontFamily: fontReguler,
  },
});
