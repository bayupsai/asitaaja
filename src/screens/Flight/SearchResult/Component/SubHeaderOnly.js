import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Grid, Col } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import { thameColors } from '../../../../base/constant';

const window = Dimensions.get('window');
const deviceHeight = window.height;
const deviceWidth = window.width;

const SubHeaderOnly = props => (
  <View style={styles.headerContainer}>
    <ImageBackground
      source={require('../../../../assets/icons/maps.png')}
      style={styles.imageBackground}
    >
      <Grid style={styles.circleDeparture}>
        <Col size={1}>
          <Icon name="circle-o" size={10} color={thameColors.white} />
        </Col>
      </Grid>
      <Grid>
        <Col size={0}>
          <Dash style={styles.dash} dashColor={thameColors.white} />
        </Col>
        <Col size={10} style={{ marginLeft: -10, marginTop: -10 }}>
          <View>
            {/* <Text style={styles.departureTitle}>Departure From</Text> */}
            <Text style={styles.departureCode}>CGK</Text>
            {/* <Text style={styles.departureCity}>Tangerang, Indonesia</Text> */}
          </View>
          <View style={{ marginTop: 10 }}>
            {/* <Text style={styles.departureTitle}>Arival At</Text> */}
            <Text style={styles.departureCode}>DPS</Text>
            {/* <Text style={styles.departureCity}>Denpasar, Indonesia</Text> */}
          </View>
        </Col>
      </Grid>
      <Grid style={styles.circleDestination}>
        <Col size={1}>
          <Icon name="circle" size={10} color={thameColors.white} />
        </Col>
      </Grid>
    </ImageBackground>
  </View>
);

export default SubHeaderOnly;

const styles = StyleSheet.create({
  imageBackground: {
    height: deviceHeight / 4,
    width: deviceWidth,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },

  circleDeparture: {
    flex: 0,
    marginLeft: -3,
    marginBottom: 4,
  },
  circleDestination: {
    flex: 0,
    marginLeft: -3,
    marginTop: 0,
  },
  departureTitle: {
    color: thameColors.pink,
    fontSize: 14,
    fontWeight: '500',
    paddingBottom: 2,
  },
  departureCode: {
    color: thameColors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  departureCity: {
    color: thameColors.white,
    fontSize: 14,
    fontWeight: '400',
  },
  headerContainer: {
    backgroundColor: thameColors.primary,
    height: deviceHeight / 3.5,
    borderBottomLeftRadius: 50,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 30,
    paddingTop: 0,
  },
  headerContent: {
    paddingLeft: 50,
  },
  textNote: {
    fontSize: 12,
    color: thameColors.white,
  },
  textH5: {
    fontSize: 14,
    color: thameColors.white,
  },
  textH1: {
    fontSize: 22,
    fontWeight: 'bold',
    color: thameColors.white,
  },
  dash: {
    width: 1,
    height: deviceHeight / 6,
    flexDirection: 'column',
    transform: [{ translateY: 0 }, { translateX: 0 }],
  },
});
