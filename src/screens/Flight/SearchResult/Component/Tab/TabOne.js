import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Numeral from 'numeral';
import moment from 'moment';
import { ButtonRounded } from '../../../../../elements/Button';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../../../../../base/constant';

let deviceHeight = Dimensions.get('window').height;

export default class TabOne extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.content}>
        <ScrollView bounces={false}>
          {this.props.detail.map((data, index) => {
            return (
              <View
                style={{
                  flex: 1,
                  marginLeft: 25,
                  marginRight: 25,
                  marginTop: 10,
                }}
                key={index}
              >
                <Grid style={{ marginTop: 10 }}>
                  <Col size={4} style={styles.colEnd}>
                    <Text style={styles.textHeader}>
                      {data.departure_city_name}
                    </Text>
                  </Col>
                  <Col size={1.5} style={styles.colCenter}>
                    <Image
                      tintColor={asitaColor.orange}
                      resizeMode="center"
                      style={{ width: 40, height: 30 }}
                      source={require('../../../../../assets/icons/icon_header_flight_result.png')}
                    />
                  </Col>
                  <Col size={4} style={styles.colStart}>
                    <Text style={styles.textHeader}>
                      {data.arrival_city_name}
                    </Text>
                  </Col>
                </Grid>

                <Grid
                  style={{ marginTop: 20, marginLeft: 10, marginRight: 10 }}
                >
                  <Col size={2} style={styles.colStart}>
                    <Grid style={{ flex: 0, marginBottom: 30 }}>
                      <Col>
                        <View>
                          <Text style={styles.textBold}>
                            {data.simple_departure_time}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.textDate}>
                            {moment(data.departure_date_time).format('DD MMM')}
                          </Text>
                        </View>
                      </Col>
                    </Grid>
                    <Grid style={{ flex: 0, marginBottom: 30 }}>
                      <Col>
                        <View>
                          <Text style={styles.textBold}>
                            {data.duration_hour} {data.duration_minute}
                          </Text>
                        </View>
                      </Col>
                    </Grid>
                    <Grid style={{ flex: 0, marginBottom: 30 }}>
                      <Col>
                        <View>
                          <Text style={styles.textBold}>
                            {data.simple_arrival_time}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.textDate}>
                            {moment(data.arrival_date_time).format('DD MMM')}
                          </Text>
                        </View>
                      </Col>
                    </Grid>
                  </Col>
                  <Col
                    size={1.5}
                    style={{
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                    }}
                  >
                    <Icon
                      size={18}
                      color={thameColors.gray}
                      name="ios-radio-button-off"
                      type="ionicon"
                    />
                    <View style={styles.gridRow}></View>
                    <Icon
                      name="ios-radio-button-on"
                      color={thameColors.gray}
                      size={18}
                      type="ionicon"
                    />
                  </Col>
                  <Col size={7.5} style={styles.colStart}>
                    <Grid>
                      <Col>
                        <Text style={styles.textBold}>
                          {data.departure_airport_name}
                        </Text>
                      </Col>
                    </Grid>
                    <Grid
                      style={[
                        styles.colCenter,
                        { marginTop: -20, marginBottom: 15 },
                      ]}
                    >
                      <Col size={3} style={styles.colStart}>
                        {this.props.name == 'Garuda Indonesia' ? (
                          <Image
                            style={{ width: 50, height: 50 }}
                            resizeMode="contain"
                            source={require('../../../../../assets/icons/logo-airlines.png')}
                          />
                        ) : (
                          <Image
                            style={{ width: 50, height: 50 }}
                            resizeMode="contain"
                            source={{ uri: data.img_src }}
                          />
                        )}
                      </Col>
                      <Col size={7} style={styles.colStart}>
                        <Text style={styles.textItinerary}>
                          {data.flight_number} • {data.service_class}
                        </Text>
                      </Col>
                    </Grid>
                    <Grid>
                      <Col>
                        <Text style={styles.textBold}>
                          {data.arrival_airport_name}
                        </Text>
                      </Col>
                    </Grid>
                  </Col>
                </Grid>
                <Grid style={{ marginLeft: 5, marginRight: 5 }}>
                  <Col style={styles.sectionHr}></Col>
                </Grid>

                <Grid
                  style={{
                    marginTop: 15,
                    marginLeft: 10,
                    marginRight: 10,
                    marginBottom: 35,
                  }}
                >
                  <Col>
                    <Grid style={{ flex: 0, marginBottom: 20 }}>
                      <Col>
                        <Text style={styles.textBold}>
                          {data.check_in_baggage > 0 && data.has_food > 0
                            ? 'Facilities'
                            : 'No Facilities'}
                        </Text>
                      </Col>
                    </Grid>
                    <Grid style={{ flex: 0, marginBottom: 10 }}>
                      <Col size={1} style={styles.colCenter}>
                        <Image
                          tintColor={asitaColor.orange}
                          resizeMode="center"
                          style={{ width: 35, height: 25 }}
                          source={require('../../../../../assets/icons/icon_baggage.png')}
                        />
                      </Col>
                      {data.check_in_baggage !== 0 ? (
                        <Col size={3} style={styles.colStart}>
                          <Text style={styles.textItinerary}>
                            Baggage {data.check_in_baggage}{' '}
                            {data.check_in_baggage_unit}
                          </Text>
                        </Col>
                      ) : null}
                      <Col size={2}></Col>
                      <Col size={1} style={styles.colCenter}>
                        <Image
                          tintColor={asitaColor.orange}
                          resizeMode="center"
                          style={{ width: 35, height: 25 }}
                          source={require('../../../../../assets/icons/icon_meals.png')}
                        />
                      </Col>
                      {data.has_food !== 0 ? (
                        <Col size={3} style={styles.colStart}>
                          <Text style={styles.textItinerary}>Meals</Text>
                        </Col>
                      ) : null}
                    </Grid>
                  </Col>
                </Grid>
              </View>
            );
          })}
        </ScrollView>
        <Grid
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 0,
            marginBottom: 15,
            marginTop: 20,
          }}
        >
          <Col>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid
              style={{
                flex: 0,
                marginBottom: 5,
                marginLeft: 25,
                marginRight: 25,
              }}
            >
              <Col>
                <Text style={styles.textBold}>Total Payment</Text>
              </Col>
              <Col style={{ alignItems: 'flex-end' }}>
                <Text style={styles.textPrice}>
                  Rp.{' '}
                  {Numeral(this.props.price_adult)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={{ marginBottom: 15 }}>
              <Col style={styles.sectionHr}></Col>
            </Grid>
            <Grid
              style={{
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 15,
                marginBottom: 5,
              }}
            >
              <ButtonRounded
                onClick={this.props.modalPress}
                label="SELECT FLIGHT"
              />
            </Grid>
          </Col>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: thameColors.white,
    flex: 1,
    height: deviceHeight,
  },
  textHeader: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textItinerary: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 14,
    marginLeft: 3,
  },
  textDate: { fontFamily: fontReguler, color: thameColors.darkGray },
  textPrice: { fontFamily: fontBold, color: asitaColor.orange, fontSize: 16 },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: thameColors.halfWhite,
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  gridRow: {
    height: 65 + '%',
    width: 0.8,
    backgroundColor: thameColors.gray,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  colCenter: { justifyContent: 'center', alignItems: 'center' },
  colEnd: { justifyContent: 'center', alignItems: 'flex-end' },
  colStart: { justifyContent: 'center', alignItems: 'flex-start' },
});
