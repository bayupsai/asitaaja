/* eslint-disable react-native/no-inline-styles */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { CheckBox } from 'react-native-elements';
import { Grid, Col, Row } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import RangeSlider from 'rn-range-slider';
import _ from 'lodash';
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder';

// Component to Imported
import HeaderPageResults from '../../../components/HeaderPageResults';
import CardResult from './Component/CardResult';
import {
  fontExtraBold,
  fontBold,
  thameColors,
  fontSemiBold,
  fontReguler,
  asitaColor,
} from '../../../base/constant';
import DateSlider from '../../../components/CalendarSlider/index';
import { ButtonRounded } from '../../../elements/Button';
import HeaderPage from '../../../components/HeaderPage';
import { scale, verticalScale } from '../../../Const/ScaleUtils';
import ViewHide from '../../../components/ViewHide';
import { statusHeight } from '../../../elements/BarStyle';
import { makeSearchResult } from '../../../redux/selectors/FlightSelector';

//Filter Flight
const transit = [
  { title: 'Direct', item: 'direct' },
  { title: '1 Transit', item: '1transit' },
  { title: '2 Transit', item: '2transit' },
];
// const transitAirports = [
//     { title: 'Balikpapan' },
//     { title: 'Jakara' },
//     { title: 'Makassar' },
//     { title: 'Semarang' },
//     { title: 'Yogyakarta' },
//     { title: 'Surabaya' }
// ]
// const departureTime = [
//     { title: '00:00 - 06:00' },
//     { title: '06:00 - 12:00' },
//     { title: '12:00 - 18:00' },
//     { title: '18:00 - 24:00' }
// ]
const airline = [
  { title: 'Garuda Indonesia', item: 'Garuda' },
  { title: 'Citilink Indonesia', item: 'Citilink' },
  { title: 'Sriwijaya Air', item: 'Sriwijaya Air' },
  { title: 'Batik Air', item: 'Batik Air' },
  { title: 'Lion Air', item: 'Lion Air' },
];
const facilities = [{ title: 'Baggage' }, { title: 'Meals' }];
const sorting = [
  { title: 'Recommend', item: ['recommend'] },
  { title: 'Lower Price', item: ['price_adult'] },
  { title: 'Higher Price', item: ['price_adult'] },
  { title: 'Earliest Departure', item: ['departure_date'] },
  { title: 'Last Departure', item: ['departure_date'] },
  { title: 'Fastest Duration', item: ['duration'] },
  { title: 'Longest Duration', item: ['duration'] },
];

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;
const styles = StyleSheet.create({
  header: {
    flex: 0,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: thameColors.white,
    borderBottomWidth: 0.5,
    borderBottomColor: thameColors.gray,
  },
  headerLeft: { paddingLeft: 10 },
  headerRight: { alignItems: 'flex-end', paddingRight: 10 },
  titleOrderFilter: {
    fontFamily: fontBold,
    color: thameColors.superBack,
    fontSize: 16,
  },
  closeButton: {
    fontFamily: fontBold,
    color: thameColors.secondary,
    fontSize: 15,
  },
  resetButton: {
    fontFamily: fontExtraBold,
    color: thameColors.secondary,
    fontSize: 15,
  },
  content: { flex: 1 },
  //card
  card: {
    backgroundColor: thameColors.white,
    flex: 1,
    height: 'auto',
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 10,
  },
  cardCalendar: {
    backgroundColor: thameColors.white,
    width: deviceWidth / 1.05,
    borderRadius: 6,
    marginBottom: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
  },
  cardBody: {
    paddingTop: 5,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  //card
});

class SearchResult extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      dating: new Date(),
      data: [
        { id: 0, airline: 'Garuda Indonesia', price: '1.000.000' },
        { id: 1, airline: 'Sriwijaya', price: '1.000' },
        { id: 2, airline: 'Citilink', price: '1.500.000' },
        { id: 3, airline: 'Air Asia', price: '2.000.000' },
      ],
      lowestPrice: false,
      earliestDeparture: false,
      lastDeparture: false,
      earliestArrival: false,
      lastArrival: false,
      fastestDuration: false,
      directFlight: false,
      transitFlight: false,
      sortType: sorting[0],
      facilitiesSelected: '',
      hasFood: false,
      hasBaggage: false,
      transitType: '',
      orderBy: '',
      airlineName: '',
      arrayDummy: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      arrayCalendar: [1],
      arrayShimerCalendar: [1, 2, 3, 4, 5, 6, 7],
      stateSearchResult: [],
      formPayload: this.props.navigation.state.params
        ? this.props.navigation.state.params.payload
        : '',
      rangeLow: 0,
      rangeHigh: 17,
      hideAirport: true,
      hideAirportName: true,
      hideFacilities: true,
      statusFilter: false,
      reset: false,
    };
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  showFilter = type => {
    this.setState({ visibleModal: type });
  };
  closeFilter = () => {
    this.setState({ visibleModal: null });
  };

  recommend = () => {
    let recommend = this.props.listResult;
    return recommend;
  };
  setSort = (item, index) => {
    if (index !== 0) {
      this.setState({ sortType: item, visibleModal: 1 });
    } else {
      this.setState({ visibleModal: 1, sortType: sorting[0] });
    }
  };

  doFilter = () => {
    let airlineName = this.state.airlineName;
    // if (fieldData != "" && sortedBy != "") {
    //     newArray = currentArray.sort(this.compareValues(fieldData, sortedBy, airlineName))
    // } else {
    //     newArray = currentArray
    //     this.setState({ stateSearchResult: newArray })
    // }

    // if (typeOfFlight != "") {
    //     if (typeOfFlight == "direct") {
    //         let arrDirect = newArray.filter((data) => {
    //             return data.stop == "Langsung"
    //         })
    //         this.setState({ visibleModal: null, stateSearchResult: arrDirect })
    //     } else {
    //         let arrTransit = newArray.filter((data) => {
    //             return data.stop != "Langsung"
    //         })
    //         this.setState({ visibleModal: null, stateSearchResult: arrTransit })
    //     }
    // }

    //     { title: 'Recommend', item: ['recommend'] },
    // { title: 'Lower Price', item: ['price_adult'] },
    // { title: 'Higher Price', item: ['price_adult'] },
    // { title: 'Earliest Departure', item: ['departure_date'] },
    // { title: 'Last Departure', item: ['departure_date'] },
    // { title: 'Fastest Duration', item: ['duration'] },
    // { title: 'Longest Duration', item: ['duration'] },
    let sortData;
    if (this.state.sortType.title === 'Lower Price') {
      sortData = _.orderBy(this.recommend(), ['price_adult'], ['asc']);
    } else if (this.state.sortType.title === 'Higher Price') {
      sortData = _.orderBy(this.recommend(), ['price_adult'], ['desc']);
    } else if (this.state.sortType.title === 'Earliest Departure') {
      sortData = _.orderBy(this.recommend(), ['departure_date'], ['asc']);
    } else if (this.state.sortType.title === 'Last Departure') {
      sortData = _.orderBy(this.recommend(), ['departure_date'], ['desc']);
    } else if (this.state.sortType.title === 'Fastest Duration') {
      sortData = _.orderBy(this.recommend(), ['duration'], ['asc']);
    } else if (this.state.sortType.title === 'Longest Duration') {
      sortData = _.orderBy(this.recommend(), ['duration'], ['desc']);
    } else {
      sortData = this.recommend();
    }

    let transitArray;
    if (this.state.transitType === 'Direct') {
      transitArray = _.filter(sortData, { stop: 'Langsung' });
    } else if (this.state.transitType === '1 Transit') {
      transitArray = _.filter(sortData, { stop: '1 Transit' });
    } else if (this.state.transitType === '2 Transit') {
      transitArray = _.filter(sortData, { stop: '2 Transit' });
    } else {
      transitArray = sortData;
    }

    let filterArray;
    if (airlineName !== '') {
      filterArray = _.filter(transitArray, { name: airlineName });
    } else {
      filterArray = transitArray;
    }

    let facilitiesArray;
    if (this.state.hasBaggage) {
      facilitiesArray ===
        _.filter(filterArray, function(o) {
          return (
            o.detail[0].check_in_baggage !== 0 && o.detail[0].hasFood === 0
          );
        });
    } else if (this.state.hasFood) {
      facilitiesArray ===
        _.filter(filterArray, function(o) {
          return (
            o.detail[0].hasFood !== 0 && o.detail[0].check_in_baggage === 0
          );
        });
    } else if (this.state.hasBaggage && this.state.hasFood) {
      facilitiesArray ===
        _.filter(filterArray, function(o) {
          return (
            o.detail[0].hasFood !== 0 && o.detail[0].check_in_baggage !== 0
          );
        });
    } else {
      facilitiesArray = filterArray;
    }

    if (this.state.reset === false) {
      this.setState({
        visibleModal: null,
        stateSearchResult: filterArray,
        statusFilter: true,
      });
    } else {
      this.setState({
        visibleModal: null,
        stateSearchResult: filterArray,
        statusFilter: false,
        reset: false,
      });
    }
  };

  compareValues = (key, order = 'asc', airlineName) => {
    return function(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }

      const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
      const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return order === 'desc' ? comparison * -1 : comparison;
    };
  };

  resetFilter = type => {
    // if (type) {
    this.setState({
      lowestPrice: false,
      earliestDeparture: false,
      lastDeparture: false,
      earliestArrival: false,
      lastArrival: false,
      fastestDuration: false,
      filterActive: null,
      transitType: '',
      airlineName: '',
      sortType: sorting[0],
      hasBaggage: false,
      hasFood: false,
      stateSearchResult: [],
      reset: true,
    });
    // }
    // else {
    //     this.setState({
    //         lowestPrice: false,
    //         earliestDeparture: false,
    //         lastDeparture: false,
    //         earliestArrival: false,
    //         lastArrival: false,
    //         fastestDuration: false,
    //         filterActive: null,
    //     })
    // }
  };

  handleFilterSelected = (data, item) => {
    if (data === 'direct') {
      this.setState({ transitType: 'Direct' });
    } else if (data === '1transit') {
      this.setState({ transitType: '1 Transit' });
    } else if (data === '2transit') {
      this.setState({ transitType: '2 Transit' });
    } else if (
      data === 'lowest_price' ||
      data === 'earliest_departure' ||
      data === 'last_departure' ||
      data === 'earliest_arrival' ||
      data === 'last_arrival' ||
      data === 'fastest_duration'
    ) {
      this.resetFilter();

      if (data === 'lowest_price') {
        this.setState({ lowestPrice: true, filterActive: 'lowest_price' });
      } else if (data === 'earliest_departure') {
        this.setState({
          earliestDeparture: true,
          filterActive: 'earliest_departure',
        });
      } else if (data === 'last_departure') {
        this.setState({ lastDeparture: true, filterActive: 'last_departure' });
      } else if (data === 'earliest_arrival') {
        this.setState({
          earliestArrival: true,
          filterActive: 'earliest_arrival',
        });
      } else if (data === 'last_arrival') {
        this.setState({ lastArrival: true, filterActive: 'last_arrival' });
      } else if (data === 'fastest_duration') {
        this.setState({
          fastestDuration: true,
          filterActive: 'fastest_duration',
        });
      }
    } else if (data === 'airlineName') {
      this.setState({ airlineName: item });
    } else if (data === 'facilities') {
      this.setState({ facilitiesSelected: item });
      if (item === 'Baggage') {
        this.setState({ hasBaggage: !this.state.hasBaggage });
      } else if (item === 'Meals') {
        this.setState({ hasFood: !this.state.hasFood });
      }
    }
  };

  // =============================================== RENDER FILTER
  renderFilter = () => {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ padding: 10 }}
        removeClippedSubviews={true}
      >
        <Grid>
          <Row
            style={{
              paddingBottom: 20,
              paddingHorizontal: 10,
              borderBottomColor: thameColors.gray,
              borderBottomWidth: 0.5,
              paddingTop: statusHeight,
            }}
          >
            <Col size={3}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.textBlack,
                  fontSize: 18,
                }}
              >
                Sort by
              </Text>
            </Col>
            <Col size={7} style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity
                onPress={() => this.showFilter(2)}
                style={{ flexDirection: 'row', alignItems: 'center' }}
              >
                <Text
                  style={{
                    fontFamily: fontReguler,
                    color: thameColors.textBlack,
                  }}
                >
                  {this.state.sortType.title}
                </Text>
                <Icon
                  type="ionicon"
                  name="ios-arrow-down"
                  color={thameColors.textBlack}
                  iconStyle={{ marginLeft: 50 }}
                  size={20}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          {/* <Row>
                        <Text style={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "bold" }}>Orders By</Text>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("lowest_price")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Lowest price' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.lowestPrice} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("earliest_departure")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Earliest departure' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.earliestDeparture} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("last_departure")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Last departure' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.lastDeparture} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("earliest_arrival")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Earliest arrival' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.earliestArrival} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("last_arrival")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Last arrival' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.lastArrival} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("fastest_duration")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Fastest duration' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.fastestDuration} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row> */}
        </Grid>
        <Grid
          style={{
            marginVertical: 10,
            borderBottomColor: thameColors.gray,
            borderBottomWidth: 0.5,
          }}
        >
          <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 18,
              }}
            >
              Filter by
            </Text>
            <Text
              style={{
                fontFamily: fontReguler,
                color: thameColors.textBlack,
                marginTop: 7.5,
              }}
            >
              Transit
            </Text>
          </Row>
          {transit.map((item, index) => (
            <Row
              key={index}
              style={{
                paddingHorizontal: 10,
                alignItems: 'center',
                marginVertical: -10,
              }}
            >
              <Col size={8}>
                <Text
                  style={{
                    fontFamily: fontReguler,
                    color: thameColors.darkGray,
                  }}
                >
                  {item.title}
                </Text>
              </Col>
              <Col
                size={2}
                style={{
                  alignSelf: 'flex-end',
                  transform: [{ translateX: 20 }],
                }}
              >
                <CheckBox
                  onPress={() => this.handleFilterSelected(item.item)}
                  checked={item.title === this.state.transitType ? true : false}
                  iconType="material"
                  checkedIcon="check-box"
                  uncheckedIcon="check-box-outline-blank"
                  checkedColor={thameColors.grayBlue}
                  uncheckedColor={thameColors.grayBlue}
                />
              </Col>
            </Row>
          ))}
        </Grid>
        {/* {
                    this.state.transitFlight ?
                        <Grid style={{ marginVertical: 10, paddingBottom: 20, borderBottomColor: thameColors.gray, borderBottomWidth: 0.5 }}>
                            <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
                                <Text style={{ fontFamily: fontBold, color: thameColors.textBlack, fontSize: 18 }}>Transit Airports</Text>
                            </Row>
                            {
                                transitAirports.map((item, index) => (
                                    <ViewHide key={index} hide={index >= 3 ? this.state.hideAirport : false}>
                                        <Row style={{ paddingHorizontal: 10, alignItems: 'center', marginVertical: -10 }}>
                                            <Col size={8}>
                                                <Text style={{ fontFamily: fontReguler, color: thameColors.darkGray }}>{item.title}</Text>
                                            </Col>
                                            <Col size={2} style={{ alignSelf: 'flex-end', transform: [{ translateX: 20 }] }}>
                                                <CheckBox checked={this.state.lowestPrice} iconType='material' checkedIcon='check-box' uncheckedIcon='check-box-outline-blank' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                                            </Col>
                                        </Row>
                                    </ViewHide>
                                ))
                            }
                            <Row style={{ paddingHorizontal: 10, }}>
                                <TouchableOpacity onPress={() => this.setState({ hideAirport: !this.state.hideAirport })}>
                                    <Text style={{ fontFamily: fontSemiBold, color: thameColors.primary }}>
                                        {this.state.hideAirport === true ? 'Show All' : 'Show Less'}
                                    </Text>
                                </TouchableOpacity>
                            </Row>
                        </Grid> : null
                } */}
        <Grid
          style={{
            marginVertical: 10,
            paddingBottom: 20,
            borderBottomColor: thameColors.gray,
            borderBottomWidth: 0.5,
          }}
        >
          <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 18,
              }}
            >
              Transit Duration
            </Text>
            <Text
              style={{ fontFamily: fontReguler, color: thameColors.darkGray }}
            >
              Duration per transit: 0h - 17h
            </Text>
          </Row>
          <Row style={{ marginTop: 7.5, paddingHorizontal: 10 }}>
            <RangeSlider
              style={{ width: '100%', height: 25 }}
              gravity={'top'}
              min={0}
              max={17}
              step={1}
              labelStyle={'none'}
              selectionColor={thameColors.primary}
              blankColor={thameColors.gray}
              thumbBorderColor={thameColors.primary}
              onValueChanged={(low, high, fromUser) => {
                this.setState({ rangeLow: low, rangeHigh: high });
              }}
            />
          </Row>
          <Row
            style={{
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 10,
            }}
          >
            <Text
              style={{ fontFamily: fontReguler, color: thameColors.darkGray }}
            >
              {this.state.rangeLow}h
            </Text>
            <Text
              style={{ fontFamily: fontReguler, color: thameColors.darkGray }}
            >
              {this.state.rangeHigh}h
            </Text>
          </Row>
        </Grid>
        {/* ============================ Departure Time & Arrival Time ============================ */}
        {/* <Grid style={{ marginVertical: 10, paddingBottom: 20, borderBottomColor: thameColors.gray, borderBottomWidth: 0.5 }}>
                    <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
                        <Text style={{ fontFamily: fontBold, color: thameColors.textBlack, fontSize: 18 }}>Departure Time</Text>
                    </Row>
                    {
                        departureTime.map((item, index) => (
                            <Row key={index} style={{ paddingHorizontal: 10, alignItems: 'center', marginVertical: -10 }}>
                                <Col size={8}>
                                    <Text style={{ fontFamily: fontReguler, color: thameColors.darkGray }}>{item.title}</Text>
                                </Col>
                                <Col size={2} style={{ alignSelf: 'flex-end', transform: [{ translateX: 20 }] }}>
                                    <CheckBox checked={this.state.lowestPrice} iconType='material' checkedIcon='check-box' uncheckedIcon='check-box-outline-blank' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                                </Col>
                            </Row>
                        ))
                    }
                </Grid>
                <Grid style={{ marginVertical: 10, paddingBottom: 20, borderBottomColor: thameColors.gray, borderBottomWidth: 0.5 }}>
                    <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
                        <Text style={{ fontFamily: fontBold, color: thameColors.textBlack, fontSize: 18 }}>Arrival Time</Text>
                    </Row>
                    {
                        departureTime.map((item, index) => (
                            <Row key={index} style={{ paddingHorizontal: 10, alignItems: 'center', marginVertical: -10 }}>
                                <Col size={8}>
                                    <Text style={{ fontFamily: fontReguler, color: thameColors.darkGray }}>{item.title}</Text>
                                </Col>
                                <Col size={2} style={{ alignSelf: 'flex-end', transform: [{ translateX: 20 }] }}>
                                    <CheckBox checked={this.state.lowestPrice} iconType='material' checkedIcon='check-box' uncheckedIcon='check-box-outline-blank' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                                </Col>
                            </Row>
                        ))
                    }
                </Grid> */}
        {/* ============================ Departure Time & Arrival Time ============================ */}
        <Grid
          style={{
            marginVertical: 10,
            paddingBottom: 20,
            borderBottomColor: thameColors.gray,
            borderBottomWidth: 0.5,
          }}
        >
          <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 18,
              }}
            >
              Airline
            </Text>
          </Row>
          {airline.map((item, index) => (
            <ViewHide
              key={index}
              hide={index >= 1 ? this.state.hideAirportName : false}
            >
              <Row
                key={index}
                style={{
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  marginVertical: -10,
                }}
              >
                <Col size={8}>
                  <Text
                    style={{
                      fontFamily: fontReguler,
                      color: thameColors.darkGray,
                    }}
                  >
                    {item.title}
                  </Text>
                </Col>
                <Col
                  size={2}
                  style={{
                    alignSelf: 'flex-end',
                    transform: [{ translateX: 20 }],
                  }}
                >
                  <CheckBox
                    onPress={() =>
                      this.handleFilterSelected('airlineName', item.item)
                    }
                    checked={
                      item.item === this.state.airlineName ? true : false
                    }
                    iconType="material"
                    checkedIcon="check-box"
                    uncheckedIcon="check-box-outline-blank"
                    checkedColor={thameColors.grayBlue}
                    uncheckedColor={thameColors.grayBlue}
                  />
                </Col>
              </Row>
            </ViewHide>
          ))}
          <Row style={{ paddingHorizontal: 10 }}>
            <TouchableOpacity
              onPress={() =>
                this.setState({ hideAirportName: !this.state.hideAirportName })
              }
            >
              <Text
                style={{ fontFamily: fontSemiBold, color: asitaColor.tealBlue }}
              >
                {this.state.hideAirportName === true ? 'Show All' : 'Show Less'}
              </Text>
            </TouchableOpacity>
          </Row>
        </Grid>
        <Grid
          style={{ marginVertical: 10, marginBottom: 35, paddingBottom: 20 }}
        >
          <Row style={{ paddingHorizontal: 10, flexDirection: 'column' }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.textBlack,
                fontSize: 18,
              }}
            >
              Facilities
            </Text>
          </Row>
          {facilities.map((item, index) => (
            <ViewHide
              key={index}
              hide={index >= 1 ? this.state.hideFacilities : false}
            >
              <Row
                key={index}
                style={{
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  marginVertical: -10,
                }}
              >
                <Col size={8}>
                  <Text
                    style={{
                      fontFamily: fontReguler,
                      color: thameColors.darkGray,
                    }}
                  >
                    {item.title}
                  </Text>
                </Col>
                <Col
                  size={2}
                  style={{
                    alignSelf: 'flex-end',
                    transform: [{ translateX: 20 }],
                  }}
                >
                  <CheckBox
                    onPress={() =>
                      this.handleFilterSelected('facilities', item.title)
                    }
                    checked={
                      index === 0 ? this.state.hasBaggage : this.state.hasFood
                    }
                    iconType="material"
                    checkedIcon="check-box"
                    uncheckedIcon="check-box-outline-blank"
                    checkedColor={thameColors.grayBlue}
                    uncheckedColor={thameColors.grayBlue}
                  />
                </Col>
              </Row>
            </ViewHide>
          ))}
          <Row style={{ paddingHorizontal: 10 }}>
            <TouchableOpacity
              onPress={() =>
                this.setState({ hideFacilities: !this.state.hideFacilities })
              }
            >
              <Text
                style={{ fontFamily: fontSemiBold, color: asitaColor.tealBlue }}
              >
                {this.state.hideFacilities ? 'Show All' : 'Show Less'}
              </Text>
            </TouchableOpacity>
          </Row>
        </Grid>

        {/* <Grid style={{ marginTop: 20 }}>
                    <Row>
                        <Text style={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "bold" }}>Filters By</Text>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("direct_flight")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Direct Flight' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.directFlight} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>
                    <Row style={{ paddingTop: 10 }}>
                        <View>
                            <CheckBox onPress={() => this.handleFilterSelected("transit")} containerStyle={{ backgroundColor: thameColors.white, padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }} title='Transit' textStyle={{ color: thameColors.grayBlue, fontSize: 16, fontWeight: "300" }} checked={this.state.transitFlight} iconType='feather' checkedIcon='check' uncheckedIcon='square' checkedColor={thameColors.grayBlue} uncheckedColor={thameColors.grayBlue} />
                        </View>
                    </Row>

                </Grid> */}
      </ScrollView>
    );
  };
  // =============================================== RENDER FILTER

  // =============================================== RENDER LOADING PLACEHOLDER
  loadingCalendar = index => {
    return (
      <Grid key={index} style={[styles.cardCalendar, { padding: 10 }]}>
        {this.state.arrayShimerCalendar.map((item, i) => (
          <Col style={{ marginVertical: 15, alignItems: 'center' }} key={i}>
            <Placeholder Animation={Fade}>
              <PlaceholderLine height={verticalScale(50)} width={scale(75)} />
            </Placeholder>
          </Col>
        ))}
      </Grid>
    );
  };

  renderLoading = index => {
    return (
      <View key={index} style={[styles.card, { padding: 10 }]}>
        <Placeholder Animation={Fade}>
          <PlaceholderLine
            height={verticalScale(25)}
            style={{ width: '100%' }}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <PlaceholderLine width={scale(25)} height={verticalScale(25)} />
            <PlaceholderLine width={scale(25)} height={verticalScale(25)} />
          </View>
        </Placeholder>
      </View>
    );
  };
  // =============================================== RENDER LOADING PLACEHOLDER

  renderContent = () => {
    if (this.props.listResult.length !== 0) {
      if (this.state.statusFilter) {
        if (this.state.stateSearchResult.length !== 0) {
          return this.state.stateSearchResult.map((item, i) => (
            <CardResult key={i} {...this.props} {...item} />
          ));
        } else {
          return this.emptyResults('from state');
        }
      } else if (this.state.statusFilter === false) {
        return this.recommend().map((item, i) => (
          <CardResult key={i} {...this.props} {...item} />
        ));
      }
    } else {
      return this.emptyResults('from redux');
    }
  };

  emptyResults = type => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          paddingBottom: 30,
        }}
      >
        <View>
          <Image
            resizeMode="center"
            style={{ height: deviceHeight / 2.5, width: deviceWidth - 40 }}
            source={require('../../../assets/icons/search_not_found.png')}
          />
        </View>
        <View style={{ paddingTop: 10, alignItems: 'center' }}>
          <Text
            style={{
              color: thameColors.textBlack,
              fontWeight: '500',
              fontSize: 22,
            }}
          >
            No Flight Available
          </Text>
        </View>
        <View
          style={{
            paddingTop: 20,
            paddingLeft: 30,
            paddingRight: 30,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: thameColors.textBlack,
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            Please search for another date
          </Text>
          {/* or you may */}
        </View>
        <View
          style={{
            paddingTop: 3,
            paddingLeft: 30,
            paddingRight: 30,
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: thameColors.textBlack,
              fontSize: 16,
              alignSelf: 'center',
              alignItems: 'center',
            }}
          >
            click the Change Search button below
          </Text>
        </View>
        <View style={{ marginTop: 30 }}>
          <ButtonRounded onClick={this.goBackHeader} label="CHANGE SEARCH" />
        </View>
      </View>
    );
  };

  //Main Render
  render() {
    return (
      <View style={{ backgroundColor: thameColors.backWhite, flex: 1 }}>
        <HeaderPageResults
          page="flight"
          callback={this.goBackHeader}
          flight={this.state.formPayload}
          loading={this.props.loading}
        />
        <View
          style={{
            backgroundColor: thameColors.backWhite,
            alignItems: 'center',
            justifyContent: 'center',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            marginTop: -20,
            padding: 10,
            paddingBottom: 0,
            marginBottom: 10,
          }}
        >
          {!this.props.loading && this.props.listResult.length > 0 ? (
            <Text
              style={{
                color: thameColors.superBack,
                fontSize: 14,
                fontFamily: fontBold,
              }}
            >
              Select Departure Flight From{' '}
              {this.props.listResult ? this.props.listResult.length : '-'}{' '}
              Schedules
            </Text>
          ) : null}
        </View>
        <ScrollView
          horizontal={false}
          showsVerticalScrollIndicator={false}
          removeClippedSubviews={true}
        >
          <Grid>
            <Col
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 0,
                marginBottom: 5,
              }}
            >
              {this.props.loading ? (
                this.state.arrayCalendar.map((item, i) =>
                  this.loadingCalendar(i)
                )
              ) : this.props.listResult.length > 0 ? (
                <DateSlider {...this.props.navigation.state.params} />
              ) : null}
            </Col>
          </Grid>
          <Grid
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
              paddingLeft: 10,
              paddingRight: 10,
            }}
          >
            <Col>
              {this.props.loading
                ? this.state.arrayDummy.map((item, i) => this.renderLoading(i))
                : this.renderContent()}
            </Col>
          </Grid>
        </ScrollView>

        {/* ========================== FAB ========================== */}
        {!this.props.loading && this.props.listResult.length > 0 ? (
          <Grid
            style={{
              position: 'absolute',
              bottom: Platform.OS === 'ios' ? 30 : 10,
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
              width: deviceWidth,
            }}
          >
            <Col style={{ alignItems: 'center' }}>
              <TouchableOpacity
                style={{ flex: 0 }}
                onPress={() => this.showFilter(1)}
              >
                <Grid
                  style={{
                    borderRadius: 20,
                    backgroundColor: asitaColor.marineBlue,
                    padding: 10,
                    width: deviceWidth / 3.5,
                    alignItems: 'center',
                  }}
                >
                  <Col
                    size={3.5}
                    style={{ justifyContent: 'center', alignItems: 'flex-end' }}
                  >
                    <Image
                      source={require('../../../assets/icons/icon_filter_new.png')}
                      style={{
                        tintColor: thameColors.white,
                        width: 25,
                        height: 16,
                        resizeMode: 'center',
                        paddingLeft: 3,
                      }}
                    />
                  </Col>
                  <Col
                    size={6.5}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Text
                      style={{
                        color: thameColors.white,
                        fontSize: 16,
                        fontFamily: fontExtraBold,
                      }}
                    >
                      Filter
                    </Text>
                  </Col>
                </Grid>
              </TouchableOpacity>
            </Col>
          </Grid>
        ) : (
          <View style={{ position: 'absolute' }} />
        )}
        {/* ========================== FAB ========================== */}

        {/* ========================== MODAL FILTER ========================== */}
        <Modal
          style={{ flex: 1, backgroundColor: thameColors.white, margin: 0 }}
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={this.closeFilter}
          onBackdropPress={this.closeFilter}
        >
          <HeaderPage
            title="Sort & Filter"
            left={true}
            right={true}
            leftComponent={
              <TouchableOpacity
                onPress={this.closeFilter}
                style={{ marginLeft: 10 }}
              >
                <Image
                  source={require('../../../assets/icons/close_icon.png')}
                  resizeMode={'contain'}
                  style={{ width: scale(20), height: scale(17.5) }}
                />
              </TouchableOpacity>
            }
            rightComponent={
              <TouchableOpacity
                onPress={this.resetFilter}
                style={{ marginRight: 10 }}
              >
                <Text
                  style={{
                    fontFamily: fontSemiBold,
                    color: thameColors.white,
                    fontSize: 18,
                  }}
                >
                  Reset
                </Text>
              </TouchableOpacity>
            }
          />
          <Grid style={styles.content}>
            <Col>{this.renderFilter()}</Col>
          </Grid>
          <Grid
            style={{
              position: 'absolute',
              bottom: Platform.OS === 'ios' ? 30 : 10,
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
              width: deviceWidth,
            }}
          >
            <Col style={{ alignItems: 'center' }}>
              <View
                style={{
                  backgroundColor: asitaColor.tealBlue,
                  padding: 10,
                  width: deviceWidth / 4,
                  alignItems: 'center',
                  borderRadius: 20,
                }}
              >
                <TouchableOpacity onPress={this.doFilter}>
                  <Text
                    style={{
                      color: thameColors.white,
                      fontFamily: fontExtraBold,
                    }}
                  >
                    DONE
                  </Text>
                </TouchableOpacity>
              </View>
            </Col>
          </Grid>
        </Modal>
        {/* ========================== MODAL FILTER ========================== */}

        {/* ========================== MODAL SortBy ========================== */}
        <Modal
          style={{ flex: 1, margin: 0, justifyContent: 'flex-end' }}
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 2}
          onBackButtonPress={() => this.showFilter(1)}
          onBackdropPress={() => this.showFilter(1)}
        >
          <View
            style={{
              backgroundColor: thameColors.white,
              paddingVertical: 20,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            }}
          >
            {sorting.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => this.setSort(item, index)}
                style={{
                  padding: 10,
                  borderBottomColor: thameColors.gray,
                  borderBottomWidth: 0.5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <Text
                  style={{
                    fontFamily: fontSemiBold,
                    color: thameColors.textBlack,
                    fontSize: 16,
                  }}
                >
                  {item.title}
                </Text>
                {this.state.sortType.item === item.item ? (
                  <Icon
                    name="ios-checkmark-circle"
                    type="ionicon"
                    color={asitaColor.marineBlue}
                    size={22}
                  />
                ) : null}
              </TouchableOpacity>
            ))}
          </View>
        </Modal>
        {/* ========================== MODAL SortBy ========================== */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    listResult: makeSearchResult(state),
    loading: state.flight.singleSearchFetch,
  };
}
export default connect(mapStateToProps)(SearchResult);
