import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import { thameColors, fontBold, asitaColor } from '../../../base/constant';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  row1: { flex: 1, flexDirection: 'row' },
  center: { alignItems: 'center', justifyContent: 'center' },
  topPadding: { paddingTop: 20 },
  bottom20: { flex: 0, marginBottom: 20 },
  left0: { marginLeft: 0 },
  textBold: {
    fontFamily: fontBold,
    fontSize: 18,
    color: thameColors.textBlack,
  },
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  button: {
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: thameColors.white,
    borderRadius: 5,
  },
  toTop: {
    marginTop: -50,
  },
  modal1: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.25,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalMode: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: deviceHeight,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  btnColor: {
    backgroundColor: asitaColor.tealBlue,
  },
});
