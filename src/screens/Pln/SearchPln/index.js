import React from 'react';
import { ScrollView, View, Text, TouchableOpacity } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import numeral from 'numeral';
// import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
// import { actionPlnPost } from '../../../redux/actions/BillsAction';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import styles from './styles';
import { InputRevampCenter, InputText } from '../../../elements/TextInput';
import { thameColors, asitaColor } from '../../../base/constant';
import { ButtonRounded } from '../../../elements/Button';
import Modalin from './components/Modal';

class Pln extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modal: null,
      mode: 'post', // pre = Prepaid & post = Postpaid
      listMode: [
        { title: 'Pay Postpaid Electricity', name: 'post' },
        { title: 'Pay PLN Prepaid', name: 'pre' },
      ],
      customerID: '521540823562',
      nominal: 20000,
    };
  }

  // Mode
  changeMode = mode => {
    this.setState({ mode }, () => {
      this.showModal(null);
    });
  };

  // Modal
  showModal = id => {
    this.setState({ modal: id });
  };

  modalContent = () => {
    const { customerID, modal } = this.state;
    return (
      <View style={styles.modalSeatClass}>
        <Grid style={styles.modalContent}>
          <TouchableOpacity
            style={styles.row1}
            onPress={() => this.showModal(null)}
          >
            <Col style={styles.center}>
              <Text style={styles.textBold}>
                {modal === 1 ? 'Customer ID' : 'Nominal'}
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid>
          <Col style={{ backgroundColor: thameColors.white }}>
            <Grid style={styles.topPadding}>
              <Row style={styles.bottom20}>
                <Col size={7} style={styles.left0}>
                  <InputText
                    label="Enter Customer ID"
                    required="*"
                    value={customerID}
                    editable
                    onChangeText={data => this.setState({ customerID: data })}
                  />
                </Col>
              </Row>
              <Row style={styles.bottom20}>
                <Col size={6} style={styles.sectionButtonArea}>
                  <ButtonRounded
                    label="SAVE"
                    onClick={() => this.showModal(null)}
                  />
                </Col>
              </Row>
            </Grid>
          </Col>
        </Grid>
      </View>
    );
  };

  modalMode = () => {
    const { mode, listMode } = this.state;
    const { modalMode, toTop, textBold } = styles;
    return (
      <View style={modalMode}>
        <HeaderPage
          title="PLN Products"
          callback={() => this.showModal(null)}
        />
        <SubHeaderPage />
        <View style={[toTop]}>
          {listMode.map((item, index) => (
            <TouchableOpacity
              style={{
                width: '90%',
                marginHorizontal: 20,
                marginVertical: 5,
                padding: 20,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: thameColors.white,
                borderRadius: 10,
              }}
              key={index}
              onPress={() => this.changeMode(item.name)}
            >
              <Text style={textBold}>{item.title}</Text>
              {mode === item.name ? (
                <Icon
                  type="ionicon"
                  name="ios-checkmark-circle"
                  color={asitaColor.tealBlue}
                />
              ) : null}
            </TouchableOpacity>
            // <InputRevampCenter
            //   key={index}
            //   onPress={() => this.changeMode(item.name)}
            //   editable={item.title}
            //   rightItem={
            //     <Icon
            //       type="ionicon"
            //       name="ios-checkmark-circle"
            //       color={asitaColor.tealBlue}
            //     />
            //   }
            // />
          ))}
        </View>
      </View>
    );
  };

  // Function
  goBack = () => {
    const {
      navigation: { goBack },
    } = this.props;
    goBack();
  };

  payPdam = () => {
    const { customerID, nominal, mode } = this.state;
    const { dispatch } = this.props;
    const payload = {
      type: 'inquiry',
      product: '3008',
      billNumber: customerID,
    };
    this.props.navigation.navigate('DetailPln', {
      mode,
    });
    // dispatch(actionPlnPost(payload))
    //   .then(res => {
    //     alert(JSON.stringify(res));
    //   })
    //   .catch(err => {
    //     alert(err.message);
    //   });
  };

  render() {
    const { container, button, toTop, modal1 } = styles;
    const { modal, nominal, mode, listMode, customerID } = this.state;
    return (
      <ScrollView style={container}>
        <HeaderPage
          title={
            mode === listMode[0].name
              ? 'Pay Postpaid Electricity'
              : 'Pay PLN Prepaid'
          }
          callback={this.goBack}
          right={true}
          rightComponent={
            <TouchableOpacity onPress={() => this.showModal(2)}>
              <Icon
                type="entypo"
                name="dots-three-vertical"
                color={thameColors.white}
              />
            </TouchableOpacity>
          }
        />
        <SubHeaderPage />
        <View style={toTop}>
          <Grid>
            <Col>
              <InputRevampCenter
                onPress={() => this.showModal(1)}
                editable={customerID ? customerID : 'Enter here'}
                label="Customer ID"
              />
            </Col>
          </Grid>
          {mode === listMode[1].name ? (
            <Grid>
              <Col>
                <InputRevampCenter
                  editable={numeral(nominal)
                    .format('0,0')
                    .replace(/,/g, '.')}
                  label="Nominal"
                />
              </Col>
            </Grid>
          ) : null}
          <Grid>
            <View style={button}>
              <ButtonRounded
                buttonStyle={styles.btnColor}
                label="BUY TOKEN"
                onClick={this.payPdam}
              />
            </View>
          </Grid>
        </View>

        {/* ======================= Modal ======================= */}
        <Modalin
          isVisible={modal}
          dismiss={() => this.showModal(null)}
          style={modal1}
          children={modal === 1 ? this.modalContent() : this.modalMode()}
        />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.bills.loadPlnPost,
  plnPost: state.bills.plnPost,
});

export default connect(mapStateToProps)(Pln);
