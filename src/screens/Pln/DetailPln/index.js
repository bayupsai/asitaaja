/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { ScrollView, View, Text } from 'react-native';
import moment from 'moment';
import numeral from 'numeral';
import { Grid, Col } from 'react-native-easy-grid';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import styles from './styles';
import { ButtonRounded } from '../../../elements/Button';

class DetailPln extends React.PureComponent {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
      mode: navigation.getParam('mode'),
    };
  }

  goBack = () => {
    const {
      navigation: { goBack },
    } = this.props;
    goBack();
  };

  // Main Render
  render() {
    const { mode } = this.state;
    return (
      <ScrollView style={styles.container}>
        <HeaderPage title="Payment Details" callback={this.goBack} />
        <SubHeaderPage />
        <View style={styles.toTop}>
          <View style={[styles.card, styles.alignCenter, styles.cardHeader]}>
            <Text style={styles.textCard}>
              You will not be able to change your details below once you proceed
              to payment
            </Text>
          </View>

          <View style={styles.card}>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Nama Lengkap</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>Hery Susanto</Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>
                  {mode === 'post' ? 'ID Pel' : 'No. Meter'}
                </Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>541103484602</Text>
              </Col>
            </Grid>
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Tarif/Daya</Text>
              </Col>
              <Col>
                {mode === 'post' ? (
                  <Text style={styles.textSemiBold}>R1/1300</Text>
                ) : (
                  <Text style={styles.textSemiBold}>R1/1300VA</Text>
                )}
              </Col>
            </Grid>
            <Grid
              style={[
                styles.itemCard,
                mode === 'post' ? null : { borderBottomWidth: 0 },
              ]}
            >
              <Col>
                <Text style={styles.textGray}>
                  {mode === 'post' ? 'Rp Tag PLN' : 'Total'}
                </Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>
                  Rp{' '}
                  {numeral(mode === 'post' ? 62000 : 22750)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            {mode === 'post' ? (
              <View>
                <Grid style={styles.itemCard}>
                  <Col>
                    <Text style={styles.textGray}>Ref</Text>
                    <Text style={styles.textSemiBold}>
                      ONIW233516906770620C4940D91844F8
                    </Text>
                  </Col>
                </Grid>
                <Grid style={styles.itemCard}>
                  <Col>
                    <Text style={styles.textGray}>No Struk</Text>
                  </Col>
                  <Col>
                    <Text style={styles.textSemiBold}>29</Text>
                  </Col>
                </Grid>
                <Grid style={styles.itemCard}>
                  <Col>
                    <Text style={styles.textGray}>Tanggal</Text>
                  </Col>
                  <Col>
                    <Text style={styles.textSemiBold}>
                      {moment().format('DD/MM/YYYY')}
                    </Text>
                  </Col>
                </Grid>
                <Grid style={styles.itemCard}>
                  <Col>
                    <Text style={styles.textGray}>Bulan/Th</Text>
                  </Col>
                  <Col>
                    <Text style={styles.textSemiBold}>
                      {moment().format('MMMM YYYY')}
                    </Text>
                  </Col>
                </Grid>
                <Grid style={[styles.itemCard, { borderBottomWidth: 0 }]}>
                  <Col>
                    <Text style={styles.textGray}>Stand Meter</Text>
                    <Text
                      style={[styles.textSemiBold, { alignSelf: 'flex-start' }]}
                    >
                      01052700-01061600
                    </Text>
                  </Col>
                </Grid>
              </View>
            ) : null}

            {/* Detail Tagihan */}
            {mode === 'post' ? null : (
              <View>
                <View style={{ marginTop: 20 }}>
                  <Text style={styles.textBold}>Detail Tagihan</Text>
                </View>
                <Grid style={styles.itemCard}>
                  <Col>
                    <Text style={styles.textGray}>Jumlah Tagihan</Text>
                  </Col>
                  <Col>
                    <Text style={styles.textSemiBold}>
                      Rp{' '}
                      {numeral(20000)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Grid>
                <Grid style={[styles.itemCard, { borderBottomWidth: 0 }]}>
                  <Col>
                    <Text style={styles.textGray}>Biaya Admin</Text>
                  </Col>
                  <Col>
                    <Text style={styles.textSemiBold}>
                      Rp{' '}
                      {numeral(2750)
                        .format('0,00')
                        .replace(/,/g, '.')}
                    </Text>
                  </Col>
                </Grid>
              </View>
            )}
          </View>

          <View style={[styles.card, { paddingVertical: 20 }]}>
            {mode === 'post' ? (
              <Text style={styles.textBold}>Detail Tagihan</Text>
            ) : null}
            {mode === 'post' ? (
              <Grid style={[styles.itemCard, { borderBottomWidth: 0 }]}>
                <Col>
                  <Text style={styles.textGray}>Biaya Admin</Text>
                </Col>
                <Col>
                  <Text style={styles.textSemiBold}>
                    Rp{' '}
                    {numeral(2500)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>
                </Col>
              </Grid>
            ) : null}
            <Grid style={styles.itemCard}>
              <Col>
                <Text style={styles.textGray}>Harga</Text>
              </Col>
              <Col>
                <Text style={styles.textSemiBold}>
                  Rp{' '}
                  {numeral(mode === 'post' ? 62000 : 20000)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
            <Grid style={[styles.itemCard, { borderBottomWidth: 0 }]}>
              <Col>
                <Text style={styles.textGray}>Price You Pay</Text>
              </Col>
              <Col>
                <Text style={styles.textPrice}>
                  Rp{' '}
                  {numeral(mode === 'post' ? 64000 : 22750)
                    .format('0,00')
                    .replace(/,/g, '.')}
                </Text>
              </Col>
            </Grid>
          </View>
        </View>
        <View style={{ margin: 20 }}>
          <ButtonRounded
            buttonStyle={styles.btnColor}
            label="CONTINUE TO PAYMENT"
          />
        </View>
      </ScrollView>
    );
  }
}

export default DetailPln;
