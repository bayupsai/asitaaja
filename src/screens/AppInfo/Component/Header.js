import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontReguler,
  fontRegulerItalic,
} from '../../../base/constant';

const BackButton = props => {
  return (
    <TouchableOpacity
      onPress={() => props.goBack()}
      style={{ paddingRight: 20 }}
    >
      <Icon name="md-arrow-back" color="#FFFFFF" type="ionicon" />
    </TouchableOpacity>
  );
};

const TitlePage = props => {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Text
        style={{ color: '#FFFFFF', fontSize: 18, fontFamily: fontExtraBold }}
      >
        {props.title}
      </Text>
    </View>
  );
};

const HeaderRight = params => {
  const navigation = params.data.navigation;
  navigateToChat = page => navigation.navigate('Chatbot', { load: 1 });
  navigateToNotifications = page => navigation.navigate('Notifications');
  return (
    <Col
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <TouchableOpacity
        style={{ flex: 0 }}
        onPress={() => alert('Where you want to Go?')}
      >
        <Icon type="entypo" name="dots-three-vertical" color="#fff" size={24} />
      </TouchableOpacity>
    </Col>
  );
};

const HeaderPageOnly = props => (
  <Header
    placement="center"
    leftComponent={<BackButton goBack={props.callback} />}
    centerComponent={<TitlePage title={props.title} />}
    containerStyle={{
      backgroundColor: '#00275d',
      paddingTop: 0,
      borderBottomWidth: 0,
      borderBottomColor: 'transparent',
      height: 50,
    }}
  />
);

export default HeaderPageOnly;
