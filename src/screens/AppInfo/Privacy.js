import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { fontBold, thameColors } from '../../base/constant/index';
import HeaderPage from '../../components/HeaderPage';
import ViewHide from '../../components/ViewHide';

export default class Privacy extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: [
        'Key Updates',
        'Terms of Services',
        'Privacy Policy',
        'Privacy Shield',
        'IP Policy',
        'Cookies',
        'Payments',
      ],
      lorem:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      hiden: null,
    };
  }

  _hideFunc = () => {
    this.setState({ hiden: !this.state.hiden });
  };

  _headerHide = ({ item, index }) => {
    const { lorem, hiden } = this.state;
    const { headerContent, content } = styles;
    return (
      <View key={index}>
        <TouchableOpacity onPress={this._hideFunc} style={headerContent}>
          <Text>{item}</Text>
        </TouchableOpacity>
        <ViewHide hide={item === index}>
          <View style={content}>
            <Text>{lorem}</Text>
          </View>
        </ViewHide>
      </View>
    );
  };

  //Main Render
  render() {
    const { title } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <HeaderPage
          callback={() => this.props.navigation.goBack()}
          title="Privacy Policy"
        />
        <View style={styles.container}>
          <Text style={styles.boldText}>Aeroaja Legal Info</Text>
          <FlatList
            data={title}
            renderItem={this._headerHide}
            keyExtractor={(__, index) => index.toString()}
            removeClippedSubviews={true}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  boldText: {
    fontFamily: fontBold,
    color: thameColors.superBack,
    fontSize: 20,
  },
  headerContent: {
    padding: 10,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: thameColors.gray,
  },
  content: {
    padding: 10,
  },
});
