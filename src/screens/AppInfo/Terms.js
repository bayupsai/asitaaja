import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { fontBold } from '../../base/constant/index';
import HeaderPage from '../../components/HeaderPage';

export default class Terms extends PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderPage
          callback={() => this.props.navigation.goBack()}
          title="Terms and Conditions"
        />
        <View style={styles.container}>
          <Text style={styles.boldText}>Terms and Conditions</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boldText: {
    fontFamily: fontBold,
    color: '#000',
    fontSize: 20,
    fontStyle: 'italic',
  },
});
