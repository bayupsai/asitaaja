import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { fontBold, fontReguler, thameColors } from '../../base/constant/index';
import HeaderPage from '../../components/HeaderPage';

const window = Dimensions.get('window');
const deviceWidth = window.width;
const deviceHeight = window.height;

export default class About extends PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderPage
          callback={() => this.props.navigation.goBack()}
          title="About App"
        />
        <View style={styles.container}>
          {/* <Text style={styles.boldText}>Aeroaja</Text> */}
          <Image
            source={require('../../assets/logos/asita_splashscreen.png')}
            style={styles.image}
            resizeMode="center"
          />
          <Text style={styles.regularText}>Version 1.0.0</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: thameColors.backWhite,
  },
  image: {
    width: deviceWidth / 2,
    height: deviceHeight / 3,
  },
  boldText: {
    fontFamily: fontBold,
    color: thameColors.primary,
    fontSize: 45,
  },
  regularText: {
    fontFamily: fontReguler,
  },
});
