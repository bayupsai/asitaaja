import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { Icon } from 'react-native-elements';
// import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps"
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
} from '../../base/constant/index';
import { InputCargo } from '../../elements/TextInput';
import { ButtonRounded } from '../../elements/Button';
import DestinationModal from './Modal/DestinationModal';
// import MapViewDirections from 'react-native-maps-directions'
import { connect } from 'react-redux';
import { setReceiverCargo } from '../../redux/actions/CargoAction';
let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class CargoDestination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapRegion: null,
      lastLat: null,
      lastLong: null,
      modalVisible: false,
      destinationBoxlist: this.props.listBox,
      searchDestination: [],
      dataOrigin: this.props.senderData.dataOrigin,
      dataDestination: null,
      originIsTyping: false,
      coordinates: null,
    };
  }

  resetValueOfState = () => {
    this.setState({
      dataDestination: null,
    });
    this.props.navigation.goBack();
  };
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.2,
          longitudeDelta: 0.2,
        };
        this.setMaps(region, region.latitude, region.longitude);
      },
      error => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );

    this.watchID = navigator.geolocation.watchPosition(position => {
      let region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: 0.2,
        longitudeDelta: 0.2,
      };
      this.setMaps(region, region.latitude, region.longitude);
    });
  }

  setMaps(region, lastLat, lastLong) {
    this.setState({
      mapRegion: region,
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong,
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  onMapPress(e) {
    // alert("onMapPress")
    // console.log(e.nativeEvent.coordinate.longitude);
    // let region = {
    //   latitude:       e.nativeEvent.coordinate.latitude,
    //   longitude:      e.nativeEvent.coordinate.longitude,
    //   latitudeDelta:  0.00922*1.5,
    //   longitudeDelta: 0.00421*1.5
    // }
    // this.onRegionChange(region, region.latitude, region.longitude);
  }

  renderBackButton = () => {
    return (
      <TouchableOpacity
        onPress={() => this.resetValueOfState()}
        style={{
          backgroundColor: 'white',
          width: 40,
          height: 40,
          borderRadius: 20,
        }}
      >
        <Icon
          name="ios-arrow-round-back"
          color="#000000"
          size={42}
          type="ionicon"
        />
      </TouchableOpacity>
    );
  };

  // ============================= SEARCHING AUTOCOMPLETE
  searchingDestination = val => {
    if (val !== '') {
      this.setState(
        {
          searchDestination: '',
        },
        () => {
          let data = this.filterValuePart(this.state.destinationBoxlist, val);
          if (data.length !== 0) {
            this.setState({ searchDestination: data });
          } else {
            this.setState({ searchDestination: 'empty' });
          }
        }
      );
    } else {
      this.setState({
        searchDestination: '',
      });
    }
  };

  filterValuePart = (arr, part) => {
    return arr.filter(function(item) {
      const itemData = item.name.toUpperCase();
      const textData = part.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
  };
  // ===================================== SEARCHING AUTOCOMPLETE

  // ORIGIN MODAL FUNCTION
  getLocationModalData = data => {
    const payload = {
      dataDestination: data,
    };
    this.props.dispatch(setReceiverCargo(payload));
    const coordinates = [
      // ini nantinya di ambil dari data pacis box untuk origin dan destination lat lang nya
      {
        latitude: -6.175392,
        longitude: 106.827153,
      },
      {
        latitude: -6.2425,
        longitude: 106.8551,
      },
    ];
    this.setState({
      modalVisible: false,
      dataDestination: data,
      coordinates: coordinates,
    });
  };
  // ORIGIN MODAL FUNCTION

  render() {
    let { searchDestination, destinationBoxlist } = this.state;
    console.log(JSON.stringify(searchDestination));
    return (
      <Grid style={{ flex: 1 }}>
        <Row size={4}>
          <Grid style={{ position: 'absolute', top: 20, left: 20 }}>
            <Col>{this.renderBackButton()}</Col>
          </Grid>
          <Col>
            <MapView
              style={styles.map}
              region={this.state.mapRegion}
              zoomEnabled={true}
              pitchEnabled={true}
              showsUserLocation={true}
              followsUserLocation={true}
              showsCompass={true}
              showsBuildings={true}
              showsTraffic={false}
              showsIndoors={true}
              provider={PROVIDER_GOOGLE}
              onPress={this.onMapPress.bind(this)}
            >
              {this.state.coordinates ? (
                <View>
                  <Marker coordinate={this.state.coordinates[0]} />
                  <Marker coordinate={this.state.coordinates[1]} />
                  <MapViewDirections
                    origin={this.state.coordinates[0]}
                    destination={this.state.coordinates[1]}
                    apikey="AIzaSyDYDTJuz_XzryOkwPjYZUzfIUiDAAplFH0"
                    strokeWidth={3}
                    strokeColor="#0da80d"
                  />
                </View>
              ) : (
                <View></View>
              )}
            </MapView>
          </Col>
        </Row>
        <ScrollView>
          <Row size={6} style={styles.container}>
            {this.state.dataDestination === null ? (
              <Col style={styles.paddingTop10}>
                <View style={styles.marginTop10}>
                  <Text
                    style={{
                      fontFamily: fontExtraBold,
                      fontSize: 22,
                      color: '#000',
                    }}
                  >
                    Destination Location
                  </Text>
                </View>
                <View style={styles.marginTop10}>
                  <TouchableOpacity
                    onPress={() => this.setState({ modalVisible: true })}
                  >
                    <InputCargo
                      icon="destination"
                      editable={false}
                      placeholder="Enter Destination Location"
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    styles.marginTop20,
                    styles.marginBottom10,
                    { alignItems: 'center' },
                  ]}
                >
                  <Image
                    style={{
                      width: deviceWidth - 80,
                      height: deviceHeight / 4,
                    }}
                    resizeMode="center"
                    source={require('../../assets/icons/parachute_box.png')}
                  />
                </View>
              </Col>
            ) : (
              <Col>
                <Row>
                  <Col style={styles.paddingTop10}>
                    <Row style={{ flex: 0 }}>
                      <Col
                        style={[
                          styles.marginTop10,
                          {
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                          },
                        ]}
                      >
                        <Text
                          style={{
                            fontFamily: fontExtraBold,
                            fontSize: 22,
                            color: '#000',
                          }}
                        >
                          Estimation
                        </Text>
                      </Col>
                      <Col
                        style={[
                          styles.marginTop10,
                          { alignItems: 'flex-end', justifyContent: 'center' },
                        ]}
                      >
                        <Text
                          style={{
                            fontFamily: fontReguler,
                            fontSize: 16,
                            color: '#000',
                          }}
                        >
                          8 HOURS
                        </Text>
                      </Col>
                    </Row>
                    <Row style={{ flex: 0 }}>
                      <Col
                        style={{
                          alignItems: 'flex-start',
                          justifyContent: 'center',
                          paddingTop: 5,
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: fontReguler,
                            fontSize: 14,
                            color: '#828282',
                          }}
                        >
                          Deliver your parcel within 8 hours maximum if your
                          order is created before 2 PM for intra-city & 12 PM
                          for inter-city
                        </Text>
                      </Col>
                    </Row>
                    <Row
                      style={[
                        styles.marginTop20,
                        { justifyContent: 'center', flex: 0 },
                      ]}
                    >
                      <Col size={1.5}>
                        <TouchableOpacity onPress={() => console.log('xxxx')}>
                          <Image
                            resizeMode="center"
                            style={{ width: 45, height: 45 }}
                            source={require('../../assets/cargo/icon_pickup_selected_new.png')}
                          />
                        </TouchableOpacity>
                      </Col>
                      <Col
                        size={8.5}
                        style={{ paddingLeft: 20, paddingRight: 10 }}
                      >
                        <TouchableOpacity onPress={() => console.log('xxxx')}>
                          <View>
                            <Text
                              style={{
                                fontFamily: fontBold,
                                fontSize: 18,
                                color: '#222222',
                              }}
                            >
                              {this.state.dataOrigin.name}
                            </Text>
                          </View>
                          <View>
                            <Text
                              style={{
                                color: '#828282',
                                fontFamily: fontReguler,
                                fontSize: 14,
                              }}
                            >
                              {this.state.dataOrigin.description}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </Col>
                    </Row>

                    {/* Dash Line Up */}
                    {/* <Row style={{ justifyContent: 'center', flex: 0, marginTop: 10 }}>
                                            <Col size={1.5}>
                                                <Dash style={{ width: 3, height: 50, flexDirection: 'column', alignSelf: 'center' }} dashStyle={{ borderRadius: 300, overflow: 'hidden' }} dashColor="rgb(224, 224, 224)" dashThickness={8} dashGap={4} dashLength={8} />
                                            </Col>
                                            <Col size={8.5}>
                                                <Text />
                                            </Col>
                                        </Row> */}
                    <Row style={{ marginTop: 5, marginBottom: -15 }}>
                      <Col
                        size={2}
                        style={{
                          justifyContent: 'center',
                          alignItems: 'flex-start',
                        }}
                      >
                        <Image
                          source={require('../../assets/icons/icon_dot_vertical.png')}
                          resizeMode="center"
                          style={{ width: 40, height: 35, marginLeft: 2 }}
                        />
                      </Col>
                      <Col size={8}></Col>
                    </Row>
                    {/* Dash Line Up */}

                    <Row
                      style={[
                        styles.marginTop20,
                        { justifyContent: 'center', flex: 0 },
                      ]}
                    >
                      <Col size={1.5}>
                        <TouchableOpacity
                          onPress={() => this.setState({ modalVisible: true })}
                        >
                          <Image
                            resizeMode="center"
                            style={{ width: 45, height: 45 }}
                            source={require('../../assets/cargo/icon_destination_selected_new.png')}
                          />
                        </TouchableOpacity>
                      </Col>
                      <Col
                        size={8.5}
                        style={{ paddingLeft: 20, paddingRight: 10 }}
                      >
                        <TouchableOpacity
                          onPress={() => this.setState({ modalVisible: true })}
                        >
                          <View>
                            <Text
                              style={{
                                fontFamily: fontBold,
                                fontSize: 18,
                                color: '#222222',
                              }}
                            >
                              {this.state.dataDestination.name}
                            </Text>
                          </View>
                          <View>
                            <Text
                              style={{
                                color: '#828282',
                                fontFamily: fontReguler,
                                fontSize: 14,
                              }}
                            >
                              {this.state.dataDestination.description}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            )}
          </Row>
        </ScrollView>
        <Row style={{ marginBottom: 10 }}>
          <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
            <View style={styles.sectionButton}>
              <ButtonRounded
                label="SELECT PACKAGE SIZE"
                onClick={() => this.props.navigation.navigate('CargoPrice')}
              />
            </View>
          </Col>
        </Row>
        {/* MODAL HERE */}
        <DestinationModal
          origin={
            searchDestination.length == 0 || searchDestination == ''
              ? destinationBoxlist
              : searchDestination
          }
          onPressData={data => this.getLocationModalData(data)}
          onChangeText={data => this.searchingDestination(data)}
          closeModal={() => this.setState({ modalVisible: false })}
          visible={this.state.modalVisible}
        />
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    senderData: state.cargo.senderData,
    listBox: state.cargo.nearestBox,
  };
}
export default connect(mapStateToProps)(CargoDestination);

const styles = StyleSheet.create({
  map: {
    flex: 1,
    zIndex: -10,
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  container: {
    marginLeft: 20,
    marginRight: 20,
  },
  paddingTop10: {
    paddingTop: 10,
  },
  marginTop10: {
    marginTop: 10,
  },
  marginTop20: {
    marginTop: 20,
  },
  marginTop30: {
    marginTop: 30,
  },
  marginBottom10: {
    marginBottom: 10,
  },
});
