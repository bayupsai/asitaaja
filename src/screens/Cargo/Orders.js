import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  Switch,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import HeaderPage from '../../components/HeaderPage';
import { ButtonPlus } from '../../elements/ButtonPlus';
import { ButtonRounded } from '../../elements/Button';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  fontRegulerItalic,
  thameColors,
} from '../../base/constant/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import SenderModal from './Modal/SenderModal';
import RecipientModal from './Modal/RecipientModal';
import { submitPacisBook } from '../../redux/actions/CargoAction';
import { LoadingDefault } from '../../components/LoadingModal';
import AlertModal from '../../components/AlertModal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class OrderCargo extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      dataRecipient: null,
      isFragile: false,
      senderName: this.props.senderData.dataSender.name,
      senderPhone: this.props.senderData.dataSender.phoneNumber,
      recipientName: '',
      recipientPhone: '',
      modalSender: false,
      modalRecipient: false,
      loadingModal: false,
      visibleModal: null,
      errorMessage: '',
    };

    console.log(this.props);
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  toggleFragile = value => {
    this.setState({ isFragile: value });
  };

  //  MODAL FUNCTION
  saveSenderData = () => {
    this.setState({ modalSender: false });
  };
  setSenderData = (type, data) => {
    if (type == 'name') {
      this.setState({ senderName: data });
    } else if (type == 'phone') {
      this.setState({ senderPhone: data });
    }
  };

  saveRecipientData = () => {
    this.setState({ modalRecipient: false });
  };
  setRecipientData = (type, data) => {
    if (type == 'name') {
      this.setState({ recipientName: data });
    } else if (type == 'phone') {
      this.setState({ recipientPhone: data });
    }
  };

  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  _alertModal = (idModal, message) => {
    const { visibleModal } = this.state;
    return (
      <AlertModal
        type="normal"
        isVisible={visibleModal === idModal}
        title={visibleModal == 91 ? 'Warning' : 'Failed'}
        contentText={message}
        onPress={() => this._toggleModal(null)}
        onDismiss={() => this._toggleModal(null)}
      />
    );
  };
  //  MODAL FUNCTION

  doPayment = () => {
    if (
      this.state.senderName == '' ||
      this.state.senderPhone == '' ||
      this.state.recipientName == '' ||
      this.state.recipientPhone == ''
    ) {
      this.setState({
        visibleModal: 91,
        errorMessage: 'Please enter Sender and Recipient contact first',
      });
      // Alert.alert(
      //     'Warning.',
      //     'Please enter Sender and Recipient contact first',
      //     [
      //       {text: 'OK', onPress: () => console.log('OK Pressed')},
      //     ],
      //     { cancelable: false },
      // );
    } else {
      let receiverPhoneNumber = parseInt(this.state.recipientPhone);
      const payloadBooking = {
        packageTypeId: this.props.packageType.id,
        packageSizeId: this.props.packageSize.id,
        originPacisBoxId: this.props.senderData.dataOrigin.id,
        destinationPacisBoxId: this.props.receiverData.dataDestination.id,
        receiverMobileNo: 62 + '' + receiverPhoneNumber,
        origin: 'Note',
        destination: 'Note',
        weight: 0,
        customLong: 0,
        customWidth: 0,
        customHeight: 0,
        notes: 'Note',
        isFragile: this.state.isFragile,
        customerId: 1,
      };

      this.setState({ loadingModal: true });

      console.log(payloadBooking);
      this.props.dispatch(submitPacisBook(payloadBooking)).then(res => {
        this.setState({ loadingModal: false });
        console.log(res);
        if (res.type == 'PACIS_BOOK_SUCCESS') {
          this.props.navigation.navigate('PaymentOrder', { pages: 'cargo' });
        } else {
          this.setState({ visibleModal: 92, errorMessage: res.message });
          // Alert.alert(
          //     'Failed.',
          //     res.message,
          //     [
          //       {text: 'OK', onPress: () => console.log('OK Pressed')},
          //     ],
          //     { cancelable: false },
          // );
        }
      });
    }
  };

  render() {
    const senderData = {
      name: this.state.senderName,
      phone: this.state.senderPhone,
    };
    const recipientData = {
      name: this.state.recipientName,
      phone: this.state.recipientPhone,
    };

    let { errorMessage } = this.state;

    return (
      <View style={styles.container}>
        {/* Alert Modal */}
        {this._alertModal(91, errorMessage)}
        {this._alertModal(92, errorMessage)}
        {/* Alert Modal */}

        <HeaderPage
          title="Shipment Detail"
          callback={this.goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          {/* PICKUP */}
          <Grid style={{ paddingTop: 20 }}>
            <Row>
              <Text style={styles.textTitle}>Sender</Text>
            </Row>
            <Row style={styles.section}>
              {this.state.senderName == '' || this.state.senderPhone == '' ? (
                <Grid>
                  <Col
                    size={8}
                    style={{
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                  >
                    <Text style={styles.textLabel}>Contact Sender</Text>
                  </Col>
                  <Col
                    size={2}
                    style={{ alignItems: 'flex-end', justifyContent: 'center' }}
                  >
                    <ButtonPlus
                      size="small"
                      colorButton="buttonColor"
                      onClick={() => this.setState({ modalSender: true })}
                    />
                  </Col>
                </Grid>
              ) : (
                <Grid>
                  <Col
                    size={4}
                    style={{
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                  >
                    <Text style={styles.textLabel}>
                      {this.state.senderName}
                    </Text>
                  </Col>
                  <Col
                    size={4}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Text style={styles.textLabel}>
                      {this.state.senderPhone}
                    </Text>
                  </Col>
                  <Col
                    size={2}
                    style={{ alignItems: 'flex-end', justifyContent: 'center' }}
                  >
                    <ButtonPlus
                      size="small"
                      colorButton="buttonColor"
                      onClick={() => this.setState({ modalSender: true })}
                    />
                  </Col>
                </Grid>
              )}
            </Row>
            {this.state.senderName == '' || this.state.senderPhone == '' ? (
              <Text style={styles.textWarning}>
                *Please fill in contact sender
              </Text>
            ) : null}
            <Row style={styles.section}>
              <Col
                size={1.5}
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <Image
                  source={require('../../assets/cargo/cargo_origin.png')}
                  resizeMode="center"
                  style={{
                    width: 30,
                    height: 30,
                    tintColor: thameColors.primary,
                  }}
                />
              </Col>
              <Col
                size={8.5}
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <View>
                  <Text style={styles.textInfo}>Estimated Pickup</Text>
                </View>
                <View>
                  <Text style={styles.textlabelBold}>
                    Wed, 21 Jan, 4PM - 5PM
                  </Text>
                </View>
              </Col>
            </Row>
          </Grid>

          {/* DESTINATION */}
          <Grid style={styles.marginTop20}>
            <Row>
              <Text style={styles.textTitle}>Recipient</Text>
            </Row>
            <Row style={styles.section}>
              {this.state.recipientName == '' ||
              this.state.recipientPhone == '' ? (
                <Grid>
                  <Col
                    size={8}
                    style={{
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                  >
                    <Text style={styles.textLabel}>Contact Recipient</Text>
                  </Col>
                  <Col
                    size={2}
                    style={{ alignItems: 'flex-end', justifyContent: 'center' }}
                  >
                    <ButtonPlus
                      size="small"
                      colorButton="buttonColor"
                      onClick={() => this.setState({ modalRecipient: true })}
                    />
                  </Col>
                </Grid>
              ) : (
                <Grid>
                  <Col
                    size={4}
                    style={{
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}
                  >
                    <Text style={styles.textLabel}>
                      {this.state.recipientName}
                    </Text>
                  </Col>
                  <Col
                    size={4}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Text style={styles.textLabel}>
                      {this.state.recipientPhone}
                    </Text>
                  </Col>
                  <Col
                    size={2}
                    style={{ alignItems: 'flex-end', justifyContent: 'center' }}
                  >
                    <ButtonPlus
                      size="small"
                      colorButton="buttonColor"
                      onClick={() => this.setState({ modalRecipient: true })}
                    />
                  </Col>
                </Grid>
              )}
            </Row>
            {this.state.recipientName == '' ||
            this.state.recipientPhone == '' ? (
              <Text style={styles.textWarning}>
                *Please fill in contact recipient
              </Text>
            ) : null}

            <Row style={styles.section}>
              <Col
                size={1.5}
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <Image
                  source={require('../../assets/cargo/cargo_destination.png')}
                  resizeMode="center"
                  style={{
                    width: 30,
                    height: 30,
                    tintColor: thameColors.secondary,
                  }}
                />
              </Col>
              <Col
                size={8.5}
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <View>
                  <Text style={styles.textInfo}>Estimated Arrival</Text>
                </View>
                <View>
                  <Text style={styles.textlabelBold}>
                    Thu, 22 Jan, 08AM - 9AM
                  </Text>
                </View>
              </Col>
            </Row>
          </Grid>

          {/* PACKAGE DETAIL */}
          <Grid style={styles.marginTop20}>
            <Row>
              <Text style={styles.textTitle}>Package Detail</Text>
            </Row>
            <Row style={[styles.section, { paddingTop: 0, paddingBottom: 0 }]}>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>
                      {this.props.packageType.name}
                    </Text>
                  </Col>
                </Row>
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>
                      {this.props.packageSize.name}
                    </Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textLabel}>
                      {this.props.packageSize.code}
                    </Text>
                  </Col>
                </Row>
                <Row style={{ padding: 10, paddingLeft: 0 }}>
                  <Col>
                    <Text style={styles.textlabelBold}>Fragile Item?</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    {/* <Text style={styles.textLabel}>X</Text> */}
                    <Switch
                      onValueChange={this.toggleFragile}
                      value={this.state.isFragile}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>

          {/* TOTAL PAYMENT */}
          <Grid style={styles.marginTop20}>
            <Row>
              <Text style={styles.textTitle}>Total Payment</Text>
            </Row>
            <Row style={[styles.section, { paddingTop: 0, paddingBottom: 0 }]}>
              <Col
                style={{ alignItems: 'flex-start', justifyContent: 'center' }}
              >
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>Shipping Cost</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textLabel}>
                      Rp{this.props.pacisPrice.totalPrice}
                    </Text>
                  </Col>
                </Row>
                <Row
                  style={{
                    padding: 10,
                    paddingLeft: 0,
                    borderBottomColor: '#e2e2e2',
                    borderBottomWidth: 0.5,
                  }}
                >
                  <Col>
                    <Text style={styles.textLabel}>
                      {this.props.packageSize.name} Package
                    </Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textLabel}>
                      Rp{this.props.packageSize.price}
                    </Text>
                  </Col>
                </Row>
                <Row style={{ padding: 10, paddingLeft: 0 }}>
                  <Col>
                    <Text style={styles.textlabelBold}>Price You Pay</Text>
                  </Col>
                  <Col style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textPrice}>
                      Rp
                      {this.props.pacisPrice.totalPrice +
                        this.props.packageSize.price}
                    </Text>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </ScrollView>

        <View style={styles.sectionButtonArea}>
          <View style={styles.sectionButton}>
            <ButtonRounded
              label="CONTINUE PAYMENT"
              onClick={() => this.doPayment()}
              isActive={true}
            />
          </View>
        </View>

        {/* MODAL         */}
        <LoadingDefault isVisible={this.state.loadingModal} />
        <SenderModal
          senderData={senderData}
          visible={this.state.modalSender}
          onChangeText={(type, data) => this.setSenderData(type, data)}
          onClick={() => this.saveSenderData()}
          closeModal={() => this.setState({ modalSender: false })}
        />
        <RecipientModal
          recipientData={recipientData}
          visible={this.state.modalRecipient}
          onChangeText={(type, data) => this.setRecipientData(type, data)}
          onClick={() => this.saveRecipientData()}
          closeModal={() => this.setState({ modalRecipient: false })}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cargo: state.cargo,
    packageSize: state.cargo.boxSelected,
    packageType: state.cargo.documentSelected,
    pacisPrice: state.cargo.pacisPriceData,
    senderData: state.cargo.senderData,
    receiverData: state.cargo.receiverData,
  };
}
export default connect(mapStateToProps)(OrderCargo);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#f4f4f4' },
  content: {
    marginTop: 0,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: deviceHeight / 10,
  },
  textTitle: { color: '#222222', fontFamily: fontExtraBold, fontSize: 18 },
  section: {
    padding: 10,
    backgroundColor: '#ffffff',
    marginTop: 10,
    borderRadius: 5,
  },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 16 },
  textInfo: { color: '#828282', fontFamily: fontReguler, fontSize: 16 },
  textlabelBold: { color: '#222222', fontFamily: fontBold, fontSize: 16 },
  textWarning: {
    color: '#d30000',
    fontFamily: fontRegulerItalic,
    fontSize: 14,
    paddingBottom: 10,
  },
  marginTop20: { marginTop: 20 },
  textPrice: { color: '#a2195b', fontFamily: fontExtraBold, fontSize: 18 },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#f0f0f0',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
});
