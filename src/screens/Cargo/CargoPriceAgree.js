import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { Grid, Row, Col } from 'react-native-easy-grid';
import SendingModal from './Modal/SendingModal';

import { Icon } from 'react-native-elements';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  thameColors,
} from '../../base/constant';
import Modal from 'react-native-modal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class CargoPrice extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modal: null,
      modalVisible: false,
      confirmModal: false,
      modalInformation: false,
    };
  }

  //modal
  showModal = () => {
    this.setState({
      modal: 1,
    });
  };

  toggleModal = visible => {
    this.setState({
      modalVisible: visible,
    });
  };
  //modal

  showHideConfirmModal = data => {
    this.setState({ confirmModal: !data });
  };

  handleBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container} {...this.props}>
        <HeaderPage
          title="Select Package Size"
          callback={this.handleBack}
          {...this.props}
        />
        <SubHeaderPage title="Max 10 Kg for all package sizes" />
        <ScrollView showsVerticalScrollIndicator={true} style={styles.content}>
          <Grid style={{ marginBottom: 5 }}>
            <Col>
              <Text style={styles.textPackage}>Package Sizes</Text>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <Text style={styles.textInfo}>INFO</Text>
            </Col>
          </Grid>
          <Grid>
            <Col style={{ justifyContent: 'center', marginRight: 5 }}>
              <View style={styles.paddingImage}>
                <Col style={styles.colImage}>
                  <Image
                    style={{
                      width: 100,
                      height: 70,
                      resizeMode: 'center',
                      margin: 15,
                      alignItems: 'center',
                    }}
                    source={require('../../assets/img/small.png')}
                  />
                </Col>
              </View>
              <View>
                <Text style={styles.textBold}>Small</Text>
              </View>
              <View>
                <Text style={styles.textPrice}>Rp. 19000</Text>
              </View>
              <View>
                <Text style={styles.textLabel}>20x11x7cm</Text>
              </View>
            </Col>
            <Col style={{ justifyContent: 'center', marginRight: 5 }}>
              <View style={{ marginBottom: 5, marginTop: 5 }}>
                <Col style={styles.colImage}>
                  <Image
                    style={{
                      width: 110,
                      height: 70,
                      resizeMode: 'center',
                      margin: 15,
                      alignItems: 'center',
                    }}
                    source={require('../../assets/img/medium.png')}
                  />
                </Col>
              </View>
              <View>
                <Text style={styles.textBold}>Medium</Text>
              </View>
              <View>
                <Text style={styles.textPrice}>RP. 28000</Text>
              </View>
              <View>
                <Text style={styles.textLabel}>30x20x12cm</Text>
              </View>
            </Col>
          </Grid>
          <Grid style={{ marginTop: 20, marginBottom: 25 }}>
            <Col style={{ justifyContent: 'center', marginRight: 5 }}>
              <View style={styles.paddingImage}>
                <Col style={styles.colImage}>
                  <Image
                    style={{
                      width: 110,
                      height: 70,
                      resizeMode: 'center',
                      margin: 15,
                      alignItems: 'center',
                    }}
                    source={require('../../assets/img/large.png')}
                  />
                </Col>
              </View>
              <View>
                <Text style={styles.textBold}>Large</Text>
              </View>
              <View>
                <Text style={styles.textPrice}>Rp. 40.000</Text>
              </View>
              <View>
                <Text style={styles.textLabel}>35x30x20cm</Text>
              </View>
            </Col>
            <Col style={{ justifyContent: 'center', marginRight: 5 }}>
              <View style={{ marginBottom: 5, marginTop: 5 }}>
                <Col style={styles.colImage}>
                  <Image
                    style={{
                      width: 120,
                      height: 70,
                      resizeMode: 'center',
                      margin: 15,
                      alignItems: 'center',
                    }}
                    source={require('../../assets/img/custom.png')}
                  />
                </Col>
              </View>
              <View>
                <Text style={styles.textBold}>Custom</Text>
              </View>
              <View>
                <Text style={styles.textPrice}>Rp. 0</Text>
              </View>
              <View>
                <Text style={styles.textLabel}>0 cm</Text>
              </View>
            </Col>
          </Grid>
          <Grid style={{ marginBottom: 40, marginTop: 20 }}>
            <Col>
              <Row
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'rgba(0, 140, 154, 0.27)',
                  borderRadius: 10,
                  flex: 0,
                  paddingBottom: 8,
                }}
              >
                <Col
                  size={1}
                  style={{ alignItems: 'flex-start', marginTop: 4 }}
                >
                  <Image
                    style={styles.icon}
                    source={require('../../assets/icons/sending.png')}
                  />
                </Col>
                <Col
                  size={8}
                  style={{
                    alignItems: 'flex-start',
                    paddingLeft: 35,
                    justifyContent: 'center',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.toggleModal(true);
                    }}
                  >
                    <Text
                      style={{
                        color: thameColors.primary,
                        fontFamily: fontBold,
                        fontSize: 14,
                      }}
                    >
                      Books & Documents
                    </Text>
                  </TouchableOpacity>
                </Col>
                <Col size={1} style={{ alignItems: 'flex-start' }}>
                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color={thameColors.primary}
                  />
                </Col>
              </Row>
            </Col>
          </Grid>
          <Grid style={{ marginBottom: 35, marginTop: 25 }}>
            <Row style={styles.roundedButton}>
              <Col
                size={4.2}
                style={{ justifyContent: 'center', alignItems: 'flex-start' }}
              >
                <Text style={styles.roundedLabel}>Shipping Costs</Text>
              </Col>
              <Col
                size={4}
                style={{ justifyContent: 'center', alignItems: 'center' }}
              >
                <Text style={styles.roundedLabel}>Rp5.000</Text>
              </Col>
              <Col
                size={1.3}
                style={{ justifyContent: 'center', alignItems: 'flex-start' }}
              >
                <Image
                  style={{
                    width: 30,
                    height: 28,
                    resizeMode: 'center',
                    alignItems: 'center',
                  }}
                  source={require('../../assets/icons/icon_next.png')}
                />
              </Col>
            </Row>
          </Grid>
          <Modal
            style={styles.modalContainer}
            animationType={'slide'}
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}
            onBackButtonPress={() => this.toggleModal(!this.state.modalVisible)}
            onBackdropPress={() => this.toggleModal(!this.state.modalVisible)}
          >
            <View style={styles.card}>
              <Col
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 10,
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.toggleModal(!this.state.modalVisible);
                  }}
                >
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: '#222222',
                      fontSize: 16,
                    }}
                  >
                    Package Size Information!
                  </Text>
                </TouchableOpacity>
              </Col>
            </View>
          </Modal>
        </ScrollView>
        {/* <TouchableOpacity onPress = {() => {this.toggleModal(true)}}>
                    <Text style = {styles.text}>Open Modal</Text>
                </TouchableOpacity> */}
        {/* <SendingModal confirmModal={this.state.confirmModal} showHideConfirmModal={(data) => this.showHideConfirmModal(data) } closeModal={()=> this.setState({modalVisible: false}) } visible={this.state.modalVisible} /> */}
      </View>
    );
  }
}
export default CargoPrice;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#ffffff' },
  content: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  roundedButton: {
    width: deviceWidth - 40,
    backgroundColor: '#a2195b',
    borderRadius: 25,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  paddingImage: { marginRight: 10, marginBottom: 5, marginTop: 5 },
  colImage: {
    borderColor: '#707070',
    borderWidth: 0.5,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 30,
    height: 40,
    resizeMode: 'center',
    marginRight: 5,
    marginLeft: 15,
    paddingTop: 12,
    paddingBottom: 5,
  },
  textPrice: { color: '#a2195b', fontFamily: fontBold, fontSize: 14 },
  textBold: { color: '#222222', fontFamily: fontBold, fontSize: 14 },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 14 },
  textInfo: { color: '#0066b3', fontFamily: fontReguler, fontSize: 16 },
  textPackage: { color: '#222222', fontFamily: fontBold, fontSize: 16 },
  roundedLabel: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: fontExtraBold,
    paddingLeft: 5,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    marginLeft: 15,
    marginRight: 15,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height: deviceHeight / 2,
    width: '100%',
    borderRadius: 5,
  },
});
