import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { ButtonRounded } from '../../elements/Button';
import SendingModal from './Modal/SendingModal';
import InfoModal from './Modal/InfoModal';
import { Icon } from 'react-native-elements';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  thameColors,
} from '../../base/constant';
import Modal from 'react-native-modal';
import numeral from 'numeral';
import { connect } from 'react-redux';
import {
  setPackageSize,
  getPacisPricing,
  setReceiverCargo,
  setDocument,
} from '../../redux/actions/CargoAction';
import AlertModal from '../../components/AlertModal';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class CargoPrice extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modal: null,
      modalVisible: false,
      confirmModal: false,
      modalInfo: false,
      typeOfDocuments: null,
      typeOfPackageSize: [
        {
          id: 3,
          name: 'Small',
          code: '20x11x7cm',
          weight: 5,
          price: <Text>{numeral(19000).format('0,0')}</Text>,
        },
        {
          id: 1,
          name: 'Large',
          code: '35x30x20cm',
          weight: 15,
          price: <Text>{numeral(40000).format('0,0')}</Text>,
        },
        {
          id: 2,
          name: 'Medium',
          code: '30x20x12cm',
          weight: 10,
          price: <Text>{numeral(28000).format('0,0')}</Text>,
        },
        {
          id: 4,
          name: 'Custom',
          code: '0cm',
          weight: 0,
          price: <Text>{numeral(0).format('0,0')}</Text>,
        },
      ],
      boxSelected: null,
    };
  }

  // MODAL
  hideModal = index => {
    if (index === 9) {
      this.setState({ modal: null });
      console.log('Ok Pressed');
    }
  };
  toggleModal(visible) {
    this.setState({ modalInfo: visible });
  }
  showHideConfirmModal = (data, isAgree, getValue) => {
    if (isAgree) {
      this.setState({
        confirmModal: !data,
        modalVisible: false,
        typeOfDocuments: getValue,
      });
      this.props.dispatch(setDocument(getValue));
    } else {
      this.setState({ confirmModal: !data, typeOfDocuments: getValue });
    }
  };
  // MODAL

  handleBack = () => {
    this.props.navigation.goBack();
  };

  setBoxSelected = data => {
    console.log(data);
    this.setState({ boxSelected: data });
    this.props.dispatch(setPackageSize(data));
  };

  renderBoxSize = index => {
    let boxs = [
      this.state.typeOfPackageSize[index - 1],
      this.state.typeOfPackageSize[index],
    ];
    return (
      <Col key={index} style={{ justifyContent: 'center' }}>
        {boxs.map((data, index) => {
          return (
            <TouchableOpacity
              key={index}
              onPress={() => this.setBoxSelected(data)}
            >
              <Col style={{ justifyContent: 'center', marginBottom: 20 }}>
                <View style={styles.paddingImage}>
                  <Col style={styles.colImage}>
                    {data.name == 'Large' ? (
                      <Image
                        style={{ height: 70, resizeMode: 'center' }}
                        source={require('../../assets/img/large.png')}
                      />
                    ) : data.name == 'Medium' ? (
                      <Image
                        style={{ height: 70, resizeMode: 'center' }}
                        source={require('../../assets/img/medium.png')}
                      />
                    ) : data.name == 'Small' ? (
                      <Image
                        style={{ height: 70, resizeMode: 'center' }}
                        source={require('../../assets/img/small.png')}
                      />
                    ) : (
                      <Image
                        style={{ height: 70, resizeMode: 'center' }}
                        source={require('../../assets/img/custom.png')}
                      />
                    )}
                  </Col>
                  {this.state.boxSelected &&
                  this.state.boxSelected.id === data.id ? (
                    <Image
                      source={require('../../assets/cargo/icon_box_selected.png')}
                      style={{
                        height: 20,
                        width: 20,
                        resizeMode: 'center',
                        position: 'absolute',
                        top: 5,
                        right: 10,
                      }}
                    />
                  ) : null}
                </View>
                <View>
                  <Text style={styles.textBold}>{data.name}</Text>
                </View>
                <View>
                  <Text style={styles.textPrice}>Rp. {data.price}</Text>
                </View>
                <View>
                  <Text style={styles.textLabel}>{data.code}</Text>
                </View>
              </Col>
            </TouchableOpacity>
          );
        })}
      </Col>
    );
  };

  continuePayment = () => {
    console.log(this.props);
    const senderData = this.props.cargo.senderData;
    const recipientData = this.props.cargo.receiverData;
    const boxData = this.props.cargo.boxSelected;
    if (this.state.boxSelected != null && this.state.typeOfDocuments != null) {
      const payload = {
        originZipCode: senderData.dataOrigin.zip_code,
        destinationZipCode: recipientData.dataDestination.zip_code,
        packageSizeId: boxData.id,
      };
      this.props.dispatch(getPacisPricing(payload)).then(res => {
        console.log(res);
        this.props.navigation.navigate('OrderCargo');
      });
    } else {
      this.hideModal(9);
      this.setState({ modal: 9 });
    }
  };

  render() {
    const boxPrice = this.state.boxSelected ? this.state.boxSelected.price : 0;
    const totPrice = this.props.pacisPrice.totalPrice;
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Select Package Size"
          callback={this.handleBack}
          {...this.props}
        />
        <SubHeaderPage title="Max 10 Kg for all package sizes" />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          <Grid style={{ marginBottom: 5, justifyContent: 'space-between' }}>
            <Col>
              <Text style={styles.textPackage}>Package Sizes</Text>
            </Col>
            <Col style={{ alignItems: 'flex-end' }}>
              <TouchableOpacity
                onPress={() => this.setState({ modalInfo: true })}
              >
                <Text style={styles.textInfo}>INFO</Text>
              </TouchableOpacity>
            </Col>
          </Grid>

          <Grid>
            {this.state.typeOfPackageSize.map((data, index) => {
              return index % 2 ? (
                this.renderBoxSize(index)
              ) : (
                <View key={index}></View>
              );
            })}
          </Grid>

          <Grid style={{ marginTop: 10, marginBottom: 20 }}>
            <Col>
              <Row
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#e2f3fa',
                  borderRadius: 7,
                  paddingTop: 7,
                  borderWidth: 0.5,
                  borderColor: '#bfe2e5',
                  paddingBottom: 8,
                }}
              >
                <Col
                  size={1}
                  style={{ alignItems: 'flex-start', marginTop: 4 }}
                >
                  <Image
                    style={styles.icon}
                    source={require('../../assets/icons/sending.png')}
                  />
                </Col>
                <Col
                  size={8}
                  style={{
                    alignItems: 'flex-start',
                    paddingLeft: 35,
                    justifyContent: 'center',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ modalVisible: true })}
                  >
                    <Text
                      style={{
                        color: thameColors.primary,
                        fontFamily: fontBold,
                        fontSize: 14,
                      }}
                    >
                      {this.state.typeOfDocuments
                        ? this.state.typeOfDocuments.name
                        : 'What Are You Sending Package?'}
                    </Text>
                  </TouchableOpacity>
                </Col>
                <Col size={1} style={{ alignItems: 'flex-start' }}>
                  <TouchableOpacity
                    onPress={() => this.setState({ modalVisible: true })}
                  >
                    <Icon
                      name="ios-arrow-forward"
                      type="ionicon"
                      color={thameColors.primary}
                      size={22}
                    />
                  </TouchableOpacity>
                </Col>
              </Row>
            </Col>
          </Grid>
        </ScrollView>

        <View style={styles.sectionButtonArea}>
          <View style={styles.sectionButton}>
            {/* <ButtonRoundedPrice cost={totPrice} onClick={() => this.continuePayment()} /> */}
            <ButtonRounded
              label="CONTINUE"
              onClick={() => this.continuePayment()}
            />
          </View>
        </View>

        {/* MODAL */}
        <SendingModal
          {...this.props}
          confirmModal={this.state.confirmModal}
          showHideConfirmModal={(data, isAgree, getValue) =>
            this.showHideConfirmModal(data, isAgree, getValue)
          }
          closeModal={() => this.setState({ modalVisible: false })}
          selectedDocument={this.state.typeOfDocuments}
          visible={this.state.modalVisible}
        />

        <InfoModal
          visible={this.state.modalInfo}
          onClick={() => this.setState({ modalInfo: false })}
        />

        <AlertModal
          type="normal"
          isVisible={this.state.modal === 9}
          title="Warning"
          contentText="Please select package size and package type first"
          onPress={() => this.setState({ modal: null })}
          onDismiss={() => this.setState({ modal: null })}
        />
        {/* MODAL */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cargo: state.cargo,
    pacisPrice: state.cargo.pacisPriceData,
  };
}
export default connect(mapStateToProps)(CargoPrice);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#ffffff' },
  content: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: deviceHeight / 10,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 5,
  },
  paddingImage: { marginRight: 10, marginBottom: 5, marginTop: 5 },
  colImage: {
    borderColor: '#707070',
    borderWidth: 0.3,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    marginRight: 5,
  },
  icon: {
    width: 30,
    tintColor: thameColors.primary,
    height: 25,
    resizeMode: 'center',
    marginRight: 5,
    marginLeft: 15,
    paddingTop: 12,
    paddingBottom: 5,
  },
  textPrice: { color: thameColors.primary, fontFamily: fontBold, fontSize: 16 },
  textBold: { color: '#222222', fontFamily: fontBold, fontSize: 14 },
  textLabel: { color: '#222222', fontFamily: fontReguler, fontSize: 14 },
  textInfo: { color: '#0066b3', fontFamily: fontReguler, fontSize: 16 },
  textPackage: { color: '#222222', fontFamily: fontBold, fontSize: 16 },
  textReguler: { fontFamily: fontReguler, fontSize: 14, color: '#222222' },
  roundedLabel: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: fontExtraBold,
    paddingLeft: 5,
  },
  roundedButton: {
    width: deviceWidth - 40,
    height: 60,
    backgroundColor: '#a2195b',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 60,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height: deviceHeight / 1.5,
    width: '100%',
    borderRadius: 5,
  },
  center: { justifyContent: 'center', alignItems: 'center' },
});
