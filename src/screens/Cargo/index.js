import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  PermissionsAndroid,
} from 'react-native';
import { Icon } from 'react-native-elements';
// import MapView, { PROVIDER_GOOGLE } from "react-native-maps"
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  fontExtraBold,
  fontBold,
  fontReguler,
  thameColors,
} from '../../base/constant/index';
import { InputCargo } from '../../elements/TextInput';
import { ButtonRounded } from '../../elements/Button';
import OriginModal from './Modal/Origin';
import SenderModal from './Modal/SenderModal';
import { connect } from 'react-redux';
import { setSenderCargo, getBoxlist } from '../../redux/actions/CargoAction';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class Cargo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapRegion: this.props.global.geolocationRegion,
      lastLat: this.props.global.geolocationLat,
      lastLong: this.props.global.geolocationLang,
      modalVisible: false,
      modalSender: false,
      originBoxlist: this.props.listBox,
      dataOrigin: null,
      searchOrigin: [],
      senderName: '',
      senderPhone: '',
      originIsTyping: false,
    };

    this.getListOfDropbox();
  }

  getListOfDropbox = () => {
    this.props.dispatch(getBoxlist()).then(res => {
      console.log(res);
      if (res.type == 'FETCH_LISTBOX_SUCCESS') {
        this.setState({ originBoxlist: res.data });
      }
    });
  };

  resetValueOfState = () => {
    this.setState({
      dataOrigin: null,
    });
    this.props.navigation.goBack();
  };

  onMapPress(e) {
    console.log('Map pressed');
    // alert("onMapPress")
    // console.log(e.nativeEvent.coordinate.longitude);
    // let region = {
    //   latitude:       e.nativeEvent.coordinate.latitude,
    //   longitude:      e.nativeEvent.coordinate.longitude,
    //   latitudeDelta:  0.00922*1.5,
    //   longitudeDelta: 0.00421*1.5
    // }
    // this.onRegionChange(region, region.latitude, region.longitude);
  }

  renderBackButton = () => {
    return (
      <TouchableOpacity
        onPress={() => this.resetValueOfState()}
        style={{
          backgroundColor: 'white',
          width: 40,
          height: 40,
          borderRadius: 20,
        }}
      >
        <Icon
          name="ios-arrow-round-back"
          color={thameColors.superBack}
          size={42}
          type="ionicon"
        />
      </TouchableOpacity>
    );
  };

  // ORIGIN MODAL FUNCTION
  getLocationModalData = data => {
    console.log(data);
    this.setState({ modalVisible: false, dataOrigin: data });
  };
  // ORIGIN MODAL FUNCTION

  //AOUTO COMPLATE
  searchingOrigin = val => {
    if (val !== '') {
      this.setState(
        {
          searchOrigin: '',
        },
        () => {
          let data = this.filterValuePart(this.state.originBoxlist, val);
          if (data.length !== 0) {
            this.setState({ searchOrigin: data });
          } else {
            this.setState({ searchOrigin: 'empty' });
          }
        }
      );
    } else {
      this.setState({
        searchOrigin: '',
      });
    }
  };
  filterValuePart = (arr, part) => {
    return arr.filter(function(item) {
      const itemData = item.name.toUpperCase();
      const textData = part.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
  };
  //AOUTO COMPLATE

  // SENDER MODAL FUNCTION
  saveSenderData = () => {
    this.setState({ modalSender: false });
  };
  setSenderData = (type, data) => {
    if (type == 'name') {
      this.setState({ senderName: data });
    } else if (type == 'phone') {
      this.setState({ senderPhone: data });
    }
  };
  // SENDER MODAL FUNCTION

  setPickupDetail = () => {
    console.log(this.state);
    const payload = {
      dataOrigin: this.state.dataOrigin,
      dataSender: {
        name: this.state.senderName,
        phoneNumber: this.state.senderPhone,
      },
    };
    this.props.dispatch(setSenderCargo(payload));
    this.props.navigation.navigate('CargoDestination');
  };

  render() {
    const senderData = {
      name: this.state.senderName,
      phone: this.state.senderPhone,
    };
    console.log(JSON.stringify(this.state.searchOrigin));
    let { searchOrigin, originBoxlist } = this.state;

    return (
      <Grid style={{ flex: 1 }}>
        <Row size={5}>
          <Grid style={{ position: 'absolute', top: 20, left: 20 }}>
            <Col>{this.renderBackButton()}</Col>
          </Grid>
          <Col>
            <MapView
              style={styles.map}
              provider={PROVIDER_GOOGLE}
              region={this.state.mapRegion}
              zoomEnabled={true}
              pitchEnabled={true}
              showsUserLocation={true}
              followsUserLocation={true}
              showsCompass={true}
              showsBuildings={true}
              showsTraffic={false}
              showsIndoors={true}
              onPress={this.onMapPress.bind(this)}
            ></MapView>
          </Col>
        </Row>
        <Row size={5} style={styles.container}>
          {this.state.dataOrigin === null ? (
            <Col style={styles.paddingTop10}>
              <View style={styles.marginTop10}>
                <Text
                  style={{
                    fontFamily: fontExtraBold,
                    fontSize: 22,
                    color: thameColors.superBack,
                  }}
                >
                  Pickup Location
                </Text>
              </View>
              <View style={styles.marginTop10}>
                <TouchableOpacity
                  onPress={() => this.setState({ modalVisible: true })}
                >
                  <InputCargo
                    editable={false}
                    placeholder="Enter Pickup Location"
                  />
                </TouchableOpacity>
              </View>
              <View
                style={[
                  styles.marginTop20,
                  styles.marginBottom10,
                  { alignItems: 'center' },
                ]}
              >
                <Image
                  style={{ width: deviceWidth - 80, height: deviceHeight / 4 }}
                  resizeMode="center"
                  source={require('../../assets/cargo/cargo_image.png')}
                />
              </View>
            </Col>
          ) : (
            <Col>
              <Row size={8}>
                <Col style={styles.paddingTop10}>
                  <Row style={{ flex: 0 }}>
                    <Col style={styles.marginTop10}>
                      <Text
                        style={{
                          fontFamily: fontExtraBold,
                          fontSize: 22,
                          color: thameColors.superBack,
                        }}
                      >
                        Pickup Details
                      </Text>
                    </Col>
                  </Row>
                  <Row
                    style={[
                      styles.marginTop20,
                      { justifyContent: 'center', flex: 0 },
                    ]}
                  >
                    <Col size={1.5}>
                      <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: true })}
                      >
                        <Image
                          resizeMode="center"
                          style={{
                            width: 45,
                            height: 45,
                            backgroundColor: thameColors.primary,
                          }}
                          source={require('../../assets/cargo/icon_pickup_selected_new.png')}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col
                      size={8.5}
                      style={{ paddingLeft: 20, paddingRight: 10 }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: true })}
                      >
                        <View>
                          <Text
                            style={{
                              fontFamily: fontBold,
                              fontSize: 18,
                              color: thameColors.textBlack,
                            }}
                          >
                            {this.state.dataOrigin.name}
                          </Text>
                        </View>
                        <View>
                          <Text
                            style={{
                              color: thameColors.darkGray,
                              fontFamily: fontReguler,
                              fontSize: 16,
                            }}
                          >
                            {this.state.dataOrigin.description}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </Col>
                  </Row>
                  <Row
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: thameColors.whiteOcean,
                      borderRadius: 7,
                      flex: 0,
                      marginTop: 20,
                      paddingTop: 10,
                      paddingBottom: 10,
                    }}
                  >
                    <Col size={1.5} style={{ alignItems: 'center' }}>
                      <Icon
                        name="adduser"
                        type="antdesign"
                        color={thameColors.primary}
                      />
                    </Col>
                    <Col
                      size={7}
                      style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setState({ modalSender: true })}
                      >
                        <Text
                          style={{
                            color: thameColors.primary,
                            fontFamily: fontBold,
                            fontSize: 16,
                          }}
                        >
                          {this.state.senderName
                            ? this.state.senderName
                            : 'Add Sender Contact'}
                        </Text>
                      </TouchableOpacity>
                    </Col>
                    <Col size={1.5} style={{ alignItems: 'center' }}>
                      <Icon
                        name="ios-arrow-forward"
                        type="ionicon"
                        color={thameColors.primary}
                        size={22}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row size={2}>
                <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <View style={styles.sectionButton}>
                    <ButtonRounded
                      label="NEXT DESTINATION"
                      onClick={() => this.setPickupDetail()}
                    />
                  </View>
                </Col>
              </Row>
            </Col>
          )}
        </Row>

        {/* MODAL HERE */}
        <OriginModal
          origin={
            searchOrigin == '' || searchOrigin.length == 0
              ? originBoxlist
              : searchOrigin
          }
          onChangeText={data => this.searchingOrigin(data)}
          onPressData={data => this.getLocationModalData(data)}
          closeModal={() => this.setState({ modalVisible: false })}
          onClick={() => this.setState({ modalVisible: false })}
          visible={this.state.modalVisible}
        />

        <SenderModal
          senderData={senderData}
          visible={this.state.modalSender}
          onChangeText={(type, data) => this.setSenderData(type, data)}
          onClick={() => this.saveSenderData()}
          closeModal={() => this.setState({ modalSender: false })}
        />
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    cargo: state.cargo,
    global: state.global,
    listBox: state.cargo.nearestBox,
  };
}
export default connect(mapStateToProps)(Cargo);

const styles = StyleSheet.create({
  map: {
    flex: 1,
    zIndex: -10,
  },
  sectionButton: {
    width: deviceWidth,
    height: 60,
    backgroundColor: thameColors.white,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  container: {
    marginLeft: 20,
    marginRight: 20,
  },
  paddingTop10: {
    paddingTop: 10,
  },
  marginTop10: {
    marginTop: 10,
  },
  marginTop20: {
    marginTop: 20,
  },
  marginTop30: {
    marginTop: 30,
  },
  marginBottom10: {
    marginBottom: 10,
  },
});
