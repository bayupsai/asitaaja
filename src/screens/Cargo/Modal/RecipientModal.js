import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Modal from 'react-native-modal';
import numeral from 'numeral';
import { InputText } from '../../../elements/TextInput';

//local component
import { Button } from '../../../elements/Button';
import { fontBold, thameColors } from '../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const RecipientModal = props => {
  const recipientData = props.recipientData;
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visible}
      onBackButtonPress={props.closeModal}
      onBackdropPress={props.closeModal}
      style={styles.bottomModal}
    >
      <View style={styles.modalPassenger}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#e2e2e2',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={props.closeModal}
          >
            <Col style={{ alignItems: 'center' }}>
              <Text
                style={{ color: '#000', fontSize: 16, fontFamily: fontBold }}
              >
                {' '}
                Recipient Contact
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid style={{ marginTop: 20 }}>
          <Col>
            <InputText
              label="Enter Recipient Name"
              value={recipientData.name}
              onChangeText={value => props.onChangeText('name', value)}
            />
          </Col>
        </Grid>
        <Grid>
          <Col>
            <InputText
              keyboardType="phone-pad"
              maxLength={12}
              label="Enter Phone Number"
              value={recipientData.phone}
              keyboardType="number-pad"
              onChangeText={value => props.onChangeText('phone', value)}
            />
          </Col>
        </Grid>
        <Grid style={{ flex: 0, paddingBottom: 10 }}>
          <Col>
            <Button label="DONE" onClick={props.onClick} />
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default RecipientModal;

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
