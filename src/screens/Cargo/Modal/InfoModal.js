import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';

//local modal\
import { fontReguler, fontBold } from '../../../base/constant/index';
import {
  ButtonRoundedNonActive,
  ButtonRounded,
} from '../../../elements/Button';

//resolution
const window = Dimensions.get('window');
let deviceHeight = window.height;

class DisplayModal extends React.PureComponent {
  render() {
    return (
      <Modal
        style={styles.modalContainer}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackButtonPress={this.props.onClick}
        onBackdropPress={this.props.onClick}
        isVisible={this.props.visible}
        backdropColor="black"
      >
        <View style={styles.card}>
          <Col>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
              }}
            >
              <Text
                style={{ fontFamily: fontBold, color: '#222222', fontSize: 16 }}
              >
                Package Size Information
              </Text>
            </View>
            <View>
              <Col style={{ marginBottom: 5 }}>
                <View style={[styles.center, { marginBottom: 20 }]}>
                  <Text style={styles.textReguler}>
                    Aerobox pricing is based on the{' '}
                  </Text>
                  <Text style={styles.textReguler}>
                    size of the items you wish to ship. There are 4
                  </Text>
                  <Text style={styles.textReguler}>
                    4 different types of Aerobox size:{' '}
                  </Text>
                  <Text style={styles.textReguler}>of Aerobox size:</Text>
                  <Text style={[styles.textReguler, { marginTop: 15 }]}>
                    1. Small (20x11x7cm)
                  </Text>
                  <Text style={styles.textReguler}>2. Medium (30x20x12cm)</Text>
                  <Text style={styles.textReguler}>3. Large (35x30x20cm)</Text>
                  <Text style={styles.textReguler}>4. Custom Size</Text>
                  <Text style={[styles.textReguler, { marginTop: 15 }]}>
                    For items exceeding these dimensions, you{' '}
                  </Text>
                  <Text style={styles.textReguler}>
                    should select the custom size. If you need
                  </Text>
                  <Text style={styles.textReguler}>
                    Aerobox to provide you with packaging,{' '}
                  </Text>
                  <Text style={styles.textReguler}>
                    you can buy our box available in 3 size
                  </Text>
                  <Text style={styles.textReguler}>
                    (Small, Medium and Large)
                  </Text>
                </View>
                <View style={{ marginBottom: 5, marginTop: 10 }}>
                  <ButtonRounded
                    label="NICE INFO"
                    size="small"
                    onClick={this.props.onClick}
                  />
                </View>
              </Col>
            </View>
          </Col>
        </View>
      </Modal>
    );
  }
}

export default DisplayModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  textReguler: { fontFamily: fontReguler, fontSize: 14, color: '#222222' },
  center: { justifyContent: 'center', alignItems: 'center' },
  colBottom: {
    padding: 10,
    borderColor: '#707070',
    borderWidth: 0.5,
    borderTopEndRadius: 5,
    borderTopStartRadius: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  card: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 60,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height: deviceHeight / 1.5,
    width: '100%',
    borderRadius: 5,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#a2195b',
    width: '100%',
    height: 50,
    borderRadius: 20,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
