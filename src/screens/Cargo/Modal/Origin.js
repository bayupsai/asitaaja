import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import {
  fontReguler,
  fontBold,
  fontExtraBold,
} from '../../../base/constant/index';
import { InputCargo } from '../../../elements/TextInput';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const OriginModal = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      onBackButtonPress={() => props.closeModal()}
      onBackdropPress={() => props.closeModal()}
      isVisible={props.visible}
      style={styles.bottomModal}
    >
      <Grid style={styles.modalContent}>
        <Row size={2} style={styles.modalHeader}>
          <Col style={styles.paddingTop10}>
            <View style={styles.marginTop10}>
              <Text
                style={{
                  fontFamily: fontExtraBold,
                  fontSize: 22,
                  color: '#000',
                }}
              >
                Pickup Location
              </Text>
            </View>
            <View style={styles.marginTop10}>
              <TouchableOpacity
                onPress={() => this.setState({ modalVisible: true })}
              >
                <InputCargo
                  editable={true}
                  onChangeText={props.onChangeText}
                  placeholder="Enter Pickup Location"
                />
              </TouchableOpacity>
            </View>
          </Col>
        </Row>
        <Row size={6.5} style={[styles.container, { marginTop: 20 }]}>
          <Col style={styles.paddingTop10}>
            <ScrollView>
              {props.origin !== 'empty' ? (
                props.origin.map((data, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => props.onPressData(data)}
                      key={index}
                      style={{ flex: 0 }}
                    >
                      <Grid style={{ paddingBottom: 10 }}>
                        <Col size={1} style={{ paddingTop: 5 }}>
                          <Image
                            resizeMode="center"
                            style={{ width: 30, height: 30 }}
                            source={require('../../../assets/cargo/cargo_pin_new.png')}
                          />
                        </Col>
                        <Col size={9} style={{ paddingLeft: 10 }}>
                          <Row>
                            <Text
                              style={{
                                fontFamily: fontBold,
                                fontSize: 16,
                                color: '#222222',
                              }}
                            >
                              {data.name}
                            </Text>
                          </Row>
                          <Row>
                            <Col size={7}>
                              <Text
                                style={{
                                  color: '#828282',
                                  fontFamily: fontReguler,
                                  fontSize: 14,
                                }}
                              >
                                {data.description}
                              </Text>
                            </Col>
                            <Col sz={3} style={{ paddingTop: 5 }}>
                              <Text
                                style={{
                                  color: '#828282',
                                  fontFamily: fontReguler,
                                  fontSize: 12,
                                }}
                              >
                                {data.distance}
                              </Text>
                            </Col>
                          </Row>
                          <Row>
                            <View
                              style={{
                                height: 1,
                                width: '100%',
                                borderBottomWidth: 0.5,
                                borderBottomColor: 'rgba(0, 0, 0, 0.1)',
                                marginTop: 20,
                              }}
                            />
                          </Row>
                        </Col>
                      </Grid>
                    </TouchableOpacity>
                  );
                })
              ) : (
                <View>
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: '#222222',
                      fontSize: 16,
                    }}
                  >
                    Location not found
                  </Text>
                </View>
              )}
            </ScrollView>
          </Col>
        </Row>
        <Row size={1.5} style={[styles.container, { alignItems: 'center' }]}>
          <TouchableOpacity onPress={() => props.closeModal()}>
            <View
              style={{
                width: 50,
                height: 50,
                borderRadius: 25,
                borderWidth: 1.5,
                padding: 10,
                borderColor: '#838383',
              }}
            >
              <Icon name="arrowdown" type="antdesign" color="black" />
            </View>
          </TouchableOpacity>
        </Row>
      </Grid>
    </Modal>
  );
};

export default OriginModal;
const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginRight: 20,
  },
  paddingTop10: {
    paddingTop: 10,
  },
  marginTop10: {
    marginTop: 10,
  },
  marginTop20: {
    marginTop: 20,
  },
  marginTop30: {
    marginTop: 30,
  },
  marginBottom10: {
    marginBottom: 10,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    padding: 0,
    // height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: '#ffffff',
    height: 100,
    width: deviceWidth - 40,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontFamily: fontReguler,
    fontSize: 16,
  },
});
