import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import {
  fontReguler,
  fontBold,
  fontExtraBold,
  thameColors,
} from '../../../base/constant/index';
import {
  ButtonRounded,
  ButtonRoundedNonActive,
} from '../../../elements/Button';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const DestinationModal = props => {
  const packageSize = [
    { id: 1, name: 'Fashion & Apparel' },
    { id: 2, name: 'Toys & Hobbies' },
    { id: 3, name: 'Sport & Entertainment' },
    { id: 4, name: 'Health & Beauty' },
    { id: 5, name: 'Non perishable Food & Snacks' },
    { id: 6, name: 'Books & Documents' },
    { id: 7, name: 'Household & Homewares' },
    { id: 8, name: 'Automobile & Motorcycle' },
  ];
  const packageLength = packageSize.length;
  return (
    <View style={{ flex: 1 }}>
      <Modal
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        onBackButtonPress={props.closeModal}
        onBackdropPress={props.closeModal}
        isVisible={props.visible}
        style={styles.bottomModal}
      >
        <Grid style={styles.modalContent}>
          <Row size={2} style={styles.modalHeader}>
            <Col style={styles.paddingTop10}>
              <View
                style={{
                  paddingTop: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: 10,
                }}
              >
                <Text
                  style={{ fontFamily: fontBold, fontSize: 18, color: '#000' }}
                >
                  What Are You Sending?
                </Text>
              </View>
              <View style={styles.marginTop10}>
                <Col style={{ marginTop: 10, marginBottom: 30 }}>
                  <View style={styles.center}>
                    <Text style={styles.textReguler}>
                      Please enter your parcel detail to get
                    </Text>
                    <Text style={styles.textReguler}>shipping cost</Text>
                  </View>
                </Col>
              </View>
            </Col>
          </Row>
          <Row
            size={0.5}
            style={{
              backgroundColor: '#f5f5f5',
              margin: 0,
              padding: 10,
              marginTop: 15,
            }}
          >
            <Col
              size={1}
              style={{ justifyContent: 'center', alignItems: 'flex-end' }}
            >
              <Icon
                name="exclamationcircle"
                type="antdesign"
                size={20}
                color="#d30000"
              />
            </Col>
            <Col
              size={9}
              style={{ justifyContent: 'center', alignItems: 'center' }}
            >
              <View>
                <Text style={styles.textRed}>
                  Prohibited items: toxic, flammable, dangerous
                </Text>
                <Text style={styles.textRed}>
                  Weapon, liquid, food, living animals, plants
                </Text>
              </View>
            </Col>
          </Row>
          <Row size={6.5} style={[styles.container, { marginTop: 20 }]}>
            <Col style={styles.paddingTop10}>
              {packageSize.map((data, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={
                      packageLength == index + 1
                        ? styles.colBottomLast
                        : styles.colBottom
                    }
                    onPress={() =>
                      props.showHideConfirmModal(props.confirmModal, null, data)
                    }
                  >
                    {props.selectedDocument &&
                    props.selectedDocument.id == data.id ? (
                      <View style={{ flexDirection: 'row' }}>
                        <Col size={8}>
                          <Text style={[styles.textChoice, styles.textActive]}>
                            {' '}
                            {data.name}{' '}
                          </Text>
                        </Col>
                        <Col size={2} style={{ alignItems: 'flex-end' }}>
                          <Icon
                            name="check"
                            type="fontawesome5"
                            color={thameColors.primary}
                            size={25}
                          />
                        </Col>
                      </View>
                    ) : (
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={[styles.textChoice]}>{data.name} </Text>
                      </View>
                    )}
                  </TouchableOpacity>
                );
              })}
            </Col>
          </Row>
        </Grid>
      </Modal>

      <Modal
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        isVisible={props.confirmModal}
        onBackButtonPress={props.closeModal}
        onBackdropPress={props.closeModal}
        style={styles.modalContainer}
      >
        <View style={styles.card}>
          <Col>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
              }}
            >
              <Text
                style={{ fontFamily: fontBold, color: '#d30000', fontSize: 16 }}
              >
                DISCLAMER!
              </Text>
            </View>
            <View>
              <Col style={{ marginBottom: 5 }}>
                <View style={[styles.center, { marginBottom: 20 }]}>
                  <Text style={styles.textReguler}>
                    By pressing agree, I hereby certify
                  </Text>
                  <Text style={styles.textReguler}>
                    that the information on this application
                  </Text>
                  <Text style={styles.textReguler}>
                    is true and correct and the contents
                  </Text>
                  <Text style={styles.textReguler}>
                    and value of this shipment are as stated.
                  </Text>
                  <Text style={styles.textReguler}>
                    Aeroaja will not be liable for any legal
                  </Text>
                  <Text style={styles.textReguler}>
                    violations shall there be{' '}
                  </Text>
                  <Text style={styles.textReguler}>
                    any act of non compliance{' '}
                  </Text>
                </View>
                <View
                  style={{
                    marginBottom: 5,
                    marginTop: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <ButtonRounded
                    label="I AM AGREE"
                    size="small"
                    onClick={() =>
                      props.showHideConfirmModal(
                        props.confirmModal,
                        'agree',
                        props.selectedDocument
                      )
                    }
                  />
                </View>
                <View
                  style={{
                    marginTop: 5,
                    marginTop: 5,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <ButtonRoundedNonActive
                    label="DISAGREE"
                    size="small"
                    color="buttonColor"
                    onClick={() =>
                      props.showHideConfirmModal(
                        props.confirmModal,
                        null,
                        props.selectedDocument
                      )
                    }
                  />
                </View>
              </Col>
            </View>
          </Col>
        </View>
      </Modal>
    </View>
  );
};

export default DestinationModal;
const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginRight: 20,
  },
  paddingTop10: {
    paddingTop: 10,
  },
  marginTop10: {
    marginTop: 10,
  },
  marginTop20: {
    marginTop: 20,
  },
  marginTop30: {
    marginTop: 30,
  },
  marginBottom10: {
    marginBottom: 10,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    marginTop: 20,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 1.8,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: '#ffffff',
    height: 100,
    width: deviceWidth - 40,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontFamily: fontReguler,
    fontSize: 16,
  },
  textReguler: { fontFamily: fontReguler, fontSize: 16, color: '#222222' },
  textRed: { fontFamily: fontReguler, fontSize: 14, color: '#d30000' },
  textOther: {
    color: '#828282',
    fontFamily: fontReguler,
    fontSize: 14,
    marginLeft: 5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  textChoice: { fontFamily: fontReguler, fontSize: 16, color: '#222222' },
  textActive: { color: thameColors.primary, fontFamily: fontExtraBold },
  center: { justifyContent: 'center', alignItems: 'center' },
  colBottom: {
    padding: 10,
    borderColor: '#707070',
    borderWidth: 0.4,
    borderBottomWidth: 0,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  colBottomLast: {
    padding: 10,
    borderColor: '#707070',
    borderWidth: 0.4,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  colTop: {
    padding: 10,
    borderColor: '#707070',
    borderWidth: 0.5,
    borderBottomEndRadius: 5,
    borderBottomStartRadius: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  colCenter: {
    padding: 10,
    borderColor: '#707070',
    borderWidth: 0.5,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  box: {
    padding: 10,
    borderColor: '#707070',
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  card: {
    marginLeft: 15,
    marginRight: 15,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height: deviceHeight / 1.8,
    width: '100%',
    borderRadius: 5,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
