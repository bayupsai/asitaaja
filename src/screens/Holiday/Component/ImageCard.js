import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontReguler } from '../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const ImageCard = props => (
  <View>
    <ImageBackground
      style={styles.content}
      source={{
        uri:
          'https://pesona.travel/files/Uploads/Salinan%2011_Nglambor%20Beach_UNDERWATER_DNS_11.jpg',
      }}
      resizeMode="stretch"
    >
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: 'rgba(0, 0, 0, 0.3)',
          padding: 15,
        }}
      >
        <Icon
          name="ios-pin"
          color="#fff"
          size={23}
          style={{ marginRight: 10 }}
        />
        <Text style={{ color: '#fff', fontSize: 20, fontFamily: fontReguler }}>
          Gunung Kidul
        </Text>
      </View>
    </ImageBackground>
    <View>
      <Text style={{ fontFamily: fontReguler }}>Welcome</Text>
    </View>
  </View>
);

export default ImageCard;

const styles = StyleSheet.create({
  content: {
    width: deviceWidth / 1.07,
    height: deviceHeight / 3,
    borderRadius: 25,
    flex: 1,
  },
});
