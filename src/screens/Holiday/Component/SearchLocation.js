import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { fontReguler } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
let deviceWidth = window.width;

const SearchLocation = props => (
  <TouchableOpacity
    style={styles.SearchLocation}
    onPress={() => alert('Hello Location')}
    placeholder="Where do you want to go?"
  >
    <Icon name="location-pin" color="#FFC107" size={20} />
    <Text style={styles.sectionText}>{props.whereToGo}</Text>
  </TouchableOpacity>
);

export default SearchLocation;

const styles = StyleSheet.create({
  SearchLocation: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.15,
    height: 'auto',
    borderRadius: 10,
    flexDirection: 'row',
    padding: 20,
    paddingLeft: 20,
    marginBottom: -10,
    // box shadow
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    transform: [{ translateY: -20 }],
  },
  sectionText: {
    paddingLeft: 10,
    fontFamily: fontReguler,
  },
});
