import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';

// imported components
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import SearchLocation from './Component/SearchLocation';
import ImageCard from './Component/ImageCard';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

export default class Holiday extends PureComponent {
  constructor(props) {
    super(props);
  }

  _handleBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.contaier}>
        <HeaderPage
          title="Holiday"
          callback={this._handleBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage title="Get your holiday" />
          <View style={styles.content}>
            <SearchLocation whereToGo="Where do you want to go?" />
            <ImageCard />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contaier: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    width: deviceWidth,
  },
});
