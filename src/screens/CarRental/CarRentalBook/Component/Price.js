import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import { fontReguler } from '../../../../base/constant/index';

class Price extends React.PureComponent {
  render() {
    return (
      <View style={styles.card}>
        <View style={{ padding: 10 }}>
          <Text
            style={{ color: '#009688', fontSize: 18, fontFamily: fontReguler }}
          >
            Price Details
          </Text>
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderColor: '#BDBDBD',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              borderBottomWidth: 2,
              borderColor: '#009688',
              width: '25%',
              marginLeft: 15,
            }}
          />
        </View>
        <Grid style={{ padding: 10 }}>
          <Row>
            <Col size={6}>
              <Text
                style={{ color: '#000', fontSize: 20, fontFamily: fontReguler }}
              >
                Daihatsu All New Xenia
              </Text>
              <Text style={{ fontFamily: fontReguler }}>
                Bali Fast Track (1 day trip)
              </Text>
            </Col>
            <Col
              size={4}
              style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}
            >
              <Text style={{ fontFamily: fontReguler }}>Rp550,000</Text>
            </Col>
          </Row>
          <Row>
            <Col>
              <Dash
                style={{
                  width: '100%',
                  height: 1,
                  marginTop: 20,
                  marginBottom: 15,
                }}
                dashColor="#BDBDBD"
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Text
                style={{ color: '#000', fontSize: 18, fontFamily: fontReguler }}
              >
                Price Your Pay
              </Text>
            </Col>
            <Col style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
              <Text style={{ color: '#FFC107', fontFamily: fontReguler }}>
                Rp550.000
              </Text>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

export default Price;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    backgroundColor: '#fff',
    borderRadius: 5,
  },
});
