import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Dash from 'react-native-dash';
import { fontReguler } from '../../../../base/constant/index';

class SetPickup extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      SetPickup: [
        { id: 1, title: 'Pick me up at airport', paid: 'Free' },
        {
          id: 2,
          title: 'Pick me up at my desired location',
          paid: 'Free',
        },
      ],
      selectedPickUp: 1,
      locationFocus: false,
    };
  }

  //Location TextInput
  _handleLocationFocus = i => {
    this.setState({ locationFocus: true });
    if (this.props.onLocationFocus) {
      this.props.onLocationFocus(i);
    }
  };
  _handleBlurLocation = i => {
    this.setState({ locationFocus: false });
    if (this.props.onBlurLocation) {
      this.props.onBlurLocation(i);
    }
  };
  //Location TextInput

  //Set Pickup
  _setPickUp1 = i => {
    this.setState({ selectedPickUp: 1 });
  };
  _setPickUp2 = i => {
    this.setState({ selectedPickUp: 2 });
  };
  //Set Pickup

  render() {
    return (
      <View style={styles.card}>
        <View style={{ padding: 10 }}>
          <Text style={{ color: '#009688', fontFamily: fontReguler }}>
            Set Pickup Location
          </Text>
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderColor: '#BDBDBD',
            justifyContent: 'center',
          }}
        >
          <View
            style={{
              borderBottomWidth: 2,
              borderColor: '#009688',
              width: '25%',
              marginLeft: 15,
            }}
          />
        </View>
        <Grid style={{ padding: 10 }}>
          <Row>
            <Col size={2}>
              <Icon
                type="ionicon"
                name={
                  this.state.selectedPickUp == 1
                    ? 'ios-radio-button-on'
                    : 'ios-radio-button-off'
                }
                color="#4CAF50"
              />
            </Col>
            <Col size={8}>
              <TouchableOpacity onPress={this._setPickUp1}>
                <Text style={styles.textBold}>
                  {this.state.SetPickup[0].title}
                </Text>
                <Text style={{ color: '#4CAF50' }}>
                  {this.state.SetPickup[0].paid}
                </Text>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col>
              <Dash
                style={{
                  width: '100%',
                  height: 1,
                  marginTop: 10,
                  marginBottom: 10,
                }}
                dashColor="#BDBDBD"
              />
            </Col>
          </Row>
          <Row>
            <Col size={2}>
              <Icon
                type="ionicon"
                name={
                  this.state.selectedPickUp == 2
                    ? 'ios-radio-button-on'
                    : 'ios-radio-button-off'
                }
                color="#4CAF50"
              />
            </Col>
            <Col size={8}>
              <TouchableOpacity onPress={this._setPickUp2}>
                <Text style={styles.textBold}>
                  {this.state.SetPickup[1].title}
                </Text>
                <Text style={{ color: '#4CAF50', fontFamily: fontReguler }}>
                  {this.state.SetPickup[1].paid}
                </Text>
              </TouchableOpacity>
            </Col>
          </Row>

          {this.state.selectedPickUp == 2 ? (
            <Row style={{ padding: 15, marginTop: 15 }}>
              <Col>
                <Text style={{ color: '#000', fontFamily: fontReguler }}>
                  Location
                </Text>
                <TextInput
                  placeholder="Ubud, Gianyar"
                  selectionColor="#4CAF50"
                  underlineColorAndroid={
                    this.state.locationFocus == true ? '#4CAF50' : '#BDBDBD'
                  }
                  onFocus={this._handleLocationFocus}
                  onBlur={this._handleBlurLocation}
                  style={styles.locationInput}
                  {...this.props}
                />
              </Col>
            </Row>
          ) : (
            <Row>
              <Col></Col>
            </Row>
          )}

          <Row style={{ padding: 15, marginTop: 10 }}>
            <Col>
              <Text style={{ color: '#000', fontFamily: fontReguler }}>
                Additional Notes
              </Text>
              <TextInput
                selectionColor="#4CAF50"
                underlineColorAndroid="#BDBDBD"
                style={styles.locationInput}
                multiline={true}
                numberOfLines={4}
                editable={true}
                maxLength={40}
                {...this.props}
              />
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

export default SetPickup;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    backgroundColor: '#fff',
    borderRadius: 5,
  },
  textBold: {
    color: '#000',
    fontSize: 18,
    fontFamily: fontReguler,
  },
  locationInput: {
    height: 40,
    // paddingLeft: 6
  },
});
