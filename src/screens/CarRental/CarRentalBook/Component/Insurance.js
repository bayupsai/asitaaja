import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { fontReguler } from '../../../../base/constant';

class Insurance extends React.PureComponent {
  render() {
    return (
      <View style={styles.card}>
        <Grid>
          <Col size={2}>
            <Icon
              type="ionicon"
              name="ios-checkmark-circle"
              // type="material-community"
              // name="checkbox-blank-circle-outline"

              color="#009688"
            />
          </Col>
          <Col size={8}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <Text style={{ color: '#000', fontFamily: fontReguler }}>
                Travel Insurance
              </Text>
              <Text style={{ color: '#4CAF50', fontFamily: fontReguler }}>
                Rp. 39.000 <Text style={{ color: '#BDBDBD' }}>/pax</Text>
              </Text>
            </View>
            <View>
              <Text style={{ fontFamily: fontReguler }}>
                Up to Rp300 million coverage for accidents, up top Rp20 million
                for trip cancellation, and up to Rp7.5 million for flight and
                baggage delay
              </Text>
            </View>
            <Text style={{ color: '#2196F3', fontFamily: fontReguler }}>
              More Info
            </Text>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <Text style={{ color: '#FFC107', fontFamily: fontReguler }}>
                Including five others protection
              </Text>
              <Icon
                type="ionicon"
                name="ios-arrow-down"
                // name="ios-arrow-up"
                color="#FFC107"
              />
            </View>
          </Col>
        </Grid>
      </View>
    );
  }
}

export default Insurance;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
  },
});
