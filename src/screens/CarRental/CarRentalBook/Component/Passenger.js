import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';

//import components
import { InputText } from '../../../../elements/TextInput';

class Passenger extends PureComponent {
  constructor(props) {
    super(props);
  }

  changeText = i => {
    console.log(i);
  };

  render() {
    return (
      <View style={styles.sectionContent}>
        <Grid>
          <Col size={3}>
            <InputText
              label="Title"
              placeholder="Mrs."
              onChangeText={this.changeText}
            />
          </Col>
          <Col size={7}>
            <InputText
              label="Full Name"
              placeholder="e.g Amir Revon"
              onChangeText={this.changeText}
            />
          </Col>
        </Grid>
        <Grid>
          <Col size={4}>
            <InputText
              label="Country Code"
              placeholder="(+62)"
              onChangeText={this.changeText}
            />
          </Col>
          <Col size={6}>
            <InputText
              label="Phone Number"
              placeholder="e.g 8282817436"
              onChangeText={this.changeText}
            />
          </Col>
        </Grid>
        <Grid>
          <Col>
            <InputText
              label="ID Numbers"
              placeholder="e.g 123-456-789-0"
              onChangeText={this.changeText}
            />
          </Col>
        </Grid>
      </View>
    );
  }
}

export default Passenger;

const styles = StyleSheet.create({
  sectionContent: {
    marginBottom: 15,
  },
});
