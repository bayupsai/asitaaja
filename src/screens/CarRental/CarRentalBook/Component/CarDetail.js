import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Dash from 'react-native-dash';
import { fontReguler } from '../../../../base/constant';

class CarDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      features: [{ title: 'Refundable' }, { title: 'Reschedule Available' }],
    };
  }
  render() {
    return (
      <Grid style={styles.sectionContent}>
        <Row>
          <Col size={6}>
            <Text style={[styles.textBold, { fontSize: 20 }]}>
              Daihatsu All New Xenia
            </Text>
            <Text style={{ fontSize: 16, fontFamily: fontReguler }}>
              Monday, 21 August 2019
            </Text>
          </Col>
          <Col size={4}>
            <Image
              source={require('../../../../assets/cars/Fakta-Mobil-Anda.png')}
              style={styles.img}
              resizeMode="stretch"
            />
          </Col>
        </Row>
        <Row>
          <Dash
            style={{
              width: '100%',
              height: 1,
              marginTop: 15,
              marginBottom: 15,
            }}
            dashColor="#BDBDBD"
          />
        </Row>
        <Row>
          <Col>
            <Text>
              Provided by:{' '}
              <Text style={styles.textBold}>Bali Fast Track Bali</Text>
            </Text>
            <Text style={{ fontFamily: fontReguler }}>
              Duration: 1 Day - Pickup at 22:00
            </Text>
            <View style={{ marginTop: 15 }}>
              {this.state.features.map((item, i) => (
                <View
                  key={i}
                  style={{ flexDirection: 'row', alignItems: 'center' }}
                >
                  <Icon
                    type="ionicon"
                    color="#4CAF50"
                    name="ios-checkmark-circle"
                  />
                  <Text
                    style={{
                      color: '#4CAF50',
                      paddingLeft: 10,
                      fontFamily: fontReguler,
                    }}
                  >
                    {item.title}
                  </Text>
                </View>
              ))}
            </View>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default CarDetail;

const styles = StyleSheet.create({
  sectionContent: {
    marginTop: -50,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
  },
  img: {
    width: 125,
    height: 100,
    borderRadius: 5,
  },
  textBold: {
    color: '#000',
    fontFamily: fontReguler,
  },
});
