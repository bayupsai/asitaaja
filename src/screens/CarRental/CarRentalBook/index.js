import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

//import components
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import { Button } from '../../../elements/Button';
import CarDetail from './Component/CarDetail';
import Contact from './Component/Contact';
import Passenger from './Component/Passenger';
import SetPickup from './Component/SetPickup';
import Insurance from './Component/Insurance';
import Price from './Component/Price';
import {} from '../../../base/constant/index';

export default class CarRentalBook extends PureComponent {
  constructor(props) {
    super(props);
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Car Rental"
          callback={this._goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage title="Your Booking" />
          <CarDetail />

          {/* Contact Information */}
          <Text
            style={[
              styles.textBold,
              {
                marginTop: 25,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 10,
                fontFamily: fontReguler,
              },
            ]}
          >
            Contact Information (for E-Ticket)
          </Text>
          <Contact />
          {/* //Contact Information */}

          {/* Passenger Details */}
          <Text
            style={[
              styles.textBold,
              {
                marginTop: 25,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 10,
                fontFamily: fontReguler,
              },
            ]}
          >
            Passenger Details
          </Text>
          <Passenger />
          {/* //Passenger Details */}

          {/* Set Pickup Location */}
          <SetPickup />
          {/* //Set Pickup Location */}

          {/* Insurance */}
          <Insurance />
          {/* //Insurance */}

          {/* Price */}
          <Price />
          {/* //Price */}

          {/* Terms Conditions */}
          <View style={styles.card}>
            <Text style={{ fontFamily: fontReguler }}>
              By Clicking this Button, you acknowledge that you
              <Text style={{ color: '#03A9F4', fontFamily: fontReguler }}>
                {' '}
                Terms & Conditions{' '}
              </Text>
              and
              <Text style={{ color: '#03A9F4', fontFamily: fontReguler }}>
                {' '}
                Privacy Policy{' '}
              </Text>
              Of Garuda Mall
            </Text>
          </View>
          {/* //Terms Conditions */}

          <View
            style={{
              marginBottom: 25,
            }}
          >
            <Button
              label="Continue to Payment"
              onClick={() => alert('Welcome to Payment!')}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  textBold: {
    fontSize: 20,
    color: '#000',
  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 25,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
  },
});
