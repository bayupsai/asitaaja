import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';

//components
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import FindLocation from './Component/FindLocation';
import { InputText } from '../../elements/TextInput';
import { Button } from '../../elements/Button';
//components

export default class CarRental extends PureComponent {
  constructor(props) {
    super(props);
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _carRentalResult = () => {
    this.props.navigation.navigate('CarRentalResult');
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Car Rental"
          callback={this._goBack}
          {...this.props}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ paddingBottom: 10 }}
        >
          <SubHeaderPage title="Book your car" />
          <FindLocation onChangeText={i => console.log(i)} />
          <Grid style={{ marginTop: 10, marginBottom: 5 }}>
            <Col>
              <InputText
                label="Rental Start Date"
                placeholder="Tuesday, Dec 6"
                onChangeText={i => console.log(i)}
              />
              <InputText
                label="Pick Up Time"
                placeholder="22:00"
                onChangeText={i => console.log(i)}
              />
              <InputText
                label="Duration"
                placeholder="1 Day"
                onChangeText={i => console.log(i)}
              />
            </Col>
          </Grid>
        </ScrollView>
        <View style={{ alignItems: 'center', marginBottom: 5 }}>
          <Button onClick={this._carRentalResult} label="Press" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
});
