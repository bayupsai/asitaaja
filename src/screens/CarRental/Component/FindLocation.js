import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';

//component
import { SearchHotelInput } from '../../../elements/TextInput';
//component

const FindLocation = props => {
  return (
    <Grid style={styles.sectionContent}>
      <Col
        style={[
          styles.sectionBody,
          {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          },
        ]}
      >
        <Icon
          type="font-awesome"
          name="map-marker"
          color="#FFA000"
          style={{ padding: 10 }}
        />
        <SearchHotelInput
          placeholder="Where do you want to go?"
          editable={true}
          onChangeText={props.onChangeText}
        />
      </Col>
    </Grid>
  );
};

export default FindLocation;

const styles = StyleSheet.create({
  sectionContent: {
    marginTop: -50,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionBody: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 5,
  },
});
