import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

//components
import HeaderPage from '../../../components/HeaderPage';
//components

export default class CarRentalDetail extends PureComponent {
  constructor(props) {
    super(props);
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View>
        <HeaderPage title="Details" oncallback={this._goBack} {...this.props} />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
