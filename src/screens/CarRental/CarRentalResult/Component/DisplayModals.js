import * as React from 'react';
import {
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { Row, Col } from 'react-native-easy-grid';

import { fontReguler } from '../../../../base/constant/index';
import Heading from './Header';
import Tabs from './Tabs/Tabs';

//Route Tab
import Tab1 from './Tabs/Tab1';
import Tab2 from './Tabs/Tab2';
import Tab3 from './Tabs/Tab3';

//component imported
const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

//Master Modal
class DisplayModal extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      page: 0,
      modalVisible: true,
    };

    this._onTabChange = this._onTabChange.bind(this);
  }

  _onTabChange(tabIndex) {
    this.setState({ page: tabIndex });
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.display}
        hardwareAccelerated={false}
        onRequestClose={this.props.close}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
      >
        <View style={styles.container}>
          <Heading title="Details" backButton={this.props.modalin} />
          <Tabs>
            {/* First tab */}
            <Tab1 title="Basic Service" />
            {/* Second tab */}
            <Tab2 title="Free Pickup" />
            {/* Third tab */}
            <Tab3 title="Information" />
          </Tabs>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
    backgroundColor: '#FFF', // Background color
  },
  // Tab content container
  content: {
    flex: 1, // Take up all available space
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
    backgroundColor: '#C2185B', // Darker background for content area
  },
  // Content header
  header: {
    margin: 5, // Add margin
    color: '#FFFFFF', // White color
    fontFamily: fontReguler, // Change font family
    fontSize: 26, // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: fontReguler,
    fontSize: 18,
  },
});

export default DisplayModal;
