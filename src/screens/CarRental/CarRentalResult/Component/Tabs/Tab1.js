import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { fontReguler } from '../../../../../base/constant/index';

export default class Tab1 extends React.PureComponent {
  render() {
    return (
      <View title={this.props.title} style={styles.content}>
        <Text style={styles.header}>Basic Service</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1, // Take up all available space
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
    backgroundColor: '#FFF', // Darker background for content area
  },
  // Content header
  header: {
    margin: 10, // Add margin
    color: '#000', // White color
    fontFamily: fontReguler, // Change font family
    fontSize: 26, // Bigger font size,
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: fontReguler,
    fontSize: 18,
  },
});
