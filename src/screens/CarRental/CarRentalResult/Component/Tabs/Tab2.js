import React from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';
import { fontReguler } from '../../../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;

export default class Tab2 extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      freePickup: [
        {
          title: 'Inside Bali excluding Karangasem, Singaraja, Pupuan',
        },
        {
          title: 'Ngurah Rai International Airport',
        },
      ],
    };
  }
  render() {
    return (
      <View title={this.props.title} style={styles.content}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.header}>
            Free pickup and drop off are included for the following areas:
          </Text>
          {this.state.freePickup.map((item, i) => (
            <View key={i}>
              <View style={{ flexDirection: 'row' }}>
                <Icon type="entypo" name="dot-single" color="#000" />
                <Text>{item.title}</Text>
              </View>
            </View>
          ))}

          <View style={styles.info}>
            <Icon
              type="ionicon"
              name="ios-information-circle"
              color="#1976D2"
              containerStyle={{ paddingRight: 5 }}
            />
            <Text style={{ color: '#000' }}>
              Rental includes driver and will be charged per day
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1, // Take up all available space
    backgroundColor: '#FFF',
    margin: 10,
    paddingBottom: 10,
  },
  // Content header
  header: {
    margin: 10, // Add margin
    color: '#000', // White color
    fontFamily: fontReguler, // Change font family
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: fontReguler,
    fontSize: 18,
  },

  info: {
    width: '100%',
    height: 75,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#2196F3',
    backgroundColor: '#BBDEFB',
    flexDirection: 'row',
    padding: 10,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
