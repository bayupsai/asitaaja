import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fontReguler, thameColors } from '../../../../base/constant/index';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

const TitleSubPage = props => (
  <View style={styles.container}>
    <ImageBackground
      source={require('../../../../assets/icons/maps.png')}
      style={styles.imageBackground}
    >
      <Grid style={styles.grid}>
        <Col size={1}>
          <Icon name="circle" color="#FFF" size={10} style={styles.iconSpace} />
        </Col>
        <Col size={8}>
          <Text style={[styles.textWhite]}>Destination</Text>
          <Text style={styles.pageTitle}>{props.title}</Text>
          {/* <Row>
                        <Col>
                            <Text style={styles.textWhite}>
                                {props.date}
                                <Icon name="circle" color="#FFF" size={5} style={styles.iconSpace} />
                                <Text style={styles.textWhite}>{props.detail}</Text>
                            </Text>
                        </Col>
                    </Row> */}
        </Col>
      </Grid>
    </ImageBackground>
  </View>
);
const SubHeaderOnly = props => (
  <TitleSubPage title={props.title} date={props.date} detail={props.detail} />
);

export default SubHeaderOnly;

const styles = StyleSheet.create({
  imageBackground: {
    height: deviceHeight / 4,
    width: deviceWidth,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  container: {
    backgroundColor: thameColors.primary,
    height: deviceHeight / 4,
    width: deviceWidth,
    borderBottomLeftRadius: 50,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 20,
    paddingTop: 3,
  },
  pageTitle: {
    color: '#FFFFFF',
    fontSize: 24,
    fontFamily: fontReguler,
  },
  textWhite: {
    color: '#fff',
    fontSize: 12,
    fontFamily: fontReguler,
  },
  grid: {
    paddingTop: 25,
    marginLeft: 20,
  },
  iconSpace: {
    // marginLeft: 20,
    marginRight: 20,
  },
});
