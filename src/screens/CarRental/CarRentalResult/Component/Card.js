import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import numeral from 'numeral';

//import components
import DisplayModal from './DisplayModals';
import { fontReguler } from '../../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

class Card extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errImage:
        'https://lh3.googleusercontent.com/qx1FOw6660hxPOAfaM0_FQhMjcYatS-JyXwWfpF_evjYFwuXljAvyT5C30SStqC3_PxHTJOU92ZCXwLaiY5yVscywsZVYhAaIMS5yTEVEIuE3SUw0H9MTtdvmv8qJxWUP6srLXcuo2ArNNcimg5J5Pv_zGimyzQdrr403V_EOzXR10mSvrPIQcTf3FhFLkrcuzjSFvvyfhssEAoIBE-9vo90cbPIZ3XUHpHtyUMEuKY9Zgdwd7S19swK3YNP4XWA8611cgrBtc6UyMELDF8nTpLAfF-blf-DNBKLaG5JzrJHKlvdDu4u5hhl0MIQFIUFyfiL7R0YNLyEOpQ6E2YtyNYhdkz5-pGm7FR4aLUUpTKJ4FI6O3VVcN79HeG2lgT8-X4QGeWXKErEb9hDrONOcx6uDO92xLaoErGRlSdpNAOnwDF_FQTu4HSDhUIiv_6r880tyARFKeBBWDzTsfJvy6B-DkNAGoQ0WGzAzsXbXcU4S6F1kss2TUTHXxN0Gxf98prbXd39yOLFvgi0FMfxlUO2IsKASYbuiu2GEwJJ_ECkhDQ5eTswoGgYDcguJ9H6mI57WO0ZcKArlybegwI0yDIb_iZvCfOgs-D3hbbHtQkd5-8tSutnhdC_aBfubk2BALZ_xHT3UK5-qHJ9UG6sTiwWJDghxWU=w500-h318-no',
      photo_url: this.props.image,
      modalVisible: false,
    };
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  render() {
    return (
      <TouchableOpacity
        style={styles.card}
        {...this.props}
        onPress={this.props.onPress}
      >
        <DisplayModal
          display={this.state.modalVisible}
          close={() => {
            this.handleModal(!this.state.modalVisible);
          }}
          modalin={() => {
            this.handleModal(!this.state.modalVisible);
          }}
        />
        <Grid style={{ flex: 0, padding: 5 }}>
          <Col size={4}>
            <Image
              source={this.state.photo_url}
              resizeMode="stretch"
              style={styles.img}
              onError={err =>
                this.setState({
                  photo_url: this.state.errImage,
                })
              }
            />
          </Col>
          <Col size={6}>
            <Row>
              <Col size={7}>
                <Text style={styles.textTitle}>{this.props.name}</Text>
                <Text>{this.props.typeCar}</Text>
              </Col>
              <Col size={3} style={{ alignItems: 'flex-end' }}>
                <TouchableOpacity
                  style={styles.collapse}
                  onPress={() => {
                    this.handleModal(true);
                  }}
                >
                  <Icon
                    name="ios-arrow-down"
                    type="ionicon"
                    size={20}
                    color="#1976D2"
                  />
                </TouchableOpacity>
              </Col>
            </Row>

            <Row>
              <Col size={6}>
                {this.props.driver == true ? (
                  <View style={styles.driverInclude}>
                    <Text style={{ color: '#00BCD4' }}>Driver Included</Text>
                  </View>
                ) : (
                  <Text>No Driver</Text>
                )}
              </Col>
              <Col
                size={4}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                }}
              >
                <Icon
                  type="material"
                  name="account-circle"
                  color="#757575"
                  size={16}
                />
                <Text style={{ paddingLeft: 5, marginRight: 5 }}>
                  {this.props.maxPassenger}
                </Text>
                <Icon
                  type="simple-line-icon"
                  name="briefcase"
                  color="#757575"
                  size={16}
                />
                <Text style={{ marginLeft: 5 }}>{this.props.maxCase}</Text>
              </Col>
            </Row>
          </Col>
        </Grid>
        <Grid style={{ flex: 0, paddingBottom: 5 }}>
          <Col
            style={{
              padding: 10,
              borderTopWidth: 1,
              borderTopColor: '#BDBDBD',
              flexDirection: 'row',
            }}
          >
            <Icon type="feather" name="map-pin" color="#757575" />
            <Text>{this.props.location}</Text>
          </Col>
          <Col
            style={{
              justifyContent: 'center',
              alignItems: 'flex-end',
              borderBottomRightRadius: 5,
              borderTopWidth: 1,
              borderTopColor: '#BDBDBD',
              paddingRight: 5,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={[styles.textGreen, { marginRight: 10 }]}>
                Incl. Tax
              </Text>
              {/* <Text style={{ textDecorationLine: 'line-through'}}>Rp. {numeral(this.props.old_price).format('0,0')}</Text> */}
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.textPrice}>
                Rp. {numeral(this.props.price).format('0,0')}
              </Text>
              <Text> / day</Text>
            </View>
          </Col>
        </Grid>
      </TouchableOpacity>
    );
  }
}

export default Card;

const styles = StyleSheet.create({
  room: {
    width: deviceWidth / 1.07,
    height: 'auto',
    marginTop: 15,
    marginBottom: 15,
  },
  textTitle: {
    fontSize: 18,
    color: '#000',
    fontFamily: fontReguler,
  },
  textRoom: {
    fontSize: 20,
    color: '#000',
    fontWeight: 'bold',
    fontFamily: fontReguler,
  },
  textRoomCount: {
    fontSize: 20,
    color: '#000',
    fontFamily: fontReguler,
  },
  textPrice: {
    color: '#FF5722',
    fontSize: 19,
    fontFamily: fontReguler,
  },
  textGreen: {
    color: '#4CAF50',
    fontFamily: fontReguler,
  },
  textWhite: {
    color: '#fff',
    fontFamily: fontReguler,
  },
  card: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.07,
    height: 'auto',
    borderRadius: 5,
    marginTop: 10,
  },
  img: {
    width: 125,
    height: 100,
    borderRadius: 5,
  },

  collapse: {
    position: 'absolute',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: "grey"
  },

  driverInclude: {
    borderColor: '#00BCD4',
    borderRadius: 10,
    borderWidth: 1,
    width: 125,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
