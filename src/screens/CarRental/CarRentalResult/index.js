import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  FlatList,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';

//components
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderOnly from './Component/SubHeaderOnly';
import Card from './Component/Card';
//components

export default class CardRentalResult extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataCar: [
        {
          name: 'Daihatsu All new Xenia',
          typeCar: 'Mini Mpv',
          image: require('../../../assets/cars/Fakta-Mobil-Anda.png'),
          driver: true,
          maxPassenger: 6,
          maxCase: 3,
          location: 'Ubud, Gianyar',
          price: 2250000,
        },
        {
          name: 'Isuzu Elf',
          typeCar: 'Minibus',
          image: require('../../../assets/cars/kisspng-toyota-hiace-sewa-mobil-innova-avanza-elf-long-5b33653424a5f9.8789852015300949001501.png'),
          driver: false,
          maxPassenger: 10,
          maxCase: 3,
          location: 'Tebet Timur, Tebet',
          price: 525000,
        },
        {
          name: 'Mercedez Ben',
          typeCar: 'Sport Car',
          image: require('../../../assets/cars/png-hd-of-car-car-png-hd-1195.png'),
          driver: false,
          maxPassenger: 4,
          maxCase: 2,
          location: 'Cawang, Jakarta',
          price: 6700000,
        },
      ],
    };
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _goBooking = () => {
    this.props.navigation.navigate('CarRentalBook');
  };

  _renderItem = ({ item, i }) => (
    <View key={i}>
      <Card
        name={item.name}
        typeCar={item.typeCar}
        image={item.image}
        driver={item.driver}
        maxPassenger={item.maxPassenger}
        maxCase={item.maxCase}
        location={item.location}
        price={item.price}
        onPress={this._goBooking}
      />
    </View>
  );

  _keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Car Rental"
          callback={this._goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderOnly title="Bali, Indonesia" />
          <View style={[styles.content, { margin: 10 }]}>
            <FlatList
              data={this.state.dataCar}
              renderItem={this._renderItem}
              keyExtractor={this._keyExtractor}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
