import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { fontBold, fontReguler } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class HomeOffer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col style={styles.col}>
            <Text style={styles.textBoldBenner}>Today, where to Sleep?</Text>
          </Col>
        </Row>
        <Row>
          <Col size={7} style={styles.colBody}>
            <Text style={styles.textBenner}>
              Explore you options in South Jakarta
            </Text>
          </Col>
          <Col size={2} style={styles.colIcon}>
            <Icon
              name="navigate-next"
              type="materialicon"
              color="#fff"
              size={20}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}
export default HomeOffer;

const styles = StyleSheet.create({
  col: { justifyContent: 'center', alignItems: 'center' },
  textBoldBenner: {
    fontSize: 18,
    color: '#ffffff',
    fontFamily: fontBold,
    paddingBottom: 5,
  },
  textBenner: {
    fontSize: 16,
    color: '#ffffff',
    paddingBottom: 10,
    fontFamily: fontReguler,
  },
  colIcon: { justifyContent: 'flex-start', alignItems: 'flex-start' },
  colBody: { justifyContent: 'center', alignItems: 'flex-end' },
});
