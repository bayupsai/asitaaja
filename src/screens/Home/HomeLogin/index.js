import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';

import { fontExtraBold, fontReguler, asitaColor } from '../../../base/constant';

const window = Dimensions.get('window');

const HomeLogin = props => {
  return (
    <View style={styles.container}>
      <Grid>
        <Col style={styles.colLogin}>
          <View>
            <Text style={styles.textBold}>Where you want to go?</Text>
          </View>
          <View>
            <Text style={styles.textBody}>
              Log in to access your upcoming trips
            </Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity onPress={props.onPress}>
              <Text style={styles.textLogin}>LOG IN / REGISTER</Text>
            </TouchableOpacity>
          </View>
        </Col>
      </Grid>
      {/* <Grid>
                <Col style={{marginBottom: 35}}>
                    <View>
                        <View>
                            <Image style={styles.image} source={require('../../../assets/img/imagehome.png')} />
                        </View>
                        <View style={{ marginTop: -105, marginBottom: 10 }}>
                            <HomeOffer />
                        </View>
                    </View>
                </Col>
            </Grid> */}
    </View>
  );
};

export default HomeLogin;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: asitaColor.white,
    width: window.width,
    marginBottom: 15,
  },
  image: {
    width: window.width,
    height: window.height / 3,
    resizeMode: 'center',
  },
  textBold: {
    fontSize: 18,
    color: asitaColor.black,
    fontFamily: fontExtraBold,
    paddingBottom: 5,
  },
  textBody: {
    fontSize: 16,
    color: asitaColor.brownGreyTwo,
    paddingBottom: 15,
    fontFamily: fontReguler,
  },
  textLogin: {
    color: asitaColor.white,
    fontSize: 16,
    fontFamily: fontExtraBold,
  },
  colLogin: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  button: {
    width: window.width - 40,
    backgroundColor: asitaColor.orange,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
