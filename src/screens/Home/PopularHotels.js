import React from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  Animated,
  StyleSheet,
  TouchableOpacity,
  InteractionManager,
} from 'react-native';
import {
  thameColors,
  fontSemiBold,
  fontReguler,
} from '../../base/constant/index';
import { Icon } from 'react-native-elements';
import { dataHotel, dataFlight } from './Component/dataHotel';
import styles from './Component/style';
// import { scale } from '../../Const/ScaleUtils'
import _ from 'lodash';
import numeral from 'numeral';

// Split arrays become 2 micro array
// const chunk = _.chunk(dataHotel, 3)

//resolution
const window = Dimensions.get('window');
const windowLong = window.width - 100;

//Star
// const starMap = (totalStar) => {
//     let arrays = []
//     for (let i = 1; i <= totalStar; i++) {
//         arrays.push(<Image key={i} source={require('../../assets/icons/stars.png')} resizeMode="stretch" style={{ width: scale(17), height: scale(20), marginRight: 5 }} />)
//     }
//     return arrays
// }

//animation
const paddingLeft = 10;
const paddingOffset = 300;

const Card = props => (
  <TouchableOpacity onPress={props.onPress}>
    <Animated.Image style={[styles.card, props.style]} source={props.img} />
    <Animated.View style={[styles.card, props.styleIn]}>
      {props.nearest == true ? (
        <View
          style={{
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            flexDirection: 'row',
          }}
        >
          <Icon
            type="material"
            name="location-on"
            color={thameColors.white}
            iconStyle={{ marginRight: 5 }}
          />
          <Text
            style={[
              styles.textRegular,
              {
                color: thameColors.white,
                fontSize: 18,
                textAlign: 'left',
                fontFamily: fontSemiBold,
              },
            ]}
          >
            {props.title}
          </Text>
        </View>
      ) : (
        <View style={{ width: 0, height: 0 }} />
      )}
      <View style={{ justifyContent: 'flex-end', flex: 1 }}>
        <Text
          style={[
            styles.textRegular,
            { color: thameColors.white, fontSize: 16, fontFamily: fontReguler },
          ]}
        >
          Starting from
        </Text>
        <Text style={[styles.textBold, { color: thameColors.yellow }]}>
          IDR{' '}
          {numeral(9000000)
            .format('0,0')
            .replace(/,/g, '.')}
        </Text>
        {/* {props.rating === 0 ? <Text style={[styles.textBold, { color: thameColors.white, fontSize: 16, fontFamily: fontBold }]}>Hotel In</Text> : null} */}
        {/* <Text style={[styles.textBold, { color: thameColors.white }]}>{props.title}</Text> */}
        {/* {props.rating !== 0 ?
                    <View style={{ flexDirection: 'row' }}>{starMap(props.rating)}</View>
                    : <View style={{ width: 0, height: 0 }} />
                } */}
      </View>
    </Animated.View>
  </TouchableOpacity>
);

class PopularHotels extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
    };
  }

  onPressPage = () => {
    this.props.navigation.navigate('SearchFlight');
  };
  _onCard = item => {
    const { navigate } = this.props.navigation;
    InteractionManager.runAfterInteractions(() => {
      navigate('SearchFlight', { getDestination: item });
    });
  };

  render() {
    const diffScrollTranslate = Animated.diffClamp(
      this.state.scrollX,
      0,
      paddingOffset
    ).interpolate({
      inputRange: [0, paddingOffset],
      outputRange: [0, -paddingLeft],
      extrapolate: 'clamp',
    });
    return (
      <Animated.View
        style={[
          styles.container,
          { backgroundColor: thameColors.white, paddingLeft: 0 },
        ]}
      >
        <View
          style={{
            marginBottom: 20,
            paddingLeft: 20,
            marginRight: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <View>
            <Text style={styles.textBold}>Most Popular Destination City</Text>
            <Text style={styles.textRegular}>
              Today, where do you want to go next?
            </Text>
          </View>
          <TouchableOpacity onPress={() => this._onCard('Bali /Denpasar')}>
            <Icon
              type="ionicon"
              name="ios-arrow-forward"
              color={thameColors.primary}
              size={25}
            />
          </TouchableOpacity>
        </View>

        {/* ============================= Hotel ============================= */}
        {/* {chunk.map((_, index) => {
                    return (
                        <Animated.ScrollView key={index}
                            // onScroll={Animated.event(
                            //     [{ nativeEvent: { contentOffset: { x: this.state.scrollX } } }]
                            // )}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                        >
                            {chunk[index].map((item, index) => (
                                <Card style={index === 0 ? [styling.card1] : [styling.card2]} styleIn={index === 0 ? [styling.card3] : [styling.card4]} key={index} {...item} />
                            ))}
                        </Animated.ScrollView>
                    )
                })} */}
        <Animated.ScrollView
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: this.state.scrollX } } }],
            { useNativeDriver: true }
          )}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={{ transform: [{ translateX: diffScrollTranslate }] }}
        >
          {dataFlight.map((item, index) => (
            <Card
              onPress={() => this._onCard(item.title)}
              style={index === 0 ? [styling.card1] : [styling.card2]}
              styleIn={index === 0 ? [styling.card3] : [styling.card4]}
              key={index}
              {...item}
            />
          ))}
        </Animated.ScrollView>
        {/* ============================= Hotel ============================= */}
      </Animated.View>
    );
  }
}

export default PopularHotels;

const styling = StyleSheet.create({
  card1: { borderRadius: 10, marginLeft: 20 },
  card2: { borderRadius: 10, width: windowLong },
  card3: {
    backgroundColor: 'rgba(0,0,0,0.25)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    marginLeft: 20,
  },
  card4: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: windowLong,
  },
});
