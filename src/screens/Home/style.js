import { StyleSheet } from 'react-native';
import {
  thameColors,
  fontExtraBold,
  fontReguler,
  fontBold,
  fontSemiBold,
} from '../../base/constant';
import { scale, verticalScale } from '../../Const/ScaleUtils';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  header: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textExtraBold: {
    fontFamily: fontExtraBold,
    fontSize: 20,
    color: thameColors.textBlack,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 18,
  },
  textRegular: {
    fontFamily: fontReguler,
    fontSize: 16,
    color: thameColors.textBlack,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
  },
  imagePopular: {
    width: scale(200),
    height: verticalScale(300),
    borderRadius: 10,
  },
  shadow: {
    shadowOpacity: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: 'rgba(0,0,0, 0)',
    shadowRadius: 0,
  },
});
