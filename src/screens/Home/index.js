/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  PermissionsAndroid,
  StatusBar,
  Platform,
  InteractionManager,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
// import Draggable from 'react-native-draggable'

import Header from '../../components/Header';
import Menu from '../../components/Menu';
import HomeLogin from './HomeLogin/index';
import PopularHotels from './PopularHotels';
// import ProductShop from './ProductShop'
import More from '../More/index';
import { asitaColor } from '../../base/constant';
import { connect } from 'react-redux';
import { setCurrentGeolocation } from '../../redux/actions/globalAction';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const isAndroid = Platform.OS === 'android';

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    width: deviceWidth,
    margin: 0,
  },
  modalContent: {
    backgroundColor: asitaColor.white,
    padding: 0,
    height: deviceHeight / 1.5,
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  gridHeader: { flex: 1.5 },
  gridContent: { flex: 7, width: deviceWidth },
  gridFooter: { flex: 1.5 },
});

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
    };
  }

  componentDidMount() {
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content');
      isAndroid && StatusBar.setBackgroundColor(asitaColor.white);
    });
  }
  componentWillUnmount() {
    this._navListener.remove();
  }

  showMoreMenu = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ visibleModal: 1 });
    });
  };
  hideMoreMenu = () => this.setState({ visibleModal: null });

  // Navigation
  navigateChatBoot = () => {
    const { navigate } = this.props.navigation;
    InteractionManager.runAfterInteractions(() => navigate('Chatbot'));
  };
  navigateTo = route => {
    const { navigate } = this.props.navigation;
    navigate(route, { load: 1 });
  };
  navigateToLogin = () => {
    const { navigate } = this.props.navigation;
    InteractionManager.runAfterInteractions(() => navigate('LoginPage'));
  };
  // Navigation

  async requestMapsPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'Some feature from our apps need your location. Please give a permission to get good experience.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentLocation();
      } else {
      }
    } catch (err) {
      console.warn(err);
    }
  }

  getCurrentLocation = () => {
    const { dispatch } = this.props;
    navigator.geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.00922 * 1.5,
          longitudeDelta: 0.00421 * 1.5,
        };
        dispatch(
          setCurrentGeolocation(region, region.latitude, region.longitude)
        );
      },
      // eslint-disable-next-line no-undef
      error,
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );

    this.watchID = navigator.geolocation.watchPosition(position => {
      let region = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        latitudeDelta: 0.00922 * 1.5,
        longitudeDelta: 0.00421 * 1.5,
      };
      dispatch(
        setCurrentGeolocation(region, region.latitude, region.longitude)
      );
    });
  };

  //Main Render
  render() {
    let { isLogin } = this.props;
    return (
      <View style={{ flex: 1, zIndex: 1 }}>
        {/* <StatusBar barStyle={'dark-content'} backgroundColor={StatusBar.setBackgroundColor(asitaColor.white)} /> */}
        <ScrollView style={{ backgroundColor: asitaColor.backWhite }}>
          <Header homeBar={true} {...this.props} />
          <ScrollView
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
          >
            <Menu
              props={this.props}
              showMore={this.showMoreMenu}
              hideMore={this.hideMoreMenu}
              more={this.showMoreMenu}
              commingSoon={this.showMoreMenu}
              {...this.props}
            />
            {/* <BestOffers/> */}
            {!isLogin ? <HomeLogin onPress={this.navigateToLogin} /> : null}
            {/* <PopularEvents {...this.props} /> */}
            {/* ==================== Promo & Best Choices ==================== */}
            <PopularHotels {...this.props} onPress />
            {/* <ProductShop {...this.props} />
                    <PackageUp {...this.props} /> */}
            {/* ==================== Promo & Best Choices ==================== */}
          </ScrollView>

          {/* MODAL */}
          <Modal
            useNativeDriver={true}
            hideModalContentWhileAnimating={true}
            isVisible={this.state.visibleModal === 1}
            onBackdropPress={this.hideMoreMenu}
            onBackButtonPress={this.hideMoreMenu}
            style={styles.bottomModal}
          >
            <View style={styles.modalContent}>
              {/* <Grid style={styles.gridHeader}>
                            <Col size={4.5} style={{ alignItems: "flex-end", justifyContent: "center" }}>
                                <Text style={{ fontSize: 18, color: 'black', fontFamily: fontBold, }}>
                                    Other Services,
                                </Text>
                            </Col>
                            <Col size={1} style={{ justifyContent: "center", alignItems: "center" }}>
                                <Icon name="lock" type="materialcommunityicons" color={thameColors.secondary} />
                            </Col>
                            <Col size={4.5} style={{ justifyContent: "center", alignItems: "flex-start" }}>
                                <Text style={{ fontSize: 18, color: thameColors.secondary, fontFamily: fontExtraBold }}>Coming Soon!</Text>
                            </Col>
                        </Grid> */}
              <Grid style={styles.gridContent}>
                <Col>
                  <More {...this.props} stateVisible={{ visibleModal: null }} />
                </Col>
              </Grid>
              <Grid style={styles.gridFooter}>
                <Col
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingBottom: 20,
                  }}
                >
                  <TouchableOpacity
                    style={{ flex: 0 }}
                    onPress={this.hideMoreMenu}
                  >
                    <View
                      style={{
                        width: 50,
                        height: 50,
                        borderRadius: 25,
                        borderWidth: 1.5,
                        padding: 10,
                        borderColor: asitaColor.brownGrey,
                      }}
                    >
                      <Icon name="arrowdown" type="antdesign" color="black" />
                    </View>
                  </TouchableOpacity>
                </Col>
              </Grid>
            </View>
          </Modal>
        </ScrollView>
        {/* <Draggable pressDrag={this.navigateChatBoot} reverse={false} renderShape="image" imageSource={require('../../assets/img/chat_bot.png')} renderSize={75} offsetX={175} offsetY={175} /> */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // global: state.global,
    isLogin: state.profile.isLogin,
  };
}
export default connect(mapStateToProps)(Home);
