import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Animated,
} from 'react-native';
import { Button } from 'react-native-elements';
import { thameColors, fontReguler, fontBold } from '../../base/constant/index';
import dataPulsa from './Component/dataPulsa';
import styles from './Component/style';

//animation
const paddingRight = 10;
const paddingOffset = 300;

class Card extends React.PureComponent {
  render() {
    return (
      <ImageBackground
        source={this.props.img}
        style={[styles.longCard, this.props.style]}
        imageStyle={{ borderRadius: 10 }}
      >
        <View
          style={{
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            flex: 1,
            width: 150,
          }}
        >
          <Text
            style={[
              styles.textBold,
              { color: thameColors.white, lineHeight: 20 },
            ]}
          >
            {this.props.title}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            flex: 1,
          }}
        >
          <Button
            title={this.props.button}
            titleStyle={{ fontFamily: fontBold, fontSize: 14 }}
            buttonStyle={styles.buttonStyle}
            containerStyle={{ backgroundColor: thameColors.primary }}
            onPress={() => alert('Top Up')}
          />
        </View>
      </ImageBackground>
    );
  }
}

class PackageUp extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
    };
  }

  render() {
    const diffScrollTranslate = Animated.diffClamp(
      this.state.scrollX,
      0,
      paddingOffset
    ).interpolate({
      inputRange: [0, paddingOffset],
      outputRange: [0, -paddingRight],
      extrapolate: 'clamp',
    });
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: thameColors.white, paddingRight: 0 },
        ]}
      >
        <Animated.ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { x: this.state.scrollX } } },
          ])}
          style={{
            transform: [{ translateX: diffScrollTranslate }],
            marginLeft: -20,
          }}
        >
          {dataPulsa.map((item, index) => (
            <Card
              key={index}
              {...item}
              style={[
                styling.slideScroll,
                index === 0 ? { marginLeft: 20 } : { position: 'relative' },
              ]}
            />
          ))}
        </Animated.ScrollView>
      </View>
    );
  }
}

export default PackageUp;

const styling = StyleSheet.create({
  slideScroll: { padding: 20 },
});
