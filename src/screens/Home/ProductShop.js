import React from 'react';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  StyleSheet,
  Animated,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { thameColors, fontBold } from '../../base/constant/index';
import { Button, Icon } from 'react-native-elements';
import styles from './Component/style';
import dataShop from './Component/dataShop';

//animation
const paddingRight = 10;
const paddingOffset = 300;
const window = Dimensions.get('window');

class Card extends React.PureComponent {
  render() {
    return (
      <View style={{ alignItems: 'flex-end' }}>
        <ImageBackground
          style={[
            styles.card,
            this.props.style,
            { height: window.height / 2.5 },
          ]}
          imageStyle={{ borderRadius: 10, height: window.height / 3 }}
          source={this.props.img}
        />
        <View
          style={[
            styles.card,
            {
              justifyContent: 'flex-end',
              flex: 1,
              alignItems: 'center',
              marginTop: -75,
              backgroundColor: thameColors.white,
              height: window.height / 5,
              padding: 10,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            },
          ]}
        >
          <View
            style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}
          >
            <Text
              style={[
                styles.textRegular,
                { fontSize: 16, fontFamily: fontBold, marginBottom: 10 },
              ]}
            >
              {this.props.title}
            </Text>
            <Button
              title="SHOP NOW"
              titleStyle={{ fontFamily: fontBold, fontSize: 14 }}
              onPress={() => alert('Show Now')}
              buttonStyle={{
                backgroundColor: thameColors.primary,
                borderRadius: 10,
                padding: 10,
                paddingLeft: 15,
                paddingRight: 15,
                marginTop: 10,
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

class ProductShop extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollX: new Animated.Value(0),
    };
  }

  render() {
    const diffScrollTranslate = Animated.diffClamp(
      this.state.scrollX,
      0,
      paddingOffset
    ).interpolate({
      inputRange: [0, paddingOffset],
      outputRange: [0, -paddingRight],
      extrapolate: 'clamp',
    });
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: thameColors.secondary, paddingLeft: 0 },
        ]}
      >
        <View
          style={{
            marginBottom: 20,
            marginRight: 20,
            paddingLeft: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <View>
            <Text style={[styles.textBold, { color: thameColors.white }]}>
              The Best Product Choice
            </Text>
            <Text style={[styles.textRegular, { color: thameColors.white }]}>
              Easy, comfortable and free of shipping
            </Text>
          </View>
          <TouchableOpacity>
            <Icon
              type="ionicon"
              name="ios-arrow-forward"
              color={thameColors.white}
              size={25}
            />
          </TouchableOpacity>
        </View>

        <Animated.ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { x: this.state.scrollX } } },
          ])}
          style={{ transform: [{ translateX: diffScrollTranslate }] }}
        >
          {dataShop.map((item, index) => (
            <Card
              key={index}
              {...item}
              style={[
                styling.slideImage,
                index === 0
                  ? { marginLeft: 20 }
                  : { backgroundColor: thameColors.white },
              ]}
            />
          ))}
        </Animated.ScrollView>
      </View>
    );
  }
}

export default ProductShop;

const styling = StyleSheet.create({
  slideImage: { paddingBottom: 0, paddingLeft: 0, paddingRight: 0 },
});
