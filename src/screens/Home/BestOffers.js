import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Animated,
} from 'react-native';
import { Header } from 'react-native-elements';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { fontBold } from '../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;
// const FIXED_BAR_WIDTH = 100
// const BAR_SPACE = 10

// const imageData = [
//     require('../../assets/offers/offers-1.png'),
//     require('../../assets/offers/offers-2.png'),
//     require('../../assets/offers/offers-3.png')
// ]

// class BestOffers extends React.PureComponent {

//     constructor(props) {
//         super(props)
//         this.state = {
//             imageData: [
//                 {
//                     'img': require('../../assets/offers/offers-1.png'),
//                 },
//                 {
//                     'img': require('../../assets/offers/offers-2.png'),
//                 },
//                 {
//                     'img': require('../../assets/offers/offers-3.png')
//                 }
//             ],
//             carouselIndicator: []
//         }
//     }

//     numItems = imageData.length
//     itemWidth = (FIXED_BAR_WIDTH / this.numItems) - ((this.numItems - 1) * BAR_SPACE)
//     animVal = new Animated.Value(0)

//     render() {
//         let imageArray = []
//         let barArray = []

//         const { carousel } = this.state.imageData

//         imageData.map((data, i)=> {
//             const thisCarousel = (
//                 <View key={`image${i}`} style={styles.sectionOffer}>
//                     <Image
//                         source={data}
//                         style={styles.sectionImage}
//                     />
//                 </View>
//             )
//             imageArray.push(thisCarousel)

//             const scrollBarVal = this.animVal.interpolate({
//                 inputRange: [deviceWidth * (i - 1), deviceWidth * (i + 1)],
//                 outputRange: [-this.itemWidth, this.itemWidth],
//                 extrapolate: 'clamp',
//             })

//             const thisBar = (
//                 <View
//                     key={`bar${i}`}
//                     style={[
//                         styles.track,
//                         {
//                         width: this.itemWidth,
//                         marginLeft: i === 0 ? 0 : BAR_SPACE,
//                         },
//                     ]}
//                     >
//                     <Animated.View

//                         style={[
//                         styles.bar,
//                         {
//                             width: this.itemWidth,
//                             transform: [
//                             { translateX: scrollBarVal },
//                             ],
//                         },
//                         ]}
//                     />
//                 </View>
//             )
//             barArray.push(thisBar)
//         })

//         return (
//             <View style={styles.container}>
//                 <Row style={styles.sectionHeader}>
//                     <Grid>
//                         <Col style={styles.sectionTitle}>
//                             <Text style={styles.title}>Special For You</Text>
//                         </Col>
//                         <Col style={styles.sectionMore}>
//                             <TouchableOpacity onPress={() => alert("View More Clicked")}>
//                                 <Text style={styles.more}>View More</Text>
//                             </TouchableOpacity>
//                         </Col>
//                     </Grid>
//                 </Row>
//                 <Row style={styles.sectionBody}>
//                     <ScrollView
//                         horizontal
//                         showsHorizontalScrollIndicator={false}
//                         scrollEventThrottle={10}
//                         pagingEnabled
//                         onScroll={
//                           Animated.event(
//                             [{ nativeEvent: { contentOffset: { x: this.animVal } } }]
//                           )
//                         }
//                     >
//                         {imageArray}
//                     </ScrollView>

//                     <View style={{ position: "absolute", bottom: 30, left: 20 }}>
//                         <View style={{ flex: 1, flexDirection: "row", zIndex: 2, position: 'absolute'}}>
//                             {barArray}
//                             {/* <View style={[styles.sectionIndicator, {backgroundColor: [activeColor]}]}/> */}
//                         </View>
//                     </View>
//                 </Row>
//             </View>
//         )
//     }
// }

const BestOffers = props => (
  <View style={styles.container} {...props}>
    <Row style={styles.sectionHeader}>
      <Grid>
        <Col style={styles.sectionTitle}>
          <Text style={styles.title}>Special For You</Text>
        </Col>
        <Col style={styles.sectionMore}>
          <TouchableOpacity onPress={props.navigateToPromo}>
            <Text style={styles.more}>View More</Text>
          </TouchableOpacity>
        </Col>
      </Grid>
    </Row>
    <Row style={styles.sectionBody}>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={styles.sectionOffer}>
          <Image
            source={require('../../assets/offers/offers-1.png')}
            style={styles.sectionImage}
          />
        </View>
        <View style={styles.sectionOffer}>
          <Image
            source={require('../../assets/offers/offers-2.png')}
            style={styles.sectionImage}
          />
        </View>
        <View style={[styles.sectionOffer, { paddingRight: 10 }]}>
          <Image
            source={require('../../assets/offers/offers-3.png')}
            style={styles.sectionImage}
          />
        </View>
      </ScrollView>
      <View style={{ position: 'absolute', bottom: 30, left: 20 }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={styles.sectionIndicatorTrue}></View>
          <View style={styles.sectionIndicator}></View>
          <View style={styles.sectionIndicator}></View>
          <View style={styles.sectionIndicator}></View>
        </View>
      </View>
    </Row>
  </View>
);

export default BestOffers;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
  },
  sectionHeader: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    paddingLeft: 10,
    paddingRight: 30,
    paddingBottom: 0,
  },
  sectionTitle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#222222',
    fontFamily: fontBold,
    fontSize: 18,
  },
  sectionMore: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 0,
  },
  more: {
    color: '#a2195b',
    fontFamily: fontBold,
    fontSize: 14,
  },
  sectionBody: {
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  sectionOffer: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  sectionImage: {
    width: deviceWidth - 40,
    height: 200,
    resizeMode: 'center',
    borderRadius: 5,
  },
  sectionIndicatorTrue: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: 20,
    height: 5,
    borderRadius: 5,
    marginRight: 5,
  },
  sectionIndicator: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: 10,
    height: 5,
    borderRadius: 5,
    marginRight: 5,
    overflow: 'hidden',
  },

  track: {
    backgroundColor: 'rgba(0, 0, 0, 0.01)',
    overflow: 'hidden',
    height: 7,
    borderRadius: 5,
  },
  bar: {
    backgroundColor: '#FFF',
    height: 7,
    position: 'absolute',
    left: 0,
    top: 0,
    borderRadius: 5,
  },
});
