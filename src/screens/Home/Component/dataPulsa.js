export default [
  {
    id: 1,
    img: require('../../../assets/img/pulsa1.png'),
    title: 'Your Smile is Always Connected',
    button: 'TOP UP PULSA',
  },
  {
    id: 2,
    img: require('../../../assets/img/pulsa1.png'),
    title: 'Charge Your Life, Charge your Electricity',
    button: 'TOP UP LISTRIK',
  },
  {
    id: 3,
    img: require('../../../assets/img/pulsa1.png'),
    title: 'No Internet Connection?',
    button: 'TOP UP PACKAGE',
  },
];
