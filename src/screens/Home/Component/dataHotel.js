const dataHotel = [
  {
    id: 1,
    img: require('../../../assets/img/hotel4.jpg'),
    title: 'DKI Jakarta',
    rating: 0,
    nearest: false,
  },
  {
    id: 2,
    img: require('../../../assets/img/hotel3.jpg'),
    title: 'Fairmont Jakarta',
    rating: 5,
    nearest: true,
  },
  {
    id: 3,
    img: require('../../../assets/img/hotel4.jpg'),
    title: 'Fairmont Jakarta',
    rating: 5,
    nearest: true,
  },
  {
    id: 4,
    img: require('../../../assets/img/hotel3.jpg'),
    title: 'Denpasar, Bali',
    rating: 0,
    nearest: false,
  },
  {
    id: 5,
    img: require('../../../assets/img/hotel4.jpg'),
    title: 'Sapulidi Resort Spa',
    rating: 5,
    nearest: true,
  },
  {
    id: 6,
    img: require('../../../assets/img/hotel3.jpg'),
    title: 'Sapulidi Resort Spa',
    rating: 5,
    nearest: true,
  },
];

const dataFlight = [
  {
    id: 1,
    img: require('../../../assets/img/hotel4.jpg'),
    title: 'Bali /Denpasar',
    rating: 0,
    nearest: true,
  },
  {
    id: 2,
    img: require('../../../assets/img/hotel3.jpg'),
    title: 'Surabaya',
    rating: 5,
    nearest: true,
  },
  {
    id: 3,
    img: require('../../../assets/img/hotel4.jpg'),
    title: 'Malang',
    rating: 5,
    nearest: true,
  },
  {
    id: 4,
    img: require('../../../assets/img/hotel3.jpg'),
    title: 'Makassar',
    rating: 5,
    nearest: true,
  },
  {
    id: 5,
    img: require('../../../assets/img/hotel4.jpg'),
    title: 'Medan',
    rating: 5,
    nearest: true,
  },
  {
    id: 6,
    img: require('../../../assets/img/hotel3.jpg'),
    title: 'Yogyakarta',
    rating: 5,
    nearest: true,
  },
];

export { dataHotel, dataFlight };
