import { StyleSheet, Dimensions } from 'react-native';
import {
  fontBold,
  fontReguler,
  thameColors,
  fontExtraBold,
} from '../../../base/constant';

const window = Dimensions.get('window');
const widthCard = window.width - 195;

export default styles = StyleSheet.create({
  container: {
    padding: 20,
    paddingTop: 25,
    paddingBottom: 25,
    paddingRight: 0,
  },
  card: {
    width: widthCard,
    height: window.height / 2.35,
    borderRadius: 10,
    padding: 20,
    margin: 5,
    marginTop: 5,
  },
  subCard: {
    backgroundColor: thameColors.white,
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    borderBottomEndRadius: 10,
    borderBottomStartRadius: 10,
    width: widthCard,
    height: window.height / 5,
  },
  longCard: {
    width: window.width - 100,
    height: window.height / 4,
    borderRadius: 10,
    padding: 20,
    margin: 5,
  },
  textBold: {
    fontFamily: fontExtraBold,
    fontSize: 20,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  buttonStyle: {
    backgroundColor: thameColors.primary,
    borderRadius: 10,
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
});
