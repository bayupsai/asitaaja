import React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Dimensions,
  FlatList,
} from 'react-native';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { Grid, Row, Col } from 'react-native-easy-grid';
import MultiSwitch from 'rn-slider-switch';
import { InputText, SearchInput } from '../../elements/TextInput';
import { Button } from '../../elements/Button';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Dates from 'react-native-dates';
import moment from 'moment';
import { connect } from 'react-redux';
import styles from '../Flight/SearchFlight/styles';
import {
  actionSearchFlight,
  failedState,
} from '../../redux/actions/FlightAction';
import { DotIndicator } from 'react-native-indicators';
import { fontBold, fontReguler } from '../../base/constant';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class GroupBooking extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: null,
      focus: 'startDate',
      startDate: null,
      endDate: null,
      flightType: 'single',
      visibleModal: null,
      seatClassSelected: 1,
      seatClassType: [
        { index: 1, title: 'Economy' },
        { index: 2, title: 'Business' },
        { index: 3, title: 'First Class' },
        { index: 4, title: 'Premium Economy' },
      ],
      destinationPopular: [
        { code: 'CGK', name: 'Jakarta' },
        { code: 'DPS', name: 'Denpasar' },
        { code: 'LOP', name: 'Lombok' },
        { code: 'JOG', name: 'Yogya' },
        { code: 'MLG', name: 'Malang' },
        { code: 'UPG', name: 'Makassar' },
        { code: 'KNO', name: 'Medan' },
        { code: 'AMQ', name: 'Ambon' },
        { code: 'MDC', name: 'Manado' },
        { code: 'SUB', name: 'Surabaya' },
      ],
      departureList: [
        { code: 'AEG', name: 'Padang Sidempuan', namex: 'Aek Godang Airport' },
        { code: 'AMQ', name: 'Ambon', namex: 'Pattimura Airport, Ambon' },
        {
          code: 'BDO',
          name: 'Bandung',
          namex: 'Husein Sastranegara International Airport',
        },
        { code: 'BEJ', name: 'Tanjung Redeb', namex: 'Kalimarau Airport' },
        { code: 'BIK', name: 'Biak', namex: 'Frans Kaisiepo Airport' },
        { code: 'BKS', name: 'Bengkulu', namex: 'Fatmawati Soekarno Airport' },
        {
          code: 'BTH',
          name: 'Batam',
          namex: 'Hang Nadim International Airport',
        },
        {
          code: 'BTJ',
          name: 'Banda Aceh',
          namex: 'Sultan Iskandar Muda International Airport',
        },
        { code: 'BXB', name: 'Babo-Papua Island', namex: 'Babo Airport' },
        {
          code: 'CGK',
          name: 'Jakarta',
          namex: 'Soekarno-Hatta International Airport',
        },
        { code: 'CXP', name: 'Cilacap', namex: 'Tunggul Wulung Airport' },
        {
          code: 'DJJ',
          name: 'Jayapura',
          namex: 'Sentani International Airport',
        },
        {
          code: 'DPS',
          name: 'Denpasar',
          namex: 'Ngurah Rai (Bali) International Airport',
        },
        { code: 'DUM', name: 'Dumai', namex: 'Pinang Kampai Airport' },
        { code: 'FKQ', name: 'Fakfak', namex: 'Fakfak Airport' },
        { code: 'GNS', name: 'Gunung Sitoli', namex: 'Binaka Airport' },
        {
          code: 'HLP',
          name: 'Jakarta',
          namex: 'Halim Perdanakusuma International Airport',
        },
        { code: 'KNG', name: 'Kaimana', namex: 'Kaimana Airport' },
        {
          code: 'KTG',
          name: 'Ketapang',
          namex: 'Ketapang(Rahadi Usman) Airport',
        },
        { code: 'LOP', name: 'Mataram', namex: 'Lombok International Airport' },
        { code: 'LSX', name: 'Lhok Sukon', namex: 'Lhok Sukon Airport' },
        { code: 'LUV', name: 'Langgur', namex: 'Dumatumbun Airport' },
        {
          code: 'LUW',
          name: 'Luwok',
          namex: 'Syukuran Aminuddin Amir Airport',
        },
        { code: 'MDC', name: 'Manado', namex: 'Sam Ratulangi Airport' },
        { code: 'MES', name: 'Medan', namex: 'Soewondo Air Force Base' },
        { code: 'MKQ', name: 'Merauke', namex: 'Mopah Airport' },
        { code: 'MKW', name: 'Manokwari', namex: 'Rendani Airport' },
        { code: 'NAH', name: 'Tahuna', namex: 'Naha Airport' },
        { code: 'NBX', name: 'Nabire', namex: 'Nabire Airport' },
        { code: 'NPO', name: 'Nanga Pinoh', namex: 'Nanga Pinoh Airport' },
        { code: 'NTX', name: 'Ranai', namex: 'Ranai Airport' },
        { code: 'OTI', name: 'Gotalalamo', namex: 'Pitu Airport' },
        {
          code: 'PDG',
          name: 'Ketaping/Padang',
          namex: 'Minangkabau International Airport',
        },
        { code: 'PDO', name: 'Talang Gudang', namex: 'Pendopo Airport' },
        {
          code: 'PKU',
          name: 'Pekanbaru',
          namex: 'Sultan Syarif Kasim Ii (Simpang Tiga) Airport',
        },
        {
          code: 'PLM',
          name: 'Palembang',
          namex: 'Sultan Mahmud Badaruddin II Airport',
        },
        { code: 'PLW', name: 'Palu', namex: 'Mutiara Airport' },
        { code: 'PNK', name: 'Pontianak', namex: 'Supadio Airport' },
        { code: 'PSJ', name: 'Poso', namex: 'Kasiguncu Airport' },
        { code: 'PSU', name: 'Putussibau', namex: 'Pangsuma Airport' },
        { code: 'RGT', name: 'Rengat', namex: 'Japura Airport' },
        { code: 'SOQ', name: 'Sorong', namex: 'Dominique Edward Osok Airport' },
        { code: 'SQG', name: 'Sintang', namex: 'Sintang(Susilo) Airport' },
        { code: 'SRI', name: 'Samarinda', namex: 'Temindung Airport' },
        {
          code: 'SUB',
          name: 'Surabaya',
          namex: 'Juanda International Airport',
        },
        { code: 'TIM', name: 'Timika', namex: 'Moses Kilangin Airport' },
        { code: 'TJG', name: 'Tanta-Tabalong', namex: 'Warukin Airport' },
        {
          code: 'TKG',
          name: 'Bandar Lampung',
          namex: 'Radin Inten II (Branti) Airport',
        },
        { code: 'TRK', name: 'Tarakan', namex: 'Juwata Airport' },
        {
          code: 'TTE',
          name: 'Sango',
          namex: 'Sultan Khairun Babullah Airport',
        },
        { code: 'UGU', name: 'Sugapa', namex: 'Bilogai-Sugapa Airport' },
        {
          code: 'UPG',
          name: 'Ujung Pandang',
          namex: 'Hasanuddin International Airport',
        },
        { code: 'WMX', name: 'Wamena', namex: 'Wamena Airport' },
      ],
      departureResult: [],
      destinationList: [
        { code: 'AEG', name: 'Padang Sidempuan', namex: 'Aek Godang Airport' },
        { code: 'AMQ', name: 'Ambon', namex: 'Pattimura Airport, Ambon' },
        {
          code: 'BDO',
          name: 'Bandung',
          namex: 'Husein Sastranegara International Airport',
        },
        { code: 'BEJ', name: 'Tanjung Redeb', namex: 'Kalimarau Airport' },
        { code: 'BIK', name: 'Biak', namex: 'Frans Kaisiepo Airport' },
        { code: 'BKS', name: 'Bengkulu', namex: 'Fatmawati Soekarno Airport' },
        {
          code: 'BTH',
          name: 'Batam',
          namex: 'Hang Nadim International Airport',
        },
        {
          code: 'BTJ',
          name: 'Banda Aceh',
          namex: 'Sultan Iskandar Muda International Airport',
        },
        { code: 'BXB', name: 'Babo-Papua Island', namex: 'Babo Airport' },
        {
          code: 'CGK',
          name: 'Jakarta',
          namex: 'Soekarno-Hatta International Airport',
        },
        { code: 'CXP', name: 'Cilacap', namex: 'Tunggul Wulung Airport' },
        {
          code: 'DJJ',
          name: 'Jayapura',
          namex: 'Sentani International Airport',
        },
        {
          code: 'DPS',
          name: 'Denpasar',
          namex: 'Ngurah Rai (Bali) International Airport',
        },
        { code: 'DUM', name: 'Dumai', namex: 'Pinang Kampai Airport' },
        { code: 'FKQ', name: 'Fakfak', namex: 'Fakfak Airport' },
        { code: 'GNS', name: 'Gunung Sitoli', namex: 'Binaka Airport' },
        {
          code: 'HLP',
          name: 'Jakarta',
          namex: 'Halim Perdanakusuma International Airport',
        },
        { code: 'KNG', name: 'Kaimana', namex: 'Kaimana Airport' },
        {
          code: 'KTG',
          name: 'Ketapang',
          namex: 'Ketapang(Rahadi Usman) Airport',
        },
        { code: 'LOP', name: 'Mataram', namex: 'Lombok International Airport' },
        { code: 'LSX', name: 'Lhok Sukon', namex: 'Lhok Sukon Airport' },
        { code: 'LUV', name: 'Langgur', namex: 'Dumatumbun Airport' },
        {
          code: 'LUW',
          name: 'Luwok',
          namex: 'Syukuran Aminuddin Amir Airport',
        },
        { code: 'MDC', name: 'Manado', namex: 'Sam Ratulangi Airport' },
        { code: 'MES', name: 'Medan', namex: 'Soewondo Air Force Base' },
        { code: 'MKQ', name: 'Merauke', namex: 'Mopah Airport' },
        { code: 'MKW', name: 'Manokwari', namex: 'Rendani Airport' },
        { code: 'NAH', name: 'Tahuna', namex: 'Naha Airport' },
        { code: 'NBX', name: 'Nabire', namex: 'Nabire Airport' },
        { code: 'NPO', name: 'Nanga Pinoh', namex: 'Nanga Pinoh Airport' },
        { code: 'NTX', name: 'Ranai', namex: 'Ranai Airport' },
        { code: 'OTI', name: 'Gotalalamo', namex: 'Pitu Airport' },
        {
          code: 'PDG',
          name: 'Ketaping/Padang',
          namex: 'Minangkabau International Airport',
        },
        { code: 'PDO', name: 'Talang Gudang', namex: 'Pendopo Airport' },
        {
          code: 'PKU',
          name: 'Pekanbaru',
          namex: 'Sultan Syarif Kasim Ii (Simpang Tiga) Airport',
        },
        {
          code: 'PLM',
          name: 'Palembang',
          namex: 'Sultan Mahmud Badaruddin II Airport',
        },
        { code: 'PLW', name: 'Palu', namex: 'Mutiara Airport' },
        { code: 'PNK', name: 'Pontianak', namex: 'Supadio Airport' },
        { code: 'PSJ', name: 'Poso', namex: 'Kasiguncu Airport' },
        { code: 'PSU', name: 'Putussibau', namex: 'Pangsuma Airport' },
        { code: 'RGT', name: 'Rengat', namex: 'Japura Airport' },
        { code: 'SOQ', name: 'Sorong', namex: 'Dominique Edward Osok Airport' },
        { code: 'SQG', name: 'Sintang', namex: 'Sintang(Susilo) Airport' },
        { code: 'SRI', name: 'Samarinda', namex: 'Temindung Airport' },
        {
          code: 'SUB',
          name: 'Surabaya',
          namex: 'Juanda International Airport',
        },
        { code: 'TIM', name: 'Timika', namex: 'Moses Kilangin Airport' },
        { code: 'TJG', name: 'Tanta-Tabalong', namex: 'Warukin Airport' },
        {
          code: 'TKG',
          name: 'Bandar Lampung',
          namex: 'Radin Inten II (Branti) Airport',
        },
        { code: 'TRK', name: 'Tarakan', namex: 'Juwata Airport' },
        {
          code: 'TTE',
          name: 'Sango',
          namex: 'Sultan Khairun Babullah Airport',
        },
        { code: 'UGU', name: 'Sugapa', namex: 'Bilogai-Sugapa Airport' },
        {
          code: 'UPG',
          name: 'Ujung Pandang',
          namex: 'Hasanuddin International Airport',
        },
        { code: 'WMX', name: 'Wamena', namex: 'Wamena Airport' },
      ],
      destinationResult: [],
      // MAIN DATA
      origin: { code: 'CGK', name: 'Jakarta' },
      destination: { code: 'DPS', name: 'Denpasar' },
      departureDate: moment().format('YYYY-MM-DD'),
      returnDate: moment().format('YYYY-MM-DD'),
      totalPax: 10,
      seatClass: 'Economy',
    };

    this.props.dispatch(failedState('searchFlight'));
  }

  // ========================= SET DATA
  setOriginDestination = (modalType, data) => {
    if (modalType == 1) {
      // departure
      this.setState({
        visibleModal: null,
        origin: data,
      });
    } else if (modalType == 2) {
      // destination
      this.setState({
        visibleModal: null,
        destination: data,
      });
    }
  };
  setPassengerData = value => {
    this.setState({ totalPax: value });
  };

  setSeatClass = seatClass => {
    this.setState({
      seatClassSelected: seatClass.index,
      seatClass: seatClass.title,
      visibleModal: null,
    });
  };

  setDepartureDate = ({ date }) => {
    this.setState({
      departureDate: moment(date).format('YYYY-MM-DD'),
      visibleModal: null,
      focus: date,
    });
  };
  setReturnDate = ({ date }) => {
    this.setState({
      returnDate: moment(date).format('YYYY-MM-DD'),
      visibleModal: null,
    });
  };
  //  ==================== SET DATA

  // ==================== GLOBAL FUNC
  searchFlight = () => {
    // let payloadData = {
    //     origin: this.state.origin.code,
    //     destination: this.state.destination.code,
    //     departureDate: this.state.departureDate,
    //     adults: this.state.adult
    // }

    // if(this.state.flightType == "return") {
    //     payloadData.returnDate = this.state.returnDate
    // }
    // if(this.state.child > 0) {
    //     payloadData.childs = this.state.child
    // }
    // if(this.state.infant > 0) {
    //     payloadData.infants = this.state.infant
    // }

    // this.props.dispatch(actionSearchFlight(payloadData))
    // .then((res) => {
    //     console.log(res)
    //     if(res.type == "SEARCH_FLIGHT_SUCCESS") {
    //         this.props.navigation.navigate('SearchResult')
    //     }
    // })

    this.props.navigation.navigate('GroupBookingResult');
  };

  resetModal = () => {
    this.setState({
      visibleModal: null,
      departureResult: [],
      destinationResult: [],
    });
  };
  _toggleModal = modal => {
    // for open modal : 1 = departure, 2 = destination, 3 = passenger, 4 = seat class
    this.setState({ visibleModal: modal });
  };

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  handleCHange = data => {
    console.log('Data nya adalah => ' + data);
  };
  _keyExtractor = (item, index) => item.code;
  // ========================== GLOBAL FUNC

  // ============================= SEARCHING AUTOCOMPLETE
  searchingDeparture = val => {
    if (val !== '') {
      this.setState(
        {
          departureResult: '',
        },
        () => {
          let data = this.filterValuePart(this.state.departureList, val);
          this.setState({
            departureResult: data,
          });
        }
      );
    } else {
      this.setState({
        departureResult: [],
      });
    }
  };

  filterValuePart = (arr, part) => {
    return arr.filter(function(item) {
      const itemData = item.code.toUpperCase() + ' ' + item.name.toUpperCase();
      const textData = part.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
  };
  // ===================================== SEARCHING AUTOCOMPLETE

  render() {
    const isDateBlocked = date => date.isBefore(moment(), 'day');

    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <ScrollView style={{ marginBottom: 60 }}>
          <HeaderPage
            title="Group Booking"
            callback={() => this.goBackHeader()}
            {...this.props}
          />
          <SubHeaderPage title="Booking your flight" />

          <Grid style={styles.sectionToggle}>
            <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
              <MultiSwitch
                currentStatus={'Open'}
                disableScroll={value => {}}
                onStatusChanged={text => {
                  if (text == 'Single') {
                    this.setState({ flightType: 'single' });
                  } else if (text == 'Return') {
                    this.setState({ flightType: 'return' });
                  }
                }}
              />
            </Col>
          </Grid>
          <Grid style={styles.sectionInput}>
            <Col>
              <TouchableOpacity onPress={() => this._toggleModal(1)}>
                <InputText
                  placeholder={this.state.origin.name}
                  label="From"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Grid>
          <Grid style={styles.sectionInput}>
            <Col>
              <TouchableOpacity onPress={() => this._toggleModal(2)}>
                <InputText
                  placeholder={this.state.destination.name}
                  label="To"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Grid>
          <Grid style={styles.sectionInput}>
            <Col>
              <TouchableOpacity onPress={() => this._toggleModal(5)}>
                <InputText
                  placeholder={moment(this.state.departureDate).format(
                    'DD MMM YYYY'
                  )}
                  label="Departure Date"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
            {this.state.flightType == 'return' ? (
              <Col>
                <TouchableOpacity onPress={() => this._toggleModal(6)}>
                  <InputText
                    placeholder={moment(this.state.returnDate).format(
                      'DD MMM YYYY'
                    )}
                    label="Return Date"
                    editable={false}
                    onChangeText={data => this.handleCHange(data)}
                  />
                </TouchableOpacity>
              </Col>
            ) : (
              <Col>
                <Text></Text>
              </Col>
            )}
          </Grid>
          <Grid style={styles.sectionInput}>
            <Col>
              <TouchableOpacity onPress={() => this._toggleModal(4)}>
                <InputText
                  placeholder={this.state.seatClass}
                  label="Seat Class"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Grid>
          <Grid style={styles.sectionInput}>
            <Col>
              <InputText
                placeholder={'e.g ' + this.state.totalPax}
                label="Total Pax"
                editable={true}
                onChangeText={data => this.setPassengerData(data)}
              />
            </Col>
          </Grid>
        </ScrollView>

        <View style={styles.sectionButtonArea}>
          <View style={styles.sectionButton}>
            {this.props.singleSearchFetch ? (
              <DotIndicator
                size={10}
                style={{ flex: 1, backgroundColor: '#FFFFFF' }}
                color="#ed6d00"
              />
            ) : (
              <Button
                label="Search Flight"
                onClick={() => this.searchFlight()}
              />
            )}
          </View>
        </View>

        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={
            this.state.visibleModal === 5 || this.state.visibleModal === 6
          }
          style={styles.bottomModal}
        >
          <View style={styles.modalContent}>
            <Grid style={styles.modalHeaderDatepicker}>
              <Row style={{ paddingBottom: 7.5 }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={() => this.resetModal()}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color="#FFFFFF"
                      size={28}
                    />
                  </Col>
                  <Col size={8} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 5
                        ? 'Departure Date'
                        : 'Return Date'}
                    </Text>
                  </Col>
                </TouchableOpacity>
              </Row>
            </Grid>
            <Grid>
              <Col>
                <View style={{ flex: 1 }}>
                  <Dates
                    date={this.state.date}
                    onDatesChange={
                      this.state.visibleModal === 5
                        ? this.setDepartureDate
                        : this.setReturnDate
                    }
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    focusedInput={this.state.focus}
                    isDateBlocked={isDateBlocked}
                    focusedInput={this.state.focus}
                  />
                  <Text
                    style={{
                      paddingTop: 20,
                      fontSize: 16,
                      color: '#000',
                      paddingLeft: 20,
                    }}
                  >
                    {this.state.visibleModal === 5
                      ? 'Departure Date: ' +
                        moment(this.state.departureDate).format('DD MMM YYYY')
                      : 'Return Date: ' +
                        moment(this.state.returnDate).format('DD MMM YYYY')}
                  </Text>
                </View>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL DEPARTURE & DESTINATION ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={
            this.state.visibleModal === 1 || this.state.visibleModal === 2
          }
          style={styles.bottomModal}
        >
          <View style={styles.modalContent}>
            <Grid style={styles.modalHeader}>
              <Row style={{ paddingBottom: 7.5 }}>
                <TouchableOpacity
                  style={{ flexDirection: 'row', flex: 1 }}
                  onPress={() => this.resetModal()}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color="#FFFFFF"
                      size={28}
                    />
                  </Col>
                  <Col size={8} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 1
                        ? 'Choose Departure'
                        : 'Choose Destination'}
                    </Text>
                  </Col>
                </TouchableOpacity>
              </Row>
              <Row style={{ paddingBottom: 15 }}>
                <Col>
                  <SearchInput
                    placeholder="Enter city or terminal"
                    onChangeText={data => this.searchingDeparture(data)}
                  />
                </Col>
              </Row>
            </Grid>
            <Grid>
              <Col
                style={{
                  backgroundColor: '#FFFFFF',
                  padding: 20,
                  paddingTop: 10,
                }}
              >
                <ScrollView>
                  <Grid
                    style={{
                      flex: 1,
                      backgroundColor: '#FFFFFF',
                      padding: 10,
                      paddingBottom: 10,
                      paddingLeft: 0,
                    }}
                  >
                    <Col style={{ alignItems: 'flex-start' }}>
                      <Text
                        style={{
                          fontFamily: fontBold,
                          color: '#000000',
                          fontSize: 18,
                        }}
                      >
                        Popular destination
                      </Text>
                    </Col>
                  </Grid>
                  <Grid>
                    <Col>
                      <FlatList
                        keyExtractor={this._keyExtractor}
                        data={
                          this.state.departureResult.length > 0
                            ? this.state.departureResult
                            : this.state.destinationPopular
                        }
                        renderItem={({ item }) => (
                          <Grid
                            style={{
                              flex: 1,
                              backgroundColor: '#FFFFFF',
                              padding: 10,
                              borderBottomColor: '#d5d5d5',
                              borderBottomWidth: 0.5,
                            }}
                          >
                            <TouchableOpacity
                              style={{ flexDirection: 'row', flex: 1 }}
                              onPress={() =>
                                this.setOriginDestination(
                                  this.state.visibleModal,
                                  item
                                )
                              }
                            >
                              <Col style={{ alignItems: 'flex-start' }}>
                                <Text
                                  style={{
                                    fontFamily: fontReguler,
                                    color: '#000',
                                    fontSize: 16,
                                  }}
                                >
                                  {item.name}
                                </Text>
                              </Col>
                              <Col style={{ alignItems: 'flex-end' }}>
                                <View
                                  style={{
                                    backgroundColor: '#f8f8f8',
                                    width: 50,
                                    height: 25,
                                    alignItems: 'flex-end',
                                    justifyContent: 'center',
                                    borderRadius: 5,
                                  }}
                                >
                                  <Text
                                    style={{
                                      fontFamily: fontReguler,
                                      color: '#838383',
                                      fontSize: 12,
                                      alignSelf: 'center',
                                    }}
                                  >
                                    {item.code}
                                  </Text>
                                </View>
                              </Col>
                            </TouchableOpacity>
                          </Grid>
                        )}
                      />
                    </Col>
                  </Grid>
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL DEPARTURE & DESTINATION ===================================== */}

        {/* =============================== MODAL SEAT CLASS ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 4}
          style={styles.bottomModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#838383',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col
                  size={2}
                  style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                >
                  <Icon
                    name="close"
                    type="evilIcon"
                    color="#008195"
                    size={20}
                  />
                </Col>
                <Col
                  size={8}
                  style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                >
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Seat Class
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: '#FFFFFF' }}>
                <ScrollView>
                  {this.state.seatClassType.map((data, index) => {
                    return (
                      <Grid
                        key={index}
                        style={{
                          flex: 1,
                          backgroundColor: '#FFFFFF',
                          padding: 10,
                          borderBottomColor: '#d5d5d5',
                          borderBottomWidth: 0.5,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => this.setSeatClass(data)}
                          style={{ flex: 1 }}
                        >
                          <Row>
                            <Col style={{ alignItems: 'flex-start' }}>
                              <Text
                                style={{
                                  fontFamily: fontReguler,
                                  color: '#000',
                                  fontSize: 16,
                                }}
                              >
                                {data.title}
                              </Text>
                            </Col>
                            <Col
                              style={{
                                alignItems: 'flex-end',
                                justifyContent: 'center',
                              }}
                            >
                              {this.state.seatClassSelected == data.index ? (
                                <Icon
                                  name="check"
                                  type="feather"
                                  color="#008195"
                                  size={22}
                                />
                              ) : (
                                <Text></Text>
                              )}
                            </Col>
                          </Row>
                        </TouchableOpacity>
                      </Grid>
                    );
                  })}
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL SEAT CLASS ===================================== */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    flight: state.flight,
    singleSearchFetch: state.flight.singleSearchFetch,
  };
}
export default connect(mapStateToProps)(GroupBooking);
