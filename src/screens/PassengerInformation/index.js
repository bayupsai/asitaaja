import React from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Dimensions,
  Switch,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { InputText } from '../../elements/TextInput';
import { Button } from '../../elements/Button';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { fontExtraBold, fontReguler, fontBold } from '../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class PassengerInfromation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalAdult: [1, 2, 3],
      totalChild: [1],
      totalInfant: [1],
      sameContact: true,
      visibleModal: null,
      titleSelected: 1,
      listTitle: [
        { index: 1, title: 'Mr' },
        { index: 2, title: 'Mrs' },
        { index: 3, title: 'Ms' },
        { index: 4, title: 'Mstr' },
      ],
      passengerData: {
        adults: [],
        children: [],
        infants: [],
      },
    };
  }

  // ====== SET DATA
  setTitlePassenger = data => {
    this.setState({
      visibleModal: null,
    });
  };
  // ====== SET DATA
  // ====== GLOBAL FUNC
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  toggleSwitch = () => {
    this.setState({ sameContact: !this.state.sameContact });
  };

  formOrder = () => {
    this.props.navigation.navigate('PaymentOrder');
  };
  generateDataPassenger(adults = 1, field) {
    let { dataPassenger } = this.state;
    for (let i = 0; i < adults; i++) {
      dataPassenger[field].push({
        title: '',
        fullName: '',
        nationality: '',
        dateOfBird: '',
      });
    }
    this.setState({ dataPassenger });
  }
  // ====== GLOBAL FUNC

  render() {
    return (
      <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
        <HeaderPage
          title="Flight"
          callback={() => this.goBackHeader()}
          {...this.props}
        />
        <SubHeaderPage title="Passenger Information" />
        <Grid>
          <Row style={styles.content}>
            <Row style={styles.cardBox}>
              <Col size={7} style={{ padding: 15 }}>
                <Text style={styles.textBody}>Login or register</Text>
                <Text style={styles.textBody}>to book faster and easier</Text>
              </Col>
              <Col style={styles.sectionButtonLogin} size={3}>
                <Button
                  size="small"
                  colorButton="buttonColor"
                  label="Login"
                  onClick={() => alert('Login')}
                />
              </Col>
            </Row>
          </Row>
        </Grid>
        <Grid style={[styles.sectionInput, { marginTop: -20 }]}>
          <Col
            size={7}
            style={{ color: 'black', paddingBottom: 5, paddingLeft: 18 }}
          >
            <Text style={styles.textHeader}> Same as contact person</Text>
          </Col>
          <Col size={3} style={{ paddingRight: 15 }}>
            <Switch
              style={{ fontSize: 15 }}
              onValueChange={this.toggleSwitch}
              value={!this.state.sameContact}
            />
          </Col>
        </Grid>
        <Grid style={styles.sectionInput}>
          <Row style={{ paddingBottom: 5, paddingLeft: 20 }}>
            <Text style={styles.textHeader}>Adult </Text>
            <Text style={styles.textHeader}>#1</Text>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col size={2.5}>
              <TouchableOpacity onPress={() => this._toggleModal(1)}>
                <InputText
                  size="small"
                  placeholder="Mr"
                  label="Title *"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
            <Col size={7.5}>
              <TouchableOpacity>
                <InputText
                  placeholder="e.g Amir Budi"
                  label="Fullname *"
                  editable={true}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col>
              <TouchableOpacity>
                <InputText
                  size="small"
                  placeholder="Indonesia"
                  label="Nationality *"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col>
              <TouchableOpacity>
                <InputText
                  placeholder="Indonesia"
                  label="Date of Birth*"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
        <Grid style={styles.sectionInput}>
          <Row style={{ paddingBottom: 5, paddingLeft: 20 }}>
            <Text style={styles.textHeader}>Adult </Text>
            <Text style={styles.textHeader}>#2</Text>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col size={2.5}>
              <TouchableOpacity onPress={() => this._toggleModal(1)}>
                <InputText
                  size="small"
                  placeholder="Mr"
                  label="Title *"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
            <Col size={7.5}>
              <TouchableOpacity>
                <InputText
                  placeholder="e.g Amir Budi"
                  label="Fullname *"
                  editable={true}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col>
              <TouchableOpacity>
                <InputText
                  size="small"
                  placeholder="Indonesia"
                  label="Nationality *"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col>
              <TouchableOpacity>
                <InputText
                  placeholder="Indonesia"
                  label="Date of Birth*"
                  editable={false}
                  onChangeText={data => this.handleCHange(data)}
                />
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
        {/* {
                    this.props.flight.map((data, index) => {
                        return (
                            <Grid key={index} style={styles.sectionInput}>
                                <Row style={{paddingBottom: 5, paddingLeft: 20}}><Text style={styles.textHeader}>Adult</Text><Text style={styles.textHeader}> {index}</Text></Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col size={2.5}>
                                        <TouchableOpacity>
                                            <InputText size="small" placeholder="Mr" label="Title *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                    <Col size={7.5}>
                                        <TouchableOpacity>
                                            <InputText placeholder="e.g Amir Budi" label="Fullname *" editable={true} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col>
                                        <TouchableOpacity>
                                            <InputText size="small" placeholder="Indonesia" label="Nationality *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col>
                                        <TouchableOpacity>
                                            <InputText placeholder="Indonesia" label="Date of Birth*" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                            </Grid>
                        )
                    })
                } */}

        {/* {
                    this.state.totalChild.map((index) => {
                        return (
                            <Grid style={styles.sectionInput}>
                                <Row style={{paddingBottom: 5, paddingLeft: 18}}><Text style={styles.textHeader}>Child</Text><Text style={styles.textHeader}> {index}</Text></Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col size={2.5}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText size="small" placeholder="(+62)" label="Code *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                    <Col size={7.5}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText placeholder="e.g 8218375003" label="Phone Number *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col size={3}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText size="small" placeholder="Indonesia" label="Nationality *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                    <Col size={7}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText placeholder="Indonesia" label="Date of Birth*" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                            </Grid>
                        )
                    })
                } */}
        {/* {
                    this.state.totalInfant.map((index) => {
                        return (
                            <Grid style={styles.sectionInput}>
                                <Row style={{paddingBottom: 5, paddingLeft: 18}}><Text style={styles.textHeader}>Infant</Text><Text style={styles.textHeader}> {index}</Text></Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col size={2.5}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText size="small" placeholder="(+62)" label="Code *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                    <Col size={7.5}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText placeholder="e.g 8218375003" label="Phone Number *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                                <Row style={{paddingTop: 10}}>
                                    <Col size={3}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText size="small" placeholder="Indonesia" label="Nationality *" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                    <Col size={7}>
                                        <TouchableOpacity onPress={() => alert('xxx') }>
                                            <InputText placeholder="Indonesia" label="Date of Birth*" editable={false} onChangeText={(data) => this.handleCHange(data) }  />
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                            </Grid>
                        )
                    })
                } */}
        {/* <Grid style={{ marginTop: 20, marginBottom: 20, marginLeft: 20, marginRight: 20 }}>
                    <Row style={{ padding: 10, backgroundColor: "#FFFFFF" }}>
                        <Col size={7} style={{ paddingRight: 10 }}> 
                            <Text style={styles.textBody}>
                                Save up to Rp. 500.000, combine your Flight with 
                                <Text style={{ fontSize: 16, color: "#008195", fontWeight: "bold" }}> Hotel now! </Text> 
                            </Text>
                        </Col>
                        <Col style={{ alignItems: "center", justifyContent: "center" }} size={3}>
                            <Button size="small" colorButton="buttonColor" label="ADD" onClick={() => console.log("hotel") }/>
                        </Col>
                    </Row>
                </Grid> */}
        <Grid>
          <Row style={{ paddingBottom: 10 }}>
            <Col style={styles.sectionButtonArea}>
              <Button label="Continue Payment" onClick={this.formOrder} />
            </Col>
          </Row>
        </Grid>

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#838383',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col
                  size={2}
                  style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                >
                  <Icon
                    name="close"
                    type="evilIcon"
                    color="#008195"
                    size={20}
                  />
                </Col>
                <Col
                  size={8}
                  style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                >
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Choose Title
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={{ padding: 10 }}>
              <Col style={{ paddingLeft: 10 }}>
                {this.state.listTitle.map((data, index) => {
                  return (
                    <Grid
                      key={index}
                      style={{
                        flex: 1,
                        backgroundColor: '#FFFFFF',
                        padding: 10,
                        borderBottomColor: '#d5d5d5',
                        borderBottomWidth: 0.5,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setTitlePassenger(data)}
                        style={{ flex: 1 }}
                      >
                        <Col style={{ alignItems: 'flex-start' }}>
                          <Text
                            style={{
                              fontFamily: fontReguler,
                              color: '#000',
                              fontSize: 16,
                            }}
                          >
                            {data.title}
                          </Text>
                        </Col>
                        <Col
                          style={{
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}
                        >
                          {this.state.titleSelected == data.index ? (
                            <Icon
                              name="check"
                              type="feather"
                              color="#008195"
                              size={22}
                            />
                          ) : (
                            <Text></Text>
                          )}
                        </Col>
                      </TouchableOpacity>
                    </Grid>
                  );
                })}
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    flight: state.flight,
    singleSearchFetch: state.flight.singleSearchFetch,
  };
}
export default connect(mapStateToProps)(PassengerInfromation);

const styles = StyleSheet.create({
  textHeader: {
    fontSize: 14,
    fontFamily: fontExtraBold,
    color: '#424242',
  },
  textBody: {
    fontSize: 14,
    color: '#434a5e',
    fontFamily: fontReguler,
  },
  content: {
    paddingBottom: -10,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    transform: [{ translateY: -25 }],
  },
  cardBox: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 70,
    borderRadius: 5,
    marginBottom: 25,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sectionInput: {
    paddingTop: -20,
    paddingBottom: 10,
  },
  sectionButtonLogin: {
    paddingRight: 30,
    width: 30,
  },
  sectionButtonArea: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  // MODAL TITLE
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // MODAL TITLE
});
