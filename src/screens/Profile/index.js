import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

//Componenets
import HeaderProfile from './Component/HeaderProfile';
import DisplayTabsProfile from './Component/DisplayTabsProfile';
import { TouchableOpacity } from 'react-native-gesture-handler';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ backgroundColor: 'fff', flex: 1 }}>
        <ScrollView>
          <View style={styles.headerStyle}>
            <HeaderProfile />
          </View>
          <Grid style={styles.tabsStyle}>
            <Col>
              <DisplayTabsProfile />
            </Col>
          </Grid>
          <Row>
            <TouchableHighlight
              onPress={() => this.props.navigation.navigate('MenuProfile')}
            >
              <Text>Menu Profile</Text>
            </TouchableHighlight>
          </Row>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerStyle: {
    height: deviceHeight / 3.6,
    width: deviceWidth,
    backgroundColor: 'black',
  },
  tabsStyle: {
    backgroundColor: '#f0f0f0',
    height: 'auto',
    width: deviceWidth,
  },
});
