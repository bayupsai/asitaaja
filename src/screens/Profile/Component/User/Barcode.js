import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

//Import Components
import HeaderPageProfile from '../../../../components/HeaderPageProfile';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

export default class Barcode extends PureComponent {
  handleBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPageProfile title="Profile" callback={this.handleBack} />
        <Grid style={{ paddingLeft: 22, paddingBottom: 10, paddingTop: 30 }}>
          <Col style={styles.cardQrcode}>
            <Row>
              <Text style={styles.textBarcode}>QR Code</Text>
            </Row>
            <Image
              source={require('../../../../assets/img/qrcode.png')}
              style={styles.imageQrCode}
            />
          </Col>
        </Grid>
        <Grid style={{ paddingLeft: 22, paddingBottom: 10 }}>
          <Col style={styles.cardBarcode}>
            <Row style={{ paddingTop: 20 }}>
              <Text style={styles.textBarcode}>Bar Code</Text>
            </Row>
            <Image
              source={require('../../../../assets/img/bar-code.png')}
              style={styles.imageBarcode}
            />
          </Col>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f0f0f0',
    flex: 1,
  },
  cardQrcode: {
    paddingTop: 20,
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 230,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardBarcode: {
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 180,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBarcode: {
    fontSize: 16,
    color: '#000000',
  },
  imageQrCode: {
    height: deviceHeight / 3.8,
    width: deviceWidth,
    resizeMode: 'contain',
    paddingBottom: 10,
  },
  imageBarcode: {
    height: deviceHeight / 5,
    width: deviceWidth,
    resizeMode: 'contain',
  },
});
