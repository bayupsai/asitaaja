import React, { PureComponent } from 'react';
import {
  Text,
  Image,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';

//Import Component
import HeaderPageProfile from '../../../../components/HeaderPageProfile';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default class MenuProfile extends PureComponent {
  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.content}>
        <HeaderPageProfile title="Menu Profile" callback={this.goBack} />
        {/* <Grid style={{ paddingLeft: 22, paddingBottom: 10}}> */}
        <Col style={{ paddingLeft: 22, paddingBottom: 10 }}>
          <Row style={{ height: 35 }}>
            <Col>
              <Text style={{ fontSize: 18, fontWeight: '800', color: 'black' }}>
                AMIR REVON
              </Text>
            </Col>
          </Row>
          <Row style={{ height: 30 }}>
            <Col
              size={1}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Icon name="email" color="black" type="meterialicons" />
            </Col>
            <Col size={9}>
              <Text style={{ fontSize: 12 }}>amir.revon@gmail.com</Text>
            </Col>
          </Row>
          <Row style={{ height: 30 }}>
            <Col
              size={1}
              style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}
            >
              <Icon name="phone" color="black" type="feather" />
            </Col>
            <Col size={9}>
              <Text style={{ fontSize: 12 }}>+62 xxx xxxx</Text>
            </Col>
          </Row>
          <Row style={styles.sectionHr} />
        </Col>

        {/* </Grid> */}

        {/* <Grid style={styles.sectionGrid}/> */}

        <Grid style={{ paddingLeft: 22, paddingBottom: 10, paddingTop: 30 }}>
          <Row onPress={() => alert('View Profile')}>
            <Col size={8}>
              <Text style={styles.textLabel}>View Profile</Text>
            </Col>
            <Col size={2} style={{ paddingTop: 5 }}>
              <Icon name="right" color="grey" type="antdesign" />
            </Col>
          </Row>
          <View style={styles.sectionHr} />
          <Row>
            <Col size={8}>
              <Text style={styles.textLabel}>Claim Missing Miles</Text>
            </Col>
            <Col size={2} style={{ paddingTop: 5 }}>
              <Icon name="right" color="grey" type="antdesign" />
            </Col>
          </Row>
          <Row style={styles.sectionHr} />
          <Row>
            <Col size={8}>
              <Text style={styles.textLabel}>Calculator Miles</Text>
            </Col>
            <Col size={2} style={{ paddingTop: 5 }}>
              <Icon name="right" color="grey" type="antdesign" />
            </Col>
          </Row>
          <Row style={styles.sectionHr} />
          <Row>
            <Col size={8}>
              <Text style={styles.textLabel}>Show QR Code</Text>
            </Col>
            <Col size={2} style={{ paddingTop: 5 }}>
              <Icon name="right" color="grey" type="antdesign" />
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  sectionGrid: {
    width: 100 + '%',
    height: 5,
    backgroundColor: '#fafafa',
    alignSelf: 'center',
  },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    backgroundColor: '#BDBDBD',
    alignSelf: 'center',
  },
  textLabel: {
    fontSize: 14,
    fontWeight: '400',
    color: 'grey',
  },
});
