import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dates from 'react-native-dates';
import moment from 'moment';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';

//Import Components
import HeaderPageProfile from '../../../../components/HeaderPageProfile';
import { InputText, AddressInput } from '../../../../elements/TextInput';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
  fontReguler,
  fontBold,
  fontExtraBold,
  thameColors,
} from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

export default class FormProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      fullName: '',
      nationality: '',
      dateOfBirth: '',
      address: '',
      streetAddress: '',
      focus: 'startDate',
      date: null,
      birthDate: moment().format('YYYY-MM-DD'),
      visibleModal: null,
      seatClassSelected: 1,
      seatClassType: [
        { index: 1, title: 'Economy' },
        { index: 2, title: 'Business' },
        { index: 3, title: 'First Class' },
        { index: 4, title: 'Premium Economy' },
      ],
    };
  }

  titleName = () => {
    this.setState({
      title: '',
    });
  };

  setBirthDate = ({ date }) => {
    this.setState({
      birthDate: moment(date).format('YYYY-MM-DD'),
      visibleModal: null,
      focus: date,
    });
  };

  setSeatClass = seatClass => {
    this.setState({
      seatClassSelected: seatClass.index,
      seatClass: seatClass.title,
      visibleModal: null,
    });
  };

  resetModal = () => {
    this.setState({
      visibleModal: null,
      departureResult: [],
      destinationResult: [],
    });
  };
  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  handleBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPageProfile title="Profile" callback={this.handleBack} />
        <ScrollView style={{ marginBottom: 5 }}>
          <View style={styles.content}>
            <Grid>
              <Col>
                <Text
                  style={{
                    padding: 20,
                    fontSize: 16,
                    fontFamily: fontExtraBold,
                  }}
                >
                  Personal Information
                </Text>
              </Col>
            </Grid>
            <Grid style={{ paddingBottom: 10 }}>
              <Col>
                <TouchableOpacity>
                  <InputText
                    onChangeText={data => this.setState({ type: data })}
                    onPress={() => this._toggleModal(4)}
                    label="Title*"
                    placeholder="Mrs"
                    editable={true}
                  />
                </TouchableOpacity>
              </Col>
            </Grid>
            <Grid style={{ paddingBottom: 10 }}>
              <Col>
                <InputText
                  onChangeText={data => this.setState({ type: data })}
                  label="Fullname*"
                  placeholder="e.g Amir Revon"
                />
              </Col>
            </Grid>
            <Grid>
              <Col size={5}>
                <TouchableOpacity>
                  <InputText
                    onChangeText={data => this.setState({ type: data })}
                    label="Nationality*"
                    placeholder="Indonesia"
                  />
                </TouchableOpacity>
              </Col>
              <Col size={5}>
                <TouchableOpacity onPress={() => this._toggleModal(5)}>
                  <InputText
                    onChangeText={data => this.setState({ type: data })}
                    placeholder={moment(this.state.birthDate).format(
                      'DD MMM YYYY'
                    )}
                    editable={false}
                    label="Date of Birth*"
                    placeholder="Birth Date"
                  />
                </TouchableOpacity>
              </Col>
            </Grid>
            <Grid style={{ paddingBottom: 10 }}>
              <Col>
                <Text
                  style={{
                    padding: 20,
                    fontSize: 16,
                    fontFamily: fontExtraBold,
                  }}
                >
                  Address
                </Text>
              </Col>
            </Grid>
            <Grid style={{ paddingBottom: 10 }}>
              <Col>
                <InputText
                  onChangeText={data => this.setState({ type: data })}
                  label="Address Type*"
                  placeholder="Private"
                />
              </Col>
            </Grid>
            <Grid style={{ paddingBottom: 10 }}>
              <Col>
                <AddressInput
                  onChangeText={data => this.setState({ type: data })}
                  label="Street Address"
                  placeholder="e.g Amir Revon"
                />
              </Col>
            </Grid>
          </View>
        </ScrollView>

        {/* =============================== MODAL SEAT CLASS ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 4}
          style={styles.bottomModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid
              style={{
                width: deviceWidth,
                height: 40,
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#838383',
                borderBottomWidth: 0.5,
              }}
            >
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col
                  size={2}
                  style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                >
                  <Icon
                    name="close"
                    type="evilIcon"
                    color="#008195"
                    size={20}
                  />
                </Col>
                <Col
                  size={8}
                  style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                >
                  <Text
                    style={{
                      color: '#000',
                      fontFamily: fontBold,
                      fontSize: 16,
                    }}
                  >
                    Seat Class
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: '#FFFFFF' }}>
                <ScrollView>
                  {this.state.seatClassType.map((data, index) => {
                    return (
                      <Grid
                        key={index}
                        style={{
                          flex: 1,
                          backgroundColor: '#FFFFFF',
                          padding: 10,
                          borderBottomColor: '#d5d5d5',
                          borderBottomWidth: 0.5,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => this.setSeatClass(data)}
                          style={{ flex: 1 }}
                        >
                          <Row>
                            <Col style={{ alignItems: 'flex-start' }}>
                              <Text
                                style={{
                                  fontFamily: fontReguler,
                                  color: '#000',
                                  fontSize: 16,
                                }}
                              >
                                {data.title}
                              </Text>
                            </Col>
                            <Col
                              style={{
                                alignItems: 'flex-end',
                                justifyContent: 'center',
                              }}
                            >
                              {this.state.seatClassSelected == data.index ? (
                                <Icon
                                  name="check"
                                  type="feather"
                                  color="#008195"
                                  size={22}
                                />
                              ) : (
                                <Text></Text>
                              )}
                            </Col>
                          </Row>
                        </TouchableOpacity>
                      </Grid>
                    );
                  })}
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL SEAT CLASS ===================================== */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    backgroundColor: '#f0f0f0',
    flex: 1,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontFamily: fontReguler,
    fontSize: 16,
  },
});
