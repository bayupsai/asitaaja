import React, { PureComponent } from 'react';
import { Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default class Redemption extends PureComponent {
  render() {
    return (
      <Grid style={styles.content}>
        <Row style={{ paddingBottom: 10, paddingTop: 15 }}>
          <Text style={styles.textHeader}> 4 Mei 2019</Text>
        </Row>
        <Row style={styles.cardBox}>
          <Col size={1.5}>
            <Row>
              <Image
                source={require('../../../../assets/icons/hotel-2.png')}
                style={styles.imageStyle}
              />
            </Row>
          </Col>
          <Col size={8}>
            <Row>
              <Text style={styles.textHeader}>Mandarin Hotel Singapore</Text>
            </Row>
            <Row>
              <Text>RFH82991777283</Text>
            </Row>
            <Row>
              <Text style={styles.textMileages}>+1000 Mileages</Text>
            </Row>
          </Col>
        </Row>
        <Row style={{ padding: 10 }} />
        <Row style={styles.cardBox}>
          <Col size={1.5}>
            <Row>
              <Image
                source={require('../../../../assets/icons/hotel-2.png')}
                style={styles.imageStyle}
              />
            </Row>
          </Col>
          <Col size={8}>
            <Row>
              <Text style={styles.textHeader}>Mandarin Hotel Singapore</Text>
            </Row>
            <Row>
              <Text>RFH82991777283</Text>
            </Row>
            <Row>
              <Text style={styles.textMileages}>+1000 Mileages</Text>
            </Row>
          </Col>
        </Row>

        <Row style={{ paddingBottom: 10, paddingTop: 20 }}>
          <Text style={styles.textHeader}> 3 Mei 2019</Text>
        </Row>
        <Row style={styles.cardBox}>
          <Col size={1.5}>
            <Row>
              <Image
                source={require('../../../../assets/icons/hotel-2.png')}
                style={styles.imageStyle}
              />
            </Row>
          </Col>
          <Col size={8}>
            <Row>
              <Text style={styles.textHeader}>Mandarin Hotel Singapore</Text>
            </Row>
            <Row>
              <Text>RFH82991777283</Text>
            </Row>
            <Row>
              <Text style={styles.textMileages}>+1000 Mileages</Text>
            </Row>
          </Col>
        </Row>
        <Row style={{ padding: 10 }} />
        <Row style={styles.cardBox}>
          <Col size={1.5}>
            <Row>
              <Image
                source={require('../../../../assets/icons/hotel-2.png')}
                style={styles.imageStyle}
              />
            </Row>
          </Col>
          <Col size={8}>
            <Row>
              <Text style={styles.textHeader}>Mandarin Hotel Singapore</Text>
            </Row>
            <Row>
              <Text>RFH82991777283</Text>
            </Row>
            <Row>
              <Text style={styles.textMileages}>+1000 Mileages</Text>
            </Row>
          </Col>
        </Row>
      </Grid>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 10,
    flex: 1,
    marginLeft: 10,
    backgroundColor: '#f0f0f0',
  },
  cardBox: {
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 70,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  textHeader: {
    color: 'black',
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '600',
  },
  textMileages: {
    color: '#11b5a5',
    fontFamily: 'Avenir',
    fontSize: 12,
  },
  imageStyle: {
    height: 45,
    width: 40,
    resizeMode: 'center',
    tintColor: '#1f796b',
  },
});
