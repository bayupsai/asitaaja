import * as React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';

import Tabs from './Tabs/Tabs';

//Route Tab
import TabOne from './Tabs/Transaction';
import TabTwo from './Tabs/Redemption';
import TabThree from './Tabs/Claim';
import { fontReguler } from '../../../base/constant';

//component imported
const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

//Master Modal
class DisplayModal extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      page: 0,
      modalVisible: true,
    };

    this._onTabChange = this._onTabChange.bind(this);
  }

  _onTabChange(tabIndex) {
    this.setState({ page: tabIndex });
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  render() {
    return (
      <View style={styles.container}>
        <Tabs>
          {/* First tab */}
          <TabOne {...this.props} title="Transaction" />
          {/* Second tab */}
          <TabTwo {...this.props} title="Redemption" />
          {/* Third tab */}
          <TabThree {...this.props} title="Claim" />
        </Tabs>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
  },
  // Tab content container
  content: {
    flex: 1, // Take up all available space
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
    backgroundColor: '#C2185B', // Darker background for content area
  },
  // Content header
  header: {
    margin: 5, // Add margin
    color: '#FFFFFF', // White color
    fontFamily: 'Avenir', // Change font family
    fontSize: 26, // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: fontReguler,
    fontSize: 18,
  },
});

export default DisplayModal;
