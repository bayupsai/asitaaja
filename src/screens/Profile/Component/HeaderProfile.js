import React, { PureComponent } from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  ImageBackground,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';

import Barcode from './User/Barcode';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default class HeaderProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
    };
  }

  // ==== CLOSE MODAL
  resetModal = () => {
    this.setState({ visibleModal: null });
  };
  // ==== CLOSE MODAL

  _toggleModal = modal => {
    // for open modal : 1 = Card
    this.setState({ visibleModal: modal });
  };

  render() {
    return (
      <Grid style={styles.container}>
        <ImageBackground
          source={require('../../../assets/img/batik.png')}
          style={styles.imageBackground}
        >
          <Row>
            <Col size={7} style={{ paddingLeft: 15 }}>
              <Text style={styles.textName}>AMIR REVON</Text>
            </Col>
            <Col
              size={3}
              style={{ height: 200, width: 500, justifyContent: 'flex-end' }}
            >
              <Row style={{ paddingLeft: 90 }}>
                <TouchableHighlight
                  onPress={() => this.props.navigation.navigate('Barcode')}
                >
                  <Icon
                    name="dots-three-vertical"
                    color="white"
                    type="entypo"
                  />
                </TouchableHighlight>
              </Row>
            </Col>
          </Row>
          <Row style={styles.paddingRow}>
            <Text style={styles.textCard}>P L A T I N U M</Text>
          </Row>
          <Row style={{ paddingTop: 10, paddingLeft: 15 }}>
            <Text style={styles.textMiles}>Your Miles</Text>
          </Row>
          <Row style={styles.paddingRow}>
            <Col size={8} style={{ paddingBottom: 3 }}>
              <Text style={styles.textDistance}>980000</Text>
            </Col>
            <Col size={2} style={{ paddingBottom: 3 }}>
              <Text style={styles.textRedeem}> Redeem</Text>
            </Col>
          </Row>
          <Row style={styles.paddingRow}>
            <Col size={8}>
              <Row>
                <Text style={styles.textDateExpire}>17000 Miles</Text>
                <Text style={styles.textExpire}>are about to experience</Text>
                <Text style={styles.textDateExpire}>21/12/19</Text>
              </Row>
            </Col>
            <Col size={2}>
              <TouchableOpacity
                style={{ paddingLeft: 15 }}
                onPress={() => this._toggleModal(1)}
              >
                <Icon name="credit-card" color="white" type="entypo" />
              </TouchableOpacity>
            </Col>
          </Row>
        </ImageBackground>

        {/* =============================== MODAL CARD ===================================== */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.visibleModal === 1}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid style={styles.cardModal}>
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={() => this.setState({ visibleModal: null })}
              >
                <Col
                  size={2}
                  style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                >
                  <Icon
                    name="close"
                    type="evilIcon"
                    color="#008195"
                    size={20}
                  />
                </Col>
                <Col
                  size={8}
                  style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                >
                  <Text
                    style={{ color: '#000', fontWeight: '500', fontSize: 16 }}
                  >
                    Your Card
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>

            <Grid
              style={{ width: deviceWidth, paddingLeft: 22, paddingBottom: 10 }}
            >
              <Col style>
                <Image
                  source={require('../../../assets/img/garuda-miles.png')}
                  style={styles.imageStyle}
                />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL CARD ===================================== */}
      </Grid>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: deviceHeight,
    width: deviceWidth,
    backgroundColor: 'black',
    flex: 1,
    position: 'absolute',
    zIndex: 0,
  },
  imageBackground: {
    height: deviceHeight / 3.6,
    width: deviceWidth,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  paddingTop: {
    paddingLeft: 15,
    paddingTop: 15,
    flex: 1,
  },
  paddingRow: {
    paddingLeft: 15,
    flex: 1,
    paddingTop: -5,
    paddingBottom: 10,
  },
  textName: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: '600',
  },
  textCard: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: '400',
  },
  textRedeem: {
    color: '#ee7815',
    paddingLeft: 7,
  },
  textMiles: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: '200',
  },
  textDistance: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '600',
  },
  textDateExpire: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '800',
  },
  textExpire: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontFamily: 'Avenir',
    fontSize: 14,
    paddingLeft: 4,
    paddingRight: 4,
  },
  cardModal: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#838383',
    borderBottomWidth: 0.5,
  },
  //   redeemBox: {
  //     borderColor: "#e6e6e6",
  //     flexDirection: "row",
  //     justifyContent: "center",
  //     alignItems: "center",
  //     height: 30,
  //     borderRadius: 8,
  //     margin: 10,
  //     borderWidth: 0.5,
  //     borderLeftWidth: 1,
  //     borderTopWidth: 1,
  //     borderRightWidth: 1,
  //     borderBottomWidth: 1,
  //     borderTopLeftRadius: 6,
  //     borderTopRightRadius: 6,
  //     borderBottomRightRadius: 6,
  //     borderBottomLeftRadius: 6,
  //     paddingBottom: 10,
  //     transform: [{ translateX: -25 }]
  //   },
  imageStyle: {
    height: deviceHeight / 2.2,
    width: deviceWidth / 1.15,
    backgroundColor: 'black',
    resizeMode: 'center',
    backgroundColor: '#fafafa',
  },

  //MODAL CARD
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  //MODAL CARD
});
