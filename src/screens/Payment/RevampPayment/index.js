import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { CheckBox } from 'react-native-elements';

//Component
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import { fontBold, fontExtraBold, fontReguler } from '../../../base/constant';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
  },
  cardBox: {
    backgroundColor: '#fff',
    padding: 10,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: { fontSize: 16, color: '#222222', fontFamily: fontExtraBold },
  textBody: { fontFamily: fontReguler, color: '#c1c1c1' },
  textTime: { fontSize: 16, color: '#a2195b', fontFamily: fontBold },
  textBank: { fontFamily: fontReguler, color: '#222222' },
});

class RevampPayment extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      choicePayment: false,
    };
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
          <HeaderPage
            title="Payment Method"
            callback={() => this.goBackHeader()}
          />
          <SubHeaderPage />
          <Grid style={{ marginTop: -60, marginBottom: 25 }}>
            <Row style={styles.content}>
              <Col style={styles.cardBox}>
                <View style={{ marginBottom: 5, marginTop: 5 }}>
                  <Text style={styles.textBold}>Rp 1.900.834</Text>
                </View>
                <View style={{ marginBottom: 5, marginTop: 5 }}>
                  <Text style={styles.textBody}>
                    Please complete this payment in
                  </Text>
                </View>
                <View style={{ marginBottom: 5, marginTop: 5 }}>
                  <Text style={styles.textTime}>01h : 28m : 00s</Text>
                </View>
              </Col>
            </Row>
          </Grid>
          <Grid>
            <Col style={styles.cardBox}>
              <View style={{ marginTop: 10 }}>
                <Text style={styles.textBold}>
                  Credit Card / Debit / Installments
                </Text>
              </View>
              <View>
                <Image
                  style={{ width: deviceWidth / 2.5, height: 60 }}
                  resizeMode="center"
                  source={require('../../../assets/icons/icon_creditcard.png')}
                />
              </View>
            </Col>
          </Grid>
          <Grid>
            <Col style={{ marginLeft: 25, marginTop: 15, marginBottom: 15 }}>
              <Text style={styles.textBold}>Virtual Account</Text>
            </Col>
          </Grid>
          <Grid style={styles.cardBox}>
            <Row style={{ marginTop: 10, marginBottom: 10 }}>
              <Col size={3} style={{ marginLeft: 5 }}>
                <Image
                  style={{ width: deviceWidth / 4.5, height: 22 }}
                  resizeMode="center"
                  source={require('../../../assets/icons/icon_finpay.png')}
                />
              </Col>
              <Col
                size={5.5}
                style={{ alignItems: 'flex-end', justifyContent: 'center' }}
              >
                <Text style={styles.textBank}>Finpay Virtual Acc</Text>
              </Col>
              <Col
                size={1.5}
                style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}
              >
                <CheckBox
                  containerStyle={{
                    backgroundColor: '#ffffff',
                    padding: 0,
                    margin: 0,
                    marginLeft: 0,
                    borderWidth: 0,
                  }}
                  checked={this.state.choicePayment}
                  onPress={() =>
                    this.setState({ choicePayment: !this.state.choicePayment })
                  }
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle"
                  checkedColor="#828282"
                  uncheckedColor="#e6e6e6"
                />
              </Col>
            </Row>
          </Grid>
        </ScrollView>

        <View>
          {/* <ScrollView style={{backgroundColor: "#f0f0f0"}}>
                        <HeaderPage title="Payment Method" callback={() => this.goBackHeader()} />
                        <SubHeaderPage/>
                        <Grid style={{ marginTop: -60 }}>
                            <Row style={styles.content}>
                                <Col style={styles.cardBox}>
                                    <Row>
                                        <Col size={8}>
                                            <Text style={styles.orderTitle}>
                                                ORDER ID: {orderId}
                                            </Text>
                                        </Col>
                                        <Col size={2} style={{justifyContent:'center', alignItems:'flex-end' }}>
                                            <TouchableOpacity onPress={this.splashScreen} style={{ backgroundColor: "#EEEEEE", paddingLeft: 10, paddingRight: 10 }}>
                                                <Icon name="ios-arrow-down" type='ionicon' size={22} color="#838383" />
                                            </TouchableOpacity>
                                        </Col>
                                    </Row>
                                    <Row style={{ paddingTop: 20}}>
                                        <Col><Text style={styles.textHeader}>Total Payment</Text></Col>
                                        <Col style={{alignItems: "flex-end", justifyContent: "center" }}>
                                            <Text style={styles.textPrice}>IDR {numeral(totalPayment).format('0,0')}</Text>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Grid>

                        <TimerPayment />
                        {
                            (this.state.paymentFetching) ? 
                            <View style={{ backgroundColor: '#FFFFFF', margin: 20, padding: 10, marginBottom: 0, alignItems: "center", justifyContent: "center" }}>
                                <DotIndicator size={10} style={{ flex: 1}} color="#008195"/>
                                <Text style={{ color: "#000", fontWeight: "500" }}>Please wait, we still process your request</Text>
                            </View> : 
                            <Text></Text>
                        }
                        <BankPayment handlePayment={()=> this.doPayment() } {...this.props}/>


                    </ScrollView> */}
        </View>
      </View>
    );
  }
}
export default RevampPayment;
