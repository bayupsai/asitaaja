const dataVirtual = [
  // { id: 1, name: 'Finpay', type: 'virtual_account', image: require('../../assets/logos/finpay-logo3.png') },
  // { id: 2, name: 'BCA', type: 'virtual_account', image: require('../../assets/logos/bcaacc3x.png') },
  {
    id: 'permata_va',
    name: 'Permata Bank',
    type: 'virtual_account',
    image: require('../../assets/logos/permata.png'),
  },
  {
    id: 'bni_va',
    name: 'BNI',
    type: 'virtual_account',
    image: require('../../assets/logos/bniacc3x.png'),
  },
  // { id: 4, name: 'BRI', type: 'virtual_account', image: require('../../assets/logos/briacc3x.png') },
  // { id: 5, name: 'Mandiri', type: 'virtual_account', image: require('../../assets/logos/mandiriacc3x.png') },
  // { id: 6, name: 'Bersama', type: 'virtual_account', image: require('../../assets/logos/mandiri-logo2.png') },
];

const dataRetail = [
  {
    id: 7,
    name: 'Alfamart',
    type: 'retail',
    image: require('../../assets/logos/alfamart-logo3.png'),
  },
  {
    id: 8,
    name: 'Indomaret',
    type: 'retail',
    image: require('../../assets/logos/indomaret-logo3.png'),
  },
];

// export default { dataRetail, dataVirtual}
export { dataRetail, dataVirtual };
