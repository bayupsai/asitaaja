/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';

import { Icon } from 'react-native-elements';
import numeral from 'numeral';
import Modal from 'react-native-modal';
import { DotIndicator } from 'react-native-indicators';

import moment from 'moment';

// redux
import { connect } from 'react-redux';
import { actionPaymentFlight } from '../../redux/actions/FlightAction';
import { submitPacisPay } from '../../redux/actions/CargoAction';
import {
  actionOrdersHistory,
  actionSelectOrder,
} from '../../redux/actions/OrdersHistoryAction';
import { Timer } from '../../utilities/helpers';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';

// import TimerPayment from '../../screens/Payment/Component/TimerPayment'
// import BankPayment from '../../screens/Payment/Component/BankPayment'
// import ModalHotel from './Component/ModalHotel'
import { fontBold, fontReguler, thameColors } from '../../base/constant';
import AlertModal from '../../components/AlertModal';
import { scale } from '../../Const/ScaleUtils';

import { dataVirtual } from './dataDummy';
import { actionBatikPay } from '../../redux/actions/BatikActions';
import { actionPayMidtrans } from '../../redux/actions/PaymentAction';

const deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
  },
  cardBox: {
    backgroundColor: thameColors.white,
    padding: 10,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontSize: 16,
    color: thameColors.textBlack,
    fontFamily: fontBold,
  },
  textPrice: { fontSize: 16, color: thameColors.primary, fontFamily: fontBold },
  textBody: { fontFamily: fontReguler, color: thameColors.gray },
  textBank: { fontFamily: fontReguler, color: thameColors.textBlack },
  textBlue: {
    fontSize: 16,
    fontFamily: fontReguler,
    color: thameColors.oceanBlue,
  },
});

class PaymentOrder extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      timer: 3,
      visibleModal: null,
      paymentFetching: false,
      sourcePayment: this.props.navigation.state.params
        ? this.props.navigation.state.params.pages
        : 'flight',
      paramsProduct: this.props.navigation.state.params
        ? this.props.navigation.state.params.product
        : null,
      errMessage: '',
    };
  }

  componentDidMount() {
    this._orderData();
  }

  // Get Order Data
  _orderData = () => {
    const { dispatch, token } = this.props;
    dispatch(actionOrdersHistory(token));
  };

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  // modal
  resetModal = () => {
    this.setState({
      visibleModal: null,
    });
  };

  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  _alertModal = (id, message) => {
    return (
      <AlertModal
        type="normal"
        isVisible={this.state.visibleModal === id}
        title="Payment"
        contentText={message}
        onPress={() => this._toggleModal(null)}
        onDismiss={() => this._toggleModal(null)}
      />
    );
  };
  // modal

  doPaymentPacis = payload => {
    const newPayload = {
      paymentMethodId: payload.method,
      bookingCode: payload.invoiceNumber,
    };
    this.props.dispatch(submitPacisPay(newPayload)).then(res => {
      if (res.type === 'PACIS_PAY_SUCCESS') {
        const URLPayment = res.data.redirectUrl;
        this.props.navigation.navigate('CreditCard', {
          redirectURL: URLPayment,
          pageType: 'pacis',
        });
      } else {
        this.setState({ visibleModal: 1 }); //     "Payment failed, please try again later.",
      }

      this.setState({ paymentFetching: false, visibleModal: null });
    });
  };

  doPayment = method => {
    let amountNumber = 0;
    let { fullName } = this.props.contact;
    if (this.state.sourcePayment === 'flight') {
      amountNumber = this.props.dataBookingFlight.total.toString();
      fullName = this.props.contact.fullName;
    } else if (this.state.sourcePayment === 'train') {
      amountNumber = this.props.dataBookingTrain.total.toString();
    } else if (this.state.sourcePayment === 'ppob') {
      amountNumber = this.state.paramsProduct.harga.toString();
    } else if (this.state.sourcePayment === 'cargo') {
      const dataCargo = this.props.dataBookingCargo;
      amountNumber = dataCargo.pacisBooking.totalAmount;
    } else if (this.state.sourcePayment === 'hotel') {
      amountNumber =
        parseInt(this.props.dataHotelBook.booking.totalNet, 10) * 14000;
    } else if (this.state.sourcePayment === 'orderFlight') {
      fullName = this.props.selectOrder.contact_fullname;
    }

    this.setState({ paymentFetching: true, visibleModal: 99 });
    const payloadFlight = {
      // invoiceNumber: this.orderId(),
      // method: 1,
      // cardHolder: fullName,
      // amount: amountNumber.toString(),
      // agent_to_pay: '0',
      code: this.orderId(),
      product_id: 'FLIGHT',
      enabled_payments: [method],
    };
    const payloadHotel = {
      // transactionDetails: {
      //   bookingCode: this.orderId(),
      //   amount: amountNumber,
      // },
      // enabledPayments: ['credit_card'],
      code: this.orderId(),
      product_id: 'HOTEL',
      enabled_payments: [method],
    };

    if (this.state.sourcePayment === 'cargo') {
      this.doPaymentPacis();
    } else if (this.state.sourcePayment === 'hotel') {
      this.props
        .dispatch(actionPayMidtrans(payloadHotel))
        .then(res => {
          this.setState({ paymentFetching: false, visibleModal: null });
          if (res.type === 'PAYMENT_MIDTRANS_SUCCESS') {
            this.props.navigation.navigate('CreditCard', {
              redirectURL: res.data.redirect_url,
              pageType: this.state.sourcePayment,
            });
          } else {
            this.setState({ errMessage: res.type }, () => {
              this.setState({ visibleModal: 1 });
            });
          }
        })
        .catch(err => {
          this.setState({ errMessage: err.message }, () => {
            this.setState({ visibleModal: 1 });
          });
        });
    } else {
      this.props
        .dispatch(actionPayMidtrans(payloadFlight))
        .then(res => {
          this.setState({ paymentFetching: false, visibleModal: null });
          if (res.type === 'PAYMENT_MIDTRANS_SUCCESS') {
            this.props.navigation.navigate('CreditCard', {
              redirectURL: res.data.redirect_url,
              pageType: this.state.sourcePayment,
            });
          } else {
            this.setState({ visibleModal: 1 });
            // alert("Payment failed, please try again.")
          }
        })
        .catch(err => {
          this.setState({ errMessage: err.message }, () => {
            this.setState({ visibleModal: 1 });
          });
        });
    }
  };

  doPaymentVA = id => {
    const { dispatch } = this.props;
    this.setState({ visibleModal: 99 });
    const payload = {
      invoiceNumber: this.orderId(),
      method: id,
    };
    dispatch(actionBatikPay(payload))
      .then(res => {
        res.type === 'BATIK_PAY_SUCCESS'
          ? this.props.navigation.navigate('CreditCard', {
              redirectURL: res.data.redirect_url,
              pageType: this.state.sourcePayment,
            })
          : alert('Payment Failed');
        this.setState({ visibleModal: null });
      })
      .catch(err => {
        alert(err.message);
        this.setState({ visibleModal: null });
      });
  };

  loadingBooking = () => (
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: thameColors.white,
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        height: 'auto',
        flex: 0,
      }}
    >
      <Col style={{ alignItems: 'center' }}>
        <Row style={{ flex: 0 }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 16,
              color: thameColors.superBack,
            }}
          >
            Please Wait.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5 }}>
          <Text
            style={{
              fontWeight: '400',
              fontSize: 16,
              color: thameColors.superBack,
            }}
          >
            We still process your Payment.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5, alignItems: 'center' }}>
          <DotIndicator
            size={10}
            style={{ flex: 0, backgroundColor: thameColors.white }}
            color={thameColors.secondary}
          />
        </Row>
      </Col>
    </Grid>
  );

  renderType = pages => {
    // Pages Payment
    const {
      dataBookingFlight,
      loadingOrder,
      dataOrder,
      selectOrder,
    } = this.props;
    if (pages === 'hotel') {
      return (
        <Grid style={{ marginBottom: 20 }}>
          {/* <Col style={styles.cardBox}>
                        <View style={{ marginTop: 10, marginBottom: 10, alignItems: 'center' }}>
                            <Text style={[styles.textBold, { fontSize: 20 }]}>{dataHotelBook.detail}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontFamily: fontReguler, color: thameColors.darkGray }}>{dataHotelBook.guest} Guest</Text>
                                <Dot />
                                <Text style={{ fontFamily: fontReguler, color: thameColors.darkGray }}>{dataHotelBook.room} Night</Text>
                                <Dot />
                                <Text style={{ fontFamily: fontReguler, color: thameColors.darkGray }}>7 Room</Text>
                            </View>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <TouchableOpacity>
                                <Text style={{ fontFamily: fontBold, color: thameColors.primary }}>View Detail</Text>
                            </TouchableOpacity>
                        </View>
                    </Col> */}
        </Grid>
      );
    }
    if (pages === 'flight') {
      return (
        <Grid style={{ marginBottom: 20 }}>
          <Col style={styles.cardBox}>
            {!loadingOrder ? (
              dataOrder !== null ? (
                <View>
                  <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <Text style={styles.textBold}>
                      {dataBookingFlight.data.details[0].name}
                    </Text>
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <Text style={[styles.textBold, { fontSize: 18 }]}>
                        {/* {dataOrder[0].flight_data[0].flight_info.detail[0].departure_city_name ? dataOrder[0].flight_data[0].flight_info.detail[0].departure_city_name : null} */}
                        ({dataBookingFlight.data.details[0].departure_code})
                      </Text>
                      {this.props.returnFlight !== '' ? (
                        <Image
                          source={require('../../assets/icons/return.png')}
                          style={{
                            width: scale(15),
                            height: scale(15),
                            marginHorizontal: 7,
                            tintColor: thameColors.textBlack,
                          }}
                          resizeMode="center"
                        />
                      ) : (
                        <Icon
                          type="antdesign"
                          size={20}
                          name="arrowright"
                          color={thameColors.superBack}
                        />
                      )}
                      <Text style={[styles.textBold, { fontSize: 18 }]}>
                        {/* {dataOrder[0].flight_data[0].flight_info.detail[0].arrival_city_name ? dataOrder[0].flight_data[0].flight_info.detail[0].arrival_city_name : null} */}
                        (
                        {
                          dataBookingFlight.data.details[
                            dataBookingFlight.data.details.length - 1
                          ].arrival_code
                        }
                        )
                      </Text>
                    </View>
                    <Text
                      style={{
                        fontFamily: fontReguler,
                        color: thameColors.darkGray,
                      }}
                    >
                      {moment(dataBookingFlight.data.details[0].date).format(
                        'ddd, D MMM YYYY'
                      )}
                    </Text>
                  </View>
                  {/* <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this._navigateDetail(dataOrder[0])}>
                                                <Text style={{ fontFamily: fontBold, color: thameColors.primary }}>View Detail</Text>
                                            </TouchableOpacity>
                                        </View> */}
                </View>
              ) : (
                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                  <TouchableOpacity onPress={this._orderData}>
                    <Text
                      style={{
                        fontFamily: fontBold,
                        color: thameColors.primary,
                      }}
                    >
                      Try again
                    </Text>
                  </TouchableOpacity>
                </View>
              )
            ) : (
              <DotIndicator />
            )}
          </Col>
        </Grid>
      );
    }
    if (pages === 'orderFlight') {
      return (
        <Grid style={{ marginBottom: 20 }}>
          <Col style={styles.cardBox}>
            {!loadingOrder ? (
              dataOrder.length !== 0 ? (
                <View>
                  <View style={{ marginVertical: 10, alignItems: 'center' }}>
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <Text style={[styles.textBold, { fontSize: 18 }]}>
                        {
                          selectOrder.flight_data[0].flight_info.detail[0]
                            .departure_city_name
                        }
                        (
                        {
                          selectOrder.flight_data[0].flight_info.detail[0]
                            .departure_city
                        }
                        )
                      </Text>
                      {this.props.returnFlight !== '' ? (
                        <Image
                          source={require('../../assets/icons/return.png')}
                          style={{
                            width: scale(15),
                            height: scale(15),
                            marginHorizontal: 7,
                            tintColor: thameColors.textBlack,
                          }}
                          resizeMode="center"
                        />
                      ) : (
                        <Icon
                          type="antdesign"
                          size={20}
                          name="arrowright"
                          color={thameColors.superBack}
                        />
                      )}
                      <Text style={[styles.textBold, { fontSize: 18 }]}>
                        {
                          selectOrder.flight_data[0].flight_info.detail[0]
                            .arrival_city_name
                        }
                        (
                        {
                          selectOrder.flight_data[0].flight_info.detail[0]
                            .arrival_city
                        }
                        )
                      </Text>
                    </View>
                    <Text
                      style={{
                        fontFamily: fontReguler,
                        color: thameColors.darkGray,
                      }}
                    >
                      {moment(
                        selectOrder.flight_data[0].flight_info.departure_date
                      ).format('ddd, D MMM YYYY')}
                    </Text>
                  </View>
                  {/* <View style={{ alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this._navigateDetail(dataOrder[0])}>
                                                <Text style={{ fontFamily: fontBold, color: thameColors.primary }}>View Detail</Text>
                                            </TouchableOpacity>
                                        </View> */}
                </View>
              ) : (
                <View style={{ marginVertical: 10, alignItems: 'center' }}>
                  <TouchableOpacity onPress={this._orderData}>
                    <Text
                      style={{
                        fontFamily: fontBold,
                        color: thameColors.primary,
                      }}
                    >
                      Try again
                    </Text>
                  </TouchableOpacity>
                </View>
              )
            ) : (
              <DotIndicator />
            )}
          </Col>
        </Grid>
      );
    }
    return <View style={{ width: 0, height: 0 }} />;
  };

  _navigateDetail = data => {
    const { dispatch, navigation } = this.props;
    dispatch(actionSelectOrder('selectOrder', data));
    navigation.navigate('OrderDetailFlight');
  };

  totalPayment = pages => {
    const { bookHotelBeds } = this.props;
    if (pages === 'flight') {
      return this.props.dataBookingFlight.total;
    }
    if (pages === 'hotel') {
      return bookHotelBeds ? bookHotelBeds.booking.totalNet * 14000 : 1200000;
    }
    if (pages === 'orderFlight') {
      return this.props.selectOrder.total_amount;
    }
    return 999000;
  };

  orderId = () => {
    const { bookHotelBeds } = this.props;
    const { params } = this.props.navigation.state;
    if (params.pages === 'flight') {
      return this.props.dataBookingFlight.partner_trxid;
    }
    if (params.pages === 'orderFlight') {
      return this.props.selectOrder.partner_trx_id;
    }
    if (params.pages === 'hotel') {
      return bookHotelBeds
        ? bookHotelBeds.booking.invoiceCompany.registrationNumber
        : 'A912345678';
    }
  };

  // Main Render
  render() {
    const { params } = this.props.navigation.state;
    return (
      <View style={{ flex: 1 }}>
        {/* Alert */}
        {this._alertModal(
          1,
          this.state.errMessage !== ''
            ? this.state.errMessage
            : 'Payment failed, please try again later.'
        )}
        {/* //Alert */}

        <ScrollView style={{ backgroundColor: thameColors.backWhite }}>
          <HeaderPage
            title="Select Payment Method"
            callback={() => this.goBackHeader()}
          />
          <SubHeaderPage title="Order ID" label={this.orderId()} />
          <Grid style={{ marginTop: -25, marginBottom: 25 }}>
            <Col style={styles.cardBox}>
              <View>
                <Text style={styles.textBody}>
                  Please complete this payment in
                </Text>
              </View>
              <View style={styles.pt5}>
                <Text style={styles.textBlue}>
                  {/* 01h : 28m : 00s */}
                  <Timer
                    // initial={600000}
                    initial={1000 * 60 * 10}
                  />
                </Text>
              </View>
            </Col>
          </Grid>
          {/* <Grid style={{ marginBottom: 20 }}>
                        <Col style={styles.cardBox}>
                            <View style={{marginTop: 10, marginBottom: 10}}>
                                <Text style={styles.textBold}>Check Your Shipment Again?</Text>
                            </View>
                        </Col>
                    </Grid> */}
          {this.renderType(params.pages)}

          <Grid style={{ marginBottom: 20 }}>
            <Col style={styles.cardBox}>
              <View>
                <Text style={styles.textBody}>Price You Pay</Text>
              </View>
              <View style={styles.pt5}>
                <Text style={styles.textPrice}>
                  Rp
                  {numeral(this.totalPayment(params.pages))
                    .format('0,0')
                    .replace(/,/g, '.')}
                </Text>
              </View>
            </Col>
          </Grid>

          <Grid>
            <TouchableOpacity
              onPress={() => this.doPayment('credit_card')}
              style={{ flex: 1 }}
            >
              <Col style={styles.cardBox}>
                <View style={{ marginTop: 10 }}>
                  <Text style={styles.textBold}>Credit Card</Text>
                </View>
                <View>
                  <Image
                    style={{ width: deviceWidth / 2.5, height: 60 }}
                    resizeMode="center"
                    source={require('../../assets/icons/icon_creditcard.png')}
                  />
                </View>
              </Col>
            </TouchableOpacity>
          </Grid>

          {/* <Grid style={{ marginTop: 20, marginBottom: 20 }}>
            <Text
              style={{
                fontFamily: fontBold,
                color: thameColors.superBack,
                fontSize: 18,
                marginLeft: 20,
              }}
            >
              Virtual Account
            </Text>
            {dataVirtual.map((item, index) => {
              return (
                <Row key={index}>
                  <TouchableOpacity
                    onPress={() => this.doPayment(item.id)}
                    style={[
                      styles.cardBox,
                      { flex: 1, marginTop: 10, flexDirection: 'row' },
                    ]}
                  >
                    <Col size={8} style={{ flex: 0 }}>
                      <Image
                        style={{ width: 75, height: 25 }}
                        resizeMode="center"
                        source={item.image}
                      />
                    </Col>
                    <Col size={2} style={{ alignItems: 'flex-end' }}>
                      <Icon
                        type="material-community"
                        name="circle"
                        size={30}
                        color={thameColors.halfWhite}
                      />
                    </Col>
                  </TouchableOpacity>
                </Row>
              );
            })}
          </Grid> */}

          {/* ========================== Retail Store ========================== */}
          {/* <Grid style={{ marginTop: 10, marginBottom: 20 }}>
                        <Text style={{ fontFamily: fontBold, color: '#000', fontSize: 22, marginLeft: 20 }}>Retail Store</Text>
                        {dataRetail.map((item, index) => {
                            return (
                                <Row key={index}>
                                    <TouchableOpacity style={[styles.cardBox, { flex: 1, marginTop: 10, flexDirection: 'row' }]}>
                                        <Col size={3} style={{ flex: 0 }}>
                                            <Image style={{ width: deviceWidth / 3.7, height: 50 }} resizeMode="center" source={item.image} />
                                        </Col>
                                        <Col size={5} style={{ alignItems: 'flex-end' }}>
                                            <Text style={{ fontFamily: fontReguler, color: '#000' }}>{item.name}</Text>
                                        </Col>
                                        <Col size={2}>
                                            <Icon type="material-community" name="circle" size={30} color="#e6e6e6" />
                                        </Col>
                                    </TouchableOpacity>
                                </Row>
                            )
                        })}
                    </Grid> */}
          {/* ========================== Retail Store ========================== */}

          {/* {
                        (this.state.paymentFetching == false) ?
                        <View style={{ backgroundColor: '#FFFFFF', margin: 20, padding: 10, marginBottom: 0, alignItems: "center", justifyContent: "center" }}>
                            <DotIndicator size={10} style={{ flex: 1}} color="#008195"/>
                            <Text style={{ color: "#000", fontWeight: "500" }}>Please wait, we still process your request</Text>
                        </View> :
                        <Text></Text>
                    } */}

          {/* <Grid>
                        <Col style={{marginLeft: 25, marginTop: 15, marginBottom: 15}}>
                            <Text style={styles.textBold}>Virtual Account</Text>
                        </Col>
                    </Grid>

                    <Grid style={styles.cardBox}>
                        <Row style={{marginTop: 10, marginBottom: 10}}>
                            <Col size={3} style={{marginLeft: 5}}>
                                <Image style={{width: deviceWidth/4.5, height: 22}} resizeMode="center" source={require('../../assets/icons/icon_finpay.png')} />
                            </Col>
                            <Col size={5} style={{alignItems: 'flex-end', justifyContent:'center'}}>
                                <Text style={styles.textBank}>Finpay Virtual Acc</Text>
                            </Col>
                            <Col size={1}>
                            {
                                (this.state.paymentFetching) ?
                                <View style={{ backgroundColor: '#FFFFFF', margin: 20, padding: 10, marginBottom: 0, alignItems: "center", justifyContent: "center" }}>
                                    <DotIndicator size={10} style={{ flex: 1}} color="#008195"/>
                                    <Text style={{ color: "#000", fontWeight: "500" }}>Please wait, we still process your request</Text>
                                </View> :
                                <Text></Text>
                            }
                            </Col>
                            <Col size={1} style={{justifyContent:'flex-end', alignItems: 'flex-end'}}>
                                <CheckBox
                                    containerStyle={{ backgroundColor: "#ffffff", padding: 0, margin: 0, marginLeft: 0, borderWidth: 0 }}
                                    checked={this.state.choicePayment}
                                    // onPress={() => this.doPayment()}
                                    onPress={() => this.props.navigation.navigate('PaymentTransfer')}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle'
                                    checkedColor='#828282'
                                    uncheckedColor='#e6e6e6'
                                />
                            </Col>
                        </Row>
                    </Grid> */}
        </ScrollView>

        {/* ========= MODAL LOADING ========= */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 99}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={250}
          animationOutTiming={250}
          backdropTransitionInTiming={250}
          backdropTransitionOutTiming={250}
        >
          {this.loadingBooking()}
        </Modal>
        {/* ========= MODAL LOADING ========= */}

        {/* <ModalHotel
                    modalVisible={
                        this.state.visibleModal === 1 ||
                        this.state.visibleModal === 2
                    }
                    resetModal={()=> this.resetModal()}
                    hotelName={this.props.dataHotelBook.hotel_name}
                    checkIn={this.props.dataHotelBook.startdate}
                    checkOut={this.props.dataHotelBook.enddate}
                    roomName={this.props.dataHotelBook.room_name}
                    guest={this.props.dataHotelBook.adult}
                    priceRoom={this.props.dataHotelBook.amount}
                    amount={this.props.dataHotelBook.amount}
                /> */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataHotelBook: state.hotel.bookHotelBeds,
    dataBookingFlight: state.flight.dataBookingFlight,
    returnFlight: state.flight.returnDate,
    dataBookingTrain: state.train.bookTrain,
    dataBookingPPOB: state.ppob.dataPulsa,
    dataBookingCargo: state.cargo,
    hotelBook: state.hotel.chooseHotelBooking,
    contact: state.flight.contactDetail,
    loadingOrder: state.ordersHistory.fetching,
    dataOrder: state.ordersHistory.data,
    isLogin: state.profile.isLogin,
    token: state.profile.token,
    messageOrder: state.ordersHistory.message,
    selectOrder: state.ordersHistory.selectOrder,
    bookHotelBeds: state.hotel.bookHotelBeds,
  };
}

export default connect(mapStateToProps)(PaymentOrder);
