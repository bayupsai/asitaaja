import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity as Touch,
  WebView,
} from 'react-native';
import {
  thameColors,
  fontBold,
  fontReguler,
  fontSemiBold,
} from '../../../base/constant';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import moment from 'moment';
import numeral from 'numeral';
import { Icon } from 'react-native-elements';
import { ButtonRounded } from '../../../elements/Button';

class VirtualAccount extends PureComponent {
  _goBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    alert(this.props.navigation.getParam('redirectURL'));
    return (
      <View style={{ flex: 1 }}>
        <WebView
          source={{
            uri:
              'https://mytelkomsel.finnet-indonesia.com/topup/howto.php?invoice=NCCFKT191001&amount=1709100&timeout=90&payment_code=8719000000136156&sof_id=vapermata',
          }}
        />
        {/* <HeaderPage title="Virtual Account" callback={this._goBack} />
                <ScrollView style={{ backgroundColor: thameColors.backWhite }}>
                    <SubHeaderPage />
                    <View style={styles.content}>
                        <View style={[styles.card, { alignItems: 'center' }]}>
                            <Text style={styles.textHeader}>Make a Payment Before</Text>
                            <Text style={styles.textNote}>Complete your payment before</Text>
                            <View style={styles.rowDirection}>
                                <Text style={[styles.textNote, { color: thameColors.primary, fontFamily: fontSemiBold }]}>{moment(new Date()).format("ddd DD, YYYY")}</Text>
                                <Text style={styles.textNote}> at </Text>
                                <Text style={[styles.textNote, { color: thameColors.primary, fontFamily: fontSemiBold }]}>03:54:12</Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={[styles.textHeader, { fontSize: 16 }]}>Please Transfer to</Text>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.rowDirection}>
                                <View>
                                    <Text style={styles.textNote}>Virtual Account</Text>
                                    <Text style={styles.textBold}>0009990000</Text>
                                    <Image source={require('../../../assets/logos/permata.png')} style={{ width: 100, height: 50 }} resizeMode="contain" />
                                </View>
                                <Touch>
                                    <Text style={[styles.textNote, { color: thameColors.primary }]}>Copy</Text>
                                </Touch>
                            </View>
                            <View style={styles.hr} />
                            <View style={styles.rowDirection}>
                                <View>
                                    <Text style={styles.textNote}>Total Amount</Text>
                                    <Text style={styles.textBold}>Rp. {numeral(100000).format('0,0').replace(/,/g, '.')}</Text>
                                </View>
                                <Touch>
                                    <Text style={[styles.textNote, { color: thameColors.primary }]}>Copy</Text>
                                </Touch>
                            </View>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={[styles.textHeader, { fontSize: 16 }]}>Payment Guide</Text>
                        </View>
                        <View style={styles.card}>
                            <Touch style={[styles.rowDirection]}>
                                <View>
                                    <Text style={styles.textBold}>ATM Permata Bank</Text>
                                </View>
                                <Touch>
                                    <Icon type="feather" name="chevron-down" color={thameColors.primary} />
                                </Touch>
                            </Touch>
                            <View style={styles.hr} />
                            <Touch style={[styles.rowDirection, { marginTop: 5 }]}>
                                <View>
                                    <Text style={styles.textBold}>Internet Banking</Text>
                                </View>
                                <Touch>
                                    <Icon type="feather" name="chevron-down" color={thameColors.primary} />
                                </Touch>
                            </Touch>
                            <View style={styles.hr} />
                            <Touch style={[styles.rowDirection, { marginTop: 5 }]}>
                                <View>
                                    <Text style={styles.textBold}>Mobile Banking</Text>
                                </View>
                                <Touch>
                                    <Icon type="feather" name="chevron-down" color={thameColors.primary} />
                                </Touch>
                            </Touch>
                            <View style={styles.hr} />
                            <Touch style={[styles.rowDirection, { marginTop: 5 }]}>
                                <View>
                                    <Text style={styles.textBold}>SMS Banking</Text>
                                </View>
                                <Touch>
                                    <Icon type="feather" name="chevron-down" color={thameColors.primary} />
                                </Touch>
                            </Touch>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={[styles.textHeader, { fontSize: 16 }]}>Complete your payment?</Text>
                        </View>
                        <View style={[styles.card, { alignItems: 'center' }]}>
                            <Text style={[styles.textNote, { color: thameColors.textBlack, textAlign: 'center' }]}>Once your payment is confirmed. We will send your flight E-ticket to your email address.</Text>
                            <View style={styles.buttonCheck}>
                                <ButtonRounded onClick={() => alert('Clicked')} label="CHECK STATUS PAYMENT" size="small" />
                            </View>
                        </View>
                    </View>
                </ScrollView> */}
      </View>
    );
  }
}

export default VirtualAccount;

const styles = StyleSheet.create({
  card: {
    width: '100%',
    padding: 15,
    backgroundColor: thameColors.white,
    justifyContent: 'center',
    borderRadius: 10,
    marginTop: 5,
  },
  content: {
    margin: 20,
    marginTop: -50,
  },
  rowDirection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textHeader: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 18,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.superBack,
  },
  textNote: {
    fontFamily: fontReguler,
  },
  hr: {
    width: '100%',
    height: 1,
    backgroundColor: thameColors.gray,
  },
  buttonCheck: {
    marginTop: 15,
  },
});
