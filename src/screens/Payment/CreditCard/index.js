/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { BackHandler, View, Text } from 'react-native';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';
import { paymentStatus } from '../../../redux/actions/globalAction';
import { actionCheckPayMidtrans } from '../../../redux/actions/PaymentAction';
import { getPacisPayment } from '../../../redux/actions/CargoAction';
import AlertModal from '../../../components/AlertModal';
import { thameColors, fontExtraBold } from '../../../base/constant';
import HeaderPage from '../../../components/HeaderPage';

class CreditCard extends React.Component {
  constructor(props) {
    super(props);

    const { navigation } = this.props;
    const redirectURL = navigation.getParam('redirectURL');
    const pageType = navigation.getParam('pageType');

    this.state = {
      urlPayment: redirectURL,
      pageType,
      visible: null,
      time: 0,
    };
  }

  // modal
  _toggleModal = modal => {
    this.setState({ visible: modal });
  };

  _alertModal = (type, idModal, title, message, onPress, onDismiss) => {
    return (
      <AlertModal
        type={type}
        isVisible={this.state.visible === idModal}
        title={title}
        contentText={message}
        onPress={onPress}
        onDismiss={onDismiss}
      />
    );
  };
  // modal

  goBackHeader = () => {
    this.setState({ visible: 1 }); // 'Please do not exit from this page until your payment process is completed.',
  };

  gotoMyOrders = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Tab',
          action: NavigationActions.navigate({
            routeName: 'Order',
          }),
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  };

  checkPacisPaymentStatus = () => {
    const invoiceNumber = this.props.dataBookingCargo.bookingCode;
    this.props.dispatch(getPacisPayment(invoiceNumber)).then(res => {
      if (res.data.Status === 'PAID') {
        clearInterval(this.interval);
        this.setState({ visible: 2 }); // 'Payment Success',
      }
    });
  };

  checkPaymentStatus = () => {
    let invoiceNo = this.props.dataBookingFlight.partner_trxid;
    let type = 'flight';
    if (this.state.pageType === 'train') {
      invoiceNo = { invoiceNumber: this.props.dataBookingTrain.partner_trxid };
      type = 'train';
    } else if (this.state.pageType === 'ppob') {
      invoiceNo = { invoiceNumber: this.props.dataBookingPulsa.partner_trxid };
      type = 'ppob';
    } else if (this.state.pageType === 'hotel') {
      invoiceNo = {
        invoiceNumber: this.props.paymentHotelPayload.invoiceNumber,
      };
      type = 'hotel';
    }
    this.props.dispatch(actionCheckPayMidtrans());
    // this.props.dispatch(paymentStatus(invoiceNo, type)).then(res => {
    //   if (res.data.data.Status === 'PAID') {
    //     clearInterval(this.interval);
    //     if (this.state.pageType === 'flight') {
    //       // this.props.navigation.navigate("ETicket", { dataBooking: this.props.dataBookingFlight })
    //       this.gotoMyOrders();
    //     } else if (this.state.pageType === 'train') {
    //       // this.props.navigation.navigate("ETicketRevamp", { dataBookingTrain: this.props.dataBookingTrain })
    //       this.gotoMyOrders();
    //     } else if (this.state.pageType === 'ppob') {
    //       // this.props.navigation.navigate("BookingPulsaSuccess")
    //       this.gotoMyOrders();
    //     } else if (this.state.pageType === 'hotel') {
    //       // this.props.navigation.navigate("Booking Hotel Success")
    //       this.gotoMyOrders();
    //     } else {
    //       this.setState({ visible: 3 }); // 'Payment Success',
    //     }
    //   } else if (res.data.data.Status === 'DECLINED') {
    //     clearInterval(this.interval);
    //     this.setState({ visible: 4 }); // 'Payment Failed',
    //   }
    // });
  };

  componentDidMount() {
    // this.backHandler();
    // this.interval();
    // this.timeOut();
    this.setInterval = setInterval(() => {
      if (this.state.pageType === 'pacis') {
        this.checkPacisPaymentStatus();
      } else {
        this.checkPaymentStatus();
      }
    }, 5000);

    this.setTimeOut = setTimeout(() => {
      this.setState({ visible: 5 });
    }, 1000 * 60 * 4);

    this.backAja = BackHandler.addEventListener('hardwareBackPress', () => {
      this.goBackHeader();
      return true;
    });
  }

  goHome = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Tab',
          action: NavigationActions.navigate({
            routeName: 'Home',
          }),
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  };

  componentWillUnmount() {
    clearInterval(this.setInterval);
    clearTimeout(this.setTimeOut);

    // handle pressed in back button
    this.backAja.remove();
    // this.removeBackHandler();
  }

  // Function Life Cycle
  backHandler = () => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.goBackHeader();
      return true;
    });
  };
  removeBackHandler = () => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.goBackHeader();
      return true;
    }).remove();
  };

  render() {
    const { visible } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0 }}>
          <HeaderPage
            title="Payment"
            callback={this.goBackHeader}
            disableRight
          />
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              padding: 10,
            }}
          >
            <Text style={{ fontFamily: fontExtraBold, color: thameColors.red }}>
              *Please do not back from this page until your payment process is
              completed. This payment process take, make sure to enter data
              correctly.
            </Text>
          </View>
        </View>
        {/* <WebView source={{ uri: this.state.urlPayment }} style={{ flex: 1 }} /> */}
        <WebView source={{ uri: this.state.urlPayment }} style={{ flex: 1 }} />

        {/* Modal Alert */}
        <AlertModal
          type="qna"
          isVisible={visible === 1}
          title="Payment"
          contentText="Please do not exit from this page until your payment process is completed."
          labelCancel="Check again"
          labelOk="Payment completed"
          onPress={this.goHome}
          onDismiss={() => this._toggleModal(null)}
        />
        <AlertModal
          type="normal"
          isVisible={visible === 2}
          title="Payment Success"
          contentText="Your transaction has been processed successfully. Go to My Orders menu to check your package status"
          onPress={this.gotoMyOrders}
          onDismiss={this.gotoMyOrders}
        />
        <AlertModal
          type="normal"
          isVisible={visible === 3}
          title="Payment Success"
          contentText="Your transaction has been processed successfully, please check your email to get your ticket."
          onPress={this.goHome}
          onDismiss={this.goHome}
        />
        <AlertModal
          type="normal"
          isVisible={visible === 4}
          title="Payment Failed"
          contentText="Please enter correct credit card number"
          onPress={() => this.props.navigation.goBack()}
        />
        <AlertModal
          type="normal"
          isVisible={visible === 5}
          title="Your Payment time is expired"
          contentText="Please try again later"
          onPress={this.goHome}
          onDismiss={this.goHome}
        />
        {/* Modal Alert */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataBookingFlight: state.flight.dataBookingFlight,
    dataBookingTrain: state.train.bookTrain,
    dataBookingPulsa: state.ppob.dataPulsa,
    dataBookingCargo: state.cargo.pacisBooking,
    paymentHotelPayload: state.hotel.paymentHotelPayload,
  };
}
export default connect(mapStateToProps)(CreditCard);
