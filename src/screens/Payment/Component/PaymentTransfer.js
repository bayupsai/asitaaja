import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  ToastAndroid,
  Clipboard,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import numeral from 'numeral';

//Component
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import {
  fontBold,
  fontReguler,
  thameColors,
} from '../../../base/constant/index';
import { ButtonRounded } from '../../../elements/Button';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ModalPaymentTransfer from './PaymentMethod/ModalPaymentTransfer';
import { connect } from 'react-redux';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class PaymentTransfer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modal: null,
      modalVisible: false,
      text: '',
    };
  }
  // MODAL
  showModal = () => {
    this.setState({ modal: null });
  };
  //Modal

  //tooltip
  _toolTip = () => {
    return (
      <Text style={{ fontFamily: fontBold, marginLeft: 20 }}>
        IMPORTANT! Please transfer until the last 3 digits
      </Text>
    );
  };
  _copyText = async text => {
    await Clipboard.setString(text);
    await this._toastAlert();
  };
  _toastAlert = () => {
    ToastAndroid.showWithGravityAndOffset(
      'Copied to Clipboard',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
      0,
      25
    );
  };
  //tooltip

  goBackHeader = () => {
    let { navigation } = this.props;
    navigation.goBack();
  };

  render() {
    let { total_amount } = this.props.selectOrder;
    let price = numeral(total_amount)
      .format('0,0')
      .replace(/,/g, '.');
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
          <HeaderPage
            title="Tranfer to BCA"
            callback={() => this.goBackHeader()}
          />
          <SubHeaderPage />
          <Grid style={[styles.margin15, { marginTop: -50 }]}>
            <Col style={styles.cardBox}>
              <Row style={{ marginTop: 10 }}>
                <Text style={styles.textBold}>Make a Payment Before</Text>
              </Row>
              <Row style={[styles.margin15, { marginTop: 5 }]}>
                <Text style={styles.textBody}>
                  Complete your payment within 17:28 PM
                </Text>
              </Row>
            </Col>
          </Grid>
          <Grid>
            <Col style={{ marginLeft: 20, marginBottom: 5 }}>
              <Text style={styles.textBold}>Please Transfer to</Text>
            </Col>
          </Grid>
          <Grid style={{ marginBottom: 5, marginTop: 5 }}>
            <Col style={[styles.cardBoxTransfer, { paddingLeft: 25 }]}>
              <Row>
                <Text style={styles.textBody}>Account Number</Text>
              </Row>
              <Row style={[styles.margin15, { marginRight: 20 }]}>
                <Col>
                  <Text style={styles.textBold}>084 122 3334</Text>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <TouchableOpacity
                    onPress={() => this._copyText('084 122 334')}
                  >
                    <Text style={styles.textBlue}>Copy</Text>
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row style={styles.sectionHr}></Row>
              <Row style={{ marginTop: 15 }}>
                <Text style={styles.textBody}>Account Holder Name</Text>
              </Row>
              <Row>
                <Text style={styles.textBold}>PT. Solusi Awan Indonesia</Text>
              </Row>
              <Row style={{ marginBottom: 10 }}>
                <Image
                  source={require('../../../assets/logos/logo-bca.png')}
                  style={{ height: 35, width: 55, resizeMode: 'center' }}
                />
              </Row>
              <Row style={styles.sectionHr}></Row>
              <Row style={{ marginTop: 15 }}>
                <Text style={styles.textBody}>Total Amount</Text>
              </Row>
              <Row>
                <Col style={{ flexDirection: 'row' }}>
                  <Text style={styles.textBold}>Rp {price.slice(0, -3)}</Text>
                  <View>
                    <Text
                      style={[
                        styles.textBold,
                        { color: thameColors.secondary },
                      ]}
                    >
                      {price.slice(-3)}
                    </Text>
                    <View style={styles.triangleShape} />
                  </View>
                </Col>
                <Col
                  style={{
                    marginBottom: 15,
                    marginRight: 20,
                    alignItems: 'flex-end',
                  }}
                >
                  <TouchableOpacity onPress={() => this._copyText('122')}>
                    <Text style={styles.textBlue}>Copy</Text>
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row style={{ backgroundColor: 'red' }}>
                <Col
                  style={{
                    backgroundColor: thameColors.yellow,
                    borderBottomStartRadius: 5,
                    borderBottomEndRadius: 5,
                    marginRight: -10,
                    marginLeft: -25,
                    marginBottom: -10,
                    padding: 5,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: fontBold,
                      marginLeft: 20,
                      color: thameColors.textBlack,
                    }}
                  >
                    IMPORTANT! Please transfer until the last 3 digits
                  </Text>
                </Col>
              </Row>
            </Col>
          </Grid>
          <Grid>
            <Col style={{ marginLeft: 20, marginBottom: 5, marginTop: 10 }}>
              <Text style={styles.textBold}>Completed Your Payment?</Text>
            </Col>
          </Grid>
          <Grid style={styles.margin15}>
            <Col style={styles.cardBox}>
              <Row style={[styles.margin3, { marginTop: 10 }]}>
                <Text style={styles.textBank}>
                  Once your payment is confirmed.
                </Text>
              </Row>
              <Row style={styles.margin3}>
                <Text style={styles.textBank}>We will send an sms to your</Text>
              </Row>
              <Row style={styles.margin3}>
                <Text style={styles.textBank}>mobile number</Text>
              </Row>
              <Row style={{ marginBottom: 15, marginTop: 15 }}>
                <ButtonRounded
                  label="I HAVE COMPLETED PAYMENT"
                  size="small"
                  onClick={() => this.setState({ modalVisible: true })}
                />
              </Row>
            </Col>
          </Grid>
        </ScrollView>
        <ModalPaymentTransfer
          {...this.props}
          visible={this.state.modalVisible}
          closeModal={() => this.setState({ modalVisible: false })}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectOrder: state.ordersHistory.selectOrder,
  };
}

export default connect(mapStateToProps)(PaymentTransfer);

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
  },
  cardBox: {
    backgroundColor: '#fff',
    padding: 10,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardBoxTransfer: {
    backgroundColor: '#fff',
    padding: 10,
    marginRight: 20,
    marginLeft: 20,
    borderRadius: 5,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  textBold: { fontSize: 16, color: '#222222', fontFamily: fontBold },
  textBody: { fontFamily: fontReguler, color: '#c1c1c1' },
  textBank: { fontFamily: fontReguler, color: '#222222' },
  textBlue: { fontSize: 16, fontFamily: fontReguler, color: '#0066b3' },
  sectionHr: {
    width: 100 + '%',
    height: 0.5,
    backgroundColor: '#c1c1c1',
    alignSelf: 'center',
    justifyContent: 'flex-end',
  },
  margin3: { margin: 3 },
  margin15: { marginBottom: 15 },
  triangleShape: {
    width: 0,
    height: 0,
    borderLeftWidth: 30,
    borderRightWidth: 30,
    borderBottomWidth: 30,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: thameColors.yellow,
    position: 'absolute',
    marginTop: 20,
    marginLeft: -10,
  },
});
