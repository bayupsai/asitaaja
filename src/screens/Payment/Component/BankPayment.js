import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  View,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { fontExtraBold, fontReguler } from '../../../base/constant';

const BankPayment = props => {
  return (
    <ScrollView>
      <Grid style={styles.container}>
        <Col style={styles.cardBoxBody}>
          <Grid style={{ marginBottom: 0 }}>
            <Col
              style={{
                paddingTop: 10,
                paddingBottom: 10,
                borderBottomColor: '#EEE',
                borderBottomWidth: 0.5,
              }}
            >
              <View>
                <Text style={styles.textHeader}>Credit Card</Text>
              </View>
              {/* <View style={{ paddingTop: 5 }}>
                                <Text style={styles.textBody}>You can pay by transfer via ATM, Internet Banking or Mobile Banking</Text>
                            </View> */}
            </Col>
          </Grid>
          <Grid style={{ paddingTop: 10, paddingBottom: 0 }}>
            <Col size={7} style={{ justifyContent: 'center' }}>
              <TouchableOpacity
                onPress={() => props.handlePayment()}
                style={{ flex: 0 }}
              >
                <Text style={styles.textBody}>Credit Card</Text>
              </TouchableOpacity>
            </Col>
            <Col
              size={3}
              style={{ alignItems: 'flex-end', justifyContent: 'center' }}
            >
              <TouchableOpacity
                onPress={() => props.handlePayment()}
                style={{ flex: 0 }}
              >
                <Image
                  style={{ width: 100, height: 30 }}
                  resizeMode="contain"
                  source={require('../../../assets/payment/cc/cc.png')}
                />
              </TouchableOpacity>
            </Col>
          </Grid>
        </Col>
      </Grid>
    </ScrollView>
  );
};

export default BankPayment;

const styles = StyleSheet.create({
  container: { marginLeft: 20, marginRight: 20 },
  cardBoxBody: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingBottom: 20,
  },
  textHeader: {
    fontSize: 16,
    fontFamily: fontExtraBold,
    color: '#424242',
  },
  textBody: {
    fontSize: 16,
    color: '#424242',
    fontFamily: fontReguler,
  },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    backgroundColor: '#BDBDBD',
    alignSelf: 'center',
  },
});
