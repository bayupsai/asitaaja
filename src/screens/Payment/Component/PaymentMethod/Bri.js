import React, { PureComponent } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  Dimensions,
  Image,
  View,
  LayoutAnimation,
  UIManager,
  Platform,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';

//Component
import HeaderPage from '../../../../components/HeaderPage';
import SubHeaderPage from '../../../../components/SubHeaderPage';
import TimerPayment from '../../Component/TimerPayment';
import { Button } from '../../../../elements/Button';
import { fontExtraBold, fontReguler } from '../../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class PaymentMethod extends PureComponent {
  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.state = {
      textLayoutHeight: 0,
      textLayoutHeightTwo: 0,
      updatedHeight: 0,
      updatedHeightTwo: 0,
      updatedHeightThree: 0,
      updatedHeightFour: 0,
      expand: false,
      expandTwo: false,
      expandThree: false,
      expandFour: false,
      buttonText: 'Virtual Account',
      buttonTextTwo: 'Credit Card',
      buttonTextThree: 'Transfer via Internet',
      buttonTextFour: 'Transfer via SMS',
    };
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  getHeight(height) {
    this.setState({ textLayoutHeight: height });
  }

  getHeightCC(height) {
    this.setState({ textLayoutHeightTwo: height });
  }

  getHeightInternet(height) {
    this.setState({ textLayoutHeight: height });
  }

  getHeightSms(height) {
    this.setState({ textLayoutHeight: height });
  }
  //Collapse Tranfer Via ATM
  expand_collapse_Function = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expand == false) {
      this.setState({
        updatedHeight: this.state.textLayoutHeight,
        expand: true,
        buttonText: <Text>Virtual Account</Text>,
      });
    } else {
      this.setState({
        updatedHeight: 0,
        expand: false,
        buttonText: 'Virtual Account',
      });
    }
  };

  //Collapse Tranfer Via CC
  expand_collapse_FunctionCC = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expandTwo == false) {
      this.setState({
        updatedHeightTwo: this.state.textLayoutHeightTwo,
        expandTwo: true,
        buttonTextTwo: <Text>Credit Card</Text>,
      });
    } else {
      this.setState({
        updatedHeightTwo: 0,
        expandTwo: false,
        buttonTextTwo: 'Credit Card',
      });
    }
  };

  // //Internet
  // expand_collapse_FunctionInternet = () => {
  //     LayoutAnimation.configureNext( LayoutAnimation.Presets.easeInEaseOut );
  //     if( this.state.expandThree == false )
  //     {
  //         this.setState({
  //             updatedHeightThree: this.state.textLayoutHeight,
  //             expandThree: true,
  //             buttonTextThree: <Text>Internet</Text>
  //         });
  //     }
  //     else
  //     {
  //         this.setState({
  //             updatedHeightThree: 0,
  //             expandThree: false,
  //             buttonTextThree: 'Transfer via Internet'
  //         });
  //     }
  // }

  // //SMS
  // expand_collapse_FunctionInternet = () => {
  //     LayoutAnimation.configureNext( LayoutAnimation.Presets.easeInEaseOut );
  //     if( this.state.expandFour == false )
  //     {
  //         this.setState({
  //             updatedHeightFour: this.state.textLayoutHeight,
  //             expandFour: true,
  //             buttonTextFour: <Text>SMS</Text>
  //         });
  //     }
  //     else
  //     {
  //         this.setState({
  //             updatedHeightFour: this.state.textLayoutHeight,
  //             expandFour: true,
  //             buttonTextFour: <Text>SMS</Text>
  //         });
  //     }
  // }

  render() {
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
          <HeaderPage title="Flight" callback={() => this.goBackHeader()} />
          <SubHeaderPage title="BRI Virtual Account" />

          <Grid>
            <Row style={styles.content}>
              <TimerPayment />
            </Row>
          </Grid>

          <Grid>
            <Col style={styles.paddingRow}>
              <Row style={{ paddingBottom: 2 }}>
                <Text style={styles.textHeader}> Please transfer to </Text>
              </Row>
              <Col style={styles.cardBoxBank}>
                <Row style={{ paddingTop: 5 }}>
                  <Text style={styles.textBody}> Virtual Account </Text>
                </Row>
                <Row>
                  <Col size={7}>
                    <Text style={styles.textHeader}> BRI </Text>
                  </Col>
                  <Col size={3}>
                    <Image
                      source={require('../../../../assets/logos/bri-logo2.png')}
                    />
                  </Col>
                </Row>
                <Row style={styles.sectionHr}></Row>
                <Row>
                  <Text style={styles.textBody}> Virtual Account Number </Text>
                </Row>
                <Row>
                  <Col size={7}>
                    <Text style={styles.textHeader}> 8319 0010 0074 6627 </Text>
                  </Col>
                  <Col size={3}>
                    <TouchableOpacity onPress={() => alert('Salin')}>
                      <Text style={styles.textPrice}>SALIN</Text>
                    </TouchableOpacity>
                  </Col>
                </Row>
                <Row style={styles.sectionHr}></Row>
                <Row style={{ paddingBottom: 5 }}>
                  <Col size={7}>
                    <Row>
                      <Text style={styles.textBody}> Total payment </Text>
                    </Row>
                  </Col>
                  <Col size={3}>
                    <Row>
                      <Text style={styles.textPrice}> Rp.2.300.000 </Text>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Col>
          </Grid>

          <Grid>
            <Col style={styles.paddingRow}>
              <Row style={{ paddingBottom: 2, paddingTop: 15 }}>
                <Text style={styles.textHeader}>Payment method</Text>
              </Row>
              {/* <Col style={styles.cardBoxPayment}> */}
              {
                <View style={styles.MainContainer}>
                  <View style={styles.ChildView}>
                    <TouchableOpacity
                      onPress={this.expand_collapse_Function}
                      activeOpacity={0.7}
                      style={styles.TouchableOpacityStyle}
                    >
                      <Text style={styles.TouchableOpacityTitleText}>
                        {this.state.buttonText}
                      </Text>
                    </TouchableOpacity>

                    <View
                      style={{
                        height: this.state.updatedHeight,
                        overflow: 'hidden',
                      }}
                    >
                      <View style={{ paddingLeft: 3, paddingRight: 3 }}>
                        <Text
                          style={styles.textBody}
                          onLayout={value =>
                            this.getHeight(value.nativeEvent.layout.height)
                          }
                        >
                          Masukan{' '}
                          <Text style={styles.textBodyBold}>Nomor Order</Text>{' '}
                          sebagai{' '}
                          <Text style={styles.textBodyBold}>Nama Singkat</Text>
                          masukan{' '}
                          <Text style={styles.textBodyBold}>
                            Nomor Rekening
                          </Text>{' '}
                          misal,{' '}
                          <Text style={styles.textBodyBold}>
                            8319 0010 0074 6627
                          </Text>
                          Lengkapi semua data yang di perlukan. Klik{' '}
                          <Text style={styles.textBodyBold}>Lanjutkan</Text>
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View style={styles.ChildView}>
                    <TouchableOpacity
                      onPress={this.expand_collapse_FunctionCC}
                      activeOpacity={0.7}
                      style={styles.TouchableOpacityStyle}
                    >
                      <Text style={styles.TouchableOpacityTitleText}>
                        {this.state.buttonTextTwo}
                      </Text>
                    </TouchableOpacity>

                    <View
                      style={{
                        height: this.state.updatedHeightTwo,
                        overflow: 'hidden',
                      }}
                    >
                      <View style={{ paddingLeft: 3, paddingRight: 3 }}>
                        <Text
                          style={styles.textBody}
                          onLayout={value =>
                            this.getHeightCC(value.nativeEvent.layout.height)
                          }
                        >
                          Masukan{' '}
                          <Text style={styles.textBodyBold}>Nomor Order</Text>{' '}
                          sebagai{' '}
                          <Text style={styles.textBodyBold}>Nama Singkat</Text>
                          masukan{' '}
                          <Text style={styles.textBodyBold}>
                            Nomor Rekening
                          </Text>{' '}
                          misal,{' '}
                          <Text style={styles.textBodyBold}>
                            8319 0010 0074 6627
                          </Text>
                          Lengkapi semua data yang di perlukan. Klik{' '}
                          <Text style={styles.textBodyBold}>Lanjutkan</Text>
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              }
              {/* </Col> */}
            </Col>
          </Grid>

          <Grid style={{ paddingBottom: 15, paddingTop: 15 }}>
            <Row style={{ paddingBottom: 8 }}>
              <Button
                label="I Have Paid"
                onClick={() => alert('Saya Sudah Membayar')}
              />
            </Row>
            <Row>
              <Button
                colorButton="buttonColor"
                label="Change Payment Method"
                onClick={() => alert('Login')}
              />
            </Row>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

export default PaymentMethod;

const styles = StyleSheet.create({
  content: {
    paddingBottom: -10,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    transform: [{ translateY: -25 }],
  },
  cardBoxTimer: {
    width: deviceWidth / 1.11,
    backgroundColor: '#fff',
    height: 70,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardBoxBank: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 140,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  cardBoxPayment: {
    backgroundColor: '#fff',
    width: deviceWidth,
    height: deviceHeight / 1.1,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
  },
  paddingRow: {
    paddingTop: 15,
    paddingLeft: 20,
  },
  paddingRowPayment: {
    paddingBottom: 10,
    paddingTop: 10,
  },
  textHeader: {
    fontSize: 14,
    fontFamily: fontExtraBold,
    color: '#424242',
    paddingBottom: 2,
  },
  textBody: {
    fontSize: 12,
    color: '#424242',
    paddingTop: 3,
    fontFamily: fontReguler,
  },
  textBodyBold: {
    fontSize: 12,
    color: '#424242',
    paddingTop: 3,
    fontFamily: fontExtraBold,
  },
  textPrice: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 14,
    color: '#ed6d00',
    fontFamily: fontExtraBold,
  },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    backgroundColor: '#BDBDBD',
    marginBottom: 3,
    alignSelf: 'center',
  },
  //Collapse
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },

  ChildView: {
    width: deviceWidth / 1.11,
    borderWidth: 1,
    borderColor: '#BDBDBD',
    margin: 5,
  },

  TouchableOpacityStyle: {
    padding: 10,
    backgroundColor: '#BDBDBD',
    width: deviceWidth / 1.11,
  },

  TouchableOpacityTitleText: {
    width: deviceWidth / 1.11,
    textAlign: 'center',
    color: '#424242',
    fontFamily: fontExtraBold,
    fontSize: 16,
  },

  ExpandViewInsideText: {
    fontSize: 16,
    color: '#000',
    padding: 12,
  },
});
