import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
//Component
import {
  fontReguler,
  fontBold,
  fontExtraBold,
} from '../../../../base/constant/index';
import { ButtonRounded } from '../../../../elements/Button';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const ModalPaymentTransfer = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visible}
      onBackButtonPress={props.closeModal}
      onBackdropPress={props.closeModal}
      style={styles.container}
    >
      <Col style={{ marginTop: 5, marginBottom: 5 }} style={styles.card}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 15,
          }}
        >
          <Text style={styles.textReguler}>
            Thank you for your order at Aeroaja.com.{' '}
          </Text>
          <Text style={styles.textReguler}>
            Please check the My Order menu to see{' '}
          </Text>
          <Text style={styles.textReguler}>your Package Status. </Text>
        </View>
        <ButtonRounded
          label="CHECK MY ORDER"
          size="small"
          color="buttonColor"
          onClick={props.closeModal}
        />
      </Col>
    </Modal>
  );
};
export default ModalPaymentTransfer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textReguler: {
    fontFamily: fontReguler,
    fontSize: 14,
    color: '#222222',
    padding: 3,
  },
  card: {
    marginLeft: 15,
    marginRight: 15,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: deviceHeight / 3.8,
    width: '100%',
    borderRadius: 5,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#a2195b',
    width: '100%',
    height: 50,
  },
});
