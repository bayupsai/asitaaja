import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { fontReguler, thameColors } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = window.height;
let deviceWidth = window.width;

class ModalHotel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
    };
  }

  render() {
    return (
      <Modal
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        isVisible={this.props.modalVisible}
        style={styles.bottomModal}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.modalContent}
        >
          <Grid style={styles.modalHeaderDatepicker}>
            <Row style={{ paddingBottom: 7.5 }}>
              <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row' }}
                onPress={this.props.resetModal}
              >
                <Col
                  size={2}
                  style={{
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                  }}
                >
                  <Icon name="close" type="evilIcon" color="#FFF" size={28} />
                </Col>
                <Col size={8} style={styles.modalTitleSection}>
                  <Text style={{ color: '#fff' }}>Hotel Detail</Text>
                </Col>
              </TouchableOpacity>
            </Row>
          </Grid>
          <Grid>
            <Col style={{ padding: 20 }}>
              <Text style={styles.textBody}>Hotel</Text>
              <Text style={styles.textBold}>{this.props.hotelName}</Text>
              <View
                style={{
                  width: '100%',
                  height: 1,
                  backgroundColor: 'grey',
                  marginBottom: 5,
                  marginTop: 3,
                }}
              />
              <Grid style={{ marginTop: 10 }}>
                <Col>
                  <Text style={styles.textBody}>Check-In</Text>
                  <Text style={styles.textBold}>{this.props.checkIn}</Text>
                </Col>
                <Col>
                  <Text style={styles.textBody}>Check-Out</Text>
                  <Text style={styles.textBold}>{this.props.checkOut}</Text>
                </Col>
              </Grid>
              <View
                style={{
                  width: '100%',
                  height: 1,
                  backgroundColor: 'grey',
                  marginBottom: 5,
                  marginTop: 3,
                }}
              />
              <Grid style={{ marginTop: 10 }}>
                <Col>
                  <Text style={styles.textBody}>Room</Text>
                  <Text style={[styles.textBold, { marginBottom: 10 }]}>
                    {this.props.roomName}
                  </Text>
                  <Text style={styles.textBody}>Capacity</Text>
                  <Text style={styles.textBold}>
                    {this.props.guest} guests/room
                  </Text>
                </Col>
              </Grid>
              <View
                style={{
                  width: '100%',
                  height: 1,
                  backgroundColor: 'grey',
                  marginBottom: 5,
                  marginTop: 3,
                }}
              />
              <Grid style={{ marginTop: 10 }}>
                <Col>
                  <Text style={{ fontSize: 19, fontFamily: fontReguler }}>
                    Guest Name
                  </Text>
                  <Text style={{ fontSize: 19, fontFamily: fontReguler }}>
                    Bayu Permana Putra
                  </Text>
                </Col>
              </Grid>
              <View
                style={{
                  width: '100%',
                  height: 1,
                  backgroundColor: 'grey',
                  marginBottom: 5,
                  marginTop: 3,
                }}
              />
              <Grid style={{ marginTop: 10 }}>
                <Row>
                  <Text style={[styles.textBold, { fontSize: 20 }]}>Price</Text>
                </Row>
                <Row style={{ marginTop: 5, marginBottom: 5 }}>
                  <Col size={7}>
                    <Text style={style.textBody}>
                      {this.props.hotelName}, {this.props.roomName}
                    </Text>
                  </Col>
                  <Col size={3} style={{ alignItems: 'flex-end' }}>
                    <Text style={styles.textBody}>
                      IDR. {this.props.priceRoom}
                    </Text>
                  </Col>
                </Row>
                <Row style={{ marginTop: 5, marginBottom: 5 }}>
                  <Col size={7}>
                    <Text style={[{ color: 'green', fontFamily: fontReguler }]}>
                      Travle Insurance
                    </Text>
                  </Col>
                  <Col size={3} style={{ alignItems: 'flex-end' }}>
                    <Text style={{ color: 'green', fontFamily: fontReguler }}>
                      IDR. 25,000
                    </Text>
                  </Col>
                </Row>
                <Row style={{ marginTop: 5, marginBottom: 5 }}>
                  <Col size={7}>
                    <Text style={[{ color: 'green', fontFamily: fontReguler }]}>
                      Tax Recovery Charges
                    </Text>
                  </Col>
                  <Col size={3} style={{ alignItems: 'flex-end' }}>
                    <Text style={{ color: 'green', fontFamily: fontReguler }}>
                      Include
                    </Text>
                  </Col>
                </Row>
                <Row style={{ marginTop: 5, marginBottom: 5 }}>
                  <Col size={6}>
                    <Text style={{ color: '#000', fontFamily: fontReguler }}>
                      Total
                    </Text>
                  </Col>
                  <Col size={4} style={{ alignItems: 'flex-end' }}>
                    <Text style={{ color: '#000', fontFamily: fontReguler }}>
                      IDR. {this.props.amount}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            </Col>
          </Grid>
        </ScrollView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    // justifyContent: "center",
    // alignItems: "flex-start",
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  textBold: {
    color: '#000',
    fontSize: 16,
    fontFamily: fontReguler,
  },
  textBody: {
    fontFamily: fontReguler,
  },
});

export default ModalHotel;
