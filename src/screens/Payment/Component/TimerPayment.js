import React, { PureComponent } from 'react';
import { StyleSheet, Text, Dimensions } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';

import { Timer } from '../../../utilities/helpers';
import {
  fontReguler,
  fontExtraBold,
  thameColors,
} from '../../../base/constant';

let deviceWidth = Dimensions.get('window').width;

class TimerPayment extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Grid style={styles.container}>
        <Row style={styles.cardTimer}>
          <Col
            size={1}
            style={{ alignItems: 'flex-start', justifyContent: 'center' }}
          >
            <Icon name="ios-timer" size={25} color={thameColors.primary} />
          </Col>
          <Col
            size={5}
            style={{ alignItems: 'center', justifyContent: 'center' }}
          >
            <Text style={styles.textBody}>Complete order within</Text>
          </Col>
          <Col
            size={4}
            style={{ alignItems: 'flex-end', justifyContent: 'center' }}
          >
            <Timer />
          </Col>
        </Row>
      </Grid>
    );
  }
}
export default TimerPayment;

const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 10,
  },
  cardTimer: {
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBody: {
    justifyContent: 'center',
    fontSize: 16,
    color: '#424242',
    fontFamily: fontReguler,
  },
  textTime: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    fontSize: 16,
    color: '#424242',
    fontFamily: fontExtraBold,
  },
});
