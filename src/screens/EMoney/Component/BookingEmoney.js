import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

//Import Component
import { Button } from '../../../elements/Button';
import HeaderPage from '../../../components/HeaderPage';
import BookingSummary from '../../../components/BookingSummary';
import SubHeaderPage from '../../../components/SubHeaderPage';

class BookingEmoney extends PureComponent {
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage title="E-Money" callback={this.goBackHeader} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <BookingSummary
            labelHeader="Booking Summary E-Money"
            labelPhoneNumber="Card Number"
            phoneNumber="6032 9860 5638 4141"
            labelProvider="Bank"
            provider="Mandiri"
            labelPaketData="Nominal"
            paketData="Rp. 25.000"
            labelTotal="Total Payment"
            total="Rp. 25.000"
          />
        </ScrollView>
        <View style={{ marginBottom: 10 }}>
          <Button
            {...this.props}
            label="CONTINUE PAYMENT"
            onClick={() => this.props.navigation.navigate('PaymentOrder')}
          />
        </View>
      </View>
    );
  }
}
export default BookingEmoney;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
});
