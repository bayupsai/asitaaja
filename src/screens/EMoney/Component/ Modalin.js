import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Modal from 'react-native-modal';
import numeral from 'numeral';

//local component
import { Button } from '../../../elements/Button';
import { fontBold, fontReguler, thameColors } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const Modalin = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visibleModal === 1}
      style={styles.bottomModal}
    >
      <View style={styles.modalPassenger}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#838383',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={props.closeModal}
          >
            <Col size={2} style={{ alignItems: 'flex-start', paddingLeft: 10 }}>
              <Icon name="close" type="evilIcon" color="#008195" size={20} />
            </Col>
            <Col
              size={8}
              style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
            >
              <Text
                style={{ color: '#000', fontFamily: fontBold, fontSize: 16 }}
              >
                Package Internet
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid style={{ padding: 10 }}>
          <Col style={{ alignItems: 'center' }}>
            <View>
              <Text
                style={{ fontFamily: fontBold, color: '#000', fontSize: 14 }}
              >
                Select your package
              </Text>
              <Text
                style={{
                  color: '#828282',
                  fontSize: 12,
                  fontFamily: fontReguler,
                }}
              >
                {props.selectedPackage}
              </Text>
            </View>
            <View>
              <ScrollPicker
                style={{ fontSize: 10 }}
                dataSource={['1Gb / month', '3Gb / 2month', '5Gb / 3month']}
                selectedIndex={props.package}
                onValueChange={props.setPackage}
                wrapperWidth={300}
                wrapperHeight={210}
                wrapperBackground={'#FFFFFF'}
                itemHeight={60}
                highlightColor={'#d8d8d8'}
                highlightBorderWidth={0.5}
                activeItemColor={'#222121'}
                itemColor={'#ed6d00'}
              />
            </View>
          </Col>
        </Grid>
        <Grid style={{ flex: 0, paddingBottom: 10 }}>
          <Col>
            <Button label="Done" onClick={props.closeModal} />
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default Modalin;

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
