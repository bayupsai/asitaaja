import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { fontBold, fontReguler } from '../../../base/constant';

//local component

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const Card = props => {
  return (
    <View style={{ flex: 1, borderStartColor: '#f0f0f0' }}>
      <Grid style={styles.card}>
        <Col style={{ marginTop: 15 }}>
          <TouchableOpacity>
            <View style={styles.col}>
              <Text style={styles.textBody}>Enter Card Number</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.textBold}>6032 9860 5638 4141</Text>
            </View>
          </TouchableOpacity>
        </Col>
      </Grid>

      <Grid style={styles.cardPackage}>
        <Col style={{ marginTop: 15 }}>
          <TouchableOpacity onPress={props.showModal}>
            <View style={styles.col}>
              <Text style={styles.textBody}>Select Nominal</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.textBold}>{props.internetPackage}</Text>
            </View>
          </TouchableOpacity>
        </Col>
      </Grid>

      <Grid style={styles.cardTotal}>
        <Col style={{ padding: 20 }}>
          <View>
            <Text style={styles.textPrice}>Information :</Text>
          </View>
          <View>
            <Text style={styles.textBody}>
              1. The maximum balance stored on the card is Rp. 2 million.
            </Text>
          </View>
          <View>
            <Text style={styles.textBody}>
              2. After Top-Up, please update the card balance
            </Text>
          </View>
          <View>
            <Text style={styles.textBody}>
              You are at this application / ATM and bank channel
            </Text>
          </View>
          <View>
            <Text style={styles.textBody}>
              the one you choose / the nearest retail outlet.
            </Text>
          </View>
          <View>
            <Text style={styles.textBody}>
              3. Admin fees for each Top-up transaction are
            </Text>
          </View>
          <View style={{ marginBottom: 8 }}>
            <Text style={styles.textBody}>Rp. 1500.</Text>
          </View>
        </Col>
      </Grid>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: -45,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 9,
  },
  cardPackage: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 9,
  },
  cardTotal: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 40,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 2.5,
  },
  textBody: {
    fontSize: 14,
    color: '#828282',
    marginBottom: 5,
    fontFamily: fontReguler,
  },
  textBold: { fontSize: 16, color: '#222222', fontFamily: fontBold },
  textPrice: {
    fontSize: 14,
    color: '#008d00',
    fontFamily: fontBold,
    marginBottom: 5,
  },
  col: { justifyContent: 'center', alignItems: 'center' },
});
