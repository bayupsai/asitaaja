import React, { PureComponent } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import numeral from 'numeral';

//local component
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { ButtonRounded } from '../../elements/Button';
import Card from './Component/Card';
// import Modalin from './Component/Modalin'
// import ModalPhoneNumber from './Component/ModalPhoneNumber'

class EMoney extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      package: 1,
      packageList: [
        { id: 1, packageName: '1Gb/month', price: 10000 },
        { id: 2, price: 25000 },
        { id: 3, price: 40000 },
      ],
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  // //modal nominal
  toggleModal = i => {
    this.setState({
      visibleModal: i,
    });
  };
  closeModal = () => {
    this.setState({ visibleModal: null });
  };
  setNominal = (type, value) => {
    if (type == 'nominal') {
      if (value > 0) {
        this.setState({ nominal: value });
      }
    }
  };
  //modal nominal

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage title="E-Money" callback={this.goBack} {...this.props} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <Card
            {...this.props}
            showModal={() => this.toggleModal(1)}
            internetPackage={`${'Rp. ' +
              numeral(this.state.packageList[this.state.package].price).format(
                '0,0'
              )}`}
            totalPrice={numeral(
              this.state.packageList[this.state.package].price
            ).format('0,0')}
            selectedPackage={`${'Rp. ' +
              numeral(this.state.packageList[this.state.package].price).format(
                '0,0'
              )}`}
          />
          <View
            style={{
              marginTop: -5,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {/* this.props.navigation.navigate("BookingEmoney") */}
            <ButtonRounded
              label="ORDER E-MONEY"
              onClick={() => console.log('lock')}
            />
          </View>
        </ScrollView>

        {/* <Modalin
                    {...this.props}
                    visibleModal={this.state.visibleModal}
                    closeModal={this.closeModal}
                    package={0}
                    setPackage={(data, selectedIndex) => this.setState({ package: selectedIndex })}
                />   */}
      </View>
    );
  }
}

export default EMoney;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
});
