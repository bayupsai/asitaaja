import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Button, ButtonRounded } from '../../../elements/Button';
import HeaderPage from '../../../components/HeaderPage';
import BookingSummary from '../../../components/BookingSummary';
import SubHeaderPage from '../../../components/SubHeaderPage';
import numeral from 'numeral';

class BookingPulsa extends PureComponent {
  constructor(props) {
    super(props);
    console.log(this.props.navigation.getParam('parameter'));
    this.state = {
      params: this.props.navigation.getParam('parameter'),
      product: this.props.navigation.getParam('parameter').productSelected[0],
    };
  }
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Is Your Order Correct?"
          callback={this.goBackHeader}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <BookingSummary
            labelHeader="Booking Summary Pulsa"
            labelPhoneNumber="Destination Number"
            phoneNumber={this.state.params.phoneNumber}
            labelProvider="Operator"
            provider={this.state.params.operatorName}
            labelPaketData="Nominal Pulsa"
            paketData={numeral(this.state.params.nominal).format('0,0')}
            labelTotal="Total Price"
            total={numeral(this.state.product.harga).format('0,0')}
          />
        </ScrollView>
        <View
          style={{
            marginBottom: 10,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ButtonRounded
            label="CONTINUE TO PAYMENT"
            onClick={() =>
              this.props.navigation.navigate('PaymentOrder', {
                pages: 'ppob',
                product: this.state.product,
              })
            }
          />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    payload: state.ppob.payloadPulsa,
    response: state.ppob.dataPulsa,
    loading: state.ppob.fetching,
  };
}
export default connect(mapStateToProps)(BookingPulsa);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
});
