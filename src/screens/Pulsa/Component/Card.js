import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { CheckBox } from 'react-native-elements';
import numeral from 'numeral';
import { fontReguler, fontBold } from '../../../base/constant';
let deviceHeight = Dimensions.get('window').height;

class Card extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      saveNumber: false,
    };
  }

  render() {
    return (
      <View style={{ backgroundColor: '#f0f0f0' }}>
        <TouchableOpacity
          onPress={this.props.showModalNumber}
          style={styles.card}
        >
          <Grid>
            <Col style={styles.col}>
              <View style={{ alignItems: 'center' }}>
                <Text style={styles.textBody}>Enter Phone Number</Text>
              </View>
              <View style={{ paddingTop: 0 }}>
                <Text style={styles.textBold}>{this.props.phone}</Text>
              </View>
            </Col>
          </Grid>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.props.showModal}>
          <Grid style={styles.cardPackage}>
            <Col style={styles.col}>
              <View>
                <Text style={styles.textBody}>Select Nominal Pulsa</Text>
              </View>
              <View style={{ alignItems: 'center' }}>
                <Text style={styles.textBold}>
                  Rp{numeral(this.props.nominal).format('0,0')}
                </Text>
              </View>
            </Col>
          </Grid>
        </TouchableOpacity>

        <Grid style={styles.cardPackage}>
          <Col size={0.5}></Col>
          <Col
            size={1.5}
            style={{ justifyContent: 'center', alignItems: 'center' }}
          >
            <CheckBox
              containerStyle={{
                backgroundColor: '#ffffff',
                padding: 0,
                margin: 0,
                marginLeft: 0,
                borderWidth: 0,
              }}
              checked={this.state.saveNumber}
              onPress={() =>
                this.setState({ saveNumber: !this.state.saveNumber })
              }
              iconType="material-community"
              checkedIcon="checkbox-marked"
              uncheckedIcon="checkbox-blank-outline"
              checkedColor="#828282"
              uncheckedColor="#828282"
            />
          </Col>
          <Col
            size={8}
            style={{ justifyContent: 'center', alignItems: 'flex-start' }}
          >
            <Text style={styles.textBody}>Save Number for Next Purchase </Text>
          </Col>
        </Grid>

        <Grid style={styles.cardTotal}>
          <Col style={styles.col}>
            <View style={{ justifyContent: 'flex-start' }}>
              <Text style={[styles.textBody]}>Total Payment</Text>
            </View>
            <View style={{ paddingTop: 0 }}>
              <Text style={styles.textPrice}>
                Rp{' '}
                {this.props.productSelected
                  ? numeral(this.props.productSelected[0].harga).format('0,0')
                  : 0}
              </Text>
            </View>
          </Col>
        </Grid>
      </View>
    );
  }
}

export default Card;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: -45,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 9.8,
  },
  cardPackage: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 9.8,
  },
  cardTotal: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 0,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 5,
  },
  textBody: {
    fontSize: 16,
    color: '#828282',
    marginBottom: 2,
    fontFamily: fontReguler,
  },
  textBold: { fontSize: 20, color: '#222222', fontFamily: fontBold },
  textPrice: {
    fontSize: 20,
    color: '#008d00',
    fontFamily: fontBold,
    letterSpacing: 1,
  },
  col: { justifyContent: 'center', alignItems: 'center' },
});
