import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { Grid, Row } from 'react-native-easy-grid';
import { ButtonRounded } from '../../../elements/Button';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import { fontReguler, fontExtraBold } from '../../../base/constant/index';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

class BookingPulsaSuccess extends PureComponent {
  goBackHeader = () => {
    this.props.navigation.navigate('Tab');
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage title="Purchase Successful" callback={this.goBackHeader} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <Grid style={styles.content}>
            <Row
              style={{
                borderBottomColor: '#f0f0f0',
                borderBottomWidth: 0.75,
                paddingBottom: 20,
              }}
            >
              <Image
                resizeMode="center"
                style={{ width: deviceWidth - 80, height: deviceHeight / 4 }}
                source={require('../../../assets/pulsa/booking_success_pulsa.png')}
              />
            </Row>
            <Row style={styles.paddingTop30}>
              <Grid style={{ alignItems: 'center' }}>
                <Row>
                  <Text style={{ fontFamily: fontReguler, fontSize: 16 }}>
                    Phone Number
                  </Text>
                </Row>
                <Row style={{ paddingTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: fontExtraBold,
                      fontSize: 18,
                      color: '#222222',
                    }}
                  >
                    {this.props.stateData.phoneNumber}
                  </Text>
                </Row>
              </Grid>
            </Row>
            <Row style={styles.paddingTop30}>
              <Grid style={{ alignItems: 'center' }}>
                <Row>
                  <Text style={{ fontFamily: fontReguler, fontSize: 16 }}>
                    Operator
                  </Text>
                </Row>
                <Row style={{ paddingTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: fontExtraBold,
                      fontSize: 18,
                      color: '#222222',
                    }}
                  >
                    {this.props.stateData.operatorName}
                  </Text>
                </Row>
              </Grid>
            </Row>
            <Row style={styles.paddingTop30}>
              <Grid style={{ alignItems: 'center' }}>
                <Row>
                  <Text style={{ fontFamily: fontReguler, fontSize: 16 }}>
                    Nominal Pulsa
                  </Text>
                </Row>
                <Row style={{ paddingTop: 5 }}>
                  <Text
                    style={{
                      fontFamily: fontExtraBold,
                      fontSize: 18,
                      color: '#222222',
                    }}
                  >
                    Rp{this.props.stateData.nominal}
                  </Text>
                </Row>
              </Grid>
            </Row>
          </Grid>
        </ScrollView>
        <View
          style={{
            marginBottom: 10,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ButtonRounded
            label="BACK TO HOME"
            onClick={() => this.props.navigation.navigate('Tab')}
          />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    payload: state.ppob.payloadPulsa,
    response: state.ppob.dataPulsa,
    stateData: state.ppob.stateDataPulsa,
    loading: state.ppob.fetching,
  };
}
export default connect(mapStateToProps)(BookingPulsaSuccess);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#f0f0f0' },
  content: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: -50,
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 7.5,
    padding: 20,
    paddingBottom: 40,
  },
  paddingTop30: { paddingTop: 20 },
});
