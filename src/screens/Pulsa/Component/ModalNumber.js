import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Icon, Tooltip } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import { Button } from '../../../elements/Button';
import { InputText } from '../../../elements/TextInput';
import { fontBold, thameColors } from '../../../base/constant';
const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const ModalNumber = props => {
  return (
    <View>
      <Modal
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        isVisible={props.visibleModalNumber === 2}
        style={styles.bottomModal}
        onBackdropPress={props.onDismiss}
        onBackButtonPress={props.onDismiss}
      >
        <View style={styles.modalPassenger}>
          <Grid
            style={{
              width: deviceWidth,
              height: 40,
              flex: 0,
              justifyContent: 'center',
              alignItems: 'center',
              borderBottomColor: '#838383',
              borderBottomWidth: 0.5,
            }}
          >
            <TouchableOpacity
              style={{ flex: 1, flexDirection: 'row' }}
              onPress={props.closeModalNumber}
            >
              <Col
                size={2}
                style={{ alignItems: 'flex-start', paddingLeft: 10 }}
              >
                <Icon name="close" type="evilIcon" color="#008195" size={20} />
              </Col>
              <Col
                size={8}
                style={{ alignItems: 'flex-start', paddingLeft: '3%' }}
              >
                <Text
                  style={{ color: '#000', fontFamily: fontBold, fontSize: 16 }}
                >
                  Enter Phone Number
                </Text>
              </Col>
            </TouchableOpacity>
          </Grid>
          <Grid style={{ marginTop: 10 }}>
            <Col>
              <InputText
                keyboardType="phone-pad"
                maxLength={12}
                label="Phone Number"
                placeholder="e.g 08xxxxxxxxxx"
                onChangeText={props.phoneNumber}
                keyboardType="number-pad"
              />
            </Col>
          </Grid>

          <Grid
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 10,
              marginLeft: 20,
            }}
          >
            <Col style={{ alignItems: 'flex-start', justifyContent: 'center' }}>
              <Text
                style={{ color: '#000', fontFamily: fontBold, fontSize: 18 }}
              >
                {props.provider}
              </Text>
            </Col>
          </Grid>

          <Grid style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Col>
              <Button label="DONE" onClick={props.closeModalNumber} />
            </Col>
          </Grid>
        </View>
      </Modal>
    </View>
  );
};

export default ModalNumber;

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 3,
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
