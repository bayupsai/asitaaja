import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import Modal from 'react-native-modal';
import numeral from 'numeral';

//local component
import { Button } from '../../../elements/Button';
import { fontBold, thameColors } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const Modalin = props => {
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating={true}
      isVisible={props.visibleModal === 1}
      style={styles.bottomModal}
      onBackButtonPress={props.onDismiss}
      onBackdropPress={props.onDismiss}
    >
      <View style={styles.modalPassenger}>
        <Grid
          style={{
            width: deviceWidth,
            height: 40,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomColor: '#838383',
            borderBottomWidth: 0.5,
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, flexDirection: 'row' }}
            onPress={props.closeModal}
          >
            <Col size={2} style={{ alignItems: 'flex-start', paddingLeft: 10 }}>
              <Icon name="close" type="evilIcon" color="#008195" size={20} />
            </Col>
            <Col size={8} style={{ paddingLeft: 30 }}>
              <Text
                style={{ color: '#000', fontSize: 16, fontFamily: fontBold }}
              >
                {' '}
                Select Nominal Pulsa
              </Text>
            </Col>
          </TouchableOpacity>
        </Grid>
        <Grid style={{ padding: 10 }}>
          <Col style={{ alignItems: 'center' }}>
            <View style={{ justifyContent: 'center' }}>
              <ScrollPicker
                style={{ fontSize: 10 }}
                dataSource={props.listPulsa}
                selectedIndex={props.nominal}
                onValueChange={(data, selectedIndex) =>
                  props.setNominal(data, selectedIndex)
                }
                wrapperWidth={300}
                wrapperHeight={210}
                wrapperBackground={'#FFFFFF'}
                itemHeight={60}
                highlightColor={'#d8d8d8'}
                highlightBorderWidth={0.5}
                activeItemColor={'#222121'}
                itemColor={'#ed6d00'}
              />
            </View>
          </Col>
        </Grid>
        <Grid style={{ flex: 0, paddingBottom: 10 }}>
          <Col>
            <Button label="DONE" onClick={props.closeModal} />
          </Col>
        </Grid>
      </View>
    </Modal>
  );
};

export default Modalin;

const styles = StyleSheet.create({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 100,
    width: deviceWidth,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: thameColors.primary,
    height: 50,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: '20%',
  },
  modalTitle: {
    color: '#FFFFFF',
    fontWeight: '400',
    fontSize: 16,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 1.99,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
