import React, { PureComponent } from 'react';
import { View, Text, ScrollView, Alert } from 'react-native';
import { connect } from 'react-redux';
import { actionPpob } from '../../redux/actions/PpobAction';
import styles from './styles';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { Button, ButtonRounded } from '../../elements/Button';
import Card from './Component/Card';
import Modalin from './Component/Modalin';
import ModalPhoneNumber from './Component/ModalNumber';
import { checkOperator } from '../../utilities/helpers';
import AlertModal from '../../components/AlertModal';

class Pulsa extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      visibleModalNumber: null,
      phoneNumber: 'e.g 08xxxxxxx',
      nominal: '',
      listPulsa: [],
      listProduct: [],
      operatorName: '',
      productSelected: '',
    };
    // 081533328150
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  toggleModal = i => {
    if (i === 1) {
      // nominal pulsa
      if (this.state.listPulsa.length == 0) {
        this.setState({ visibleModal: 90 });
        // Alert.alert(
        //     'Top Up Pulsa',
        //     'Please enter the phone number first.',
        //     [
        //       {text: 'OK', onPress: () => console.log('OK Pressed')},
        //     ],
        //     {cancelable: false},
        // );
      } else {
        this.setState({
          visibleModal: i,
        });
      }
    } else {
      this.setState({
        visibleModal: i,
      });
    }
  };
  closeModal = () => {
    this.setState({ visibleModal: null });
  };

  toggleModalNumber = i => {
    this.setState({
      visibleModalNumber: i,
      operatorName: '',
    });
  };
  closeModalNumber = () => {
    this.setState({ visibleModalNumber: null });
  };

  filterProduct = (arr, nominalData) => {
    return arr.filter(function(data, index) {
      if (data.nominal == nominalData) {
        return arr[index];
      }
    });
  };

  setNominal = (type, value) => {
    if (type == 'nominal') {
      if (value > 0) {
        let data = this.filterProduct(this.state.listProduct, value);
        this.setState({ nominal: value, productSelected: data });
      }
    }
  };

  setPhoneNumber = phone => {
    this.setState({
      listPulsa: [],
      operatorName: '',
      productSelected: '',
      nominal: '',
    });
    const getListOperator = checkOperator(phone);
    if (getListOperator) {
      let arrayNominal = [];
      arrayNominal.push('-');
      getListOperator.dataProduct.map((data, index) => {
        arrayNominal.push(data.nominal);
      });
      this.setState({
        phoneNumber: phone,
        listProduct: getListOperator.dataProduct,
        listPulsa: arrayNominal,
        operatorName: getListOperator.name,
      });
    }
  };

  orderPulsa = () => {
    console.log(this.state);
    if (this.state.productSelected == '') {
      this.setState({ visibleModal: 91 });
      // Alert.alert(
      //     'Top Up Pulsa',
      //     'Please enter phone number and select nominal pulsa correctly',
      //     [
      //         { text: 'OK', onPress: () => console.log('OK Pressed') },
      //     ],
      //     { cancelable: false },
      // );
    } else {
      console.log(this.state.productSelected);
      var productCode = this.state.productSelected[0].code;
      console.log(productCode);
      if (productCode.charAt(0) == 'I') {
        let payload = {
          command: 'PURCHASE',
          product: this.state.productSelected[0].code,
          customer: this.state.phoneNumber,
          partner_trxid: '',
        };
        console.log(payload);
        if (this.state.productSelected[0].code == 'I12') {
          this.props
            .dispatch(actionPpob(payload, this.state))
            .then(res => {
              if (res.type == 'PULSA_SUCCESS') {
                this.props.navigation.navigate('BookingPulsa', {
                  parameter: this.state,
                });
                // alert(JSON.stringify(res))
              } else {
                this.setState({ visibleModal: 92 });
                // Alert.alert(
                //     'Top Up Pulsa',
                //     res.message,
                //     // 'Failed, please try again later.',
                //     [
                //         { text: 'OK', onPress: () => console.log('OK Pressed') },
                //     ],
                //     { cancelable: false },
                // );
              }
            })
            .catch(err => {
              console.log(`Error at : ${err}`);
            });
        } else {
          this.setState({ visibleModal: 93 });
          // Alert.alert(
          //     'Top Up Pulsa',
          //     'Currently, we only available for Indosat 12,000',
          //     [
          //         { text: 'OK', onPress: () => console.log('OK Pressed') },
          //     ],
          //     { cancelable: false },
          // );
        }
      } else {
        this.setState({ visibleModal: 94 });
        // Alert.alert(
        //     'Top Up Pulsa',
        //     'Currently, we only available for Indosat pulsa. We will launch for all provider soon, Thanks.',
        //     [
        //         { text: 'OK', onPress: () => console.log('OK Pressed') },
        //     ],
        //     { cancelable: false },
        // );
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Top Up Pulsa"
          callback={this.goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <Card
            {...this.props}
            showModal={() => this.toggleModal(1)}
            phoneNumber={i => this.setPhoneNumber(i)}
            productSelected={this.state.productSelected}
            nominal={this.state.nominal}
            phone={this.state.phoneNumber}
            showModalNumber={() => this.toggleModalNumber(2)}
          />
          <View
            style={{
              marginBottom: 15,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 30,
            }}
          >
            <ButtonRounded
              label="ORDER PULSA"
              onClick={() => this.orderPulsa()}
            />
          </View>
        </ScrollView>
        {/* ============================ START MODAL HERE ============================ */}
        <ModalPhoneNumber
          {...this.props}
          phoneNumber={i => this.setPhoneNumber(i)}
          provider={this.state.operatorName}
          visibleModalNumber={this.state.visibleModalNumber}
          closeModalNumber={this.closeModalNumber}
          onDismiss={() => this.setState({ visibleModalNumber: null })}
        />

        <Modalin
          {...this.props}
          visibleModal={this.state.visibleModal}
          closeModal={this.closeModal}
          nominal={0}
          listPulsa={this.state.listPulsa}
          operatorName={this.state.operatorName}
          setNominal={(data, selectedIndex) =>
            this.setNominal('nominal', data, selectedIndex)
          }
          onDismiss={() => this.setState({ visibleModal: null })}
        />

        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 90}
          onDismiss={() => this.setState({ visibleModal: null })}
          onPress={() => this.setState({ visibleModal: null })}
          title="Top Up Pulsa"
          contentText="Please enter the phone number first."
        />
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 91}
          onDismiss={() => this.setState({ visibleModal: null })}
          onPress={() => this.setState({ visibleModal: null })}
          title="Top Up Pulsa"
          contentText="Please enter phone number and select nominal pulsa correctly"
        />
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 92}
          onDismiss={() => this.setState({ visibleModal: null })}
          onPress={() => this.setState({ visibleModal: null })}
          title="Top Up Pulsa"
          contentText="Failed, please try again later."
        />
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 93}
          onDismiss={() => this.setState({ visibleModal: null })}
          onPress={() => this.setState({ visibleModal: null })}
          title="Top Up Pulsa"
          contentText="Currently, we only available for Indosat 12,000"
        />
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 94}
          onDismiss={() => this.setState({ visibleModal: null })}
          onPress={() => this.setState({ visibleModal: null })}
          title="Top Up Pulsa"
          contentText="Currently, we only available for Indosat pulsa. We will launch for all provider soon, Thanks."
        />
        {/* ============================ START MODAL HERE ============================ */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    pulsa: state.ppob.dataPulsa,
    loading: state.ppob.fetching,
  };
}
export default connect(mapStateToProps)(Pulsa);
