import React, { PureComponent } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Icon as IconElements } from 'react-native-elements';
import { Row, Col, Grid } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import moment from 'moment';
import Dates from 'react-native-dates';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import { DotIndicator } from 'react-native-indicators';
import _ from 'lodash';

// redux
import { connect } from 'react-redux';
import {
  actionDestinationBeds,
  actionSearchHotelBeds,
} from '../../redux/actions/HotelAction';

// import other components
import { SearchInput, InputTextRevamp } from '../../elements/TextInput';
import { ButtonRounded, Button } from '../../elements/Button';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import styles from './styles';
import {
  fontReguler,
  thameColors,
  fontSemiBold,
  asitaColor,
} from '../../base/constant';
import AlertModal from '../../components/AlertModal';
import {
  makeListCity,
  makeListDestinationBeds,
} from '../../redux/selectors/HotelSelector';
import { makeDataProfile } from '../../redux/selectors/ProfileSelector';

const isDateBlocked = date => date.isBefore(moment(), 'day');

class SearchHotel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      searchHotelCity: 'jakarta',
      date: null,
      focus: 'start_date',
      startDate: null,
      endDate: null,
      listCity: [
        { name: 'Jakarta' },
        { name: 'Bali' },
        { name: 'Palembang' },
        { name: 'Pontianak' },
        { name: 'Semarang' },
        { name: 'Surabaya' },
        { name: 'Jogja' },
        { name: 'Solo' },
        { name: 'Medan' },
        { name: 'Bandung' },
        { name: 'Pekanbaru' },
      ],
      destinationLocation: [
        { city_code: 'JKT', name: 'Jakarta' },
        { city_code: 'DPS', name: 'Denpasar' },
      ],
      switch: false,
      star: [
        { id: 0, star: '1' },
        { id: 1, star: '2' },
        { id: 2, star: '3' },
        { id: 3, star: '4' },
        { id: 4, star: '5' },
      ],
      dataNight: [
        {
          id: 1,
          checkout: moment()
            .add(1, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 2,
          checkout: moment()
            .add(2, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 3,
          checkout: moment()
            .add(3, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 4,
          checkout: moment()
            .add(4, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 5,
          checkout: moment()
            .add(5, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 6,
          checkout: moment()
            .add(6, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 7,
          checkout: moment()
            .add(7, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 8,
          checkout: moment()
            .add(8, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 9,
          checkout: moment()
            .add(9, 'days')
            .format('YYYY-MM-DD'),
        },
        {
          id: 10,
          checkout: moment()
            .add(10, 'days')
            .format('YYYY-MM-DD'),
        },
      ],
      selectedStar: [null],
      destinationResult: [],
      destination: { city_code: 'null', name: 'Where you want to go?' },
      q: '',
      start_date: moment()
        .add(1, 'days')
        .format('YYYY-MM-DD'),
      end_date: moment()
        .add(2, 'days')
        .format('YYYY-MM-DD'),
      // end_date: null,
      adult: 1,
      child: 0,
      night: 1,
      rooms: 1,
    };
  }

  componentDidMount() {
    // const { destinationLocation } = this.state;
    // InteractionManager.runAfterInteractions(() => {
    //   this.getListHotelCity(destinationLocation[0].name);
    //   this.props.dispatch(failedState('searchHotel'));
    // });
  }

  getListHotelCity = text => {
    const { dispatch } = this.props;
    const query = {
      query: text,
    };
    dispatch(actionDestinationBeds(query));
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  _switchToggle = toggle => {
    this.setState({ switch: toggle });
  };

  navigateTo = page => {
    this.props.navigation.navigate(page);
  };

  // Modal
  // 1 = destination, 2 = checkin, 3 = checkout, 4 = guest, 5 = room
  _toggleModal = modal => this.setState({ visibleModal: modal });

  _toggleModal1 = () => this.setState({ visibleModal: 1 });

  _toggleModal2 = () => this.setState({ visibleModal: 2 });

  _toggleModal3 = () => this.setState({ visibleModal: 3 });

  _toggleModal5 = () => this.setState({ visibleModal: 5 });

  _setNullModal = () => this.setState({ visibleModal: null });

  resetModal = () =>
    this.setState({ visibleModal: null, destinationResult: [] });
  // Modal

  // Flatlist Conf
  _keyExtractor = (item, index) => index.toString();

  _renderListHotelCity = ({ item, i }) => {
    const str = item ? item.name : '';
    const city_name = str.substr(0, 23);
    return (
      <Grid key={i} style={styles.contentListCity}>
        <TouchableOpacity
          style={styles.buttonListCity}
          onPress={() =>
            this.setOriginDestination(this.state.visibleModal, item)
          }
        >
          <Col style={styles.startBetween}>
            <Text style={styles.textBoldBlack}>
              {str.length > 24 ? `${city_name}...` : str}
            </Text>
            <View style={styles.childListCity}>
              <Text
                style={{
                  fontFamily: fontSemiBold,
                  color: asitaColor.tealBlue,
                }}
              >
                {item ? item.type.toUpperCase() : ''}
              </Text>
            </View>
          </Col>
        </TouchableOpacity>
      </Grid>
    );
  };
  // Flatlist Conf

  // Filter City
  searchingHotel = val => {
    if (val.length > 3) {
      this.setState({ searchHotelCity: val });
    }
  };

  filterValuePart = (arr, part) => {
    return arr.filter(function(item) {
      const itemData = item.name.toUpperCase();
      const textData = part.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
  };
  // Filter City

  setOriginDestination = (modalType, data) => {
    if (modalType === 1) {
      // departure
      this.setState({
        visibleModal: null,
        q: data,
      });
    }
  };

  setCheckIn = ({ date }) => {
    if (this.state.start_date === this.state.end_date) {
      this.setState({
        start_date: moment(date).format('YYYY-MM-DD'),
        end_date: moment(date)
          .add(this.state.night, 'days')
          .format('YYYY-MM-DD'),
        visibleModal: null,
        focus: date,
      });
    } else if (this.state.end_date >= this.state.start_date) {
      this.setState({
        start_date: moment(date).format('YYYY-MM-DD'),
        end_date: moment(date)
          .add(this.state.night, 'days')
          .format('YYYY-MM-DD'),
        visibleModal: null,
        focus: date,
      });
    } else {
      this.setState({
        start_date: moment(date).format('YYYY-MM-DD'),
        visibleModal: null,
        focuse: date,
      });
    }
  };

  setDuration = (guestType, val) => {
    if (
      guestType === 'duration' ||
      this.state.start_date === this.state.end_date ||
      this.state.start_date >= this.state.end_date
    ) {
      this.setState({
        night: val,
        end_date: moment(this.state.start_date)
          .add(val, 'days')
          .format('YYYY-MM-DD'),
      });
    }
  };

  setGuestData = (guestType, value) => {
    if (guestType === 'adult') {
      if (value > 0) {
        this.setState({ adult: value });
      }
    } else if (guestType === 'child') {
      this.setState({ child: value });
    }
  };

  setRoomData = (roomType, value) => {
    if (roomType === 'room') {
      if (value > 0) {
        this.setState({ rooms: value });
      }
    }
  };
  // /Modal

  // loading
  loadingBooking = () => (
    <Grid style={styles.contentLoading}>
      <Col style={styles.alignCenter}>
        <Row style={styles.flex0}>
          <Text style={styles.textSuperBold}>Please Wait.</Text>
        </Row>
        <Row style={styles.flexTop}>
          <Text style={styles.textReguler}>We still process your request.</Text>
        </Row>
        <Row style={styles.contentIndicator}>
          <DotIndicator
            size={10}
            style={styles.indicator}
            color={thameColors.secondary}
          />
        </Row>
      </Col>
    </Grid>
  );
  // loading

  // Empty List
  emptyComponent = () => (
    <View style={styles.center}>
      <Text
        style={{
          color: thameColors.textBlack,
          fontFamily: fontReguler,
        }}
      >
        Could not find your city
      </Text>
    </View>
  );

  // Search Hotel
  searchHotel = () => {
    const { dispatch, navigation } = this.props;
    const payload = {
      searchBy: {
        type: this.state.q !== '' ? this.state.q.type : 'city',
        code: this.state.q !== '' ? this.state.q.code : 'JAV',
      },
      stay: {
        checkIn: this.state.start_date,
        checkOut: this.state.end_date,
      },
      occupancies: [
        {
          rooms: this.state.rooms,
          adults: this.state.adult,
          children: this.state.child,
        },
      ],
      stars: 5,
      offset: 1,
      limit: 50,
    };
    requestAnimationFrame(() => {
      dispatch(actionSearchHotelBeds(payload));
      navigation.navigate('SearchHotelResult', {
        keywords: this.state.q !== '' ? this.state.q.name : 'Hotels Near You',
        payloadHotel: payload,
      });
    });
  };
  // Search Hotel

  // Main Render
  render() {
    const { start_date, end_date, night } = this.state;
    const { listDestinationBeds, loadingDestinationBeds } = this.props;
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Booking Your Hotels"
          callback={this.goBack}
          {...this.props}
          rightComponent={
            <TouchableOpacity style={styles.buttonHeader}>
              <IconElements
                type="feather"
                name="bookmark"
                color={thameColors.white}
                size={30}
              />
            </TouchableOpacity>
          }
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          {/* New */}
          <Grid style={styles.contentInput}>
            <Col>
              <InputTextRevamp
                source={require('../../assets/icons/map_3x.png')}
                onPress={this._toggleModal1}
                label="Destination"
                editable={this.state.q ? this.state.q.name : 'Hotel Nears You'}
              />
            </Col>
          </Grid>
          <Grid style={styles.marginTop}>
            <Col>
              <InputTextRevamp
                source={require('../../assets/icons/icon_departure.png')}
                onPress={this._toggleModal2}
                label="Check-in"
                editable={moment(this.state.start_date).format(
                  'ddd, DD MMM YYYY'
                )}
              />
            </Col>
          </Grid>
          <Grid style={styles.marginTop}>
            <Col>
              <InputTextRevamp
                source={require('../../assets/icons/icon_return.png')}
                onPress={this._toggleModal3}
                label="Duration"
                editable={
                  start_date === end_date
                    ? `${moment(start_date)
                        .add(night, 'days')
                        .format('ddd, DD MMM YYYY')} ` +
                      `(${this.state.night} Night)`
                    : `${moment(end_date).format('ddd, DD MMM YYYY')} ` +
                      `(${night} Night)`
                }
              />
            </Col>
          </Grid>
          <Grid style={styles.marginTop}>
            <Col>
              <InputTextRevamp
                source={require('../../assets/icons/icon_total_passenger.png')}
                onPress={this._toggleModal5}
                label="Room & Guest"
                editable={`${this.state.rooms} Room, ${this.state.adult +
                  this.state.child} Guest (#${this.state.adult}, #${
                  this.state.child
                })`}
              />
            </Col>
          </Grid>
          {/* <Grid style={styles.marginTop}>
            <Col>
              <InputTextRevamp
                source={require('../../assets/icons/icon_total_passenger.png')}
                onPress={() => this._toggleModal(6)}
                label="Hotel Star"
              // editable={}
              />
            </Col>
          </Grid> */}
          {/* //New */}
          <Grid style={styles.sectionButtonArea}>
            <View style={styles.sectionButton}>
              <ButtonRounded label="SEARCH HOTEL" onClick={this.searchHotel} />
            </View>
          </Grid>
        </ScrollView>

        {/* =============================== MODAL Hotel City ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalContent}>
            <Grid style={styles.flex0}>
              <Row style={[styles.modalHeader, styles.topPad]}>
                <TouchableOpacity
                  style={styles.buttonCloseModal}
                  onPress={this.resetModal}
                >
                  <Col size={2} style={styles.center}>
                    <Icon
                      name="close"
                      type="evilIcon"
                      color={thameColors.white}
                      size={28}
                    />
                  </Col>
                  <Col size={6.5} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>Choose Destination</Text>
                  </Col>
                  <Col size={1.5} />
                </TouchableOpacity>
              </Row>
              <SubHeaderPage />
              <Row style={styles.modalSearch}>
                <Col>
                  <SearchInput
                    placeholder="City, Destination or Hotel name"
                    onChangeText={data => data}
                    returnKeyType="search"
                    onSubmitEditing={event =>
                      this.getListHotelCity(event.nativeEvent.text)
                    }
                  />
                </Col>
              </Row>
            </Grid>
            <Grid>
              <Col style={styles.modalContentCity}>
                <ScrollView>
                  <Grid style={styles.popularDestination}>
                    <Col style={styles.alignStart}>
                      <Text style={styles.textPopular}>
                        Popular destination
                      </Text>
                    </Col>
                  </Grid>
                  <Grid>
                    <Col>
                      {loadingDestinationBeds ? (
                        <DotIndicator color={asitaColor.orange} />
                      ) : (
                        <FlatList
                          keyExtractor={this._keyExtractor}
                          data={
                            listDestinationBeds.hotels !== null &&
                            listDestinationBeds.city !== null &&
                            listDestinationBeds.length !== 0
                              ? _.concat(
                                  listDestinationBeds.city,
                                  listDestinationBeds.hotels
                                )
                              : []
                          }
                          renderItem={this._renderListHotelCity}
                          getItemLayout={(item, index) => ({
                            length: 10,
                            offset: 10 * index,
                            index,
                          })}
                          removeClippedSubviews
                          windowSize={30}
                          ListEmptyComponent={this.emptyComponent}
                        />
                      )}
                    </Col>
                  </Grid>
                </ScrollView>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL Hotel City ===================================== */}

        {/* Modal, versi CheckIn */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 2}
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View style={styles.modalContent}>
            <Grid style={styles.modalHeaderDatepicker}>
              <Row style={styles.bottomPad}>
                <TouchableOpacity
                  style={styles.buttonCloseModal}
                  onPress={this.resetModal}
                >
                  <Col size={2} style={styles.center}>
                    <Icon
                      name="close"
                      type="evilIcon"
                      color={thameColors.white}
                      size={28}
                    />
                  </Col>
                  <Col size={6.5} style={styles.modalTitleSection}>
                    <Text style={styles.modalTitle}>
                      {this.state.visibleModal === 2 ? 'Check In' : 'Check Out'}
                    </Text>
                  </Col>
                  <Col size={1.5} />
                </TouchableOpacity>
              </Row>
            </Grid>
            <Grid>
              <Col>
                <View style={styles.flex}>
                  <Dates
                    date={this.state.date}
                    onDatesChange={
                      this.state.visibleModal === 2
                        ? this.setCheckIn
                        : this.setCheckOut
                    }
                    startDate={this.state.start_date}
                    endDate={this.state.end_date}
                    focusedInput={this.state.focus}
                    isDateBlocked={isDateBlocked}
                  />
                  <Text style={styles.textDate}>
                    {this.state.visibleModal === 2
                      ? `Check In: ${moment(this.state.start_date).format(
                          'DD MMM YYYY'
                        )}`
                      : `Check Out: ${this.state.end_date}`}
                  </Text>
                </View>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* Modal, versi CheckIn */}

        {/* Modal, versi Duration */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 3}
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View style={styles.modalPassenger}>
            <Grid style={styles.modalContentDuration}>
              <TouchableOpacity
                style={styles.buttonCloseModal}
                onPress={this._setNullModal}
              >
                <Col size={2} style={styles.startLeft10}>
                  <Icon
                    name="close"
                    type="evilIcon"
                    color={asitaColor.tealBlue}
                    size={20}
                  />
                </Col>
                <Col size={8} style={styles.alignStart}>
                  <Text style={styles.textDuration}>
                    Choose Duration of Stay(s)
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={styles.padding}>
              <Col style={styles.alignCenter}>
                <View>
                  {/* <Text
                    style={{ fontWeight: "500", color: "#000", fontSize: 14 }}
                  >
                    Checkout
                  </Text>
                  <Text style={{ color: "#828282", fontSize: 12 }}> / Night(s)</Text> */}
                </View>
                <View>
                  <ScrollPicker
                    // style={{ fontSize: 10 }}
                    dataSource={[
                      1,
                      2,
                      3,
                      4,
                      5,
                      6,
                      7,
                      8,
                      9,
                      10,
                      11,
                      12,
                      13,
                      14,
                      15,
                    ]}
                    selectedIndex={this.state.night}
                    onValueChange={(data, selectedIndex) =>
                      this.setDuration('duration', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.gray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.superBack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
            </Grid>
            <Grid style={styles.buttonDuration}>
              <Col>
                <Button label="Done" onClick={this._setNullModal} />
              </Col>
            </Grid>
          </View>
        </Modal>

        {/* Modal, versi Room */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 5}
          style={styles.bottomModal}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
        >
          <View style={styles.modalPassenger}>
            {/* room */}
            <Grid style={styles.modalContentRoom}>
              <TouchableOpacity
                style={styles.buttonCloseModal}
                onPress={this._setNullModal}
              >
                <Col size={2} style={styles.startLeft10}>
                  <Icon
                    name="close"
                    type="evilIcon"
                    color={asitaColor.tealBlue}
                    size={20}
                  />
                </Col>
                <Col size={6} style={styles.alignCenter}>
                  <Text style={styles.textBlack}>Choose Total Guest(s)</Text>
                </Col>
                <Col size={2} />
              </TouchableOpacity>
            </Grid>
            {/* adult */}
            <Grid style={styles.padding}>
              <Col style={styles.alignCenter}>
                <View>
                  <Text style={styles.textGuest}>Total Room(s)</Text>
                </View>
                <View>
                  <ScrollPicker
                    // style={{ fontSize: 10 }}
                    dataSource={[1, 2, 3, 4, 5, 6, 7]}
                    selectedIndex={this.state.rooms - 1}
                    onValueChange={(data, selectedIndex) =>
                      this.setRoomData('room', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.gray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
              <Col style={styles.alignCenter}>
                <View>
                  <Text style={styles.textGuest}>Total Adult(s)</Text>
                </View>
                <View>
                  <ScrollPicker
                    // style={{ fontSize: 10 }}
                    dataSource={[1, 2, 3, 4, 5, 6, 7, 8, 9]}
                    selectedIndex={this.state.adult - 1}
                    onValueChange={(data, selectedIndex) =>
                      this.setGuestData('adult', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.gray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
              <Col style={styles.alignCenter}>
                <View>
                  <Text style={styles.textGuest}>Total Child(s)</Text>
                </View>
                <View>
                  <ScrollPicker
                    // style={{ fontSize: 10 }}
                    dataSource={[0, 1, 2, 3, 4, 5, 6, 7, 8]}
                    selectedIndex={this.state.adult - 1}
                    onValueChange={(data, selectedIndex) =>
                      this.setGuestData('child', data)
                    }
                    wrapperHeight={210}
                    wrapperBackground={thameColors.white}
                    itemHeight={60}
                    highlightColor={thameColors.gray}
                    highlightBorderWidth={0.5}
                    activeItemColor={thameColors.textBlack}
                    itemColor={thameColors.orange}
                  />
                </View>
              </Col>
            </Grid>
            <Grid style={styles.buttonDuration}>
              <Col>
                <Button label="Done" onClick={this._setNullModal} />
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* Modal, versi Room */}

        {/* ========= MODAL LOADING ========= */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 99}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={250}
          animationOutTiming={250}
          backdropTransitionInTiming={250}
          backdropTransitionOutTiming={250}
        >
          {this.loadingBooking()}
        </Modal>
        {/* ========= MODAL LOADING ========= */}

        {/* ========= Alert Modal ========= */}
        <AlertModal
          type="normal"
          isVisible={this.state.visibleModal === 999}
          title="Alert"
          contentText="Choose your destination first"
          onPress={this._setNullModal}
          onDismiss={this._setNullModal}
        />
        {/* ========= Alert Modal ========= */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    hotelLoading: state.hotel.fetchingHotel,
    listHotelCity: makeListCity(state),
    listDestinationBeds: makeListDestinationBeds(state),
    loadingCity: state.hotel.fetchingHotelCity,
    loadingDestinationBeds: state.hotel.fetchingDestinationBeds,
    // Login
    isLogin: state.profile.isLogin,
    profile: makeDataProfile(state),
    token: state.profile.token,
  };
}

export default connect(mapStateToProps)(SearchHotel);
