import React, { PureComponent } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Animated,
} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { Icon } from 'react-native-elements';
import { Grid, Col } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { thameColors, asitaColor } from '../../../base/constant';
import Carousel from '../Component/Carousel';
import { facilities } from '../dummyHotel';
import HeaderTransparent from '../Component/HeaderTransparent';
import { scale } from '../../../Const/ScaleUtils';
import DetailFacilities from './Component/DetailFacilities';
import DetailFooter from '../Component/DetailFooter';
import BarTranslucent from '../../../elements/BarTranslucent';
import {
  makeHotelDetailPayload,
  makeSearchPayload,
  makeHotelDetailBeds,
  makeSearchHotelBeds,
} from '../../../redux/selectors/HotelSelector';
import { starMap } from '../../../utilities/helpers';
import styles from './Component/styles';
import ViewHide from '../../../components/ViewHide';

// Animation Header
const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 70;

class HotelDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      emptyImage:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXz9Pa5vsq2u8jN0dnV2N/o6u7FydPi5Onw8fS+ws3f4ee6v8v29/jY2+Hu7/Ly9PbJztbQ1dxJagBAAAAC60lEQVR4nO3b2ZaCMBREUQbDJOP//2wbEGVIFCHKTa+zH7uVRVmBBJQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCpdOzvQQqaq2KmuSrOzQ02lSeRem8rpsQq/ozg72Kj4UkAxEev8awnzs7P1yiIadsfpQXjfZCHhUCzbfmeurdNz6bDRsBWRsB+k0cXxdHjpa0wkTBn3hKnjzRZyEgYk3IeEv2RKWCt1cN9EJ0zjfm7Mq/rAVgUnbLpwnK/zA2tnuQmzJHquuqJq91blJuwmAW8rHbV3q2ITFrOAt7Xz3l2UmrBMlpcHe9fOUhOqRYVhFO/cqtSEy0H6bh/tJ1uhCctqlTB/NSnG9pOt1ISXjxLq825laVFowo9GaRPrF9talJqw3n6macaZ09yi1ISG2cLyriwePwxzi1ITru4s2naxma59TC2KTRjE83FqmQ6yeDaUDS3KTRhMV96h5TTSLD4HQ4uCE9bxePUU5pYL/3mD5o9CcMKgTONc39NNLrV5iK4aNLUoOWHQ38RQtW3nsm6db92i8ISvGBtct+hvwqyzBFxE9DehrcHlQPU1YWNvcNGirwlfNThv0ZOE9eJG1OsGZy36kVBdczU9e7RvAz5b9CFhqfIwSp4XwG+OwUWLPiRUV/33Z4tbGtTvGK635CfUDfb/SO5rt20N9t8m65fLT9g3GD5abDY2qC+lvEg4NjhEvLW4tUFvEj4a7OXq3TzoW8Jpg0PEzfk8SThv8EMeJFw1+O8SHmrQg4QHG/Qg4cEGxSc83KD4hIcblJ6w3L508TXh+vtDEpLw3GwDEpKQhOdznVD2fRr9tdpRw/1HqQndIeEvkXCXUlDC+1NBndsnge/fwyVnp9PGH3p95dm1WMKza4/fI37j+UPXR/c+2X9/hjQI0uO3LsyuMioM9A8Sjy/W1iIhY7Sn2tzpUahdWyXiNDNSxcWtSlCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAwCn+AEXGNosxDBhFAAAAAElFTkSuQmCC',
      about: true,
    };
  }

  // Function
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  navigateTo = route => {
    const { navigation } = this.props;
    // const payload = {
    //   check_in: searchPayload.check_in,
    //   check_out: searchPayload.check_out,
    //   rate_types: ['PAY_NOW'],
    //   room_group: {
    //     rooms: [
    //       {
    //         num_of_adults: searchPayload.room_group.rooms[0].num_of_adults,
    //       },
    //     ],
    //   },
    //   metadata: {
    //     customer_session_id: searchPayload.metadata.customer_session_id,
    //     customer_ipaddress: searchPayload.metadata.customer_ipaddress,
    //     customer_user_agent: searchPayload.metadata.customer_user_agent,
    //     locale: searchPayload.metadata.locale,
    //     currency_code: searchPayload.metadata.currency_code,
    //     client_interface: searchPayload.metadata.client_interface,
    //   },
    // };
    // dispatch(
    //   actionSelectRoomHotel(
    //     payload,
    //     isLogin ? token : '',
    //     detailPayload.property_ids[0]
    //   )
    // );
    navigation.navigate(route);
  };

  _selectRoom = () =>
    requestAnimationFrame(() => this.navigateTo('SelectRoomHotel'));

  showAbout = () => {
    const { about } = this.state;
    requestAnimationFrame(() => {
      this.setState({ about: !about });
    });
  };

  // Main Render
  render() {
    const { container, textBold, textSemiBold, textRegular } = styles;
    const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
    const headerHeight = this.state.scrollY.interpolate({
      // Animate the header
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });
    const headerOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 70],
      extrapolate: 'clamp',
    });

    const { hotelDetail, hotelResultBeds } = this.props;
    return (
      <View style={container}>
        <BarTranslucent />
        {/* Header when scrolled Down */}
        <Animated.View
          style={[styles.animateHeader, { height: headerOpacity }]}
        >
          <View style={styles.buttonBack}>
            <TouchableOpacity onPress={this.goBack}>
              <Icon
                name="ios-arrow-round-back"
                color={thameColors.textBlack}
                size={42}
                type="ionicon"
              />
            </TouchableOpacity>
            <Text style={textBold}>
              {hotelDetail.name.length > 25
                ? `${hotelDetail.name.substring(0, 25)}...`
                : hotelDetail.name}
            </Text>
            <TouchableOpacity>
              <Icon
                name="dots-vertical"
                color={thameColors.textBlack}
                type="material-community"
              />
            </TouchableOpacity>
          </View>
        </Animated.View>
        {/* Header when scrolled Down */}
        <ScrollView
          indicatorStyle="white"
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={Animated.event([
            {
              nativeEvent: { contentOffset: { y: this.state.scrollY } },
            },
          ])}
        >
          <Carousel
            imgStyle={{ backgroundColor: thameColors.gray }}
            pathImg={hotelResultBeds.pathAsset}
            dataImage={
              hotelDetail.detail.images.length !== 0
                ? hotelDetail.detail.images
                : [this.state.emptyImage]
            }
          />
          <HeaderTransparent
            style={{ height: headerHeight }}
            onLeftPress={this.goBack}
            {...this.props}
          />
          {/* Title */}
          <View>
            <View style={styles.titleContent}>
              <Grid>
                <Col size={9}>
                  <Text style={textBold}>{hotelDetail.name}</Text>
                  <View style={styles.titleStar}>
                    {starMap(hotelDetail.categoryName.substr(0, 1))}
                    <Text style={[textSemiBold, styles.left10]}>
                      {hotelDetail.detail.accommodationTypeCode}
                    </Text>
                  </View>
                </Col>
                <Col size={1}>
                  <Icon
                    type="feather"
                    name="bookmark"
                    color={thameColors.textBlack}
                    size={30}
                  />
                </Col>
              </Grid>
            </View>
          </View>
          {/* Title */}

          {/* Facilities */}
          <View style={styles.margin}>
            <Text style={[textBold, { color: asitaColor.tealBlue }]}>
              Facilities
            </Text>
            <DetailFacilities data={facilities} />
          </View>
          {/* Facilities */}
          {/* About */}
          <TouchableOpacity style={styles.buttonAbout} onPress={this.showAbout}>
            <Text style={[textRegular, styles.fontSize16]}>
              About This Hotel
            </Text>
            <Icon
              type="ionicon"
              name={this.state.about ? 'ios-arrow-down' : 'ios-arrow-up'}
              color={thameColors.textBlack}
              size={25}
              iconStyle={styles.right5}
            />
          </TouchableOpacity>
          <ViewHide hide={this.state.about} style={styles.buttonAbout}>
            <Text>
              {hotelDetail.detail.description.content
                ? hotelDetail.detail.description.content
                : 'Best Hotel in Jakarta'}
            </Text>
          </ViewHide>
          {/* About */}
          {/* Location */}
          <View style={styles.contentLocation}>
            <View style={styles.padding}>
              <Text style={[textBold, { color: asitaColor.tealBlue }]}>
                Location
              </Text>
              <View style={styles.iconLocation}>
                <Icon
                  type="material"
                  name="location-on"
                  color={thameColors.gray}
                  size={20}
                  iconStyle={styles.right5}
                />
                <Text style={[textRegular, { color: thameColors.gray }]}>
                  {`${
                    hotelDetail.detail.address.content !== null
                      ? hotelDetail.detail.address.content
                      : 'Jl. Semanggi'
                  } - ${hotelDetail.zoneName}`}
                </Text>
              </View>
            </View>
            {/* <Image source={require('../../../assets/attraction/maps.png')} style={{ width: '100%', height: verticalScale(250) }} /> */}
            <MapView
              region={{
                latitude: hotelDetail.detail.coordinates.latitude
                  ? hotelDetail.detail.coordinates.latitude
                  : -6.21462,
                longitude: hotelDetail.detail.coordinates.longitude
                  ? hotelDetail.detail.coordinates.longitude
                  : 106.84513,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              provider={PROVIDER_GOOGLE}
              style={styles.mapStyle}
            >
              <Marker
                coordinate={{
                  latitude: hotelDetail.detail.coordinates.latitude
                    ? hotelDetail.detail.coordinates.latitude
                    : -6.21462,
                  longitude: hotelDetail.detail.coordinates.longitude
                    ? hotelDetail.detail.coordinates.longitude
                    : 106.84513,
                }}
              />
            </MapView>
            <View style={styles.buttonMap}>
              <View style={styles.alignCenter}>
                <Image
                  source={require('../../../assets/icons/direction_blue_icon.png')}
                  style={{ width: scale(50), height: scale(50) }}
                  resizeMode="center"
                />
                <Text style={[textSemiBold, styles.top10]}>
                  Show Directions
                </Text>
              </View>
              <View style={styles.alignCenter}>
                <Image
                  source={require('../../../assets/icons/location_blue_icon.png')}
                  style={{ width: scale(50), height: scale(50) }}
                  resizeMode="center"
                />
                <Text style={[textSemiBold, styles.top10]}>Show Map</Text>
              </View>
            </View>
          </View>
          {/* Location */}
        </ScrollView>
        <DetailFooter
          label="SELECT ROOM"
          price={hotelDetail.minRate}
          currency={'Rp.'}
          active
          onPress={this._selectRoom}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    hotelDetail: makeHotelDetailBeds(state),
    searchPayload: makeSearchPayload(state),
    detailPayload: makeHotelDetailPayload(state),
    hotelResultBeds: makeSearchHotelBeds(state),
    isLogin: state.profile.isLogin,
    token: state.profile.token,
  };
}

export default connect(mapStateToProps)(HotelDetail);
