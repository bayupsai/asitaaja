import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { scale } from '../../../../Const/ScaleUtils';
import { thameColors } from '../../../../base/constant';

const DetailFacilities = props => {
  return (
    <View>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {props.data.map((item, index) => (
          <View
            key={index}
            style={{
              width: scale(75),
              alignItems: 'center',
              marginTop: 10,
              marginHorizontal: 3,
            }}
          >
            <Image
              source={item.img}
              style={{ width: scale(40), height: scale(40) }}
            />
            <Text style={{ color: thameColors.textBlack, textAlign: 'center' }}>
              {item.title}
            </Text>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

export default DetailFacilities;
