import { StyleSheet } from 'react-native';
import {
  thameColors,
  fontBold,
  fontSemiBold,
  fontReguler,
} from '../../../../base/constant';
import { verticalScale } from '../../../../Const/ScaleUtils';

const styles = StyleSheet.create({
  margin: { margin: 20 },
  padding: { padding: 20 },
  left10: { marginLeft: 10 },
  right5: { marginRight: 5 },
  top10: { marginTop: 10 },
  fontSize16: { fontSize: 16 },
  alignCenter: { alignItems: 'center' },
  container: {
    flex: 1,
    backgroundColor: thameColors.white,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 20,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  // Animate Header
  animateHeader: {
    backgroundColor: thameColors.white,
    borderBottomColor: thameColors.gray,
    borderBottomWidth: 0.5,
  },
  buttonBack: {
    margin: 20,
    flexDirection: 'row',
    paddingTop: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  // Title
  titleContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 20,
  },
  titleStar: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  // About
  buttonAbout: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    padding: 20,
    borderColor: thameColors.buttonGray,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
  },
  // Location
  contentLocation: {
    width: '100%',
    borderColor: thameColors.buttonGray,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    margin: 0,
  },
  iconLocation: { flexDirection: 'row', marginTop: 5 },
  mapStyle: { width: '100%', height: verticalScale(250) },
  buttonMap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    transform: [{ translateY: -40 }],
  },
});

export default styles;
