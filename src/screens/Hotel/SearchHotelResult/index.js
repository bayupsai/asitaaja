import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import {
  Placeholder,
  PlaceholderLine,
  Fade,
  PlaceholderMedia,
} from 'rn-placeholder';
import Modal from 'react-native-modal';
import numeral from 'numeral';

// redux
import { connect } from 'react-redux';
import { chooseHotel } from '../../../redux/actions/HotelAction';

// import components
import HeaderPageResults from '../../../components/HeaderPageResults';
import Card from './Component/Card';
import { fontExtraBold, thameColors } from '../../../base/constant';
import { ButtonRounded } from '../../../elements/Button';
import { scale, verticalScale } from '../../../Const/ScaleUtils';
import AlertModal from '../../../components/AlertModal';
import {
  makeSearchHotelResult,
  makeSearchHotelBeds,
} from '../../../redux/selectors/HotelSelector';
import styles from './Component/styles';

const window = Dimensions.get('window');
const deviceHeight = window.height;
const deviceWidth = window.width;

class SearchHomeResult extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      arrayDummy: [1, 2, 3, 4, 5],
      errorMessage: '',
      emptyImage:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXz9Pa5vsq2u8jN0dnV2N/o6u7FydPi5Onw8fS+ws3f4ee6v8v29/jY2+Hu7/Ly9PbJztbQ1dxJagBAAAAC60lEQVR4nO3b2ZaCMBREUQbDJOP//2wbEGVIFCHKTa+zH7uVRVmBBJQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCpdOzvQQqaq2KmuSrOzQ02lSeRem8rpsQq/ozg72Kj4UkAxEev8awnzs7P1yiIadsfpQXjfZCHhUCzbfmeurdNz6bDRsBWRsB+k0cXxdHjpa0wkTBn3hKnjzRZyEgYk3IeEv2RKWCt1cN9EJ0zjfm7Mq/rAVgUnbLpwnK/zA2tnuQmzJHquuqJq91blJuwmAW8rHbV3q2ITFrOAt7Xz3l2UmrBMlpcHe9fOUhOqRYVhFO/cqtSEy0H6bh/tJ1uhCctqlTB/NSnG9pOt1ISXjxLq825laVFowo9GaRPrF9talJqw3n6macaZ09yi1ISG2cLyriwePwxzi1ITru4s2naxma59TC2KTRjE83FqmQ6yeDaUDS3KTRhMV96h5TTSLD4HQ4uCE9bxePUU5pYL/3mD5o9CcMKgTONc39NNLrV5iK4aNLUoOWHQ38RQtW3nsm6db92i8ISvGBtct+hvwqyzBFxE9DehrcHlQPU1YWNvcNGirwlfNThv0ZOE9eJG1OsGZy36kVBdczU9e7RvAz5b9CFhqfIwSp4XwG+OwUWLPiRUV/33Z4tbGtTvGK635CfUDfb/SO5rt20N9t8m65fLT9g3GD5abDY2qC+lvEg4NjhEvLW4tUFvEj4a7OXq3TzoW8Jpg0PEzfk8SThv8EMeJFw1+O8SHmrQg4QHG/Qg4cEGxSc83KD4hIcblJ6w3L508TXh+vtDEpLw3GwDEpKQhOdznVD2fRr9tdpRw/1HqQndIeEvkXCXUlDC+1NBndsnge/fwyVnp9PGH3p95dm1WMKza4/fI37j+UPXR/c+2X9/hjQI0uO3LsyuMioM9A8Sjy/W1iIhY7Sn2tzpUahdWyXiNDNSxcWtSlCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAwCn+AEXGNosxDBhFAAAAAElFTkSuQmCC',
    };
  }

  goBack = () => this.props.navigation.goBack();

  // Modal
  _toggleModal = id => this.setState({ visibleModal: id });

  _setNullModal = () => this.setState({ visibleModal: null });
  // Modal

  // Flatlist Conf
  _keyExtractor = (item, i) => i.toString();

  _renderLoading = i => (
    <View key={i} style={[styles.card, styles.padding]}>
      <Placeholder Animation={Fade}>
        <View style={styles.rowDirection}>
          <PlaceholderMedia
            style={{ width: scale(100), height: verticalScale(100) }}
          />
          <PlaceholderLine width={scale(30)} height={verticalScale(25)} />
        </View>
      </Placeholder>
    </View>
  );

  emptyResults = () => (
    <View style={[styles.flex, styles.center]}>
      <View>
        <Image
          resizeMode="center"
          style={{ height: deviceHeight / 2.5, width: deviceWidth - 40 }}
          source={require('../../../assets/icons/search_not_found.png')}
        />
      </View>
      <View style={styles.topCenter}>
        <Text style={styles.textBold}>No Hotels Available</Text>
      </View>
      <View style={styles.contentEmpty}>
        <Text style={styles.textEmpty}>Please search for another date</Text>
        <Text style={styles.textEmpty}>
          click the Change Search button below
        </Text>
      </View>
      <View style={styles.top30}>
        <ButtonRounded onClick={this.goBack} label="CHANGE SEARCH" />
      </View>
    </View>
  );

  _renderItem = ({ item, index }) => {
    const pathHotel =
      this.props.hotelResultBeds !== null
        ? this.props.hotelResultBeds.pathAsset
        : '';
    return (
      <Card
        key={index}
        img={
          item.detail.images !== null ||
          item.detail.images.length > 0 ||
          item.detail.images.length !== undefined
            ? `${pathHotel}${item.detail.images[0].path}`
            : this.state.emptyImage
        }
        title={item.name}
        location={`${item.zoneName}`}
        totalStar={item.categoryName.substr(0, 1)}
        price={numeral(parseInt(item.minRate, 14) * 14000)
          .format('0,00')
          .replace(/,/g, '.')}
        currency={'Rp.'}
        onPress={() => requestAnimationFrame(() => this._detail(item))}
      />
    );
  };

  // Flatlist Conf

  _detail = item => {
    const { navigation, dispatch } = this.props;
    dispatch(chooseHotel(item));
    navigation.navigate('HotelDetail');
    // const payloadData = {
    //   locale: 'id_ID',
    //   property_ids: [item.property_id],
    //   options: [],
    // };
    // dispatch(actionHotelDetail(payloadData, isLogin ? token : ''))
    //   .then(res => {
    //     if (res.type === 'HOTEL_DETAIL_SUCCESS') {
    //       navigation.navigate('HotelDetail');
    //     } else {
    //       this.setState({ errorMessage: res.message }, () =>
    //         this._toggleModal(99)
    //       );
    //     }
    //   })
    //   .catch(err => {
    //     this.setState({ errorMessage: err.message }, () =>
    //       this._toggleModal(99)
    //     );
    //   });
  };

  // Main Render
  render() {
    const { hotelResultBeds, loadingBeds } = this.props;
    return (
      <View style={styles.container}>
        <HeaderPageResults
          page="hotel"
          hotel={this.props.navigation.state.params.payloadHotel}
          cityName={this.props.navigation.state.params.keywords}
          callback={this.goBack}
        />

        <View style={styles.subHeader}>
          <Text style={styles.textSubHeader}>
            {loadingBeds
              ? 'Please Wait for Best Hotel for You'
              : hotelResultBeds.hotels !== null
              ? hotelResultBeds.hotels.length > 0 &&
                hotelResultBeds.hotels !== undefined
                ? `Available ${
                    hotelResultBeds.hotels.length > 0 &&
                    hotelResultBeds.hotels.length !== undefined
                      ? hotelResultBeds.hotels.length
                      : '-'
                  } Hotels for You`
                : 'There is No Hotel for you'
              : 'There is No Hotel for You '}
          </Text>
        </View>

        <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
          <View style={[styles.content, styles.margin]}>
            {loadingBeds ? (
              this.state.arrayDummy.map((__, index) =>
                this._renderLoading(index)
              )
            ) : (
              <FlatList
                data={
                  hotelResultBeds.hotels !== null
                    ? hotelResultBeds.hotels.length > 0 &&
                      hotelResultBeds.hotels.length !== undefined
                      ? hotelResultBeds.hotels
                      : new Array(0)
                    : new Array(0)
                }
                renderItem={this._renderItem}
                showsVerticalScrollIndicator={false}
                keyExtractor={this._keyExtractor}
                ListEmptyComponent={this.emptyResults()}
                getItemLayout={(item, index) => ({
                  length: 5,
                  offset: 5 * index,
                  index,
                })}
                initialNumToRender={8}
                maxToRenderPerBatch={2}
                onEndReachedThreshold={0.5}
              />
            )}
          </View>
        </ScrollView>

        {/* ========================== FAB ========================== */}
        {/* <Grid style={{ position: "absolute", bottom: 10, borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: "transparent", width: deviceWidth }}>
                    <Col style={{ alignItems: 'center' }}>
                        <TouchableOpacity style={{ flex: 0 }} onPress={() => alert('filter')}>
                            <Grid style={{ borderRadius: 20, backgroundColor: thameColors.secondary, padding: 10, width: deviceWidth / 3.5, alignItems: "center" }}>
                                <Col>
                                    <Icon name="filter" color={thameColors.white} type='font-awesome' size={18} />
                                </Col>
                                <Col>
                                    <Text style={{ color: thameColors.white, fontSize: 16, fontWeight: "bold" }}>Filter</Text>
                                </Col>
                            </Grid>
                        </TouchableOpacity>
                    </Col>
                </Grid> */}
        {/* ========================== FAB ========================== */}

        {/* ========================== MODAL FILTER ========================== */}
        <Modal
          style={styles.modalFilter}
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.visibleModal === 1}
        >
          <Grid style={styles.header}>
            <Col style={styles.headerLeft}>
              <TouchableOpacity onPress={() => this.closeFilter('normal')}>
                <Text style={styles.closeButton}>CLOSE</Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.alignCenter}>
              <Text style={styles.titleOrderFilter}>FILTER</Text>
            </Col>
            <Col style={styles.headerRight}>
              <TouchableOpacity onPress={() => this.resetFilter('all')}>
                <Text style={styles.resetButton}>RESET</Text>
              </TouchableOpacity>
            </Col>
          </Grid>
          {/* <Grid style={styles.content}>
                        <Col style={{ padding: 20 }}>
                            { this.renderFilter() }
                        </Col>
                    </Grid> */}
          <Grid style={styles.modalButton}>
            <Col style={styles.alignCenter}>
              <View style={styles.btnFilter}>
                <TouchableOpacity onPress={this._setNullModal}>
                  <Text
                    style={{
                      color: thameColors.white,
                      fontFamily: fontExtraBold,
                    }}
                  >
                    DONE
                  </Text>
                </TouchableOpacity>
              </View>
            </Col>
          </Grid>
        </Modal>
        {/* ========================== MODAL FILTER ========================== */}

        {/* ========================== Modal Alert ========================== */}
        <AlertModal
          isVisible={this.state.visibleModal === 99}
          onDismiss={this._setNullModal}
          onPress={this._setNullModal}
          type="normal"
          title="Warning"
          contentText={this.state.errorMessage}
        />
        {/* ========================== Modal Alert ========================== */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    hotelResult: makeSearchHotelResult(state),
    hotelResultBeds: makeSearchHotelBeds(state),
    loadingBeds: state.hotel.fetchingHotelBeds,
    loading: state.hotel.fetchingHotel,
    isLogin: state.profile.isLogin,
    token: state.profile.token,
  };
}

export default connect(mapStateToProps)(SearchHomeResult);
