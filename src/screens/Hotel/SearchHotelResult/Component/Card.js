import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';

// redux
import { connect } from 'react-redux';
import {
  fontReguler,
  thameColors,
  fontBold,
  asitaColor,
} from '../../../../base/constant';
import { scale } from '../../../../Const/ScaleUtils';
import { starMap } from '../../../../utilities/helpers';

class Card extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errImage:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXz9Pa5vsq2u8jN0dnV2N/o6u7FydPi5Onw8fS+ws3f4ee6v8v29/jY2+Hu7/Ly9PbJztbQ1dxJagBAAAAC60lEQVR4nO3b2ZaCMBREUQbDJOP//2wbEGVIFCHKTa+zH7uVRVmBBJQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCpdOzvQQqaq2KmuSrOzQ02lSeRem8rpsQq/ozg72Kj4UkAxEev8awnzs7P1yiIadsfpQXjfZCHhUCzbfmeurdNz6bDRsBWRsB+k0cXxdHjpa0wkTBn3hKnjzRZyEgYk3IeEv2RKWCt1cN9EJ0zjfm7Mq/rAVgUnbLpwnK/zA2tnuQmzJHquuqJq91blJuwmAW8rHbV3q2ITFrOAt7Xz3l2UmrBMlpcHe9fOUhOqRYVhFO/cqtSEy0H6bh/tJ1uhCctqlTB/NSnG9pOt1ISXjxLq825laVFowo9GaRPrF9talJqw3n6macaZ09yi1ISG2cLyriwePwxzi1ITru4s2naxma59TC2KTRjE83FqmQ6yeDaUDS3KTRhMV96h5TTSLD4HQ4uCE9bxePUU5pYL/3mD5o9CcMKgTONc39NNLrV5iK4aNLUoOWHQ38RQtW3nsm6db92i8ISvGBtct+hvwqyzBFxE9DehrcHlQPU1YWNvcNGirwlfNThv0ZOE9eJG1OsGZy36kVBdczU9e7RvAz5b9CFhqfIwSp4XwG+OwUWLPiRUV/33Z4tbGtTvGK635CfUDfb/SO5rt20N9t8m65fLT9g3GD5abDY2qC+lvEg4NjhEvLW4tUFvEj4a7OXq3TzoW8Jpg0PEzfk8SThv8EMeJFw1+O8SHmrQg4QHG/Qg4cEGxSc83KD4hIcblJ6w3L508TXh+vtDEpLw3GwDEpKQhOdznVD2fRr9tdpRw/1HqQndIeEvkXCXUlDC+1NBndsnge/fwyVnp9PGH3p95dm1WMKza4/fI37j+UPXR/c+2X9/hjQI0uO3LsyuMioM9A8Sjy/W1iIhY7Sn2tzpUahdWyXiNDNSxcWtSlCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAwCn+AEXGNosxDBhFAAAAAElFTkSuQmCC',
      imageCard: this.props.img,
    };
  }

  render() {
    return (
      <TouchableOpacity style={styles.card} onPress={this.props.onPress}>
        <Grid style={styles.cardBody}>
          <Col size={3}>
            <Image
              source={{ uri: this.state.imageCard }}
              style={styles.img}
              onError={() => this.setState({ imageCard: this.state.errImage })}
            />
          </Col>
          <Col size={5}>
            <Row size={8} style={styles.top10}>
              <Col>
                <Text style={styles.textTitle}>
                  {this.props.title.length > 25
                    ? `${this.props.title.substring(0, 25)}...`
                    : this.props.title}
                </Text>
                <View style={[styles.sectionStar]}>
                  <View style={styles.rowStar}>
                    {starMap(this.props.totalStar)}
                  </View>
                  <View style={styles.address}>
                    <Icon
                      type="material"
                      name="location-on"
                      color={thameColors.gray}
                    />
                    <Text
                      style={{
                        fontFamily: fontReguler,
                        color: thameColors.gray,
                      }}
                    >
                      {this.props.location
                        ? `${this.props.location}`
                        : '8,5 km'}
                    </Text>
                  </View>
                </View>
              </Col>
            </Row>

            <Row size={2} style={styles.bottom10}>
              <View style={styles.rowDirection}>
                <Text style={styles.textPrice}>
                  {this.props.currency} {this.props.price}
                  {/* {numeral(this.props.price)
                    .format('0,0')
                    .replace(/,/g, '.')} */}
                </Text>
                <Text style={styles.roomNightText}>/room/night</Text>
              </View>
            </Row>
          </Col>
        </Grid>
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state) {
  return {
    hotelLoad: state.hotel.fetchingHotel,
  };
}

export default connect(mapStateToProps)(Card);

const styles = StyleSheet.create({
  top10: { marginTop: 10 },
  bottom10: { marginBottom: 10 },
  rowDirection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  card: {
    backgroundColor: thameColors.white,
    width: '100%',
    height: 'auto',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    // marginLeft: 20,
    // marginRight: 20,
  },
  cardBody: {
    flex: 0,
  },
  cardFooter: {
    flex: 0,
    padding: 10,
    borderTopWidth: 1,
    borderColor: thameColors.gray,
    marginTop: 5,
  },
  img: {
    width: 105,
    height: 155,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  dropDown: {
    // paddingBottom: -10,
    alignItems: 'center',
    // position: 'absolute'
  },
  collapse: {
    position: 'absolute',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: "grey"
  },
  location: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  price: {
    alignItems: 'flex-end',
  },
  textPrice: {
    fontSize: 18,
    color: asitaColor.orange,
    fontFamily: fontBold,
  },
  textTitle: {
    fontSize: 20,
    color: thameColors.superBack,
    fontFamily: fontBold,
  },
  hotel: {
    borderColor: thameColors.lightGreen,
    borderRadius: 10,
    borderWidth: 1,
    width: 75,
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionStar: {
    flexDirection: 'row',
    marginBottom: 5,
    marginTop: 5,
  },
  star: { width: scale(17), height: scale(20), marginRight: 5 },
  rowStar: { flexDirection: 'row', width: '55%' },
  address: {
    flexDirection: 'row',
    marginRight: 5,
    width: '35%',
  },
  roomNightText: {
    fontFamily: fontReguler,
    alignItems: 'flex-end',
    color: thameColors.gray,
  },
});
