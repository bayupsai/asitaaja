import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Grid, Col } from 'react-native-easy-grid';
import { thameColors } from '../../../../base/constant';

const window = Dimensions.get('window');
const deviceHeight = window.height;
const deviceWidth = window.width;

const TitleSubPage = props => (
  <View style={styles.container}>
    <ImageBackground
      source={require('../../../../assets/icons/maps.png')}
      style={styles.imageBackground}
    >
      <Grid style={styles.grid}>
        <Col size={1}>
          <Icon
            name="circle"
            color={thameColors.white}
            size={10}
            style={styles.iconSpace}
          />
        </Col>
        <Col size={8}>
          <Text style={[styles.textWhite]}>Destination</Text>
          <Text style={styles.pageTitle}>{props.title}</Text>
          {/* <Row>
                        <Col>
                            <Text style={styles.textWhite}>
                                {props.date}
                                <Icon name="circle" color={thameColors.white} size={5} style={styles.iconSpace} />
                                <Text style={styles.textWhite}>{props.detail}</Text>
                            </Text>
                        </Col>
                    </Row> */}
        </Col>
      </Grid>
    </ImageBackground>
  </View>
);
const SubHeaderOnly = props => (
  <TitleSubPage title={props.title} date={props.date} detail={props.detail} />
);

export default SubHeaderOnly;

const styles = StyleSheet.create({
  imageBackground: {
    height: deviceHeight / 4,
    width: deviceWidth,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  container: {
    backgroundColor: thameColors.primary,
    height: deviceHeight / 4,
    width: deviceWidth,
    borderBottomLeftRadius: 50,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 20,
    paddingTop: 3,
  },
  pageTitle: {
    color: thameColors.white,
    fontSize: 24,
    fontWeight: '400',
  },
  textWhite: {
    color: thameColors.white,
    fontSize: 12,
  },
  grid: {
    paddingTop: 25,
    marginLeft: 20,
  },
  iconSpace: {
    // marginLeft: 20,
    marginRight: 20,
  },
});
