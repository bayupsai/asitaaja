import { StyleSheet, Dimensions } from 'react-native';
import { thameColors, fontBold, fontReguler } from '../../../../base/constant';

const window = Dimensions.get('window');
const deviceWidth = window.width;

const styles = StyleSheet.create({
  margin: { margin: 3 },
  padding: { padding: 10 },
  flex: { flex: 1 },
  top30: { marginTop: 30 },
  alignCenter: { alignItems: 'center' },
  topCenter: { paddingTop: 10, alignItems: 'center' },
  rowDirection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  content: {
    padding: 1,
    marginLeft: 10,
    marginRight: 20,
  },
  card: {
    backgroundColor: thameColors.white,
    width: '100%',
    height: 'auto',
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  textBold: {
    color: thameColors.textBlack,
    fontFamily: fontBold,
    fontSize: 22,
  },

  // Empty Result
  contentEmpty: {
    paddingTop: 20,
    paddingHorizontal: 30,
    alignItems: 'center',
  },
  textEmpty: {
    color: thameColors.textBlack,
    fontFamily: fontReguler,
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
  },

  // Sub Header
  subHeader: {
    backgroundColor: thameColors.backWhite,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: -20,
    padding: 10,
    paddingBottom: 10,
  },
  textSubHeader: {
    color: thameColors.superBack,
    fontSize: 14,
    fontFamily: fontBold,
  },

  // Modal Filter
  modalFilter: { flex: 1, backgroundColor: thameColors.white, margin: 0 },
  modalButton: {
    position: 'absolute',
    bottom: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: deviceWidth,
  },
  btnFilter: {
    backgroundColor: thameColors.secondary,
    padding: 10,
    width: deviceWidth / 4,
    alignItems: 'center',
    borderRadius: 10,
  },
});

export default styles;
