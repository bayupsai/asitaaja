import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import numeral from 'numeral';
import {
  thameColors,
  fontBold,
  fontSemiBold,
  fontReguler,
} from '../../../../base/constant';

class PriceDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      payRoom: [1],
    };
  }
  renderRoom = (item, index) => (
    <View
      key={index}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Icon type="entypo" name="dot-single" color={thameColors.darkGray} />
        <Text style={[styles.textRegular, { color: thameColors.darkGray }]}>
          Night {index + 1} x 1 Room
        </Text>
      </View>
      <Text style={[styles.textRegular, { color: thameColors.darkGray }]}>
        Rp
        {numeral(this.props.roomHotel.rates.chargeable_rate.total)
          .format('0,0')
          .replace(/,/g, '.')}
      </Text>
    </View>
  );
  totalPayment = (value, total) => {
    return value * total;
  };

  //Main Render
  render() {
    const { container, textBold, textRegular } = styles;
    const { payRoom } = this.state;
    let { roomHotel } = this.props;
    return (
      <View title="Price Details">
        <View style={container}>
          <Text style={textBold}>Fee & Tax Details</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginVertical: 20,
            }}
          >
            <Text style={[textRegular, { fontSize: 18 }]}>
              {roomHotel.roomTitle} Room
            </Text>
            <Text style={[textRegular, { fontSize: 18 }]}>
              Rp
              {numeral(
                this.totalPayment(
                  roomHotel.rates.chargeable_rate.total,
                  payRoom.length
                )
              )
                .format('0,0')
                .replace(/,/g, '.')}
            </Text>
          </View>
          {payRoom.map((item, index) => this.renderRoom(item, index))}
        </View>
        <View style={{ borderTopWidth: 0.5, borderTopColor: thameColors.gray }}>
          <View
            style={[
              container,
              {
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              },
            ]}
          >
            <Text style={[textRegular, { fontSize: 18 }]}>Total Payment</Text>
            <Text style={[textBold, { color: thameColors.primary }]}>
              Rp
              {numeral(
                this.totalPayment(
                  roomHotel.rates.chargeable_rate.total,
                  payRoom.length
                )
              )
                .format('0,0')
                .replace(/,/g, '.')}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default PriceDetail;

const styles = StyleSheet.create({
  container: {
    margin: 20,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 20,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
});
