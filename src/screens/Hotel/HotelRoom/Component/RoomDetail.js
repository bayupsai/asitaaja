import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  fontBold,
  fontReguler,
  fontSemiBold,
  thameColors,
} from '../../../../base/constant';
import { Icon } from 'react-native-elements';
import { scale } from '../../../../Const/ScaleUtils';
import _ from 'lodash';
import { roomFacilities } from '../../dummyHotel';

class RoomDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dummy: roomFacilities,
    };
  }
  renderFacilities = (item, index) => {
    return (
      <View
        key={index}
        style={{ flexDirection: 'row', alignItems: 'center', marginRight: 15 }}
      >
        <Image
          source={item.img}
          style={styles.imgIcon}
          resizeMode={'contain'}
        />
        <Text style={[styles.textRegular, { marginLeft: 10 }]}>
          {item.title}
        </Text>
      </View>
    );
  };

  //Main Render
  render() {
    const { container, textBold, textRegular } = styles;
    const { dummy } = this.state;
    return (
      <View title="Room Details">
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 10 }}
        >
          <View style={container}>
            <Text style={textBold}>{this.props.roomHotel.roomTitle}</Text>
            <View
              style={{ marginTop: 10, flexDirection: 'row', marginBottom: 10 }}
            >
              {dummy.map((item, index) => this.renderFacilities(item, index))}
            </View>
          </View>

          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              padding: 20,
              borderColor: thameColors.buttonGray,
              borderTopWidth: 0.5,
              borderBottomWidth: 0.5,
            }}
          >
            <Text style={[textRegular, { fontSize: 16 }]}>Room Overview</Text>
            <Icon
              type="ionicon"
              name="ios-arrow-down"
              color={thameColors.textBlack}
              size={25}
              iconStyle={{ marginRight: 5 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              padding: 20,
              borderColor: thameColors.buttonGray,
              borderTopWidth: 0.5,
              borderBottomWidth: 0.5,
            }}
          >
            <Text style={[textRegular, { fontSize: 16 }]}>Room Facilities</Text>
            <Icon
              type="ionicon"
              name="ios-arrow-down"
              color={thameColors.textBlack}
              size={25}
              iconStyle={{ marginRight: 5 }}
            />
          </TouchableOpacity>
          <TouchableOpacity style={[container, { marginTop: 30 }]}>
            <View
              style={{
                width: '100%',
                paddingHorizontal: 20,
                paddingVertical: 10,
                backgroundColor: thameColors.semiGray,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderRadius: 10,
              }}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                  source={require('../../../../assets/icons/fileClose.png')}
                  style={{ width: scale(30), height: scale(35) }}
                  resizeMode={'contain'}
                />
                <Text style={[textBold, { fontSize: 18, marginLeft: 20 }]}>
                  Cancelation Policy
                </Text>
              </View>
              <Icon
                type="ionicon"
                name="ios-arrow-forward"
                color={thameColors.primary}
                size={25}
              />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

export default RoomDetail;

const styles = StyleSheet.create({
  container: {
    margin: 20,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 20,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  imgIcon: {
    width: scale(25),
    height: scale(25),
  },
});
