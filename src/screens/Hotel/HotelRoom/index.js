import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Animated,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { thameColors, fontBold } from '../../../base/constant';
import HeaderTransparent from '../Component/HeaderTransparent';
import Carousel from '../Component/Carousel';
import Tabs from '../Component/Tabs';
import RoomDetail from './Component/RoomDetail';
import PriceDetail from './Component/PriceDetail';
import DetailFooter from '../Component/DetailFooter';
import BarTranslucent from '../../../elements/BarTranslucent';
import { connect } from 'react-redux';
import { chooseHotelRoom } from '../../../redux/actions/HotelAction';
import { makeChooseRoom } from '../../../redux/selectors/HotelSelector';

//Animation Header
const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 60;

class HotelRoom extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      emptyImage:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXz9Pa5vsq2u8jN0dnV2N/o6u7FydPi5Onw8fS+ws3f4ee6v8v29/jY2+Hu7/Ly9PbJztbQ1dxJagBAAAAC60lEQVR4nO3b2ZaCMBREUQbDJOP//2wbEGVIFCHKTa+zH7uVRVmBBJQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCpdOzvQQqaq2KmuSrOzQ02lSeRem8rpsQq/ozg72Kj4UkAxEev8awnzs7P1yiIadsfpQXjfZCHhUCzbfmeurdNz6bDRsBWRsB+k0cXxdHjpa0wkTBn3hKnjzRZyEgYk3IeEv2RKWCt1cN9EJ0zjfm7Mq/rAVgUnbLpwnK/zA2tnuQmzJHquuqJq91blJuwmAW8rHbV3q2ITFrOAt7Xz3l2UmrBMlpcHe9fOUhOqRYVhFO/cqtSEy0H6bh/tJ1uhCctqlTB/NSnG9pOt1ISXjxLq825laVFowo9GaRPrF9talJqw3n6macaZ09yi1ISG2cLyriwePwxzi1ITru4s2naxma59TC2KTRjE83FqmQ6yeDaUDS3KTRhMV96h5TTSLD4HQ4uCE9bxePUU5pYL/3mD5o9CcMKgTONc39NNLrV5iK4aNLUoOWHQ38RQtW3nsm6db92i8ISvGBtct+hvwqyzBFxE9DehrcHlQPU1YWNvcNGirwlfNThv0ZOE9eJG1OsGZy36kVBdczU9e7RvAz5b9CFhqfIwSp4XwG+OwUWLPiRUV/33Z4tbGtTvGK635CfUDfb/SO5rt20N9t8m65fLT9g3GD5abDY2qC+lvEg4NjhEvLW4tUFvEj4a7OXq3TzoW8Jpg0PEzfk8SThv8EMeJFw1+O8SHmrQg4QHG/Qg4cEGxSc83KD4hIcblJ6w3L508TXh+vtDEpLw3GwDEpKQhOdznVD2fRr9tdpRw/1HqQndIeEvkXCXUlDC+1NBndsnge/fwyVnp9PGH3p95dm1WMKza4/fI37j+UPXR/c+2X9/hjQI0uO3LsyuMioM9A8Sjy/W1iIhY7Sn2tzpUahdWyXiNDNSxcWtSlCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAwCn+AEXGNosxDBhFAAAAAElFTkSuQmCC',
    };
  }
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };
  totalPayment = (value, length) => {
    return value * length;
  };
  navigateTo = route => this.props.navigation.navigate(route);

  bookRoom = () => {
    let { dispatch, roomHotel } = this.props;
    const { navigate } = this.props.navigation;
    dispatch(chooseHotelRoom(roomHotel));
    navigate('FormOrderHotel');
  };

  //Main Render
  render() {
    const { container, textBold } = styles;
    const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
    const headerHeight = this.state.scrollY.interpolate({
      // Animate the header
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });
    const headerOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 70],
      extrapolate: 'clamp',
    });
    let { roomHotel } = this.props;
    return (
      <View style={container}>
        <BarTranslucent />
        {/* Header when scrolled down */}
        <Animated.View
          style={{
            height: headerOpacity,
            backgroundColor: thameColors.white,
            borderBottomColor: thameColors.gray,
            borderBottomWidth: 0.5,
          }}
        >
          <View
            style={{
              margin: 20,
              flexDirection: 'row',
              paddingTop: 10,
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <TouchableOpacity onPress={this.goBack}>
              <Icon
                name="ios-arrow-round-back"
                color={thameColors.textBlack}
                size={42}
                type="ionicon"
              />
            </TouchableOpacity>
            <Text style={textBold}>{roomHotel.name}</Text>
            <TouchableOpacity>
              <Icon
                name="dots-vertical"
                color={thameColors.textBlack}
                type="material-community"
              />
            </TouchableOpacity>
          </View>
        </Animated.View>
        {/* Header when scrolled down */}
        <ScrollView
          indicatorStyle={'white'}
          showsVerticalScrollIndicator={false}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: this.state.scrollY } } },
          ])}
        >
          <Carousel
            imgStyle={{ backgroundColor: thameColors.primary }}
            dataImage={
              roomHotel.images.length > 0
                ? roomHotel.images
                : [this.state.emptyImage]
            }
          />
          <HeaderTransparent
            style={{ height: headerHeight }}
            onLeftPress={this.goBack}
            {...this.props}
          />
          <Tabs>
            <RoomDetail title="Room Details" {...this.props} />
            <PriceDetail title="Price Details" {...this.props} />
          </Tabs>
        </ScrollView>
        <DetailFooter
          price={this.totalPayment(roomHotel.rates.chargeable_rate.total, 1)}
          label="BOOK NOW"
          active={true}
          onPress={this.bookRoom}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    roomHotel: makeChooseRoom(state),
  };
}

export default connect(mapStateToProps)(HotelRoom);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.white,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 20,
  },
});
