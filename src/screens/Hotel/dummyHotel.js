const hotelNear = [
  {
    id: 1,
    img: require('../../assets/img/hotel3.jpg'),
    title: 'Fairmont Jakarta',
    star: 5,
    price: 2325000,
  },
  {
    id: 2,
    img: require('../../assets/img/hotelone.png'),
    title: 'Grand Mahakam Jakarta',
    star: 4,
    price: 1335000,
  },
  {
    id: 3,
    img: require('../../assets/img/hotelfour.png'),
    title: 'Hotel Mulia Jakarta',
    star: 5,
    price: 2325000,
  },
  {
    id: 4,
    img: require('../../assets/img/hotelthree.png'),
    title: 'Couler Hotel Jakarta',
    star: 3,
    price: 1005000,
  },
];
const facilities = [
  {
    img: require('../../assets/icons/swimming_icon_round.png'),
    title: 'Swimming Pool',
  },
  {
    img: require('../../assets/icons/restaurant_icon_round.png'),
    title: 'Restaurant',
  },
  {
    img: require('../../assets/icons/wifi_icon_round.png'),
    title: 'Free Wifi',
  },
  {
    img: require('../../assets/icons/parking_icon_round.png'),
    title: 'Parking Available',
  },
];
const roomFacilities = [
  { img: require('../../assets/icons/wifi_icon.png'), title: 'Free Wifi' },
  {
    img: require('../../assets/icons/restaurant_icon.png'),
    title: 'Breakfast',
  },
];
const dummyRoom = [
  {
    img: require('../../assets/img/hotelthree.png'),
    title: 'Premiere Deluxe',
    price: 350000,
  },
  {
    img: require('../../assets/img/hotel3.jpg'),
    title: 'Exclusive Deluxe',
    price: 500000,
  },
  {
    img: require('../../assets/img/hotelone.png'),
    title: 'Platinum Deluxe',
    price: 675000,
  },
];
const carousel = [
  { img: require('../../assets/img/hotelthree.png') },
  { img: require('../../assets/img/hotelthree.png') },
  { img: require('../../assets/img/hotelthree.png') },
  { img: require('../../assets/img/hotelthree.png') },
];

export { hotelNear, facilities, roomFacilities, dummyRoom, carousel };
