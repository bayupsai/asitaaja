import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
  Text,
  InteractionManager,
  Alert,
} from 'react-native';
import numeral from 'numeral';
import { DotIndicator } from 'react-native-indicators';
import { connect } from 'react-redux';
import { thameColors, fontReguler, fontSemiBold } from '../../../base/constant';
import HeaderPage from '../../../components/HeaderPage';
// import SubMenu from '../Component/SubMenu';
import { BarStyle } from '../../../elements/BarStyle';
import SubHeaderRoom from '../Component/SubHeaderRoom';
import CardRoom from '../Component/CardRoom';
import { scale } from '../../../Const/ScaleUtils';
// import { chooseHotelRoom } from '../../../redux/actions/HotelAction';
import {
  makeHotelDetailPayload,
  makeRoomHotel,
  makeRoomHotelPayload,
  makeHotelDetailBeds,
  makeSearchPayloadBeds,
  makeSearchHotelBeds,
} from '../../../redux/selectors/HotelSelector';
import Modal from 'react-native-modal';
import { chooseHotelRoom } from '../../../redux/actions/HotelAction';
import { groupingRoomImages } from '../../../utilities/helpers';

class SelectRoomHotel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showRoom: false,
      showDetail: false,
      roomType: [],
      selectedRoom: {},
      selectedRoomType: {},
      dataMenu: [
        { title: 'Free Wifi' },
        { title: 'Free Breakfast' },
        { title: 'Free Cancellation' },
      ],
      result: [],
      selectedMenu: null,
      changeValue: false,
      room: 1,
      guest:
        this.props.searchPayload.occupancies[0].adults +
        this.props.searchPayload.occupancies[0].children,
      emptyImage:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXz9Pa5vsq2u8jN0dnV2N/o6u7FydPi5Onw8fS+ws3f4ee6v8v29/jY2+Hu7/Ly9PbJztbQ1dxJagBAAAAC60lEQVR4nO3b2ZaCMBREUQbDJOP//2wbEGVIFCHKTa+zH7uVRVmBBJQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCpdOzvQQqaq2KmuSrOzQ02lSeRem8rpsQq/ozg72Kj4UkAxEev8awnzs7P1yiIadsfpQXjfZCHhUCzbfmeurdNz6bDRsBWRsB+k0cXxdHjpa0wkTBn3hKnjzRZyEgYk3IeEv2RKWCt1cN9EJ0zjfm7Mq/rAVgUnbLpwnK/zA2tnuQmzJHquuqJq91blJuwmAW8rHbV3q2ITFrOAt7Xz3l2UmrBMlpcHe9fOUhOqRYVhFO/cqtSEy0H6bh/tJ1uhCctqlTB/NSnG9pOt1ISXjxLq825laVFowo9GaRPrF9talJqw3n6macaZ09yi1ISG2cLyriwePwxzi1ITru4s2naxma59TC2KTRjE83FqmQ6yeDaUDS3KTRhMV96h5TTSLD4HQ4uCE9bxePUU5pYL/3mD5o9CcMKgTONc39NNLrV5iK4aNLUoOWHQ38RQtW3nsm6db92i8ISvGBtct+hvwqyzBFxE9DehrcHlQPU1YWNvcNGirwlfNThv0ZOE9eJG1OsGZy36kVBdczU9e7RvAz5b9CFhqfIwSp4XwG+OwUWLPiRUV/33Z4tbGtTvGK635CfUDfb/SO5rt20N9t8m65fLT9g3GD5abDY2qC+lvEg4NjhEvLW4tUFvEj4a7OXq3TzoW8Jpg0PEzfk8SThv8EMeJFw1+O8SHmrQg4QHG/Qg4cEGxSc83KD4hIcblJ6w3L508TXh+vtDEpLw3GwDEpKQhOdznVD2fRr9tdpRw/1HqQndIeEvkXCXUlDC+1NBndsnge/fwyVnp9PGH3p95dm1WMKza4/fI37j+UPXR/c+2X9/hjQI0uO3LsyuMioM9A8Sjy/W1iIhY7Sn2tzpUahdWyXiNDNSxcWtSlCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAwCn+AEXGNosxDBhFAAAAAElFTkSuQmCC',
    };
  }

  componentDidMount() {
    this.generateMenu();
  }

  generateMenu = () => {
    const initialChecl = this.state.dataMenu.map(() => false);
    this.setState({ result: initialChecl });
  };

  // Modal
  showRoom = () => {
    const { showRoom } = this.state;
    this.setState({ showRoom: !showRoom });
  };

  showDetail = () => {
    const { showDetail } = this.state;
    this.setState({ showDetail: !showDetail });
  };

  // Select Room by rate
  selecRoomType = item => {
    const { dispatch } = this.props;
    requestAnimationFrame(() => {
      this.setState({ selectedRoomType: item }, () => {
        const { showRoom, selectedRoomType, selectedRoom } = this.state;
        const payload = {
          room: selectedRoom,
          roomType: selectedRoomType,
        };
        dispatch(chooseHotelRoom(payload));
        this.setState({ showRoom: !showRoom }, () => {
          // Alert.alert('Alert', JSON.stringify(payload));
          this.navigateTo('FormOrderHotel');
          null;
        });
      });
    });
  };

  goBack = () => this.props.navigation.goBack();

  // Flatlist Conf
  _keyExtractor = (item, index) => index.toString();

  // Get Image under Room
  getImage = code => {
    const images = this.props.hotelDetail.detail.images;
    const hotelRoom = this.props.hotelDetail.rooms;
    const roomImage = groupingRoomImages(images, hotelRoom, code);
    return roomImage;
  };

  _renderRoomList = (item, index) => {
    // const { emptyImage } = this.state;
    const pathImg = this.props.hotelResultBeds.pathAsset;
    let theImage = pathImg + this.getImage(item.item.code);
    const onError = ({ nativeEvent: { error } }) => {
      theImage = this.state.emptyImage;
    };
    return (
      <CardRoom
        img={{ uri: theImage }}
        imgError={onError}
        title={item.item.name}
        // title={theImage}
        maxGuest={item.item.rates[0].allotment}
        price={parseInt(item.item.rates[0].net, 14) * 14000}
        currency={'Rp. '}
        key={index}
        onPress={() => this.getRoom(item, 'detail')}
        onPressDetail={() => this.getRoom(item, 'book')}
      />
    );
  };

  _emptyRoomList = () => (
    <View style={styles.alignCenter}>
      <Text
        style={{
          fontFamily: fontReguler,
          color: thameColors.textBlack,
        }}
      >
        Could not get any room for you
      </Text>
      <TouchableOpacity>
        <Text
          style={{
            fontFamily: fontReguler,
            color: thameColors.primary,
          }}
        >
          Try Again
        </Text>
      </TouchableOpacity>
    </View>
  );
  // Flatlist Conf
  handleSubMenu = (item, index) => {
    // let { dataMenu, result } = this.state
    // result[index] = !result[index]
    // this.setState({ result })
    // if (result[index] == true) {
    //     result.push(item.title)
    // } else {
    //     result.pop(item.title)
    // }
    this.setState({ selectedMenu: index });
  };

  changeValue = () => {
    // Open change function
    const { changeValue } = this.state;
    InteractionManager.runAfterInteractions(() => {
      this.setState({ changeValue: !changeValue });
    });
  };

  roomList = () => {
    const { changeValue, room, guest } = this.state;
    if (room > guest) {
      Alert.alert("Number of Rooms can't be more than number of Guests");
    } else {
      this.setState({ changeValue: !changeValue });
    }
  };

  // Change Room
  _roomMin = () => {
    const { room } = this.state;
    if (room !== 1) {
      this.setState({ room: room - 1 });
    }
  };

  _roomPlus = () => {
    const { room } = this.state;
    if (room !== 7) {
      this.setState({ room: room + 1 });
    } else {
      Alert.alert('Max Room is 7');
    }
  };

  _guestMin = () => {
    const { guest } = this.state;
    if (guest !== 1) {
      this.setState({ guest: guest - 1 });
    }
  };

  _guestPlus = () => {
    const { guest } = this.state;
    if (guest !== 7) {
      this.setState({ guest: guest + 1 });
    } else {
      Alert.alert('Max Guest is 7');
    }
  };
  // Change Room

  navigateTo = route => {
    const { navigate } = this.props.navigation;
    navigate(route);
  };

  getRoom = (item, type) => {
    this.showRoom();
    this.setState({ roomType: item.item.rates, selectedRoom: item.item });
    // const { dispatch } = this.props;
    // dispatch(chooseHotelRoom(item.item));
    // if (type === 'book') {
    //   this.navigateTo('HotelRoom');
    // } else if (type === 'detail') {
    //   this.navigateTo('FormOrderHotel');
    // }
  };

  render() {
    const { container, content } = styles;
    const { changeValue, room, guest } = this.state;
    const { fetchRoom, searchPayload, hotelDetail } = this.props;
    return (
      <View style={container}>
        <BarStyle />
        <HeaderPage
          title="Select Room"
          callback={this.goBack}
          left={changeValue}
          right={changeValue}
          leftComponent={
            changeValue === true ? (
              <TouchableOpacity onPress={this.changeValue} style={styles.left}>
                <Image
                  source={require('../../../assets/icons/close_icon.png')}
                  resizeMode="contain"
                  style={{ width: scale(20), height: scale(17.5) }}
                />
              </TouchableOpacity>
            ) : null
          }
          rightComponent={
            changeValue === true ? (
              <TouchableOpacity onPress={this.roomList} style={styles.left}>
                <Image
                  source={require('../../../assets/icons/check_icon.png')}
                  resizeMode="contain"
                  style={{ width: scale(20), height: scale(17.5) }}
                />
              </TouchableOpacity>
            ) : null
          }
        />
        <ScrollView>
          <SubHeaderRoom
            selected={changeValue}
            // onPress={this.changeValue}
            totalRoom={room}
            totalGuest={guest}
            {...this.props}
            startDate={searchPayload.stay.checkIn}
            endDate={searchPayload.stay.checkOut}
            minRoom={this._roomMin}
            plusRoom={this._roomPlus}
            minGuest={this._guestMin}
            plusGuest={this._guestPlus}
          />
          <View style={[content, styles.subContent]}>
            {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ paddingTop: 15, paddingLeft: 7 }}>
                            {
                                dataMenu.map((item, index) => (
                                    <SubMenu key={index} selected={index === this.state.selectedMenu ? true : false} label={item.title} style={index === 0 ? { marginLeft: 10 } : { position: 'relative' }}
                                        onPress={() => this.handleSubMenu(item, index)}
                                    />
                                ))
                            }
                        </ScrollView> */}
            {fetchRoom ? (
              <DotIndicator color={thameColors.primary} />
            ) : (
              <FlatList
                keyExtractor={this._keyExtractor}
                data={hotelDetail.rooms ? hotelDetail.rooms : new Array(0)}
                renderItem={this._renderRoomList}
                ListEmptyComponent={this._emptyRoomList}
                getItemLayout={(item, index) => ({
                  length: 10,
                  offset: 10 * index,
                  index,
                })}
              />
            )}
          </View>
        </ScrollView>

        {/* =========================== Modal =========================== */}
        {/* Modal Select Room */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.showRoom}
          style={styles.bottomModal}
          onBackButtonPress={this.showRoom}
          onBackdropPress={this.showRoom}
        >
          <View style={styles.showRoom}>
            <View style={styles.headerRoom}>
              <Text style={styles.textRoom}>Choose Room to continue</Text>
            </View>
            {this.state.roomType.map((item, index) => (
              <TouchableOpacity
                key={index}
                style={styles.btnRoom}
                onPress={() => this.selecRoomType(item)}
              >
                <Text style={styles.textRoom}>{item.boardName}</Text>
                <Text style={styles.textNote}>
                  <Text style={styles.textPrice}>
                    Rp.{' '}
                    {numeral(parseInt(item.net, 14) * 14000)
                      .format('0,00')
                      .replace(/,/g, '.')}
                  </Text>{' '}
                  | Max {item.allotment} Guest
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        </Modal>

        {/* Modal Detail Room */}
        <Modal
          useNativeDriver={true}
          hideModalContentWhileAnimating
          isVisible={this.state.showDetail}
          onBackButtonPress={this.showDetail}
          onBackdropPress={this.showDetail}
        >
          <View style={styles.modalFull}>
            <Text>Awokawokwaok</Text>
          </View>
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    fetchRoom: state.hotel.fetchingRoomHotel,
    dataRoom: makeRoomHotel(state),
    searchPayload: makeSearchPayloadBeds(state),
    detailPayload: makeHotelDetailPayload(state),
    isLogin: state.profile.isLogin,
    token: state.profile.token,
    payloadListRoom: makeRoomHotelPayload(state),
    hotelDetail: makeHotelDetailBeds(state),
    hotelResultBeds: makeSearchHotelBeds(state),
  };
}

export default connect(mapStateToProps)(SelectRoomHotel);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  content: {
    borderTopLeftRadius: 20,
  },
  subContent: { marginTop: -20, backgroundColor: thameColors.backWhite },
  left: { marginLeft: 7.5 },
  alignCenter: { alignItems: 'center' },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalFull: {
    width: '100%',
    backgroundColor: thameColors.white,
  },
  showRoom: {
    backgroundColor: thameColors.white,
    width: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textRoom: {
    fontFamily: fontSemiBold,
    color: thameColors.superBack,
  },
  textNote: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    fontSize: 14,
  },
  textPrice: {
    fontFamily: fontReguler,
    color: thameColors.primary,
    fontSize: 14,
  },
  btnRoom: {
    borderBottomWidth: 0.5,
    paddingVertical: 5,
    padding: 20,
    borderBottomColor: thameColors.gray,
    marginBottom: 5,
  },
  headerRoom: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    borderBottomWidth: 0.5,
    borderBottomColor: thameColors.textBlack,
  },
});
