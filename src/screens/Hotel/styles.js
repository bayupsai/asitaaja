import { StyleSheet, Dimensions } from 'react-native';
import {
  fontReguler,
  fontBold,
  thameColors,
  fontExtraBold,
  asitaColor,
} from '../../base/constant';

const window = Dimensions.get('window');
const deviceWidth = window.width;
const deviceHeight = window.height;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  flex: { flex: 1 },
  flex0: { flex: 0 },
  flexTop: { flex: 0, paddingTop: 5 },
  topPad: { paddingTop: 15 },
  bottomPad: { paddingBottom: 7.5 },
  padding: { padding: 10 },
  alignCenter: { alignItems: 'center' },
  alignStart: { alignItems: 'flex-start' },
  center: { alignItems: 'center', justifyContent: 'center' },
  startLeft10: { alignItems: 'flex-start', paddingLeft: 10 },
  startBetween: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  contentForm: {},
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 25,
  },
  sectionButton: {
    width: deviceWidth,
    height: 50,
    marginBottom: 10,
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 5,
  },
  content: {
    alignItems: 'center',
  },
  sectionFlexDirection: {
    flexDirection: 'row',
  },
  body: {
    flexDirection: 'row',
    width: deviceWidth,
    // justifyContent: 'space-between',
    alignItems: 'center',
    padding: 30,
  },
  textInput: {
    fontFamily: fontReguler,
    width: deviceWidth / 3,
    height: 50,
    borderWidth: 1,
    borderRadius: 7,
    borderColor: thameColors.gray,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 7,
    marginTop: 5,
  },
  textBoldBlack: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textSuperBold: {
    fontFamily: fontExtraBold,
    fontSize: 16,
    color: thameColors.superBack,
  },
  textBlack: {
    color: thameColors.superBack,
    fontFamily: fontBold,
    fontSize: 16,
  },
  textReguler: {
    fontFamily: fontReguler,
    fontSize: 16,
    color: thameColors.superBack,
  },
  starBody: {
    margin: 10,
    marginLeft: -10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: thameColors.gray,
    flexDirection: 'row',
  },
  starRating: {
    width: 45,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: thameColors.gray,
    backgroundColor: thameColors.white,
    position: 'relative',
  },
  textYellow: {
    color: thameColors.yellow,
    fontSize: 17,
    fontFamily: fontReguler,
  },
  textGuest: {
    fontFamily: fontBold,
    color: thameColors.superBack,
    fontSize: 14,
  },
  textDuration: {
    color: thameColors.superBack,
    fontWeight: '500',
    fontSize: 16,
  },
  textPopular: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 16,
    marginLeft: 5,
  },
  textDate: {
    paddingTop: 20,
    fontSize: 16,
    color: thameColors.superBack,
    paddingLeft: 20,
    fontFamily: fontReguler,
  },
  gridToggle: {
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 10,
  },
  marginTop: {
    marginTop: 10,
  },
  searchButton: {
    width: deviceWidth / 1.1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    color: thameColors.white,
    backgroundColor: thameColors.orange,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: thameColors.white,
  },
  textSearchButton: {
    fontSize: 20,
    color: thameColors.white,
    fontFamily: fontReguler,
  },

  locationGo: {
    backgroundColor: thameColors.white,
  },
  sectionText: {
    paddingLeft: 10,
  },

  // Modal
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    flex: 1,
    backgroundColor: thameColors.semiGray,
    padding: 0,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 45,
    width: deviceWidth,
  },
  modalTitleSection: {
    alignItems: 'center',
    justifyContent: 'center',
    // paddingLeft:deviceWidth/8
  },
  modalTitle: {
    color: thameColors.white,
    fontFamily: fontBold,
    fontSize: 18,
  },
  modalSearch: {
    flex: 0,
    height: 70,
    width: deviceWidth,
    marginTop: -35,
    marginBottom: 10,
  },
  modalHeaderDatepicker: {
    flex: 0,
    backgroundColor: asitaColor.marineBlue,
    height: 50,
    width: deviceWidth,
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContentCity: {
    backgroundColor: thameColors.white,
    padding: 20,
    paddingTop: 10,
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: thameColors.white,
  },
  modalContentDuration: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  modalContentRoom: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  popularDestination: {
    flex: 1,
    backgroundColor: thameColors.white,
    padding: 10,
    paddingBottom: 10,
    paddingLeft: 0,
  },
  buttonCloseModal: { flexDirection: 'row', flex: 1 },
  buttonDuration: { flex: 0, paddingBottom: 10 },

  // List City
  contentListCity: {
    flex: 1,
    backgroundColor: thameColors.white,
    padding: 10,
    borderBottomColor: thameColors.gray,
    borderBottomWidth: 0.5,
  },
  buttonListCity: { flexDirection: 'row', flex: 1 },
  childListCity: {
    padding: 5,
    borderColor: asitaColor.tealBlue,
    borderWidth: 0.5,
    backgroundColor: asitaColor.lightBlueGrey,
    borderRadius: 5,
  },

  // Loading Booking
  contentLoading: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: thameColors.white,
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 5,
    height: 'auto',
    flex: 0,
  },
  contentIndicator: { flex: 0, paddingTop: 5, alignItems: 'center' },
  indicator: { flex: 0, backgroundColor: thameColors.white },

  // Main
  buttonHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 3,
  },
  contentInput: { marginTop: -50 },
});
