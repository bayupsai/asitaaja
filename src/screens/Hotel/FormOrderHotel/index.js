import React, { PureComponent } from 'react';
import { View, StyleSheet, ScrollView, Image, Text, Alert } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import { DotIndicator } from 'react-native-indicators';
import { connect } from 'react-redux';
import HeaderPage from '../../../components/HeaderPage';
import SubHeaderPage from '../../../components/SubHeaderPage';
import AlertModal from '../../../components/AlertModal';
import HotelInfo from './Component/HotelInfo';
import {
  thameColors,
  fontExtraBold,
  fontReguler,
} from '../../../base/constant';
import ContactInfo from './Component/ContactInfo';
import PersonInfo from './Component/PersonInfo';
import PriceInfo from './Component/PriceInfo';
import { ButtonRounded } from '../../../elements/Button';
import { actionBookHotelBeds } from '../../../redux/actions/HotelAction';
import {
  makeSearchPayload,
  makeRoomHotel,
  makeRoomHotelPayload,
  makeContactDetail,
  makeChooseRoom,
  makeGuestData,
  makeHotelDetailPayload,
  makeChooseHotelBooking,
  makeHotelDetailBeds,
} from '../../../redux/selectors/HotelSelector';

class FormOrderHotel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: null,
      loadingState: false,
      errMessage: '',
      validationStatusData: false,
    };
  }

  // Modal Setting
  _toggleModal = id => this.setState({ isVisible: id });

  _setNullModal = () => this.setState({ isVisible: null });
  // Modal Setting

  // LOading Modal
  loadingBooking = () => (
    <Grid style={styles.contentLoading}>
      <Row style={styles.flex0}>
        <Col size={3} style={styles.center}>
          <Image
            source={require('../../../assets/img/loading3x.png')}
            style={styles.imgLoading}
            resizeMode="center"
          />
        </Col>
      </Row>
      <Row style={styles.flex0}>
        <Col size={7}>
          <Row style={[styles.flex0, styles.center]}>
            <Text style={styles.textLoading}>Relaxed for a moment</Text>
          </Row>
          <Row style={styles.bodyLoading}>
            <Text style={styles.textLoadingMini}>
              We are currently processing your booking.
            </Text>
          </Row>
          <Row style={[styles.flex0, styles.center]}>
            <Text style={styles.textLoadingMini}>
              This may take a few minutes.
            </Text>
          </Row>
          <Row style={styles.bodyLoading}>
            <DotIndicator
              size={10}
              style={styles.loadingIndicator}
              color={thameColors.secondary}
            />
          </Row>
        </Col>
      </Row>
    </Grid>
  );
  // Loading Modal

  // Validating all Data
  validateAllData = () => {
    const { searchPayload, guests } = this.props;
    const totalGuest =
      searchPayload.occupancies[0].adults +
      searchPayload.occupancies[0].children;
    if (guests.length < totalGuest) {
      this.setState({
        isVisible: 404,
        errMessage:
          'Please enter your contact details and passenger details correctly',
      });
    } else {
      let checkValid = false;
      guests.map(item => {
        if (item.first_name === '' || item.last_name === '') {
          checkValid = false;
        } else {
          checkValid = true;
        }
      });

      if (checkValid) {
        this.setState({ validationStatusData: true }, () => {
          this._toggleModal(1);
        });
      } else {
        this.setState({
          isVisible: 404,
          errMessage:
            'Please enter your contact details and passenger details correctly',
        });
      }
    }
  };
  // Validating all Data

  goBack = () => this.props.navigation.goBack();

  bookNow = () => {
    const { dispatch, contact, chooseRoom, guests } = this.props;
    // getFirstNameLastname(contact.fullName, res => {
    //   (objFullname.first_name = res.firstName),
    //     (objFullname.last_name = res.lastName);
    // });
    const payloadData = {
      holder: contact,
      rooms: [
        {
          rateKey: chooseRoom.roomType.rateKey,
          paxes: guests,
        },
      ],
      clientReference: 'ORDER-00001',
      remark: '',
      tolerance: '5.00',
    };
    // Alert.alert('Alert', JSON.stringify(payloadData));
    this.setState({ loadingState: true, isVisible: 9 }, () => {
      dispatch(actionBookHotelBeds(payloadData))
        .then(res => {
          if (res.type === 'BOOK_HOTEL_BEDS_FAILED') {
            this.setState({
              loadingState: false,
              isVisible: 404,
              errMessage: res.message,
            });
          } else {
            this.setState(
              {
                loadingState: false,
                isVisible: null,
              },
              () => {
                this.props.navigation.navigate('PaymentOrder', {
                  pages: 'hotel',
                });
              }
            );
          }
          // this.setState(
          //   {
          //     loadingState: false,
          //     isVisible: null,
          //     errMessage:
          //       res.type === 'BOOK_HOTEL_BEDS_FAILED'
          //         ? res.message
          //         : 'Continue to Payment',
          //   },
          //   () => {
          //     this.props.navigation.navigate('PaymentOrder', {
          //       pages: 'hotel',
          //     });
          //   }
          // );
        })
        .catch(err => {
          this.setState(
            { loadingState: false, isVisible: null, errMessage: err.message },
            () => {
              Alert.alert('Alert', this.state.errMessage);
            }
          );
        });
      // dispatch(
      //   actionHotelBook(
      //     payloadData,
      //     detailPayload.property_ids[0],
      //     chooseRoom.id,
      //     isLogin ? token : ''
      //   )
      // )
      //   .then(res => {
      //     this.setState(
      //       {
      //         loadingState: false,
      //         isVisible: null,
      //         errMessage:
      //           res.type === 'HOTEL_BOOK_FAILED'
      //             ? res.message.result_string
      //             : 'Continue to Credit Card',
      //       },
      //       () => {
      //         // this.setState({ isVisible: 404 })
      //         this.props.navigation.navigate('PaymentOrder', {
      //           pages: 'hotel',
      //         });
      //       }
      //     );
      //   })
      //   .catch(err => {
      //     this.setState(
      //       { loadingState: false, isVisible: null, errMessage: err.message },
      //       () => {
      //         // this.setState({ isVisible: 404 })
      //         Alert.alert('Alert', this.state.errMessage);
      //       }
      //     );
      //   });
    });
  };

  // Main Render
  render() {
    const { container, topY } = styles;
    return (
      <View style={container}>
        <HeaderPage
          title="Booking Summary"
          callback={this.goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <View style={topY}>
            <HotelInfo {...this.props} />
            <ContactInfo {...this.props} />
            <PersonInfo />
            <PriceInfo {...this.props} />
            <View style={styles.margin}>
              <ButtonRounded
                label="CONTINUE PAYMENT"
                onClick={this.validateAllData}
              />
            </View>
          </View>
        </ScrollView>

        {/* ======================= Modal Alert ======================= */}
        <AlertModal
          type="qna"
          isVisible={this.state.isVisible === 1}
          onDismiss={this._setNullModal}
          onPress={this.bookNow}
          titleColor={thameColors.superBack}
          title="Are your Detail Booking is Correct?"
          contentText="You will not be able to change your booking details once you proceed to payment. Do you want to continue?"
          labelOk="YES, CONTINUE"
          labelCancel="CHECK AGAIN"
        />
        <AlertModal
          type="normal"
          isVisible={this.state.isVisible === 404}
          onDismiss={this._setNullModal}
          onPress={this._setNullModal}
          title="Error"
          contentText={this.state.errMessage}
        />
        {/* ======================= Modal Alert ======================= */}

        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={this.state.isVisible === 9}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={200}
          animationOutTiming={200}
          backdropTransitionInTiming={200}
          backdropTransitionOutTiming={200}
        >
          {this.loadingBooking()}
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    bookingRoom: makeChooseHotelBooking(state),
    searchPayload: makeSearchPayload(state),
    detailPayload: makeHotelDetailPayload(state),
    roomHotel: makeRoomHotel(state),
    roomHotelPayload: makeRoomHotelPayload(state),
    contact: makeContactDetail(state),
    chooseRoom: makeChooseRoom(state),
    guests: makeGuestData(state),
    hotelDetailBeds: makeHotelDetailBeds(state),
    isLogin: state.profile.isLogin,
    token: state.profile.token,
  };
}

export default connect(mapStateToProps)(FormOrderHotel);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: thameColors.backWhite,
  },
  topY: {
    marginTop: -75,
  },
  flex0: { flex: 0 },
  margin: { margin: 20 },
  center: { alignItems: 'center', justifyContent: 'center' },
  // Content Loading
  contentLoading: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: thameColors.white,
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 5,
    height: 'auto',
    flex: 0,
  },
  imgLoading: { width: 175, height: 175 },
  textLoading: {
    fontFamily: fontExtraBold,
    fontSize: 16,
    color: thameColors.superBack,
  },
  textLoadingMini: {
    fontFamily: fontReguler,
    fontSize: 14,
    color: thameColors.superBack,
  },
  bodyLoading: {
    flex: 0,
    paddingTop: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingIndicator: { flex: 0, backgroundColor: thameColors.white },
});
