/* eslint-disable no-sequences */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Alert,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { InputText } from '../../../../elements/TextInput';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import { ButtonRounded } from '../../../../elements/Button';
import { actionSetGuest } from '../../../../redux/actions/HotelAction';
import {
  fontBold,
  thameColors,
  fontSemiBold,
  fontRegulerItalic,
  fontReguler,
} from '../../../../base/constant';
import { getFirstNameLastname } from '../../../../utilities/helpers';
import {
  makeAllHotel,
  makeContactDetail,
  makeGuestData,
  makeSearchPayload,
  makeRoomHotelPayload,
} from '../../../../redux/selectors/HotelSelector';

const window = Dimensions.get('window');
const deviceHeight = window.height;
const deviceWidth = window.width;

class PersonInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      // Modal
      visibleModal: null,
      modalActiveData: '',
      modalActiveIndex: 0,
      // Modal
      errorMessage: '',
      listTitle: [
        { index: 1, title: 'MR' },
        { index: 2, title: 'MRS' },
        { index: 3, title: 'MS' },
      ],
      titleSelected: 1,
      stateDataGuest: [],

      // Form
      title: 'MR',
      fullname: '',
      // FOrm
    };
  }

  componentDidMount() {
    const { searchPayload } = this.props;
    // const num_adult = roomHotelPayload
    //   ? roomHotelPayload.room_group.rooms[0].num_of_adults
    //   : searchPayload.room_group.rooms[0].num_of_adults;
    const num_adult =
      searchPayload.occupancies[0].adults +
      searchPayload.occupancies[0].children;
    this.generateStateGuest(num_adult);
  }

  // Modal
  setModal = id => this.setState({ visibleModal: id });

  setModal1 = () => this.setState({ visibleModal: 1 });

  setModal2 = () => this.setState({ visibleModal: 2 });

  _setNullModal = () => this.setState({ visibleModal: null });

  openModalPassenger = (index, currentData) => {
    const { stateDataGuest } = this.state;
    this.setState(
      {
        modalActiveIndex: index,
        modalActiveData: currentData,
        title: stateDataGuest[index].title,
        fullname:
          stateDataGuest[index].name !== '' &&
          stateDataGuest[index].surname !== ''
            ? `${stateDataGuest[index].name} ${stateDataGuest[index].surname}`
            : null,
      },
      () => this.setState({ visibleModal: 1 })
    );
  };
  // Modal

  // Flatlist Conf
  _keyExtractor = (item, index) => index.toString();

  _renderDataGuest = ({ index }) => {
    const { stateDataGuest } = this.state;
    const { card, textSemiBold } = styles;
    return (
      <View key={index}>
        <TouchableOpacity
          onPress={() => this.openModalPassenger(index, null)}
          style={card}
        >
          <Text style={textSemiBold}>
            {stateDataGuest[index].name === '' &&
            stateDataGuest[index].surname === ''
              ? `Guest ${index + 1}`
              : `${stateDataGuest[index].name} ${stateDataGuest[index].surname}`}
          </Text>
          <ButtonPlus size="small" colorButton="buttonColor" />
        </TouchableOpacity>
        {stateDataGuest[index].name === '' &&
        stateDataGuest[index].surname === '' ? (
          <Text style={styles.textRed}>*Please fill in passenger details</Text>
        ) : null}
      </View>
    );
  };

  _renderTitle = () => {
    const { listTitle } = this.state;
    return listTitle.map((data, index) => {
      return (
        <Grid key={index} style={styles.contentTitle}>
          <TouchableOpacity
            onPress={() => this.handleTitle(data)}
            style={styles.flex1}
          >
            <Col style={styles.alignStart}>
              <Text style={styles.textBlack}>{data.title}</Text>
            </Col>
            <Col style={styles.endCenter}>
              {index + 1 === data.index ? (
                <Icon
                  name="check"
                  type="feather"
                  color={thameColors.semiGreen}
                  size={22}
                />
              ) : null}
            </Col>
          </TouchableOpacity>
        </Grid>
      );
    });
  };
  // Flatlist Conf

  // Generate Total Guest
  generateStateGuest(total) {
    const { stateDataGuest } = this.state;
    for (let i = 0; i < total; i++) {
      stateDataGuest.push({
        roomId: i + 1,
        type: 'AD',
        name: '',
        surname: '',
        // title: 'MR',
        // first_name: '',
        // last_name: '',
      });
    }
    this.setState({ stateDataGuest });
  }

  // Render Form
  renderFormGuest() {
    const { stateDataGuest } = this.state;
    return (
      <View>
        <FlatList
          keyExtractor={this._keyExtractor}
          data={stateDataGuest}
          renderItem={this._renderDataGuest}
          removeClippedSubviews
          getItemLayout={(data, index) => ({
            length: 3,
            offset: 3 * index,
            index,
          })}
        />
      </View>
    );
  }
  // Render Form

  // Input Form
  handleInput = (type, data) => {
    if (type === 'fullname') {
      this.setState({ fullname: data });
    }
  };

  handleTitle = data => {
    const { modalActiveIndex, stateDataGuest } = this.state;
    const field = 'title';
    if (stateDataGuest[modalActiveIndex]) {
      stateDataGuest[modalActiveIndex][field] = data.title;
    }
    this.setState({
      stateDataGuest,
      visibleModal: 1,
    });
  };

  saveGuest = () => {
    const { dispatch } = this.props;
    const { modalActiveIndex, stateDataGuest, fullname } = this.state;
    const fieldFirst = 'name';
    const fieldLast = 'surname';
    const objGuest = {};
    if (fullname !== '') {
      getFirstNameLastname(fullname, res => {
        (objGuest.name = res.firstName), (objGuest.surname = res.lastName);
      });
      if (stateDataGuest[modalActiveIndex]) {
        stateDataGuest[modalActiveIndex][fieldFirst] = objGuest.name;
        stateDataGuest[modalActiveIndex][fieldLast] = objGuest.surname;
      }
      this.setState(
        {
          stateDataGuest,
          visibleModal: null,
        },
        () => {
          dispatch(actionSetGuest(stateDataGuest));
        }
      );
    } else {
      Alert('Alert', 'Fullname is required');
    }
  };

  // Input Form

  // Main Render
  render() {
    const { container, textBold } = styles;
    const { visibleModal, fullname, modalActiveIndex } = this.state;
    return (
      <View style={container}>
        <View style={styles.titleHead}>
          <Text style={textBold}>Guests</Text>
        </View>
        {/* Contact Details */}
        {this.renderFormGuest()}

        {/* =============================== MODAL FORM ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={visibleModal === 1}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid style={styles.modalContent}>
              <TouchableOpacity
                style={styles.row1}
                onPress={this._setNullModal}
              >
                <Col style={styles.center}>
                  <Text style={styles.textBold}>
                    Guest {parseInt(modalActiveIndex, 2) + 1}
                  </Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: thameColors.white }}>
                <Grid style={styles.topPadding}>
                  <Row style={styles.bottom20}>
                    {/* <Col size={3}>
                      <TouchableOpacity onPress={this.setModal2}>
                        <InputText
                          size="small"
                          placeholder={title}
                          label="Title"
                          required="*"
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col> */}
                    <Col size={7} style={styles.left0}>
                      <InputText
                        label="Fullname"
                        required="*"
                        value={fullname}
                        editable
                        onChangeText={data =>
                          this.handleInput('fullname', data)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={styles.bottom20}>
                    <Col size={6} style={styles.sectionButtonArea}>
                      <ButtonRounded label="SAVE" onClick={this.saveGuest} />
                    </Col>
                  </Row>
                </Grid>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL FORM ===================================== */}

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={visibleModal === 2}
          onBackButtonPress={this.setModal1}
          onBackdropPress={this.setModal1}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid style={styles.modalContentTitle}>
              <TouchableOpacity style={styles.row1} onPress={this.setModal1}>
                <Col style={styles.center}>
                  <Text style={styles.textSemiBold}>Choose Title</Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={styles.padding}>
              <Col style={styles.leftPadding}>{this._renderTitle()}</Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    hotel: makeAllHotel(state),
    contact: makeContactDetail(state),
    guests: makeGuestData(state),
    searchPayload: makeSearchPayload(state),
    roomHotelPayload: makeRoomHotelPayload(state),
  };
}

export default connect(stateToProps)(PersonInfo);

const styles = StyleSheet.create({
  container: {
    margin: 20,
    marginTop: 0,
    marginBottom: 0,
  },
  textBold: {
    fontFamily: fontBold,
    fontSize: 18,
    color: thameColors.textBlack,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textRed: {
    color: thameColors.red,
    fontFamily: fontRegulerItalic,
    marginTop: -7,
  },
  textBlack: {
    fontFamily: fontReguler,
    color: thameColors.superBack,
    fontSize: 16,
  },
  card: {
    borderRadius: 10,
    padding: 20,
    backgroundColor: thameColors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },

  titleHead: {
    // marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  // Modal
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.25,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  modalContentTitle: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  // Conf
  flex1: { flex: 1 },
  alignStart: { alignItems: 'flex-start' },
  endCenter: { alignItems: 'flex-end', justifyContent: 'center' },
  center: { alignItems: 'center', justifyContent: 'center' },
  row1: { flex: 1, flexDirection: 'row' },
  topPadding: { paddingTop: 20 },
  leftPadding: { paddingLeft: 10 },
  padding: { padding: 10 },
  bottom20: { flex: 0, marginBottom: 20 },
  left0: { marginLeft: 0 },

  //Title
  contentTitle: {
    flex: 1,
    backgroundColor: thameColors.white,
    padding: 10,
    borderBottomColor: thameColors.buttonGray,
    borderBottomWidth: 0.5,
  },
});
