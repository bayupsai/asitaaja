import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Icon } from 'react-native-elements';
import numeral from 'numeral';
import Modal from 'react-native-modal';
import {
  fontBold,
  thameColors,
  fontSemiBold,
  fontReguler,
  asitaColor,
} from '../../../../base/constant';

const window = Dimensions.get('window');

class PriceInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      payRoom: [1],
    };
  }

  //= ============= Modal Price ================
  nullVisible = () => this.setState({ visibleModal: null });

  renderRoom = (item, index) => (
    <View key={index} style={styles.rowBetween}>
      <View style={styles.rowBetween}>
        <Icon type="entypo" name="dot-single" color={thameColors.darkGray} />
        <Text style={[styles.textRegular, { color: thameColors.darkGray }]}>
          Night
          {index + 1} x 1 Room
        </Text>
      </View>
      <Text style={[styles.textRegular, { color: thameColors.darkGray }]}>
        Rp
        {numeral(this.props.chooseRoom.net)
          .format('0,0')
          .replace(/,/g, '.')}
      </Text>
    </View>
  );

  // ============== Modal Price ============
  totalPay = (value, length) => {
    return value * length;
  };

  // Main Render
  render() {
    const { container, textBold, textSemiBold, card, textRegular } = styles;
    const { payRoom } = this.state;
    const { chooseRoom } = this.props;
    return (
      <View style={container}>
        <View style={[styles.top20, styles.rowBetween]}>
          <Text style={textBold}>Total Payment</Text>
          <TouchableOpacity
            onPress={() => this.setState({ visibleModal: 1 })}
            style={styles.rowBetween}
          >
            <Text style={[textSemiBold, { color: asitaColor.orange }]}>
              Price Details
            </Text>
            <Icon
              type="ionicon"
              name="ios-arrow-forward"
              color={asitaColor.orange}
              iconStyle={styles.left10}
            />
          </TouchableOpacity>
        </View>

        <View style={card}>
          <Text style={textSemiBold}>Price You Pay</Text>
          <Text style={[textBold, { color: asitaColor.orange }]}>
            Rp.{' '}
            {numeral(parseInt(chooseRoom.roomType.net, 14) * 14000)
              .format('0,00')
              .replace(/,/g, '.')}
            {/* {numeral(this.totalPay(chooseRoom.net, 1))
              .format('0,0')
              .replace(/,/g, '.')} */}
          </Text>
        </View>

        {/* =============== Modal =============== */}
        <Modal
          useNativeDriver
          isVisible={this.state.visibleModal === 1}
          onBackButtonPress={this.nullVisible}
          onBackdropPress={this.nullVisible}
          style={styles.modal}
        >
          <View style={styles.modalContent}>
            <TouchableOpacity
              onPress={this.nullVisible}
              style={styles.btnCloseModal}
            >
              <Text style={styles.textBlack}>Price Details</Text>
            </TouchableOpacity>
            <View style={styles.modalDetail}>
              <View>
                <Text style={styles.textBold}>Fee & Tax Details</Text>
                <View style={[styles.vertical, styles.rowBetween]}>
                  <Text style={[styles.textRegular, styles.sizeFont]}>
                    {this.props.bookingRoom.roomTitle} Room
                  </Text>
                  <Text style={[styles.textRegular, styles.sizeFont]}>
                    Rp
                    {numeral(this.totalPay(chooseRoom.net, 1))
                      .format('0,0')
                      .replace(/,/g, '.')}
                  </Text>
                </View>
                {payRoom.map((item, index) => this.renderRoom(item, index))}
              </View>
            </View>
            <View style={styles.modalBottom}>
              <View style={[container, styles.rowBetween]}>
                <Text style={[textRegular, styles.sizeFont]}>
                  Total Payment
                </Text>
                <Text style={[textBold, { color: thameColors.primary }]}>
                  Rp
                  {numeral(this.totalPay(chooseRoom.net, 1))
                    .format('0,0')
                    .replace(/,/g, '.')}
                </Text>
              </View>
            </View>
            {/* <View style={{ margin: 20 }}>
                            <ButtonRounded label="CONTINUE PAYMENT" onClick={() => alert('You are not allowed to sleep in this hotel')} />
                        </View> */}
          </View>
        </Modal>
      </View>
    );
  }
}

export default PriceInfo;

const styles = StyleSheet.create({
  container: {
    margin: 20,
    marginTop: 0,
    marginBottom: 0,
  },
  textBold: {
    fontFamily: fontBold,
    fontSize: 18,
    color: thameColors.textBlack,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  textBlack: {
    fontFamily: fontBold,
    color: thameColors.superBack,
    fontSize: 18,
  },
  card: {
    borderRadius: 10,
    padding: 20,
    backgroundColor: thameColors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },

  // Modal
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    backgroundColor: thameColors.white,
    padding: 0,
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  btnCloseModal: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    borderBottomWidth: 0.5,
    paddingVertical: 10,
  },
  modalDetail: { width: window.width, padding: 20 },
  modalBottom: {
    borderTopWidth: 0.5,
    borderTopColor: thameColors.gray,
    paddingVertical: 20,
  },

  // Conf
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  top20: { marginTop: 20 },
  left10: { marginLeft: 10 },
  vertical: { marginVertical: 10 },
  sizeFont: { fontSize: 18 },
});
