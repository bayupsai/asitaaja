import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { InputText } from '../../../../elements/TextInput';
import { ButtonLogin, ButtonRounded } from '../../../../elements/Button';
import { ButtonPlus } from '../../../../elements/ButtonPlus';
import {
  fontBold,
  thameColors,
  fontSemiBold,
  fontRegulerItalic,
  fontReguler,
} from '../../../../base/constant';
import LoginModal from '../../../../components/LoginModal';
import AlertModal from '../../../../components/AlertModal';
import { actionContactDetail } from '../../../../redux/actions/HotelAction';
import { validateEmailFormat } from '../../../../utilities/helpers';
import { makeDataProfile } from '../../../../redux/selectors/ProfileSelector';

const window = Dimensions.get('window');
const deviceHeight = window.height;
const deviceWidth = window.width;

class ContactInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: null,
      titleSelected: 1,
      listTitle: [
        { index: 1, title: 'MR' },
        { index: 2, title: 'MRS' },
        { index: 3, title: 'MS' },
        { index: 4, title: 'MSTR' },
      ],
      title: 'MR',
      fullName: ' ',
      countryCode: '+62',
      phoneNumber: '',
      emailAddress: '',
      isValidEmail: true,
    };
  }

  componentDidMount() {
    this.handleLogin();
  }

  //= =========== Modal ===========
  setModal = item => this.setState({ isVisible: item });

  _setNullModal = () => this.setState({ isVisible: null });

  setModal1 = () => this.setState({ isVisible: 1 });

  setModal2 = () => this.setState({ isVisible: 2 });

  loginModal = () => this.setState({ isVisible: 99999 });
  //= =========== Modal ===========

  setTitlePassenger = data => {
    this.setState(
      {
        title: data.title,
        titleSelected: data.index,
      },
      () => {
        this.setState({ visibleModal: 1 });
      }
    );
  };

  handleLogin = () => {
    const { fullname, mobileNo, email } = this.props.dataProfile;
    this.setState(
      { fullName: fullname, phoneNumber: mobileNo, emailAddress: email },
      () => this.informationPassengers()
    );
  };

  _goRegister = () => {
    const { navigation } = this.props;
    this.setState({ isVisible: null }, () => navigation.navigate('SignUp'));
  };

  handleInput = (type, data) => {
    if (type === 'fullName') {
      this.setState({ fullName: data });
    } else if (type === 'phoneNumber') {
      this.setState({ phoneNumber: data });
    } else if (type === 'emailAddress') {
      const checkEmail = validateEmailFormat(data);
      if (checkEmail) {
        this.setState({ emailAddress: data, isValidEmail: true });
      } else {
        this.setState({ emailAddress: '', isValidEmail: false });
      }
    }
  };

  informationPassengers = () => {
    const { fullName, phoneNumber, emailAddress } = this.state;
    const { dispatch } = this.props;
    if (fullName === '' || phoneNumber === '' || emailAddress === '') {
      this.setModal(999);
    } else {
      const splitName = fullName ? fullName.split(' ') : [' ', ' '];
      const payload = {
        name: splitName[0],
        surname: splitName[1],
        email: emailAddress,
        phoneNumber,
        // title,
        // fullName,
        // countryCode,
        // phoneNumber,
        // emailAddress,
      };
      dispatch(actionContactDetail(payload));
      this._setNullModal();
    }
  };

  render() {
    const { container, textBold, textSemiBold, card } = styles;
    const {
      isVisible,
      fullName,
      title,
      titleSelected,
      phoneNumber,
      emailAddress,
      isValidEmail,
      listTitle,
    } = this.state;
    const { isLogin } = this.props;
    return (
      <View style={container}>
        {/* =============================== LOGIN BUTTON ===================================== */}
        {isLogin === false ? <ButtonLogin onPress={this.loginModal} /> : null}
        {/* =============================== LOGIN BUTTON ===================================== */}

        {/* =============================== CONTACT DETAILS ===================================== */}
        <View style={styles.top20}>
          <Text style={textBold}>Contact Details</Text>
        </View>
        <TouchableOpacity onPress={this.setModal1} style={card}>
          <Text style={textSemiBold}>
            {fullName ? `${title}. ${fullName}` : 'Contact Person'}
          </Text>
          <ButtonPlus size="small" colorButton="buttonColor" />
        </TouchableOpacity>
        {/* {
                    (fullName == "" || phoneNumber == "" || emailAddress == "")
                        ? <Text style={{ color: thameColors.red, fontFamily: fontRegulerItalic, marginTop: -7 }}>*Please fill in contact details</Text>
                        : null
                } */}
        <Text style={styles.textRed}>
          {fullName === '' || phoneNumber === '' || emailAddress === ''
            ? '*Please fill in contact details'
            : null}
        </Text>
        {/* =============================== CONTACT DETAILS ===================================== */}

        {/* =============================== MODAL FORM ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={isVisible === 1}
          onBackButtonPress={this._setNullModal}
          onBackdropPress={this._setNullModal}
          style={styles.bottomModal}
        >
          <View style={styles.modalSeatClass}>
            <Grid style={styles.modalContent}>
              <TouchableOpacity
                style={styles.row1}
                onPress={this._setNullModal}
              >
                <Col style={styles.center}>
                  <Text style={styles.textBold}>Contact Details</Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid>
              <Col style={{ backgroundColor: thameColors.white }}>
                <Grid style={styles.topPadding}>
                  <Row style={styles.bottom20}>
                    <Col size={3}>
                      <TouchableOpacity onPress={this.setModal2}>
                        <InputText
                          size="small"
                          placeholder={title}
                          label="Title"
                          required="*"
                          editable={false}
                        />
                      </TouchableOpacity>
                    </Col>
                    <Col size={7}>
                      <InputText
                        label="Fullname"
                        required="*"
                        value={fullName}
                        editable
                        onChangeText={data =>
                          this.handleInput('fullName', data)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={styles.bottom20}>
                    <Col>
                      <InputText
                        label="Phone Number"
                        required="*"
                        value={phoneNumber}
                        editable
                        maxLength={13}
                        keyboardType="number-pad"
                        onChangeText={data =>
                          this.handleInput('phoneNumber', data)
                        }
                      />
                    </Col>
                  </Row>
                  <Row style={styles.bottom20}>
                    <Col>
                      <View>
                        <InputText
                          label="Email Address"
                          required="*"
                          value={emailAddress}
                          editable
                          onChangeText={data =>
                            this.handleInput('emailAddress', data)
                          }
                        />
                      </View>
                      {isValidEmail === false ? (
                        <View>
                          <Text style={styles.textRedEmail}>
                            Please enter a valid email address
                          </Text>
                        </View>
                      ) : null}
                    </Col>
                  </Row>
                  <Row style={styles.bottom20}>
                    <Col size={6} style={styles.sectionButtonArea}>
                      <ButtonRounded
                        label="SAVE CONTACT"
                        onClick={this.informationPassengers}
                      />
                    </Col>
                  </Row>
                </Grid>
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL FORM ===================================== */}

        {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        <Modal
          useNativeDriver
          hideModalContentWhileAnimating
          isVisible={isVisible === 2}
          onBackButtonPress={this.setModal1}
          onBackdropPress={this.setModal1}
          style={styles.bottomModal}
        >
          <View style={styles.modalPassenger}>
            <Grid style={styles.modalTitlePassenger}>
              <TouchableOpacity
                style={styles.row1}
                onPress={this._setNullModal}
              >
                <Col style={styles.center}>
                  <Text style={styles.textBold}>Choose Title</Text>
                </Col>
              </TouchableOpacity>
            </Grid>
            <Grid style={styles.padding}>
              <Col style={styles.leftPadding}>
                {listTitle.map((data, index) => {
                  return (
                    <Grid key={index} style={styles.modalButtonTitle}>
                      <TouchableOpacity
                        onPress={() => this.setTitlePassenger(data)}
                        style={styles.flex1}
                      >
                        <Col style={styles.alignStart}>
                          <Text style={styles.textSuperBlack}>
                            {data.title}
                          </Text>
                        </Col>
                        <Col style={styles.endCenter}>
                          {titleSelected === data.index ? (
                            <Icon
                              name="check"
                              type="feather"
                              color={thameColors.semiGreen}
                              size={22}
                            />
                          ) : (
                            <Text />
                          )}
                        </Col>
                      </TouchableOpacity>
                    </Grid>
                  );
                })}
              </Col>
            </Grid>
          </View>
        </Modal>
        {/* =============================== MODAL TITLE PASSENGER ===================================== */}

        {/* =============================== MODAL ALERT ===================================== */}
        <AlertModal
          type="normal"
          isVisible={isVisible === 999}
          title="Warning."
          contentText="Please enter data correctly."
          onPress={this.setModal1}
          onDismiss={this.setModal1}
        />
        {/* =============================== MODAL ALERT ===================================== */}

        {/* ================ Modal ================ */}
        <LoginModal
          isVisible={isVisible === 99999}
          onDismiss={this._setNullModal}
          goRegister={this._goRegister}
          callbackLogin={this.handleLogin}
        />
        {/* ================ Modal ================ */}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLogin: state.profile.isLogin,
    dataProfile: makeDataProfile(state),
  };
}

export default connect(mapStateToProps)(ContactInfo);

const styles = StyleSheet.create({
  alignStart: { alignItems: 'flex-start' },
  flex1: { flex: 1 },
  top20: { marginTop: 20 },
  bottom20: { flex: 0, marginBottom: 20 },
  topPadding: { paddingTop: 20 },
  padding: { padding: 10 },
  leftPadding: { paddingLeft: 10 },
  row1: { flex: 1, flexDirection: 'row' },
  center: { alignItems: 'center', justifyContent: 'center' },
  endCenter: {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  container: {
    margin: 20,
    marginTop: 0,
    marginBottom: 0,
  },
  textBold: {
    fontFamily: fontBold,
    fontSize: 18,
    color: thameColors.textBlack,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
    fontSize: 16,
  },
  textRed: {
    color: thameColors.red,
    fontFamily: fontRegulerItalic,
    marginTop: -7,
  },
  textRedEmail: {
    fontFamily: fontBold,
    color: 'red',
    fontSize: 12,
    paddingLeft: 20,
  },
  textSuperBlack: {
    fontFamily: fontReguler,
    color: thameColors.superBack,
    fontSize: 16,
  },
  card: {
    borderRadius: 10,
    padding: 20,
    backgroundColor: thameColors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  modalContent: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  modalTitlePassenger: {
    width: deviceWidth,
    height: 40,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: thameColors.darkGray,
    borderBottomWidth: 0.5,
  },
  modalButtonTitle: {
    flex: 1,
    backgroundColor: thameColors.white,
    padding: 10,
    borderBottomColor: thameColors.buttonGray,
    borderBottomWidth: 0.5,
  },
  modalSeatClass: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 1.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  sectionButtonArea: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
