import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import moment from 'moment';
import {
  thameColors,
  fontBold,
  fontReguler,
  fontSemiBold,
} from '../../../../base/constant';

const dummyDay = [
  { title: 'Check-in', date: 'Wed, 21 Jan 2019' },
  { title: 'Check-out', date: 'Mon, 28 Jan 2019' },
];

// const Dot = () => (
//   <Icon type="entypo" name="dot-single" color={thameColors.textBlack} />
// );

const HotelInfo = props => {
  const {
    container,
    card,
    flexRow,
    textSemiBold,
    textBold,
    textRegular,
  } = styles;
  const { chooseRoom, searchPayload, hotelDetailBeds } = props;
  return (
    <View style={container}>
      <View style={card}>
        <View style={styles.row}>
          {/* <Text style={[textSemiBold]}>{props.bookingRoom.guest} Guest</Text> */}
          <Text style={[textSemiBold]}>
            {searchPayload.occupancies[0].adults +
              searchPayload.occupancies[0].children}{' '}
            Guest | {searchPayload.occupancies[0].rooms} Room
          </Text>
          {/* <Dot />
                    <Text style={[textSemiBold]}>{props.bookingRoom.room} Night</Text> */}
          {/* <Dot />
                    <Text style={[textSemiBold]}>7 Room</Text> */}
        </View>
        <Text style={textBold}>
          {hotelDetailBeds.name} - {chooseRoom.room.name}
        </Text>
        <View style={styles.top10}>
          {dummyDay.map((item, index) => (
            <View key={index} style={flexRow}>
              <Text style={textRegular}>{item.title}</Text>
              <Text style={[textRegular, { color: thameColors.darkGray }]}>
                {moment(
                  index === 0
                    ? searchPayload.stay.checkIn
                    : searchPayload.stay.checkOut
                ).format('ddd, D MMM YYYY')}
              </Text>
            </View>
          ))}
        </View>
      </View>
    </View>
  );
};

export default HotelInfo;

const styles = StyleSheet.create({
  top10: { marginTop: 10 },
  container: {
    margin: 20,
    marginBottom: 0,
  },
  card: {
    borderRadius: 10,
    padding: 20,
    backgroundColor: thameColors.white,
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  row: { flexDirection: 'row' },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 20,
  },
  textSemiBold: {
    fontFamily: fontSemiBold,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
});
