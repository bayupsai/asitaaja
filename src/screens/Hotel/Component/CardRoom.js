import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import numeral from 'numeral';
import {
  thameColors,
  fontBold,
  fontSemiBold,
  fontReguler,
  fontExtraBold,
  asitaColor,
} from '../../../base/constant';
import { scale } from '../../../Const/ScaleUtils';
import ProgressiveImage from '../../../components/ProgressiveImage';

const dummyFacility = [
  {
    title: 'Breakfast',
    img: require('../../../assets/icons/restaurant_icon.png'),
  },
  { title: 'WiFi', img: require('../../../assets/icons/wifi_icon.png') },
];

const CardRoom = props => {
  const {
    card,
    textBold,
    textRegular,
    textExtraBold,
    imgCard,
    iconFacilities,
    button,
  } = styles;
  return (
    <View style={card}>
      {/* Header */}
      <View style={styles.cardHeader}>
        <View style={styles.cardTitle}>
          <Text style={[textBold, styles.font20]}>{props.title}</Text>
        </View>
        <TouchableOpacity style={styles.cardDetails}>
          <Text style={{ fontFamily: fontSemiBold, color: asitaColor.orange }}>
            See Details
          </Text>
        </TouchableOpacity>
      </View>
      {/* Body */}
      <View style={styles.cardBody}>
        <ProgressiveImage uri={props.img} style={imgCard} resizeMode="cover" />
        {/* <Image
          source={props.img}
          onError={() => props.imgError}
          style={imgCard}
          resizeMode="cover"
        /> */}
        <View style={styles.left10}>
          <Text style={textRegular}>
            Max {props.maxGuest ? props.maxGuest : ''} Guest
          </Text>
          <View style={styles.row}>
            {dummyFacility.map((item, index) => (
              <View key={index} style={styles.facility}>
                <Image
                  source={item.img}
                  style={iconFacilities}
                  resizeMode="contain"
                />
                <Text style={[styles.left5, textRegular]}>{item.title}</Text>
              </View>
            ))}
          </View>
        </View>
      </View>
      {/* Footer */}
      <View style={styles.cardFooter}>
        <View style={styles.contentPrice}>
          <Text style={[textBold, styles.textPrice]}>
            {props.currency}
            {numeral(props.price)
              .format('0,0')
              .replace(/,/g, '.')}
          </Text>
          <Text style={[textRegular, styles.textRoomNight]} />
        </View>
        <View>
          <TouchableOpacity onPress={props.onPress} style={[button]}>
            <Text style={[textExtraBold, { color: thameColors.white }]}>
              SELECT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CardRoom;

const styles = StyleSheet.create({
  font20: { fontSize: 20 },
  left10: { marginLeft: 10 },
  left5: { marginLeft: 5 },
  row: { flexDirection: 'row' },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  cardTitle: { width: '70%' },
  cardDetails: { width: '30%', alignItems: 'flex-end', padding: 5 },
  card: {
    margin: 20,
    backgroundColor: thameColors.white,
    padding: 15,
    borderRadius: 10,
    flex: 1,
    marginVertical: 10,
  },
  cardBody: { flexDirection: 'row', marginBottom: 10 },
  cardFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  textExtraBold: {
    fontFamily: fontExtraBold,
    fontSize: 16,
  },
  textBold: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
  },
  imgCard: {
    width: scale(100),
    height: scale(100),
    borderRadius: 10,
  },
  iconFacilities: {
    width: scale(20),
    height: scale(20),
  },
  button: {
    width: '100%',
    padding: 7,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    backgroundColor: asitaColor.tealBlue,
  },
  facility: { flexDirection: 'row', margin: 3 },
  contentPrice: { flexDirection: 'row', alignItems: 'center' },
  textPrice: { color: asitaColor.orange, fontSize: 20 },
  textRoomNight: { color: thameColors.gray, marginLeft: 5 },
});
