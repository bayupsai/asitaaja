import React from 'react';
import { StyleSheet, Image, TouchableOpacity, Animated } from 'react-native';
import { thameColors } from '../../../base/constant';
import { scale } from '../../../Const/ScaleUtils';

const HeaderTransparent = props => {
  const { container, leftButton, rightButton, customIcon } = styles;
  return (
    <Animated.View style={[container, props.style]}>
      <TouchableOpacity style={leftButton} onPress={props.onLeftPress}>
        <Image
          source={require('../../../assets/icons/arrow_left_icon.png')}
          style={[{ marginRight: 5, width: '90%', height: '90%' }]}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[rightButton, customIcon]}
        onPress={props.onRightPress}
      >
        <Image
          source={require('../../../assets/icons/share_icon.png')}
          style={{ width: '100%', height: '100%' }}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </Animated.View>
  );
};

export default HeaderTransparent;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    width: '100%',
  },
  leftButton: {
    backgroundColor: thameColors.white,
    borderRadius: 20,
    padding: 5,
    top: 40,
    left: 20,
    width: scale(40),
    height: scale(30),
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightButton: { top: 41, right: 20 },
  customIcon: { width: scale(30), height: scale(30) },
});
