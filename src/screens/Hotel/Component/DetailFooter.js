import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import numeral from 'numeral';
import {
  thameColors,
  fontExtraBold,
  fontReguler,
  fontBold,
  asitaColor,
} from '../../../base/constant';

const DetailFooter = props => {
  return (
    <View style={style.bottomFix}>
      <View style={style.contentFooter}>
        <Text style={[{ color: thameColors.gray, fontFamily: fontReguler }]}>
          {props.type === 'ticket' ? props.labelType : 'Start From'}
        </Text>
        <Text style={style.textPrice}>
          {props.currency}{' '}
          {numeral(parseInt(props.price, 14) * 14000)
            .format('0,00')
            .replace(/,/g, '.')}
        </Text>
      </View>
      <View style={style.justifyCenter}>
        <TouchableOpacity
          onPress={props.onPress}
          style={[
            style.button,
            props.active === true ? style.buttonActive : style.buttonNonActive,
          ]}
        >
          <Text
            style={[
              style.text,
              props.active === true ? style.textActive : style.textNonActive,
            ]}
          >
            {props.label}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DetailFooter;

const style = StyleSheet.create({
  bottomFix: {
    padding: 20,
    backgroundColor: thameColors.white,
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: window.width,
    borderTopWidth: 0.5,
    borderTopColor: thameColors.gray,
  },
  contentFooter: { width: '50%', justifyContent: 'center' },
  button: {
    width: '100%',
    padding: 10,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  buttonActive: {
    backgroundColor: asitaColor.tealBlue,
  },
  buttonNonActive: {
    backgroundColor: thameColors.white,
    borderWidth: 2,
    borderColor: thameColors.buttonGray,
  },
  justifyCenter: { justifyContent: 'center' },
  text: {
    fontFamily: fontExtraBold,
    color: thameColors.white,
    fontSize: 16,
  },
  textActive: {
    color: thameColors.white,
  },
  textNonActive: {
    color: thameColors.buttonGray,
  },
  textPrice: { color: asitaColor.orange, fontSize: 22, fontFamily: fontBold },
});
