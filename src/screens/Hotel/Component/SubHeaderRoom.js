import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import { thameColors, fontReguler, asitaColor } from '../../../base/constant';
import { scale } from '../../../Const/ScaleUtils';

const SubHeaderRoom = props => {
  const { container, content, imgIcon, textStyle, change } = styles;
  return (
    <View style={container}>
      {props.selected === true ? (
        <View>
          <View style={[content]}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                  source={require('../../../assets/icons/room_icon.png')}
                  style={imgIcon}
                  resizeMode="contain"
                />
                <Text style={[textStyle, { fontSize: 20, marginLeft: 10 }]}>
                  Night
                </Text>
              </View>
              <View style={change}>
                <TouchableOpacity onPress={props.minRoom}>
                  <Icon
                    type="simple-line-icon"
                    name="minus"
                    color={thameColors.white}
                  />
                </TouchableOpacity>
                <Text style={textStyle}>{props.totalRoom}</Text>
                <TouchableOpacity onPress={props.plusRoom}>
                  <Icon
                    type="simple-line-icon"
                    name="plus"
                    color={thameColors.white}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                  source={require('../../../assets/icons/user_icon.png')}
                  style={imgIcon}
                  resizeMode="contain"
                />
                <Text style={[textStyle, { fontSize: 20, marginLeft: 10 }]}>
                  Guest
                </Text>
              </View>
              <View style={change}>
                <TouchableOpacity onPress={props.minGuest}>
                  <Icon
                    type="simple-line-icon"
                    name="minus"
                    color={thameColors.white}
                  />
                </TouchableOpacity>
                <Text style={textStyle}>{props.totalGuest}</Text>
                <TouchableOpacity onPress={props.plusGuest}>
                  <Icon
                    type="simple-line-icon"
                    name="plus"
                    color={thameColors.white}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      ) : (
        <View>
          <TouchableOpacity
            onPress={props.onPress}
            style={[content, { flexDirection: 'row', alignItems: 'center' }]}
          >
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../../../assets/icons/room_icon.png')}
                style={imgIcon}
                resizeMode="contain"
              />
              <Text style={[textStyle, { marginLeft: 5 }]}>
                {props.totalRoom}
              </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../../../assets/icons/user_icon.png')}
                style={imgIcon}
                resizeMode="contain"
              />
              <Text style={[textStyle, { marginLeft: 5 }]}>
                {props.totalGuest}
              </Text>
            </View>
            <View
              style={{
                borderLeftWidth: 1,
                borderLeftColor: thameColors.whiteOcean,
                padding: 10,
              }}
            >
              <Text style={textStyle}>
                {moment(props.startDate).format('D MMM YYYY')} -{' '}
                {moment(props.endDate).format('D MMM YYYY')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default SubHeaderRoom;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 20,
    backgroundColor: asitaColor.marineBlue,
    paddingBottom: 40,
    paddingTop: -40,
  },
  content: {
    backgroundColor: asitaColor.oceanBlue,
    padding: 10,
    borderRadius: 5,
    width: '100%',
    justifyContent: 'space-between',
  },
  imgIcon: {
    width: scale(15),
    height: scale(17.5),
  },
  textStyle: {
    fontFamily: fontReguler,
    color: thameColors.white,
  },
  change: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: scale(100),
    backgroundColor: thameColors.primary,
    borderRadius: 20,
    paddingHorizontal: 2.5,
    paddingVertical: 2.5,
  },
});
