import React from 'react';
import { Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { fontReguler } from '../../../base/constant';

const window = Dimensions.get('window');
const deviceWidth = window.width;

const LocationGo = props => (
  <TouchableOpacity
    style={styles.locationGo}
    placeholder="Where do you want to go?"
  >
    <Icon name="location-pin" color="#FFC107" size={15} />
    <Text style={styles.sectionText}>{props.whereToGo}</Text>
  </TouchableOpacity>
);

export default LocationGo;

const styles = StyleSheet.create({
  locationGo: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.15,
    height: 'auto',
    borderRadius: 15,
    flexDirection: 'row',
    padding: 30,
    paddingLeft: 20,
    marginBottom: -10,
    // box shadow
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    transform: [{ translateY: -40 }],
  },
  sectionText: {
    paddingLeft: 10,
    fontFamily: fontReguler,
  },
});
