import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
//Component
import { Button } from '../../../elements/Button';
import { InputText } from '../../../elements/TextInput';
import { fontExtraBold, fontReguler } from '../../../base/constant';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default class Splash extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Grid style={styles.container}>
        <Row style={{ paddingLeft: 20 }}>
          <Text style={styles.textSignIn}>S I G N I N</Text>
        </Row>
        <Row style={styles.SectionStyle}>
          <View style={{ paddingBottom: 40 }}>
            <Text
              style={{
                fontSize: 12,
                paddingLeft: 200,
                fontFamily: fontReguler,
              }}
            >
              Garuda Miles Number
            </Text>
          </View>
          <TextInput
            placeholder="e.g 9388938494"
            placeholderTextColor="#e6e6e6"
            style={styles.inputId}
          />
        </Row>

        <Row style={styles.SectionStyle}>
          <Icon
            name="lock"
            color="black"
            type="fontawesome"
            style={styles.ImageStyle}
          />
          <TextInput
            style={{ flex: 1 }}
            placeholder="Password"
            placeholderTextColor="#e6e6e6"
            style={{ width: 210, padding: 5 }}
          />
          <Text>
            |{' '}
            <Text
              style={{
                color: 'blue',
                fontSize: 12,
                justifyContent: 'flex-start',
                fontFamily: fontReguler,
              }}
            >
              Forgot?
            </Text>
          </Text>
        </Row>

        <Row style={{ flex: 1, paddingTop: 10 }}>
          <Button label="Sign in" onClick={() => alert('Login')} />
        </Row>
        <Row
          style={{ paddingLeft: 20, justifyContent: 'center', paddingTop: 20 }}
        >
          <Text style={{ fontSize: 14, fontFamily: fontReguler }}>
            Don't have account?{' '}
            <Text style={{ fontSize: 14, fontWeight: '600' }}>Sign up</Text>
          </Text>
        </Row>
      </Grid>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    color: '#838383',
    paddingBottom: 5,
    fontWeight: '400',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  inputId: {
    paddingRight: 40,
    paddingBottom: -10,
    width: deviceWidth,
    transform: [{ translateX: -15 }],
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  container: {
    backgroundColor: '#fff',
    height: deviceHeight / 2,
    width: deviceWidth,
    borderRadius: 15,
    paddingRight: 10,
    paddingTop: 35,
    paddingBottom: 20,
    elevation: 5,
  },
  textSignIn: {
    fontSize: 18,
    fontFamily: fontExtraBold,
    flex: 1,
  },
  SectionStyle: {
    backgroundColor: '#FFFFFF',
    borderColor: '#e6e6e6',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderRadius: 5,
    margin: 10,
    borderWidth: 0.5,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
});
