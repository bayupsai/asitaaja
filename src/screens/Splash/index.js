/* eslint-disable react-native/no-inline-styles */
import React, { PureComponent } from 'react';
import { Image, StatusBar } from 'react-native';
import { Grid, Row } from 'react-native-easy-grid';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { actionGetProfile } from '../../redux/actions/ProfileAction';
import { actionListCountry } from '../../redux/actions/ListCountryAction';
import { actionListAirport } from '../../redux/actions/FlightAction';
import { actionBatikAll } from '../../redux/actions/BatikActions';
import OfflineNotice from '../../components/OfflineNotice';
import { verticalScale } from '../../Const/ScaleUtils';

class Splash extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { logUser, dispatch } = this.props;
    logUser !== null ? dispatch(actionGetProfile(logUser.access_token)) : null;
    dispatch(actionListCountry(1000));
    // this.getBatikAll();
    // this.getAirport()

    setTimeout(() => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Tab',
          }),
        ],
      });
      this.props.navigation.dispatch(resetAction);
    }, 1000);
  }

  getAirport = () => {
    let { dispatch } = this.props;
    dispatch(actionListAirport({}));
  };

  getBatikAll = () => {
    const { dispatch } = this.props;
    dispatch(actionBatikAll());
  };

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Grid>
        <StatusBar hidden={true} />
        <OfflineNotice />
        <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Image
            style={{ width: '87.5%', height: verticalScale(300) }}
            resizeMode="center"
            source={require('../../assets/logos/logo-asitatrave.png')}
          />
        </Row>
      </Grid>
    );
  }
}

function stateToProps(state) {
  return {
    logUser: state.login.data,
  };
}

export default connect(stateToProps)(Splash);
