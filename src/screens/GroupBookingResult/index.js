import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

// Component to Imported
import HeaderPage from '../../components/HeaderPage';
import SubHeaderOnly from './Component/SubHeaderOnly';
import DateScroll from './Component/DateScroll';
import CardResult from './Component/CardResult';
import SubHeaderPage from '../../components/SubHeaderPage';
import { Grid, Col } from 'react-native-easy-grid';

const window = Dimensions.get('window');
let deviceHeight = window.height;
var deviceWidth = window.width;

class GroupBookingResult extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dating: new Date(),
      data: [
        { id: 0, airline: 'Garuda Indonesia', price: '1.000.000' },
        { id: 1, airline: 'Sriwijaya', price: '1.000' },
        { id: 2, airline: 'Citilink', price: '1.500.000' },
        { id: 3, airline: 'Air Asia', price: '2.000.000' },
      ],
    };
  }
  goBackHeader = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={{ backgroundColor: '#f0f0f0', flex: 1 }}>
        <HeaderPage
          title="Group Booking"
          callback={this.goBackHeader}
          {...this.props}
        />
        <ScrollView horizontal={false}>
          <SubHeaderPage title="Flight result" flight={true} />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: -30,
            }}
          >
            <DateScroll />
          </View>

          <Grid
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: -10,
            }}
          >
            <Col>
              {this.state.data.map((item, i) => (
                <CardResult
                  {...this.props}
                  key={i}
                  airline={item.airline}
                  price={item.price}
                />
              ))}
            </Col>
          </Grid>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    justifyContent: 'center',
    marginTop: -10,
    alignItems: 'center',
    width: deviceWidth,
    height: 'auto',
    transform: [{ translateY: -10 }],
  },
  sectionScrollView: {
    // height: deviceHeight,
    // width: deviceWidth,
    // padding: 5,
    // marginTop: 0
  },
  sectionContentScroll: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

function mapStateToProps(state) {
  return {
    searchResult: state.flight,
  };
}
export default connect(mapStateToProps)(GroupBookingResult);
