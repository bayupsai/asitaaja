import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { fontReguler } from '../../../base/constant';

const Back = props => {
  return (
    <TouchableOpacity onPress={props.goBack} style={{ paddingRight: 20 }}>
      <Icon name="md-arrow-back" color="#FFFFFF" type="ionicon" />
    </TouchableOpacity>
  );
};

const Title = props => {
  return (
    <Text
      style={{
        color: '#FFFFFF',
        fontSize: 18,
        fontFamily: fontReguler,
        letterSpacing: 0.5,
        marginLeft: -10,
      }}
    >
      {props.title}
    </Text>
  );
};

const Heading = props => (
  <Header
    placement="left"
    leftComponent={<Back goBack={props.backButton} />}
    centerComponent={<Title title={props.title} />}
    containerStyle={{
      backgroundColor: '#00275d',
      paddingTop: 0,
      borderBottomWidth: 0,
      borderBottomColor: 'transparent',
      height: 50,
    }}
  ></Header>
);

export default Heading;
