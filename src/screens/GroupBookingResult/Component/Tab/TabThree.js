import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class TabOne extends React.PureComponent {
  render() {
    return (
      <View title={this.props.title} style={styles.content}>
        <Text style={styles.header}>Ease of Learning</Text>
        <Text style={styles.text}>
          It’s much easier to read and write comparing to native platform’s code
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1, // Take up all available space
    justifyContent: 'center', // Center vertically
    alignItems: 'center', // Center horizontally
    backgroundColor: '#FFF', // Darker background for content area
  },
  // Content header
  header: {
    margin: 10, // Add margin
    color: '#000', // White color
    fontFamily: 'Avenir', // Change font family
    fontSize: 26, // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20, // Add horizontal margin
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center', // Center
    fontFamily: 'Avenir',
    fontSize: 18,
  },
});
