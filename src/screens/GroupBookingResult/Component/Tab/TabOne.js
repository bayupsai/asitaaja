import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dash from 'react-native-dash';
import { fontReguler, fontExtraBold } from '../../../../base/constant';

export default class TabOne extends React.PureComponent {
  render() {
    return (
      <View title={this.props.title} style={styles.content}>
        <Grid style={{ flex: 0, height: 50, marginBottom: 5 }}>
          <Col>
            <Image
              style={{ width: 200, height: 50 }}
              resizeMode="center"
              source={require('../../../../assets/icons/logo.png')}
            />
          </Col>
        </Grid>
        <Grid style={{ flex: 0, marginBottom: 10 }}>
          <Col>
            <Text style={{ fontFamily: fontReguler }}>(GA-652) Economy</Text>
          </Col>
        </Grid>
        <Grid style={{ flex: 0, flexDirection: 'row' }}>
          <View>
            <Icon name="ios-radio-button-off" size={18} color="#0387cc" />
          </View>
          <View style={{ paddingLeft: 10 }}>
            <Text style={styles.textBold}>09:00</Text>
          </View>
          <View style={{ paddingLeft: 10 }}>
            <Text style={styles.textBold}>-</Text>
          </View>
          <View style={{ paddingLeft: 10 }}>
            <Text style={{ fontFamily: fontReguler }}>Soekarno Hatta</Text>
          </View>
        </Grid>
        <Grid style={{ flex: 0, flexDirection: 'row' }}>
          <View>
            <Dash style={styles.dash} dashColor="#0387cc" />
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'column',
              paddingLeft: 20,
              paddingTop: 5,
            }}
          >
            <View style={{ flex: 0, flexDirection: 'row' }}>
              <View style={styles.kode}>
                <Text style={{ fontFamily: fontReguler }}>CGK</Text>
              </View>
              <View style={styles.kode2}>
                <Text style={styles.textBlue}>21 Jan</Text>
              </View>
            </View>
            <View style={{ paddingTop: 15 }}>
              <Text style={{ fontFamily: fontReguler }}>2 hrs 10 min</Text>
            </View>
          </View>
        </Grid>

        <Grid style={{ flex: 0, flexDirection: 'row', marginTop: 5 }}>
          <View>
            <Icon name="ios-radio-button-on" size={18} color="#0387cc" />
          </View>
          <View style={{ marginTop: -2, flex: 0, flexDirection: 'row' }}>
            <View style={{ paddingLeft: 10 }}>
              <Text style={styles.textBold}>11:10</Text>
            </View>
            <View style={{ paddingLeft: 10 }}>
              <Text style={styles.textBold}>-</Text>
            </View>
            <View style={{ paddingLeft: 10 }}>
              <Text style={{ fontFamily: fontReguler }}>Ngurah Rai (DPS)</Text>
            </View>
          </View>
        </Grid>
        <Grid style={{ flex: 0, flexDirection: 'row' }}>
          <View
            style={{
              flex: 0,
              flexDirection: 'column',
              paddingLeft: 25,
              paddingTop: 5,
            }}
          >
            <View style={{ flex: 0, flexDirection: 'row' }}>
              <View style={styles.kode}>
                <Text style={{ fontFamily: fontReguler }}>CGK</Text>
              </View>
              <View style={styles.kode2}>
                <Text style={styles.textBlue}>21 Jan</Text>
              </View>
            </View>
          </View>
        </Grid>
        {/* <Grid style={{flex: 0, paddingTop: 10}}>
                    <View>
                        <Icon name="ios-radio-button-off" size={17} color="#0387cc" style={[styles.sectionPaddingLeft, { transform: [{ translateY: -7 }] }]} />
                        <Text style={styles.textBold}>09:00</Text>
                        <Text> - </Text>
                        <Text>Soekarno Hatta</Text>
                    </View>
                    <View style={{ paddingLeft: 50, marginTop: 0, flexDirection: 'row' }}>
                        <View style={styles.kode} ><Text>CGK</Text></View>
                        <Text style={styles.textBlue}>21 Jan</Text>
                    </View>
                </Grid>
                <Grid style={{flex: 0, paddingTop: 30}}>
                    <Dash style={styles.dash} dashColor="#0387cc" />
                    <View style={{ marginLeft: -80, paddingTop: 25}}><Text>2 hrs 10 min</Text></View>
                </Grid>
                <Grid style={{flex: 0}}>
                    <Icon name="ios-radio-button-on" size={17} color="#0387cc" style={[styles.sectionPaddingLeft, { transform: [{ translateY: -7 }] }]} />
                    <Text style={styles.textBold}>11:10</Text>
                    <Text> - </Text>
                    <Text>Ngurah Rai (DPS)</Text>
                </Grid>
                <Grid style={{flex: 0}}>
                    <View style={styles.kode} ><Text>DPS</Text></View>
                    <Text style={styles.textBlue}>23 Jan</Text>
                </Grid> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    flex: 1,
    marginLeft: 10,
  },
  header: {
    padding: 10,
    color: '#000',
  },
  textHeader: {
    fontSize: 20,
    color: '#000',
    fontFamily: fontReguler,
  },
  text: {
    marginHorizontal: 20,
    color: 'rgba(255, 255, 255, 0.75)',
    textAlign: 'center',
    fontFamily: fontReguler,
    fontSize: 18,
  },
  textBold: {
    fontFamily: fontExtraBold,
    fontSize: 15,
  },
  sectionFrom: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    borderStyle: 'dashed',
    borderColor: '#000',
  },
  sectionPaddingLeft: {
    padding: 10,
  },
  sectionContentCenter: {
    justifyContent: 'center',
    paddingBottom: -20,
  },
  sectionDistance: {
    height: 75,
    width: 1,
    borderStyle: 'dotted',
    borderColor: '#000',
    borderWidth: 2,
    borderRadius: 1,
  },
  sectionDirectionRow: {
    flexDirection: 'row',
  },
  transformUp: {
    transform: [{ translateY: -10 }],
  },
  dash: {
    marginLeft: 7,
    width: 1,
    height: 100,
    flexDirection: 'column',
    transform: [{ translateY: 0 }, { translateX: 0 }],
  },
  kode: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 2,
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#d0d0d0',
  },
  kode2: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 2,
    paddingLeft: 0,
    paddingRight: 10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#ffffff',
  },
  textBlue: {
    color: '#0387cc',
    fontFamily: fontReguler,
  },
});
