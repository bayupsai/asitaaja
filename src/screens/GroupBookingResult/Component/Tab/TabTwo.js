import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

export default class TabOne extends React.PureComponent {
  render() {
    return (
      <View title={this.props.title} style={styles.container}>
        <View style={styles.sectionItem}>
          <Image
            source={require('../../../../assets/icons/bag3x.png')}
            style={styles.sectionImage}
          />
          <Text>Baggage 20 kg</Text>
        </View>
        <View style={styles.sectionItem}>
          <Image
            source={require('../../../../assets/icons/meal3x.png')}
            style={styles.sectionImage}
          />
          <Text>Meals</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    padding: 5,
    paddingLeft: 40,
  },
  sectionItem: {
    padding: 5,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  sectionImage: {
    marginRight: 10,
  },
});
