import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  Modal,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/Ionicons';

//component imported
import DisplayModal from './DisplayModal';
import {
  fontExtraBold,
  fontReguler,
  thameColors,
} from '../../../base/constant';

const window = Dimensions.get('window');
let deviceWidth = window.width;

class CardResult extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  handleModal = modalin => {
    this.setState({ modalVisible: modalin });
  };

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('ContactDetails')}
        style={[styles.card]}
      >
        <DisplayModal
          display={this.state.modalVisible}
          close={() => {
            this.handleModal(!this.state.modalVisible);
          }}
          modalin={() => {
            this.handleModal(!this.state.modalVisible);
          }}
        />
        <Grid style={[styles.cardBody]}>
          <Col
            size={3}
            style={{
              backgroundColor: 'white',
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: fontExtraBold,
                color: thameColors.primary,
                fontSize: 16,
              }}
            >
              09:10
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontReguler,
                paddingLeft: 5,
                color: '#a3a3a3',
              }}
            >
              CGK
            </Text>
          </Col>
          <Col
            size={3}
            style={{
              backgroundColor: 'white',
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('../../../assets/icons/plane3x-new.png')}
              style={{ width: 25, height: 20, resizeMode: 'center' }}
            />
          </Col>
          <Col
            size={3}
            style={{
              backgroundColor: 'white',
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: fontExtraBold,
                color: '#424242',
                fontSize: 16,
              }}
            >
              11:10
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontReguler,
                paddingLeft: 5,
                color: '#a3a3a3',
              }}
            >
              DPS
            </Text>
          </Col>
          <Col
            size={1}
            style={{
              backgroundColor: '#f8f8f8',
              alignItems: 'center',
              justifyContent: 'center',
              paddingLeft: 10,
              paddingRight: 10,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.handleModal(true);
              }}
            >
              <Icon
                name="ios-arrow-down"
                size={25}
                color={thameColors.primary}
              />
            </TouchableOpacity>
          </Col>
        </Grid>

        <Grid style={[styles.cardBody, { paddingTop: 10 }]}>
          <Col>
            <View>
              <Text
                style={{
                  fontSize: 13,
                  color: thameColors.primary,
                  fontFamily: fontExtraBold,
                }}
              >
                2 hrs 10 min
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontSize: 13,
                  color: '#b9bcb9',
                  fontFamily: fontReguler,
                }}
              >
                Business - Direct
              </Text>
            </View>
          </Col>
          <Col style={styles.sectionAlignEnd}>
            <Image source={require('../../../assets/icons/bag3x.png')} />
          </Col>
        </Grid>

        <Grid style={styles.cardFooter}>
          <Col size={6}>
            <Grid style={{ alignItems: 'center' }}>
              <Col size={1}>
                <Image source={require('../../../assets/icons/SJ2x.png')} />
              </Col>
              <Col size={9} style={{ paddingLeft: 15 }}>
                <Text style={styles.airlines}>{this.props.airline}</Text>
              </Col>
            </Grid>
          </Col>
          <Col
            size={4}
            style={{ alignItems: 'flex-end', justifyContent: 'center' }}
          >
            <Text style={styles.prices}>Rp. {this.props.price}</Text>
          </Col>
        </Grid>
      </TouchableOpacity>
    );
  }
}

export default CardResult;

const styles = StyleSheet.create({
  card: {
    // backgroundColor: "red",
    backgroundColor: '#fff',
    flex: 1,
    // width: deviceWidth / 1.15,
    height: 'auto',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    margin: 10,
  },
  cardHeader: {
    padding: 5,
  },
  cardBody: {
    padding: 10,
  },
  cardFooter: {
    padding: 10,
    borderTopWidth: 0.25,
    borderTopColor: '#e0e0e0',
  },
  sectionCenterText: {
    alignSelf: 'center',
  },
  sectionAlignCenter: {
    alignItems: 'center',
    transform: [{ translateY: 7 }],
  },
  sectionAlignEnd: {
    alignItems: 'flex-end',
  },
  textBlue: {
    color: '#00275d',
    fontFamily: fontReguler,
  },
  textGrey: {
    color: '#424242',
    fontFamily: fontReguler,
  },
  textOrage: {
    color: '#ed6d00',
    fontFamily: fontReguler,
  },
  textNote: {
    fontSize: 9,
    fontFamily: fontReguler,
  },
  text17: {
    fontSize: 17,
    fontFamily: fontReguler,
  },
  text19: {
    fontSize: 19,
    fontFamily: fontReguler,
  },
  airlines: {
    fontSize: 13,
    fontFamily: fontReguler,
  },
  prices: {
    color: '#ed6d00',
    fontFamily: fontExtraBold,
    fontSize: 16,
  },
});
