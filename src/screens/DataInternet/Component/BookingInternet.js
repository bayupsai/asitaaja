import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

//Import Component
import { Button } from '../../../elements/Button';
import HeaderPage from '../../../components/HeaderPage';
import BookingSummary from '../../../components/BookingSummary';
import SubHeaderPage from '../../../components/SubHeaderPage';

class BookingInternet extends PureComponent {
  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage title="Package Internet" callback={this.goBackHeader} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <BookingSummary
            labelHeader="Booking Summary Internet"
            labelPhoneNumber="Phone Number"
            phoneNumber="+62 812 7072 3917"
            labelProvider="Provider"
            provider="Telkomsel"
            labelPeriod="Active Period"
            activePeriod="30 Days"
            labelPaketData="Package Internet"
            paketData="3Gb/Month Rp. 25.000"
            labelTotal="Total Payment"
            total="Rp. 25.000"
          />
        </ScrollView>
        <View style={{ marginBottom: 10 }}>
          <Button
            {...this.props}
            label="CONTINUE PAYMENT"
            onClick={() => this.props.navigation.navigate('PaymentOrder')}
          />
        </View>
      </View>
    );
  }
}
export default BookingInternet;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
});
