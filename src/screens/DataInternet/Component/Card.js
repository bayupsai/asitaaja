import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { CheckBox } from 'react-native-elements';
import { fontReguler, fontExtraBold } from '../../../base/constant';

//local component

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class Card extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      saveNumbers: false,
    };
  }

  render() {
    return (
      <View style={{ flex: 1, borderStartColor: '#f0f0f0' }}>
        <Grid style={styles.card}>
          <Col style={styles.col}>
            <TouchableOpacity onPress={this.props.showModalNumber}>
              <View>
                <Text style={styles.textBody}>Enter Phone Number</Text>
              </View>
              <View>
                <Text style={styles.textBold}>+62 812 7072 3917</Text>
              </View>
            </TouchableOpacity>
          </Col>
        </Grid>

        <Grid style={styles.cardPackage}>
          <Col style={styles.col}>
            <TouchableOpacity onPress={this.props.showModal}>
              <View>
                <Text style={styles.textBody}>Select Package Internet</Text>
              </View>
              <View>
                <Text style={styles.textBold}>
                  {this.props.internetPackage}
                </Text>
              </View>
            </TouchableOpacity>
          </Col>
        </Grid>

        <Grid style={styles.cardPackage}>
          <Col size={0.5}></Col>
          <Col
            size={1.5}
            style={{ justifyContent: 'center', alignItems: 'center' }}
          >
            <CheckBox
              containerStyle={{
                backgroundColor: '#ffffff',
                padding: 0,
                margin: 0,
                marginLeft: 0,
                borderWidth: 0,
              }}
              checked={this.state.saveNumbers}
              onPress={() =>
                this.setState({ saveNumbers: !this.state.saveNumbers })
              }
              iconType="material-community"
              checkedIcon="checkbox-marked"
              uncheckedIcon="checkbox-blank-outline"
              checkedColor="#828282"
              uncheckedColor="#828282"
            />
          </Col>
          <Col
            size={8}
            style={{ justifyContent: 'center', alignItems: 'flex-start' }}
          >
            <Text style={styles.textBody}>Save Number for Next Purchase </Text>
          </Col>
        </Grid>

        <Grid style={styles.cardTotal}>
          <Col style={styles.col}>
            <View>
              <Text style={styles.textBody}>Total Payment</Text>
            </View>
            <View>
              <Text style={styles.textPrice}>
                {this.props.internetPackage}{' '}
              </Text>
            </View>
          </Col>
        </Grid>

        {/* KODINGAN BAYU */}
        {/* <Grid >
                    <Col>
                        <InputText
                            label="Phone Number"
                            placeholder="e.g 08xx-xxxx-xxxx"
                            onChangeText={(i) => console.log(i)}
                            keyboardType="number-pad"
                        />
                    </Col>
                </Grid>
                <Grid style={{ marginTop: 15 }}>
                    <Col>
                        <TouchableOpacity onPress={props.showModal}>
                            <InputText
                                label="Select Package Internet"
                                placeholder={props.internetPackage}
                                editable={false}
                            />
                        </TouchableOpacity>
                    </Col>
                </Grid>
                <Grid style={{ marginTop: 20 }}>
                    <Col>
                        <View style={styles.total}>
                            <Text style={[styles.textHeader, { marginBottom: 10 }]}>Total Price</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 16 }}>Package Name</Text>
                                <Text style={{ color: '#FF9800', fontSize: 16 }}>Rp. {props.totalPrice}</Text>
                            </View>
                        </View>
                    </Col>
                </Grid> */}
        {/* KODINGAN BAYU */}
      </View>
    );
  }
}

export default Card;

const styles = StyleSheet.create({
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: -45,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 9.8,
  },
  cardPackage: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 9.8,
  },
  cardTotal: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 40,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 5,
  },
  textBody: {
    fontSize: 16,
    color: '#828282',
    marginBottom: 4,
    fontFamily: fontReguler,
  },
  textBold: { fontSize: 20, color: '#222222', fontFamily: fontExtraBold },
  textPrice: { fontSize: 20, color: '#008d00', fontFamily: fontExtraBold },
  col: { justifyContent: 'center', alignItems: 'center' },
  /* Style KODINGAN BAYU */
  // total: {
  //     margin: 5,
  //     padding: 10,
  //     borderColor: '#BDBDBD',
  //     borderWidth: 2,
  //     borderRadius: 5
  // },
  // textHeader: {
  //     fontSize: 20,
  //     color: '#000'
  // }
  /* Style KODINGAN BAYU */
});
