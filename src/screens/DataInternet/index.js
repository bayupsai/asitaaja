import React, { PureComponent } from 'react';
import { View, Text, ScrollView } from 'react-native';
import numeral from 'numeral';

//local component
import styles from './styles';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { ButtonRounded } from '../../elements/Button';
import Card from './Component/Card';
import Modalin from './Component/Modalin';
import ModalPhoneNumber from './Component/ModalNumber';

class DataInternet extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      visibleModalNumber: null,
      phoneNumber: '',
      package: 1,
      packageList: [
        { id: 1, packageName: '1Gb/month', price: 10000 },
        { id: 2, packageName: '3Gb/month', price: 25000 },
        { id: 3, packageName: '5Gb/month', price: 40000 },
      ],
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  //modal nominal
  toggleModal = i => {
    this.setState({ visibleModal: i });
  };

  closeModal = () => {
    this.setState({ visibleModal: null });
  };

  setPackage = (type, value) => {
    if (type == 'package') {
      if (value > 0) {
        this.setState({ package: value });
      }
    }
  };
  //modal nominal

  //modal number
  toggleModalNumber = i => {
    this.setState({ visibleModalNumber: i });
  };

  closeModalNumber = () => {
    this.setState({ visibleModalNumber: null });
  };
  //modal number

  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          title="Package Internet"
          callback={this.goBack}
          {...this.props}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <SubHeaderPage />
          <Card
            {...this.props}
            showModal={() => this.toggleModal(1)}
            phoneNumber={i => this.setState({ phoneNumber: i })}
            phone={this.state.phoneNumber}
            showModalNumber={() => this.toggleModalNumber(2)}
            internetPackage={`${this.state.packageList[this.state.package]
              .packageName +
              ' ' +
              'Rp. ' +
              numeral(this.state.packageList[this.state.package].price).format(
                '0,0'
              )}`}
            totalPrice={numeral(
              this.state.packageList[this.state.package].price
            ).format('0,0')}
            selectedPackage={`${this.state.packageList[this.state.package]
              .packageName +
              ' ' +
              'Rp. ' +
              numeral(this.state.packageList[this.state.package].price).format(
                '0,0'
              )}`}
          />
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            {/* this.props.navigation.navigate("BookingInternet")*/}
            <ButtonRounded
              label="ORDER PACKAGE INTERNET"
              onClick={() => console.log('SS')}
            />
          </View>
        </ScrollView>

        {/* Modal Phone Number */}
        <Modalin
          {...this.props}
          visibleModal={this.state.visibleModal}
          closeModal={this.closeModal}
          package={0}
          setPackage={(data, selectedIndex) =>
            this.setState({ package: selectedIndex })
          }
        />
        {/* Modal Phone Number */}

        {/* Modal Nominal*/}
        <ModalPhoneNumber
          {...this.props}
          visibleModalNumber={this.state.visibleModalNumber}
          closeModalNumber={this.closeModalNumber}
          phoneNumber={i => this.setState({ phoneNumber: i })}
        />
        {/* Modal Nominal*/}
      </View>
    );
  }
}

export default DataInternet;
