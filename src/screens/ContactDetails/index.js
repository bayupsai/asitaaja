import React from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  Dimensions,
  View,
  FlatList,
  Alert,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
// Redux
import { actionContactDetail } from '../../redux/actions/FlightAction';
//Component
import { InputText, SearchInput } from '../../elements/TextInput';
import { Button } from '../../elements/Button';
import HeaderPage from '../../components/HeaderPage';
import SubHeaderPage from '../../components/SubHeaderPage';
import { fontReguler } from '../../base/constant/index';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

class ContactDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
      titleSelected: 1,
      listTitle: [
        { index: 1, title: 'Mr' },
        { index: 2, title: 'Mrs' },
        { index: 3, title: 'Ms' },
        { index: 4, title: 'Mstr' },
      ],
      title: 'Mr',
      fullName: '',
      countryCode: '+62',
      phoneNumber: '',
      emailAddress: '',
    };
  }

  // ======== SET DATA
  setTitlePassenger = data => {
    alert('wakwak');
    this.setState({
      title: data.title,
      visibleModal: null,
    });
  };

  handleInput = (type, data) => {
    if (type == 'fullName') {
      this.setState({ fullName: data });
    } else if (type == 'phoneNumber') {
      this.setState({ phoneNumber: data });
    } else if (type == 'emailAddress') {
      this.setState({ emailAddress: data });
    }
  };
  // ======== SET DATA

  goBackHeader = () => {
    this.props.navigation.goBack();
  };

  _toggleModal = modal => {
    this.setState({ visibleModal: modal });
  };

  informationPassengers = () => {
    this.props.navigation.navigate('PaymentOrder');
    // if(this.state.fullName == "" || this.state.phoneNumber == "" || this.state.emailAddress == "") {
    //     Alert.alert(
    //         'Warning',
    //         'Please enter data correctly!',
    //         [
    //           {text: 'OK'},
    //         ],
    //         { cancelable: false },
    //     );
    // } else {
    //     const payload = {
    //         title: this.state.title,
    //         fullName: this.state.fullName,
    //         countryCode: this.state.countryCode,
    //         phoneNumber: this.state.phoneNumber,
    //         emailAddress: this.state.emailAddress
    //     }
    //     this.props.dispatch(actionContactDetail(payload)).then((res) => {
    //         if(res.type == "SET_CONTACT_DETAIL") {
    //             this.props.navigation.navigate('PassengerInformation')
    //         }
    //     })
    // }
  };

  render() {
    return (
      <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
        <HeaderPage
          title="Flight"
          callback={this.goBackHeader()}
          {...this.props}
        />
        <SubHeaderPage title="Contact Details" />
        <Grid style={{ flex: 0 }}>
          <Row style={styles.content}>
            <Row style={styles.cardBox}>
              <Col size={7} style={{ padding: 15 }}>
                <Text style={{ color: 'black', fontFamily: fontReguler }}>
                  Login or Register
                </Text>
                <Text style={{ color: 'black', fontFamily: fontReguler }}>
                  to book faster and easier
                </Text>
              </Col>
              <Col style={styles.sectionButtonLogin} size={3}>
                <Button
                  size="small"
                  colorButton="buttonColor"
                  label="Login"
                  onClick={() => alert('Login')}
                />
              </Col>
            </Row>
          </Row>
        </Grid>
        <Grid style={[styles.sectionInput, { marginTop: -30 }]}>
          <Row>
            <Col size={3}>
              <TouchableOpacity onPress={() => this._toggleModal(1)}>
                <InputText
                  size="small"
                  placeholder={this.state.title}
                  label="Title *"
                  editable={false}
                />
              </TouchableOpacity>
            </Col>
            <Col size={7} style={{ marginLeft: 0 }}>
              <InputText
                placeholder="e.g Amir Budi"
                label="Fullname *"
                editable={true}
                onChangeText={data => this.handleInput('fullName', data)}
              />
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            {/* <Col size={2.5}>
                            <TouchableOpacity onPress={() => alert('xxx') }>
                                <InputText size="small" placeholder="(+62)" label="Code *" editable={false} onChangeText={(data) => this.handleCHange(data) }/>
                            </TouchableOpacity>
                        </Col> */}
            <Col>
              <InputText
                placeholder="e.g 02116133444"
                label="Phone Number *"
                editable={true}
                onChangeText={data => this.handleInput('phoneNumber', data)}
              />
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col>
              <InputText
                placeholder="e.g youremail@email.com"
                label="Email Address"
                editable={true}
                onChangeText={data => this.handleInput('emailAddress', data)}
              />
            </Col>
          </Row>
          <Row style={{ paddingTop: 10 }}>
            <Col size={6} style={styles.sectionButtonArea}>
              <Button label="Continue" onClick={this.informationPassengers} />
            </Col>
          </Row>

          {/* =============================== MODAL TITLE PASSENGER ===================================== */}
          <Modal
            useNativeDriver={true}
            hideModalContentWhileAnimating={true}
            isVisible={this.state.visibleModal === 1}
            style={styles.bottomModal}
          >
            <View style={styles.modalPassenger}>
              <Grid
                style={{
                  width: deviceWidth,
                  height: 40,
                  flex: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderBottomColor: '#838383',
                  borderBottomWidth: 0.5,
                }}
              >
                <TouchableOpacity
                  style={{ flex: 1, flexDirection: 'row' }}
                  onPress={() => this.setState({ visibleModal: null })}
                >
                  <Col
                    size={2}
                    style={{ alignItems: 'flex-start', paddingLeft: 10 }}
                  >
                    <Icon
                      name="close"
                      type="evilIcon"
                      color="#008195"
                      size={20}
                    />
                  </Col>
                  <Col
                    size={8}
                    style={{ alignItems: 'flex-start', paddingLeft: '20%' }}
                  >
                    <Text
                      style={{ color: '#000', fontWeight: '500', fontSize: 16 }}
                    >
                      Choose Title
                    </Text>
                  </Col>
                </TouchableOpacity>
              </Grid>
              <Grid style={{ padding: 10 }}>
                <Col style={{ paddingLeft: 10 }}>
                  {this.state.listTitle.map((data, index) => {
                    return (
                      <Grid
                        key={index}
                        style={{
                          flex: 1,
                          backgroundColor: '#FFFFFF',
                          padding: 10,
                          borderBottomColor: '#d5d5d5',
                          borderBottomWidth: 0.5,
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => this.setTitlePassenger(data)}
                          style={{ flex: 1 }}
                        >
                          <Col style={{ alignItems: 'flex-start' }}>
                            <Text
                              style={{
                                fontWeight: '400',
                                color: '#000',
                                fontSize: 16,
                              }}
                            >
                              {data.title}
                            </Text>
                          </Col>
                          <Col
                            style={{
                              alignItems: 'flex-end',
                              justifyContent: 'center',
                            }}
                          >
                            {this.state.titleSelected == data.index ? (
                              <Icon
                                name="check"
                                type="feather"
                                color="#008195"
                                size={22}
                              />
                            ) : (
                              <Text></Text>
                            )}
                          </Col>
                        </TouchableOpacity>
                      </Grid>
                    );
                  })}
                </Col>
              </Grid>
            </View>
          </Modal>
          {/* =============================== MODAL TITLE PASSENGER ===================================== */}
        </Grid>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    flight: state.flight,
    singleSearchFetch: state.flight.singleSearchFetch,
  };
}
export default connect(mapStateToProps)(ContactDetails);

const styles = StyleSheet.create({
  content: {
    paddingBottom: -10,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth,
    transform: [{ translateY: -25 }],
  },
  sectionButtonLogin: {
    paddingRight: 30,
  },
  sectionButtonArea: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  sectionInput: {
    paddingBottom: 10,
  },
  cardBox: {
    backgroundColor: '#fff',
    width: deviceWidth / 1.11,
    height: 70,
    borderRadius: 5,
    marginBottom: 25,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  // MODAL TITLE
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalPassenger: {
    backgroundColor: '#FFFFFF',
    padding: 0,
    height: deviceHeight / 2.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // MODAL TITLE
});
