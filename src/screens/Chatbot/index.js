import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  AsyncStorage,
} from 'react-native';
import { connect } from 'react-redux';
import Alert from '../../common/Alert';
import ListChats from '../../common/ListChats';
import ChatInput from '../../common/ChatInput';
import FlightDetailForm from '../../common/FlightDetailForm';
import RegisterChatForm from '../../common/RegisterChatForm';
import { moderateScale } from '../../Const/ScaleUtils';
import { dispatch } from '../../redux/store';
import Voice from 'react-native-voice';
import { USERDATACONFIG } from '../../Const/Config';

import Header from '../../components/Header';
import { thameColors } from '../../base/constant';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
let loadStore = [];

class Chatbot extends React.Component {
  constructor() {
    super();
    this.state = {
      token: '',
    };
  }

  componentWillMount() {
    Voice.stop();

    AsyncStorage.getItem(USERDATACONFIG).then(data => {
      const { load } = this.props.navigation.state.params;
      let user = JSON.parse(data);
      console.log('user : ', user);

      this.setState({
        token: user.access_token,
      });

      if (user.access_token != undefined) {
        if (loadStore.length < 1) {
          dispatch.main.getChat('menu');
          loadStore.push(load);
        } else {
          dispatch.main.stopRecord();
        }
      }
    }); //end asyncstorage
  }

  // componentDidMount(){
  //     const { load } = this.props.navigation.state.params;
  //     console.log('this.state token mount: ', this.state.token)
  //     if(this.state.token != '')
  //     {

  //     }
  // if(loadStore.length < 1)
  //     {
  //         dispatch.main.getChat('menu')
  //         loadStore.push(load)
  //     }
  //     else
  //     {
  //         dispatch.main.stopRecord()
  // }
  // }

  render() {
    const { main, navigation } = this.props;

    return (
      <ImageBackground
        source={require('../../assets/chat/image_background_doodle.png')}
        style={{ width: '100%', height: '100%' }}
      >
        <View style={styles.container}>
          <Alert />
          <Header {...this.props} />
          {/* <View style={{backgroundColor:'#0063b4', height:moderateScale(20)}}></View>
                    <View style={styles.navbar}>
                        <View style={{width:deviceWidth-50,marginLeft:moderateScale(40),justifyContent:'center', alignItems:'center'}}>
                            <Text style={{color:'#fff',fontSize:moderateScale(20), fontWeight:'700'}}><Text style={styles.hajiText}>Aero </Text>Aja</Text>
                        </View>
                    </View> */}

          <ListChats
            data={main.data}
            isKeyboardShow={main.keyboardMode}
            navigation={navigation}
            scrollOnKeyboardShow="bottom-end"
          />

          <ChatInput />
          <FlightDetailForm />

          {this.state.token == undefined || this.state.token == '' ? (
            <RegisterChatForm />
          ) : (
            console.log('token get')
          )}
        </View>
      </ImageBackground>
    );
  }
}

export default connect(({ main, user, wallet, nav }) => ({
  main,
  user,
  wallet,
  nav,
}))(Chatbot);

const styles = StyleSheet.create({
  loading: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  navbar: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: moderateScale(50),
    width: deviceWidth,
    backgroundColor: '#0063b4',
  },
  toggleKeyboard: {
    width: 40,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 10,
  },
  badgeContainer: {
    width: 23,
    height: 23,
    backgroundColor: 'red',
    position: 'absolute',
    bottom: 8,
  },
  hajiText: {
    fontWeight: '700',
    fontSize: moderateScale(20),
    color: '#F4D50C',
  },
});
