import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Icon } from 'react-native-elements';
import { fontReguler } from '../../../base/constant';

class Card extends React.PureComponent {
  render() {
    return (
      <Grid style={styles.card} {...this.props}>
        <Col
          size={2}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          {/* <Image
                        source={require('../../../assets/icons/cargo.png')}
                        style={{ width: 50, height: 50 }}
                    /> */}
          <Icon
            type={this.props.type == 'cargo' ? 'feather' : 'material-community'}
            name={
              this.props.type == 'cargo'
                ? 'box'
                : this.props.type == 'flight'
                ? 'airplane-takeoff'
                : this.props.type == 'hotel'
                ? 'hotel'
                : ''
            }
            color="#4CAF50"
            size={50}
          />
        </Col>
        <Col size={5}>
          <Text style={styles.textTitle}>{this.props.title}</Text>
          <Text style={{ fontFamily: fontReguler }}>Updated 2 hours ago</Text>
        </Col>
        <Col
          size={3}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          {this.props.type !== 'cargo' ? (
            <TouchableOpacity
              style={{
                width: 75,
                height: 25,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#009688',
              }}
            >
              <Text style={styles.textTime}>Check In</Text>
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </Col>
      </Grid>
    );
  }
}

export default Card;

const styles = StyleSheet.create({
  card: {
    marginTop: 25,
    marginLeft: 20,
    marginRight: 20,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#FFF',
  },
  textTitle: {
    color: '#000',
    fontFamily: fontReguler,
  },
  textTime: {
    fontSize: 12,
    color: '#fff',
    fontFamily: fontReguler,
  },
});
