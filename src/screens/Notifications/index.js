import React, { PureComponent } from 'react';
import { View, Text, ScrollView, FlatList } from 'react-native';

//import some components
import HeaderPage from '../../components/HeaderPage';
import styles from './styles';
import Card from './Component/Card';

class Notifications extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataCard: [
        { id: 1, title: 'Your cargo already sent by Go-Jek', type: 'cargo' },
        {
          id: 2,
          title:
            'Your flight is gonna be ready in 2 hours, you can check in now',
          type: 'flight',
        },
        { id: 3, title: 'Your hotel will be destroy', type: 'hotel' },
      ],
    };
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _renderItem = ({ item, i }) => (
    <Card key={i} title={item.title} type={item.type} />
  );
  render() {
    return (
      <View style={styles.container}>
        <HeaderPage
          callback={this._goBack}
          title="Notifications"
          {...this.props}
        />
        <ScrollView>
          <FlatList
            data={this.state.dataCard}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </View>
    );
  }
}

export default Notifications;
