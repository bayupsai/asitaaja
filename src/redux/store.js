// import { createStore, applyMiddleware} from 'redux'
// import { persistStore, persistReducer } from 'redux-persist'
// import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
// import storage from 'redux-persist/lib/storage'
// import logger from 'redux-logger'
// import thunk from 'redux-thunk'

// import RootReducer from './reducers/index'

// const persistConfig = {
//     timeout: 10000,
//     key: 'root',
//     storage: storage,
//     stateReconciler: autoMergeLevel2
// };

// const persistedReducer = persistReducer(persistConfig, RootReducer);

// export const store = createStore(persistedReducer, applyMiddleware(logger, thunk));
// export const persistor = persistStore(store);
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import AsyncStorage from '@react-native-community/async-storage';

import storeReducer from './reducers/index';

const persistConfig = {
  timeout: 10000,
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
};

// const persistedReducer = persistReducer(persistConfig, RootReducer);

export const store = storeReducer;
export const { dispatch, getState } = storeReducer;
// export const persistor = persistStore(store);
