const INITIAL_STATE = {
  message: '',
  geolocationRegion: '',
  geolocationLat: '',
  geolocationLang: '',
};

export default function GlobalReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'GET_MESSAGE': {
      return {
        ...state,
        message: action.data,
      };
    }
    case 'SET_LOCATION': {
      return {
        ...state,
        geolocationRegion: action.region,
        geolocationLat: action.lat,
        geolocationLang: action.lang,
      };
    }
    default:
      return state;
  }
}
