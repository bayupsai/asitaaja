const initial_state = {
  payment: {},
  isLoading: false,
  payload: {},
};

export default function PaymentMidtransReducer(state = initial_state, action) {
  switch (action.type) {
    case 'PAYMENT_MIDTRANS':
      return {
        ...state,
        isLoading: true,
      };

    case 'PAYMENT_MIDTRANS_SUCCESS':
      return {
        ...state,
        isLoading: false,
        payment: action.data,
        payload: action.payload,
      };

    case 'PAYMENT_MIDTRANS_FAILED':
      return {
        ...state,
        isLoading: false,
        payment: {},
      };

    default:
      return state;
  }
}
