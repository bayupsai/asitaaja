const INITIAL_STATE = {
  isLogin: false,
  fetchingLogin: false,
  data: null,
};

export default function LoginReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'LOGIN_MAIL': {
      return {
        ...state,
        fetchingLogin: true,
      };
    }
    case 'LOGIN_MAIL_SUCCESS': {
      return {
        ...state,
        fetchingLogin: false,
        data: action.data,
        isLogin: true,
      };
    }
    case 'LOGIN_MAIL_FAILED': {
      return {
        ...state,
        fetchingLogin: false,
        isLogin: false,
        data: null,
      };
    }

    default:
      return state;
  }
}
