const initialState = {
  plnPost: {},
  payloadPlnPost: {},
  loadPlnPost: false,

  plnPostPay: {},
  payloadPlnPostPay: {},
  loadPlnPostPay: false,

  pulsaFin: {},
  payloadPulsaFin: {},
  loadPulsaFin: false,
};

const BillsReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'PLNPOST':
      return {
        ...state,
        loadPlnPost: true,
      };

    case 'PLNPOST_SUCCESS':
      return {
        ...state,
        loadPlnPost: false,
        plnPost: action.data,
        payloadPlnPost: action.payload,
      };

    case 'PLNPOST_FAILED':
      return {
        ...state,
        loadPlnPost: false,
        plnPost: {},
        payloadPlnPost: action.payload,
      };

    // Pay
    case 'PLNPOSTPAY':
      return {
        ...state,
        loadPlnPostPay: true,
      };

    case 'PLNPOSTPAY_SUCCESS':
      return {
        ...state,
        loadPlnPostPay: false,
        plnPostPay: action.data,
        payloadPlnPostPay: action.payload,
      };

    case 'PLNPOSTPAY_FAILED':
      return {
        ...state,
        loadPlnPostPay: false,
        plnPostPay: {},
        payloadPlnPostPay: action.payload,
      };

    // Pulsa Fin
    case 'PULSAFIN':
      return {
        ...state,
        loadPulsaFin: true,
      };

    case 'PULSAFIN_SUCCESS':
      return {
        ...state,
        loadPulsaFin: false,
        pulsaFin: action.data,
        payloadPulsaFin: action.payload,
      };

    case 'PULSAFIN_FAILED':
      return {
        ...state,
        loadPulsaFin: false,
        pulsaFin: {},
        payloadPulsaFin: action.payload,
      };
    default:
      return state;
  }
};

export default BillsReducer;
