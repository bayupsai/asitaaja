const INITIAL_STATE = {
  //universal
  data: {},
  dataChangePassword: {},
  //getProfile
  payloadGet: {},
  loadingGet: false,
  //updateProfile
  payloadUpdate: {},
  //refreshProfile
  payloadRefresh: {},
  payloadChangePassword: {},

  isLoading: false,
  isLoadingChangePassword: false,
  isLogin: false,
  token: '',
};

export default function ProfileReducer(state = INITIAL_STATE, action) {
  let { type, token, data, payload } = action;
  switch (type) {
    // get profile
    case 'GET_PROFILE': {
      return {
        ...state,
        loadingGet: true,
      };
    }
    case 'GET_PROFILE_SUCCESS': {
      return {
        ...state,
        loadingGet: false,
        data: data,
        payloadGet: payload,
        isLogin: true,
        token: token,
      };
    }
    case 'GET_PROFILE_FAIELD': {
      return {
        ...state,
        loadingGet: false,
        data: {},
        isLogin: false,
        token: '',
      };
    }

    //update Profile
    case 'UPDATE_PROFILE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'UPDATE_PROFILE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: data,
        payloadUpdate: payload,
        isLogin: true,
        token: token,
      };
    }
    case 'UPDATE_PROFILE_FAILED': {
      return {
        ...state,
        isLoading: false,
        data: data,
        isLogin: false,
        token: '',
      };
    }

    // refresh Profile
    case 'REFRESH_PROFILE_PROFILE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'REFRESH_PROFILE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: data,
        payloadRefresh: payload,
        isLogin: true,
        token: token,
      };
    }
    case 'REFRESH_PROFILE_FAILED': {
      return {
        ...state,
        isLoading: false,
        data: data,
        isLogin: false,
        token: 'token',
      };
    }

    //Change Password Login
    case 'CHANGE_PASSWORD_LOGIN': {
      return {
        ...state,
        isLoadingChangePassword: true,
      };
    }
    case 'CHANGE_PASSWORD_LOGIN_SUCCESS': {
      return {
        ...state,
        isLoadingChangePassword: false,
        dataChangePassword: data,
        payloadChangePassword: payload,
      };
    }
    case 'CHANGE_PASSWORD_LOGIN_FAILED': {
      return {
        ...state,
        isLoadingChangePassword: false,
        dataChangePassword: data,
        payloadChangePassword: payload,
      };
    }

    default:
      return state;
  }
}
