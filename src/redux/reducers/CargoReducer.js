const INITIAL_STATE = {
  senderData: {},
  receiverData: {},
  nearestBox: {},
  // originBoxResult: [],
  // destinationBoxResult: [],
  fetchingNearestBox: false,
  fetchingPrice: false,
  pacisPriceData: {},
  boxSelected: {},
  documentSelected: {},
  fetchingBooking: false,
  fetchingPayment: false,
  pacisBooking: {},
  pacisPayment: {},
};

export default function CargoReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'SET_SENDER_CARGO': {
      return {
        ...state,
        senderData: action.payload,
      };
    }

    case 'SET_RECEIVER_CARGO': {
      return {
        ...state,
        receiverData: action.payload,
      };
    }

    case 'SET_PACKAGE_SIZE': {
      return {
        ...state,
        boxSelected: action.payload,
      };
    }

    case 'SET_DOCUMENTS': {
      return {
        ...state,
        documentSelected: action.payload,
      };
    }

    case 'FETCH_LISTBOX': {
      return {
        ...state,
        fetchingNearestBox: true,
      };
    }

    case 'FETCH_LISTBOX_SUCCESS': {
      return {
        ...state,
        nearestBox: action.data,
        fetchingNearestBox: false,
      };
    }

    case 'FETCH_PRICE': {
      return {
        ...state,
        fetchingPrice: true,
      };
    }

    case 'FETCH_PRICE_SUCCESS': {
      return {
        ...state,
        pacisPriceData: action.data,
        fetchingPrice: false,
      };
    }

    case 'FETCH_BOOK': {
      return {
        ...state,
        fetchingBooking: true,
      };
    }

    case 'PACIS_BOOK_SUCCESS': {
      return {
        ...state,
        fetchingBooking: false,
        pacisBooking: action.data,
      };
    }

    case 'FETCH_PAY': {
      return {
        ...state,
        fetchingPayment: true,
      };
    }

    case 'PACIS_PAY_SUCCESS': {
      return {
        ...state,
        fetchingPayment: false,
        pacisPayment: action.data,
      };
    }

    default:
      return state;
  }
}
