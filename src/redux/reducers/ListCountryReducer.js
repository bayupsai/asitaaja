const initial_state = {
  fetching: false,
  dataCountry: [],
  limit: 1000,
};

export default function ListCountryReducer(state = initial_state, action) {
  let { type, data, limit } = action;
  switch (type) {
    case 'LIST_COUNTRY': {
      return {
        ...state,
        fetching: true,
        limit,
      };
    }
    case 'LIST_COUNTRY_SUCCESS': {
      return {
        ...state,
        fetching: false,
        dataCountry: data,
        limit,
      };
    }
    case 'LIST_COUNTRY_FAILED': {
      return {
        ...state,
        fetching: false,
        dataCountry: [],
        limit,
      };
    }
    default:
      return state;
  }
}
