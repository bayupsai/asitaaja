const INITIAL_STATE = {
  //store data
  registerApplyData: {},
  registerConfirmData: {},
  registerNowData: {},
  mobileApplyData: {},
  mobileConfirmData: {},

  //payload
  payloadApply: {},
  payloadConfirm: {},
  payloadNow: {},
  payloadMobileApply: {},
  payloadMobileConfirm: {},

  //fetching process
  fetchingRegister: false,
};

export default function RegisterReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    //fetching
    case 'REGISTER_APPLY': {
      return {
        ...state,
        fetchingRegister: true,
      };
    }
    case 'REGISTER_CONFIRM': {
      return {
        ...state,
        fetchingRegister: true,
      };
    }
    case 'REGISTER_NOW': {
      return {
        ...state,
        fetchingRegister: true,
      };
    }
    case 'MOBILE_APPLY': {
      return {
        ...state,
        fetchingRegister: true,
      };
    }
    case 'MOBILE_CONFIRM': {
      return {
        ...state,
        fetchingRegister: true,
      };
    }

    //success
    case 'REGISTER_APPLY_SUCCESS': {
      return {
        ...state,
        fetchingRegister: false,
        registerApplyData: action.data,
        payloadApply: action.payload,
      };
    }
    case 'REGISTER_CONFIRM_SUCCESS': {
      return {
        ...state,
        fetchingRegister: false,
        registerConfirmData: action.data,
        payloadConfirm: action.payload,
      };
    }
    case 'REGISTER_NOW_SUCCESS': {
      return {
        ...state,
        fetchingRegister: false,
        registerNowData: action.data,
        payloadNow: action.payload,
      };
    }
    case 'MOBILE_APPLY_SUCCESS': {
      return {
        ...state,
        fetchingRegister: false,
        mobileApplyData: action.data,
        payloadMobileApply: action.payload,
      };
    }
    case 'MOBILE_CONFIRM_SUCCESS': {
      return {
        ...state,
        fetchingRegister: false,
        mobileConfirmData: action.data,
        payloadMobileConfirm: action.payload,
      };
    }

    //failed
    case 'REGISTER_APPLY_FAILED': {
      return {
        ...state,
        fetchingRegister: false,
      };
    }
    case 'REGISTER_CONFIRM_FAILED': {
      return {
        ...state,
        fetchingRegister: false,
      };
    }
    case 'REGISTER_NOW_FAILED': {
      return {
        ...state,
        fetchingRegister: false,
      };
    }
    case 'MOBILE_APPLY_FAILED': {
      return {
        ...state,
        fetchingRegister: false,
      };
    }
    case 'MOBILE_CONFIRM_FAILED': {
      return {
        ...state,
        fetchingRegister: false,
      };
    }

    default:
      return state;
  }
}
