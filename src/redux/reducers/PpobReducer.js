const INITIAL_STATE = {
  fetching: false,
  // pulsa
  dataPulsa: {},
  payloadPulsa: {},
  stateDataPulsa: {},

  // Pulsa Price
  dataPulsaPrice: {},
  payloadPulsaPrice: {},

  // Inquiry PLN Postpaid
  plnPostpaid: {},
  payloadPlnPostpaid: {},

  // Book PLN Postpaid (Payment);
  bookPlnPostpaid: {},
  payloadBookPlnPostpaid: {},

  // Search PDAM
  searchPdam: [],
  payloadSearchPdam: {},
};

export default function PpobReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    //pulsa
    case 'PULSA': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'PULSA_SUCCESS': {
      return {
        ...state,
        fetching: false,
        dataPulsa: action.data,
        payloadPulsa: action.payload,
        stateDataPulsa: action.stateData,
      };
    }
    case 'PULSA_FAILED': {
      return {
        ...state,
        fetching: false,
        dataPulsa: {},
        payloadPulsa: {},
        stateDataPulsa: {},
      };
    }

    // Pulsa Price
    case 'PULSA_PRICE':
      return {
        ...state,
        fetching: true,
      };

    case 'PULSA_PRICE_SUCCESS':
      return {
        ...state,
        fetching: false,
        dataPulsaPrice: action.data,
        payloadPulsaPrice: action.payload,
      };

    case 'PULSA_PRICE_FAILED':
      return {
        ...state,
        fetching: false,
        dataPulsaPrice: {},
      };

    // PLN Postpaid
    case 'PLN_POSTPAID':
      return {
        ...state,
        fetching: true,
      };

    case 'PLN_POSTPAID_SUCCESS':
      return {
        ...state,
        fetching: false,
        plnPostpaid: action.data,
        payloadPlnPostpaid: action.payload,
      };

    case 'PLN_POSTPAID_FAILED':
      return {
        ...state,
        fetching: false,
        plnPostpaid: action.data,
      };

    // Book PLN Postpaid
    case 'BOOK_PLN_POST':
      return {
        ...state,
        fetching: true,
      };

    case 'BOOK_PLN_POST_SUCCESS':
      return {
        ...state,
        fetching: false,
        bookPlnPostpaid: action.data,
        payloadBookPlnPostpaid: action.payload,
      };

    case 'BOOK_PLN_POST_FAILED':
      return {
        ...state,
        fetching: false,
        bookPlnPostpaid: {},
      };

    case 'SEARCH_PDAM':
      return {
        ...state,
        fetching: true,
      };

    case 'SEARCH_PDAM_SUCCESS':
      return {
        ...state,
        fetching: false,
        searchPdam: action.data,
        payloadSearchPdam: action.payload,
      };

    case 'SEARCH_PDAM_FAILED':
      return {
        ...state,
        fetching: false,
        searchPdam: [],
      };

    default:
      return state;
  }
}
