const INITIAL_STATE = {
  isConnected: false,
};

export default function ConnectionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'CHANGE_CONNECTION_STATUS':
      return Object.assign({}, state, {
        isConnected: action.status,
      });

    default:
      return state;
  }
}
