const INITIAL_STATE = {
  //data
  stationSearch: {},
  newTrain: {},
  searchTrain: {},
  dataTrain: {},
  seatMap: {},
  bookTrain: {},
  departureDate: 'XXX', // YYYY-MM-DD
  returnDate: '', // YYYY-MM-DD
  trainCalendarList: [],
  //payload
  payloadStation: {},
  payloadNewTrain: {},
  payloadTrain: {},
  payloadSeat: {},
  payloadBook: {},
  //loading process
  fetchingStation: false,
  fetchingTrain: false,
  fetchingSeat: false,
  fetchingBook: false,
  //failed
  failedStation: {},
  failedSearch: {},
  failedSeat: {},
  failedBook: {},
  contactDetail: {},
  passengerData: [],
  trainSelected: {},
  trainReturnSelected: {},
};

export default function TrainReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    //SEARCH STATION
    case 'STATION_SEARCH': {
      return {
        ...state,
        fetchingStation: true,
      };
    }
    case 'STATION_SEARCH_SUCCESS': {
      return {
        ...state,
        fetchingStation: false,
        stationSearch: action.data,
        payloadStation: action.payload,
      };
    }
    case 'STATION_SEARCH_FAILED': {
      return {
        ...state,
        fetchingStation: false,
        failedStation: action.data,
        trainCalendarList: [],
      };
    }
    //SEARCH STATION

    //SEARCH TRAIN
    case 'SEARCH_TRAIN': {
      return {
        ...state,
        fetchingTrain: true,
      };
    }
    case 'SEARCH_TRAIN_SUCCESS': {
      return {
        ...state,
        fetchingTrain: false,
        searchTrain: action.data,
        payloadTrain: action.payload,
        departureDate: action.payload.departure_date,
        // returnDate: action.payload.data.return_date
        //   ? action.payload.data.return_date
        //   : '',
        // trainCalendarList: action.trainCalendarList
      };
    }

    case 'SEARCH_TRAIN_FAILED': {
      return {
        ...state,
        fetchingTrain: false,
        failedSearch: action.data,
        contactDetail: [],
        passengerData: [],
        trainSelected: {},
        trainReturnSelected: {},
        searchTrain: {},
        trainCalendarList: [],
      };
    }
    //SEARCH TRAIN

    //SEAT MAP
    case 'SEAT_MAP': {
      return {
        ...state,
        fetchingSeat: true,
      };
    }
    case 'SEAT_MAP_SUCCESS': {
      return {
        ...state,
        fetchingSeat: false,
        seatMap: action.data,
        payloadSeat: action.payload,
      };
    }
    case 'SEAT_MAP_FAILED': {
      return {
        ...state,
        fetchingSeat: false,
        failedSeat: action.data,
      };
    }
    //SEAT MAP

    //BOOK TRAIN
    case 'BOOK_TRAIN': {
      return {
        ...state,
        fetchingBook: true,
      };
    }
    case 'BOOK_TRAIN_SUCCESS': {
      return {
        ...state,
        fetchingBook: false,
        bookTrain: action.data,
        payloadBook: action.payload,
      };
    }
    case 'BOOK_TRAIN_FAILED': {
      return {
        ...state,
        fetchingBook: false,
        failedBook: action.data,
      };
    }
    //BOOK TRAIN

    //SET DATA
    case 'SET_CONTACT_DETAIL': {
      return {
        ...state,
        contactDetail: action.payload,
      };
    }

    case 'SET_PASSENGER': {
      return {
        ...state,
        passengerData: action.payload,
      };
    }
    case 'SET_FLIGHT_SELECTED': {
      return {
        ...state,
        trainSelected: action.payload,
      };
    }

    case 'SET_FLIGHT_RETURN_SELECTED': {
      return {
        ...state,
        trainReturnSelected: action.payload,
      };
    }
    //SET DATA

    case 'NEW_TRAIN': {
      return {
        ...state,
        fetchingTrain: true,
      };
    }

    case 'NEW_TRAIN_SUCCESS': {
      return {
        ...state,
        fetchingTrain: false,
        newTrain: action.data,
        payloadNewTrain: action.payload,
      };
    }

    case 'NEW_TRAIN_FAILED': {
      return {
        ...state,
        fetchingTrain: false,
        newTrain: {},
        payloadNewTrain: action.payload,
      };
    }

    default:
      return state;
  }
}
