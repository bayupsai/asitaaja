const initial_state = {
  tokenFacebook: '',
  tokenGoogle: '',
  dataFacebook: {},
  payloadFacebook: {},
  dataGoogle: {},
  payloadGoogle: {},
  isLogin: false,
  fetching: false,
  authType: '',
};

export default function SocmedReducer(state = initial_state, action) {
  let { data, token, type, payload, message } = action;
  switch (type) {
    //Getting Token Socmed
    case 'TOKEN_FACEBOOK': {
      return {
        ...state,
        tokenFacebook: token,
      };
    }
    case 'TOKEN_GOOGLE': {
      return {
        ...state,
        tokenGoogle: token,
      };
    }

    //Login Socmed - Facebook
    case 'LOGIN_FACEBOOK': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'LOGIN_FACEBOOK_SUCCESS': {
      return {
        ...state,
        fetching: false,
        dataFacebook: data,
        payloadFacebook: payload,
        isLogin: true,
        authType: 'facebook',
      };
    }
    case 'LOGIN_FACEBOOK_FAILED': {
      return {
        ...state,
        fetching: false,
        dataFacebook: message,
        isLogin: false,
        tokenGoogle: '',
        authType: '',
      };
    }
    //Login Socmed - Google
    case 'LOGIN_GOOGLE': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'LOGIN_GOOGLE_SUCCESS': {
      return {
        ...state,
        fetching: false,
        dataGoogle: data,
        payloadGoogle: payload,
        isLogin: true,
        authType: 'google',
      };
    }
    case 'LOGIN_GOOGLE_FAILED': {
      return {
        ...state,
        fetching: false,
        dataGoogle: message,
        isLogin: false,
        tokenGoogle: '',
        authType: '',
      };
    }

    //Logging out Socmed
    case 'LOGOUT_FACEBOOK': {
      return {
        ...state,
        isLogin: false,
        dataFacebook: {},
        tokenFacebook: '',
        payloadFacebook: {},
        authType: '',
      };
    }
    case 'LOGOUT_GOOGLE': {
      return {
        ...state,
        isLogin: false,
        dataGoogle: {},
        tokenGoogle: '',
        payloadGoogle: {},
        authType: '',
      };
    }
    default:
      return state;
  }
}
