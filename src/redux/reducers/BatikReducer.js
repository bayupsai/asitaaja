const initial_state = {
  batikAll: [],

  batikDetail: {},
  batikDetailPayload: {},

  batikBook: [],
  batikBookPayload: {},

  batikPay: {},
  batikPayPayload: {},

  fetching: false,
};

export default function BatikReducer(state = initial_state, action) {
  let { type, data, payload } = action;

  switch (type) {
    // SHOW ALL BATIK
    case 'BATIK_ALL': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'BATIK_ALL_SUCCESS': {
      return {
        ...state,
        fetching: false,
        batikAll: data,
        batikAllPayload: payload,
      };
    }
    case 'BATIK_ALL_FAILED': {
      return {
        ...state,
        fetching: false,
        batikAll: [],
        batikAllPayload: payload,
      };
    }

    // SHOW DETAIL BATIK
    case 'BATIK_DETAIL': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'BATIK_DETAIL_SUCCESS': {
      return {
        ...state,
        fetching: false,
        batikDetail: data,
        batikDetailPayload: payload,
      };
    }
    case 'BATIK_DETAIL_FAILED': {
      return {
        ...state,
        fetching: false,
        batikDetail: {},
        batikDetailPayload: payload,
      };
    }

    // BOOKING BATIK
    case 'BATIK_BOOK': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'BATIK_BOOK_SUCCESS': {
      return {
        ...state,
        fetching: false,
        batikBook: data,
        batikBookPayload: payload,
      };
    }
    case 'BATIK_BOOK_FAILED': {
      return {
        ...state,
        fetching: false,
        batikBook: {},
        batikBookPayload: payload,
      };
    }

    // PAY BATIK
    case 'BATIK_PAY': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'BATIK_PAY_SUCCESS': {
      return {
        ...state,
        fetching: false,
        batikPay: data,
        batikPayPayload: payload,
      };
    }
    case 'BATIK_PAY_FAILED': {
      return {
        ...state,
        fetching: false,
        batikPay: {},
        batikPayPayload: payload,
      };
    }

    default:
      return state;
  }
}
