const initial_state = {
  dataForgot: {},
  dataVerify: {},
  dataChange: {},
  payloadForgot: {},
  payloadVerify: {},
  payloadChange: {},
  isLoading: false,
};

export default function ForgotReducer(state = initial_state, action) {
  let { data, payload, message, type } = action;
  switch (type) {
    //Forgot Password
    case 'FORGOT_PASSWORD': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'FORGOT_PASSWORD_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        dataForgot: data,
        payloadForgot: payload,
      };
    }
    case 'FORGOT_PASSWORD_FAILED': {
      return {
        ...state,
        isLoading: false,
        dataForgot: message,
        payloadForgot: {},
      };
    }

    //Verify Forgot Password
    case 'VERIFY_FORGOT': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'VERIFY_FORGOT_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        dataVerify: data,
        payloadVerify: payload,
      };
    }
    case 'VERIFY_FORGOT_FAILED': {
      return {
        ...state,
        isLoading: false,
        dataVerify: message,
        payloadVerify: {},
      };
    }

    // Change password by forgot password
    case 'CHANGE_FORGOT': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'CHANGE_FORGOT_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        dataChange: data,
        payloadChange: payload,
      };
    }
    case 'CHANGE_FORGOT_FAILED': {
      return {
        ...state,
        isLoading: false,
        dataChange: message,
        payloadChange: payload,
      };
    }

    default:
      return state;
  }
}
