const initial_state = {
  fetching: false,
  data: [],
  message: '',
  selectOrder: {},
};

function OrdersHistoryReducer(state = initial_state, action) {
  let { data, message, type, token } = action;
  switch (type) {
    case 'ORDERS_HISTORY': {
      return {
        ...state,
        fetching: true,
      };
    }
    case 'ORDERS_HISTORY_SUCCESS': {
      return {
        ...state,
        fetching: false,
        data,
        token,
      };
    }
    case 'ORDERS_HISTORY_FAILED': {
      return {
        ...state,
        fetching: false,
        data: [],
        message,
        token,
      };
    }
    case 'SELECT_ORDER': {
      return {
        ...state,
        selectOrder: data,
      };
    }
    default:
      return state;
  }
}

export default OrdersHistoryReducer;
