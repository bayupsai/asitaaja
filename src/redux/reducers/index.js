// import { combineReducers } from 'redux'
// import GlobalReducer from './globalReducer'
// import FlightReducer from './FlightReducer'
// import HotelReducer from './HotelReducer'
// import HotelSearchReducer from './HotelSearchReducer'
// const RootReducer = combineReducers({
//   global: GlobalReducer,
//   flight: FlightReducer,
//   hotel: HotelReducer,
//   HotelSearchReducer: HotelSearchReducer
// })
// export default RootReducer
import { init } from '@rematch/core';
// import { combineReducers } from 'redux'
import GlobalReducer from './globalReducer';
import ConnectionReducer from './ConnectionReducer';
import FlightReducer from './FlightReducer';
import HotelReducer from './HotelReducer';
import RegisterReducer from './RegisterReducer';
import CargoReducer from './CargoReducer';
import TrainReducer from './TrainReducer';
import LoginReducer from './LoginReducer';
import ProfileReducer from './ProfileReducer';
import PpobReducer from './PpobReducer';
import AttractionReducer from './AttractionReducer';
import SocmedReducer from './SocmedReducer';
import ForgotReducer from './ForgotReducer';
import BatikReducer from './BatikReducer';
import PaymentMidtransReducer from './PaymentReducer';
import BillsReducer from './BillsReducer';

import models from '../../Models';
import thunk from 'redux-thunk';
// import storage from 'redux-persist/lib/storage';
import AsyncStorage from '@react-native-community/async-storage';
import logger from 'redux-logger';
import createRematchPersist from '@rematch/persist';
import ListCountryReducer from './ListCountryReducer';
import OrdersHistoryReducer from './OrdersHistoryReducer';

const persistPlugin = createRematchPersist({
  key: 'root',
  storage: AsyncStorage,
  throttle: 500,
  version: 1,
});
const storeReducer = init({
  models,
  plugins: [persistPlugin],
  redux: {
    reducers: {
      connection: ConnectionReducer,
      global: GlobalReducer,
      flight: FlightReducer,
      hotel: HotelReducer,
      register: RegisterReducer,
      cargo: CargoReducer,
      train: TrainReducer,
      login: LoginReducer,
      profile: ProfileReducer,
      ppob: PpobReducer,
      attraction: AttractionReducer,
      socmed: SocmedReducer,
      forgot: ForgotReducer,
      listCountry: ListCountryReducer,
      ordersHistory: OrdersHistoryReducer,
      batik: BatikReducer,
      paymentMidtrans: PaymentMidtransReducer,
      bills: BillsReducer,
    },
    middlewares: [thunk, logger],
  },
});

export default storeReducer;
