const INITIAL_STATE = {
  detailActivities: {},
  detailMovies: {},
  detailEvents: {},
  dateActivities: '',
  dateMovies: '',
  dateEvents: '',
  ticketActivities: [],
  ticketMovies: [],
  ticketEvents: [],
};

export default function AttractionReducer(state = INITIAL_STATE, action) {
  let { data, date } = action;
  switch (action.type) {
    //select details
    case 'DETAIL_ACTIVITIES': {
      return {
        ...state,
        detailActivities: data,
      };
    }
    case 'DETAIL_MOVIES': {
      return {
        ...state,
        detailMovies: data,
      };
    }
    case 'DETAIL_EVENTS': {
      return {
        ...state,
        detailEvents: data,
      };
    }

    //select date
    case 'SET_DATE_ACTIVITIES': {
      return {
        ...state,
        dateActivities: date,
      };
    }
    case 'SET_DATE_MOVIES': {
      return {
        ...state,
        dateMovies: date,
      };
    }
    case 'SET_DATE_EVENTS': {
      return {
        ...state,
        dateEvents: date,
      };
    }

    //select Ticket
    case 'TICKET_ACTIVITIES': {
      return {
        ...state,
        ticketActivities: data,
      };
    }
    case 'TICKET_MOVIES': {
      return {
        ...state,
        ticketMovies: data,
      };
    }
    case 'TICKET_EVENTS': {
      return {
        ...state,
        ticketEvents: data,
      };
    }

    default:
      return state;
  }
}
