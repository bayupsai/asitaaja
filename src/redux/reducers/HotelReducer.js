const INITIAL_STATE = {
  // List Hotel CIty
  listHotelCity: [],
  payloadHotelCity: {},
  fetchingHotelCity: false,
  // List Hotel City
  queryData: {},
  hotelDetail: {},
  searchResult: [],
  fetchingHotel: false,

  // select room
  roomHotel: [],
  roomHotelPayload: {},
  fetchingRoomHotel: false,
  // select room

  // Booking Progress
  hotelBook: {},
  fetchBook: false,
  hotelBookPayload: {},

  // Payment Hotel
  paymentHotel: {},
  fetchPayment: false,
  paymentHotelPayload: {},

  // new when redesign hotel
  chooseHotel: {},
  chooseHotelRoom: {},
  chooseHotelBooking: {},

  // Booking Summary
  contactDetail: {},
  guestData: [],

  // payload
  searchPayload: {},
  hotelDetailPayload: {},

  // hotelBeds
  listDestinationBeds: [],
  fetchingDestinationBeds: false,

  // Search Hotel Beds
  searchHotelBeds: {},
  fetchingHotelBeds: false,
  payloadHotelBeds: {},

  // Book Hotel Beds
  bookHotelBeds: {},
  fetchingBookHotelBeds: false,
  payloadBookHotelBeds: {},
};

const typeConst = {
  BOOK_HOTEL_BEDS: 'BOOK_HOTEL_BEDS',
};

export default function HotelReducer(state = INITIAL_STATE, action) {
  const { BOOK_HOTEL_BEDS } = typeConst;
  switch (action.type) {
    // List Hotel City
    case 'LIST_HOTEL_CITY': {
      return {
        ...state,
        fetchingHotelCity: true,
      };
    }
    case 'LIST_HOTEL_CITY_SUCCESS': {
      return {
        ...state,
        fetchingHotelCity: false,
        listHotelCity: action.data,
        payloadHotelCity: action.payload,
      };
    }
    case 'LIST_HOTEL_CITY_FAILED': {
      return {
        ...state,
        fetchingHotelCity: false,
        listHotelCity: [],
        payloadHotelCity: action.payload,
      };
    }
    // List Hotel City

    // List Destination Beds
    case 'DESTINATION_BEDS': {
      return {
        ...state,
        fetchingDestinationBeds: true,
      };
    }
    case 'DESTINATION_BEDS_SUCCESS': {
      return {
        ...state,
        fetchingDestinationBeds: false,
        listDestinationBeds: action.data,
      };
    }
    case 'DESTINATION_BEDS_FAILED': {
      return {
        ...state,
        fetchingDestinationBeds: false,
        listDestinationBeds: [],
      };
    }
    // List Destination Beds

    // Search Hotel Beds
    case 'SEARCH_HOTEL_BEDS': {
      return {
        ...state,
        fetchingHotelBeds: true,
      };
    }
    case 'SEARCH_HOTEL_BEDS_SUCCESS': {
      return {
        ...state,
        fetchingHotelBeds: false,
        searchHotelBeds: action.data,
        payloadHotelBeds: action.payload,
      };
    }
    case 'SEARCH_HOTEL_BEDS_FAILED': {
      return {
        ...state,
        fetchingHotelBeds: false,
        searchHotelBeds: {},
        payloadHotelBeds: action.payload,
      };
    }
    // Search Hotel Beds

    // Book Hotel Beds
    case BOOK_HOTEL_BEDS: {
      return {
        ...state,
        fetchingBookHotelBeds: true,
      };
    }

    case `${BOOK_HOTEL_BEDS}_SUCCESS`: {
      return {
        ...state,
        fetchingBookHotelBeds: false,
        bookHotelBeds: action.data,
        payloadBookHotelBeds: action.payload,
      };
    }

    case `${BOOK_HOTEL_BEDS}_FAILED`: {
      return {
        ...state,
        fetchingBookHotelBeds: false,
        bookHotelBeds: {},
        payloadBookHotelBeds: action.payload,
      };
    }
    // Book Hotel Beds

    // Search Hotel
    case 'SEARCH_HOTEL': {
      return {
        ...state,
        fetchingHotel: true,
      };
    }
    case 'SEARCH_HOTEL_SUCCESS': {
      return {
        ...state,
        fetchingHotel: false,
        searchResult: action.data,
        searchPayload: action.payload,
      };
    }
    case 'SEARCH_HOTEL_FAILED': {
      return {
        ...state,
        fetchingHotel: false,
        searchResult: [],
        searchPayload: action.payload,
      };
    }
    // Search Hotel

    // Hotel Detail
    case 'HOTEL_DETAIL': {
      return {
        ...state,
        fetchingHotel: true,
      };
    }
    case 'HOTEL_DETAIL_SUCCESS': {
      return {
        ...state,
        fetchingHotel: false,
        hotelDetail: action.data,
        hotelDetailPayload: action.payload,
      };
    }
    case 'HOTEL_DETAIL_FAILED': {
      return {
        ...state,
        fetchingHotel: false,
        hotelDetail: [],
        hotelDetailPayload: action.payload,
      };
    }
    // Hotel Detail

    // Payment Hotel
    case 'PAYMENT_HOTEL': {
      return {
        ...state,
        fetchPayment: true,
      };
    }
    case 'PAYMENT_HOTEL_SUCCESS': {
      return {
        ...state,
        fetchPayment: false,
        paymentHotel: action.data,
        paymentHotelPayload: action.payload,
      };
    }
    case 'PAYMENT_HOTEL_FAILED': {
      return {
        ...state,
        fetchPayment: false,
        paymentHotel: {},
        paymentHotelPayload: action.payload,
      };
    }
    // Payment Hotel

    // Hotel Detail
    case 'SELECT_ROOM_HOTEL': {
      return {
        ...state,
        fetchingRoomHotel: true,
      };
    }
    case 'SELECT_ROOM_HOTEL_SUCCESS': {
      return {
        ...state,
        fetchingRoomHotel: false,
        roomHotel: action.data,
        roomHotelPayload: action.payload,
      };
    }
    case 'SELECT_ROOM_HOTEL_FAILED': {
      return {
        ...state,
        fetchingRoomHotel: false,
        roomHotel: [],
        roomHotelPayload: action.payload,
      };
    }
    // Hotel Detail

    // Hotel Book
    case 'HOTEL_BOOK': {
      return {
        ...state,
        fetchBook: true,
      };
    }
    case 'HOTEL_BOOK_SUCCESS': {
      return {
        ...state,
        fetchBook: false,
        hotelBook: action.data,
        hotelBookPayload: action.payload,
      };
    }
    case 'HOTEL_BOOK_FAILED': {
      return {
        ...state,
        fetchBook: false,
        hotelBook: [],
        hotelBookPayload: action.payload,
      };
    }
    // Hotel Book

    // New Redesign when Hotel
    case 'CHOOSE_HOTEL': {
      return {
        ...state,
        chooseHotel: action.data,
      };
    }
    case 'CHOOSE_HOTEL_ROOM': {
      return {
        ...state,
        chooseHotelRoom: action.data,
      };
    }
    case 'CHOOSE_HOTEL_BOOKING': {
      return {
        ...state,
        chooseHotelBooking: action.data,
      };
    }
    // New Redesign when Hotel

    // Booking Summary
    case 'SET_CONTACT_DETAIL': {
      return {
        ...state,
        contactDetail: action.payload,
      };
    }
    case 'SET_GUESTS': {
      return {
        ...state,
        guestData: action.payload,
      };
    }
    // Booking Summary

    default:
      return state;
  }
}
