const INITIAL_STATE = {
  origin: '',
  destination: '',
  departureDate: 'XXX', // YYYY-MM-DD
  returnDate: '', // YYYY-MM-DD
  adults: 1,
  childs: 0,
  infants: 0,
  seatClass: '', // economy, business, firstclass
  flightSelected: {},
  flightReturnSelected: {},
  contactDetail: {},
  passengerData: [],
  searchResult: {},
  listAirport: [],
  searchResultReturn: {},
  flightCalendarList: [],
  singleSearchFetch: false,
  returnSearchFetch: false,
  bookingFetch: false,
  dataBookingFlight: {},
  listAirportPayload: {},
  loadingAirport: false,
};

export default function FlightReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'SEARCH_FLIGHT': {
      return {
        ...state,
        singleSearchFetch: true,
      };
    }
    case 'SEARCH_FLIGHT_SUCCESS': {
      return {
        ...state,
        singleSearchFetch: false,
        searchResult: action.data,
        searchResultReturn: action.returnFlight ? action.returnFlight : [],
        flightCalendarList: action.flightCalendar,
        origin: action.payload.data.departure_code,
        destination: action.payload.data.arrival_code,
        departureDate: action.payload.data.departure_date,
        returnDate: action.payload.data.return_date
          ? action.payload.data.return_date
          : '',
        adults: action.payload.data.adult,
        childs: action.payload.data.child ? action.payload.data.child : 0,
        infants: action.payload.data.infant ? action.payload.data.infant : 0,
        seatClass: action.payload.seatClass ? action.payload.seatClass : '',
      };
    }
    case 'SEARCH_FLIGHT_FAILED': {
      return {
        ...state,
        singleSearchFetch: false,
        flightSelected: {},
        flightReturnSelected: {},
        contactDetail: {},
        passengerData: [],
        searchResult: [],
        searchResultReturn: [],
        flightCalendarList: [],
        adults: 1,
        childs: 0,
        infants: 0,
      };
    }

    case 'SET_FLIGHT_SELECTED': {
      return {
        ...state,
        flightSelected: action.payload,
      };
    }

    case 'SET_FLIGHT_RETURN_SELECTED': {
      return {
        ...state,
        flightReturnSelected: action.payload,
      };
    }

    case 'SET_CONTACT_DETAIL': {
      return {
        ...state,
        contactDetail: action.payload,
      };
    }

    case 'SET_PASSENGER': {
      return {
        ...state,
        passengerData: action.payload,
      };
    }

    case 'BOOKING_FLIGHT': {
      return {
        ...state,
        bookingFetch: true,
      };
    }
    case 'BOOKING_FLIGHT_SUCCESS': {
      return {
        ...state,
        bookingFetch: false,
        dataBookingFlight: action.data,
      };
    }

    case 'BOOKING_FLIGHT_FAILED': {
      return {
        ...state,
        bookingFetch: false,
        dataBookingFlight: action.message,
      };
    }

    case 'LIST_AIRPORT': {
      return {
        ...state,
        loadingAirport: true,
      };
    }
    case 'LIST_AIRPORT_SUCCESS': {
      return {
        ...state,
        loadingAirport: false,
        listAirport: action.data,
        listAirportPayload: action.payload,
      };
    }
    case 'LIST_AIRPORT_FAILED': {
      return {
        ...state,
        loadingAirport: false,
        listAirport: [],
      };
    }

    default:
      return state;
  }
}
