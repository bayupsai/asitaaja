import {
  purchasePulsa,
  pulsaPrice,
  PlnPostpaid,
  bookPlnPostpaid,
  searchPdam,
} from '../../services/API';

const pulsaPP = 'pulsaPrice';
const inqPln = 'inqPln';
const bookPlnPost = 'bookPlnPost';
const searchWater = 'searchWater';

export function actionInquiryPln(payload) {
  return async dispatch => {
    dispatch(requestState(inqPln));
    try {
      const res = await PlnPostpaid(payload);
      if (res.success) {
        return dispatch(successState(inqPln, res.data, payload));
      } else {
        return dispatch(failedState(inqPln, res.message));
      }
    } catch (err) {
      return dispatch(failedState(inqPln, err.message));
    }
  };
}

export function actionBookPlnPostpaid(payload) {
  return dispatch => {
    dispatch(requestState(bookPlnPost));
    return bookPlnPostpaid(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState(bookPlnPost, res.data, payload));
        } else {
          return dispatch(failedState(bookPlnPost, res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState(bookPlnPost, err.message));
      });
  };
}

export function actionPulsaPrice(payload) {
  return dispatch => {
    dispatch(requestState(pulsaPP));
    return pulsaPrice(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState(pulsaPP, res.data, payload));
        } else {
          return dispatch(failedState(pulsaPP, res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState(pulsaPP, err.essage));
      });
  };
}

export function actionPpob(payload, stateData) {
  return dispatch => {
    dispatch(requestState('pulsa'));
    return purchasePulsa(payload)
      .then(res => {
        if (res.status) {
          const response = res.data ? res.data : [];
          return dispatch(successState('pulsa', response, payload, stateData));
        } else {
          return dispatch(failedState('pulsa', res.error_msg));
        }
      })
      .catch(err => {
        return dispatch(failedState('pulsa', err.message));
      });
  };
}

export function actionSearchPDAM(payload) {
  return dispatch => {
    dispatch(requestState(searchWater));
    return searchPdam(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState(searchWater, res.data, payload));
        } else {
          return dispatch(failedState(searchWater, res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState(searchWater, err.message));
      });
  };
}

//action state
export function requestState(type) {
  if (type === 'pulsa') {
    return {
      type: 'PULSA',
    };
  } else if (type === pulsaPP) {
    return {
      type: 'PULSA_PRICE',
    };
  } else if (type === inqPln) {
    return {
      type: 'PLN_POSTPAID',
    };
  } else if (type === bookPlnPost) {
    return {
      type: 'BOOK_PLN_POST',
    };
  } else if (type === searchWater) {
    return {
      type: 'SEARCH_PDAM',
    };
  }
}

export function successState(type, data, payload, stateData) {
  if (type === 'pulsa') {
    return {
      type: 'PULSA_SUCCESS',
      data: data,
      payload: payload,
      stateData: stateData,
    };
  } else if (type === pulsaPP) {
    return {
      type: 'PULSA_PRICE_SUCCESS',
      data,
      payload,
    };
  } else if (type === inqPln) {
    return {
      type: 'PLN_POSTPAID_SUCCESS',
      data,
      payload,
    };
  } else if (type === bookPlnPost) {
    return {
      type: 'BOOK_PLN_POST_SUCCESS',
      data,
      payload,
    };
  } else if (type === searchWater) {
    return {
      type: 'SEARCH_PDAM_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, message) {
  if (type === 'pulsa') {
    return {
      type: 'PULSA_FAILED',
      message: message,
    };
  } else if (type === pulsaPP) {
    return {
      type: 'PULSA_PRICE_FAILED',
      message,
    };
  } else if (type === inqPln) {
    return {
      type: 'PLN_POSTPAID_FAILED',
      message,
    };
  } else if (type === bookPlnPost) {
    return {
      type: 'BOOK_PLN_POST_FAILED',
      message,
    };
  } else if (type === searchWater) {
    return {
      type: 'SEARCH_PDAM_FAILED',
      message,
    };
  }
}
