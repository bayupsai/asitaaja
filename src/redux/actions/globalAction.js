import { checkPaymentStatus } from '../../services/API';

export function getMessages(message) {
  return {
    type: 'GET_MESSAGE',
    message: message,
  };
}

export function setCurrentGeolocation(region, lat, lang) {
  return {
    type: 'SET_LOCATION',
    region: region,
    lat: lat,
    lang: lang,
  };
}

export function paymentStatus(invoice, type) {
  return dispatch => {
    dispatch(requestState());
    return checkPaymentStatus(invoice, type)
      .then(res => {
        return dispatch(successState(res, type));
      })
      .catch(err => {
        return dispatch(failedState(err.message, type));
      });
  };
}

export function requestState(type) {
  return {
    type: 'CHECK_STATUS_PAYMENT',
  };
}

export function successState(data, typePage) {
  return {
    type: 'CHECK_STATUS_PAYMENT_SUCCESS',
    data,
    typePage,
  };
}

export function failedState(message, typePage) {
  return {
    type: 'SEARCH_FLIGHT_SUCCESS',
    message,
    typePage,
  };
}
