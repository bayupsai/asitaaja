import { PlnPostpaid } from '../../services/API';

const plnPostpaid = 'plnPostpaid';
const plnPostpaidPay = 'plnPostpaidPay';
const pulsaFin = 'pulsaFin';

const requestState = type => {
  if (type === plnPostpaid) {
    return {
      type: 'PLNPOST',
    };
  } else if (type === plnPostpaidPay) {
    return {
      type: 'PLNPOSTPAY',
    };
  } else if (type === pulsaFin) {
    return {
      type: 'PULSAFIN',
    };
  }
};

const successState = (type, data, payload) => {
  if (type === plnPostpaid) {
    return {
      type: 'PLNPOST_SUCCESS',
      data,
      payload,
    };
  } else if (type === plnPostpaidPay) {
    return {
      type: 'PLNPOSTPAY_SUCCESS',
      data,
      payload,
    };
  } else if (type === pulsaFin) {
    return {
      type: 'PULSAFIN_SUCCESS',
      data,
      payload,
    };
  }
};

const failedState = (type, message, payload) => {
  if (type === plnPostpaid) {
    return {
      type: 'PLNPOST_FAILED',
      message,
      payload,
    };
  } else if (type === plnPostpaidPay) {
    return {
      type: 'PLNPOSTPAY_FAILED',
      message,
      payload,
    };
  } else if (type === pulsaFin) {
    return {
      type: 'PULSAFIN_FAILED',
      message,
      payload,
    };
  }
};

export const actionPlnPost = payload => {
  return dispatch => {
    dispatch(requestState(plnPostpaid));
    return PlnPostpaid()
      .then(res => {
        if (res.error.code === 0) {
          return dispatch(successState(plnPostpaid, res.data, payload));
        } else {
          return dispatch(failedState(plnPostpaid, res.error.message, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState(plnPostpaid, err.message, payload));
      });
  };
};

export const actionPlnPostPay = payload => {
  return dispatch => {
    dispatch(requestState(plnPostpaidPay));
    return PlnPostpaid()
      .then(res => {
        if (res.error.code === 0) {
          return dispatch(successState(plnPostpaidPay, res.data, payload));
        } else {
          return dispatch(
            failedState(plnPostpaidPay, res.error.message, payload)
          );
        }
      })
      .catch(err => {
        return dispatch(failedState(plnPostpaidPay, err.message, payload));
      });
  };
};

export const actionPulsaFin = payload => {
  return dispatch => {
    dispatch(requestState(pulsaFin));
    return PlnPostpaid()
      .then(res => {
        if (res.error.code === 0) {
          return dispatch(successState(pulsaFin, res.data, payload));
        } else {
          return dispatch(failedState(pulsaFin, res.error.message, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState(pulsaFin, err.message, payload));
      });
  };
};
