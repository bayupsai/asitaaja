import {
  forgotPassword,
  verifyForgotPassword,
  changeForgotPassword,
} from '../../services/API';

export function forgot(payload) {
  return dispatch => {
    dispatch(requestState('forgotPassword'));
    return forgotPassword(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState('forgotPassword', res.data, payload));
        } else {
          return dispatch(failedState('forgotPassword', res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState('forgotPassword', err.message, payload));
      });
  };
}

export function verifyForgot(payload) {
  return dispatch => {
    dispatch(requestState('verifyForgot'));
    return verifyForgotPassword(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState('verifyForgot', res.data, payload));
        } else {
          return dispatch(failedState('verifyForgot', res.message, payload));
        }
      })
      .catch(err => {
        return dispatch(
          failedState('verifyForgot', 'Verify Forgot Password Error')
        );
      });
  };
}

export function changeForgot(payload) {
  return dispatch => {
    dispatch(requestState('changeForgot'));
    return changeForgotPassword(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState('changeForgot', res.data, payload));
        } else {
          return dispatch(failedState('changeForgot', res.message, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('changeForgot', err.message, payload));
      });
  };
}

export function requestState(type) {
  if (type == 'forgotPassword') {
    return {
      type: 'FORGOT_PASSWORD',
    };
  } else if (type == 'verifyForgot') {
    return {
      type: 'VERIFY_FORGOT',
    };
  } else if (type == 'changeForgot') {
    return {
      type: 'CHANGE_FORGOT',
    };
  }
}

export function successState(type, data, payload) {
  if (type == 'forgotPassword') {
    return {
      type: 'FORGOT_PASSWORD_SUCCESS',
      data,
      payload,
    };
  } else if (type == 'verifyForgot') {
    return {
      type: 'VERIFY_FORGOT_SUCCESS',
      data,
      payload,
    };
  } else if (type == 'changeForgot') {
    return {
      type: 'CHANGE_FORGOT_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, message, payload) {
  if (type == 'forgotPassword') {
    return {
      type: 'FORGOT_PASSWORD_FAILED',
      message,
      payload,
    };
  } else if (type == 'verifyForgot') {
    return {
      type: 'VERIFY_FORGOT_FAILED',
      message,
      payload,
    };
  } else if (type == 'changeForgot') {
    return {
      type: 'CHANGE_FORGOT_FAILED',
      message,
      payload,
    };
  }
}
