import {
  getProfile,
  updateProfile,
  refreshProfile,
  changePasswordLogin,
} from '../../services/API';

export function actionGetProfile(token) {
  return dispatch => {
    dispatch(requestState('getProfile'));
    return getProfile(token)
      .then(res => {
        if (res.success === true) {
          return dispatch(successState('getProfile', res.data, token));
        } else {
          return dispatch(failedState('getProfile', res.message, token));
        }
      })
      .catch(err => {
        return dispatch(failedState('getProfile', err, token));
      });
  };
}

export function actionUpdateProfile(token, payload) {
  return dispatch => {
    dispatch(requestState('updateProfile'));
    return updateProfile(token, payload).then(res => {
      if (res.success) {
        return dispatch(successState('updateProfile', res, token, payload));
      } else {
        return dispatch(
          failedState('updateProfile', res.message, token, payload)
        );
      }
    });
  };
}

export function actionRefreshProfile(token, payload) {
  return dispatch => {
    dispatch(requestState('refreshProfile'));
    return refreshProfile(token, payload).then(res => {
      if (res.success) {
        return dispatch(successState('refreshProfile', res, token, payload));
      } else {
        return dispatch(
          failedState('refreshProfile', res.message, token, payload)
        );
      }
    });
  };
}

export function actionChangePasswordLogin(token, payload) {
  return dispatch => {
    dispatch(requestState('changePasswordLogin'));
    return changePasswordLogin(token, payload)
      .then(res => {
        if (res.success) {
          return dispatch(
            successState('changePasswordLogin', res, token, payload)
          );
        } else {
          return dispatch(
            failedState('changePasswordLogin', res.message, token, payload)
          );
        }
      })
      .catch(err => {
        return dispatch(
          failedState('changePasswordLogin', err.message, token, payload)
        );
      });
  };
}

export function actionDestroyProfile(token) {
  return dispatch => {
    const response = token;
    return dispatch(successState('destroyProfile', response, token));
  };
}

//stating
export function requestState(type) {
  if (type == 'getProfile') {
    return {
      type: 'GET_PROFILE',
    };
  } else if (type == 'updateProfile') {
    return {
      type: 'UPDATE_PROFILE',
    };
  } else if (type == 'refreshProfile') {
    return {
      type: 'REFRESH_PROFILE',
    };
  } else if (type == 'changePasswordLogin') {
    return {
      type: 'CHANGE_PASSWORD_LOGIN',
    };
  }
}
export function successState(type, data, token, payload) {
  if (type == 'getProfile') {
    return {
      type: 'GET_PROFILE_SUCCESS',
      data,
      token,
    };
  } else if (type == 'updateProfile') {
    return {
      type: 'UPDATE_PROFILE_SUCCESS',
      data,
      token,
      payload,
    };
  } else if (type == 'refreshProfile') {
    return {
      type: 'REFRESH_PROFILE_SUCCESS',
      data,
      token,
      payload,
    };
  } else if (type == 'destroyProfile') {
    return {
      type: 'DESTROY_PROFILE',
      data,
    };
  } else if (type == 'changePasswordLogin') {
    return {
      type: 'CHANGE_PASSWORD_LOGIN_SUCCESS',
      data,
      token,
      payload,
    };
  }
}
export function failedState(type, data, token, payload) {
  if (type == 'getProfile') {
    return {
      type: 'GET_PROFILE_FAIELD',
      data,
      token,
      payload,
    };
  } else if (type == 'updateProfile') {
    return {
      type: 'UPDATE_PROFILE_FAILED',
      data,
      token,
      payload,
    };
  } else if (type == 'refreshProfile') {
    return {
      type: 'REFRESH_PROFILE_SUCCSESS',
      data,
      token,
      payload,
    };
  } else if (type == 'changePasswordLogin') {
    return {
      type: 'CHANGE_PASSWORD_LOGIN_FAILED',
      data,
      token,
      payload,
    };
  }
}
