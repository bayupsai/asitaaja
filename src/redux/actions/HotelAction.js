import {
  searchHotel,
  hotelDetail,
  hotelBooking,
  listHotelCity,
  selectRoomHotel,
  paymentHotel,
  listDestinationBeds,
  searchHotelBeds,
  bookHotelBeds,
} from '../../services/API';

const typeConst = {
  bookHotelBedsConst: 'bookHotelBeds',
};

export function actionListHotelCity(country, query, token) {
  return dispatch => {
    dispatch(requestState('listHotelCity'));
    const payload = {
      country_code: country,
      city_name: query,
    };
    return listHotelCity(country, query, token)
      .then(res => {
        if (res.status === true) {
          return dispatch(successState('listHotelCity', res.data, payload));
        }
        return dispatch(failedState('listHotelCity', res, payload));
      })
      .catch(err => {
        return dispatch(failedState('listHotelCity', err.message, payload));
      });
  };
}

export function actionSearchHotel(payload, token) {
  return dispatch => {
    dispatch(requestState('searchHotel'));
    return searchHotel(payload, token)
      .then(res => {
        if (res.status === true) {
          return dispatch(successState('searchHotel', res.data, payload));
        }
        return dispatch(failedState('searchHotel', res, payload));
      })
      .catch(err => {
        return dispatch(failedState('searchHotel', err.message, payload));
      });
  };
}

export function actionHotelDetail(payload, token) {
  return dispatch => {
    dispatch(requestState('hotelDetail'));
    return hotelDetail(payload, token)
      .then(res => {
        if (res.status === true) {
          return dispatch(successState('hotelDetail', res.data, payload));
        }
        return dispatch(failedState('hotelDetail', res, payload));
      })
      .catch(err => {
        return dispatch(failedState('hotelDetail', err.message, payload));
      });
  };
}

export function actionSelectRoomHotel(payload, token, property) {
  return dispatch => {
    dispatch(requestState('selectRoomHotel'));
    return selectRoomHotel(payload, token, property)
      .then(res => {
        if (res.status === true) {
          return dispatch(successState('selectRoomHotel', res.data, payload));
        }
        return dispatch(failedState('selectRoomHotel', res, payload));
      })
      .catch(err => {
        return dispatch(failedState('selectRoomHotel', err.message, payload));
      });
  };
}

export function actionHotelBook(payload, hotelId, roomId, token) {
  return dispatch => {
    dispatch(requestState('hotelBook'));
    return hotelBooking(payload, hotelId, roomId, token)
      .then(res => {
        if (res.status === true) {
          return dispatch(successState('hotelBook', res.data, payload));
        }
        return dispatch(failedState('hotelBook', res, payload));
      })
      .catch(err => {
        return dispatch(failedState('hotelBook', err.message, payload));
      });
  };
}

export function actionPaymentHotel(payload, token) {
  return dispatch => {
    dispatch(requestState('paymentHotel'));
    return paymentHotel(payload, token)
      .then(res => {
        if (res.status_desc === 'Success') {
          return dispatch(successState('paymentHotel', res, payload));
        }
        return dispatch(failedState('paymentHotel', res, payload));
      })
      .catch(err => {
        return dispatch(failedState('paymentHotel', err.message, payload));
      });
  };
}

// Beds
export function actionDestinationBeds(query) {
  return dispatch => {
    dispatch(requestState('destinationBeds'));
    return listDestinationBeds(query)
      .then(res => {
        return dispatch(
          res.status === true
            ? successState('destinationBeds', res, query)
            : failedState('destinationBeds', res, `${query} dalam status`)
        );
      })
      .catch(err => {
        return dispatch(
          failedState(
            'destinationBeds',
            err,
            `${JSON.stringify(query)} luar setatus`
          )
        );
      });
  };
}

export function actionSearchHotelBeds(query) {
  return dispatch => {
    dispatch(requestState('searchHotelBeds'));
    return searchHotelBeds(query)
      .then(res => {
        return dispatch(
          res.status === true
            ? successState('searchHotelBeds', res, query)
            : failedState('searchHotelBeds', res, `${query} dalam status`)
        );
      })
      .catch(err => {
        return dispatch(failedState('searchHotelBeds', err.message, query));
      });
  };
}

export function actionBookHotelBeds(query) {
  const { bookHotelBedsConst } = typeConst;
  return dispatch => {
    dispatch(requestState(bookHotelBedsConst));
    return bookHotelBeds(query)
      .then(res => {
        return dispatch(
          res.status === true
            ? successState(bookHotelBedsConst, res, query)
            : failedState(bookHotelBedsConst, res.message, query)
        );
      })
      .catch(err => {
        return dispatch(failedState(bookHotelBedsConst, err.message, query));
      });
  };
}
// Beds

export function chooseHotel(data) {
  return {
    type: 'CHOOSE_HOTEL',
    data,
  };
}

export function chooseHotelRoom(data) {
  return {
    type: 'CHOOSE_HOTEL_ROOM',
    data,
  };
}

export function chooseHotelBooking(data) {
  return {
    type: 'CHOOSE_HOTEL_BOOKING',
    data,
  };
}

export function actionContactDetail(payload) {
  return {
    type: 'SET_CONTACT_DETAIL',
    payload,
  };
}

export function actionSetGuest(payload) {
  return {
    type: 'SET_GUESTS',
    payload,
  };
}

export function requestState(type) {
  const { bookHotelBedsConst } = typeConst;
  if (type === 'listHotelCity') {
    return {
      type: 'LIST_HOTEL_CITY',
    };
  } else if (type === 'searchHotel') {
    return {
      type: 'SEARCH_HOTEL',
    };
  } else if (type === 'hotelDetail') {
    return {
      type: 'HOTEL_DETAIL',
    };
  } else if (type === 'selectRoomHotel') {
    return {
      type: 'SELECT_ROOM_HOTEL',
    };
  } else if (type === 'hotelBook') {
    return {
      type: 'HOTEL_BOOK',
    };
  } else if (type === 'paymentHotel') {
    return {
      type: 'PAYMENT_HOTEL',
    };
  } else if (type === 'destinationBeds') {
    return {
      type: 'DESTINATION_BEDS',
    };
  } else if (type === 'searchHotelBeds') {
    return {
      type: 'SEARCH_HOTEL_BEDS',
    };
  } else if (type === bookHotelBedsConst) {
    return {
      type: 'BOOK_HOTEL_BEDS',
    };
  }
}

export function successState(type, data, payload) {
  const { bookHotelBedsConst } = typeConst;
  if (type === 'listHotelCity') {
    return {
      type: 'LIST_HOTEL_CITY_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'searchHotel') {
    return {
      type: 'SEARCH_HOTEL_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'hotelDetail') {
    return {
      type: 'HOTEL_DETAIL_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'selectRoomHotel') {
    return {
      type: 'SELECT_ROOM_HOTEL_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'hotelBook') {
    return {
      type: 'HOTEL_BOOK_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'paymentHotel') {
    return {
      type: 'PAYMENT_HOTEL_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'destinationBeds') {
    return {
      type: 'DESTINATION_BEDS_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'searchHotelBeds') {
    return {
      type: 'SEARCH_HOTEL_BEDS_SUCCESS',
      data,
      payload,
    };
  } else if (type === bookHotelBedsConst) {
    return {
      type: 'BOOK_HOTEL_BEDS_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, message, payload) {
  const { bookHotelBedsConst } = typeConst;
  if (type === 'listHotelCity') {
    return {
      type: 'LIST_HOTEL_CITY_FAILED',
      message,
      payload,
    };
  } else if (type === 'searchHotel') {
    return {
      type: 'SEARCH_HOTEL_FAILED',
      message,
      payload,
    };
  } else if (type === 'hotelDetail') {
    return {
      type: 'HOTEL_DETAIL_FAILED',
      message,
      payload,
    };
  } else if (type === 'selectRoomHotel') {
    return {
      type: 'SELECT_ROOM_HOTEL_FAILED',
      message,
      payload,
    };
  } else if (type === 'hotelBook') {
    return {
      type: 'HOTEL_BOOK_FAILED',
      message,
      payload,
    };
  } else if (type === 'paymentHotel') {
    return {
      type: 'PAYMENT_HOTEL_FAILED',
      message,
      payload,
    };
  } else if (type === 'destinationBeds') {
    return {
      type: 'DESTINATION_BEDS_FAILED',
      message,
      payload,
    };
  } else if (type === 'searchHotelBeds') {
    return {
      type: 'SEARCH_HOTEL_BEDS_FAILED',
      message,
      payload,
    };
  } else if (type === bookHotelBedsConst) {
    return {
      type: 'BOOK_HOTEL_BEDS_FAILED',
      message,
      payload,
    };
  }
}
