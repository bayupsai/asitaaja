import {
  stationSearch,
  searchTrain,
  seatMap,
  bookTrain,
} from '../../services/API';

export function actionStationSearch() {
  return async dispatch => {
    dispatch(requestState('stationSearch'));
    try {
      const res = await stationSearch();
      return dispatch(successState('stationSearch', res.result));
    } catch (err) {
      return dispatch(failedState('stationSearch', err.message));
    }
  };
}

export function actionSearchTrain(payload) {
  return async dispatch => {
    dispatch(requestState('searchTrain'));
    try {
      const res = await searchTrain(payload);
      return dispatch(successState('searchTrain', res.result, payload));
    } catch (err) {
      return dispatch(failedState('searchTrain', err.message));
    }
  };
}

export function newSearchTrain(payload) {
  return async dispatch => {
    dispatch(requestState('newTrain'));
    try {
      const res = await searchTrain(payload);
      return dispatch(successState('newTrain', res.result, payload));
    } catch (err) {
      return dispatch(failedState('newTrain', err.message));
    }
  };
}

export function actionSeatMap(payload) {
  return async dispatch => {
    dispatch(requestState('seatMap'));
    try {
      const res = await seatMap(payload);
      const response = res.data ? res.data : [];
      return dispatch(successState('seatMap', response, payload));
    } catch (err) {
      return dispatch(failedState());
    }
  };
}

export function actionBookTrain(payload) {
  return async dispatch => {
    dispatch(requestState('bookTrain'));
    try {
      const res = await bookTrain(payload);
      if (res.err_str === 'OK') {
        return dispatch(successState('bookTrain', res, payload));
      } else {
        return dispatch(failedState('bookTrain', res, payload));
      }
    } catch (err) {
      return dispatch(
        failedState('bookTrain', 'Failed to booking, please try again')
      );
    }
  };
}

export function actionContactDetail(payload) {
  return {
    type: 'SET_CONTACT_DETAIL',
    payload: payload,
  };
}

export function actionSetPassenger(payload) {
  return {
    type: 'SET_PASSENGER',
    payload: payload,
  };
}

export function actionTrainSelected(payload, pageType = null) {
  if (pageType) {
    return {
      type: 'SET_FLIGHT_RETURN_SELECTED',
      payload: payload,
    };
  } else {
    return {
      type: 'SET_FLIGHT_SELECTED',
      payload: payload,
    };
  }
}

export function requestState(type) {
  if (type == 'stationSearch') {
    return {
      type: 'STATION_SEARCH',
    };
  } else if (type == 'searchTrain') {
    return {
      type: 'SEARCH_TRAIN',
    };
  } else if (type == 'seatMap') {
    return {
      type: 'SEAT_MAP',
    };
  } else if (type == 'bookTrain') {
    return {
      type: 'BOOK_TRAIN',
    };
  } else if (type === 'newTrain') {
    return {
      type: 'NEW_TRAIN',
    };
  }
}

export function successState(type, data, payload) {
  if (type == 'stationSearch') {
    return {
      type: 'STATION_SEARCH_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type == 'searchTrain') {
    return {
      type: 'SEARCH_TRAIN_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type == 'seatMap') {
    return {
      type: 'SEAT_MAP_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type == 'bookTrain') {
    return {
      type: 'BOOK_TRAIN_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type === 'newTrain') {
    return {
      type: 'NEW_TRAIN_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, errMsg) {
  if (type == 'stationSearch') {
    return {
      type: 'STATION_SEARCH_FAILED',
    };
  } else if (type == 'searchTrain') {
    return {
      type: 'SEARCH_TRAIN_FAILED',
      errMsg,
    };
  } else if (type == 'seatMap') {
    return {
      type: 'SEAT_MAP_FAILED',
    };
  } else if (type == 'bookTrain') {
    return {
      type: 'BOOK_TRAIN_FAILED',
      errorMessage: errMsg,
    };
  } else if (type === 'newTrain') {
    return {
      type: 'NEW_TRAIN_FAILED',
      errMsg,
    };
  }
}
