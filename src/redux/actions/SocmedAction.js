import { loginSocmed } from '../../services/API';

export function actionLoginSocmed(type, payload) {
  if (type === 'facebook') {
    return dispatch => {
      dispatch(requestState('loginFacebook'));
      return loginSocmed(payload)
        .then(res => {
          if (res.success) {
            return dispatch(successState('loginFacebook', res.data, payload));
          } else {
            return dispatch(failedState('loginFacebook', res));
          }
        })
        .catch(err => {
          return dispatch(failedState('loginFacebook', err.message));
        });
    };
  } else if (type === 'google') {
    return dispatch => {
      dispatch(requestState('loginGoogle'));
      return loginSocmed(payload)
        .then(res => {
          if (res.success) {
            return dispatch(successState('loginGoogle', res.data, payload));
          } else {
            return dispatch(failedState('loginGoogle', res));
          }
        })
        .catch(err => {
          return dispatch(failedState('loginGoogle', err.message));
        });
    };
  }
}

export function requestState(type) {
  if (type === 'loginFacebook') {
    return { type: 'LOGIN_FACEBOOK' };
  } else if (type === 'loginGoogle') {
    return { type: 'LOGIN_GOOGLE' };
  }
}

export function successState(type, data, payload) {
  if (type === 'loginFacebook') {
    return {
      type: 'LOGIN_FACEBOOK_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'loginGoogle') {
    return {
      type: 'LOGIN_GOOGLE_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, message) {
  if (type === 'loginFacebook') {
    return {
      type: 'LOGIN_FACEBOOK_FAILED',
      message,
    };
  } else if (type === 'loginGoogle') {
    return {
      type: 'LOGIN_GOOGLE_FAILED',
      message,
    };
  }
}

export function actionGetToken(type, token) {
  if (type === 'facebook') {
    return {
      type: 'TOKEN_FACEBOOK',
      token,
    };
  } else if (type === 'google') {
    return {
      type: 'TOKEN_GOOGLE',
      token,
    };
  }
}

export function actionLogOutSocmed(type) {
  if (type === 'facebook') {
    return {
      type: 'LOGOUT_FACEBOOK',
    };
  } else if (type === 'google') {
    return {
      type: 'LOGOUT_GOOGLE',
    };
  }
}
