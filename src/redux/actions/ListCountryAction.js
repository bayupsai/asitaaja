import { listCountry } from '../../services/API';

export function actionListCountry(limit) {
  return dispatch => {
    dispatch(requestState('listCountry'));
    return listCountry(limit)
      .then(res => {
        return res.status !== true
          ? dispatch(successState('listCountry', res.data, limit))
          : dispatch(failedState('listCountry', res.message, limit));
      })
      .catch(err => {
        return dispatch(failedState('listCountry', err, limit));
        // return dispatch(failedState('listCountry', err, limit))
      });
  };
}

export function requestState(type) {
  if (type == 'listCountry') {
    return {
      type: 'LIST_COUNTRY',
    };
  }
}
export function successState(type, data, limit) {
  if (type == 'listCountry') {
    return { type: 'LIST_COUNTRY_SUCCESS', data, limit };
  }
}
export function failedState(type, message, limit) {
  if (type == 'listCountry') {
    return { type: 'LIST_COUNTRY_FAILED', message, limit };
  }
}
