import { loginMail } from '../../services/API';

export function actionLoginMail(payload) {
  return dispatch => {
    dispatch(requestState('loginMail'));
    return loginMail(payload)
      .then(res => {
        if (res.success) {
          const response = res.data ? res.data : [];
          return dispatch(successState('loginMail', response, payload));
        } else {
          return dispatch(failedState('loginMail', res.message, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('loginMail', err.message, payload));
      });
  };
}

export function requestState(type) {
  if (type == 'loginMail') {
    return {
      type: 'LOGIN_MAIL',
    };
  }
}

export function successState(type, data, payload) {
  if (type == 'loginMail') {
    return {
      type: 'LOGIN_MAIL_SUCCESS',
      data: data,
      payload: payload,
    };
  }
}

export function failedState(type, message, payload) {
  if (type == 'loginMail') {
    return {
      type: 'LOGIN_MAIL_FAILED',
      message,
      payload,
    };
  }
}
