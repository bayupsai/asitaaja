import {
  batikAll,
  batikDetail,
  batikBook,
  payFlight,
} from '../../services/API';

export function actionBatikAll() {
  return dispatch => {
    dispatch(requestState('batikAll'));
    return batikAll()
      .then(res => {
        return res.status
          ? dispatch(successState('batikAll', res.data))
          : dispatch(failedState('batikAll', res));
      })
      .catch(err => {
        return dispatch(failedState('batikAll', err.message));
      });
  };
}

export function actionBatikDetail(payload) {
  return dispatch => {
    dispatch(requestState('batikDetail'));
    return batikDetail(payload)
      .then(res => {
        return res.status
          ? dispatch(successState('batikDetail', res.data))
          : dispatch(failedState('batikDetail', res));
      })
      .catch(err => {
        return dispatch(failedState('batikDetail', err.message, payload));
      });
  };
}

export function actionBatikBook(payload) {
  return dispatch => {
    dispatch(requestState('batikBook'));
    return batikBook(payload)
      .then(res => {
        return res.status
          ? dispatch(successState('batikBook', res.data))
          : dispatch(failedState('batikBook', res));
      })
      .catch(err => {
        return dispatch(failedState('batikBook', err.message, payload));
      });
  };
}

export function actionBatikPay(payload) {
  return dispatch => {
    dispatch(requestState('batikPay'));
    return payFlight(payload)
      .then(res => {
        return res.status_desc
          ? dispatch(successState('batikPay', res))
          : dispatch(failedState('batikPay', res));
      })
      .catch(err => {
        return dispatch(failedState('batikPay', err.message, payload));
      });
  };
}

export function requestState(type) {
  if (type === 'batikAll') {
    return {
      type: 'BATIK_ALL',
    };
  } else if (type === 'batikDetail') {
    return {
      type: 'BATIK_DETAIL',
    };
  } else if (type === 'batikBook') {
    return {
      type: 'BATIK_BOOK',
    };
  } else if (type === 'batikPay') {
    return {
      type: 'BATIK_PAY',
    };
  }
}

export function successState(type, data, payload) {
  if (type === 'batikAll') {
    return {
      type: 'BATIK_ALL_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'batikDetail') {
    return {
      type: 'BATIK_DETAIL_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'batikBook') {
    return {
      type: 'BATIK_BOOK_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'batikPay') {
    return {
      type: 'BATIK_PAY_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, message, payload) {
  if (type === 'batikAll') {
    return {
      type: 'BATIK_ALL_FAILED',
      message,
      payload,
    };
  } else if (type === 'batikDetail') {
    return {
      type: 'BATIK_DETAIL_FAILED',
      message,
      payload,
    };
  } else if (type === 'batikBook') {
    return {
      type: 'BATIK_BOOK_FAILED',
      message,
      payload,
    };
  } else if (type === 'batikPay') {
    return {
      type: 'BATIK_PAY_FAILED',
      message,
      payload,
    };
  }
}
