import {
  sendCargo,
  getListOfDropbox,
  getPacisPrice,
  submitPacisBooking,
  submitPacisPayment,
  getPacisPaymentStatus,
} from '../../services/API';

export function setSenderCargo(payload) {
  return {
    type: 'SET_SENDER_CARGO',
    payload: payload,
  };
}

export function setReceiverCargo(payload) {
  return {
    type: 'SET_RECEIVER_CARGO',
    payload: payload,
  };
}

export function setPackageSize(payload) {
  return {
    type: 'SET_PACKAGE_SIZE',
    payload: payload,
  };
}

export function setDocument(payload) {
  return {
    type: 'SET_DOCUMENTS',
    payload: payload,
  };
}

export function getBoxlist(string) {
  return dispatch => {
    dispatch(requestState('listbox'));
    return getListOfDropbox(string)
      .then(res => {
        if (res.success) {
          return dispatch(successState('listbox', res.data));
        } else {
          return dispatch(failedState('listbox', res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState('listbox', res.message));
      });
  };
}

export function getPacisPricing(payload) {
  return dispatch => {
    dispatch(requestState('getPrice'));
    return getPacisPrice(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState('getPrice', res.data));
        } else {
          return dispatch(failedState('getPrice', res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState('getPrice', err.message));
      });
  };
}

export function submitPacisBook(payload) {
  return dispatch => {
    dispatch(requestState('pacisBook'));
    return submitPacisBooking(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState('pacisBook', res.data));
        } else {
          return dispatch(failedState('pacisBook', res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState('pacisBook', err.message));
      });
  };
}

export function getPacisPayment(bookingCode) {
  return dispatch => {
    dispatch(requestState('pacis_status'));
    return getPacisPaymentStatus(bookingCode)
      .then(res => {
        if (res.success) {
          return dispatch(successState('pacis_status', res.data));
        } else {
          return dispatch(failedState('pacis_status', res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState('pacis_status', err.message));
      });
  };
}

export function submitPacisPay(payload) {
  return dispatch => {
    dispatch(requestState('pacisPay'));
    return submitPacisPayment(payload)
      .then(res => {
        if (res.success) {
          return dispatch(successState('pacisPay', res.data));
        } else {
          return dispatch(failedState('pacisPay', res.message));
        }
      })
      .catch(err => {
        return dispatch(failedState('pacisPay', err.message));
      });
  };
}

export function requestState(type) {
  if (type == 'listbox') {
    return {
      type: 'FETCH_LISTBOX',
    };
  } else if (type == 'getPrice') {
    return {
      type: 'FETCH_PRICE',
    };
  } else if (type == 'pacisBook') {
    return {
      type: 'FETCH_BOOK',
    };
  } else if (type == 'pacisPay') {
    return {
      type: 'FETCH_PAY',
    };
  } else if (type == 'pacis_status') {
    return {
      type: 'PACIS_STATUS',
    };
  }
}

export function successState(type, data, payload) {
  if (type == 'listbox') {
    return {
      type: 'FETCH_LISTBOX_SUCCESS',
      data: data,
    };
  } else if (type == 'getPrice') {
    return {
      type: 'FETCH_PRICE_SUCCESS',
      data: data,
    };
  } else if (type == 'pacisBook') {
    return {
      type: 'PACIS_BOOK_SUCCESS',
      data: data,
    };
  } else if (type == 'pacisPay') {
    return {
      type: 'PACIS_PAY_SUCCESS',
      data: data,
    };
  } else if (type == 'pacis_status') {
    return {
      type: 'PACIS_STATUS_SUCCESS',
      data: data,
    };
  }
}

export function failedState(type) {
  if (type == 'sendCargo') {
    return {
      type: 'SEND_CARGO_FAILED',
    };
  } else if (type == 'orderCargo') {
    return {
      type: 'ORDER_CARGO_FAILED',
    };
  } else if (type == 'pacisBook') {
    return {
      type: 'PACIS_BOOK_FAILED',
    };
  } else if (type == 'pacisPay') {
    return {
      type: 'PACIS_PAY_FAILED',
    };
  }
}
