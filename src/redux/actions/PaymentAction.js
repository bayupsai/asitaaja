import { paymentMidtrans, checkPaymentMidtrans } from '../../services/API';

const payMT = 'paymentMidtrans';
const checkMT = 'checkPaymentMidtrans';

export function requestState(type) {
  if (type === payMT) {
    return {
      type: 'PAYMENT_MIDTRANS',
    };
  } else if (type === checkMT) {
    return {
      type: 'CHECK_MIDTRANS',
    };
  }
}

export function successState(type, data, payload) {
  if (type === payMT) {
    return {
      type: 'PAYMENT_MIDTRANS_SUCCESS',
      data,
      payload,
    };
  } else if (type === checkMT) {
    return {
      type: 'CHECK_MIDTRANS_SUCCESS',
      data,
    };
  }
}

export function failedState(type, message, payload) {
  if (type === payMT) {
    return {
      type: 'PAYMENT_MIDTRANS_FAILED',
      message,
      payload,
    };
  } else if (type === checkMT) {
    return {
      type: 'CHECK_MIDTRANS_FAILED',
      message,
      payload,
    };
  }
}

export function actionPayMidtrans(payload) {
  return async dispatch => {
    dispatch(requestState(payMT));
    try {
      const res = await paymentMidtrans(payload);
      if (res.success === true) {
        return dispatch(successState(payMT, res.data, payload));
      }
      return dispatch(failedState(payMT, res, payload));
    } catch (err) {
      return dispatch(failedState(payMT, err.message, payload));
    }
  };
}

export function actionCheckPayMidtrans() {
  return async dispatch => {
    dispatch(requestState(checkMT));
    try {
      const res = await checkPaymentMidtrans();
      if (res.status === true) {
        return dispatch(successState(checkMT, res.data));
      }
      return dispatch(failedState(checkMT, res));
    } catch (err) {
      return dispatch(failedState(checkMT, err.message));
    }
  };
}
