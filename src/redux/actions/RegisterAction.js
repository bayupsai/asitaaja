import {
  registerApply,
  registerConfirm,
  registerNow,
  mobileApply,
  mobileConfirm,
} from '../../services/API';

//email
export function actionRegisterApply(payload) {
  return dispatch => {
    dispatch(requestState('registerApply'));
    return registerApply(payload)
      .then(res => {
        if (res.success) {
          const response = res.data;
          return dispatch(successState('registerApply', response, payload));
        } else {
          const response = res;
          return dispatch(failedState('registerApply', response, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('registerApply', err.message, payload));
      });
  };
}

export function actionRegisterConfirm(payload) {
  return dispatch => {
    dispatch(requestState('registerConfirm'));
    return registerConfirm(payload)
      .then(res => {
        if (res.success) {
          const response = res.data;
          return dispatch(successState('registerConfirm', response, payload));
        } else {
          const response = res.message;
          return dispatch(failedState('registerConfirm', response, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('registerConfirm', err.message, payload));
      });
  };
}
//email

//mobile
export function actionMobileApply(payload) {
  return dispatch => {
    dispatch(requestState('mobileApply'));
    return mobileApply(payload)
      .then(res => {
        if (res.success) {
          const response = res.data;
          return dispatch(successState('mobileApply', response, payload));
        } else {
          const response = res.message;
          return dispatch(failedState('mobileApply', response, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('mobileApply', err.message, payload));
      });
  };
}

export function actionMobileConfirm(payload) {
  return dispatch => {
    dispatch(requestState('mobileConfirm'));
    return mobileConfirm(payload)
      .then(res => {
        if (res.success) {
          const response = res.data;
          return dispatch(successState('mobileConfirm', response, payload));
        } else {
          const response = res.message;
          return dispatch(failedState('mobileConfirm', response, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('mobileConfirm', err.message, payload));
      });
  };
}
//mobile

//register all
export function actionRegisterNow(payload) {
  return dispatch => {
    dispatch(requestState('registerNow'));
    return registerNow(payload)
      .then(res => {
        if (res.success) {
          const response = res.data;
          return dispatch(successState('registerNow', response, payload));
        } else {
          const response = res.message;
          return dispatch(failedState('registerNow', response, payload));
        }
      })
      .catch(err => {
        return dispatch(failedState('registerNow', err.message, payload));
      });
  };
}
//register all

export function requestState(type) {
  if (type == 'registerApply') {
    return {
      type: 'REGISTER_APPLY',
    };
  } else if (type == 'registerConfirm') {
    return {
      type: 'REGISTER_CONFIRM',
    };
  } else if (type == 'registerNow') {
    return {
      type: 'REGISTER_NOW',
    };
  } else if (type == 'mobileApply') {
    return {
      type: 'MOBILE_APPLY',
    };
  } else if (type == 'mobileConfirm') {
    return {
      type: 'MOBILE_CONFIRM',
    };
  }
}

export function successState(type, data, payload) {
  if (type == 'registerApply') {
    return {
      type: 'REGISTER_APPLY_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type == 'registerConfirm') {
    return {
      type: 'REGISTER_CONFIRM_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type == 'registerNow') {
    return {
      type: 'REGISTER_NOW_SUCCESS',
      data: data,
      payload: payload,
    };
  } else if (type == 'mobileApply') {
    return {
      type: 'MOBILE_APPLY_SUCCESS',
      data,
      payload,
    };
  } else if (type == 'mobileConfirm') {
    return {
      type: 'MOBILE_CONFIRM_SUCCESS',
      data,
      payload,
    };
  }
}

export function failedState(type, data, payload) {
  if (type == 'registerApply') {
    return {
      type: 'REGISTER_APPLY_FAILED',
      data,
      payload,
    };
  } else if (type == 'registerConfirm') {
    return {
      type: 'REGISTER_CONFIRM_FAILED',
      data,
      payload,
    };
  } else if (type == 'registerNow') {
    return {
      type: 'REGISTER_NOW_FAILED',
      data,
      payload,
    };
  } else if (type == 'mobileApply') {
    return {
      type: 'MOBILE_APPLY_FAILED',
      data,
      payload,
    };
  } else if (type == 'mobileConfirm') {
    return {
      type: 'MOBILE_CONFIRM_FAILED',
      data,
      payload,
    };
  }
}
