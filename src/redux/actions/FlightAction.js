import {
  searchFlight,
  bookingFlight,
  paymentFlight,
  listAirport,
} from '../../services/API';
// import { searchFlight } from '../../services/Omega'

export function actionSearchFlight(payload) {
  return async dispatch => {
    dispatch(requestState('searchFlight'));
    try {
      const res = await searchFlight(payload);
      if (res.data.departures) {
        if (res.data.departures.length > 0) {
          if (res.data.returns) {
            if (res.data.returns.length > 0) {
              return dispatch(
                successState(
                  'searchFlight',
                  res.data.departures,
                  payload,
                  res.data.flight_calendar,
                  res.data.returns
                )
              );
            } else {
              return dispatch(
                successState(
                  'searchFlight',
                  res.data.departures,
                  payload,
                  res.data.flight_calendar,
                  null
                )
              );
            }
          } else {
            return dispatch(
              successState(
                'searchFlight',
                res.data.departures,
                payload,
                res.data.flight_calendar,
                null
              )
            );
          }
        } else {
          return dispatch(failedState('searchFlight', res, payload));
        }
      } else {
        return dispatch(failedState('searchFlight', res, payload));
      }
    } catch (err) {
      return dispatch(failedState('searchFlight', err.message, payload));
    }
  };
}

export function actionBookingFlight(payload, token) {
  return async dispatch => {
    dispatch(requestState('bookingFlight'));
    try {
      const res = await bookingFlight(payload, token);
      if (res.status) {
        return dispatch(successState('bookingFlight', res, payload));
      } else {
        return dispatch(failedState('bookingFlight', res, payload));
      }
    } catch (err) {
      return dispatch(failedState('bookingFlight', err, payload));
    }
  };
}

export function actionPaymentFlight(payload) {
  return async dispatch => {
    dispatch(requestState('paymentFlight'));
    try {
      const res = await paymentFlight(payload);
      console.log('RES STATUS PAYMENT', res);
      if (res.status_code === '00') {
        return dispatch(
          successState('paymentFlight', res.redirect_url, payload)
        );
      } else {
        return dispatch(failedState('paymentFlight', res, payload));
      }
    } catch (err) {
      return dispatch(failedState('paymentFlight', err.message, payload));
    }
  };
}

export function actionFlightSelected(payload, pageType = null) {
  if (pageType) {
    return {
      type: 'SET_FLIGHT_RETURN_SELECTED',
      payload: payload,
    };
  } else {
    return {
      type: 'SET_FLIGHT_SELECTED',
      payload: payload,
    };
  }
}

export function actionContactDetail(payload) {
  return {
    type: 'SET_CONTACT_DETAIL',
    payload: payload,
  };
}

export function actionSetPassenger(payload) {
  return {
    type: 'SET_PASSENGER',
    payload: payload,
  };
}

export function actionListAirport() {
  return async dispatch => {
    dispatch(requestState('listAirport'));
    try {
      const res = await listAirport();
      return res.result_code === 0
        ? dispatch(successState('listAirport', res.data.airports))
        : dispatch(failedState('listAirport', res));
    } catch (err) {
      return dispatch(failedState('listAirport', err.message));
    }
  };
}

export function requestState(type) {
  if (type === 'searchFlight') {
    return {
      type: 'SEARCH_FLIGHT',
    };
  } else if (type === 'bookingFlight') {
    return {
      type: 'BOOKING_FLIGHT',
    };
  } else if (type === 'paymentFlight') {
    return {
      type: 'PAYMENT_FLIGHT',
    };
  } else if (type === 'listAirport') {
    return {
      type: 'LIST_AIRPORT',
    };
  }
}

export function successState(
  type,
  data,
  payload,
  flightCalendar,
  returnFlight
) {
  if (type === 'searchFlight') {
    return {
      type: 'SEARCH_FLIGHT_SUCCESS',
      data: data,
      payload: payload,
      flightCalendar: flightCalendar,
      returnFlight: returnFlight,
    };
  } else if (type === 'bookingFlight') {
    return {
      type: 'BOOKING_FLIGHT_SUCCESS',
      data,
      payload,
    };
  } else if (type === 'paymentFlight') {
    return {
      type: 'PAYMENT_FLIGHT_SUCCESS',
      redirect_url: data,
      payload,
    };
  } else if (type === 'listAirport') {
    return {
      type: 'LIST_AIRPORT_SUCCESS',
      data,
    };
  }
}

export function failedState(type, message, payload) {
  if (type === 'searchFlight') {
    return {
      type: 'SEARCH_FLIGHT_FAILED',
      message,
      payload,
    };
  } else if (type === 'bookingFlight') {
    return {
      type: 'BOOKING_FLIGHT_FAILED',
      message,
      payload,
    };
  } else if (type === 'paymentFlight') {
    return {
      type: 'PAYMENT_FLIGHT_FAILED',
      message,
      payload,
    };
  } else if (type === 'listAirport') {
    return {
      type: 'LIST_AIRPORT_FAILED',
      message,
    };
  }
}
