export function actionDetailAttraction(data, type) {
  if (type == 'activities') {
    return {
      type: 'DETAIL_ACTIVITIES',
      data,
    };
  } else if (type == 'movies') {
    return {
      type: 'DETAIL_MOVIES',
      data,
    };
  } else if (type == 'events') {
    return {
      type: 'DETAIL_EVENTS',
      data,
    };
  }
}

export function actionDateAttraction(data, type) {
  if (type == 'setDateActivities') {
    return {
      type: 'SET_DATE_ACTIVITIES',
      date: data,
    };
  } else if (type == 'setTicketActivities') {
    return {
      type: 'SET_TICKET_ACTIVITIES',
      data,
    };
  } else if (type == 'setDateEvents') {
    return {
      type: 'SET_DATE_EVENTS',
      date: data,
    };
  } else if (type == 'setTicketEvents') {
    return {
      type: 'SET_TICKET_ACTIVITIES',
      data,
    };
  }
}

export function actionTicketAttraction(data, type) {
  if (type == 'ticketAcvitities') {
    return {
      type: 'TICKET_ACTIVITIES',
      data,
    };
  }
  if (type == 'ticketMovies') {
    return {
      type: 'TICKET_MOVIES',
      data,
    };
  }
  if (type == 'ticketEvents') {
    return {
      type: 'TICKET_EVENTS',
      data,
    };
  }
}
