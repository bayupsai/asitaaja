import { createSelector } from 'reselect';

// ========================= ALL FLIGHT =========================
const allFlight = state => state.flight;
export const makeAllFLight = createSelector(allFlight, item => {
  return item;
});
// ========================= ALL FLIGHT =========================

// ========================= LIST AIPORT =========================
const getAirport = state => state.flight.listAirport;
export const makeGetAirport = createSelector(getAirport, item => {
  return item;
});
// ========================= LIST AIPORT =========================

// ========================= SEARCH RESULT =========================
const searchResult = state => state.flight.searchResult;
export const makeSearchResult = createSelector(searchResult, item => {
  return item;
});
// ========================= SEARCH RESULT =========================

// ========================= SEARCH RETURN RESULT =========================
const searchReturnResult = state => state.flight.searchResultReturn;
export const makeSearchReturnResult = createSelector(
  searchReturnResult,
  item => {
    return item;
  }
);
// ========================= SEARCH RETURN RESULT =========================

// ========================= FLIGHT SELECTED =========================
const flightSelected = state => state.flight.flightSelected;
export const makeFligthSelected = createSelector(flightSelected, item => {
  return item;
});
// ========================= FLIGHT SELECTED =========================

// ========================= FLIGHT RETURN SELECTED =========================
const flightReturnSelected = state => state.flight.flightReturnSelected;
export const makeFlightReturnSelected = createSelector(
  flightReturnSelected,
  item => {
    return item;
  }
);
// ========================= FLIGHT RETURN SELECTED =========================

// ========================= DATA BOOKING FLIGHT =========================
const dataBooking = state => state.flight.dataBookingFlight;
export const makeDataBooking = createSelector(dataBooking, item => {
  return item;
});
// ========================= DATA BOOKING FLIGHT =========================

// ========================= CONTACT DETAIL =========================
const contactDetail = state => state.flight.contactDetail;
export const makeContactDetail = createSelector(contactDetail, item => {
  return item;
});
// ========================= CONTACT DETAIL =========================

// ========================= PASSENGER DATA =========================
const passengerData = state => state.flight.passengerData;
export const makePassengerData = createSelector(passengerData, item => {
  return item;
});
// ========================= PASSENGER DATA =========================
