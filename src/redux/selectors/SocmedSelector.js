import { createSelector } from 'reselect';

const socmedFacebook = state => state.socmed.dataFacebook;
export const makeSocmedFacebook = createSelector(socmedFacebook, item => {
  return item;
});

const socmedGoogle = state => state.socmed.dataGoogle;
export const makeSocmedGoogle = createSelector(socmedGoogle, item => {
  return item;
});
