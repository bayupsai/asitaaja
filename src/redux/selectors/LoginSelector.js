import { createSelector } from 'reselect';

const dataLogin = state => state.login.data;
export const makeDataLogin = createSelector(dataLogin, item => {
  return item;
});
