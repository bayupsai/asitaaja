import { createSelector } from 'reselect';

// ======================== OrderHistory ========================
const orderHistory = state => state.ordersHistory.data;
export const makeOrderHistory = createSelector(orderHistory, item => {
  return item;
});
// ======================== OrderHistory ========================
