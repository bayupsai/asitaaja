import { createSelector } from 'reselect';

const batikAll = state => state.batik.batikAll;
export const batikAllSelector = createSelector(batikAll, item => {
  return item;
});

const batikDetail = state => state.batik.batikDetail;
export const batikDetailSelector = createSelector(batikDetail, item => {
  return item;
});

const batikBook = state => state.batik.batikBook;
export const batikBookSelector = createSelector(batikBook, item => {
  return item;
});
