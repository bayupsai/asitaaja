import { createSelector } from 'reselect';

// ========================= LIST COUNTRY =========================
const listCountry = state => state.listCountry.dataCountry;
export const makeListCountry = createSelector(listCountry, item => {
  return item;
});
// ========================= LIST COUNTRY =========================
