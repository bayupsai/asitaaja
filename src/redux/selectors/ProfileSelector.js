import { createSelector } from 'reselect';

// ========================= Data Profile =========================
const dataProfile = state => state.profile.data;
export const makeDataProfile = createSelector(dataProfile, item => {
  return item;
});
// ========================= Data Profile =========================
