import { createSelector } from 'reselect';

const allHotel = state => state.hotel;
export const makeAllHotel = createSelector(allHotel, item => {
  return item;
});

const listCity = state => state.hotel.listHotelCity;
export const makeListCity = createSelector(listCity, item => {
  return item;
});

// List Hotel City Beds
const listDestinationBeds = state => state.hotel.listDestinationBeds;
export const makeListDestinationBeds = createSelector(
  listDestinationBeds,
  item => {
    return item;
  }
);

// Search Hotel Beds
const searchHotelBeds = state => state.hotel.searchHotelBeds;
export const makeSearchHotelBeds = createSelector(
  searchHotelBeds,
  item => item
);

// Payload Search Hotel Beds
const payloadBeds = state => state.hotel.payloadHotelBeds;
export const makeSearchPayloadBeds = createSelector(payloadBeds, item => item);

// Hotel Detail Beds
const hotelDetailBeds = state => state.hotel.chooseHotel;
export const makeHotelDetailBeds = createSelector(
  hotelDetailBeds,
  item => item
);

const searchPayload = state => state.hotel.payloadHotelBeds;
export const makeSearchPayload = createSelector(searchPayload, item => {
  return item;
});

const searchHotelResult = state => state.hotel.searchResult;
export const makeSearchHotelResult = createSelector(searchHotelResult, item => {
  return item;
});

const hotelDetail = state => state.hotel.hotelDetail;
export const makeHotelDetail = createSelector(hotelDetail, item => {
  return item;
});

const hotelDetailPayload = state => state.hotel.hotelDetailPayload;
export const makeHotelDetailPayload = createSelector(
  hotelDetailPayload,
  item => {
    return item;
  }
);

const roomHotel = state => state.hotel.roomHotel;
export const makeRoomHotel = createSelector(roomHotel, item => {
  return item;
});

const roomHotelPayload = state => state.hotel.roomHotelPayload;
export const makeRoomHotelPayload = createSelector(roomHotelPayload, item => {
  return item;
});

const chooseRoom = state => state.hotel.chooseHotelRoom;
export const makeChooseRoom = createSelector(chooseRoom, item => {
  return item;
});

const chooseHotelBooking = state => state.hotel.chooseHotelBooking;
export const makeChooseHotelBooking = createSelector(
  chooseHotelBooking,
  item => {
    return item;
  }
);

const contact = state => state.hotel.contactDetail;
export const makeContactDetail = createSelector(contact, item => {
  return item;
});

const guest = state => state.hotel.guestData;
export const makeGuestData = createSelector(guest, item => {
  return item;
});
