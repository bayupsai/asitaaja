import { createSelector } from 'reselect';

const payloadRegisterEmail = state => state.register.payloadApply;
export const makePayloadRegisterEmail = createSelector(
  payloadRegisterEmail,
  item => {
    return item;
  }
);

const paylaodRegisterMobile = state => state.register.payloadMobileApply;
export const makePayloadRegisterMobile = createSelector(
  paylaodRegisterMobile,
  item => {
    return item;
  }
);

const registerNow = state => state.register.registerNowData;
export const makeRegisterNow = createSelector(registerNow, item => {
  return item;
});
