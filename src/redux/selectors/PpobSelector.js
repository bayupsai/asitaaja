import { createSelector } from 'reselect';

// Search PDAM
const searchPDAM = state => state.ppob.searchPdam;
export const makeSearchPDAM = createSelector(searchPDAM, item => {
  return item;
});
