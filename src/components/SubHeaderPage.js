import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Header, Icon } from 'react-native-elements';
import {
  fontReguler,
  fontExtraBold,
  thameColors,
  asitaColor,
} from '../base/constant';
const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

// const TitleSubPage = (props) => (
//     <Grid style={styles.container}>
//         <ImageBackground source={require('../assets/icons/maps.png')} style={styles.imageBackground}>
//             <Grid style={{ flex: 0, paddingBottom: 10 }}>
//                 <Col>
//                     <Text style={styles.pageTitle}>
//                         {props.title}
//                     </Text>
//                 </Col>
//             </Grid>
//             {
//                 (props.flightInfo) ?
//                 <Grid>
//                     <Col style={{  flexDirection: "row" }}>
//                         <View>
//                             <Text style={{ color: "#FFFFFF", fontWeight: "500", fontSize: 16 }}>
//                                 {props.flightInfo.origin}
//                             </Text>
//                         </View>
//                         <View style={{ alignItems: "center", justifyContent: "flex-start", paddingLeft: 10, paddingRight: 10 }}>
//                             <Icon name="ios-arrow-round-forward" size={20} color="#FFFFFF" />
//                         </View>
//                         <View>
//                             <Text style={{ color: "#FFFFFF", fontWeight: "500", fontSize: 16 }}>
//                                 {props.flightInfo.destination}
//                             </Text>
//                         </View>
//                         <View style={{ paddingLeft: 10 }}>
//                             <Text style={{ color: "#FFFFFF" }}>|</Text>
//                         </View>
//                         <View style={{ paddingLeft: 10 }}>
//                             <Text style={{ color: "#FFFFFF" , fontSize: 16}}>
//                                 { moment(props.flightInfo.departureDate).format("DD MMM") }
//                             </Text>
//                         </View>
//                         <View style={{ paddingLeft: 10 }}>
//                             <Text style={{ color: "#FFFFFF", fontSize: 16 }}>|</Text>
//                         </View>
//                         <View style={{ paddingLeft: 10 }}>
//                             <IconPassengers name="account-outline" size={20} color="#FFFFFF" />
//                         </View>
//                         <View style={{ paddingLeft: 5 }}>
//                             <Text style={{ color: "#FFFFFF", fontSize: 16 }}>
//                                 {
//                                     props.flightInfo.adults + props.flightInfo.childs + props.flightInfo.infants
//                                 }
//                             </Text>
//                         </View>
//                     </Col>
//                 </Grid> : <Text></Text>

//             }
//         </ImageBackground>
//     </Grid>
// )

const TitleSubPage = props => (
  <Grid style={styles.container}>
    <Text
      style={{
        color: asitaColor.white,
        fontSize: 14,
        fontFamily: fontExtraBold,
        marginRight: 50,
      }}
    >
      {props.title}{' '}
      <Text style={{ fontFamily: fontExtraBold }}>{props.label}</Text>
    </Text>
  </Grid>
);

const SubHeaderPage = props => (
  <Header
    placement="left"
    centerComponent={<TitleSubPage title={props.title} label={props.label} />}
    rightComponent={null}
    containerStyle={{
      backgroundColor: asitaColor.marineBlue,
      height: deviceHeight / 10,
      width: '100%',
      borderBottomLeftRadius: 50,
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: 3,
    }}
  />
);

export default SubHeaderPage;

const styles = StyleSheet.create({
  imageBackground: {
    height: deviceHeight / 10,
    width: deviceWidth,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  container: {
    // backgroundColor: "#002561",
    // backgroundColor: thameColors.primary,
    height: deviceHeight / 10,
    width: deviceWidth,
    borderBottomLeftRadius: 50,
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginTop: -8,
  },
  pageTitle: {
    color: asitaColor.white,
    fontSize: 24,
    fontWeight: '400',
    paddingTop: 30,
  },
});
