import React from 'react';
import {
  Text,
  Image,
  TouchableOpacity,
  View,
  TouchableHighlight,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { fontExtraBold, thameColors, asitaColor } from '../base/constant/index';
import OfflineNotice from './OfflineNotice';
import { BarStyle, statusHeight } from '../elements/BarStyle';

// const BackButton = (props) => {
//     return (
//         <TouchableOpacity onPress={()=> props.goBack() } style={{ paddingRight: 20 }}>
//             <Icon name="md-arrow-back" color='#fff' type='ionicon' />
//         </TouchableOpacity>
//     )
// }

const TitlePage = props => {
  return (
    <Text
      style={{
        color: asitaColor.white,
        fontSize: 18,
        fontFamily: fontExtraBold,
        letterSpacing: 0.5,
        marginLeft: -10,
      }}
    >
      {props.title}
    </Text>
  );
};

const Menu = props => {
  return (
    <TouchableOpacity
      onPress={() => props.navigation.navigate('Language')}
      style={{ paddingRight: 0 }}
    >
      <Icon name="dots-three-vertical" color="#FFFFFF" type="entypo" />
    </TouchableOpacity>
  );
};

const HeaderLogin = props => (
  <View>
    <OfflineNotice />
    <BarStyle />
    <Header
      placement="center"
      // leftComponent={<BackButton goBack={props.callback} />}
      centerComponent={<TitlePage title={props.title} />}
      // rightComponent={(props.page) ? <View></View> : <Menu {...props} />}
      containerStyle={[
        {
          backgroundColor: asitaColor.marineBlue,
          paddingTop: 0,
          borderBottomWidth: 0,
          borderBottomColor: 'transparent',
          height: 30 + statusHeight,
        },
      ]}
    />
  </View>
);

export default HeaderLogin;
