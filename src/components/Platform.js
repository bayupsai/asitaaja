import { Platform, StyleSheet } from 'react-native';

const HeaderIos = StyleSheet.create({
  marginTop: Platform.OS === 'ios' ? 30 : 0,
});

export { HeaderIos };
