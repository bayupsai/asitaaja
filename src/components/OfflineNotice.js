import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { thameColors, fontBold } from '../base/constant';
import NetInfo from '@react-native-community/netinfo';

const { width } = Dimensions.get('window');

class OfflineNotice extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isConnected: true,
    };
  }

  shouldComponentUpdate(nextState) {
    return nextState.isConnected !== this.state.isConnected;
  }

  componentDidMount() {
    this.subscription();
    this.isConnectedFetchInterval();
  }

  componentWillUnmount() {
    if (this.subscription && typeof this.subscription.remove === 'function') {
      this.subscription.remove();
      clearInterval(this.isConnectedFetchInterval);
    }
  }

  isConnectedFetchInterval = () => {
    setInterval(async () => {
      await NetInfo.isConnected.fetch();
    }, 1000);
  };

  subscription = () =>
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectionChange
    );

  handleConnectionChange = isConnected => {
    this.setState({ isConnected });
  };

  render() {
    return !this.state.isConnected ? (
      <View style={styles.isConnection}>
        <Text style={styles.textConnection}>No Internet Connection</Text>
      </View>
    ) : null;
  }
}

export default OfflineNotice;

const styles = StyleSheet.create({
  isConnection: {
    width,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: thameColors.red,
  },
  textConnection: {
    color: thameColors.white,
    fontFamily: fontBold,
  },
});
