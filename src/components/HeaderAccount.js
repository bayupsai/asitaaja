import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { fontBold, thameColors } from '../base/constant/index';
import OfflineNotice from './OfflineNotice';
import { BarStyle } from '../elements/BarStyle';

const Back = props => {
  return (
    <TouchableOpacity>
      <Icon name="dots-three-vertical" color="#FFFFFF" type="entypo" />
    </TouchableOpacity>
  );
};

const Title = props => {
  return (
    <Text style={{ color: '#FFFFFF', fontSize: 18, fontFamily: fontBold }}>
      {props.title}
    </Text>
  );
};

const HeaderAccount = props => (
  <View>
    <OfflineNotice />
    <BarStyle />
    <Header
      leftComponent={<Text></Text>}
      rightComponent={<Back goBack={props.backButton} />}
      centerComponent={<Title title={props.title} />}
      containerStyle={[
        {
          backgroundColor: thameColors.primary,
          borderBottomColor: 'transparent',
        },
      ]}
      statusBarProps={{
        barStyle: 'light-content',
        backgroundColor: thameColors.statusBar,
      }}
    />
  </View>
);

export default HeaderAccount;
