import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { DotIndicator } from 'react-native-indicators';
import {
  fontReguler,
  fontBold,
  thameColors,
  asitaColor,
} from '../base/constant/index';
import { InputLogin, InputPassword } from '../elements/TextInput';
import { validateEmailFormat } from '../utilities/helpers';

//redux
import { connect } from 'react-redux';
import { actionLoginMail } from '../redux/actions/LoginAction';
import { actionGetProfile } from '../redux/actions/ProfileAction';

class LoginModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isValidEmail: true,
      secureText: true,
    };
  }

  handleInputEmail = email => {
    let checkEmail = validateEmailFormat(email);
    if (checkEmail) {
      this.setState({ email: email, isValidEmail: true });
    } else {
      this.setState({ email: '', isValidEmail: false });
    }
  };

  doLogin = () => {
    if (this.props.loading == false) {
      if (this.state.email == '' || this.state.password == '') {
        alert('Please fill all blank field');
      } else {
        let payload = {
          email: this.state.email,
          password: this.state.password,
        };
        this.props
          .dispatch(actionLoginMail(payload))
          .then(res => {
            if (res.type == 'LOGIN_MAIL_SUCCESS') {
              this.props
                .dispatch(actionGetProfile(this.props.dataLogin.access_token))
                .then(res => {
                  this.props.callbackLogin();
                  // alert('Success Login')
                })
                .catch(err => {
                  alert(err);
                });
            } else {
              alert(res.message);
            }
          })
          .catch(err => {
            alert(err);
          });
      }
    }
  };
  render() {
    return (
      <Modal
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        isVisible={this.props.isVisible}
        onBackButtonPress={this.props.onDismiss}
        onBackdropPress={this.props.onDismiss}
        style={{ flex: 1 }}
        children={<View></View>}
      >
        {!this.props.isLogin ? (
          <View style={styles.card}>
            <View style={{ alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: fontReguler,
                  color: thameColors.superBack,
                  fontSize: 18,
                }}
              >
                Hi, there
              </Text>
              <Text style={{ fontFamily: fontReguler }}>
                Log In to your account here
              </Text>
            </View>
            <View style={{ width: '100%' }}>
              <InputLogin
                placeholder="Email address"
                editable={true}
                autoCapitalize="none"
                onChangeText={data => this.handleInputEmail(data)}
              />
              {this.state.isValidEmail == false ? (
                <View>
                  <Text
                    style={{
                      fontFamily: fontBold,
                      color: 'red',
                      fontSize: 12,
                      paddingLeft: 10,
                    }}
                  >
                    Please enter a valid email address
                  </Text>
                </View>
              ) : null}
              {/* <InputLogin placeholder="Password" secureTextEntry={true} autoCapitalize="none" onChangeText={data => this.setState({ password: data })} /> */}
              <InputPassword
                placeholder="Password"
                onChangeText={data => this.setState({ password: data })}
                showPassword={this.state.secureText}
                onShowPassword={() =>
                  this.setState({ secureText: !this.state.secureText })
                }
              />
            </View>

            <TouchableOpacity
              onPress={() =>
                this.props.loading ? console.log('loading') : this.doLogin()
              }
              style={{
                marginTop: 20,
                backgroundColor: asitaColor.orange,
                width: '90%',
                padding: 10,
                borderRadius: 20,
                color: 'fff',
                fontSize: 16,
                alignItems: 'center',
              }}
            >
              {this.props.loading ? (
                <DotIndicator
                  size={8}
                  style={{ padding: 10 }}
                  color={thameColors.white}
                />
              ) : (
                <Text
                  style={{
                    color: thameColors.white,
                    fontSize: 16,
                    fontFamily: fontBold,
                  }}
                >
                  LOG IN
                </Text>
              )}
            </TouchableOpacity>

            <View style={{ alignItems: 'center', marginTop: 20 }}>
              <Text
                style={{
                  fontFamily: fontBold,
                  color: thameColors.superBack,
                  fontSize: 16,
                }}
              >
                New Member?
              </Text>
              <Text style={{ fontFamily: fontReguler }}>
                Signup and create your account
              </Text>
              <TouchableOpacity
                onPress={() => this.props.goRegister()}
                style={{ marginTop: 10 }}
              >
                <Text
                  style={{
                    fontFamily: fontBold,
                    fontSize: 18,
                    color: asitaColor.orange,
                  }}
                >
                  Register
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}
      </Modal>
    );
  }
}

function stateToProps(state) {
  return {
    loading: state.login.fetchingLogin,
    dataLogin: state.login.data,
    isLogin: state.login.isLogin,
    dataProfile: state.profile.data,
  };
}

export default connect(stateToProps)(LoginModal);

const styles = StyleSheet.create({
  card: {
    backgroundColor: thameColors.white,
    borderRadius: 10,
    padding: 20,
    marginTop: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
