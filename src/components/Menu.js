import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Dimensions,
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  fontReguler,
  thameColors,
  fontSemiBold,
  asitaColor,
} from '../base/constant/index';

let deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

class MainMenu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: null,
    };
  }

  // === CLOSE MODAL
  resetModal = () => this.setState({ visibleModal: null });
  // === CLOSE MODAL

  // === TOGEL MODAL
  _toggleModal = modal => this.setState({ visibleModal: modal });
  // === TOGEL MODAL

  // navigateTo = (page) => {
  //      const { props } = this.props
  //      if (page == "SearchFlight") {
  //           props.navigation.navigate(page, { getDestination: 'Bali /Denpasar' })
  //      } else {
  //           props.navigation.navigate(page)
  //      }
  // }

  navigateFlight = () =>
    this.props.navigation.navigate('SearchFlight', {
      getDestination: 'Bali /Denpasar',
    });
  navigateHotel = () => this.props.navigation.navigate('SearchHotel');
  navigateAttraction = () => this.props.navigation.navigate('Attraction');
  navigateTrains = () => this.props.navigation.navigate('Train');
  navigatePulsa = () => this.props.navigation.navigate('Pulsa');
  navigatePdam = () => this.props.navigation.navigate('Pdam');
  navigatePln = () => this.props.navigation.navigate('Pln');

  shareWA = () =>
    Linking.openURL('whatsapp://send?text=Halo&phone=+6285319724999');

  render() {
    return (
      <View>
        <Row>
          <Grid style={[styles.section, styles.mt15, styles.pt20]}>
            <Col style={styles.column}>
              <TouchableOpacity
                onPress={this.navigateFlight}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_flight.png')}
                    style={styles.icons}
                  />
                </View>
                <Text style={styles.titleMenu}>Flights</Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.column}>
              {/* this.navigateHotel */}
              <TouchableOpacity
                onPress={this.navigateHotel}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_hotel.png')}
                    style={styles.icons}
                  />
                </View>
                <Text style={styles.titleMenu}>Hotels</Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.column}>
              {/* this.navigateTrains */}
              <TouchableOpacity
                onPress={this.navigateTrains}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_trains.png')}
                    style={styles.icons40}
                  />
                </View>
              </TouchableOpacity>
              <Text style={styles.titleMenu}>Trains</Text>
            </Col>
            <Col style={styles.column}>
              <TouchableOpacity
                // this.navigatePdam
                onPress={this.navigatePdam}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_shop.png')}
                    style={styles.icons40}
                  />
                </View>
                <Text style={styles.titleMenu}>PDAM</Text>
              </TouchableOpacity>
            </Col>
          </Grid>
        </Row>

        <Row>
          <Grid style={[styles.section, styles.mb15]}>
            <Col style={styles.column}>
              <TouchableOpacity
                onPress={this.navigatePulsa}
                // onPress={this.props.commingSoon}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_pulsa.png')}
                    style={styles.icons}
                  />
                </View>
                <Text style={styles.titleMenu}>Top-Up</Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.column}>
              <TouchableOpacity
                // this.navigatePln
                onPress={this.navigatePln}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_paket_data.png')}
                    style={styles.icons40}
                  />
                </View>
                <Text style={styles.titleMenu}>PLN</Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.column}>
              {/* this.navigateAttraction */}
              <TouchableOpacity
                onPress={this.props.commingSoon}
                style={styles.menu}
              >
                <Image
                  source={require('../assets/icons/attraction_icon.png')}
                  style={styles.icons40}
                />
              </TouchableOpacity>
              <Text style={styles.titleMenu}>Attraction</Text>
            </Col>
            <Col style={styles.column}>
              {/* onPress={this.shareWA }    */}
              <TouchableOpacity
                onPress={this.props.more}
                style={{ alignItems: 'center' }}
              >
                <View style={styles.menu}>
                  <Image
                    source={require('../assets/icons/icon_more.png')}
                    style={styles.icons40}
                  />
                </View>
                <Text style={styles.titleMenu}>More</Text>
              </TouchableOpacity>
            </Col>
          </Grid>
        </Row>
      </View>
    );
  }
}

export default MainMenu;

const styles = StyleSheet.create({
  mt15: {
    marginTop: 15,
  },
  mb15: {
    marginBottom: 15,
  },
  pt20: {
    paddingTop: 20,
  },
  section: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 20,
    backgroundColor: asitaColor.white,
  },
  column: {
    alignItems: 'center',
  },
  menu: {
    backgroundColor: asitaColor.tealBlue,
    borderRadius: 15,
    height: deviceWidth / 6,
    width: deviceWidth / 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icons: {
    resizeMode: 'center',
    height: deviceWidth / 9,
    width: deviceWidth / 9,
  },
  icons40: {
    resizeMode: 'center',
    height: deviceWidth / 10,
    width: deviceWidth / 10,
  },
  icons50: {
    resizeMode: 'center',
    height: 30,
    width: 30,
  },
  titleMenu: {
    color: asitaColor.brownGreyTwo,
    fontFamily: fontSemiBold,
    paddingTop: 5,
    fontSize: 14,
  },

  // === Style Modal
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalPassenger: {
    backgroundColor: thameColors.white,
    padding: 0,
    height: deviceHeight / 3.5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  // === Style Modal
});
