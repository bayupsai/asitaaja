import React from 'react';
import {
  Text,
  Image,
  TouchableOpacity,
  View,
  TouchableHighlight,
  StatusBar,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { fontReguler, thameColors } from '../base/constant/index';
import OfflineNotice from './OfflineNotice';
import { statusHeight } from '../elements/BarStyle';

const BackButton = props => {
  return (
    <TouchableOpacity
      onPress={() => props.goBack()}
      style={{ paddingRight: 20 }}
    >
      <Icon name="md-arrow-back" color={thameColors.white} type="ionicon" />
    </TouchableOpacity>
  );
};

const Popper = props => {
  return (
    <TouchableOpacity
      onPress={() => props.goBack()}
      style={{ paddingRight: 20 }}
    >
      <Icon name="md-arrow-back" color={thameColors.white} type="ionicon" />
    </TouchableOpacity>
  );
};

const TitlePage = props => {
  return (
    <Text
      style={{
        color: thameColors.white,
        fontSize: 18,
        fontFamily: fontReguler,
        letterSpacing: 0.5,
        marginLeft: -10,
      }}
    >
      {props.title}
    </Text>
  );
};

const HeaderPageProfile = props => (
  <View>
    <OfflineNotice />
    <StatusBar barStyle="default" backgroundColor={thameColors.superBack} />
    <Header
      placement="left"
      leftComponent={<BackButton goBack={props.callback} />}
      centerComponent={<TitlePage title={props.title} />}
      // rightComponent={<Popper /> }
      containerStyle={[
        {
          backgroundColor: thameColors.superBack,
          paddingTop: 0,
          borderBottomWidth: 0,
          borderBottomColor: 'transparent',
          height: 30 + statusHeight,
        },
      ]}
    />
  </View>
);

export default HeaderPageProfile;
