import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
// import ShimmerPlaceHolder from 'react-native-shimmer-placeholder'
import { Grid, Col, Row } from 'react-native-easy-grid';

const window = Dimensions.get('window');
let deviceWidth = window.width;
let deviceHeight = window.height;

const Placeholder = props => {
  return (
    <Grid {...props}>
      <Col size={3}>
        {/* image */}
        {/* <ShimmerPlaceHolder autoRun={true} visible={props.visible} width={105} height={155} /> */}
      </Col>
      <Col size={5}>
        <Row size={8}>
          <Col>
            {/* <ShimmerPlaceHolder autoRun={true} visible={props.visible}  />
                        <ShimmerPlaceHolder autoRun={true} visible={props.visible}  /> */}
          </Col>
        </Row>
        <Row size={2}>
          <Col>
            {/* <ShimmerPlaceHolder autoRun={true} visible={props.visible} /> */}
          </Col>
        </Row>
      </Col>
      {props.children}
    </Grid>
  );
};

export default Placeholder;

const styles = StyleSheet.create({});
