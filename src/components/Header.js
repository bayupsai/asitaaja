import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
  StyleSheet,
  Easing,
  StatusBar,
  Platform,
} from 'react-native';
import { Header } from 'react-native-elements';
import { Row, Col } from 'react-native-easy-grid';
import { thameColors, asitaColor } from '../base/constant';
import { Icon } from 'react-native-elements';
import OfflineNotice from './OfflineNotice';
import { scale, verticalScale } from '../Const/ScaleUtils';
import { BarStyle, statusHeight } from '../elements/BarStyle';

class AnimateCircle extends React.PureComponent {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.animate();
  }
  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
    }).start(() => this.animate());
  }

  render({ children } = this.props) {
    const borderBottomColor = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: ['rgba(0, 0, 0, 0)', thameColors.yellow, 'rgba(0, 0, 0, 0)'],
    });
    return (
      <Animated.View
        style={{
          borderWidth: 2,
          borderRadius: 50,
          borderColor: borderBottomColor,
          padding: 2,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {children}
      </Animated.View>
    );
  }
}

const HeaderLeft = () => {
  return (
    <Row style={{ marginLeft: 0, paddingTop: 5 }}>
      <Image
        source={require('../assets/logos/logo-asitatrave.png')}
        style={{ height: null, width: 120, resizeMode: 'center' }}
      />
    </Row>
  );
};

const HeaderRight = params => {
  const navigation = params.data.navigation;
  navigateTo = page => {
    navigation.navigate('Chatbot', { load: 1 });
  };

  navigateToNotifications = page => {
    navigation.navigate('Notifications');
  };

  navigateLanguage = page => {
    navigation.navigate('Language');
  };

  return (
    <Row
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 12.5,
        paddingTop: 5,
      }}
    >
      {/* <TouchableOpacity style={{ flex: 0, }} onPress={() => this.navigateToNotifications('Notifications')}>
                <Col style={{ paddingRight: 15, justifyContent: "center" }}>
                    <AnimateCircle>
                        <Image source={require('../assets/icons/label_rounded.png')} resizeMode={'center'} style={{ width: scale(30), height: scale(30) }} />
                    </AnimateCircle>
                </Col>
            </TouchableOpacity> */}
      <TouchableOpacity
        style={{ flex: 0 }}
        onPress={() => this.navigateTo('Chatbot')}
      >
        <Col style={{ justifyContent: 'center' }}>
          <Image
            source={require('../assets/icons/messenger_rounded.png')}
            resizeMode={'center'}
            style={{ width: scale(40), height: scale(30) }}
          />
        </Col>
      </TouchableOpacity>
    </Row>
  );
};

const HeaderBar = props => (
  <View>
    <OfflineNotice />
    <BarStyle home={props.homeBar} />
    <Header
      leftComponent={<View />}
      centerComponent={<HeaderLeft />}
      rightComponent={<View />}
      containerStyle={[
        {
          backgroundColor: asitaColor.white,
          paddingTop: 0,
          height: 30 + statusHeight,
        },
      ]}
    />
    {/* <Header
            leftComponent={<HeaderLeft />}
            centerComponent={<View></View>}
            rightComponent={(props.page) ? <View></View> : <HeaderRight data={props} />}
            containerStyle={{
                backgroundColor: thameColors.primary,
                paddingTop: 0,
                height: 60
            }}
        /> */}
  </View>
);
export default HeaderBar;
