/* eslint-disable no-lone-blocks */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
//local component
import {
  fontBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../base/constant/index';
import { ButtonRounded, ButtonRoundedNonActive } from '../elements/Button';

{
  /* ============== Custom config ============== */
}
const ZeroConfig = props => {
  return <View style={styles.zeroConfig} />;
};
{
  /* ============== Custom Config ============== */
}

{
  /* ============== Normal Alert ============== */
}
const NormalAlert = props => {
  return (
    <View style={styles.card}>
      <View style={styles.cardHeader}>
        <Text
          style={{
            fontFamily: fontBold,
            color: asitaColor.orange,
            fontSize: 20,
          }}
        >
          {props.title}
        </Text>
      </View>
      <View style={styles.cardBody}>
        <Text
          style={{
            fontFamily: fontReguler,
            textAlign: 'center',
            fontSize: 16,
            color: thameColors.textBlack,
          }}
        >
          {props.contentText}
        </Text>
      </View>
      <ButtonRounded size="small" label="OK" onClick={props.onPress} />
    </View>
  );
};
{
  /* ============== Q&A Alert ============== */
}
const QuestAlert = props => {
  return (
    <View style={styles.card}>
      <View style={styles.cardHeader}>
        <Text
          style={{
            fontFamily: fontBold,
            color: props.titleColor ? props.titleColor : asitaColor.orange,
            fontSize: 20,
            textAlign: 'center',
          }}
        >
          {props.title}
        </Text>
      </View>
      <View style={[styles.cardBody]}>
        <Text
          style={{ fontFamily: fontReguler, textAlign: 'center', fontSize: 16 }}
        >
          {props.contentText}
        </Text>
      </View>
      <ButtonRounded
        size="small"
        label={props.labelOk}
        onClick={props.onPress}
      />
      <View style={{ marginTop: 10 }}>
        <ButtonRoundedNonActive
          size="small"
          label={props.labelCancel}
          onClick={props.onDismiss}
        />
      </View>
    </View>
  );
};
{
  /* ============== Q&A Alert ============== */
}

export default class AlertModal extends Component {
  render() {
    return (
      <Modal
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        isVisible={this.props.isVisible}
        onBackButtonPress={this.props.onDismiss}
        onBackdropPress={this.props.onDismiss}
        style={styles.container}
        children={<View />}
      >
        {/* ============== Normal Alert ============== */}
        {this.props.type === 'normal' ? (
          <NormalAlert
            title={this.props.title}
            contentText={this.props.contentText}
            onPress={this.props.onPress}
          />
        ) : (
          <ZeroConfig />
        )}
        {/* ============== Normal Alert ============== */}

        {/* ============== Q&A Alert ============== */}
        {this.props.type === 'qna' ? (
          <QuestAlert
            title={this.props.title}
            contentText={this.props.contentText}
            onPress={this.props.onPress}
            onDismiss={this.props.onDismiss}
            labelCancel={this.props.labelCancel}
            labelOk={this.props.labelOk}
            titleColor={this.props.titleColor}
          />
        ) : (
          <ZeroConfig />
        )}
        {/* ============== Q&A Alert ============== */}
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  zeroConfig: {
    width: 0,
    height: 0,
  },
  card: {
    backgroundColor: '#fff',
    width: '100%',
    borderRadius: 5,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  cardBody: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
});
