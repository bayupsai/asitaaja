import React from 'react';
import { View, Text, StyleSheet, Switch, TouchableOpacity } from 'react-native';
import numeral from 'numeral';
import { fontBold, fontReguler, thameColors } from '../base/constant';

const GlobalInsurance = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.textBold}>{props.title}</Text>
      <View style={styles.card}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={[styles.textRegular, { color: thameColors.primary }]}>
            Rp
            {numeral(props.price)
              .format('0,00')
              .replace(/,/g, '.')}{' '}
            <Text style={{ color: thameColors.textBlack }}>/pax</Text>
          </Text>
          <Switch value={props.value} onValueChange={() => props.onChange()} />
        </View>
        <View style={{ marginTop: 15 }}>
          <Text style={[styles.textRegular, { color: thameColors.darkGray }]}>
            Want to read {props.title} information?{' '}
            <Text
              onPress={() => alert(`${props.title} Info`)}
              style={[
                styles.textBold,
                { color: thameColors.primary, fontSize: 16 },
              ]}
            >
              More Info
            </Text>
          </Text>
        </View>
      </View>
    </View>
  );
};

export default GlobalInsurance;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  card: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    padding: 20,
    borderRadius: 5,
  },
  textBold: {
    fontFamily: fontBold,
    fontSize: 20,
    color: thameColors.textBlack,
  },
  textRegular: {
    fontFamily: fontReguler,
    color: thameColors.textBlack,
    fontSize: 16,
  },
});
