import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import { Icon } from 'react-native-elements';
import {
  fontBold,
  fontReguler,
  fontExtraBold,
  thameColors,
  asitaColor,
} from '../../base/constant/index';
import { actionSearchFlight } from '../../redux/actions/FlightAction';
import { actionSearchTrain } from '../../redux/actions/TrainAction';

const toMonth = moment(new Date())
  .format('MMM')
  .toUpperCase();

export default class CalendarSlider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      buttonColor: '#979797',
      selectedItems: [4],
      unusedDateState: {
        aDay: moment(new Date())
          .add(-1, 'days')
          .format('YYYY-MM-DD'),
        twoDay: moment(new Date())
          .add(-2, 'days')
          .format('YYYY-MM-DD'),
        threeDay: moment(new Date())
          .add(-3, 'days')
          .format('YYYY-MM-DD'),
      },
    };
  }

  datingNow = () => {
    moment().format('MMMM Do YYYY, h:mm:ss a');
  };
  //Select Date
  selectDate = date => {
    if (this.props.type == 'flight') {
      this.selectDateFlight(date);
    } else if (this.props.type == 'flightReturn') {
      this.selectDateFlightReturn(date);
    } else if (this.props.type == 'train') {
      this.selectDateTrain(date);
      // alert(date)
    }
  };
  //Select Date

  //Unused Date
  unusedDate = date => {
    let now = new Date();
    let { unusedDateState } = this.state;
    if (unusedDateState.aDay == date) {
    } else {
      this.selectDate(date);
    }
  };
  //Unused Date

  //TRAIN FUCTION SELECT DATE
  selectDateTrain = date => {
    const pay = this.props.payloads;
    let payloadData = {
      org: pay.org,
      des: pay.des,
      departure_date: pay.departure_date,
      adult_quantity: pay.adult_quantity,
      infant_quantity: pay.infant_quantity,
      // command: 'SCHEDULE',
      // product: 'KERETA',
      // data: {
      //   departure_code: data.departure_code,
      //   arrival_code: data.arrival_code,
      //   date: moment(date).format('YYYY-MM-DD'),
      //   adult: data.adult,
      //   class: ''
      // }
    };

    // if (data.infant) {
    //   payloadData.data.infant = data.infant;
    // }
    this.props.dispatch(actionSearchTrain(payloadData));
  };
  //TRAIN FUCTION SELECT DATE

  //flight function select date
  selectDateFlight = date => {
    let { data } = this.props.payload;
    let payloadData = {
      command: 'SCHEDULE',
      product: 'FLIGHT',
      data: {
        departure_code: data.departure_code,
        departure_date: moment(date).format('YYYY-MM-DD'),
        arrival_code: data.arrival_code,
        adult: data.adult,
        class: data.class,
      },
    };
    if (data.return_date !== '') {
      payloadData.data.return_date = data.return_date;
    }
    if (data.child) {
      payloadData.data.child = data.child;
    }
    if (data.infant) {
      payloadData.data.infant = data.infant;
    }
    this.props.dispatch(actionSearchFlight(payloadData));
  };
  selectDateFlightReturn = date => {
    let { data } = this.props.payload;
    let payloadData = {
      command: 'SCHEDULE',
      product: 'FLIGHT',
      data: {
        departure_code: data.departure_code,
        departure_date: data.departure_date,
        arrival_code: data.arrival_code,
        adult: data.adult,
        class: data.class,
        return_date: moment(date).format('YYYY-MM-DD'),
      },
    };
    if (data.child) {
      payloadData.data.child = data.child;
    }
    if (data.infant) {
      payloadData.data.infant = data.infant;
    }
    this.props.dispatch(actionSearchFlight(payloadData));
  };
  //flight function select date

  render() {
    let { aDay, twoDay, threeDay } = this.state.unusedDateState;
    let dateDay = [
      {
        id: 1,
        date: moment(this.props.date)
          .add(-3, 'days')
          .format('D'),
        day: moment(this.props.date)
          .add(-3, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date)
          .add(-3, 'days')
          .format('YYYY-MM-DD'),
      },
      {
        id: 2,
        date: moment(this.props.date)
          .add(-2, 'days')
          .format('D'),
        day: moment(this.props.date)
          .add(-2, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date)
          .add(-2, 'days')
          .format('YYYY-MM-DD'),
      },
      {
        id: 3,
        date: moment(this.props.date)
          .add(-1, 'days')
          .format('D'),
        day: moment(this.props.date)
          .add(-1, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date)
          .add(-1, 'days')
          .format('YYYY-MM-DD'),
      },
      {
        id: 4,
        date: moment(this.props.date).format('D'),
        day: moment(this.props.date)
          .add(0, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date).format('YYYY-MM-DD'),
      },
      {
        id: 5,
        date: moment(this.props.date)
          .add(1, 'days')
          .format('D'),
        day: moment(this.props.date)
          .add(1, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date)
          .add(1, 'days')
          .format('YYYY-MM-DD'),
      },
      {
        id: 6,
        date: moment(this.props.date)
          .add(2, 'days')
          .format('D'),
        day: moment(this.props.date)
          .add(2, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date)
          .add(2, 'days')
          .format('YYYY-MM-DD'),
      },
      {
        id: 7,
        date: moment(this.props.date)
          .add(3, 'days')
          .format('D'),
        day: moment(this.props.date)
          .add(3, 'days')
          .format('ddd'),
        selectDate: moment(this.props.date)
          .add(3, 'days')
          .format('YYYY-MM-DD'),
      },
    ];
    const checkColor = thameColors.gray;
    return (
      <View
        style={{
          flexDirection: 'row',
          marginLeft: 10,
          marginRight: 15,
          alignItems: 'center',
        }}
      >
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {/* <Text style={styles.month}>{toMonth}</Text> */}
          {dateDay.map((item, key) => (
            <TouchableOpacity
              onPress={() => this.unusedDate(item.selectDate)}
              key={key}
            >
              {this.state.selectedItems == item.id ? (
                <View style={styles.today}>
                  <View>
                    <Text style={styles.textDateActive}>{item.date}</Text>
                  </View>
                  <View style={{ paddingTop: 7.5 }}>
                    <Text style={styles.textDayActive}>
                      {item.day.toUpperCase()}
                    </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: 'flex-start', paddingTop: 5 }}
                  >
                    <Icon
                      name="dot-single"
                      type="entypo"
                      size={25}
                      color={asitaColor.orange}
                    />
                  </View>
                </View>
              ) : (
                <View style={styles.day}>
                  <View>
                    <Text
                      style={[
                        styles.textDate,
                        aDay == item.selectDate ||
                        twoDay == item.selectDate ||
                        threeDay == item.selectDate
                          ? { color: checkColor }
                          : { position: 'relative' },
                      ]}
                    >
                      {item.date}
                    </Text>
                  </View>
                  <View style={{ paddingTop: 7.5 }}>
                    <Text
                      style={[
                        styles.textDay,
                        aDay == item.selectDate ||
                        twoDay == item.selectDate ||
                        threeDay == item.selectDate
                          ? { color: checkColor }
                          : { position: 'relative' },
                      ]}
                    >
                      {item.day.toUpperCase()}
                    </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: 'flex-start', paddingTop: 5 }}
                  >
                    <Icon
                      name="dot-single"
                      type="entypo"
                      size={25}
                      color="#ffffff"
                    />
                  </View>
                </View>
              )}
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sectionMont: {
    backgroundColor: '#f6f6f6',
  },
  // month: {
  //     color: '#767676',
  //     fontFamily: fontBold,
  //     transform: [
  //         { rotate: '-90deg' },
  //         { translateY: 15 }
  //     ],
  //     fontSize: 14,
  //     paddingBottom: 15,
  //     fontFamily: fontReguler
  // },
  day: {
    alignItems: 'center',
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
  },
  today: {
    alignItems: 'center',
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
  },
  textDate: { fontFamily: fontExtraBold, color: '#222222', fontSize: 18 },
  textDateActive: {
    fontFamily: fontExtraBold,
    color: asitaColor.orange,
    fontSize: 18,
  },
  textDay: { fontFamily: fontBold, color: '#222222', fontSize: 13 },
  textDayActive: {
    fontFamily: fontBold,
    color: asitaColor.orange,
    fontSize: 13,
  },
});
