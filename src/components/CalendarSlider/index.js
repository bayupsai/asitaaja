import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import CalendarSlider from './CalendarSlider';
import { formatDateTrain } from '../../utilities/helpers';

const window = Dimensions.get('window');
let deviceWidth = window.width;

class DateSlider extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      typeDate: null,
    };
  }

  componentDidMount() {
    this.dateCondition();
  }

  dateCondition = () => {
    const {
      departureDateFlight,
      returnDateFlight,
      departureDateTrain,
    } = this.props;
    if (this.props.type == 'flight') {
      return departureDateFlight;
    } else if (this.props.type == 'flightReturn') {
      return returnDateFlight;
    } else if (this.props.type == 'train') {
      return formatDateTrain(departureDateTrain);
    } else if (this.props.type == 'trainReturn') {
      return returnDateTrain;
    }
  };

  render() {
    return (
      <View style={styles.card}>
        <CalendarSlider date={this.dateCondition()} {...this.props} />
      </View>
    );
  }
}

function stateToProps(state) {
  return {
    departureDateFlight: state.flight.departureDate,
    loadingSingleFlight: state.flight.singleSearchFetch,
    searchResult: state.flight.searchResult,
    returnDateFlight: state.flight.returnDate,
    departureDateTrain: state.train.departureDate,
    loadingSingleTrain: state.train.fetchingTrain,
    searchResultTrain: state.train.searchTrain,
    returnDateTrain: state.train.returnDate,
  };
}

export default connect(stateToProps)(DateSlider);

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#FFFFFF',
    width: deviceWidth / 1.05,
    height: 80,
    borderRadius: 6,
    marginBottom: 10,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
  },
});
