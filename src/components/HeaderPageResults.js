/* eslint-disable radix */
// @flow
import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { Grid, Col } from 'react-native-easy-grid';
import moment from 'moment';
import { connect } from 'react-redux';
import {
  fontBold,
  fontExtraBold,
  fontReguler,
  thameColors,
  asitaColor,
} from '../base/constant/index';
import OfflineNotice from './OfflineNotice';
import { BarStyle, statusHeight } from '../elements/BarStyle';

const BackButton = props => {
  return (
    <TouchableOpacity onPress={() => props.goBack()} style={styles.leftMargin}>
      <Icon
        name="ios-arrow-round-back"
        color={thameColors.white}
        size={42}
        type="ionicon"
      />
    </TouchableOpacity>
  );
};

// ========================== TITLE FLIGHT ==========================
const TitlePage = props => {
  return (
    <Grid style={styles.contentTitle}>
      <Col size={4} style={styles.centerEnd}>
        <Text style={styles.textBold}>{props.city.data.departure_code}</Text>
      </Col>
      <Col size={2} style={styles.center}>
        <Image
          resizeMode="center"
          style={styles.imgTitle}
          source={require('../assets/icons/icon_header_flight_result.png')}
        />
      </Col>
      <Col size={4} style={styles.centerStart}>
        <Text style={styles.textBold}>{props.city.data.arrival_code}</Text>
      </Col>
    </Grid>
  );
};
// ========================== TITLE FLIGHT ==========================

// ========================== TITLE TRAIN ==========================
const TitlePageTrain = props => {
  return (
    <Grid style={styles.contentTitle}>
      <Col size={4} style={styles.centerEnd}>
        <Text style={styles.textBold}>{props.city.departure_code}</Text>
      </Col>
      <Col size={2} style={styles.center}>
        <Image
          source={require('../assets/icons/icon_trains.png')}
          style={styles.imgTrain}
          resizeMode="center"
        />
      </Col>
      <Col size={4} style={styles.centerStart}>
        <Text style={styles.textBold}>{props.city.arrival_code}</Text>
      </Col>
    </Grid>
  );
};
// ========================== TITLE TRAIN ==========================

// ========================== TITLE Hotel ==========================
const TitlePageHotel = props => {
  return (
    <Grid style={styles.contentTitle}>
      <Col size={2} style={styles.centerEnd}>
        <Text style={styles.textBold}>{/* {props.city.departure_code} */}</Text>
      </Col>
      <Col size={6} style={styles.center}>
        <Text style={styles.textBold}>{props.cityName}</Text>
      </Col>
      <Col size={2} style={styles.centerStart}>
        <Text style={styles.textBold}>{/* {props.city.arrival_code} */}</Text>
      </Col>
    </Grid>
  );
};
// ========================== TITLE Hotel ==========================

const renderTitle = (page, props) => {
  if (page === 'train') {
    return <TitlePageTrain city={props.train} />;
  }
  if (page === 'flight') {
    return <TitlePage city={props.flight} />;
  }
  if (page === 'hotel') {
    return <TitlePageHotel city={props.hotel} cityName={props.cityName} />;
  }
  if (page === 'orderFlight') {
    return <TitlePage city={props.flight} />;
  }
};

const Dot = props => (
  <Icon type="entypo" name="dot-single" color={thameColors.white} />
);

const HeaderPageResults = props => (
  <View>
    <OfflineNotice />
    <BarStyle />
    <View>
      <Header
        placement="left"
        leftComponent={<BackButton goBack={props.callback} />}
        centerComponent={renderTitle(props.page, props)}
        rightComponent={props.rightComponent ? props.rightComponent : null}
        containerStyle={styles.headerContainer}
      />
    </View>

    {/* ============= FLIGHT ============= */}
    {props.page === 'flight' ? (
      <View style={styles.headerFlight}>
        <Text style={styles.textReguler}>
          {props.loading
            ? 'Wait for a moment'
            : `${moment(props.departureDate).format('DD MMM')} - ${parseInt(
                props.flight.data.adult
              ) +
                (props.flight.data.child
                  ? parseInt(props.flight.data.child)
                  : 0) +
                (props.flight.data.infant
                  ? parseInt(props.flight.data.infant)
                  : 0)} Pax`}
        </Text>
      </View>
    ) : null}
    {/* ============= FLIGHT ============= */}

    {/* ============= TRAIN ============= */}
    {props.page === 'train' ? (
      <View style={styles.headerTrain}>
        <Text style={styles.textReguler}>
          {moment(props.departureDateTrain).format('DD MMM')} -
          {parseInt(props.train.adult)} Pax
        </Text>
      </View>
    ) : null}
    {/* ============= TRAIN ============= */}

    {/* ============= Hotel ============= */}
    {props.page === 'hotel' ? (
      <View style={styles.headerHotel}>
        <View style={styles.rowDir}>
          <Text style={styles.textReguler}>
            {moment(props.hotel.stay.checkIn).format('DD MMM')}
          </Text>
          {/* <Dot />
                        <Text style={{ color: thameColors.white, fontSize: 14, fontFamily: fontReguler }}>{parseInt(props.hotel.check_out - props.hotel.check_in)} Night </Text> */}
          <Dot />
          <Text style={styles.textReguler}>
            {parseInt(
              props.hotel.occupancies[0].adults +
                props.hotel.occupancies[0].children
            )}{' '}
            Guest
          </Text>
        </View>
      </View>
    ) : null}
    {/* ============= Hotel ============= */}

    {/* ============= Order Flight ============= */}
    {props.page === 'orderFlight' ? (
      <View style={styles.headerOrder}>
        <Text style={styles.textReguler}>Booking ID {props.bookingId}</Text>
      </View>
    ) : null}
    {/* ============= Order Flight ============= */}
  </View>
);

function stateToProps(state) {
  return {
    departureDate: state.flight.departureDate,
    departureDateTrain: state.train.departureDate,
  };
}

export default connect(stateToProps)(HeaderPageResults);

const styles = StyleSheet.create({
  leftMargin: { marginLeft: 10 },
  contentTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: -30,
  },
  centerEnd: { alignItems: 'flex-end', justifyContent: 'center' },
  center: { alignItems: 'center', justifyContent: 'center' },
  centerStart: { alignItems: 'flex-start', justifyContent: 'center' },
  textBold: {
    color: thameColors.white,
    fontSize: 18,
    fontFamily: fontBold,
    paddingRight: 5,
  },
  textReguler: {
    color: thameColors.white,
    fontSize: 14,
    fontFamily: fontReguler,
  },
  imgTitle: { width: 40, height: 30 },
  imgTrain: { width: 30, height: 25, tintColor: thameColors.yellow },
  rowDir: { flexDirection: 'row', alignItems: 'center' },
  headerContainer: {
    backgroundColor: asitaColor.marineBlue,
    paddingTop: 0,
    borderBottomWidth: 0,
    borderBottomColor: 'transparent',
    height: 30 + statusHeight,
  },
  headerFlight: {
    backgroundColor: asitaColor.marineBlue,
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 0,
    marginTop: -10,
    alignItems: 'center',
    paddingBottom: 30,
  },
  headerTrain: {
    backgroundColor: asitaColor.marineBlue,
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 0,
    marginTop: -10,
    alignItems: 'center',
    paddingBottom: 30,
  },
  headerHotel: {
    backgroundColor: asitaColor.marineBlue,
    marginTop: -10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 30,
  },
  headerOrder: {
    backgroundColor: asitaColor.marineBlue,
    padding: 10,
    paddingTop: 0,
    marginTop: -10,
    alignItems: 'center',
    paddingBottom: 30,
  },
});
