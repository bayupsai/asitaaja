import React from 'react';
import { Text } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { DotIndicator } from 'react-native-indicators';
import Modal from 'react-native-modal';
import { thameColors } from '../base/constant';

const LoadingDefault = props => (
  <Modal
    useNativeDriver={true}
    hideModalContentWhileAnimating={true}
    isVisible={props.isVisible}
    animationIn="zoomInDown"
    animationOut="zoomOutUp"
    animationInTiming={250}
    animationOutTiming={250}
    backdropTransitionInTiming={250}
    backdropTransitionOutTiming={250}
  >
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#FFFFFF',
        marginRight: 10,
        marginLeft: 10,
        borderRadius: 5,
        height: 'auto',
        flex: 0,
      }}
    >
      <Col style={{ alignItems: 'center' }}>
        <Row style={{ flex: 0 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000' }}>
            Please Wait.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5 }}>
          <Text style={{ fontWeight: '400', fontSize: 16, color: '#000' }}>
            We still process your request.
          </Text>
        </Row>
        <Row style={{ flex: 0, paddingTop: 5, alignItems: 'center' }}>
          <DotIndicator
            size={10}
            style={{ flex: 0, backgroundColor: '#FFFFFF' }}
            color={thameColors.secondary}
          />
        </Row>
      </Col>
    </Grid>
  </Modal>
);

export { LoadingDefault };
