import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { InputCircle } from '../../elements/TextInput';
import { thameColors } from '../../base/constant';
import { scale } from '../../Const/ScaleUtils';

const Left = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{ justifyContent: 'center', alignItems: 'flex-end' }}
    >
      {/* <Icon type="antdesign" name="arrowleft" color={thameColors.white} /> */}
      <Image
        source={require('../../assets/icons/arrow_left_icon.png')}
        style={{
          width: scale(22.5),
          height: scale(20),
          tintColor: thameColors.white,
        }}
      />
    </TouchableOpacity>
  );
};

const Center = props => {
  return (
    <View style={{ width: '105%' }}>
      <InputCircle
        placeholder={props.placeholder}
        placeholderTextColor={thameColors.primary}
        onChangeText={text => props.onChangeText(text)}
      />
    </View>
  );
};

class HeaderSearch extends React.PureComponent {
  render({ children } = this.props) {
    return (
      <Header
        leftComponent={<Left onPress={this.props.onPress} />}
        centerComponent={
          <View
            style={{
              flexDirection: 'row',
              width: '125%',
              alignItems: 'center',
            }}
          >
            <Center
              placeholder={this.props.placeholder}
              onChangeText={text => this.props.onChangeText(text)}
            />
            {this.props.dropDown === true ? (
              <TouchableOpacity
                onPress={this.props.onDropDown}
                style={{ transform: [{ translateX: -40 }] }}
              >
                <Icon
                  type="ionicon"
                  name="ios-arrow-down"
                  color={thameColors.primary}
                  size={25}
                />
              </TouchableOpacity>
            ) : (
              <View />
            )}
          </View>
        }
        rightComponent={
          <TouchableOpacity
            onPress={this.props.onPressRight}
            style={styles.buttonStyle}
          >
            {/* <Icon type="feather" name="bookmark" color={thameColors.white} size={30} /> */}
            {children}
          </TouchableOpacity>
        }
        containerStyle={{ paddingTop: -15 }}
        centerContainerStyle={{ width: '100%' }}
        leftContainerStyle={{ transform: [{ translateX: 10 }] }}
        rightContainerStyle={{ marginRight: 10 }}
      />
    );
  }
}

export default HeaderSearch;

const styles = StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
