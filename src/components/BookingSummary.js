import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { Grid, Col } from 'react-native-easy-grid';
import { fontExtraBold, fontReguler } from '../base/constant/index';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const BookingSummary = props => (
  <View style={styles.container}>
    <View style={styles.card}>
      <Grid
        style={{
          borderBottomWidth: 1,
          borderBottomColor: '#f0f0f0',
          marginBottom: 20,
        }}
      >
        <Col style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View>
            <Text style={styles.textHeader}>
              You will not be able to change your details
            </Text>
          </View>
          <View>
            <Text style={styles.textHeader}>
              below once you proceed to payment
            </Text>
          </View>
        </Col>
      </Grid>

      <Grid>
        <Col>
          <View>
            <Text style={styles.textLabel} {...props}>
              {props.labelPhoneNumber}
            </Text>
          </View>
          <View>
            <Text style={styles.textBody} {...props}>
              {props.phoneNumber}
            </Text>
          </View>
        </Col>
      </Grid>

      <Grid>
        <Col>
          <View>
            <Text style={styles.textLabel} {...props}>
              {props.labelProvider}
            </Text>
          </View>
          <View>
            <Text style={styles.textBody} {...props}>
              {props.provider}
            </Text>
          </View>
        </Col>
      </Grid>

      <Grid>
        <Col>
          <View>
            <Text style={styles.textLabel} {...props}>
              {props.labelPaketData}
            </Text>
          </View>
          <View>
            <Text style={styles.textBody} {...props}>
              {props.paketData}
            </Text>
          </View>
        </Col>
      </Grid>

      {props.labelPeriod && props.activePeriod ? (
        <Grid>
          <Col>
            <View>
              <Text style={styles.textLabel}>{props.labelPeriod}</Text>
            </View>
            <View>
              <Text style={styles.textBody}>{props.activePeriod}</Text>
            </View>
          </Col>
        </Grid>
      ) : (
        <View></View>
      )}

      <Grid>
        <Col style={{ backgroundColor: 'center', alignItems: 'center' }}>
          <View>
            <Text style={styles.textLabel}>{props.labelTotal}</Text>
          </View>
          <View>
            <Text style={styles.textBody}>{props.total}</Text>
          </View>
        </Col>
      </Grid>
    </View>
  </View>
);

export default BookingSummary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: -50,
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: deviceHeight / 1.4,
  },
  sectionHr: {
    width: 100 + '%',
    height: 1,
    alignSelf: 'center',
    backgroundColor: '#f0f0f0',
  },
  textHeader: {
    fontFamily: fontReguler,
    fontSize: 16,
    color: '#222222',
    alignSelf: 'center',
  },
  textLabel: {
    fontFamily: fontReguler,
    fontSize: 16,
    color: '#828282',
    alignSelf: 'center',
  },
  textBody: {
    paddingTop: 5,
    fontFamily: fontExtraBold,
    fontSize: 18,
    color: '#222222',
    alignSelf: 'center',
  },
});
