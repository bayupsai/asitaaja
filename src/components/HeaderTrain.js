import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import moment from 'moment';
import {
  fontBold,
  fontExtraBold,
  fontReguler,
  thameColors,
} from '../base/constant/index';
import OfflineNotice from './OfflineNotice';
import { statusHeight } from '../elements/BarStyle';

const BackButton = props => {
  return (
    <TouchableOpacity
      onPress={() => props.goBack()}
      style={{ paddingRight: 20 }}
    >
      <Icon
        name="ios-arrow-round-back"
        color={thameColors.white}
        size={42}
        type="ionicon"
      />
    </TouchableOpacity>
  );
};

const Menu = props => {
  return (
    <TouchableOpacity>
      <Icon
        name="dots-three-vertical"
        color={thameColors.white}
        type="entypo"
      />
    </TouchableOpacity>
  );
};

const TitlePage = props => {
  return (
    <Grid
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: -20,
      }}
    >
      <Col
        size={5}
        style={{ alignItems: 'flex-end', justifyContent: 'center' }}
      >
        <Text
          style={{
            color: thameColors.white,
            fontSize: 18,
            fontFamily: fontBold,
            paddingRight: 5,
          }}
        >
          {props.origin}
        </Text>
      </Col>
      <Col size={1} style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Image
          resizeMode="center"
          style={{ width: 35, height: 25 }}
          tintColor="#ffc850"
          source={require('../assets/icons/icon_trains.png')}
        />
      </Col>
      <Col
        size={4}
        style={{ alignItems: 'flex-start', justifyContent: 'center' }}
      >
        <Text
          style={{
            color: thameColors.white,
            fontSize: 18,
            fontFamily: fontBold,
            paddingLeft: 5,
          }}
        >
          {props.destination}
        </Text>
      </Col>
    </Grid>
  );
};

const HeaderTrain = props => (
  <View>
    <OfflineNotice />
    <Header
      placement="left"
      leftComponent={<BackButton goBack={props.callback} />}
      centerComponent={
        <TitlePage origin={props.origin} destination={props.destination} />
      }
      rightComponent={<Menu />}
      containerStyle={{
        backgroundColor: thameColors.darkBlue,
        paddingTop: 0,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        height: 30 + statusHeight,
      }}
    />
  </View>
);

export default HeaderTrain;
