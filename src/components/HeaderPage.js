import React from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { thameColors, asitaColor } from '../base/constant';
import OfflineNotice from './OfflineNotice';
import { BarStyle, statusHeight } from '../elements/BarStyle';

var deviceWidth = Dimensions.get('window').width;

const BackButton = props => {
  return (
    <TouchableOpacity onPress={() => props.goBack()} style={{ marginLeft: 10 }}>
      <Icon
        name="ios-arrow-round-back"
        color={asitaColor.white}
        size={42}
        type="ionicon"
      />
    </TouchableOpacity>
  );
};

const TitlePage = props => {
  return (
    <View
      style={{
        width: deviceWidth / 1.5,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Text
        style={{
          color: asitaColor.white,
          fontSize: 20,
          fontFamily: 'NunitoSans-Bold',
          letterSpacing: 0.5,
        }}
      >
        {props.title}
      </Text>
    </View>
  );
};

const HeaderPage = props => (
  <View>
    <OfflineNotice />
    <BarStyle />
    <Header
      placement="left"
      leftComponent={
        props.left === true ? (
          props.leftComponent
        ) : (
          <BackButton goBack={props.callback} />
        )
      }
      centerComponent={<TitlePage title={props.title} />}
      rightComponent={
        props.right === true ? (
          props.rightComponent
        ) : (
          <Icon
            type="feather"
            name="plus"
            color={asitaColor.marineBlue}
            iconStyle={{ marginRight: 10 }}
          />
        )
      }
      containerStyle={[
        {
          backgroundColor: asitaColor.marineBlue,
          paddingTop: 0,
          borderBottomWidth: 0,
          borderBottomColor: 'transparent',
          height: 30 + statusHeight,
        },
      ]}
    />
  </View>
);

export default HeaderPage;
