import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, WebView, Linking } from 'react-native';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';
import { moderateScale } from '../Const/ScaleUtils';
const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

class Payment extends Component {
  constructor() {
    super();
    this.state = {
      url: '',
    };
  }

  render() {
    const { item } = this.props;
    console.log('item : ', item.url);

    return (
      <View style={styles.container}>
        {
          <View style={{ width: deviceWidth - 20 }}>
            <WebView
              javaScriptEnabled={true}
              bounces={false}
              source={{ uri: item.url }}
              scalesPageToFit={true}
              style={{
                flex: 1,
                height: 450,
                width: deviceWidth - 30,
                backgroundColor: 'rgba(255,255,255,0.8)',
              }}
            />
          </View>
        }
      </View>
    );
  }
}

export default connect(({ main, alert }) => ({ main, alert }))(Payment);

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    alignItems: 'flex-start',
    borderRadius: 7,
    // minHeight:deviceWidth/4,
    marginHorizontal: moderateScale(10),
    marginVertical: moderateScale(2.5),
  },
  htmlStyle: {
    // paddingVertical         : moderateScale(8),
    marginLeft: moderateScale(25),
    marginVertical: moderateScale(2.5),
    borderBottomLeftRadius: moderateScale(0),
    borderBottomRightRadius: moderateScale(10),
    alignSelf: 'flex-start',
    borderTopLeftRadius: moderateScale(10),
    borderTopRightRadius: moderateScale(10),
    backgroundColor: 'rgba(255,255,255,0.8)',
    width: deviceWidth - 40,
  },
});
