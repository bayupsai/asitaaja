import React, { PureComponent } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import API from '../Const/APIEndPoints';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
const MARGIN_BUBBLE = 10;
const WIDTH = deviceWidth - MARGIN_BUBBLE * 2;

export default class BubbleShowImage extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { item } = this.props;
    const { data } = item;
    let urlCheck = data.indexOf('https://');
    const baseUrl = urlCheck === -1 ? API.assets : '';
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={this.goZoom} style={{ height: 300 }}>
          <Image
            source={{ uri: baseUrl + data }}
            style={styles.image}
            resizeMode="stretch"
          />
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
  },
  image: {
    flex: 1,
    height: undefined,
    width: 300,
    paddingHorizontal: 8,
    marginHorizontal: 10,
    paddingVertical: 8,
    marginVertical: 2.5,
  },
});
