import React, { PureComponent } from 'react';
import {
  Image,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Dimensions,
  Linking,
  TouchableHighlight,
  ImageBackground,
} from 'react-native';
import { connect } from 'react-redux';
import { Button } from 'react-native-elements';
import { dispatch } from '../redux/store';
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';
import moment from 'moment';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

const { width } = Dimensions.get('window');

export default class BubbleFlightSchedule extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { item } = this.props;
    const { data } = item;
    // console.log('data schedule flight : ', item.columns)
    return (
      <View>
        <ScrollView horizontal={true} bounces={false}>
          {item.columns.map((element, i) => (
            <FlightSchedule item={element} key={i} />
          ))}
        </ScrollView>
      </View>
    );
  }
}

class FlightSchedule extends PureComponent {
  constructor() {
    super();
  }
  send = (data, city, time) => {
    dispatch.alert.show({
      message: `Book tiket pesawat ke ${city} keberangkatan pukul ${time}`,
      title: 'Apakah Anda yakin?',
      image: 'warning',
      color: 'garuda',
      btncolor: 'garuda',
      buttons: [
        {
          title: 'TIDAK',
          type: 'reject',
          callback: () => dispatch.alert.hide(),
        },
        {
          title: 'YA',
          type: 'resolve',
          callback: () => this.sendData(data, city, time),
        },
      ],
    });
  };

  sendData = (data, city, time) => {
    let text = `Ok pesankan tiket ke ${city} jam ${time}`;
    dispatch.main.sendNoGetChat(text);
    dispatch.main.getChat(data);
  };

  render() {
    const { item } = this.props;

    let itemDepartDate = item.departDate;
    let getDate = itemDepartDate.split(' ');
    let splitDate = getDate[0].split('-');

    let changeFormat = splitDate[2] + '-' + splitDate[1];
    let departDate = moment(changeFormat, 'DD-MM').format('DD MMM');

    return (
      <View style={styles.carousel}>
        <View style={styles.imgContainer}>
          <Image source={{ uri: item.airlineImageUrl }} style={styles.image} />
        </View>
        <View style={styles.containerBody}>
          <View style={styles.dateTime}>
            <View>
              <Text style={styles.departTime}>{item.departTime}</Text>
              <Text style={styles.date}>{departDate}</Text>
            </View>
            <View>
              <Text style={styles.arrivalTime}>{item.arriveTime}</Text>
              <Text style={styles.date}>{departDate}</Text>
            </View>
          </View>

          <View style={{ width: moderateScale(30) }}>
            <Image
              source={require('../assets/chat/assets/img_destinations_list.png')}
              style={styles.imageLine}
            />
          </View>

          <View>
            <View>
              <Text style={styles.airportLoc}>{item.orgCity}</Text>
              <Text style={styles.flightType}>
                {item.flightType} :{' '}
                {item.details[0].destAirport +
                  ' ' +
                  `(${item.details[0].destCode})`}{' '}
              </Text>
              <Text style={styles.airportLoc}>{item.destCity}</Text>
              {/* <Text style={styles.airportLoc}>{item.destAirport + ' ' + `(${item.details[0].destCode})`}</Text> */}
            </View>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title={item.actions.label}
            titleStyle={{ fontWeight: '700' }}
            onPress={this.send.bind(
              this,
              item.actions.data,
              item.destCity,
              item.departTime
            )}
            containerViewStyle={{ marginLeft: 4, marginRight: 4 }}
            buttonStyle={[styles.button]}
            fontSize={moderateScale(14)}
            color="#fff"
            fontWeight="bold"
          />
        </View>
      </View>
    );
  }
}

connect(({ main }) => ({ main }))(FlightSchedule);

const styles = StyleSheet.create({
  imageLine: {
    height: deviceHeight / 9,
    width: deviceWidth / 10,
    resizeMode: 'contain',
  },
  image: {
    height: deviceHeight / 5,
    width: deviceWidth / 3,
    resizeMode: 'contain',
  },
  imgContainer: {
    backgroundColor: '#F6F7F8',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerBody: {
    flexDirection: 'row',
    backgroundColor: '#F6F7F8',
    height: moderateScale(100),
    alignItems: 'center',
    justifyContent: 'center',
  },
  carousel: {
    flex: 1,
    width: moderateScale(width * 0.65),
    paddingVertical: moderateScale(8),
    marginVertical: moderateScale(2.5),
    marginHorizontal: moderateScale(5),
    shadowRadius: moderateScale(3),
    shadowOpacity: moderateScale(0.1),
    shadowOffset: { width: moderateScale(2), height: moderateScale(6) },
  },
  buttonContainer: {
    height: moderateScale(50),
    backgroundColor: '#F6F7F8',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: moderateScale(200),
    height: moderateScale(40),
    backgroundColor: '#ec650a',
    borderTopWidth: 1,
    borderColor: '#dcdbdb',
    borderRadius: 20,
    marginTop: moderateScale(3),
  },
  departTime: {
    fontSize: moderateScale(12),
    fontWeight: '700',
    marginBottom: moderateScale(5),
  },
  arrivalTime: {
    fontSize: moderateScale(12),
    fontWeight: '700',
    marginBottom: moderateScale(5),
  },
  flightType: {
    fontSize: moderateScale(12),
    color: '#4A4A4A',
    marginVertical: moderateScale(10),
  },
  airportLoc: {
    fontSize: moderateScale(12),
    fontWeight: '700',
    color: '#165B9C',
  },
  date: {
    fontSize: moderateScale(10),
    marginBottom: moderateScale(5),
  },
});
