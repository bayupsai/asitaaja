import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ActionSheetIOS,
  Picker,
  Platform,
} from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import FormInput from '../common/FormInputDefault';
import t from 'tcomb-form-native';
import { formInput } from '../Const/Styles';
import Colors from '../Const/Colors';
import DatePicker from 'react-native-datepicker';
import { moderateScale } from '../Const/ScaleUtils';
import moment from 'moment';

let fieldsNameAdult = {};
let fieldsNameChild = {};
let fieldsNameInfant = {};

// let fieldsName = {}

const { width: deviceWidth } = Dimensions.get('window');

class FormInputView extends Component {
  constructor() {
    super();
    this.state = {
      hide: false,
      arr: [],
      title: 'Mr',
      dateChild: [],
      dateInfant: [],
      con: '',
      titleAdult: [],
      titleChild: [],
      titleInfant: [],
      test: [],
      titleAdultAndroid: [],
      titleChildAndroid: [],
      titleInfantAndroid: [],
    };
  }

  componentWillMount() {
    this.pickerDataAdult();
    this.pickerDataChild();
    this.pickerDataInfant();
    this.datePickerDataChild();
    this.datePickerDataInfant();
  }

  pickerDataAdult = () => {
    const { item: data } = this.props;
    let passengerAdult = data.columns.adultPassenger;

    let pickerData = [];
    for (let i = 0; i < passengerAdult.length; i++) {
      pickerData.push('MR');
    }
    this.setState({
      titleAdult: pickerData,
      titleAdultAndroid: pickerData,
    });
  };

  pickerDataChild = () => {
    const { item: data } = this.props;
    let passengerChild = data.columns.childPassenger;

    let pickerData = [];
    for (let i = 0; i < passengerChild.length; i++) {
      pickerData.push('MSTR');
    }
    this.setState({
      titleChild: pickerData,
      titleChildAndroid: pickerData,
    });
  };

  pickerDataInfant = () => {
    const { item: data } = this.props;
    let passengerInfant = data.columns.infantPassenger;

    let pickerData = [];
    for (let i = 0; i < passengerInfant.length; i++) {
      pickerData.push('MSTR');
    }
    this.setState({
      titleInfant: pickerData,
      titleInfantAndroid: pickerData,
    });
  };

  datePickerDataChild = () => {
    const { item: data } = this.props;
    let passengerChild = data.columns.childPassenger;
    let pickerDateData = [];
    for (let i = 0; i < passengerChild.length; i++) {
      pickerDateData.push('01-08-2009');
    }
    this.setState({
      dateChild: pickerDateData,
    });
  };

  datePickerDataInfant = () => {
    const { item: data } = this.props;
    let passengerInfant = data.columns.infantPassenger;

    let pickerDateData = [];
    for (let i = 0; i < passengerInfant.length; i++) {
      pickerDateData.push('01-08-2017');
    }
    this.setState({
      dateInfant: pickerDateData,
    });
  };

  send = () => {
    const { item: data } = this.props;

    let passengerAdult = data.columns.adultPassenger;
    let passengerChild = data.columns.childPassenger;
    let passengerInfant = data.columns.infantPassenger;
    let allPassenger =
      passengerAdult.length + passengerChild.length + passengerInfant.length;
    //YYYYMMDD

    let arrFields = [];
    let arrFieldsChild = [];
    let arrFieldsinfant = [];

    let arrPassenger = {
      adult: arrFields,
      child: arrFieldsChild,
      infant: arrFieldsinfant,
      scheduleDetail: data.data.scheduleDetail,
    };

    let lengthNameAdult =
      passengerAdult.length > 0
        ? Object.keys(fieldsNameAdult).length
        : passengerAdult.length;
    let lengthNameChild =
      passengerChild.length > 0
        ? Object.keys(fieldsNameChild).length
        : passengerChild.length;
    let lengthNameInfant =
      passengerInfant.length > 0
        ? Object.keys(fieldsNameInfant).length
        : passengerInfant.length;

    if (
      lengthNameAdult == passengerAdult.length &&
      lengthNameChild == passengerChild.length &&
      lengthNameInfant == passengerInfant.length
    ) {
      if (passengerAdult.length > 0) {
        for (let i = 0; i < passengerAdult.length; i++) {
          arrFields.push({
            title:
              Platform.OS == 'android'
                ? this.state.titleAdultAndroid[i].toLowerCase()
                : this.state.titleAdult[i].toLowerCase(),
            name: fieldsNameAdult[i],
            email: 'aeroaja@aeroaja.co.id',
            phone: '0856757522',
            birth_date: '19900817',
            nationality: 'ID',
            passport_no: '',
            passport_country: '',
            passport_issue_date: '',
            passport_expire: '',
          });
        }
      } //end of passengeradult

      if (passengerChild.length > 0) {
        for (let i = 0; i < passengerChild.length; i++) {
          let dateFormat = moment(this.state.dateChild[i], 'DD-MM-YYYY').format(
            'YYYYMMDD'
          );

          arrFieldsChild.push({
            title:
              Platform.OS == 'android'
                ? this.state.titleChildAndroid[i].toLowerCase()
                : this.state.titleChild[i].toLowerCase(),
            name: fieldsNameChild[i],
            email: 'aeroaja@aeroaja.co.id',
            phone: '0856757522',
            birth_date: dateFormat,
            nationality: 'ID',
            passport_no: '',
            passport_country: '',
            passport_issue_date: '',
            passport_expire: '',
          });
        }
      }

      if (passengerInfant.length > 0) {
        for (let i = 0; i < passengerInfant.length; i++) {
          arrFieldsinfant.push({
            title:
              Platform.OS == 'android'
                ? this.state.titleInfantAndroid[i].toLowerCase()
                : this.state.titleInfant[i].toLowerCase(),
            name: fieldsNameInfant[i],
            email: 'aeroaja@aeroaja.co.id',
            phone: '08567522322',
            birth_date: this.state.dateInfant[i],
            nationality: 'ID',
            passport_no: '',
            passport_country: '',
            passport_issue_date: '',
            passport_expire: '',
          });
        }
      }

      let paramStringfy = JSON.stringify(arrPassenger);

      let text = `Pesankan saya tiket pesawat untuk ${allPassenger} orang`;

      dispatch.alert.show({
        image: 'warning',
        color: '#0090d2',
        btncolor: '#0090d2',
        title: 'Apakah Anda yakin?',
        message: 'Data yang Anda isi sudah benar ?',
        buttons: [
          {
            title: 'BATAL',
            type: 'resolve',
            callback: () => dispatch.alert.hide(),
          },
          {
            title: 'YA',
            type: 'resolve',
            callback: () => this.sendPassenger(paramStringfy, text),
          },
        ],
      });

      lengthNameAdult = passengerAdult.length;
      lengthNameChild = passengerChild.length;
      lengthNameInfant = passengerInfant.length;
    } else {
      dispatch.alert.show({
        image: 'warning',
        color: 'warning',
        btncolor: 'warning',
        title: 'Perhatian !',
        message: 'Harap isi seluruh data',
        buttons: [
          {
            title: 'OK',
            type: 'resolve',
            callback: () => dispatch.alert.hide(),
          },
        ],
      });
    }
  };

  sendPassenger = (data, text) => {
    dispatch.main.sendNoGetChat(text);
    dispatch.main.getChat(data);

    this.setState({
      hide: true,
    });
  };
  cancel = () => {
    dispatch.main.send('Batal');
    this.setState({
      hide: true,
    });
  };

  actionAdult = id => {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ['Cancel', 'MR', 'MRS', 'MS'],
        cancelButtonIndex: 0,
        MR: 1,
        MRS: 2,
        MS: 3,
      },
      buttonIndex => {
        if (buttonIndex === 1) {
          var array = [...this.state.titleAdult]; // make a separate copy of the array
          array.splice(id, 1, 'MR');
          this.setState({ titleAdult: array });
        } else if (buttonIndex === 2) {
          var array = [...this.state.titleAdult]; // make a separate copy of the array

          array.splice(id, 1, 'MRS');

          this.setState({ titleAdult: array });
        } else if (buttonIndex === 3) {
          var array = [...this.state.titleAdult]; // make a separate copy of the array

          array.splice(id, 1, 'MS');

          this.setState({ titleAdult: array });
        }
      }
    );
  };

  actionChild = id => {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ['Cancel', 'MSTR', 'MISS'],
        cancelButtonIndex: 0,
        MSTR: 1,
        MISS: 2,
      },
      buttonIndex => {
        if (buttonIndex === 1) {
          var array = [...this.state.titleChild]; // make a separate copy of the array
          array.splice(id, 1, 'MSTR');
          this.setState({ titleChild: array });
        } else if (buttonIndex === 2) {
          var array = [...this.state.titleChild]; // make a separate copy of the array
          array.splice(id, 1, 'MISS');
          this.setState({ titleChild: array });
        }
      }
    );
  };

  actionInfant = id => {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ['Cancel', 'MSTR', 'MISS'],
        cancelButtonIndex: 0,
        MSTR: 1,
        MISS: 2,
      },
      buttonIndex => {
        if (buttonIndex === 1) {
          var array = [...this.state.titleInfant]; // make a separate copy of the array
          array.splice(id, 1, 'MSTR');
          this.setState({ titleInfant: array });
        } else if (buttonIndex === 2) {
          var array = [...this.state.titleInfant]; // make a separate copy of the array
          array.splice(id, 1, 'MISS');
          this.setState({ titleInfant: array });
        }
      }
    );
  };

  changeDateChild = (date, id) => {
    var array = [...this.state.dateChild];

    array.splice(id, 1, date);

    this.setState({ dateChild: array });
  };

  changeDateInfant = (date, id) => {
    var array = [...this.state.dateInfant];

    array.splice(id, 1, date);

    this.setState({ dateInfant: array });
  };

  changeTitlePickerAdult = (title, id) => {
    var array = [...this.state.titleAdultAndroid];

    array.splice(id, 1, title);

    this.setState({ titleAdultAndroid: array });
  };

  changeTitlePickerChild = (title, id) => {
    var array = [...this.state.titleChildAndroid];

    array.splice(id, 1, title);

    this.setState({ titleChildAndroid: array });
  };

  changeTitlePickerInfant = (title, id) => {
    var array = [...this.state.titleInfantAndroid];

    array.splice(id, 1, title);

    this.setState({ titleInfantAndroid: array });
  };

  render() {
    const { item: data } = this.props;

    let passengerAdult = data.columns.adultPassenger;
    let passengerChild = data.columns.childPassenger;
    let passengerInfant = data.columns.infantPassenger;

    return (
      <View>
        {this.state.hide == false ? (
          <View style={styles.container}>
            <View style={styles.child}>
              <ScrollView>
                <Text style={styles.headerText}>
                  Contact Details (for E-ticket)
                </Text>
                <View>
                  <Text style={styles.title}>Full Name</Text>
                  <TextInput
                    style={styles.fieldContanct}
                    editable={false}
                    value={data.columns.contact[0].value}
                  />
                  <Text style={styles.title}>Email</Text>
                  <TextInput
                    style={styles.fieldContanct}
                    editable={false}
                    value={data.columns.contact[1].value}
                  />
                  <Text style={styles.title}>Phone Number</Text>
                  <TextInput
                    style={styles.fieldContanct}
                    editable={false}
                    value={data.columns.contact[2].value}
                  />
                </View>

                <View
                  style={{
                    borderBottomColor: '#3C3C3C',
                    borderWidth: 0.5,
                    width: deviceWidth,
                    marginBottom: moderateScale(12),
                    marginTop: moderateScale(10),
                  }}
                />

                <Text style={styles.headerText}>Passenger Details</Text>
                {/* ADULT */}
                {data.columns.adultPassenger.map(passenger =>
                  passengerAdult.length > 0 ? (
                    <View>
                      <Text style={styles.title}>{`Adult #${passenger.id +
                        1}`}</Text>

                      <View
                        style={{ flexDirection: 'row', width: deviceWidth }}
                      >
                        {/* Field Container adult*/}
                        {/* Title container */}
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.title}>Title</Text>
                          {Platform.OS == 'android' ? (
                            <Picker
                              style={styles.fieldTitleAndroid}
                              selectedValue={
                                this.state.titleAdultAndroid[passenger.id]
                              }
                              onValueChange={title =>
                                this.changeTitlePickerAdult(title, passenger.id)
                              }
                            >
                              <Picker.Item label={'MR'} value={'MR'} />
                              <Picker.Item label={'MRS'} value={'MRS'} />
                              <Picker.Item label={'MS'} value={'MS'} />
                            </Picker>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.actionAdult(passenger.id)}
                            >
                              <View style={styles.fieldTitle}>
                                <Text style={styles.textPickerIOS}>
                                  {this.state.titleAdult[passenger.id]}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        </View>

                        {/* Title container */}

                        {/* Name Container */}
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.title}>{`Full Name`}</Text>
                          <TextInput
                            style={styles.fieldLong}
                            placeholder={'Nama Penumpang'}
                            key={passenger.id}
                            onChangeText={text =>
                              (fieldsNameAdult[passenger.id] = text)
                            }
                          />
                        </View>
                        {/* Name Container */}

                        {/* Field Container adult*/}
                      </View>
                    </View>
                  ) : (
                    <View></View>
                  )
                )}
                {/* ADULT */}

                {/* CHILD */}
                {data.columns.childPassenger.map(passenger =>
                  passengerChild.length > 0 ? (
                    <View>
                      <Text style={styles.title}>{`Child #${passenger.id +
                        1}`}</Text>
                      <View
                        style={{ flexDirection: 'row', width: deviceWidth }}
                      >
                        {/* Field Container child*/}
                        {/* Title container */}
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.title}>Title</Text>
                          {Platform.OS == 'android' ? (
                            <Picker
                              style={styles.fieldTitleAndroid}
                              selectedValue={
                                this.state.titleChildAndroid[passenger.id]
                              }
                              onValueChange={title =>
                                this.changeTitlePickerChild(title, passenger.id)
                              }
                            >
                              <Picker.Item label={'MSTR'} value={'MSTR'} />
                              <Picker.Item label={'MISS'} value={'MISS'} />
                            </Picker>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.actionChild(passenger.id)}
                            >
                              <View style={styles.fieldTitle}>
                                <Text style={styles.textPickerIOS}>
                                  {this.state.titleChild[passenger.id]}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        </View>
                        {/* Title container */}

                        {/* Name Container */}
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.title}>{`Full Name`}</Text>
                          <TextInput
                            style={styles.fieldLong}
                            placeholder={'Nama Penumpang'}
                            key={passenger.id}
                            onChangeText={text =>
                              (fieldsNameChild[passenger.id] = text)
                            }
                          />

                          <Text style={styles.title}>{`Tanggal Lahir`}</Text>
                          <DatePicker
                            style={styles.fieldDate}
                            customStyles={{
                              dateText: {
                                color: '#000000',
                                fontSize: moderateScale(14),
                              },
                            }}
                            date={this.state.dateChild[passenger.id]}
                            mode="date"
                            placeholder="Tanggal lahir"
                            format="DD-MM-YYYY"
                            minDate="01-01-2007"
                            maxDate="01-01-2017"
                            showIcon={false}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            onDateChange={date =>
                              this.changeDateChild(date, passenger.id)
                            }
                          />
                        </View>
                        {/* Name Container */}

                        {/* Field Container child*/}
                      </View>
                    </View>
                  ) : (
                    <View></View>
                  )
                )}
                {/* CHILD */}

                {/* INFANT */}
                {data.columns.infantPassenger.map(passenger =>
                  passengerInfant.length > 0 ? (
                    <View>
                      <Text style={styles.title}>{`Infant #${passenger.id +
                        1}`}</Text>
                      <View
                        style={{ flexDirection: 'row', width: deviceWidth }}
                      >
                        {/* Field Container child*/}
                        {/* Title container */}
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.title}>Title</Text>
                          {Platform.OS == 'android' ? (
                            <Picker
                              style={styles.fieldTitleAndroid}
                              selectedValue={
                                this.state.titleInfantAndroid[passenger.id]
                              }
                              onValueChange={title =>
                                this.changeTitlePickerInfant(
                                  title,
                                  passenger.id
                                )
                              }
                            >
                              <Picker.Item label={'MSTR'} value={'MSTR'} />
                              <Picker.Item label={'MISS'} value={'MISS'} />
                            </Picker>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.actionInfant(passenger.id)}
                            >
                              <View style={styles.fieldTitle}>
                                <Text style={styles.textPickerIOS}>
                                  {this.state.titleInfant[passenger.id]}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          )}
                        </View>
                        {/* Title container */}

                        {/* Name Container */}
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={styles.title}>{`Full Name`}</Text>
                          <TextInput
                            style={styles.fieldLong}
                            placeholder={'Nama Penumpang'}
                            key={passenger.id}
                            onChangeText={text =>
                              (fieldsNameInfant[passenger.id] = text)
                            }
                          />

                          <Text style={styles.title}>{`Tanggal Lahir`}</Text>
                          <DatePicker
                            style={styles.fieldDate}
                            customStyles={{
                              dateText: {
                                color: '#000000',
                                fontSize: moderateScale(14),
                              },
                            }}
                            date={this.state.dateInfant[passenger.id]}
                            mode="date"
                            placeholder="Tanggal lahir"
                            format="DD-MM-YYYY"
                            minDate="01-01-2017"
                            maxDate="01-01-2019"
                            showIcon={false}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            onDateChange={date =>
                              this.changeDateInfant(date, passenger.id)
                            }
                          />
                        </View>
                        {/* Name Container */}

                        {/* Field Container child*/}
                      </View>
                    </View>
                  ) : (
                    <View></View>
                  )
                )}
                {/* INFANT */}

                <View
                  style={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                >
                  <Button
                    title="BATAL"
                    titleStyle={{ color: '#0090d2' }}
                    buttonStyle={styles.buttonCancel}
                    onPress={this.cancel}
                  />
                  <Button
                    title="YA"
                    titleStyle={{ color: '#FFFFFF' }}
                    buttonStyle={styles.button}
                    onPress={this.send}
                  />
                </View>
              </ScrollView>
            </View>
          </View>
        ) : (
          <View></View>
        )}
      </View>
    );
  }
}

export default connect(({ main, alert }) => ({ main, alert }))(FormInputView);

const styles = StyleSheet.create({
  container: {
    maxWidth: deviceWidth - 20,
    width: deviceWidth - 20,
    marginHorizontal: 10,
    marginVertical: 2.5,
    borderRadius: 7,
    backgroundColor: '#fff',
  },
  formContainer: {
    flexDirection: 'row',
  },
  child: {
    width: deviceWidth - 20,
    padding: 15,
  },
  title: {
    alignSelf: 'center',
    fontSize: 14,
    color: Colors.dark,
    marginBottom: 10,
  },
  button: {
    height: moderateScale(45),
    borderRadius: 4,
    backgroundColor: '#0090d2',
    marginLeft: 10,
    width: moderateScale(60),
  },
  buttonCancel: {
    height: moderateScale(45),
    borderRadius: 4,
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: '#0090d2',
    width: moderateScale(70),
  },
  fieldTitle: {
    width: deviceWidth / 5.5,
    height: moderateScale(40),
    marginRight: moderateScale(5),
    paddingTop: moderateScale(10),
    paddingLeft: moderateScale(10),
    backgroundColor: '#F8F6F9',
  },
  fieldTitleAndroid: {
    width: moderateScale(100),
    height: moderateScale(40),
    marginRight: moderateScale(5),
    paddingTop: moderateScale(10),
    paddingLeft: moderateScale(10),
    backgroundColor: '#F8F6F9',
  },
  fieldLong: {
    width: deviceWidth / 1.6,
    height: moderateScale(40),
    marginBottom: moderateScale(10),
    backgroundColor: '#F8F6F9',
    paddingLeft: moderateScale(10),
  },
  fieldDate: {
    width: deviceWidth / 1.6,
    height: moderateScale(40),
    marginBottom: moderateScale(10),
    backgroundColor: '#F8F6F9',
  },
  title: {
    fontSize: moderateScale(14),
    color: '#3C3C3C',
    marginBottom: moderateScale(5),
  },
  fieldContanct: {
    width: deviceWidth / 1.2,
    height: moderateScale(40),
    marginBottom: moderateScale(10),
    backgroundColor: '#F8F6F9',
    paddingLeft: moderateScale(10),
  },
  headerText: {
    fontSize: moderateScale(21),
    backgroundColor: 'transparent',
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10),
    color: '#0090d2',
    fontWeight: '700',
  },
});
