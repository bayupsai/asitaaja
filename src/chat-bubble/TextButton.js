import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import Colors from '../Const/Colors';
import { Button } from 'react-native-elements';

const { width: deviceWidth } = Dimensions.get('window');

class TextButton extends Component {
  constructor() {
    super();
  }
  select = item => {
    dispatch.main.send(item.text);
  };
  render() {
    return (
      <View style={styles.lenna}>
        <Text style={[styles.lennaText, this.props.me && styles.meText]}>
          {this.props.item.text}
        </Text>
        {this.props.item.buttons.map((item, i) => (
          <Button
            key={i}
            title={item.text}
            titleStyle={{ color: '#0C56C1' }}
            buttonStyle={styles.button}
            containerViewStyle={styles.buttonContainer}
            onPress={this.select.bind(this, item)}
          />
        ))}
      </View>
    );
  }
}
export default connect(({ main }) => ({ main }))(TextButton);

const styles = StyleSheet.create({
  lenna: {
    width: deviceWidth - 50,
    marginHorizontal: 10,
    marginVertical: 2.5,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    alignSelf: 'flex-start',
    borderTopLeftRadius: 0,
    borderTopRightRadius: 10,
    backgroundColor: '#FFFFFF',
    overflow: 'hidden',
  },
  lennaText: {
    fontSize: 13,
    color: Colors.dark,
    margin: 10,
  },
  button: {
    backgroundColor: '#fff',
    width: deviceWidth - 50,
  },
  buttonContainer: {
    marginRight: 0,
    marginLeft: 0,
  },
});
