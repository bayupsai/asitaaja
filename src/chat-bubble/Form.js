import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Dimensions } from 'react-native';
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

export default class Form extends Component {
  constructor() {
    super();
  }

  render() {
    const { item } = this.props;
    console.log('item : ', item);

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={{ uri: item.imageUrl }} style={styles.image} />
        </View>
        <View style={styles.table}>
          <View>
            <Text style={styles.detailSummary}>Detail Summary </Text>
          </View>
          {item.columns.map((item, i) => (
            <Row data={item} key={i} />
          ))}
        </View>
      </View>
    );
  }
}

class Row extends Component {
  constructor() {
    super();
  }
  render() {
    const { data } = this.props;

    return (
      <View style={styles.row}>
        <Text style={styles.field}>{data.field}</Text>
        <Text>: </Text>
        <View style={{ width: moderateScale(200) }}>
          <Text style={styles.value}>{data.value}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    maxWidth: deviceWidth - 20,
    width: deviceWidth - 20,
    marginHorizontal: 10,
    marginVertical: 2.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: moderateScale(150),
    height: moderateScale(100),
    resizeMode: 'contain',
  },
  header: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    width: deviceWidth - 20,
    height: deviceHeight / 5,
    backgroundColor: '#fff',
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7,
  },
  title: {
    color: '#333',
    fontSize: 13,
    fontWeight: 'bold',
  },
  table: {
    width: deviceWidth - 20,
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
    paddingHorizontal: 13,
    borderBottomRightRadius: 7,
    borderBottomLeftRadius: 7,
  },
  row: {
    flexDirection: 'row',
  },
  field: {
    minWidth: deviceWidth / 2.9,
    fontWeight: 'bold',
  },
  value: {
    width: moderateScale(180),
    // fontWeight: 'bold',
  },
  imageLine: {
    height: deviceHeight / 9,
    width: deviceWidth / 10,
    resizeMode: 'contain',
  },
  image: {
    height: deviceHeight / 5,
    width: deviceWidth / 3,
    resizeMode: 'contain',
  },
  containerBody: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    width: deviceWidth - 20,
    height: moderateScale(100),
  },
  departTime: {
    fontSize: moderateScale(16),
    fontWeight: '700',
    marginBottom: moderateScale(5),
  },
  arrivalTime: {
    fontSize: moderateScale(16),
    fontWeight: '700',
    marginBottom: moderateScale(5),
  },
  flightType: {
    fontSize: moderateScale(12),
    color: '#4A4A4A',
    marginVertical: moderateScale(10),
  },
  airportLoc: {
    fontSize: moderateScale(14),
    fontWeight: '700',
    color: '#165B9C',
  },
  date: {
    fontSize: moderateScale(10),
    marginBottom: moderateScale(5),
  },
  dateTime: {
    alignItems: 'center',
    width: moderateScale(150),
  },
  passengerContainer: {
    width: deviceWidth - 20,
    height: deviceHeight / 7.3,
    backgroundColor: '#fff',
  },
  passengerTitle: {
    fontSize: moderateScale(14),
    color: '#3C3C3C',
  },
  passengerName: {
    fontSize: moderateScale(16),
    fontWeight: '700',
    color: '#3C3C3C',
  },
  passengerImage: {
    width: moderateScale(40),
    height: moderateScale(20),
    resizeMode: 'contain',
  },
  containerLocation: {
    marginLeft: moderateScale(13),
  },
  detailSummary: {
    fontSize: moderateScale(14),
    fontWeight: '700',
    color: '#3C3C3C',
  },
});
