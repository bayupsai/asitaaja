import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import Colors from '../Const/Colors';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
const BUBBLE_WIDTH = deviceWidth - 20;
const BUBBLE_HEIGHT = deviceWidth / 4;

const COL_LEFT = BUBBLE_HEIGHT;
const COL_RIGHT = BUBBLE_HEIGHT + 10;
const COL_CENTER = BUBBLE_WIDTH - (COL_LEFT + COL_RIGHT);
export default class Flight extends Component {
  constructor() {
    super();
  }
  render() {
    const { data } = this.props.item;
    return (
      <ScrollView bounces={false} style={{ height: deviceHeight / 2.5 }}>
        {data.map((item, i) => (
          <FlightList item={item} key={i} />
        ))}
      </ScrollView>
    );
  }
}
class FlightList extends Component {
  constructor() {
    super();
  }
  send = value => {
    let text = `Ok pesankan ${value.airlinename} jam ${value.departtime}`;
    dispatch.alert.show({
      title: 'Apakah kamu yakin?',
      message: `Book tiket pesawat ${value.airlinename} keberangkatan ${value.departtime}`,
      buttons: [
        {
          title: 'Batal',
          type: 'reject',
          callback: () => console.log('cancel'),
        },
        {
          title: 'YA',
          type: 'resolve',
          callback: () => dispatch.main.send(text),
        },
      ],
    });
  };
  render() {
    const { item } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.flight}>
          <View style={styles.colLeft}>
            <Image
              source={{ uri: item.airlineimg }}
              style={styles.image}
              resizeMode="center"
            />
            <View style={{ alignItems: 'center', padding: 5 }}>
              <Text style={{ fontSize: 9, fontWeight: 'bold' }}>
                {item.airlinecode}
              </Text>
              <Text style={{ fontSize: 11 }}>{item.flighttype}</Text>
            </View>
          </View>

          <View style={styles.colCenter}>
            <View style={styles.subColCenter}>
              <Text style={{ fontSize: 13, fontWeight: 'bold' }}>
                {item.airportdepartcode}
              </Text>
              <Text style={styles.text}>{item.airportdepart}</Text>
              <Text style={styles.text}>{item.datetext}</Text>
              <Text style={styles.text}>{item.departtime}</Text>
            </View>

            <Image
              source={require('../assets/chat/assets/icon_pesawat.png')}
              style={styles.iconPlane}
              resizeMode="stretch"
              resizeMethod="resize"
            />

            <View style={styles.subColCenter}>
              <Text style={{ fontSize: 13, fontWeight: 'bold' }}>
                {item.airportarrivalcode}
              </Text>
              <Text style={styles.text}>{item.airportarrival}</Text>
              <Text style={styles.text}>{item.datetext}</Text>
              <Text style={styles.text}>{item.arrivaltime}</Text>
            </View>
          </View>

          <View style={styles.colRight}>
            <Text style={{ fontSize: 11, fontWeight: 'bold' }}>
              {item.priceconvert}
            </Text>
            <Button
              title="Book"
              fontSize={10}
              buttonStyle={styles.button}
              onPress={this.send.bind(this, item)}
            />
          </View>
        </View>
      </View>
    );
  }
}

connect(({ alert, main }) => ({ alert, main }))(FlightList);

const styles = StyleSheet.create({
  bubbleChat: {
    maxWidth: BUBBLE_WIDTH,
    paddingVertical: 10,
    paddingHorizontal: 10,
    margin: 15,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    alignSelf: 'flex-start',
    borderTopLeftRadius: 0,
    borderTopRightRadius: 7,
    backgroundColor: 'rgba(255,255,255,0.8)',
  },
  container: {
    overflow: 'hidden',
    alignItems: 'flex-start',
    borderRadius: 7,
    width: BUBBLE_WIDTH,
    // height:BUBBLE_HEIGHT,
    marginHorizontal: 10,
    marginVertical: 2.5,
    backgroundColor: 'rgba(255,255,255,0.8)',
  },
  flight: {
    flexDirection: 'row',
    maxWidth: deviceWidth - 20,
  },
  colLeft: {
    backgroundColor: '#fff',
    width: COL_LEFT,
    // height:BUBBLE_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  colCenter: {
    width: COL_CENTER,
    // height:BUBBLE_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subColCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    width: COL_CENTER / 2 - 15,
    // height:BUBBLE_HEIGHT
  },
  colRight: {
    width: COL_RIGHT,
    // height:BUBBLE_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftColor: 'rgba(0,0,0,0.2)',
    borderLeftWidth: 1,
  },
  image: {
    height: 50,
    width: deviceWidth / 4,
  },
  iconPlane: {
    margin: 5,
    height: 15,
    width: 20,
  },
  text: {
    fontSize: 12,
    flexWrap: 'wrap',
    textAlign: 'center',
  },
  button: {
    backgroundColor: Colors.danger,
    borderRadius: 3,
    paddingVertical: 3,
    paddingHorizontal: 15,
    marginTop: 10,
  },
});
