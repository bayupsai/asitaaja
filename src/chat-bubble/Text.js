import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  View,
} from 'react-native';
import HTML from 'react-native-render-html';
import Form from './Form';
import TextButton from './TextButton';
import FormInputView from './FormInputView';
import FlightSchedule from './FlightSchedule';
import Html from './Html';
import Payment from './Payment';
import ShowImage from './ShowImage';
import ImageSlider from './ShowImageSlider';
import { moderateScale } from '../Const/ScaleUtils';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
class BubbleText extends Component {
  constructor() {
    super();
  }

  render() {
    const { item, navigation, user } = this.props;

    switch (item.type) {
      case 'loading':
        return null;

      case 'chat':
        return <TextView {...this.props} />;

      case 'text':
        return <TextView {...this.props} />;

      case 'quickbutton':
        return <QuickButton {...this.props} />;

      case 'flight_table':
        return <FlightSchedule {...this.props} />;

      case 'html':
        return <Html {...this.props} />;

      case 'webview':
        return <Payment {...this.props} />;

      case 'flightTripDetailForm':
        return null;

      case 'flightScheduleCarousel':
        return <FlightSchedule {...this.props} />;

      case 'flightPassengerForm':
        return <FormInputView {...this.props} />;

      case 'summary':
        return <Form {...this.props} />;

      case 'text_button':
        return <TextButton {...this.props} />;

      case 'image':
        return <ShowImage {...this.props} />;

      case 'carousel':
        return <ImageSlider {...this.props} />;

      default:
        return <TextView {...this.props} />;
    }
  }
}

class TextView extends Component {
  constructor() {
    super();
    this.state = {
      textVoice: false,
    };
  }

  componentWillReceiveProps() {
    const { text } = this.props.item;
    if (text == '') {
      this.setState({
        textVoice: true,
      });
    } else {
      this.setState({
        textVoice: false,
      });
    }
  }

  render() {
    const { text, me, time, voice } = this.props.item;

    const avatar =
      me == undefined ? (
        <Image
          source={require('../assets/chat/assets/avatar_gadis.png')}
          style={styles.imgChat}
        />
      ) : (
        <View style={styles.transparent}>
          <Text></Text>
        </View>
      );
    let checkHTML = text != undefined ? text.indexOf('<') : -1;
    const showText =
      checkHTML == -1 ? (
        <Text style={[styles.lennaText, me && styles.meText]}>{text}</Text>
      ) : (
        <HTML html={text} />
      );

    if (voice == true) {
      return this.state.textVoice == true ? (
        <View></View>
      ) : (
        <View>
          {avatar}
          <View style={[styles.lenna, me && styles.me]}>
            {showText}
            <Text style={[styles.timeTextLenna, me && styles.timeTextMe]}>
              {time}
            </Text>
          </View>
        </View>
      );
    } else {
      return (
        <View>
          {avatar}
          <View style={[styles.lenna, me && styles.me]}>
            {showText}
            <Text style={[styles.timeTextLenna, me && styles.timeTextMe]}>
              {time}
            </Text>
          </View>
        </View>
      );
    }
  }
}

var _0x1171 = [];
let storeQb = [];
let prev;
class QuickButton extends Component {
  constructor() {
    super();
    this.state = {
      hide: false,
      qbArr: [],
    };
  }

  componentWillMount() {
    var _0xa9fd = [
      '\x69\x74\x65\x6D',
      '\x70\x72\x6F\x70\x73',
      '\x73\x65\x74\x53\x74\x61\x74\x65',
    ];
    const { text, size } = this[_0xa9fd[1]][_0xa9fd[0]];
    storeQb = [];
    this[_0xa9fd[2]]({ qbArr: [storeQb] });
  }

  componentDidMount() {
    this.getArr();
  }

  componentWillReceiveProps(nextProps) {
    var _0x42b1 = [
      '\x6C\x65\x6E\x67\x74\x68',
      '\x64\x61\x74\x61',
      '\x6D\x61\x69\x6E',
      '\x73\x65\x74\x53\x74\x61\x74\x65',
    ];
    storeQb = [];
    if (nextProps[_0x42b1[2]][_0x42b1[1]][_0x42b1[0]] > prev) {
      this[_0x42b1[3]]({ qbArr: [storeQb] });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    var _0x8220 = [
      '\x6C\x65\x6E\x67\x74\x68',
      '\x64\x61\x74\x61',
      '\x6D\x61\x69\x6E',
    ];
    prev = prevProps[_0x8220[2]][_0x8220[1]][_0x8220[0]];
  }

  getArr = () => {
    var _0xce5b = [
      '\x69\x74\x65\x6D',
      '\x70\x72\x6F\x70\x73',
      '',
      '\x70\x75\x73\x68',
      '\x6D\x61\x70',
      '\x73\x65\x74\x53\x74\x61\x74\x65',
    ];
    const { text } = this[_0xce5b[1]][_0xce5b[0]];
    storeQb = [];
    if (text[0] != _0xce5b[2]) {
      text[_0xce5b[4]](_0xd3b7x1 => {
        storeQb[_0xce5b[3]](_0xd3b7x1);
      });
    }
    this[_0xce5b[5]]({ qbArr: [storeQb] });
  };

  sends = value => {
    var _0xd590 = ['', '\x69\x6E\x69\x20\x74\x65\x78\x74', '\x6C\x6F\x67'];
    let text = _0xd590[0] + value + _0xd590[0];
    console[_0xd590[2]](_0xd590[1], value);
    dispatch.main.send(text);
    this.setState({
      hide: true,
    });
  };

  render() {
    var _0x1a56 = [
      '\x69\x74\x65\x6D',
      '\x70\x72\x6F\x70\x73',
      '\x71\x62\x41\x72\x72',
      '\x73\x74\x61\x74\x65',
    ];
    const { text, size } = this[_0x1a56[1]][_0x1a56[0]];
    let arr = this[_0x1a56[3]][_0x1a56[2]];

    return (
      <View style={styles.qButtonContainer}>
        <ScrollView horizontal={true} bounces={false}>
          {this.state.hide == true || size == 0 ? (
            <View></View>
          ) : (
            arr[0].map((texts, k) => (
              <View item={texts} key={k} style={styles.qButton}>
                <Text
                  onPress={this.sends.bind(this, texts)}
                  style={styles.btnText}
                >
                  {texts}
                </Text>
              </View>
            ))
          )}
        </ScrollView>
      </View>
    );
  }
}

export default connect(({ main, nav, auth, user }) => ({
  main,
  nav,
  auth,
  user,
}))(BubbleText);

const styles = StyleSheet.create({
  time: {
    maxWidth: moderateScale(deviceWidth - 50),
    marginHorizontal: moderateScale(10),
    alignSelf: 'flex-start',
    backgroundColor: 'transparent',
  },
  timeMe: {
    alignSelf: 'flex-end',
    borderTopLeftRadius: moderateScale(7),
    borderTopRightRadius: moderateScale(0),
  },
  timeTextLenna: {
    color: '#3C3C3C',
    alignSelf: 'flex-end',
    fontSize: moderateScale(12),
    backgroundColor: 'transparent',
    marginTop: moderateScale(5),
  },
  timeTextMe: {
    color: '#FFFFFF',
    alignSelf: 'flex-end',
    fontSize: moderateScale(12),
    backgroundColor: 'transparent',
    marginTop: moderateScale(5),
  },
  lenna: {
    maxWidth: moderateScale(deviceWidth - 50),
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(10),
    marginHorizontal: moderateScale(35),
    marginTop: moderateScale(5),
    marginBottom: moderateScale(10),
    borderBottomLeftRadius: moderateScale(20),
    borderBottomRightRadius: moderateScale(20),
    alignSelf: 'flex-start',
    borderTopLeftRadius: moderateScale(20),
    borderTopRightRadius: moderateScale(20),
    backgroundColor: '#F6F7F8',
    shadowRadius: moderateScale(3),
    shadowOpacity: moderateScale(0.1),
    shadowOffset: { width: moderateScale(2), height: moderateScale(6) },
  },
  lennaText: {
    fontSize: moderateScale(13),
    color: '#3C3C3C',
  },
  me: {
    borderBottomLeftRadius: moderateScale(20),
    borderBottomRightRadius: moderateScale(20),
    alignSelf: 'flex-end',
    paddingHorizontal: moderateScale(10),
    borderTopLeftRadius: moderateScale(20),
    borderTopRightRadius: moderateScale(20),
    backgroundColor: '#0090d2',
    marginBottom: moderateScale(20),
    marginHorizontal: moderateScale(10),
    shadowRadius: moderateScale(3),
    shadowOpacity: moderateScale(0.2),
    shadowOffset: { width: moderateScale(3), height: moderateScale(5) },
  },

  meText: {
    fontSize: moderateScale(13),
    color: '#FFFFFF',
  },
  qButton: {
    backgroundColor: '#EFEFEF',
    alignSelf: 'flex-start',
    height: undefined,
    borderTopLeftRadius: moderateScale(10),
    borderTopRightRadius: moderateScale(10),
    borderBottomLeftRadius: moderateScale(10),
    borderBottomRightRadius: moderateScale(10),
    paddingVertical: moderateScale(3),
    marginVertical: moderateScale(2.5),
    marginHorizontal: moderateScale(5),
  },
  qButtonContainer: {
    paddingVertical: moderateScale(5),
  },
  btnText: {
    textAlign: 'center',
    marginHorizontal: moderateScale(10),
    color: '#3C3C3C',
  },
  imgChat: {
    height: moderateScale(30),
    width: moderateScale(30),
    position: 'absolute',
    marginHorizontal: moderateScale(3),
    marginTop: moderateScale(20),
  },
  transparent: {
    backgroundColor: 'transparent',
  },
});
