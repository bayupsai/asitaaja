import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, WebView, Linking } from 'react-native';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';
import { moderateScale } from '../Const/ScaleUtils';
const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

class Html extends Component {
  constructor() {
    super();
    this.state = {
      url: '',
    };
  }

  render() {
    const { item: data } = this.props;
    const url = data.data;
    const cekUrl = url.indexOf('<iframe');
    const cekUrl2 = url.indexOf('<table');

    console.log('data : ', data);
    return (
      <View style={styles.container}>
        {cekUrl >= 0 || cekUrl2 >= 0 ? (
          <View style={{ width: deviceWidth - 20 }}>
            <WebView
              javaScriptEnabled={true}
              bounces={false}
              source={{ html: data.data }}
              scalesPageToFit={false}
              style={{
                flex: 1,
                height: 350,
                width: deviceWidth - 30,
                backgroundColor: 'rgba(255,255,255,0.8)',
              }}
            />
          </View>
        ) : (
          <View style={styles.htmlStyle}>
            <HTML
              html={data.data}
              onLinkPress={(evt, href) => {
                Linking.openURL(href);
              }}
              containerStyle={{ paddingHorizontal: moderateScale(10) }}
            />
          </View>
        )}
      </View>
    );
  }
}

export default connect(({ main, alert }) => ({ main, alert }))(Html);

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    alignItems: 'flex-start',
    borderRadius: 7,
    // minHeight:deviceWidth/4,
    marginHorizontal: moderateScale(10),
    marginVertical: moderateScale(2.5),
  },
  htmlStyle: {
    // paddingVertical         : moderateScale(8),
    marginLeft: moderateScale(25),
    marginVertical: moderateScale(2.5),
    borderBottomLeftRadius: moderateScale(0),
    borderBottomRightRadius: moderateScale(10),
    alignSelf: 'flex-start',
    borderTopLeftRadius: moderateScale(10),
    borderTopRightRadius: moderateScale(10),
    backgroundColor: 'rgba(255,255,255,0.8)',
    width: deviceWidth - 40,
  },
});
