import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TextInput,
  Picker,
  ScrollView,
} from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import Colors from '../Const/Colors';
import DatePicker from 'react-native-datepicker';
import { moderateScale } from '../Const/ScaleUtils';

let titles = ['Mr', 'Mrs'];
let allPassenger = 2;

const { width: deviceWidth } = Dimensions.get('window');

class FormInputView extends Component {
  constructor() {
    super();
    this.state = {
      hide: false,
      selected: 'Mrs',
      arr: [],
      title: '',
      date: new Date(),

      fieldTitle1: 'Mr',
      fieldTitle2: 'Mr',
      fieldTitle3: 'Mr',
      fieldTitle4: 'Mr',
      fieldTitle5: 'Mr',
      fieldTitle6: 'Mr',
      fieldTitle7: 'Mr',

      fieldName1: '',
      fieldName2: '',
      fieldName3: '',
      fieldName4: '',
      fieldName5: '',
      fieldName6: '',
      fieldName7: '',

      fieldEmail1: '',
      fieldEmail2: '',
      fieldEmail3: '',
      fieldEmail4: '',
      fieldEmail5: '',
      fieldEmail6: '',
      fieldEmail7: '',

      fieldPhone1: '',
      fieldPhone2: '',
      fieldPhone3: '',
      fieldPhone4: '',
      fieldPhone5: '',
      fieldPhone6: '',
      fieldPhone7: '',

      fieldBirth1: new Date(),
      fieldBirth2: new Date(),
      fieldBirth3: new Date(),
      fieldBirth4: new Date(),
      fieldBirth5: new Date(),
      fieldBirth6: new Date(),
      fieldBirth7: new Date(),
    };
  }

  // componentWillMount(){
  //   this.theTextInput()
  // }

  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  send = () => {
    const { item: data } = this.props;

    let arrFields = [];
    let arrPassenger = {
      adult: arrFields,
      scheduleDetail: '123123123123',
    };

    // let lengthTitle = Object.keys(fieldsTitle).length;
    // let lengthName = Object.keys(fieldsName).length;
    // let lengthEmail = Object.keys(fieldsEmail).length;
    // let lengthPhone = Object.keys(fieldsPhone).length;

    // if(lengthTitle && lengthName && lengthName && lengthEmail && lengthPhone > 0)
    // {
    if (allPassenger == 1) {
      arrFields.push({
        title: this.state.fieldTitle1,
        name: this.state.fieldName1,
        email: this.state.fieldEmail1,
        phone: this.state.fieldPhone1,
        // identity_number:fieldsKTP[i],
        birth_date: this.state.fieldBirth1,
        nationality: 'ID',
        passport_no: '',
        passport_country: '',
        passport_issue_data: '',
        passport_expire: '',
      });
    } else if (allPassenger == 2) {
      arrFields.push(
        {
          title: this.state.fieldTitle1,
          name: this.state.fieldName1,
          email: this.state.fieldEmail1,
          phone: this.state.fieldPhone1,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth1,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle2,
          name: this.state.fieldName2,
          email: this.state.fieldEmail2,
          phone: this.state.fieldPhone2,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth2,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        }
      );
    } else if (allPassenger == 3) {
      arrFields.push(
        {
          title: this.state.fieldTitle1,
          name: this.state.fieldName1,
          email: this.state.fieldEmail1,
          phone: this.state.fieldPhone1,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth1,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle2,
          name: this.state.fieldName2,
          email: this.state.fieldEmail2,
          phone: this.state.fieldPhone2,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth2,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle3,
          name: this.state.fieldName3,
          email: this.state.fieldEmail3,
          phone: this.state.fieldPhone3,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth3,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        }
      );
    } else if (allPassenger == 4) {
      arrFields.push(
        {
          title: this.state.fieldTitle1,
          name: this.state.fieldName1,
          email: this.state.fieldEmail1,
          phone: this.state.fieldPhone1,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth1,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle2,
          name: this.state.fieldName2,
          email: this.state.fieldEmail2,
          phone: this.state.fieldPhone2,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth2,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle3,
          name: this.state.fieldName3,
          email: this.state.fieldEmail3,
          phone: this.state.fieldPhone3,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth3,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle4,
          name: this.state.fieldName4,
          email: this.state.fieldEmail4,
          phone: this.state.fieldPhone4,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth4,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        }
      );
    } else if (allPassenger == 5) {
      arrFields.push(
        {
          title: this.state.fieldTitle1,
          name: this.state.fieldName1,
          email: this.state.fieldEmail1,
          phone: this.state.fieldPhone1,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth1,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle2,
          name: this.state.fieldName2,
          email: this.state.fieldEmail2,
          phone: this.state.fieldPhone2,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth2,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle3,
          name: this.state.fieldName3,
          email: this.state.fieldEmail3,
          phone: this.state.fieldPhone3,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth3,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle4,
          name: this.state.fieldName4,
          email: this.state.fieldEmail4,
          phone: this.state.fieldPhone4,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth4,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle5,
          name: this.state.fieldName5,
          email: this.state.fieldEmail5,
          phone: this.state.fieldPhone5,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth5,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        }
      );
    } else if (allPassenger == 6) {
      arrFields.push(
        {
          title: this.state.fieldTitle1,
          name: this.state.fieldName1,
          email: this.state.fieldEmail1,
          phone: this.state.fieldPhone1,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth1,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle2,
          name: this.state.fieldName2,
          email: this.state.fieldEmail2,
          phone: this.state.fieldPhone2,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth2,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle3,
          name: this.state.fieldName3,
          email: this.state.fieldEmail3,
          phone: this.state.fieldPhone3,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth3,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle4,
          name: this.state.fieldName4,
          email: this.state.fieldEmail4,
          phone: this.state.fieldPhone4,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth4,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle5,
          name: this.state.fieldName5,
          email: this.state.fieldEmail5,
          phone: this.state.fieldPhone5,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth5,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle6,
          name: this.state.fieldName6,
          email: this.state.fieldEmail6,
          phone: this.state.fieldPhone6,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth6,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        }
      );
    } else if (allPassenger == 7) {
      arrFields.push(
        {
          title: this.state.fieldTitle1,
          name: this.state.fieldName1,
          email: this.state.fieldEmail1,
          phone: this.state.fieldPhone1,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth1,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle2,
          name: this.state.fieldName2,
          email: this.state.fieldEmail2,
          phone: this.state.fieldPhone2,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth2,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle3,
          name: this.state.fieldName3,
          email: this.state.fieldEmail3,
          phone: this.state.fieldPhone3,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth3,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle4,
          name: this.state.fieldName4,
          email: this.state.fieldEmail4,
          phone: this.state.fieldPhone4,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth4,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle5,
          name: this.state.fieldName5,
          email: this.state.fieldEmail5,
          phone: this.state.fieldPhone5,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth5,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle6,
          name: this.state.fieldName6,
          email: this.state.fieldEmail6,
          phone: this.state.fieldPhone6,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth6,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        },
        {
          title: this.state.fieldTitle7,
          name: this.state.fieldName7,
          email: this.state.fieldEmail7,
          phone: this.state.fieldPhone7,
          // identity_number:fieldsKTP[i],
          birth_date: this.state.fieldBirth7,
          nationality: 'ID',
          passport_no: '',
          passport_country: '',
          passport_issue_data: '',
          passport_expire: '',
        }
      );
    }

    let paramStringfy = JSON.stringify(arrPassenger);
    console.log('paramStringfy : ', paramStringfy);
    console.log('arr : ', arrPassenger);
    let text = `Pesankan saya tiket pesawat untuk ${allPassenger} orang`;

    dispatch.alert.show({
      image: 'warning',
      color: 'garuda',
      btncolor: 'garuda',
      title: 'Apakah Anda yakin?',
      message: 'Data yang Anda isi sudah benar ?',
      buttons: [
        { title: 'Batal', type: 'resolve', callback: () => this.cancel() },
        {
          title: 'YA',
          type: 'resolve',
          callback: () => this.sendPassenger(paramStringfy, text),
        },
      ],
    });
    // }
    // else
    // {
    //   dispatch.alert.show({
    //     image:'warning',
    //     color:'warning',
    //     btncolor:'warning',
    //     title:'Perhatian !',
    //     message:'Harap isi seluruh data',
    //     buttons:[
    //       {title:'OK',type:'resolve',callback:()=>dispatch.alert.hide()},
    //     ]
    //   })
    // }
  };

  sendPassenger = (data, text) => {
    dispatch.main.sendNoGetChat(text);
    dispatch.main.getChat(data);
    console.log('data sendPassenger : ', data);
    this.setState({
      hide: true,
    });
  };
  cancel = () => {
    dispatch.main.send('Batal');
    this.setState({
      hide: true,
    });
  };

  callViewPassenger1 = () => {
    return (
      <View style={{ flexDirection: 'row', width: deviceWidth }}>
        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>Title</Text>
          {/* <TextInput style={styles.fieldTitle} onChangeText={(text)=>this.setState({fieldTitle1:text})} placeholder={'Mr/Mrs'}/> */}
          <Picker
            selectedValue={this.state.fieldTitle1}
            onValueChange={text => this.setState({ fieldTitle1: text })}
            style={styles.fieldTitle}
          >
            <Picker.Item label="Mr" value="Mr" />
            <Picker.Item label="Mrs" value="Mrs" />
          </Picker>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>{`Nama Penumpang 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldName1: text })}
            placeholder={'Nama Penumpang'}
          />

          <Text style={styles.title}>{`Email 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldEmail1: text })}
            placeholder={'Email Penumpang'}
          />

          <Text style={styles.title}>{`No. Handphone 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldPhone1: text })}
            placeholder={'Nomor Handphone'}
          />

          {/* <Text style={styles.title}>{`No. KTP ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsKTP[i] = text} placeholder={'Nomor KTP'}/> */}

          {/* <Text style={styles.title}>{`Tanggal Lahir ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsLahir[i] = text} placeholder={'Nama Penumpang'} /> */}
          <Text style={styles.title}>{`Tanggal Lahir 1`}</Text>
          <DatePicker
            style={styles.fieldLong}
            customStyles={{
              dateText: {
                color: '#000000',
                fontSize: moderateScale(16),
              },
              dateInput: {
                width: deviceWidth / 1.6,
                height: moderateScale(40),
                borderColor: '#3C3C3C',
                borderWidth: 0.5,
                marginBottom: moderateScale(10),
                backgroundColor: '#F8F6F9',
              },
            }}
            date={this.state.fieldBirth1}
            mode="date"
            placeholder="Tanggal"
            format="YYYYMMDD"
            minDate="1945-01-01"
            maxDate="2050-01-01"
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={date => this.setState({ fieldBirth1: date })}
          />
        </View>
      </View>
    );
  };

  callViewPassenger2 = () => {
    return (
      <View style={{ flexDirection: 'row', width: deviceWidth }}>
        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>Title</Text>
          {/* <TextInput style={styles.fieldTitle} onChangeText={(text)=>this.setState({fieldTitle1:text})} placeholder={'Mr/Mrs'}/> */}
          <Picker
            selectedValue={this.state.fieldTitle1}
            onValueChange={text => this.setState({ fieldTitle1: text })}
            style={styles.fieldTitle}
          >
            <Picker.Item label="Mr" value="Mr" />
            <Picker.Item label="Mrs" value="Mrs" />
          </Picker>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>{`Nama Penumpang 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldName1: text })}
            placeholder={'Nama Penumpang'}
          />

          <Text style={styles.title}>{`Email 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldEmail1: text })}
            placeholder={'Email Penumpang'}
          />

          <Text style={styles.title}>{`No. Handphone 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldPhone1: text })}
            placeholder={'Nomor Handphone'}
          />

          {/* <Text style={styles.title}>{`No. KTP ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsKTP[i] = text} placeholder={'Nomor KTP'}/> */}

          {/* <Text style={styles.title}>{`Tanggal Lahir ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsLahir[i] = text} placeholder={'Nama Penumpang'} /> */}
          <Text style={styles.title}>{`Tanggal Lahir 1`}</Text>
          <DatePicker
            style={styles.fieldLong}
            customStyles={{
              dateText: {
                color: '#000000',
                fontSize: moderateScale(16),
              },
              dateInput: {
                width: deviceWidth / 1.6,
                height: moderateScale(40),
                borderColor: '#3C3C3C',
                borderWidth: 0.5,
                marginBottom: moderateScale(10),
                backgroundColor: '#F8F6F9',
              },
            }}
            date={this.state.fieldBirth1}
            mode="date"
            placeholder="Tanggal"
            format="YYYYMMDD"
            minDate="1945-01-01"
            maxDate="2050-01-01"
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={date => this.setState({ fieldBirth1: date })}
          />
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>Title 2</Text>
          {/* <TextInput style={styles.fieldTitle} onChangeText={(text)=>this.setState({fieldTitle1:text})} placeholder={'Mr/Mrs'}/> */}
          <Picker
            selectedValue={this.state.fieldTitle2}
            onValueChange={text => this.setState({ fieldTitle2: text })}
            style={styles.fieldTitle}
          >
            <Picker.Item label="Mr" value="Mr" />
            <Picker.Item label="Mrs" value="Mrs" />
          </Picker>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>{`Nama Penumpang 2`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldName2: text })}
            placeholder={'Nama Penumpang'}
          />

          <Text style={styles.title}>{`Email 2`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldEmail2: text })}
            placeholder={'Email Penumpang'}
          />

          <Text style={styles.title}>{`No. Handphone 2`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldPhone2: text })}
            placeholder={'Nomor Handphone'}
          />

          {/* <Text style={styles.title}>{`No. KTP ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsKTP[i] = text} placeholder={'Nomor KTP'}/> */}

          {/* <Text style={styles.title}>{`Tanggal Lahir ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsLahir[i] = text} placeholder={'Nama Penumpang'} /> */}
          <Text style={styles.title}>{`Tanggal Lahir 2`}</Text>
          <DatePicker
            style={styles.fieldLong}
            customStyles={{
              dateText: {
                color: '#000000',
                fontSize: moderateScale(16),
              },
              dateInput: {
                width: deviceWidth / 1.6,
                height: moderateScale(40),
                borderColor: '#3C3C3C',
                borderWidth: 0.5,
                marginBottom: moderateScale(10),
                backgroundColor: '#F8F6F9',
              },
            }}
            date={this.state.fieldBirth2}
            mode="date"
            placeholder="Tanggal"
            format="YYYYMMDD"
            minDate="1945-01-01"
            maxDate="2050-01-01"
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={date => this.setState({ fieldBirth2: date })}
          />
        </View>
      </View>
    );
  };

  callViewPassenger3 = () => {
    return (
      <View style={{ flexDirection: 'row', width: deviceWidth }}>
        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>Title</Text>
          {/* <TextInput style={styles.fieldTitle} onChangeText={(text)=>this.setState({fieldTitle1:text})} placeholder={'Mr/Mrs'}/> */}
          <Picker
            selectedValue={this.state.fieldTitle1}
            onValueChange={text => this.setState({ fieldTitle1: text })}
            style={styles.fieldTitle}
          >
            <Picker.Item label="Mr" value="Mr" />
            <Picker.Item label="Mrs" value="Mrs" />
          </Picker>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>{`Nama Penumpang 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldName1: text })}
            placeholder={'Nama Penumpang'}
          />

          <Text style={styles.title}>{`Email 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldEmail1: text })}
            placeholder={'Email Penumpang'}
          />

          <Text style={styles.title}>{`No. Handphone 1`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldPhone1: text })}
            placeholder={'Nomor Handphone'}
          />

          {/* <Text style={styles.title}>{`No. KTP ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsKTP[i] = text} placeholder={'Nomor KTP'}/> */}

          {/* <Text style={styles.title}>{`Tanggal Lahir ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsLahir[i] = text} placeholder={'Nama Penumpang'} /> */}
          <Text style={styles.title}>{`Tanggal Lahir 1`}</Text>
          <DatePicker
            style={styles.fieldLong}
            customStyles={{
              dateText: {
                color: '#000000',
                fontSize: moderateScale(16),
              },
              dateInput: {
                width: deviceWidth / 1.6,
                height: moderateScale(40),
                borderColor: '#3C3C3C',
                borderWidth: 0.5,
                marginBottom: moderateScale(10),
                backgroundColor: '#F8F6F9',
              },
            }}
            date={this.state.fieldBirth1}
            mode="date"
            placeholder="Tanggal"
            format="YYYYMMDD"
            minDate="1945-01-01"
            maxDate="2050-01-01"
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={date => this.setState({ fieldBirth1: date })}
          />
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>Title 2</Text>
          {/* <TextInput style={styles.fieldTitle} onChangeText={(text)=>this.setState({fieldTitle1:text})} placeholder={'Mr/Mrs'}/> */}
          <Picker
            selectedValue={this.state.fieldTitle2}
            onValueChange={text => this.setState({ fieldTitle2: text })}
            style={styles.fieldTitle}
          >
            <Picker.Item label="Mr" value="Mr" />
            <Picker.Item label="Mrs" value="Mrs" />
          </Picker>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>{`Nama Penumpang 2`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldName2: text })}
            placeholder={'Nama Penumpang'}
          />

          <Text style={styles.title}>{`Email 2`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldEmail2: text })}
            placeholder={'Email Penumpang'}
          />

          <Text style={styles.title}>{`No. Handphone 2`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldPhone2: text })}
            placeholder={'Nomor Handphone'}
          />

          {/* <Text style={styles.title}>{`No. KTP ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsKTP[i] = text} placeholder={'Nomor KTP'}/> */}

          {/* <Text style={styles.title}>{`Tanggal Lahir ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsLahir[i] = text} placeholder={'Nama Penumpang'} /> */}
          <Text style={styles.title}>{`Tanggal Lahir 2`}</Text>
          <DatePicker
            style={styles.fieldLong}
            customStyles={{
              dateText: {
                color: '#000000',
                fontSize: moderateScale(16),
              },
              dateInput: {
                width: deviceWidth / 1.6,
                height: moderateScale(40),
                borderColor: '#3C3C3C',
                borderWidth: 0.5,
                marginBottom: moderateScale(10),
                backgroundColor: '#F8F6F9',
              },
            }}
            date={this.state.fieldBirth2}
            mode="date"
            placeholder="Tanggal"
            format="YYYYMMDD"
            minDate="1945-01-01"
            maxDate="2050-01-01"
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={date => this.setState({ fieldBirth2: date })}
          />
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>Title 3</Text>
          {/* <TextInput style={styles.fieldTitle} onChangeText={(text)=>this.setState({fieldTitle1:text})} placeholder={'Mr/Mrs'}/> */}
          <Picker
            selectedValue={this.state.fieldTitle3}
            onValueChange={text => this.setState({ fieldTitle3: text })}
            style={styles.fieldTitle}
          >
            <Picker.Item label="Mr" value="Mr" />
            <Picker.Item label="Mrs" value="Mrs" />
          </Picker>
        </View>

        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.title}>{`Nama Penumpang 3`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldName3: text })}
            placeholder={'Nama Penumpang'}
          />

          <Text style={styles.title}>{`Email 3`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldEmail3: text })}
            placeholder={'Email Penumpang'}
          />

          <Text style={styles.title}>{`No. Handphone 3`}</Text>
          <TextInput
            style={styles.fieldLong}
            onChangeText={text => this.setState({ fieldPhone3: text })}
            placeholder={'Nomor Handphone'}
          />

          {/* <Text style={styles.title}>{`No. KTP ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsKTP[i] = text} placeholder={'Nomor KTP'}/> */}

          {/* <Text style={styles.title}>{`Tanggal Lahir ${i+1}`}</Text>
              <TextInput style={styles.fieldLong} onChangeText={(text)=>fieldsLahir[i] = text} placeholder={'Nama Penumpang'} /> */}
          <Text style={styles.title}>{`Tanggal Lahir 3`}</Text>
          <DatePicker
            style={styles.fieldLong}
            customStyles={{
              dateText: {
                color: '#000000',
                fontSize: moderateScale(16),
              },
              dateInput: {
                width: deviceWidth / 1.6,
                height: moderateScale(40),
                borderColor: '#3C3C3C',
                borderWidth: 0.5,
                marginBottom: moderateScale(10),
                backgroundColor: '#F8F6F9',
              },
            }}
            date={this.state.fieldBirth3}
            mode="date"
            placeholder="Tanggal"
            format="YYYYMMDD"
            minDate="1945-01-01"
            maxDate="2050-01-01"
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={date => this.setState({ fieldBirth3: date })}
          />
        </View>
      </View>
    );
  };

  render() {
    const { item: data } = this.props;
    // let passengerContact = data.columns.contact

    return (
      <View>
        {this.state.hide == false ? (
          <View style={styles.container}>
            <View style={styles.child}>
              <ScrollView>
                <Text style={styles.title}>{data.headertext}</Text>

                {this.callViewPassenger3()}

                <View
                  style={{ flexDirection: 'row', justifyContent: 'flex-end' }}
                >
                  <Button
                    title="Batal"
                    titleStyle={{ color: '#0C56C1' }}
                    buttonStyle={styles.buttonCancel}
                    onPress={this.cancel}
                  />
                  <Button
                    title="Ok"
                    titleStyle={{ color: '#FFFFFF' }}
                    buttonStyle={styles.button}
                    onPress={this.send}
                  />
                </View>
              </ScrollView>
            </View>
          </View>
        ) : (
          <View></View>
        )}
      </View>
    );
  }
}

export default connect(({ main, alert }) => ({ main, alert }))(FormInputView);

const styles = StyleSheet.create({
  container: {
    maxWidth: deviceWidth - 20,
    width: deviceWidth - 20,
    marginHorizontal: 10,
    marginVertical: 2.5,
    borderRadius: 7,
    backgroundColor: '#fff',
  },
  formContainer: {
    flexDirection: 'row',
  },
  child: {
    width: deviceWidth - 20,
    padding: 15,
  },
  title: {
    alignSelf: 'center',
    fontSize: 14,
    color: Colors.dark,
    marginBottom: 10,
  },
  button: {
    height: 45,
    borderRadius: 4,
    backgroundColor: Colors.primary,
    marginLeft: 10,
    width: 60,
  },
  buttonCancel: {
    height: 45,
    borderRadius: 4,
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: Colors.primary,
    width: 60,
  },
  fieldTitle: {
    width: 100,
    height: moderateScale(40),
    borderColor: '#3C3C3C',
    borderWidth: 0.5,
    marginRight: moderateScale(5),
    backgroundColor: '#F8F6F9',
  },
  fieldLong: {
    width: deviceWidth / 1.6,
    height: moderateScale(40),
    borderColor: '#3C3C3C',
    borderWidth: 0.5,
    marginBottom: moderateScale(10),
    backgroundColor: '#F8F6F9',
  },
  title: {
    fontSize: moderateScale(14),
    color: '#3C3C3C',
    marginBottom: moderateScale(5),
  },
});
