import React, { PureComponent } from 'react';
import {
  Image,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Dimensions,
  Linking,
  TouchableHighlight,
  ImageBackground,
} from 'react-native';
import API from '../Const/APIEndPoints';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import { Button } from 'react-native-elements';
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';

const { width: width, height: deviceHeight } = Dimensions.get('window');

export default class BubbleSliderImage extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { item } = this.props;

    const { data } = item.columns;
    console.log('data : ', item.columns);

    return (
      <ScrollView horizontal={true} bounces={false}>
        {item.columns.map((element, i) => (
          <ImageSlider item={element} key={i} />
        ))}
      </ScrollView>
    );
  }
}

class ImageSlider extends PureComponent {
  constructor() {
    super();
    this.state = {
      payload: '',
    };
  }

  send = value => {
    console.log('value send : ', value);
    let text = `${value}`; //the payload is object from backend
    // let getPayload = text.indexOf('native')

    // if(getPayload === 0)
    // {
    //     let strLoc = text
    //     let splitStart = strLoc.split("(")[1];
    //     let splitEnd = splitStart.split(")")[0];
    //     splitEnd = splitStart.split(",");

    //     let label = (splitEnd[2]);
    //     let strLabel = label.replace(")","")
    //     let namePlace = encodeURIComponent(strLabel)
    //     console.log('nameplace : ', namePlace)

    //     let locations = API.map + namePlace
    //     Linking.openURL(locations)

    // }
    // else
    // {
    dispatch.main.send(text);
    // }
  };

  render() {
    const { item } = this.props;
    console.log('item :', item);
    // let urlCheck = item.imageurl.indexOf('https://')
    description = item.description;

    // const baseUrl = (urlCheck === -1 ) ? API.assets : ''

    // let noSpaceImg = item.imageurl.replace(" ","")

    return (
      <View style={styles.carousel}>
        <ImageBackground
          source={{ uri: item.thumbnailImageUrl }}
          style={styles.imageBackground}
          imageStyle={{ borderRadius: moderateScale(15) }}
        >
          <View style={styles.itemContainer}>
            <View style={styles.textContainer}>
              <ScrollView bounces={false}>
                <Text style={styles.textStyle}>{item.title}</Text>
              </ScrollView>
            </View>
            {item.actions.map((subelement, k) => (
              <View style={styles.buttonContainer}>
                <Button
                  title={subelement.label}
                  titleStyle={{
                    fontWeight: '700',
                    fontSize: moderateScale(14),
                  }}
                  onPress={this.send.bind(this, subelement.data)}
                  containerViewStyle={{ marginLeft: 4, marginRight: 4 }}
                  buttonStyle={[styles.button]}
                  color="#FFFFFF"
                  fontWeight="bold"
                />
              </View>
            ))}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

connect(({ main }) => ({ main }))(ImageSlider);

const styles = StyleSheet.create({
  imageBackground: {
    height: deviceHeight / 2.5,
    width: moderateScale(width * 0.5),
    resizeMode: 'stretch',
  },
  itemContainer: {
    height: deviceHeight / 2.5,
    width: moderateScale(width * 0.5),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: moderateScale(40),
  },
  textContainer: {
    height: moderateScale(100),
  },
  textStyle: {
    color: '#FFFFFF',
    fontSize: moderateScale(16),
    fontWeight: '700',
    marginHorizontal: moderateScale(10),
  },
  carousel: {
    flex: 1,
    height: deviceHeight / 2.5,
    width: moderateScale(width * 0.5),
    paddingVertical: moderateScale(8),
    marginVertical: moderateScale(2.5),
    marginHorizontal: moderateScale(5),
  },
  buttonContainer: {
    alignItems: 'center',
  },
  button: {
    width: width * 0.5 - 20,
    height: moderateScale(40),
    backgroundColor: '#ec650a',
    borderRadius: 20,
    alignItems: 'center',
  },
});
