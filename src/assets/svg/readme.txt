to use svg file, follow this solution:

1. Convert .svg image to JSX with https://svg2jsx.herokuapp.com/
2. Convert the JSX to react-native-svg component with https://svgr.now.sh/ (check the "React Native checkbox)

