import { dispatch } from '../redux/store';

const INITIAL_STATE = {
  isOpen: false,
  title: 'Warning',
  message: 'Something happened',
  color: 'yellow',
  btncolor: 'default',
  image: 'warning',
  buttons: [
    { title: 'OK', type: 'resolve', callback: () => dispatch.alert.hide() },
  ],
  featureModal: false,
  list: [],
};
export default {
  state: INITIAL_STATE, // initial state
  reducers: {
    // handle state changes with pure functions
    hide: (state, payload) => Object.assign({}, state, { isOpen: false }),
    show: (state, payload) =>
      Object.assign({}, state, { isOpen: true }, payload),
    reset: (state, payload) => (state = INITIAL_STATE),
  },
  effects: {
    alert: async payload => {
      dispatch.alert.show(payload);
    },
  },
};
