import { AsyncStorage } from 'react-native';
import { post, get, postWithHeader, postNoToken } from '../services/Rest';
import moment from 'moment';
import Voice from 'react-native-voice';
import API from '../Const/APIEndPoints';
import Sound from 'react-native-sound';
import { dispatch } from '../redux/store';
import { userData } from '../services/Auth';
import { USERDATACONFIG } from '../Const/Config';

const INITIAL_STATE = {
  data: [],
  type: [],
  keyboardMode: true, //change this to false for set type mode to voice at Main.js screen
  isListen: false,
  query: '',
  showModal: false,
  tripType: '',
};

let access_token;
let user_id;
let sound; //var play sound

export default {
  state: INITIAL_STATE, // initial state
  reducers: {
    // handle state changes with pure functions
    addChat: (state, payload) =>
      Object.assign({}, state, { data: [...state.data, payload] }),
    addType: (state, payload) =>
      Object.assign({}, state, { type: [...state.type, payload] }),
    update: (state, payload) => Object.assign({}, state, payload),
    reset: (state, payload) => (state = INITIAL_STATE),
    toggleKeyboard: (state, payload) =>
      Object.assign({}, state, { keyboardMode: !state.keyboardMode }),
  },
  effects: {
    send: async query => {
      if (query) {
        let time = moment().format('HH:mm');

        let timestamp = {
          time: time,
        };

        let param = {
          text: query,
          me: true,
        };

        const params = Object.assign({}, param, timestamp);
        dispatch.main.addChat(params);
        dispatch.main.getChat(query);
      }
    },
    sendNoGetChat: async query => {
      if (query) {
        let time = moment().format('HH:mm');

        let timestamp = {
          time: time,
        };

        let param = {
          text: query,
          me: true,
        };

        const params = Object.assign({}, param, timestamp);
        dispatch.main.addChat(params);
      }
    },
    startRecord: async () => {
      Voice.start('id_ID');
      dispatch.main.update({ isListen: true });
      sound.stop();
    },
    stopRecord: async () => {
      Voice.stop();
      dispatch.main.update({ isListen: false });
    },

    showModal: async tripType => {
      dispatch.main.update({ showModal: true, tripType: tripType });
    },
    hideModal: async () => {
      dispatch.main.update({ showModal: false });
    },

    getChat: async payload => {
      let user = await AsyncStorage.getItem(USERDATACONFIG);
      user = JSON.parse(user);
      console.log('user getchat : ', user);
      let acc_token;
      let userid;

      if (user_id == undefined) {
        userid = user.user_id;
      } else {
        userid = user_id;
      }

      let param = {
        user_id: userid,
        bot_id: '1yXDy4',
        query: payload,
      };

      //   let param = {
      //     user_id: "5Dja4B",
      //     bot_id: "1yXDy4",
      //     query: payload,

      // };

      if (access_token == undefined) {
        acc_token = user.access_token;
      } else {
        acc_token = access_token;
      }

      let speak = [];
      // let headertoken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjY4MjI1OTRiY2NhNzg3YzI1OTk0ZmNlNTJkOWY1Yzc4MjU5OTA0Njk4ODE5YjgwMDg3NzRmNjM5M2I2NWFiMTFlYjNiNTFmMWE2MzEwMjZiIn0.eyJhdWQiOiIxIiwianRpIjoiNjgyMjU5NGJjY2E3ODdjMjU5OTRmY2U1MmQ5ZjVjNzgyNTk5MDQ2OTg4MTliODAwODc3NGY2MzkzYjY1YWIxMWViM2I1MWYxYTYzMTAyNmIiLCJpYXQiOjE1NjY4MDEzOTQsIm5iZiI6MTU2NjgwMTM5NCwiZXhwIjoxNTk4NDIzNzk0LCJzdWIiOiIyOTc4MCIsInNjb3BlcyI6W119.EdOP9e5mPw7Yc5XE5lKU9Hk1dKEuUNgVLeOb23vkDkOjL0cTiAY-eYrEKAcTCLfUAgIK3zopH6N1dtnabXOBnEanHaXWlMbuD_OTxQtJUvNZwu_aLi4ShN_BWKo2jo7owAROoPnNB9eONF6pnsQ_oNp1opnblXFUVCJiG1OMZgapiuUplJs6OwybwKBrXmI9o_ZQNhI5kd6isH_QkbSv1_k4TW_QGedWwbhEO2BTTo4U1ckC1EDf1DS2Iy4-mr_ufHLRCt2XVw_HBomgPhOctM4SyyK_491_bUg0Sbp6mg86BLbJFucUCm_7Kep6tfx9NWway5HJvSsBdVM-HwhvnIaL-mLbofuW9_Pja5A7E4ryIpp9xLiH9s0_pwL-kkgTlOJjJyp3inMLXZqsod6EOCpKXmVvJQs4-GShtdfjrtBZNMeiAnl7zqt1naT8JOPKFY00HnVcdjZFZOW1YTUwbRuXawTvxt-CMNjDi1ae8SQOdYrqeS8PAzmiqumAp_ocBn5iRTeUqntJHCMfIrU5riUFaQa-bLgvuQpg2L2IPREOZMQZXl3aa07eV4POQBiJeyKRshlY4N4dyY3w0wwm65_hnN_gSOWjdN9dZfC88yhmXRUt_quKCjNgiA2r8TXo5SeCwupYFWE5AKrcGqSGG5nh-N4HSm_vYlex5aq7tbg"

      let headertoken = acc_token;
      try {
        let response = await postWithHeader(API.lennaV2, param, headertoken);
        console.log('response : ', response);

        if (response.result.output.length > 0) {
          response.result.output.forEach(element => {
            element.entities = response.result.entities
              ? response.result.entities
              : null;
            element.intents = response.result.intents
              ? response.result.intents
              : null;

            let time = moment().format('HH:mm');

            let timestamp = {
              time: time,
            };

            // if (payload == "pesawat"){
            //   dispatch.main.showModal()
            // }

            if (element.type == 'flightTripDetailForm') {
              dispatch.main.showModal(element.columns[0].value);
            }

            const params = Object.assign({}, element, timestamp);

            dispatch.main.addChat(params);

            if (response.result.quickbutton.length > 1) {
              let params = {
                text: response.result.quickbutton,
                type: 'quickbutton', //use param type to make new type
              };

              dispatch.main.addChat(params);
              // dispatch.main.sendTime(false)
            }

            // dispatch.main.addChat(element)
            speak.push(element.speech);
            // console.log('element : ', element)
          });

          dispatch.main.speak(speak.join('.'));
          // dispatch.main.sendTime(false)
        }
      } catch (error) {
        console.log('response getchat error : ', error);
        if (error == 'Network Error' || error == 'timeout of 0ms exceeded') {
          let message = {
            text: `Tidak ada koneksi internet`,
            type: 'text',
          };

          dispatch.main.addChat(message);
          dispatch.main.speak(message.text);
          // dispatch.main.sendTime(false)
        } else {
          dispatch.main.error();
        }
      }
    },
    getTokenChat: async payload => {
      console.log('payload pak :', payload);

      try {
        let responseAcc = await postNoToken(API.emailHit, payload);

        if (responseAcc.statusCode == 2001) {
          let store = {
            user_id: responseAcc.data.user.hashed_id,
            access_token: responseAcc.data.token.access_token,
          };

          access_token = responseAcc.data.token.access_token;
          user_id = responseAcc.data.user.hashed_id;

          userData(store);

          let param = {
            user_id: responseAcc.data.user.hashed_id,
            bot_id: '1yXDy4',
            query: 'menu',
          };

          let response = await postWithHeader(
            API.lennaV2,
            param,
            responseAcc.data.token.access_token
          );
          let speak = [];

          if (response.result.output.length > 0) {
            response.result.output.forEach(element => {
              element.entities = response.result.entities
                ? response.result.entities
                : null;
              element.intents = response.result.intents
                ? response.result.intents
                : null;

              let time = moment().format('HH:mm');

              let timestamp = {
                time: time,
              };

              const params = Object.assign({}, element, timestamp);

              dispatch.main.addChat(params);

              if (response.result.quickbutton.length > 1) {
                let params = {
                  text: response.result.quickbutton,
                  type: 'quickbutton', //use param type to make new type
                };

                dispatch.main.addChat(params);
                // dispatch.main.sendTime(false)
              }

              speak.push(element.speech);
            });

            dispatch.main.speak(speak.join('.'));
          } // end of chat looping
        } else {
        }
      } catch (error) {
        console.log('error gettoken chat : ', error);
      }
    },
    speak: async text => {
      let payload = {
        platform: 'ios',
      };

      const response = await post(API.getVoice, payload);

      let resfemale = response.female;
      let encodetext = encodeURI(text);
      var replace;

      replace = resfemale.replace('$paramtext', encodetext);

      if (typeof sound != 'undefined') {
        sound.stop();
        sound = undefined;
      }

      sound = new Sound(replace, null, error => {
        if (error) {
          console.log('error voice : ', error);
        } else {
          sound.play();
        }
      });
    },

    error: async () => {
      let time = moment().format('HH:mm');

      let timestamp = {
        time: time,
      };
      let message = {
        text: `Maaf Terjadi kesalahan, silahkan coba beberapa saat lagi`,
        type: 'text',
      };

      const params = Object.assign({}, message, timestamp);

      dispatch.main.addChat(params);
      dispatch.main.speak(message.text);
    },
  },
};
