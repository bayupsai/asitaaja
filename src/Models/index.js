import alert from './alert';
import main from './main';

export default {
  alert,
  main,
};
