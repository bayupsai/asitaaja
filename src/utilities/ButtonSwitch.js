/* Switch Button Component class
 */
import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import { fontExtraBold } from '../../../src/base/constant';

const getLabel = (type, active) => {
  let icn;
  switch (type) {
    case 'Open':
      ``;
      icn = active ? (
        <Text style={{ color: 'white', fontFamily: fontExtraBold }}>
          One Way
        </Text>
      ) : (
        <Text
          style={{
            color: '#838383',
            fontFamily: fontExtraBold,
            paddingRight: 30,
          }}
        >
          One Way
        </Text>
      );
      break;
    case 'In Progress':
      icn = active ? (
        <Text style={{ color: 'white', fontFamily: fontExtraBold }}>-</Text>
      ) : (
        <Text style={{ color: 'blue', fontFamily: fontExtraBold }}>-</Text>
      );
      break;
    case 'Complete':
      icn = active ? (
        <Text style={{ color: 'white', fontFamily: fontExtraBold }}>
          Return
        </Text>
      ) : (
        <Text
          style={{
            color: '#838383',
            fontFamily: fontExtraBold,
            paddingLeft: 30,
          }}
        >
          Return
        </Text>
      );
      break;
  }
  return icn;
};

const Button = props => {
  return (
    <View>
      <TouchableOpacity onPress={props.onPress} style={styles.buttonStyle}>
        {getLabel(props.type, props.active)}
      </TouchableOpacity>
    </View>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  active: PropTypes.bool,
  onPress: PropTypes.func,
};

Button.defaultProps = {
  active: false,
};

export default Button;
