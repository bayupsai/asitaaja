import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Modal from 'react-native-modal';
import { DotIndicator } from 'react-native-indicators';
import {
  LIST_OPERATOR,
  thameColors,
  fontReguler,
} from '../base/constant/index';
import { scale } from '../Const/ScaleUtils';

const LoadingMoment = props => {
  return (
    <Modal
      isVisible={props.isVisible}
      useNativeDriver
      hideModalContentWhileAnimating
      onBackButtonPress={props.hideModal}
      style={styles.modalin}
      {...props}
    >
      <View style={styles.content}>
        <Text style={{ fontFamily: fontReguler }}>Please Wait a moment</Text>
        <DotIndicator color="orange" />
      </View>
    </Modal>
  );
};

const getFirstNameLastname = (string, callback) => {
  const fullName = string.split(' ');
  const firstName = fullName[0];
  let lastName = '';
  fullName.map((data, index) => {
    if (index > 0) {
      lastName += `${data} `;
    }
  });

  const response = {
    firstName,
    lastName,
  };

  callback(response);
};

const checkOperator = (number = '') => {
  const tempNumber = !number ? '' : number.slice(0, 4);
  const listOperator = LIST_OPERATOR;
  let selectOperator = null;
  let indexOperator = null;
  const checkNumberXL = tempNumber.match(
    /^(0817)$|(0818)$|(0819)$|(0859)$|(0877)$|(0878)$/gm
  );
  const checkNumberTelkomcel = tempNumber.match(
    /^(0811)$|(0812)$|(0813)$|(0852)$|(0853)$|(0821)$|(0822)$|(0823)$/gm
  );
  const checkNumberIndosat = tempNumber.match(
    /^(0814)$|(0815)$|(0816)$|(0855)$|(0856)$|(0857)$|(0858)$/gm
  );
  const checkNumberAxisXL = tempNumber.match(/^(0831)$|(0832)$|(0838)$/gm);
  const checkNumberSmart = tempNumber.match(
    /^(0881)$|(0882)$|(0887)$|(0888)$/gm
  );
  const checkNumberTri = tempNumber.match(/^(0896)$|(0897)$|(0898)$/gm);

  if (checkNumberTelkomcel != null) {
    indexOperator = 0;
  }
  if (checkNumberIndosat != null) {
    indexOperator = 1;
  }
  if (checkNumberXL != null) {
    indexOperator = 2;
  }
  if (checkNumberAxisXL != null) {
    indexOperator = 3;
  }
  if (checkNumberSmart != null) {
    indexOperator = 4;
  }
  if (checkNumberTri != null) {
    indexOperator = 5;
  }
  if (indexOperator != null) {
    selectOperator = listOperator[indexOperator];
  }

  return selectOperator;
};

const getRegionForCoordinates = points => {
  // points should be an array of { latitude: X, longitude: Y }
  let minX;
  let maxX;
  let minY;
  let maxY;

  // init first point
  (point => {
    minX = point.latitude;
    maxX = point.latitude;
    minY = point.longitude;
    maxY = point.longitude;
  })(points[0]);

  // calculate rect
  points.map(point => {
    minX = Math.min(minX, point.latitude);
    maxX = Math.max(maxX, point.latitude);
    minY = Math.min(minY, point.longitude);
    maxY = Math.max(maxY, point.longitude);
  });

  const midX = (minX + maxX) / 2;
  const midY = (minY + maxY) / 2;

  return {
    latitude: midX,
    longitude: midY,
    latitudeDelta: 0.01,
    longitudeDelta: 0.0075,
  };
};

const validateEmailFormat = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const validatePhoneNumber = number => {
  const phoneNumber = number.substring(0, 1);
  return phoneNumber;
};

export const starMap = totalStar => {
  const arrays = [];
  for (let i = 1; i <= totalStar; i++) {
    arrays.push(
      <Image
        key={i}
        source={require('../assets/icons/stars.png')}
        resizeMode="stretch"
        style={styles.totalStar}
      />
    );
  }
  return arrays;
};

// For In Room Hotel Beds
// How To Use ===> groupingRoomImages(images, hotelRoom, room.code);
export function groupingRoomImages(images, rooms = [], code = '') {
  let pathResult = '';
  let _images = images.sort((obj1, obj2) =>
    obj2.roomCode < obj1.roomCode ? 1 : -1
  );
  let dataGrouping = [];
  if (rooms.length > 0) {
    for (let i = 0; i < rooms.length; i++) {
      let _path = '';
      for (let j = 0; j < _images.length; j++) {
        if (rooms[i].code && _images[j].roomCode) {
          if (
            rooms[i].code.toLowerCase() === _images[j].roomCode.toLowerCase()
          ) {
            _path = _images[j].path;
            break;
          }
        }
      }
      rooms[i].path = _path;
      dataGrouping.push(rooms[i]);
    }
    dataGrouping.map(item => {
      if (code.toLowerCase() === item.code.toLowerCase()) {
        pathResult = item.path;
      }
    });
    return pathResult;
  }
}

export {
  LoadingMoment,
  getFirstNameLastname,
  checkOperator,
  getRegionForCoordinates,
  validateEmailFormat,
  validatePhoneNumber,
};

const styles = StyleSheet.create({
  modalin: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  content: {
    height: 500,
    marginLeft: 25,
    marginRight: 25,
    backgroundColor: thameColors.white,
  },
  totalStar: { width: scale(20), height: scale(20), marginRight: 5 },
  timer: {
    fontSize: 16,
    fontFamily: 'NunitoSans-Regular',
    color: thameColors.oceanBlue,
    fontWeight: '500',
  },
});
