import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import { fontReguler } from '../base/constant';

export default class LoadingMoment extends React.PureComponent {
  render() {
    return (
      <Modal
        isVisible={this.props.isVisible}
        useNativeDriver={true}
        hideModalContentWhileAnimating={true}
        style={styles.modalin}
        {...this.props}
      >
        <Text style={{ fontFamily: fontReguler }}>Welcome</Text>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalin: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
});
