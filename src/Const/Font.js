const fontExtraBold = 'NunitoSans-ExtraBold';
const fontBold = 'NunitoSans-Bold';
const fontReguler = 'NunitoSans-Regular';

export { fontExtraBold, fontBold, fontReguler };
