export const API_URL = 'http://43.231.129.14:3004/wlpsrest/api/v1/';
export const URL_PACIS = 'http://43.231.129.14:3004/';
export const TOKEN = 'FCBF1EA0008B3EB289AF6D30F297B007';

//FONT
export const fontExtraBold = 'NunitoSans-ExtraBold';
export const fontBold = 'NunitoSans-Bold';
export const fontSemiBold = 'NunitoSans-SemiBold';
export const fontReguler = 'NunitoSans-Regular';
export const fontRegulerItalic = 'NunitoSans-Italic';
//FONT

//Asita Color
export const asitaColor = {
  oceanBlue: '#0066b3',
  brownishGrey: '#6f6f6f',
  black: '#222222',
  superBlack: '#222222',
  white: '#ffffff',
  backWhite: '#F0F0F0',
  red: '#d30000',
  marineBlue: '#002561',
  brownGrey: '#969696',
  green: '#0fa031',
  berry: '#a2195b',
  brownGreyF: '#9f9f9f',
  tealBlue: '#008c9a',
  orange: '#ea6520',
  grassGreen: '#23900a',
  tangerine: '#fc9400',
  tomato: '#ee2424',
  duskyBlue: '#46608c',
  lightPink: '#fbe9ef',
  buff: '#fff59e',
  peach: '#f4b08c',
  softPink: '#f0a5be',
  lightBlueGrey: '#b4dde1',
  brownGreyTwo: '#7c7c7c',
  dustyOrange: '#e69d45',
  slateBlue: '#5d7599',
  darkPink: '#da2864',
  coralPÒink: '#fe676e',
  greyish: '#aaaaaa',
};
//Asita Color

//COLOR
export const thameColors = {
  primary: '#0090d2',
  statusBar: '#008CCC',
  deepSkyBlue: '#149bd9',
  secondary: '#a2195b',
  pink: '#decbfb',
  textBlack: '#222222',
  white: '#ffffff',
  darkGray: '#828282',
  gray: '#c1c1c1',
  semiGray: '#F8F8F8',
  buttonGray: '#dbdbdb',
  green: '#008d00',
  lightGreen: '#4CAF50',
  semiGreen: '#008195',
  darkBlue: '#002561',
  oceanBlue: '#0066b3',
  grayBlue: '#434a5e',
  yellow: '#f5e135',
  yellowFlash: '#F6D278',
  red: '#d30000',
  redVelvet: '#E98282',
  orange: '#ed6d00',
  superBack: '#000000',
  whiteOcean: '#e2f3fa',
  lightBrown: '#534e4c',
  grayBrown: '#767676',
  halfWhite: '#e6e6e6',
  backWhite: '#F0F0F0',
};
//COLOR

export const LIST_OPERATOR = [
  {
    name: 'Telkomsel',
    dataProduct: [
      { code: 'S1', name: 'TSEL 1.000', nominal: 1000, harga: 1750 },
      { code: 'S2', name: 'TSEL 2.000', nominal: 2000, harga: 2750 },
      { code: 'S3', name: 'TSEL 3.000', nominal: 3000, harga: 3750 },
      { code: 'S4', name: 'TSEL 4.000', nominal: 4000, harga: 4750 },
      { code: 'S5', name: 'TSEL 5.000', nominal: 5000, harga: 5150 },
      { code: 'S10', name: 'TSEL 10.000', nominal: 10000, harga: 10050 },
      { code: 'S20', name: 'TSEL 20.000', nominal: 20000, harga: 19825 },
      { code: 'S25', name: 'TSEL 25.000', nominal: 25000, harga: 24850 },
      { code: 'S50', name: 'TSEL 50.000', nominal: 50000, harga: 49700 },
      { code: 'S100', name: 'TSEL 100.000', nominal: 100000, harga: 97000 },
    ],
  },
  {
    name: 'Indosat',
    dataProduct: [
      { code: 'I5', name: 'INDOSAT 5.000', nominal: 5000, harga: 5975 },
      { code: 'I10', name: 'INDOSAT 10.000', nominal: 10000, harga: 11000 },
      { code: 'I12', name: 'INDOSAT 12.000', nominal: 12000, harga: 12000 },
      { code: 'I25', name: 'INDOSAT 25.000', nominal: 25000, harga: 24850 },
      { code: 'I50', name: 'INDOSAT 50.000', nominal: 50000, harga: 48800 },
      { code: 'I100', name: 'INDOSAT 100.000', nominal: 100000, harga: 96550 },
      { code: 'I150', name: 'INDOSAT 150.000', nominal: 150000, harga: 143000 },
      { code: 'I200', name: 'INDOSAT 200.000', nominal: 200000, harga: 185900 },
      { code: 'I250', name: 'INDOSAT 250.000', nominal: 250000, harga: 232000 },
      { code: 'I500', name: 'INDOSAT 500.000', nominal: 500000, harga: 464800 },
    ],
  },
  {
    code: 'XR',
    name: 'XL',
    dataProduct: [
      { code: 'XR5', name: 'XL 5.000', nominal: 5000, harga: 5525 },
      { code: 'XR10', name: 'XL 10.000', nominal: 10000, harga: 10625 },
      { code: 'XR15', name: 'XL 15.000', nominal: 15000, harga: 14775 },
      { code: 'XR25', name: 'XL 25.000', nominal: 25000, harga: 24625 },
      { code: 'XR30', name: 'XL 30.000', nominal: 30000, harga: 29600 },
      { code: 'XR50', name: 'XL 50.000', nominal: 50000, harga: 49250 },
      { code: 'XR100', name: 'XL 100.000', nominal: 100000, harga: 98500 },
    ],
  },
  {
    code: 'A',
    name: 'AXIS(XL)',
    dataProduct: [
      { code: 'A5', name: 'AXIS 5.000', nominal: 5000, harga: 5525 },
      { code: 'A10', name: 'AXIS 10.000', nominal: 10000, harga: 10625 },
      { code: 'A25', name: 'AXIS 25.000', nominal: 25000, harga: 24625 },
      { code: 'A50', name: 'AXIS 50.000', nominal: 50000, harga: 49250 },
      { code: 'A100', name: 'AXIS 100.000', nominal: 100000, harga: 98500 },
    ],
  },
  {
    code: 'Y',
    name: 'Smartfren',
    dataProduct: [
      { code: 'A5', name: 'SMARTFREN 5.000', nominal: 5000, harga: 5100 },
      { code: 'A10', name: 'SMARTFREN 10.000', nominal: 10000, harga: 10100 },
      { code: 'A20', name: 'SMARTFREN 20.000', nominal: 20000, harga: 20050 },
      { code: 'A25', name: 'SMARTFREN 25.000', nominal: 25000, harga: 25000 },
      { code: 'A50', name: 'SMARTFREN 50.000', nominal: 50000, harga: 50000 },
      { code: 'A60', name: 'SMARTFREN 60.000', nominal: 60000, harga: 60000 },
      {
        code: 'A100',
        name: 'SMARTFREN 100.000',
        nominal: 100000,
        harga: 100000,
      },
    ],
  },
  {
    code: 'T',
    name: '3/Tri',
    dataProduct: [
      { code: 'T1', name: 'Tri 1.000', nominal: 1000, harga: 1180 },
      { code: 'T2', name: 'Tri 2.000', nominal: 2000, harga: 2175 },
      { code: 'T3', name: 'Tri 3.000', nominal: 3000, harga: 3325 },
      { code: 'T4', name: 'Tri 4.000', nominal: 4000, harga: 4325 },
      { code: 'T5', name: 'Tri 5.000', nominal: 5000, harga: 5280 },
      { code: 'T10', name: 'Tri 10.000', nominal: 10000, harga: 10175 },
      { code: 'T20', name: 'Tri 20.000', nominal: 20000, harga: 19550 },
      { code: 'T25', name: 'Tri 25.000', nominal: 25000, harga: 24438 },
      { code: 'T50', name: 'Tri 50.000', nominal: 50000, harga: 48875 },
      { code: 'T100', name: 'Tri 100.000', nominal: 100000, harga: 97750 },
    ],
  },
];
