/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Image } from 'react-native';
import {
  createAppContainer,
} from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

// Screen
// import Register from '../../screens/Register'
import Home from '../../screens/Home/index';
import Order from '../../screens/Order/index';
// import Offers from '../../screens/Offers/index';
import Inbox from '../../screens/Inbox/index';

// import Account from '../../screens/Account/index'
import SearchResult from '../../screens/Flight/SearchResult';
import SearchReturnResult from '../../screens/Flight/SearchReturnResult';
import SearchFlight from '../../screens/Flight/SearchFlight';
import ETicket from '../../screens/Flight/SearchFlight/Component/E-Ticket';
import ContactDetails from '../../screens/ContactDetails/index';
import PassengerInformation from '../../screens/PassengerInformation';
import FormOrder from '../../screens/Flight/FormOrder/index';
import FormOrderTrain from '../../screens/Train/Component/FormOrderTrain';
import CheckIn from '../../screens/CheckIn/index';

import OrderDetailFlight from '../../screens/Order/OrderDetailFlight';
import OrderDetailBills from '../../screens/Order/OrderDetailBills';

//payment
import PaymentOrder from '../../screens/Payment/index';
import PaymentMethod from '../../screens/Payment/Component/PaymentMethod/Bri';
import RevampPayment from '../../screens/Payment/RevampPayment/index';
import PaymentTransfer from '../../screens/Payment/Component/PaymentTransfer';
import VirtualAccount from '../../screens/Payment/VA';
//payment

//hotel
import SearchHotel from '../../screens/Hotel';
import SearchHotelResult from '../../screens/Hotel/SearchHotelResult';
import HotelDetail from '../../screens/Hotel/HotelDetail';
import SelectRoomHotel from '../../screens/Hotel/SelectRoomHotel';
import HotelRoom from '../../screens/Hotel/HotelRoom';
import FormOrderHotel from '../../screens/Hotel/FormOrderHotel';
//hotel

import GroupBooking from '../../screens/GroupBooking/index';
import GroupBookingResult from '../../screens/GroupBookingResult/index';
import Chatbot from '../../screens/Chatbot/index';

//cargo
import Cargo from '../../screens/Cargo/index';
import CargoDestination from '../../screens/Cargo/Destination';
import CargoPrice from '../../screens/Cargo/CargoPrice';
import OrderCargo from '../../screens/Cargo/Orders';
import CargoPriceAgree from '../../screens/Cargo/CargoPriceAgree';
// import CargoDetail from '../../screens/Cargo/CargoDetail'
// import ReviewCargo from '../../screens/Cargo/Component/ReviewCargo'
// import CargoBook from '../../screens/Cargo/CargoBook'
//cargo

import Holiday from '../../screens/Holiday';
import Splash from '../../screens/Splash/index';
import Profile from '../../screens/Profile/index';
import CreditCard from '../../screens/Payment/CreditCard';
import FormProfile from '../../screens/Profile/Component/User/Form';
import Barcode from '../../screens/Profile/Component/User/Barcode';
import MenuProfile from '../../screens/Profile/Component/User/MenuProfile';

//train
import Train from '../../screens/Train/index';
import SearchTrain from '../../screens/Train/Component/SearchTrain';
import SearchTrainReturn from '../../screens/Train/Component/ReturnTrain';
import IssuedTicket from '../../screens/Train/Component/IssuedTicket';
import BookingTrain from '../../screens/Train/Component/BookingDetails';
import ETicketRevamp from '../../screens/Train/Component/ETicket/ETicketRevamp';
//train

//car rental
import CarRental from '../../screens/CarRental';
import CarRentalResult from '../../screens/CarRental/CarRentalResult';
import CarRentalBook from '../../screens/CarRental/CarRentalBook';
//car rental

import Notifications from '../../screens/Notifications';
import Promo from '../../screens/Promo';

//user Account
import LoginPage from '../../screens/SignIn/index';
import MyAccount from '../../screens/SignIn/MyAccount/MyAccount';
import EditProfile from '../../screens/SignIn/MyAccount/EditProfile';
import CardUser from '../../screens/SignIn/MyAccount/CardUser';
import SignUp from '../../screens/SignUp';
import More from '../../screens/More/index';
import SignUpVerify from '../../screens/SignUp/SignUpVerify';
import SignUpForm from '../../screens/SignUp/SignUpForm';
import Language from '../../screens/SignIn/MyAccount/Language';
import ForgotPassword from '../../screens/SignIn/ForgotPassword/ForgotPassword';
import VerifyForgot from '../../screens/SignIn/ForgotPassword/VerifyForgot';
import ChangeForgot from '../../screens/SignIn/ForgotPassword/ChangeForgot';
import ChangePassword from '../../screens/SignIn/MyAccount/ChangePassword';
///user account

//pulsa
import Pulsa from '../../screens/Pulsa';
import BookingPulsa from '../../screens/Pulsa/Component/BookingPulsa';
import BookingPulsaSuccess from '../../screens/Pulsa/Component/BookingPulsaSuccess';
//pulsa

//internet
import DataInternet from '../../screens/DataInternet';
import BookingInternet from '../../screens/DataInternet/Component/BookingInternet';
//internet

//e-money
import EMoney from '../../screens/EMoney/index';
import BookingEmoney from '../../screens/EMoney/Component/BookingEmoney';
//e-money

import BookingSummary from '../../components/BookingSummary';
// Screen

//AppInfo
import About from '../../screens/AppInfo/About';
import Terms from '../../screens/AppInfo/Terms';
import Privacy from '../../screens/AppInfo/Privacy';
import LoginModal from '../../components/LoginModal';
//AppInfo

//Attraction
import Attraction from '../../screens/Attraction';
import AttractionActivities from '../../screens/Attraction/Activities';
import AttractionEvents from '../../screens/Attraction/Events';
import AttractionMovies from '../../screens/Attraction/Movies';
import DetailActivities from '../../screens/Attraction/Activities/DetailActivities';
import DetailEvents from '../../screens/Attraction/Events/DetailEvents';
import OrderAttractionActivities from '../../screens/Attraction/OrderAttraction/OrderAttractionActivities';
//Attraction

// Bills
import Pdam from '../../screens/Pdam/SearchPdam';
import DetailPdam from '../../screens/Pdam/DetailPdam';
import Pln from '../../screens/Pln/SearchPln';
import DetailPln from '../../screens/Pln/DetailPln';
// Bills

import OrderDetails from '../../screens/Order/OrderDetail';
import CargoTracking from '../../screens/Order/CargoTracking';
// import EticketTrain from '../../screens/Train/Component/ETicket/ETicket';
import { asitaColor } from '../constant';

const TABS = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      path: 'home',
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused }) =>
          focused ? (
            <Image
              source={require('../../assets/icons/home-active.png')}
              style={{
                width: 25,
                height: 25,
                resizeMode: 'center',
                tintColor: asitaColor.oceanBlue,
              }}
            />
          ) : (
              <Image
                source={require('../../assets/icons/home.png')}
                style={{ width: 25, height: 25, resizeMode: 'center' }}
              />
            ),
      },
    },
    Order: {
      screen: Order,
      path: 'order',
      navigationOptions: {
        tabBarLabel: 'Order',
        tabBarIcon: ({ focused }) =>
          focused ? (
            <Image
              source={require('../../assets/icons/garuda-mall-active.png')}
              style={{
                width: 25,
                height: 25,
                resizeMode: 'center',
                tintColor: asitaColor.oceanBlue,
              }}
            />
          ) : (
              <Image
                source={require('../../assets/icons/garuda-mall.png')}
                style={{ width: 25, height: 25, resizeMode: 'center' }}
              />
            ),
      },
    },
    Inbox: {
      screen: Inbox,
      path: 'inbox',
      navigationOptions: {
        tabBarLabel: 'Inbox',
        tabBarIcon: ({ focused }) =>
          focused ? (
            <Image
              source={require('../../assets/icons/inbox-active-new.png')}
              style={{ width: 25, height: 25, resizeMode: 'center' }}
            />
          ) : (
              <Image
                source={require('../../assets/icons/inbox.png')}
                style={{ width: 25, height: 25, resizeMode: 'center' }}
              />
            ),
      },
    },
    LoginPage: {
      screen: LoginPage,
      path: 'loginpage',
      navigationOptions: {
        tabBarLabel: 'Account',
        tabBarIcon: ({ focused }) =>
          focused ? (
            <Image
              source={require('../../assets/icons/account-active-new.png')}
              style={{ width: 25, height: 25, resizeMode: 'center' }}
            />
          ) : (
              <Image
                source={require('../../assets/icons/account.png')}
                style={{ width: 25, height: 25, resizeMode: 'center' }}
              />
            ),
      },
    },
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: asitaColor.oceanBlue,
      inactiveTintColor: asitaColor.black,
      labelStyle: {
        letterSpacing: 0.7,
        fontSize: 12,
        fontWeight: '400',
        paddingTop: 5,
      },
      style: {
        backgroundColor: asitaColor.white,
        paddingTop: 5,
      },
      tabStyle: {
        margin: 0,
        padding: 0,
      },
    },
  }
);

const STACK = createStackNavigator(
  {
    // Register: {
    //     screen: Register,
    //     navigationOptions: {
    //         header: null
    //     }
    // },
    Tab: {
      screen: TABS,
      navigationOptions: {
        header: null,
      },
    },
    SearchFlight: {
      screen: SearchFlight,
      navigationOptions: {
        header: null,
      },
    },
    SearchResult: {
      screen: SearchResult,
      navigationOptions: {
        header: null,
      },
    },
    SearchReturnResult: {
      screen: SearchReturnResult,
      navigationOptions: {
        header: null,
      },
    },
    ETicket: {
      screen: ETicket,
      navigationOptions: {
        header: null,
      },
    },
    ContactDetails: {
      screen: ContactDetails,
      navigationOptions: {
        header: null,
      },
    },
    PassengerInformation: {
      screen: PassengerInformation,
      navigationOptions: {
        header: null,
      },
    },
    FormOrder: {
      screen: FormOrder,
      navigationOptions: {
        header: null,
      },
    },
    FormOrderTrain: {
      screen: FormOrderTrain,
      navigationOptions: {
        header: null,
      },
    },
    PaymentOrder: {
      screen: PaymentOrder,
      navigationOptions: {
        header: null,
      },
    },
    PaymentMethod: {
      screen: PaymentMethod,
      navigationOptions: {
        header: null,
      },
    },
    RevampPayment: {
      screen: RevampPayment,
      navigationOptions: {
        header: null,
      },
    },
    PaymentTransfer: {
      screen: PaymentTransfer,
      navigationOptions: {
        header: null,
      },
    },
    CreditCard: {
      screen: CreditCard,
      navigationOptions: {
        header: null,
      },
    },
    VirtualAccount: {
      screen: VirtualAccount,
      navigationOptions: {
        header: null,
      },
    },
    CheckIn: {
      screen: CheckIn,
      navigationOptions: {
        header: null,
      },
    },

    SearchHotel: {
      screen: SearchHotel,
      navigationOptions: {
        header: null,
      },
    },
    SearchHotelResult: {
      screen: SearchHotelResult,
      navigationOptions: {
        header: null,
      },
    },
    HotelDetail: {
      screen: HotelDetail,
      navigationOptions: {
        header: null,
      },
    },
    FormOrderHotel: {
      screen: FormOrderHotel,
      navigationOptions: {
        header: null,
      },
    },
    SelectRoomHotel: {
      screen: SelectRoomHotel,
      navigationOptions: {
        header: null,
      },
    },
    HotelRoom: {
      screen: HotelRoom,
      navigationOptions: {
        header: null,
      },
    },
    GroupBooking: {
      screen: GroupBooking,
      navigationOptions: {
        header: null,
      },
    },
    GroupBookingResult: {
      screen: GroupBookingResult,
      navigationOptions: {
        header: null,
      },
    },
    Chatbot: {
      screen: Chatbot,
      navigationOptions: {
        header: null,
      },
    },
    Cargo: {
      screen: Cargo,
      navigationOptions: {
        header: null,
      },
    },
    CargoDestination: {
      screen: CargoDestination,
      navigationOptions: {
        header: null,
      },
    },
    CargoPrice: {
      screen: CargoPrice,
      navigationOptions: {
        header: null,
      },
    },
    OrderCargo: {
      screen: OrderCargo,
      navigationOptions: {
        header: null,
      },
    },
    OrderDetails: {
      screen: OrderDetails,
      navigationOptions: {
        header: null,
      },
    },
    OrderDetailFlight: {
      screen: OrderDetailFlight,
      navigationOptions: {
        header: null,
      },
    },
    OrderDetailBills: {
      screen: OrderDetailBills,
      navigationOptions: {
        header: null,
      },
    },
    CargoTracking: {
      screen: CargoTracking,
      navigationOptions: {
        header: null,
      },
    },
    CargoPriceAgree: {
      screen: CargoPriceAgree,
      navigationOptions: {
        header: null,
      },
    },
    Holiday: {
      screen: Holiday,
      navigationOptions: {
        header: null,
      },
    },
    Splash: {
      screen: Splash,
      navigationOptions: {
        header: null,
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        header: null,
      },
    },
    FormProfile: {
      screen: FormProfile,
      navigationOptions: {
        header: null,
      },
    },
    EditProfile: {
      screen: EditProfile,
      navigationOptions: {
        header: null,
      },
    },
    Language: {
      screen: Language,
      navigationOptions: {
        header: null,
      },
    },
    CarRental: {
      screen: CarRental,
      navigationOptions: {
        header: null,
      },
    },
    CarRentalResult: {
      screen: CarRentalResult,
      navigationOptions: {
        header: null,
      },
    },
    Barcode: {
      screen: Barcode,
      navigationOptions: {
        header: null,
      },
    },
    CarRentalBook: {
      screen: CarRentalBook,
      navigationOptions: {
        header: null,
      },
    },
    MenuProfile: {
      screen: MenuProfile,
      navigationOptions: {
        header: null,
      },
    },
    Notifications: {
      screen: Notifications,
      navigationOptions: {
        header: null,
      },
    },
    Train: {
      screen: Train,
      navigationOptions: {
        header: null,
      },
    },
    Promo: {
      screen: Promo,
      navigationOptions: {
        header: null,
      },
    },
    SearchTrain: {
      screen: SearchTrain,
      navigationOptions: {
        header: null,
      },
    },
    SearchTrainReturn: {
      screen: SearchTrainReturn,
      navigationOptions: {
        header: null,
      },
    },
    IssuedTicket: {
      screen: IssuedTicket,
      navigationOptions: {
        header: null,
      },
    },
    ETicketRevamp: {
      screen: ETicketRevamp,
      navigationOptions: {
        header: null,
      },
    },
    BookingTrain: {
      screen: BookingTrain,
      navigationOptions: {
        header: null,
      },
    },
    LoginPage: {
      screen: LoginPage,
      navigationOptions: {
        header: null,
      },
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        header: null,
      },
    },
    MyAccount: {
      screen: MyAccount,
      navigationOptions: {
        header: null,
      },
    },
    CardUser: {
      screen: CardUser,
      navigationOptions: {
        header: null,
      },
    },
    SignUpVerify: {
      screen: SignUpVerify,
      navigationOptions: {
        header: null,
      },
    },
    SignUpForm: {
      screen: SignUpForm,
      navigationOptions: {
        header: null,
      },
    },
    More: {
      screen: More,
      navigationOptions: {
        header: null,
      },
    },
    Pulsa: {
      screen: Pulsa,
      navigationOptions: {
        header: null,
      },
    },
    BookingPulsa: {
      screen: BookingPulsa,
      navigationOptions: {
        header: null,
      },
    },
    BookingPulsaSuccess: {
      screen: BookingPulsaSuccess,
      navigationOptions: {
        header: null,
      },
    },
    DataInternet: {
      screen: DataInternet,
      navigationOptions: {
        header: null,
      },
    },
    EMoney: {
      screen: EMoney,
      navigationOptions: {
        header: null,
      },
    },
    BookingEmoney: {
      screen: BookingEmoney,
      navigationOptions: {
        header: null,
      },
    },
    BookingSummary: {
      screen: BookingSummary,
      navigationOptions: {
        header: null,
      },
    },
    BookingInternet: {
      screen: BookingInternet,
      navigationOptions: {
        header: null,
      },
    },
    About: {
      screen: About,
      navigationOptions: {
        header: null,
      },
    },
    Terms: {
      screen: Terms,
      navigationOptions: {
        header: null,
      },
    },
    Privacy: {
      screen: Privacy,
      navigationOptions: {
        header: null,
      },
    },
    LoginModal: {
      screen: LoginModal,
      navigationOptions: {
        header: null,
      },
    },
    Attraction: {
      screen: Attraction,
      navigationOptions: {
        header: null,
      },
    },
    AttractionActivities: {
      screen: AttractionActivities,
      navigationOptions: {
        header: null,
      },
    },
    AttractionEvents: {
      screen: AttractionEvents,
      navigationOptions: {
        header: null,
      },
    },
    AttractionMovies: {
      screen: AttractionMovies,
      navigationOptions: {
        header: null,
      },
    },
    DetailActivities: {
      screen: DetailActivities,
      navigationOptions: {
        header: null,
      },
    },
    DetailEvents: {
      screen: DetailEvents,
      navigationOptions: {
        header: null,
      },
    },
    OrderAttractionActivities: {
      screen: OrderAttractionActivities,
      navigationOptions: {
        header: null,
      },
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        header: null,
      },
    },
    VerifyForgot: {
      screen: VerifyForgot,
      navigationOptions: {
        header: null,
      },
    },
    ChangeForgot: {
      screen: ChangeForgot,
      navigationOptions: {
        header: null,
      },
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: {
        header: null,
      },
    },
    Pdam: {
      screen: Pdam,
      navigationOptions: {
        header: null,
      },
    },
    DetailPdam: {
      screen: DetailPdam,
      navigationOptions: {
        header: null,
      },
    },
    Pln: {
      screen: Pln,
      navigationOptions: {
        header: null,
      },
    },
    DetailPln: {
      screen: DetailPln,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    // initialRouteName: 'OrderDetailBills',
    initialRouteName: 'Splash',
  }
);

export default createAppContainer(STACK);
