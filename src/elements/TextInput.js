import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import { Grid, Col } from 'react-native-easy-grid';
import {
  fontReguler,
  fontBold,
  fontExtraBold,
  thameColors,
  asitaColor,
} from '../base/constant/index';
import { Icon } from 'react-native-elements';

let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const InputCargo = props => {
  return (
    <View>
      <TextInput
        testID={props.testID}
        style={styles.inputCargo}
        keyboardType={props.keyboardType ? props.keyboardType : 'default'}
        maxLength={props.maxLength ? props.maxLength : 100}
        placeholder={props.placeholder}
        placeholderTextColor={thameColors.darkGray}
        value={props.value ? props.value : null}
        editable={props.editable}
        onChangeText={text => {
          props.onChangeText(text);
        }}
      />
      <View style={{ position: 'absolute', top: 12.5, left: 10 }}>
        {props.icon == 'destination' ? (
          <Image
            resizeMode="center"
            style={{ width: 25, height: 25, tintColor: thameColors.secondary }}
            source={require('../assets/cargo/cargo_destination.png')}
          />
        ) : (
          <Image
            resizeMode="center"
            style={{ width: 25, height: 25, tintColor: thameColors.primary }}
            source={require('../assets/cargo/cargo_origin.png')}
          />
        )}
      </View>
    </View>
  );
};

const InputText = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>
        {props.label}
        {props.required ? (
          <Text style={{ color: 'red', fontWeight: 'bold' }}>
            {' '}
            {props.required}
          </Text>
        ) : (
          ''
        )}
      </Text>
      <TextInput
        testID={props.testID}
        style={[styles.input, props.style]}
        {...props}
        style={styles.input}
        keyboardType={props.keyboardType ? props.keyboardType : 'default'}
        maxLength={props.maxLength ? props.maxLength : 100}
        placeholder={props.placeholder}
        placeholderTextColor={thameColors.superBack}
        value={props.value ? props.value : null}
        editable={props.editable}
        onChangeText={text => {
          props.onChangeText(text);
        }}
      />
    </View>
  );
};

const InputPassword = props => {
  return (
    <View style={[styles.container, { marginTop: 15, height: 50 }]}>
      <Grid style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Col size={10}>
          <TextInput
            testID={props.testID}
            style={[styles.input, props.style, { width: '100%' }]}
            {...props}
            style={styles.inputLogin}
            placeholder={props.placeholder}
            placeholderTextColor={thameColors.darkGray}
            value={props.value ? props.value : null}
            editable={props.editable}
            onChangeText={props.onChangeText}
            secureTextEntry={props.showPassword}
            autoCapitalize="none"
          />
        </Col>
        <TouchableOpacity
          onPress={props.onShowPassword}
          style={{
            position: 'absolute',
            right: 15,
            width: 35,
            height: 35,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Icon
            type="ionicon"
            color={thameColors.darkGray}
            name={props.showPassword ? 'ios-eye-off' : 'ios-eye'}
          />
        </TouchableOpacity>
      </Grid>
    </View>
  );
};

const InputTextRevamp = props => {
  return (
    <Grid style={[styles.cardInput, props.style]} onPress={props.onPress}>
      <Col
        size={1.5}
        style={{ justifyContent: 'center', alignItems: 'center' }}
      >
        <Image
          source={props.source}
          style={{ width: 30, height: 35, tintColor: asitaColor.orange }}
          resizeMode="center"
        />
      </Col>
      <Col size={0.5} />
      <Col
        size={8}
        style={{ justifyContent: 'center', alignItems: 'flex-start' }}
      >
        <View>
          <Text style={styles.textLabel}>{props.label}</Text>
        </View>
        {props.contentInput}
        <View>
          <Text style={styles.textBold}>{props.editable}</Text>
        </View>
      </Col>
    </Grid>
  );
};

export const InputRevampCenter = props => {
  return (
    <Grid
      style={[
        styles.cardInput,
        { justifyContent: 'center', alignItems: 'center' },
        props.style,
      ]}
      onPress={props.onPress}
      {...props}
    >
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}
      >
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text style={styles.textLabel}>{props.label}</Text>
          <Text style={styles.textBold}>{props.editable}</Text>
        </View>
        <View style={{ alignSelf: 'flex-start' }}>{props.rightItem}</View>
      </View>
    </Grid>
  );
};

const SearchHotelInput = props => {
  return (
    <View style={styles.container}>
      <TextInput
        testID={props.testID}
        {...props}
        style={styles.inputHotel}
        placeholder={props.placeholder}
        editable={props.editable}
        onChangeText={text => {
          props.onChangeText(text);
        }}
      />
    </View>
  );
};

// const SearchInput = (props) => {
//     return (
//         <View style={styles.container}>
//             <TextInput
//                 style={styles.inputSearch}
//                 placeholder={props.placeholder}
//                 placeholderTextColor={thameColors.darkGray}
//                 onChangeText={(text) => {
//                     props.onChangeText(text)
//                 }}
//             />
//         </View>
//       )
// }

const SearchInput = props => {
  return (
    <View style={styles.container}>
      <View style={[styles.inputSearchFlight, { flexDirection: 'row' }]}>
        <Image
          source={require('../assets/icons/map_3x.png')}
          style={{
            tintColor: asitaColor.orange,
            marginLeft: 15,
            marginTop: 12,
            padding: 10,
            height: 30,
            width: 25,
            justifyContent: 'center',
            resizeMode: 'center',
            alignItems: 'center',
          }}
        />
        <TextInput
          {...props}
          testID={props.testID}
          style={{ flex: 1, marginLeft: 10, fontFamily: fontReguler }}
          placeholder={props.placeholder}
          placeholderTextColor={thameColors.darkGray}
          onChangeText={props.onChangeText}
          onSubmitEditing={props.onSubmitEditing}
        />
      </View>
    </View>
  );
};

export const SearchText = props => {
  return (
    <View style={styles.container}>
      <View
        style={[
          styles.inputSearchFlight,
          { flexDirection: 'row', alignItems: 'center' },
        ]}
      >
        <Icon type="feather" name="search" size={25} color={thameColors.gray} />
        <TextInput
          {...props}
          testID={props.testID}
          style={{ flex: 1, marginLeft: 10, fontFamily: fontReguler }}
          placeholder={props.placeholder}
          placeholderTextColor={thameColors.darkGray}
          onChangeText={text => {
            props.onChangeText(text);
          }}
          onSubmitEditing={props.onSubmitEditing}
        />
      </View>
    </View>
  );
};

const AddressInput = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      <TextInput
        testID={props.testID}
        style={[styles.inputAddress, props.style]}
        {...props}
        style={styles.inputAddress}
        placeholder={props.placeholder}
        // placeholderTextColor="black"
        editable={props.editable}
        onChangeText={text => {
          props.onChangeText(text);
        }}
      />
    </View>
  );
};

const InputLogin = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      <TextInput
        testID={props.testID}
        style={[styles.input, props.style]}
        {...props}
        style={styles.inputLogin}
        placeholder={props.placeholder}
        placeholderTextColor={thameColors.darkGray}
        value={props.value ? props.value : null}
        editable={props.editable}
        onChangeText={text => {
          props.onChangeText(text);
        }}
      />
    </View>
  );
};

const InputCircle = props => {
  return (
    <View style={styles.container}>
      <TextInput
        testID={props.testID}
        {...props}
        placeholder={props.placeholder}
        placeholderTextColor={
          props.placeholderTextColor
            ? props.placeholderTextColor
            : thameColors.darkGray
        }
        onChangeText={text => props.onChangeText(text)}
        style={[
          styles.inputHotel,
          {
            borderRadius: 25,
            height: 40,
            fontFamily: fontReguler,
            paddingLeft: 25,
          },
        ]}
      />
    </View>
  );
};

const InputDate = props => {
  const { container } = styles;
  return (
    <View style={container}>
      <Text style={styles.label}>
        {props.label}
        {props.required ? (
          <Text style={{ color: 'red', fontWeight: 'bold' }}>
            {' '}
            {props.required}
          </Text>
        ) : (
          ''
        )}
      </Text>
      <TextInputMask
        editable={props.editable}
        testID={props.testID}
        type={'custom'}
        options={{ mask: '9999-99-99', format: 'YYYY-MM-DD' }}
        placeholder={props.placeholder}
        placeholderTextColor={thameColors.superBack}
        value={props.value ? props.value : null}
        onChangeText={text => {
          props.onChangeText(text);
        }}
        style={[styles.input, props.style]}
        maxLength={10}
        keyboardType={'number-pad'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  label: {
    color: thameColors.darkGray,
    paddingBottom: 5,
    fontFamily: fontReguler,
  },
  inputCargo: {
    color: thameColors.superBack,
    paddingLeft: 50,
    paddingRight: 10,
    height: 50,
    fontSize: 16,
    fontFamily: fontBold,
    backgroundColor: thameColors.white,
    borderColor: thameColors.halfWhite,
    borderWidth: 0.5,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  input: {
    color: thameColors.superBack,
    paddingLeft: 20,
    paddingRight: 10,
    height: 40,
    backgroundColor: thameColors.white,
    borderColor: thameColors.halfWhite,
    borderWidth: 0.5,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  inputHotel: {
    color: thameColors.superBack,
    paddingLeft: 20,
    paddingRight: 10,
    height: 60,
    fontSize: 16,
    backgroundColor: thameColors.white,
  },
  inputSearch: {
    color: thameColors.darkGray,
    paddingLeft: 20,
    paddingRight: 10,
    backgroundColor: thameColors.white,
    borderColor: thameColors.halfWhite,
    borderWidth: 0.3,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  inputSearchFlight: {
    height: 60,
    color: thameColors.darkGray,
    paddingLeft: 20,
    paddingRight: 10,
    backgroundColor: thameColors.white,
    borderColor: thameColors.white,
    borderWidth: 0.3,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  inputAddress: {
    color: thameColors.superBack,
    paddingLeft: 20,
    paddingRight: 10,
    height: 120,
    backgroundColor: thameColors.white,
    borderColor: thameColors.halfWhite,
    borderWidth: 0.5,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  inputLogin: {
    color: thameColors.superBack,
    paddingLeft: 20,
    paddingRight: 10,
    fontSize: 16,
    backgroundColor: thameColors.white,
    borderColor: thameColors.halfWhite,
    borderWidth: 0.5,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  cardInput: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: thameColors.white,
    borderRadius: 5,
    // height: deviceHeight / 10.5,
  },
  textLabel: {
    fontFamily: fontReguler,
    color: thameColors.darkGray,
    fontSize: 16,
    paddingBottom: 5,
  },
  textBold: {
    fontFamily: fontExtraBold,
    color: thameColors.textBlack,
    fontSize: 16,
    paddingBottom: 4,
  },
  textInitial: {
    fontFamily: fontBold,
    color: thameColors.textBlack,
    fontSize: 14,
  },
});

export {
  InputText,
  SearchInput,
  AddressInput,
  SearchHotelInput,
  InputLogin,
  InputTextRevamp,
  InputCargo,
  InputCircle,
  InputDate,
  InputPassword,
};
