import React from 'react';
import { StatusBar } from 'react-native';
import { thameColors } from '../base/constant';

const BarTranslucent = props => {
  return (
    <StatusBar
      translucent={true}
      barStyle={'dark-content'}
      animated={true}
      backgroundColor={thameColors.white}
    />
  );
};

export default BarTranslucent;
