import React from 'react';
import { View, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';
import { thameColors, asitaColor } from '../base/constant';
var deviceWidth = Dimensions.get('window').width;

const ButtonPlus = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.sectionButtonColor}
        onPress={props.onClick}
      >
        <Icon
          type="ionicon"
          size={22}
          name="ios-arrow-forward"
          color={thameColors.white}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { width: deviceWidth / 8 },
  sectionButtonColor: {
    backgroundColor: asitaColor.tealBlue,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    width: 35,
    height: 35,
    borderRadius: 20,
  },
});

export { ButtonPlus };
