import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import {
  fontExtraBold,
  fontReguler,
  thameColors,
  fontBold,
  asitaColor,
} from '../base/constant/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/FontAwesome';

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const Button = props => {
  if (props.isActive == false) {
    return (
      <View
        style={
          props.size
            ? props.size == 'small'
              ? styles.containerSmall
              : styles.container
            : styles.container
        }
        {...props}
      >
        <TouchableOpacity
          style={styles.sectionButtonNonActive}
          onPress={props.onClick}
        >
          <Text style={styles.buttonLabel}>{props.label}</Text>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View
        style={
          props.size
            ? props.size == 'small'
              ? styles.containerSmall
              : styles.container
            : styles.container
        }
        {...props}
      >
        <TouchableOpacity
          style={
            props.colorButton
              ? props.colorButton == 'buttonColor'
                ? styles.sectionButtonColor
                : styles.sectionButton
              : styles.sectionButton
          }
          onPress={props.onClick}
        >
          <Text style={styles.buttonLabel}>{props.label}</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

const ButtonRounded = props => {
  if (props.isActive == false) {
    return (
      <TouchableOpacity onPress={props.onClick} style={props.buttonStyle}>
        <View
          style={[
            props.size
              ? props.size === 'small'
                ? styles.roundedButtonSmall
                : styles.buttonRoundNonactive
              : styles.buttonRoundNonactive,
            props.style,
          ]}
          {...props}
        >
          {props.children}
          <Text style={styles.roundedLabel}>{props.label}</Text>
        </View>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity onPress={props.onClick}>
        <View
          style={[
            props.size
              ? props.size == 'small'
                ? styles.roundedButtonSmall
                : styles.roundedButton
              : styles.roundedButton,
            props.buttonStyle,
          ]}
          {...props}
        >
          <Text style={styles.roundedLabel}>{props.label}</Text>
        </View>
      </TouchableOpacity>
    );
  }
};

const ButtonRoundedNonActive = props => {
  return (
    <TouchableOpacity onPress={props.onClick}>
      <View
        style={
          props.size
            ? props.size == 'small'
              ? styles.buttonRoundNonactiveSmall
              : styles.buttonRoundNonactive
            : styles.buttonRoundNonactive
        }
        {...props}
      >
        <Text style={styles.roundedLabelNonactive}>{props.label}</Text>
      </View>
    </TouchableOpacity>
  );
};

const ButtonRoundedPrice = props => {
  return (
    <TouchableOpacity onPress={props.onClick}>
      <Grid style={styles.roundedButtonPrice}>
        <Col size={5} style={{ justifyContent: 'center' }}>
          <Text style={styles.roundedLabel}>SHIPING COST</Text>
        </Col>
        <Col
          size={3.5}
          style={{ alignItems: 'center', justifyContent: 'center' }}
        >
          <Text style={[styles.roundedLabel, { fontSize: 18 }]}>
            Rp{props.cost}
          </Text>
        </Col>
        <Col
          size={1.5}
          style={{ alignItems: 'flex-end', justifyContent: 'center' }}
        >
          <Icon name="arrow-circle-right" size={26} color={thameColors.white} />
        </Col>
      </Grid>
    </TouchableOpacity>
  );
};

const ButtonPrice = props => {
  <View style={styles.roundedButton} {...props}>
    <TouchableOpacity onPress={props.onClick}>
      <Col
        size={5}
        style={{ justifyContent: 'center', alignItems: 'flex-start' }}
      >
        <Text style={styles.roundedLabelNonactive}>{props.label}</Text>
      </Col>
      <Col size={4} style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Text style={styles.roundedLabelNonactive}>{props.price}</Text>
      </Col>
      {/* <Col size={1} style={{justifyContent:'center', alignItems: 'flex-start'}}>
                <Icon name={props.name} type={props.type} size={props.size} color={props.color}/>
            </Col> */}
    </TouchableOpacity>
  </View>;
};

const ButtonLogin = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        marginTop: 20,
        backgroundColor: asitaColor.lightBlueGrey,
        padding: 10,
        borderRadius: 5,
      }}
    >
      <Row>
        <Col
          size={3}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <Image
            source={require('../assets/icons/button_login.png')}
            style={{ width: 50, height: 50, tintColor: asitaColor.tealBlue }}
            resizeMode="stretch"
          />
        </Col>
        <Col size={7}>
          <Text style={{ color: asitaColor.black, fontFamily: fontReguler }}>
            <Text
              style={{
                color: asitaColor.tealBlue,
                fontFamily: fontBold,
                fontSize: 16,
              }}
            >
              Log In{' '}
            </Text>
            or
            <Text
              style={{
                fontSize: 16,
                color: asitaColor.tealBlue,
                fontFamily: fontBold,
              }}
            >
              {' '}
              Register{' '}
            </Text>
            to enjoy this member-only benefit
          </Text>
        </Col>
      </Row>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  roundedLabel: {
    color: thameColors.white,
    fontSize: 16,
    fontFamily: fontExtraBold,
  },
  roundedButton: {
    width: deviceWidth - 40,
    backgroundColor: asitaColor.tealBlue,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  roundedButtonPrice: {
    width: deviceWidth - 60,
    marginBottom: 10,
    padding: 10,
    backgroundColor: asitaColor.tealBlue,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  roundedButtonSmall: {
    width: deviceWidth - 80,
    backgroundColor: asitaColor.tealBlue,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerSmall: {
    paddingLeft: 0,
    width: deviceWidth / 4,
    paddingRight: 8,
  },
  container: { paddingLeft: 20, paddingRight: 20, width: deviceWidth },
  sectionButtonColor: {
    backgroundColor: thameColors.lightGreen,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 6,
  },
  sectionButtonColorRounded: {
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 20,
  },
  sectionButton: {
    backgroundColor: asitaColor.tealBlue,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 6,
  },
  sectionButtonRounded: {
    backgroundColor: asitaColor.tealBlue,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 20,
  },
  sectionButtonNonActive: {
    backgroundColor: thameColors.gray,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 6,
  },
  buttonLabel: {
    color: thameColors.white,
    fontSize: 16,
    fontFamily: fontExtraBold,
  },
  buttonRoundedNonactive: {
    width: deviceWidth - 40,
    backgroundColor: thameColors.semiGray,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonRoundNonactiveSmall: {
    width: deviceWidth - 80,
    backgroundColor: thameColors.semiGray,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 20,
  },
  buttonRoundNonactive: {
    width: deviceWidth - 40,
    backgroundColor: thameColors.gray,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 20,
  },
  roundedLabelNonactive: {
    color: asitaColor.tealBlue,
    fontSize: 16,
    fontFamily: fontExtraBold,
  },
});

export {
  Button,
  ButtonRounded,
  ButtonRoundedPrice,
  ButtonRoundedNonActive,
  ButtonPrice,
  ButtonLogin,
};
