import React from 'react';
import { StatusBar, Platform } from 'react-native';
import { thameColors, asitaColor } from '../base/constant';

const BarStyle = props => {
  if (props.home == true) {
    return (
      <StatusBar barStyle={'dark-content'} backgroundColor={asitaColor.white} />
    );
  } else {
    return (
      <StatusBar
        barStyle={'light-content'}
        backgroundColor={asitaColor.marineBlue}
        translucent={false}
      />
    );
  }
};

const statusHeight = Platform.OS === 'ios' ? 80 : StatusBar.currentHeight;

export { BarStyle, statusHeight };
