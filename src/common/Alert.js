import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import Modal from 'react-native-modalbox';
import { Button } from 'react-native-elements';
import { moderateScale } from '../Const/ScaleUtils';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

const contentImage = {
  warning: require('../assets/chat/assets/alert_warning.png'),
  incomplete: require('../assets/chat/assets/alert_incomplete.png'),
  success: require('../assets/chat/assets/alert_success.png'),
  email: require('../assets/chat/assets/alert_email.png'),
  confirm: require('../assets/chat/assets/alert_confirm.png'),
};
const gradient_color = {
  yellow: ['#F6B101', '#EF8508'],
  warning: ['#F27D38', '#E36012'],
  successorcfm: ['#006D8C', '#006582'],
  red: ['#BE3234', '#7B1011'],
  green: ['#237C6B', '#04392F'],
  blue: ['#1348B3', '#061943'],
  bni: ['#005F7A', '#004357'],
  garuda: ['#0090d2', '#0090d2'],
  shadeyellow: ['#ffffff', '#F6B101'],
};

const btn_color = {
  fail: '#BE3234',
  warning: '#F27D38',
  successorcfm: '#006D8C',
  default: '#2B3A42',
};
class AlertComponent extends Component {
  constructor() {
    super();
  }
  componentDidMount() {
    // console.log(this.props)
  }
  callback(itemCallback) {
    dispatch.alert.hide();
    itemCallback();
  }
  close = () => {
    dispatch.alert.hide();
    setTimeout(() => {
      dispatch.alert.reset();
    }, 1000);
  };
  render() {
    const {
      color,
      btncolor,
      buttons,
      title,
      image,
      message,
      isOpen,
      featureModal,
      list,
    } = this.props.alert;

    return (
      <Modal
        style={[styles.modal]}
        isOpen={isOpen}
        position={'center'}
        swipeToClose={false}
        backdropPressToClose={false}
        backdropOpacity={0.4}
      >
        <View
          style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}
        >
          <View style={styles.imageBackground}>
            <View style={styles.imageContainer}>
              <Image
                source={contentImage[image]}
                resizeMode="contain"
                style={styles.image}
              />
            </View>
          </View>
        </View>
        <View style={{ flex: 0.5, width: deviceWidth / 1.3 }}>
          <ScrollView style={styles.messageContainer} bounces={false}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.message}>{message}</Text>
          </ScrollView>
        </View>
        <View style={styles.buttons}>
          {buttons.map((item, index) => (
            <Button
              key={index}
              onPress={this.callback.bind(this, item.callback)}
              title={item.title}
              titleStyle={{ color: '#0063b4' }}
              fontWeight="bold"
              buttonStyle={[
                styles.button,
                { width: (deviceWidth - 100) / buttons.length },
              ]}
              containerViewStyle={
                buttons.length >= 2
                  ? { marginLeft: moderateScale(10), marginRight: 0 }
                  : { marginLeft: 0, marginRight: 0, width: deviceWidth / 6 }
              }
            />
          ))}
        </View>
      </Modal>
    );
  }
}

export default connect(({ alert, nav }) => ({ alert, nav }))(AlertComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerModal: {
    height: moderateScale(150),
  },
  modalFeature: {
    marginTop: 20,
    overflow: 'hidden',
    justifyContent: 'flex-start',
    // alignItems: 'center',
    width: deviceWidth,
    height: deviceHeight / 1.3,
    backgroundColor: '#cddced',
    // borderRadius: 10,
  },
  modal: {
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    width: deviceWidth / 1.3,
    height: deviceHeight / 2,
    borderRadius: moderateScale(15),
  },
  imageBackground: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight / 5,
    backgroundColor: '#0090d2',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  image: {
    width: deviceWidth / 4.5,
    height: deviceWidth / 4.5,
    marginTop: moderateScale(30),
  },
  title: {
    backgroundColor: 'transparent',
    color: '#42535E',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: moderateScale(20),
    marginTop: moderateScale(15),
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
    marginBottom: moderateScale(10),
  },
  messageContainer: {
    height: moderateScale(220),
    marginTop: moderateScale(5),
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
  },
  message: {
    backgroundColor: 'transparent',
    color: '#42535E',
    fontFamily: 'Arial',
    fontSize: moderateScale(16),
    marginLeft: moderateScale(10),
    marginBottom: moderateScale(10),
    marginRight: moderateScale(10),
    textAlign: 'left',
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // position:'relative',
    // bottom:0,
    // alignSelf: 'flex-end',
    marginBottom: moderateScale(10),
    // flexWrap: 'wrap'
  },
  button: {
    height: moderateScale(50),
    backgroundColor: 'transparent',
  },
});
