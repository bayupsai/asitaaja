import React, { Component } from 'react';
import {
  Dimensions,
  Animated,
  FlatList,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
} from 'react-native';

import Bubble from '../chat-bubble/Text';
import ChatInput from './ChatInput';
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';

// get device OS
const OS = Platform.OS;
// get OS navigation bar height
const NAVIGATION_HEIGHT = OS === 'ios' ? 64 : 54;
// get window height
const WINDOW_HEIGHT = Dimensions.get('window').height;

/**
 * @export @class FlatChat
 * @desc          FlatChat component
 */
export default class FlatChat extends Component {
  constructor(props) {
    super(props);

    const {
      data,
      chatVerticalOffset,
      fixNavBarOffset,
      scrollOnKeyboardShow,
    } = this.props;

    // get navigation bar height based on 'fixNavBarOffset' prop
    const navigationHeight = fixNavBarOffset ? NAVIGATION_HEIGHT : 0;
    // get FlatChat vertical offset to allow extra space on top
    const verticalOffset = (chatVerticalOffset || 0) + navigationHeight;

    this.state = {
      // FlatList data
      data: [],
      // flags based on 'scrollBottomOnKeyboardShow' prop type
      scrollOnKeyboardShowBottomStart: false,
      scrollOnKeyboardShowBottomEnd: true,

      /* -----------------------------------------------------
       * FlatChat layout settings
       */

      // manage keyboard status
      activeKeyboard: false,

      // manage FlatList scroll position on onScroll event
      scrollPosition: 0,

      // manage FlatList layout to get scroll max position (scrollLimit)
      contentSize: 0,
      layoutHeight: 0,

      // FlatChat layout variables
      flatListOffset: verticalOffset,
      flatListHeight: WINDOW_HEIGHT - verticalOffset,
      listHeight: new Animated.Value(WINDOW_HEIGHT - verticalOffset),
    };
  }

  componentWillMount() {
    this.keyboardWillShowListener = Keyboard.addListener(
      'keyboardWillShow',
      this.keyboardWillShow.bind(this)
    );
    this.keyboardWillHideListener = Keyboard.addListener(
      'keyboardWillHide',
      this.keyboardWillHide.bind(this)
    );
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide.bind(this)
    );
  }

  componentWillReceiveProps({ data }) {
    if (this.state.data.length !== data.length) {
      this.setState({ data });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.data.length !== this.state.data.length &&
      this.state.data.length > 0
    ) {
      setTimeout(() => {
        this.scroller.scrollToEnd();
      }, 250);
    }
  }

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillHideListener.remove();
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardWillShow(e) {
    const {
      listHeight,
      flatListHeight,
      scrollOnKeyboardShowBottomStart,
    } = this.state;
    const keyboardHeight = e.endCoordinates.height;

    if (scrollOnKeyboardShowBottomStart) {
      listHeight.addListener(() => this.scroller.scrollToEnd());
    } else
      Animated.timing(listHeight, {
        // animate the FlatList height to his initial minus the keyboard height
        toValue: flatListHeight - keyboardHeight,
        // uses the keyboard animation timing to be synchronized
        duration: e.duration,

        // scroll to the end when animation is finished
      }).start(() => {
        /** remove all listHeight listeners if @prop @type scrollOnKeyboardShow = "bottom-start" */
        if (this.state.scrollOnKeyboardShowBottomStart) {
          this.state.listHeight.removeAllListeners();
        }
      });
  }

  keyboardDidShow() {
    this.setState({ activeKeyboard: true });
    if (this.state.scrollOnKeyboardShowBottomEnd) {
      this.scroller.scrollToEnd();
    }
  }

  keyboardWillHide(e) {
    const {
      listHeight,
      flatListHeight,
      scrollPosition,
      contentSize,
      layoutHeight,
    } = this.state;
    /*************************** FLATLIST ANIMATION  */

    Animated.timing(listHeight, {
      // animate the FlatList height to his initial height
      toValue: flatListHeight,
      // fixer to speed up the resizing animation on closing keyboard
      duration: e.duration - 150,
    }).start();
  }

  keyboardDidHide() {
    this.setState({ activeKeyboard: false });
  }

  updateScrollPosition(e) {
    const scrollPosition = this.state.scrollPosition;
    const currentScrollPos = e.nativeEvent.contentOffset.y;

    // ignore negative scroll positions in case of iOS momentum or bugs
    if (currentScrollPos >= 0) {
      const sensitivity = 10;
      const scrollPositionOffset = currentScrollPos - scrollPosition;

      if (Math.abs(scrollPositionOffset) > sensitivity) {
        this.setState({ scrollPosition: currentScrollPos });
      }
    }
  }

  updateLayoutHeight(e) {
    this.setState({ layoutHeight: e.nativeEvent.layout.height });
  }

  updateContentSize(w, h) {
    this.setState({ contentSize: h });
  }

  _renderItem = ({ item, index }) => {
    return <Bubble item={item} navigation={this.props.navigation} />;
  };

  render() {
    const { data, isKeyboardShow, navigation } = this.props;

    return (
      <FlatList
        bounces={false}
        ref={ref => (this.scroller = ref)}
        data={this.state.data}
        renderItem={this._renderItem}
        keyExtractor={(item, key) => key}
        style={{ height: this.state.flatListHeight, paddingBottom: 30 }}
      />
    );
  }
}
