import React, { Component } from 'react';
import {
  View,
  TextInput,
  Dimensions,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  Picker,
  PickerIOS,
  Platform,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import { Button } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';
import Modal from 'react-native-modalbox';
import axios from 'axios';

import moment from 'moment';
import API from '../Const/APIEndPoints';

class RegisterChatForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
    };
  }

  componentDidMount() {
    this.refs.modal1.open();
  }

  confirmationSend = () => {
    if (this.state.email != '' && this.state.name != '') {
      this.refs.modal2.open();
    } else {
      this.refs.modal3.open();
    }
  };

  send = () => {
    if (this.state.email != '' && this.state.name != '') {
      let params = {
        name: this.state.name.toLowerCase(),
        email: this.state.email.toLowerCase(),
      };

      let paramStringfy = JSON.stringify(params);

      let sendText = `Daftarkan nama saya sebagai ${this.state.name} dan alamat email saya adalah ${this.state.email}`;

      dispatch.main.sendNoGetChat(sendText);
      dispatch.main.getTokenChat(params);
      dispatch.main.getChat('menu');

      this.refs.modal2.close();
      this.refs.modal1.close();
    } else {
      this.refs.modal3.open();
    }
  };

  onOpen = () => {
    dispatch.main.hideModal();
  };

  render() {
    const { main } = this.props;
    name = this.state.name;
    email = this.state.email;

    return (
      <Modal
        ref={'modal1'}
        style={styles.modal}
        position={'bottom'}
        onOpened={this.onOpen}
        backdrop={true}
        backdropPressToClose={false}
        entry={'bottom'}
        swipeToClose={false}
      >
        <View style={styles.header}>
          <View style={{ width: deviceWidth / 1 }}>
            <Text style={styles.headerTextModal1}>Daftar Akun</Text>
          </View>
          <View style={{ width: deviceWidth / 6.3 }}>
            <TouchableOpacity onPress={() => this.refs.modal4.open()}>
              <Text style={styles.exit}>X</Text>
            </TouchableOpacity>
          </View>
        </View>

        <Text style={styles.textDesc}>Nama</Text>
        <TextInput
          style={styles.textInputContainer}
          value={this.state.name}
          onChangeText={text => this.setState({ name: text })}
        />

        <Text style={styles.textDesc}>Email</Text>
        <TextInput
          style={styles.textInputContainer}
          value={this.state.email}
          onChangeText={text => this.setState({ email: text })}
        />

        <View style={styles.buttonContainer}>
          <Button
            title="DAFTAR"
            loading={false}
            titleStyle={{
              fontWeight: '700',
              fontSize: moderateScale(18),
              color: '#ffffff',
            }}
            buttonStyle={styles.btnPesan}
            containerStyle={{ marginTop: 5 }}
            onPress={() => this.confirmationSend()}
          />
        </View>

        {/* Start Modal 2 */}
        <Modal
          ref={'modal2'}
          style={styles.modalCfm}
          position={'top'}
          backdrop={true}
          backdropPressToClose={false}
          entry={'bottom'}
        >
          <View style={styles.headerCfm}>
            <Text style={styles.headerText}>Konfirmasi</Text>
          </View>
          <Text style={styles.textDescCfm}>
            Apakah kamu yakin data sudah benar ?
          </Text>
          <View style={styles.buttonContainerCfm}>
            <Button
              title="Ya"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#ec650a',
              }}
              buttonStyle={styles.btnYa}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.send()}
            />
            <Button
              title="Batal"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#9b9b9b',
              }}
              buttonStyle={styles.btnBatal}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.refs.modal2.close()}
            />
          </View>
        </Modal>
        {/* End of Modal 2*/}

        {/* Start Modal 3 */}
        <Modal
          ref={'modal3'}
          style={styles.modalCfm}
          position={'top'}
          backdrop={true}
          backdropPressToClose={false}
          entry={'bottom'}
        >
          <View style={styles.headerCfmAlert}>
            <Text style={styles.headerText}>Perhatian !</Text>
          </View>
          <Text style={styles.textDescCfm}>Harap isi seluruh kolom</Text>
          <View style={styles.buttonContainerCfm}>
            <Button
              title="OK"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#ec650a',
              }}
              buttonStyle={styles.btnYa}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.refs.modal3.close()}
            />
          </View>
        </Modal>
        {/* End of Modal 3*/}

        {/* Start Modal 3 */}
        <Modal
          ref={'modal4'}
          style={styles.modalCfm}
          position={'top'}
          backdrop={true}
          backdropPressToClose={false}
          entry={'bottom'}
        >
          <View style={styles.headerCfmAlert}>
            <Text style={styles.headerText}>Perhatian !</Text>
          </View>
          <Text style={styles.textDescCfm}>
            Untuk dapat menggunakan fitur chat Anda diharuskan untuk mendaftar
            pada form ini terlebih dahulu
          </Text>
          <View style={styles.buttonContainerCfm}>
            <Button
              title="OK"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#ec650a',
              }}
              buttonStyle={styles.btnYa}
              containerStyle={{ marginTop: 5, marginBottom: moderateScale(10) }}
              onPress={() => this.refs.modal4.close()}
            />
          </View>
        </Modal>
        {/* End of Modal 3*/}
      </Modal>
    );
  }
}

export default connect(({ main, nav, user, alert }) => ({
  main,
  nav,
  user,
  alert,
}))(RegisterChatForm);

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: deviceWidth / 1.1,
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(20),
    padding: 10,
  },
  buttonContainerCfm: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: deviceWidth / 1.1,
    marginLeft: moderateScale(20),
    marginTop: moderateScale(20),
  },
  textDescCfm: {
    color: '#3C3C3C',
    fontSize: moderateScale(16),
    fontWeight: '700',
    marginLeft: moderateScale(20),
    marginRight: moderateScale(20),
    textAlign: 'center',
  },

  //////NEW
  modal: {
    height: deviceHeight / 1.6,
    width: deviceWidth,
    // marginTop:moderateScale(120),
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  modalCfm: {
    height: deviceHeight / 3,
    width: deviceWidth,
    // marginTop:moderateScale(220),
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  modalPicker: {
    height: deviceHeight / 2.3,
    width: deviceWidth,
  },
  headerCfm: {
    backgroundColor: '#FFFFFF',
    marginBottom: moderateScale(20),
    height: moderateScale(50),
    justifyContent: 'center',
    // borderBottomLeftRadius:20,
    // borderBottomRightRadius:20
  },
  headerCfmAlert: {
    backgroundColor: '#FFFFFF',
    marginBottom: moderateScale(20),
    height: moderateScale(50),
    justifyContent: 'center',
    textAlign: 'center',
    // borderBottomLeftRadius:20,
    // borderBottomRightRadius:20
  },
  header: {
    backgroundColor: '#ffffff',
    marginBottom: moderateScale(30),
    height: moderateScale(50),
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flexDirection: 'row',
    // borderBottomLeftRadius:20,
    // borderBottomRightRadius:20
  },
  headerTextModal1: {
    fontSize: moderateScale(21),
    backgroundColor: 'transparent',
    marginTop: moderateScale(10),
    marginLeft: moderateScale(60),
    textAlign: 'center',
    color: '#0090d2',
    fontWeight: '700',
  },
  headerText: {
    fontSize: moderateScale(21),
    backgroundColor: 'transparent',
    marginTop: moderateScale(10),
    textAlign: 'center',
    color: '#0090d2',
    fontWeight: '700',
  },
  textDesc: {
    color: '#3C3C3C',
    fontSize: moderateScale(14),
    fontWeight: '700',
    marginLeft: moderateScale(20),
  },
  exit: {
    fontSize: moderateScale(21),
    fontWeight: '700',
    color: '#ec650a',
    marginTop: moderateScale(10),
  },
  btnPesan: {
    backgroundColor: '#ec650a',
    width: deviceWidth / 1.2,
    height: moderateScale(40),
    borderColor: 1,
    borderWidth: moderateScale(0),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
  },
  btnBatal: {
    backgroundColor: 'transparent',
    width: moderateScale(120),
    height: moderateScale(40),
    borderColor: '#9b9b9b',
    borderWidth: moderateScale(2),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
    marginLeft: moderateScale(10),
  },
  btnYa: {
    backgroundColor: 'transparent',
    width: moderateScale(120),
    height: moderateScale(40),
    borderColor: '#ec650a',
    borderWidth: moderateScale(2),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
  },
  textInputContainer: {
    height: moderateScale(50),
    width: deviceWidth / 1.1,
    marginBottom: moderateScale(20),
    backgroundColor: '#F8F6F9',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(10),
    paddingLeft: moderateScale(10),
  },
  textDesc: {
    color: '#3C3C3C',
    fontSize: moderateScale(14),
    fontWeight: '700',
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(5),
  },
});
