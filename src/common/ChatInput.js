import React, { Component } from 'react';
import {
  View,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  StyleSheet,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import Voice from 'react-native-voice';
import API from '../Const/APIEndPoints';
const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
const MIC_ACTIVE = require('../assets/chat/assets/ic_active_mic.png');
const MIC_INACTIVE = require('../assets/chat/assets/ic_inactive_mic.png');
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';
import moment from 'moment';

class ChatInputMode extends Component {
  constructor() {
    super();
  }
  componentWillMount() {}

  toggleKeyboard = () => {
    dispatch.main.toggleKeyboard();
  };

  onChangeText = query => {
    dispatch.main.update({ query });
  };

  send = () => {
    const { main, user } = this.props;
    dispatch.main.send(main.query);
    dispatch.main.update({ query: '' });
  };

  onSpeechStartHandler(e) {
    let time = moment().format('HH:mm');

    let param = {
      text: '',
      me: true,
      voice: true,
      time: time,
    };

    dispatch.main.addChat(param);
  }

  onSpeechResultsHandler(e) {
    const { main } = this.props;
    let chats = main.data;

    setTimeout(() => {
      if (e.value[0] == chats[chats.length - 1].text) {
        dispatch.main.stopRecord();
      }
    }, 2000);
    if (chats[chats.length - 1].me) {
      chats[chats.length - 1].text = e.value[0];
      if (chats[chats.length - 1].text == '') {
        chats.splice(chats.length - 1, 1);
      }
    }
    dispatch.main.update({ data: chats }); //ini berfungsi untuk mengupdate voice yang sudah masuk
    if (Platform.OS == 'android') {
      dispatch.main.getChat(e.value[0]);
    }
  }

  onSpeechEndHandler(e) {
    const { main } = this.props;
    if (Platform.OS == 'ios') {
      if (
        main.data[main.data.length - 1].me &&
        main.data[main.data.length - 1].text != null
      ) {
        dispatch.main.getChat(main.data[main.data.length - 1].text);
      }
    } else {
      console.log('onSpeechEndHandler: ', e);
    }
  }

  onSpeechErrorHandler(e) {
    console.log('error : ', e);
  }

  record = () => {
    const { main, user } = this.props;
    main.isListen ? dispatch.main.stopRecord() : dispatch.main.startRecord();
  };

  render() {
    const { main, user } = this.props;
    Voice.onSpeechStart = this.onSpeechStartHandler.bind(this);
    Voice.onSpeechEnd = this.onSpeechEndHandler.bind(this);
    Voice.onSpeechResults = this.onSpeechResultsHandler.bind(this);
    Voice.onSpeechError = this.onSpeechErrorHandler.bind(this);

    // console.log('wordToReplace :', wordToReplace)
    if (Platform.OS == 'ios') {
      return (
        //tadinya minheight 20 terus keyboard
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.chatInput}>
            <TextInput
              autoFocus={false}
              value={main.query}
              onChangeText={query => this.onChangeText(query)}
              style={styles.textInput}
              underlineColorAndroid="transparent"
              returnKeyType="send"
              onSubmitEditing={this.send}
              blurOnSubmit={true}
              placeholder="Klik untuk mengetik..."
            />
            <TouchableHighlight onPress={this.record}>
              <Image
                source={main.isListen ? MIC_INACTIVE : MIC_ACTIVE}
                style={styles.mic}
              />
            </TouchableHighlight>
          </View>
        </KeyboardAvoidingView>
      );
    } else {
      return (
        <View style={styles.chatInput}>
          <TextInput
            autoFocus={false}
            value={main.query}
            onChangeText={query => this.onChangeText(query)}
            style={styles.textInput}
            underlineColorAndroid="transparent"
            returnKeyType="send"
            onSubmitEditing={this.send}
            blurOnSubmit={true}
            placeholder="Klik untuk mengetik..."
          />
          <TouchableHighlight onPress={this.record}>
            <Image
              source={main.isListen ? MIC_INACTIVE : MIC_ACTIVE}
              style={styles.mic}
            />
          </TouchableHighlight>
        </View>
      );
    }
  }
}

export default connect(({ main, nav, user }) => ({ main, nav, user }))(
  ChatInputMode
);

const styles = StyleSheet.create({
  textInput: {
    minHeight: moderateScale(40), //tadinya 40
    fontSize: moderateScale(14),
    width: moderateScale(280),
    paddingHorizontal: moderateScale(0),
    alignSelf: 'center',
    paddingVertical: moderateScale(7),
    paddingLeft: moderateScale(5),
  },
  button: {
    width: 50,
    height: 40,
    padding: 5,
    margin: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    backgroundColor: 'transparent',
  },
  mic: {
    width: deviceWidth / 6,
    height: deviceWidth / 6,
    ...Platform.select({
      ios: {
        position: 'absolute',
      },
      android: {},
    }),
    // marginBottom:moderateScale(120)
    bottom: moderateScale(1),
    // right:moderateScale(1)
  },
  chatInput: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    minHeight: moderateScale(20),
    borderTopWidth: moderateScale(1),
    borderColor: '#EAEAEA',
  },
});
