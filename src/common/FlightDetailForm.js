import React, { Component } from 'react';
import {
  View,
  TextInput,
  Dimensions,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  Picker,
  PickerIOS,
  Platform,
  FlatList,
  AsyncStorage,
} from 'react-native';
import { connect } from 'react-redux';
import { dispatch } from '../redux/store';
import { Button } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');
import ScaleUtils, { verticalScale, moderateScale } from '../Const/ScaleUtils';
import Modal from 'react-native-modalbox';
import moment from 'moment';
import { SearchBar, Icon } from 'react-native-elements';
import { get, getWithHeader } from '../services/Rest';
import API from '../Const/APIEndPoints';
import { USERDATACONFIG } from '../Const/Config';

var currentDate = moment().format('DD-MM-YYYY');
var new_date = moment(currentDate, 'DD-MM-YYYY').add('days', 1);
var new_dateYear = moment(currentDate, 'DD-MM-YYYY').add('days', 365);

var day = new_date.format('DD');
var month = new_date.format('MM');
var year = new_date.format('YYYY');

var dayLater = new_dateYear.format('DD');
var monthLater = new_dateYear.format('MM');
var yearLater = new_dateYear.format('YYYY');

var maxDate = dayLater + '-' + monthLater + '-' + yearLater;

class ModalBoxInputPwd extends Component {
  constructor() {
    super();
    (this.state = {
      text: '',
      tripType: 'one-way',
      penumpang: ['0', '1', '2', '3', '4', '5', '6', '7'],
      dewasa: '1',
      anak: '0',
      bayi: '0',
      tanggalPergi: day + '-' + month + '-' + year,
      tanggalPulang: day + '-' + month + '-' + year,
      bandaraAsal: 'CGK',
      bandaraTujuan: 'DPS',
      namaDanKodeBandaraAsal: 'Soekarno Hatta - CGK',
      namaDanKodeBandaraTujuan: 'Ngurah Rai - DPS',
      search: '',
      dataSource: {},
      airportDatas: {},
      token: '',
    }),
      (this.arrayholder = []);
  }

  async componentDidMount() {
    AsyncStorage.getItem(USERDATACONFIG).then(async data => {
      let user = JSON.parse(data);
      let response = await getWithHeader(API.getAirports, user.access_token);

      this.setState(
        {
          dataSource: response.result.output,
        },
        function() {
          this.arrayholder = response.result.output;
        }
      );
    }); //end asyncstorage
  }

  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter(function(item) {
      //applying filter for the inserted text in search bar
      const itemData = item.label ? item.label.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      search: text,
    });
  }

  send = () => {
    const { main } = this.props;
    //convert
    let convertDewasa = parseInt(this.state.dewasa);
    let convertAnak = parseInt(this.state.anak);
    let convertBayi = parseInt(this.state.bayi);
    let passengerAmount = convertDewasa + convertAnak + convertBayi;
    let dateFormatPergi = moment(this.state.tanggalPergi, 'DD-MM-YYYY').format(
      'YYYY-MM-DD'
    );
    let dateFormatPulang = moment(
      this.state.tanggalPulang,
      'DD-MM-YYYY'
    ).format('YYYY-MM-DD');
    let emptyDate = '';

    if (
      this.state.tripType != '' &&
      (this.state.tanggalPergi != '' || this.state.tanggalPulang != '') &&
      passengerAmount > 0
    ) {
      if (passengerAmount > 7) {
        this.refs.modal4.open();
      } else {
      }

      let params = {
        tripType: main.tripType == 'round-trip' ? 'two-way' : 'one-way',
        origin: this.state.bandaraAsal,
        destination: this.state.bandaraTujuan,
        departure: dateFormatPergi,
        return: main.tripType == 'round-trip' ? dateFormatPulang : emptyDate,
        adult: convertDewasa,
        child: convertAnak,
        infant: convertBayi,
      };
      console.log('params :', params);

      let paramStringfy = JSON.stringify(params);

      let sendText = `pesan tiket ke ${this.state.namaDanKodeBandaraTujuan} dari ${this.state.namaDanKodeBandaraAsal} untuk tanggal ${this.state.tanggalPergi}`;

      dispatch.main.sendNoGetChat(sendText);
      dispatch.main.getChat(paramStringfy);
      dispatch.main.hideModal();

      this.refs.modal2.close();
      this.refs.modal1.close();

      arrKodeBandara = [];
    } else {
      this.refs.modal2.close();
      this.refs.modal3.open();
    }
  };

  onOpen = () => {
    dispatch.main.hideModal();
  };

  jumlahPenumpang = () => {
    return this.state.penumpang.map((x, i) => {
      return <Picker.Item label={x} key={i} value={x} />;
    });
  };

  exitForm = () => {
    this.refs.modal1.close();
    dispatch.main.send('batal');
  };

  exitSearchBandaraAsal = () => {
    this.refs.modal5.close();
  };

  exitSearchBandaraTujuan = () => {
    this.refs.modal6.close();
  };

  bandaraAsal = (name, kode) => {
    this.setState({
      bandaraAsal: kode,
      namaDanKodeBandaraAsal: name + ' - ' + kode,
    });
    this.refs.modal5.close();
  };

  bandaraTujuan = (name, kode) => {
    this.setState({
      bandaraTujuan: kode,
      namaDanKodeBandaraTujuan: name + ' - ' + kode,
    });
    this.refs.modal6.close();
  };

  render() {
    const { main } = this.props;
    console.log('main triptype pak : ', main.tripType);
    {
      main.showModal ? this.refs.modal1.open() : '';
    }
    return (
      <Modal
        ref={'modal1'}
        style={styles.modal}
        position={'bottom'}
        onOpened={this.onOpen}
        backdrop={true}
        backdropPressToClose={false}
        entry={'bottom'}
        swipeToClose={false}
      >
        <View style={styles.header}>
          <View style={{ width: deviceWidth / 1 }}>
            <Text style={styles.headerText}>Detail Perjalanan</Text>
          </View>
          <View style={{ width: deviceWidth / 6.3 }}>
            <TouchableOpacity onPress={() => this.exitForm()}>
              <Text style={styles.exit}>X</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <Text style={styles.textDesc}>Perjalanan</Text>

          {/* TRIP TYPE */}
          <View style={styles.btnContainerTrip}>
            {main.tripType == 'round-trip' ? (
              <Button
                title="Pulang - pergi"
                loading={false}
                titleStyle={styles.titleTripSelected}
                buttonStyle={styles.btnTripTypeSelected}
                containerStyle={{ marginTop: moderateScale(5) }}
                onPress={() => this.setState({ tripType: 'two-way' })}
              />
            ) : (
              <Button
                title="1x Perjalanan"
                loading={false}
                titleStyle={styles.titleTripSelected}
                buttonStyle={styles.btnTripTypeSelected}
                containerStyle={{
                  marginTop: moderateScale(5),
                  marginRight: moderateScale(15),
                }}
                onPress={() => this.setState({ tripType: 'one-way' })}
              />
            )}
          </View>

          {/* AIRPORT ASAL*/}

          <View>
            <TouchableOpacity onPress={() => this.refs.modal5.open()}>
              <View style={styles.airportContainer}>
                <Text style={styles.textPickerIOS}>
                  {this.state.namaDanKodeBandaraAsal}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.refs.modal6.open()}>
              <View style={styles.airportContainer}>
                <Text style={styles.textPickerIOS}>
                  {this.state.namaDanKodeBandaraTujuan}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: 'row' }}>
            {/* KEBERANGKATAN */}
            <View>
              <Text style={styles.textDesc}>Keberangkatan</Text>
              <DatePicker
                style={
                  main.tripType == 'round-trip'
                    ? styles.dateContainer
                    : styles.dateContainerLong
                }
                customStyles={{
                  dateText: {
                    color: '#000000',
                    fontSize: moderateScale(16),
                  },
                  dateInput: {
                    height: moderateScale(50),
                    paddingVertical: 8,
                    borderWidth: 0,
                    marginTop: moderateScale(20),
                    alignItems: 'flex-start',
                    paddingHorizontal: moderateScale(10),
                  },
                }}
                date={this.state.tanggalPergi}
                mode="date"
                placeholder="Tanggal keberangkatan"
                format="DD-MM-YYYY"
                minDate={currentDate}
                maxDate={maxDate}
                showIcon={false}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                onDateChange={tanggalPergi => {
                  this.setState({ tanggalPergi: tanggalPergi });
                }}
              />
            </View>

            {/* KEPULANGAN */}

            {main.tripType == 'round-trip' ? (
              <View>
                <Text style={styles.textDesc}>Kepulangan</Text>
                <DatePicker
                  style={styles.dateContainer}
                  customStyles={{
                    dateText: {
                      color: '#000000',
                      fontSize: moderateScale(16),
                    },
                    dateInput: {
                      height: moderateScale(50),
                      paddingVertical: 8,
                      borderWidth: 0,
                      marginTop: moderateScale(20),
                      alignItems: 'flex-start',
                      paddingHorizontal: moderateScale(10),
                    },
                  }}
                  date={this.state.tanggalPulang}
                  mode="date"
                  placeholder="Tanggal Kepulangan"
                  format="DD-MM-YYYY"
                  minDate={currentDate}
                  maxDate={maxDate}
                  showIcon={false}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  onDateChange={tanggalPulang => {
                    this.setState({ tanggalPulang: tanggalPulang });
                  }}
                />
              </View>
            ) : (
              <View />
            )}
          </View>
          <View></View>
          {Platform.OS == 'ios' ? (
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => this.refs.modal7.open()}>
                <View style={styles.penumpangContainer}>
                  <Text style={styles.textPickerIOS}>{this.state.dewasa}</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.refs.modal8.open()}>
                <View style={styles.penumpangContainer}>
                  <Text style={styles.textPickerIOS}>{this.state.anak}</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.refs.modal9.open()}>
                <View style={styles.penumpangContainer}>
                  <Text style={styles.textPickerIOS}>{this.state.bayi}</Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Text style={styles.textDesc}>Dewasa</Text>
                <Picker
                  style={styles.penumpangContainer}
                  selectedValue={this.state.dewasa}
                  onValueChange={jumlah => this.setState({ dewasa: jumlah })}
                >
                  {this.jumlahPenumpang()}
                </Picker>
              </View>
              <View>
                <Text style={styles.textDesc}>Anak-anak</Text>
                <Picker
                  style={styles.penumpangContainer}
                  selectedValue={this.state.anak}
                  onValueChange={jumlah => this.setState({ anak: jumlah })}
                >
                  {this.jumlahPenumpang()}
                </Picker>
              </View>
              <View>
                <Text style={styles.textDesc}>Bayi</Text>
                <Picker
                  style={styles.penumpangContainer}
                  selectedValue={this.state.bayi}
                  onValueChange={jumlah => this.setState({ bayi: jumlah })}
                >
                  {this.jumlahPenumpang()}
                </Picker>
              </View>
            </View>
          )}

          <View style={styles.buttonContainer}>
            <Button
              title="Cari Tiket Penerbangan"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(18),
                color: '#ffffff',
              }}
              buttonStyle={styles.btnPesan}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.refs.modal2.open()}
            />
          </View>
        </ScrollView>

        {/* Start Modal 2 */}
        <Modal
          ref={'modal2'}
          style={styles.modalCfm}
          position={'top'}
          backdrop={true}
          backdropPressToClose={false}
          entry={'bottom'}
        >
          <View style={styles.headerCfm}>
            <Text style={styles.headerText}>Konfirmasi</Text>
          </View>
          <Text style={styles.textDescCfm}>
            Apakah kamu yakin data sudah benar ?
          </Text>
          <View style={styles.buttonContainerCfm}>
            <Button
              title="Ya"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#ec650a',
              }}
              buttonStyle={styles.btnYa}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.send()}
            />
            <Button
              title="Batal"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#9b9b9b',
              }}
              buttonStyle={styles.btnBatal}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.refs.modal2.close()}
            />
          </View>
        </Modal>
        {/* End of Modal 2*/}

        {/* Start Modal 3 */}
        <Modal
          ref={'modal3'}
          style={styles.modalCfm}
          position={'top'}
          backdrop={true}
          backdropPressToClose={false}
          entry={'bottom'}
        >
          <View style={styles.headerCfm}>
            <Text style={styles.headerText}>Perhatian !</Text>
          </View>
          <Text style={styles.textDescCfm}>Harap isi seluruh kolom</Text>
          <View style={styles.buttonContainerCfm}>
            <Button
              title="OK"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#ec650a',
              }}
              buttonStyle={styles.btnYa}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.refs.modal3.close()}
            />
          </View>
        </Modal>
        {/* End of Modal 3*/}

        {/* Start Modal 4 */}
        <Modal
          ref={'modal4'}
          style={styles.modalCfm}
          position={'top'}
          backdrop={true}
          backdropPressToClose={false}
          entry={'bottom'}
        >
          <View style={styles.headerCfm}>
            <Text style={styles.headerText}>Perhatian !</Text>
          </View>
          <Text style={styles.textDescCfm}>
            Maaf maksimum pembelian tiket adalah 7 Orang
          </Text>
          <View style={styles.buttonContainerCfm}>
            <Button
              title="OK"
              loading={false}
              titleStyle={{
                fontWeight: '700',
                fontSize: moderateScale(16),
                color: '#ec650a',
              }}
              buttonStyle={styles.btnYa}
              containerStyle={{ marginTop: 5 }}
              onPress={() => this.refs.modal4.close()}
            />
          </View>
        </Modal>
        {/* End of Modal 4*/}

        {/* Start Modal 5 Bandara asal */}
        <Modal
          ref={'modal5'}
          style={styles.modalPicker5}
          position={'top'}
          backdrop={true}
          backdropPressToClose={true}
          entry={'bottom'}
        >
          <View>
            <View style={styles.headerSearch}>
              <View style={{ width: deviceWidth / 1 }}>
                <Text style={styles.textDescPicker}>Bandara Asal</Text>
              </View>
              <View style={{ width: deviceWidth / 6.3 }}>
                <TouchableOpacity onPress={() => this.exitSearchBandaraAsal()}>
                  <Text style={styles.exit}>X</Text>
                </TouchableOpacity>
              </View>
            </View>

            <SearchBar
              round
              lightTheme
              inputStyle={{ fontSize: 14 }}
              inputContainerStyle={{ backgroundColor: 'white', height: 40 }}
              containerStyle={{
                height: 60,
                borderBottomColor: 'transparent',
                borderTopColor: 'transparent',
              }}
              searchIcon={{ size: 24 }}
              onChangeText={text => this.SearchFilterFunction(text)}
              onClear={text => this.SearchFilterFunction('')}
              placeholder="Type here"
              value={this.state.search}
            />
            {/* <Button title='Ok' titleStyle={{color:'#FFFFFF'}} buttonStyle={styles.button} onPress={()=>this.refs.modal5.close()}/> */}
            <FlatList
              data={this.state.dataSource}
              //   ItemSeparatorComponent={this.ListViewItemSeparator}
              //Item Separator View
              renderItem={({ item }) => (
                <View style={styles.airportListContainer}>
                  <TouchableOpacity
                    onPress={() => this.bandaraAsal(item.label, item.value)}
                  >
                    <View style={styles.airportLabelContainer}>
                      <Text style={styles.airportNameLabel}>
                        {item.label + ' - ' + item.value}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
              enableEmptySections={true}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </Modal>
        {/* End of Modal 5 Bandara asal*/}

        {/* Start Modal 6 Bandara tujuan */}
        <Modal
          ref={'modal6'}
          style={styles.modalPicker5}
          position={'top'}
          backdrop={true}
          backdropPressToClose={true}
          entry={'bottom'}
        >
          <View>
            <View style={styles.headerSearch}>
              <View style={{ width: deviceWidth / 1 }}>
                <Text style={styles.textDescPicker}>Bandara Tujuan</Text>
              </View>
              <View style={{ width: deviceWidth / 6.3 }}>
                <TouchableOpacity
                  onPress={() => this.exitSearchBandaraTujuan()}
                >
                  <Text style={styles.exit}>X</Text>
                </TouchableOpacity>
              </View>
            </View>

            <SearchBar
              round
              lightTheme
              inputStyle={{ fontSize: 14 }}
              inputContainerStyle={{ backgroundColor: 'white', height: 40 }}
              containerStyle={{
                height: 60,
                borderBottomColor: 'transparent',
                borderTopColor: 'transparent',
              }}
              searchIcon={{ size: 24 }}
              onChangeText={text => this.SearchFilterFunction(text)}
              onClear={text => this.SearchFilterFunction('')}
              placeholder="Type here"
              value={this.state.search}
            />
            {/* <Button title='Ok' titleStyle={{color:'#FFFFFF'}} buttonStyle={styles.button} onPress={()=>this.refs.modal5.close()}/> */}
            <FlatList
              data={this.state.dataSource}
              //   ItemSeparatorComponent={this.ListViewItemSeparator}
              //Item Separator View
              renderItem={({ item }) => (
                <View style={styles.airportListContainer}>
                  <TouchableOpacity
                    onPress={() => this.bandaraTujuan(item.label, item.value)}
                  >
                    <View style={styles.airportLabelContainer}>
                      <Text style={styles.airportNameLabel}>
                        {item.label + ' - ' + item.value}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
              enableEmptySections={true}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </Modal>
        {/* End of Modal 6 Bandara tujuan*/}

        {/* Start Modal 7 amount dewasa */}
        <Modal
          ref={'modal7'}
          style={styles.modalPicker}
          position={'top'}
          backdrop={true}
          backdropPressToClose={true}
          entry={'bottom'}
        >
          <View>
            <Text style={styles.textDescPicker}>Penumpang Dewasa</Text>
            <PickerIOS
              selectedValue={this.state.dewasa}
              onValueChange={amount => this.setState({ dewasa: amount })}
            >
              {this.jumlahPenumpang()}
            </PickerIOS>
          </View>
        </Modal>
        {/* End of Modal 7 amount dewasa*/}

        {/* Start Modal 6 Bandara tujuan */}
        <Modal
          ref={'modal8'}
          style={styles.modalPicker}
          position={'top'}
          backdrop={true}
          backdropPressToClose={true}
          entry={'bottom'}
        >
          <View>
            <Text style={styles.textDescPicker}>Penumpang Anak</Text>
            <PickerIOS
              selectedValue={this.state.anak}
              onValueChange={anak => this.setState({ anak: anak })}
            >
              {this.jumlahPenumpang()}
            </PickerIOS>
          </View>
        </Modal>
        {/* End of Modal 6 Bandara tujuan*/}

        {/* Start Modal 6 Bandara tujuan */}
        <Modal
          ref={'modal9'}
          style={styles.modalPicker}
          position={'top'}
          backdrop={true}
          backdropPressToClose={true}
          entry={'bottom'}
        >
          <View>
            <Text style={styles.textDescPicker}>Penumpang Bayi</Text>
            <PickerIOS
              selectedValue={this.state.bayi}
              onValueChange={amount => this.setState({ bayi: amount })}
            >
              {this.jumlahPenumpang()}
            </PickerIOS>
          </View>
        </Modal>
        {/* End of Modal 6 Bandara tujuan*/}
      </Modal>
    );
  }
}

export default connect(({ main, nav, user, alert }) => ({
  main,
  nav,
  user,
  alert,
}))(ModalBoxInputPwd);

const styles = StyleSheet.create({
  textInput: {
    width: deviceWidth / 1.1,
    height: moderateScale(40),
    backgroundColor: '#f8f6f9',
    color: '#000000',
    paddingHorizontal: moderateScale(10),
    borderColor: '#5CAEFF',
    borderWidth: 2,
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: deviceWidth / 1.1,
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(20),
    padding: 10,
  },
  buttonContainerCfm: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: deviceWidth / 1.1,
    marginLeft: moderateScale(20),
    marginTop: moderateScale(30),
  },
  textDescCfm: {
    color: '#3C3C3C',
    fontSize: moderateScale(16),
    fontWeight: '700',
    marginLeft: moderateScale(20),
    textAlign: 'center',
  },

  tripType: {
    flexDirection: 'row',
    marginTop: moderateScale(10),
    alignItems: 'center',
    flex: 1,
  },

  //////NEW
  modal: {
    height: deviceHeight - deviceHeight / 5,
    width: deviceWidth,
    // marginTop:moderateScale(120),
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  modalCfm: {
    height: deviceHeight / 3,
    width: deviceWidth,
    // marginTop:moderateScale(220),
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  modalPicker: {
    height: deviceHeight / 2.3,
    width: deviceWidth,
  },
  modalPicker5: {
    height: deviceHeight / 1.2,
    width: deviceWidth,
  },
  headerCfm: {
    backgroundColor: '#FFFFFF',
    marginBottom: moderateScale(20),
    height: moderateScale(50),
    justifyContent: 'center',
    // borderBottomLeftRadius:20,
    // borderBottomRightRadius:20
  },
  header: {
    backgroundColor: '#ffffff',
    marginBottom: moderateScale(30),
    height: moderateScale(50),
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flexDirection: 'row',
    // borderBottomLeftRadius:20,
    // borderBottomRightRadius:20
  },
  headerSearch: {
    backgroundColor: '#ffffff',
    height: moderateScale(50),
    justifyContent: 'center',
    flexDirection: 'row',
  },
  headerText: {
    fontSize: moderateScale(21),
    backgroundColor: 'transparent',
    marginTop: moderateScale(10),
    marginLeft: moderateScale(50),
    color: '#0090d2',
    fontWeight: '700',
  },
  textPickerIOS: {
    color: '#000000',
    fontSize: moderateScale(16),
    marginTop: moderateScale(15),
    marginLeft: moderateScale(10),
  },
  textDesc: {
    color: '#3C3C3C',
    fontSize: moderateScale(14),
    fontWeight: '700',
    marginLeft: moderateScale(20),
  },
  textDescPicker: {
    color: '#3C3C3C',
    fontSize: moderateScale(20),
    fontWeight: '700',
    textAlign: 'center',
    marginLeft: moderateScale(20),
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10),
  },
  exit: {
    fontSize: moderateScale(21),
    fontWeight: '700',
    color: '#ec650a',
    marginTop: moderateScale(10),
  },
  btnContainerTrip: {
    flexDirection: 'row',
    width: deviceWidth / 1.1,
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(10),
  },
  airportContainer: {
    height: moderateScale(50),
    width: deviceWidth / 1.1,
    marginBottom: moderateScale(20),
    backgroundColor: '#F8F6F9',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(10),
  },
  airportName: {
    color: '#000000',
    fontSize: moderateScale(16),
    marginTop: moderateScale(15),
    paddingHorizontal: moderateScale(10),
  },
  dateContainer: {
    height: moderateScale(50),
    width: deviceWidth / 2.32,
    marginBottom: moderateScale(20),
    backgroundColor: '#F8F6F9',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(10),
  },
  dateContainerLong: {
    height: moderateScale(50),
    width: deviceWidth / 1.1,
    marginBottom: moderateScale(20),
    backgroundColor: '#F8F6F9',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(10),
  },
  penumpangContainer: {
    height: moderateScale(50),
    width: deviceWidth / 3.7,
    marginBottom: moderateScale(20),
    backgroundColor: '#F8F6F9',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(20),
    marginBottom: moderateScale(10),
  },
  btnPesan: {
    backgroundColor: '#ec650a',
    width: deviceWidth / 1.2,
    height: moderateScale(60),
    borderColor: 1,
    borderWidth: moderateScale(0),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
  },
  btnTripType: {
    backgroundColor: 'transparent',
    width: moderateScale(130),
    height: moderateScale(50),
    borderColor: '#9b9b9b',
    borderWidth: moderateScale(2),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
  },
  btnTripTypeSelected: {
    backgroundColor: 'transparent',
    width: moderateScale(130),
    height: moderateScale(50),
    borderColor: '#ec650a',
    borderWidth: moderateScale(2),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
  },
  titleTrip: {
    fontWeight: '700',
    fontSize: 15,
    color: '#9b9b9b',
  },
  titleTripSelected: {
    fontWeight: '700',
    fontSize: 15,
    color: '#ec650a',
  },
  btnBatal: {
    backgroundColor: 'transparent',
    width: moderateScale(120),
    height: moderateScale(40),
    borderColor: '#9b9b9b',
    borderWidth: moderateScale(2),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
    marginLeft: moderateScale(10),
  },
  btnYa: {
    backgroundColor: 'transparent',
    width: moderateScale(120),
    height: moderateScale(40),
    borderColor: '#ec650a',
    borderWidth: moderateScale(2),
    borderRadius: moderateScale(20),
    marginTop: moderateScale(10),
  },
  airportName: {
    marginBottom: moderateScale(10),
  },
  //   airportListContainer:{
  //       height:moderateScale(15),
  //       marginBottom:moderateScale(5)
  //   }
  airportLabelContainer: {
    height: moderateScale(40),
    backgroundColor: '#F8F6F9',
    marginTop: moderateScale(5),
  },
  airportNameLabel: {
    marginLeft: moderateScale(10),
    marginTop: moderateScale(10),
    fontSize: moderateScale(18),
    color: '#3C3C3C',
  },
});
