//================= SIGNOUT SOCMED USING GOOGLE & FACEBOOK =================

import React from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  StyleSheet,
  Image,
  Button,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Button as ButtonNativeBase,
  Title,
  Thumbnail,
} from 'native-base';
import { createStackNavigator } from 'react-navigation';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import { actionsLogout } from '../../actions/LoginAction';
import {
  ppstChangePassword,
  postUpdateProfile,
  getDataProfile,
  uploadPhotoProfile,
} from '../../actions/ProfileAction';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker, { openCamera } from 'react-native-image-crop-picker';
import { removeHttps } from '../../config/constant';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      biography: this.props.profile.profileData.biography,
      firstname: this.props.profile.profileData.firstname,
      lastname: this.props.profile.profileData.lastname,
      location: 'Jakarta',
      phoneNumber: this.props.profile.profileData.phone_number,
      twitter: this.props.profile.profileData.twitter,
      instagram: this.props.profile.profileData.instagram,
      facebook: this.props.profile.profileData.facebook,
      oldPassword: '',
      newPassword: '',
      reNewPassword: '',
    };

    this.setupGoogleSignin();
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    if (params) {
      this.props
        .dispatch(
          uploadPhotoProfile(this.props.login.data.tokenid, params.data)
        )
        .then(res => {
          if (res.type == 'UPLOAD_PHOTO_STATE_SUCCESS') {
            Alert.alert(
              'Success',
              'Photo Uploaded.',
              [
                {
                  text: 'OK',
                  onPress: () => {
                    this.props.dispatch(
                      getDataProfile(this.props.login.data.tokenid)
                    );
                    this.props.navigation.goBack();
                  },
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(
              'Error',
              'Failed to change photo, please try again later.',
              [{ text: 'OK', onPress: () => this.props.navigation.pop(2) }],
              { cancelable: false }
            );
          }
        });
    } else {
      console.log('no image params');
    }
  }

  logout = () => {
    this.props.dispatch(actionsLogout());
    console.log(this.props.login);
    if (this.props.login.socmed == 'google') {
      this.signOut();
      console.log('Google');
    } else if (this.props.login.socmed == 'facebook') {
      this.FBLogout();
      console.log('Facebook');
    }

    const navigation = this.props.navigation;
    setTimeout(function() {
      navigation.goBack();
    }, 300);
  };

  FBLogout = async () => {
    console.log('logout facebook');
    let accessToken = this.props.login.fbToken;
    try {
      let logout = new GraphRequest(
        'me/permissions/',
        {
          accessToken: accessToken,
          httpMethod: 'DELETE',
        },
        (error, result) => {
          if (error) {
            console.log('Error fetching data: ' + error.toString());
          } else {
            console.log('loging manager');
            LoginManager.logOut();
          }
        }
      );

      new GraphRequestManager().addRequest(logout).start();
    } catch (err) {
      console.log('Error fb logout');
    }
  };

  async setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices();
      await GoogleSignin.configure({
        webClientId:
          '989206896006-q3qjf4mcr70d3a592h5lhomc6mnj5nvo.apps.googleusercontent.com',
        offlineAccess: false,
      });
    } catch (err) {
      console.log('Google signin error', err.code, err.message);
    }
  }

  signOut = async () => {
    console.log('Call signOut Google');
    try {
      await GoogleSignin.revokeAccess();
    } catch (error) {
      console.error(error);
    }
  };

  updateProfile = () => {
    this.props
      .dispatch(
        postUpdateProfile(
          this.props.login.data.tokenid,
          this.state.firstname,
          this.state.lastname,
          this.state.phoneNumber,
          this.state.twitter,
          this.state.instagram,
          this.state.biography,
          this.state.facebook
        )
      )
      .then(res => {
        this.props
          .dispatch(getDataProfile(this.props.login.data.tokenid))
          .then(results => {
            if (results.type == 'PROFILE_STATE_SUCCESS') {
              Alert.alert(
                'Success',
                'Profile updated.',
                [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
                { cancelable: false }
              );
            } else {
              Alert.alert(
                'Error',
                'Update failed, please try again.',
                [{ text: 'OK', onPress: () => console.log('error detect!') }],
                { cancelable: false }
              );
            }
          });
      });
  };

  changePassword = () => {
    const oldPass = this.state.oldPassword;
    const newPass = this.state.newPassword;
    const reNewPass = this.state.reNewPassword;

    if (oldPass == '' || newPass == '' || reNewPass == '') {
      Alert.alert(
        'Profile',
        'Please enter your all password correctly!',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false }
      );
    } else if (newPass != reNewPass) {
      Alert.alert(
        'Profile',
        'Re-Enter password you entered does not match!',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false }
      );
    } else {
      this.props
        .dispatch(
          ppstChangePassword(
            this.props.login.data.tokenid,
            newPass,
            reNewPass,
            oldPass
          )
        )
        .then(res => {
          console.log(res);
          if (res.type == 'CP_STATE_SUCCESS') {
            Alert.alert(
              'Profile',
              'Your password has been changes.',
              [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
              { cancelable: false }
            );
          } else {
            Alert.alert(
              'Error',
              'Failed to change password, maybe you wrong to enter the old password. Please check and try again.',
              [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
              { cancelable: false }
            );
          }
        });
    }
  };

  actionSheetOpen = () => {
    this.ActionSheet.show();
  };

  openCamera = () => {
    this.props.navigation.navigate('OpenCamera', {
      stateData: this.state,
    });
  };

  getImageFromLibrary = () => {
    ImagePicker.openPicker({
      multiple: false,
      includeBase64: true,
    }).then(images => {
      if (images) {
        let img = 'data:image/jpeg;base64, ' + images.data;

        this.props
          .dispatch(uploadPhotoProfile(this.props.login.data.tokenid, img))
          .then(res => {
            if (res.type == 'UPLOAD_PHOTO_STATE_SUCCESS') {
              Alert.alert(
                'Success',
                'Photo Uploaded.',
                [
                  {
                    text: 'OK',
                    onPress: () => {
                      this.props.dispatch(
                        getDataProfile(this.props.login.data.tokenid)
                      );
                      this.props.navigation.goBack();
                    },
                  },
                ],
                { cancelable: false }
              );
            } else {
              Alert.alert(
                'Error',
                'Failed to change photo, please try again later.',
                [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
                { cancelable: false }
              );
            }
          });
      }
    });
  };

  render() {
    const profile = this.props.profile.profileData;
    const validatePictIsHttps = removeHttps(this.props.profilePicture);
    let photoProfile = this.props.profilePicture;
    if (validatePictIsHttps.isHttps == true) {
      photoProfile = 'http' + validatePictIsHttps.sliceUrl;
    }

    return (
      <Container>
        <Header
          androidStatusBarColor="#1f1f1f"
          style={{ backgroundColor: '#1f1f1f' }}
        >
          <Left>
            <ButtonNativeBase
              onPress={() => this.props.navigation.goBack()}
              transparent
            >
              <Icon name="arrow-back" />
            </ButtonNativeBase>
          </Left>
          <Body>
            <Title>Edit Profile</Title>
          </Body>
        </Header>
        <Content
          style={{
            width: deviceWidth,
            paddingLeft: deviceWidth * 0.05,
            paddingRight: deviceWidth * 0.05,
            backgroundColor: '#fff',
          }}
        >
          <Row style={{ paddingTop: 20 }}>
            <Col>
              {this.props.profilePicture ? (
                // <Image style={{height: 150, width: 150 }}
                //   source={{ uri: photoProfile }}
                // />
                <Thumbnail large source={{ uri: photoProfile }} />
              ) : (
                <Image
                  style={{ height: 150, width: 150 }}
                  source={require('../../assets/no_image_camera.png')}
                />
              )}
            </Col>
          </Row>
          <Row
            style={{
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
              paddingTop: 10,
            }}
          >
            <TouchableOpacity onPress={this.actionSheetOpen}>
              <Text
                style={{
                  color: '#606060',
                  fontWeight: 'bold',
                  fontSize: 14,
                  borderBottomColor: '#606060',
                  borderBottomWidth: 1.5,
                }}
              >
                Upload Photo
                {/* {
                    (this.props.profile.uploadingPhoto) ? "Uploading Photo.." : "Upload Photo"
                  } */}
              </Text>
            </TouchableOpacity>
          </Row>
          {/* ACTION SHEETS */}
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            options={['Take a Photo', 'Photo Library', 'Cancel']}
            cancelButtonIndex={2}
            // destructiveButtonIndex={1}
            onPress={index => {
              if (index == 0) {
                this.openCamera();
              } else if (index == 1) {
                this.getImageFromLibrary();
              }
            }}
          />
          {/* ACTION SHEETS */}
          <Row>
            <Col style={{ marginTop: 30, marginBottom: 20 }}>
              <Text style={styles.subtitleprofile}>PROFILE SETTINGS</Text>
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={biography => this.setState({ biography })}
                placeholder="Biography"
                value={this.state.biography}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={firstname => this.setState({ firstname })}
                placeholder="Firstname"
                value={this.state.firstname}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={lastname => this.setState({ lastname })}
                placeholder="Lastname"
                value={this.state.lastname}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={email => this.setState({ email })}
                placeholder="Email"
                editable={false}
                value={profile.email}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={location => this.setState({ location })}
                placeholder="Location"
                value={this.state.location}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={phoneNumber => this.setState({ phoneNumber })}
                placeholder="Phone Number"
                value={this.state.phoneNumber}
              />
            </Col>
          </Row>
          <Row>
            <Col style={{ marginTop: 30, marginBottom: 20 }}>
              <Text style={styles.subtitleprofile}>SOCIAL MEDIA SETTINGS</Text>
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={facebook => this.setState({ facebook })}
                placeholder="Facebook Account"
                value={this.state.facebook}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={twitter => this.setState({ twitter })}
                placeholder="Twitter Account"
                value={this.state.twitter}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={instagram => this.setState({ instagram })}
                placeholder="Instagram Account"
                value={this.state.instagram}
              />
            </Col>
          </Row>
          <Row style={{ paddingTop: 10, paddingBottom: 40 }}>
            <Col>
              <Grid>
                <Row>
                  <Col>
                    <ButtonNativeBase
                      onPress={this.updateProfile}
                      style={{
                        backgroundColor: '#f3483c',
                        borderRadius: 5,
                        paddingLeft: 20,
                        paddingRight: 20,
                      }}
                    >
                      <Text
                        style={{
                          color: '#FFF',
                          fontWeight: '500',
                          fontSize: 16,
                        }}
                      >
                        {this.props.profile.fetchingGetProfile
                          ? 'Loading'
                          : 'Update'}
                      </Text>
                    </ButtonNativeBase>
                  </Col>
                </Row>
              </Grid>
            </Col>
          </Row>

          <Row>
            <Col style={{ marginTop: 30, marginBottom: 20 }}>
              <Text style={styles.subtitleprofile}>ADVANCE SETTINGS</Text>
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={oldPassword => this.setState({ oldPassword })}
                placeholder="Old Password"
                secureTextEntry={true}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={newPassword => this.setState({ newPassword })}
                placeholder="New Password"
                secureTextEntry={true}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <TextInput
                style={[styles.makingborderbottom, styles.profileedit]}
                onChangeText={reNewPassword => this.setState({ reNewPassword })}
                placeholder="Re-Enter New Password"
                secureTextEntry={true}
              />
            </Col>
          </Row>
          <Row style={{ paddingTop: 10, paddingBottom: 20 }}>
            <Col>
              <Grid>
                <Row>
                  <Col>
                    <ButtonNativeBase
                      onPress={this.changePassword}
                      style={{
                        backgroundColor: '#f3483c',
                        borderRadius: 5,
                        paddingLeft: 20,
                        paddingRight: 20,
                      }}
                    >
                      <Text
                        style={{
                          color: '#FFF',
                          fontWeight: '500',
                          fontSize: 16,
                        }}
                      >
                        Update
                      </Text>
                    </ButtonNativeBase>
                  </Col>
                </Row>
              </Grid>
            </Col>
          </Row>
          <Row
            style={{
              paddingBottom: 30,
              marginTop: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <TouchableOpacity
              onPress={this.logout}
              style={{
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomColor: '#606060',
                borderBottomWidth: 1.5,
              }}
            >
              <Text
                style={{ color: '#606060', fontWeight: '400', fontSize: 16 }}
              >
                Logout
              </Text>
            </TouchableOpacity>
          </Row>
        </Content>
      </Container>
    );
  }
}

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  subtitleprofile: {
    fontFamily: 'PT Sans',
    fontWeight: 'normal',
    color: '#6e6e6e',
    fontSize: 20,
    letterSpacing: 1,
  },
  makingborderbottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
  },
  profileedit: {
    color: '#444',
    fontSize: 16,
  },
  actionbutton: {
    width: 74,
    height: 38,
    backgroundColor: '#f3483c',
  },
  logoutbutton: {
    width: 54,
    height: 14,
    borderRadius: 12,
    backgroundColor: '#606060',
  },
});

function mapStateToProps(state, ownProps) {
  return {
    isFetching: state.profile.fetchingProfile,
    login: state.login,
    profile: state.profile,
    profilePicture: state.profile.profilePicture,
  };
}

export default connect(mapStateToProps)(EditProfile);
