import { URL_PACIS } from '../base/constant/index';
import axios from 'axios';
import 'cross-fetch/polyfill';

const URL_PURCHASE = 'http://43.231.129.200:3005/v1/product/purchase';
const RELEASE_URL = 'https://api.aeroaja.com';
const DEV_URL = 'https://apidev.aeroaja.com';
const LIST_AIRPORT = 'https://api-gateway-flight-dev.aeroaja.com/list_airport';
const OLD_URL = 'http://43.231.129.14:3004';
const HOTEL_URL = 'https://api-gateway-hotel-dev.aeroaja.com';
const HOTEL_BEDS = 'https://apinodegw-dev.aeroaja.com';
const BATIK_URL = 'https://api-gateway-flight-dev.aeroaja.com/batikfest';
const FLIGHT_URL = 'https://api-gateway-flight.aeroaja.com';
const GATEWAY = 'https://api-gateway-flight-dev.aeroaja.com';

const SEARCH_FLIGHT_TRAVELOKA =
  'https://api-gateway-flight-dev.aeroaja.com/traveloka/search';
const BOOK_FLIGHT_TRAVELOKA =
  'https://api-gateway-flight-dev.aeroaja.com/traveloka/book';
const PAY_FLIGHT_TRAVELOKA =
  'https://api-gateway-flight-dev.aeroaja.com/pay_flight';
// https://api-gateway-flight-dev.aeroaja.com
const TRAIN_URL = 'https://api-gateway-flight-dev.aeroaja.com/rail';

export async function searchFlight(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };

  const response = await fetch(SEARCH_FLIGHT_TRAVELOKA, headers);
  return handleResponse(response);
}

export async function bookingFlight(data, token) {
  let valueToken = '';
  if (token !== '') {
    valueToken = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    };
  } else {
    valueToken = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    };
  }
  const headers = {
    method: 'POST',
    headers: valueToken,
    body: JSON.stringify(data),
  };

  const response = await fetch(
    `${DEV_URL}/v1/gateway/flights/booking`,
    headers
  );
  return handleResponse(response);
}

export async function paymentFlight(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(data),
  };

  const response = await fetch(PAY_FLIGHT_TRAVELOKA, headers);
  return handleResponse(response);
}

// Payment Midtrans
export async function paymentMidtrans(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(data),
  };

  const response = await fetch(`${DEV_URL}/v1/payment/midtrans`, headers);
  return handleResponse(response);
}

export async function checkPaymentMidtrans() {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
  };

  const response = await fetch(`${DEV_URL}/v1/payment/midtrans`, headers);
  return handleResponse(response);
}

// Payment Midtrans

export async function checkPaymentStatus(trx_id, type) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    // body: JSON.stringify(data)
  };

  const response = await fetch(
    `${DEV_URL}/v1/payment/${trx_id}?type=${type}`,
    headers
  );
  return handleResponse(response);
}

export async function ordersHistory(token) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await fetch(
    `${DEV_URL}/v1/gateway/flights/orders-history`,
    headers
  );
  return handleResponse(response);
}

// ===================== Hotel =====================
export async function listHotelCity(country, query, token) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await fetch(
    `${DEV_URL}/v1/gateway/city?country_code=${country}&query=${query}`,
    headers
  );
  return handleResponse(response);
}

// Using HotelBeds
export async function listDestinationBeds(query) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(query),
  };
  const res = await fetch(`${HOTEL_BEDS}/search-destination`, headers);
  return handleResponse(res);
}

export async function searchHotelBeds(payload) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(payload),
  };
  const res = await fetch(`${HOTEL_BEDS}/search-hotel`, headers);
  return handleResponse(res);
}

export async function bookHotelBeds(payload) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(payload),
  };
  const res = await fetch(`${HOTEL_BEDS}/booking-hotel`, headers);
  return handleResponse(res);
}

// Using Hotel Beds
export async function searchHotel(payload, token) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(`${DEV_URL}/v1/gateway/hotels/list`, headers);
  return handleResponse(response);
}

export async function hotelDetail(data, token) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(
    `${DEV_URL}/v1/gateway/property/detail`,
    headers
  );
  return handleResponse(response);
}

export async function selectRoomHotel(data, token, property) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(
    `${DEV_URL}/v1/gateway/hotels/detail/${property}/rooms`,
    headers
  );
  return handleResponse(response);
}

export async function hotelBooking(data, hotelId, roomId, token) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(
    `${DEV_URL}/v1/gateway/hotels/detail/${hotelId}/rooms/${roomId}/book`,
    headers
  );
  return handleResponse(response);
}

export async function paymentHotel(data, token) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(`${DEV_URL}/v1/gateway/hotels/payment`, headers);
  return handleResponse(response);
}

// ===================== Hotel =====================

export async function sendCargo(data) {
  const CARGO_URL = 'http://198.71.88.27:30080/pacisSearchPrice';
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(CARGO_URL, headers);
  return handleResponse(response);
}

export async function orderCargo(data) {
  const ORDER_CARGO_URL = 'http://198.71.88.27:30080/pacisOrderDelivery';
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(ORDER_CARGO_URL, headers);
  return handleResponse(response);
}

//Train
export async function stationSearch() {
  const STATION_SEARCH_URL = `${TRAIN_URL}/get_stations`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  };
  const response = await fetch(STATION_SEARCH_URL, headers);
  return handleResponse(response);
}

export async function searchTrain(data) {
  const SEARCH_TRAIN_URL = `${TRAIN_URL}/get_schedule`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(SEARCH_TRAIN_URL, headers);
  return handleResponse(response);
}

export async function seatMap(data) {
  const SEAT_MAP_URL = 'http://43.231.129.200:30080/omegaAPI';
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(SEAT_MAP_URL, headers);
  return handleResponse(response);
}

export async function bookTrain(data) {
  const BOOK_TRAIN_URL = `${TRAIN_URL}/book`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(BOOK_TRAIN_URL, headers);
  return handleResponse(response);
}
//Train

//register email
export async function registerApply(data) {
  const REGISTER_APPLY_URL = `${DEV_URL}/v1/customers/apply?by=email`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(REGISTER_APPLY_URL, headers);
  return handleResponse(response);
}

export async function registerConfirm(data) {
  const REGISTER_CONFIRM_URL = `${DEV_URL}/v1/customers/apply-verified?by=email`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(REGISTER_CONFIRM_URL, headers);
  return handleResponse(response);
}
//register email

//register mobile
export async function mobileApply(data) {
  const MOBILE_APPLY = `${DEV_URL}/v1/customers/apply?by=mobileno`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(MOBILE_APPLY, headers);
  return handleResponse(response);
}

export async function mobileConfirm(data) {
  const MOBILE_CONFIRM = `${DEV_URL}/v1/customers/apply-verified?by=mobileno`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(MOBILE_CONFIRM, headers);
  return handleResponse(response);
}
//register mobile

//register All
export async function registerNow(data) {
  const REGISTER_NOW_URL = `${DEV_URL}/v1/customers/register`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(REGISTER_NOW_URL, headers);
  return handleResponse(response);
}
//register All

//login
export async function loginMail(data) {
  const LOGIN_MAIL_URL = `${DEV_URL}/v1/customers/login`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(LOGIN_MAIL_URL, headers);
  return handleResponse(response);
}

export async function loginSocmed(data) {
  const LOGIN_SOCMED_URL = `${DEV_URL}/v1/customers/login`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(LOGIN_SOCMED_URL, headers);
  return handleResponse(response);
}
//login

//Forgot Password
export async function forgotPassword(data) {
  const url = `${DEV_URL}/v1/customers/forgot-password`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(url, headers);
  return handleResponse(response);
}
export async function verifyForgotPassword(data) {
  const url = `${DEV_URL}/v1/customers/forgot-password-verified`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(url, headers);
  return handleResponse(response);
}
export async function changeForgotPassword(data) {
  const url = `${DEV_URL}/v1/customers/forgot-password-apply`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(url, headers);
  return handleResponse(response);
}
//Forgot Password

// =================== PULSA =====================
export async function purchasePulsa(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(URL_PURCHASE, headers);
  return handleResponse(response);
}

export async function pulsaPrice(data) {
  const urlPulsaPrice =
    'https://api-gateway-flight-dev.aeroaja.com/postfin/get_pulsa_price';
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(urlPulsaPrice, headers);
  return handleResponse(response);
}
// =================== PULSA =====================

// =================== PACIS =====================
export async function getListOfDropbox(query) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  };
  let URL = URL_PACIS + 'v1/pacis/box?q=';
  if (query) {
    URL = URL_PACIS + 'v1/pacis/box?q=' + query;
  }
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

export async function getPacisPrice(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  let URL = URL_PACIS + 'v1/pacis/price';
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

export async function submitPacisBooking(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  let URL = URL_PACIS + 'v1/booking';
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

export async function submitPacisPayment(data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  let URL = URL_PACIS + 'v1/payment';
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

export async function getPacisPaymentStatus(bookingCode) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  };
  let URL = URL_PACIS + 'v1/payment/' + bookingCode;
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

// =================== PACIS =====================

// =================== Profile =====================
//get Profile Detail
export async function getProfile(token) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await fetch(`${DEV_URL}/v1/customers/me`, headers);
  return handleResponse(response);
}
// update Profile
export async function updateProfile(token, data) {
  const headers = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(`${DEV_URL}/v1/customers/me`, headers);
  return handleResponse(response);
}
// refresh Token of Profile
export async function refreshProfile(token, data) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(
    `${DEV_URL}/v1/customers/refresh_token`,
    headers
  );
  return handleResponse(response);
}
// Change Password when User is Logged In
export async function changePasswordLogin(token, payload) {
  const url = `${DEV_URL}/v1/customers/change-password`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(url, headers);
  return handleResponse(response);
}
// =================== Profile =====================

// =================== List Country ===================
export async function listCountry(limit) {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(
    `${DEV_URL}/v1/master/country?limit=${limit}`,
    headers
  );
  return handleResponse(response);
}
// =================== List Country ===================

// =================== List Airport ===================
export async function listAirport(payload) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({}),
  };
  const response = await fetch(LIST_AIRPORT, headers);
  return handleResponse(response);
}
// =================== List Airport ===================

// =================== BATIK FEST ===================
export async function batikAll() {
  const headers = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(`${BATIK_URL}/allprices`, headers);
  return handleResponse(response);
}
export async function batikDetail(payload) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(`${BATIK_URL}/package_detail`, headers);
  return handleResponse(response);
}
export async function batikBook(payload) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(`${BATIK_URL}/book`, headers);
  return handleResponse(response);
}
export async function payFlight(payload) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(`${FLIGHT_URL}/pay_flight`, headers);
  return handleResponse(response);
}
// =================== BATIK FEST ===================

// =================== BILLS ===================
export async function PlnPostpaid(payload) {
  const URL = `${GATEWAY}/postfin`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

export async function bookPlnPostpaid(payload) {
  const URL = `${GATEWAY}/postfin`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(URL, headers);
  return handleResponse(response);
}

export async function searchPdam(payload) {
  const URL = `${GATEWAY}/postfin/get_pdam_products`;
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'x-platform-source': 'APP_AND',
      'x-sai-source': 'ASITAAJA',
    },
    body: JSON.stringify(payload),
  };
  const response = await fetch(URL, headers);
  return handleResponse(response);
}
// =================== BILLS ===================

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    return data;
  });
}
