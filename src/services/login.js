//========= SIGNIN EXAMPLE USING GOOGLE & FACEBOOK ==========

import React from 'react';
import {
  View,
  Dimensions,
  ScrollView,
  StyleSheet,
  Image,
  TextInput,
  CheckBox,
  TouchableOpacity,
  Alert,
  AsyncStorage,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Container, Header, Content, Icon, Button, Text } from 'native-base';
import { connect } from 'react-redux';
import { actionsLogin, actionsLoginSocmed } from '../../actions/LoginAction';
import { actionsRegisterSocmed } from '../../actions/RegisterAction';
import { getDataProfile } from '../../actions/ProfileAction';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
import Toast, { DURATION } from 'react-native-easy-toast';
import firebase from 'react-native-firebase';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // email: "sundanaeka@gmail.com",
      // password: "123456",
      email: '',
      password: '',
      fetchingLoginFB: false,
      fetchingLoginGoogle: false,
      fcmToken: '',
    };

    this.setupGoogleSignin();
    this.getToken();
    this.facebookLogin = this.facebookLogin.bind(this);
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        console.log('user request new token : ', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
        this.setState({ fcmToken: fcmToken });
      }
    } else {
      console.log('fcmToken already have :', fcmToken);
      this.setState({ fcmToken: fcmToken });
    }
  }

  async setupGoogleSignin() {
    try {
      await GoogleSignin.hasPlayServices();
      await GoogleSignin.configure({
        // scopes: ["email", "profile"]
        webClientId:
          '524064147827-4kidl67efs333e9m70dugt0lk9l8nkhm.apps.googleusercontent.com',
      });
    } catch (err) {
      console.log('Google signin error', err.code, err.message);
    }
  }

  googleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const data = await GoogleSignin.signIn();
      const result = data.user;
      console.log(result);
      this.setState({ fetchingLoginGoogle: true });
      // this.props.dispatch(actionsLoginSocmed('google', result.email, result.name, this.state.fcmToken))
      // .then( (res) => {
      //   this.setState({fetchingLoginGoogle: false})
      //   if(res.type == "LOGIN_FAILED") {
      //     this.props.dispatch(actionsRegisterSocmed('google', result.email, result.name))
      //     .then((res) => {
      //       this.props.dispatch(actionsLoginSocmed('google', result.email, this.state.fcmToken))
      //       .then( (res) => {
      //         if(res.type == "LOGIN_SUCCESS") {
      //           this.props.dispatch(getDataProfile(res.data.tokenid))
      //         }
      //       })
      //     })
      //   } else {
      //     this.props.dispatch(getDataProfile(res.data.tokenid))
      //   }
      // })
    } catch (error) {
      console.log(error);
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
        this.refs.toast.show('User Cancelled the Login', 1500, () => {
          this.setState({ modalVisible: false });
        });
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
        this.refs.toast.show(
          'Google Play Services not available or outdated',
          1500,
          () => {
            this.setState({ modalVisible: false });
          }
        );
      } else {
        console.log('Some Other Error Happened');
        console.log(error);
        console.log(statusCodes);
        this.refs.toast.show(
          'Some other error happened, Please try again later',
          1500,
          () => {
            this.setState({ modalVisible: false });
          }
        );
      }
    }
  };

  async facebookLogin() {
    let result;
    try {
      LoginManager.setLoginBehavior('NATIVE_ONLY');
      result = await LoginManager.logInWithReadPermissions([
        'public_profile',
        'email',
      ]);
    } catch (nativeError) {
      try {
        LoginManager.setLoginBehavior('WEB_ONLY');
        result = await LoginManager.logInWithReadPermissions([
          'email',
          'public_profile',
        ]);
      } catch (webError) {}
    }

    if (result.isCancelled) {
      this.setState({
        showLoadingModal: false,
      });
    } else {
      AccessToken.getCurrentAccessToken().then(data => {
        let accessToken = data.accessToken;
        console.log(accessToken);
        const responseInfoCallback = (error, result) => {
          console.log(result);
          if (error) {
            console.log('Error fetching data: ' + error.toString());
          } else {
            this.setState({ fetchingLoginFB: true });
            console.log('SUCCESS LOGIN FB HERE 0');
            // this.props.dispatch(actionsLoginSocmed('facebook', result.email, result.name, this.state.fcmToken, accessToken))
            // .then( (res) => {
            //   this.setState({fetchingLoginFB: false})
            //   if(res.type == "LOGIN_FAILED") {
            //     this.props.dispatch(actionsRegisterSocmed('facebook', result.email, result.name))
            //     .then((res) => {

            //       this.props.dispatch(actionsLoginSocmed('facebook', result.email, result.name, this.state.fcmToken))
            //       .then( (res) => {
            //         if(res.type == "LOGIN_SUCCESS") {
            //           this.props.dispatch(getDataProfile(res.data.tokenid))
            //         }
            //       })
            //     })
            //   } else {
            //     console.log("SUCCESS LOGIN FB HERE 1")
            //     this.props.dispatch(getDataProfile(res.data.tokenid))
            //   }
            // })
          }
        };

        const infoRequest = new GraphRequest(
          '/me',
          {
            accessToken: accessToken,
            parameters: {
              fields: {
                string: 'email,name,first_name,middle_name,last_name',
              },
            },
          },
          responseInfoCallback
        );

        new GraphRequestManager().addRequest(infoRequest).start();
      });
    }
  }

  dologin = () => {
    if (this.state.email == '' || this.state.password == '') {
      Alert.alert(
        'Sign In',
        'Email and password is required, please enter correctly.',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false }
      );
    } else {
      this.props
        .dispatch(
          actionsLogin(
            this.state.email,
            this.state.password,
            this.state.fcmToken
          )
        )
        .then(res => {
          if (res.type == 'LOGIN_FAILED') {
            Alert.alert(
              'Sign In',
              'Failed to login, please make sure to enter the correct password.',
              [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
              { cancelable: false }
            );
          } else {
            this.props.dispatch(getDataProfile(res.data.tokenid));
          }
        });
    }
  };

  render() {
    const props = this.props.prop;
    return (
      <Container>
        <Content
          style={{
            width: deviceWidth,
            paddingLeft: deviceWidth * 0.075,
            paddingRight: deviceWidth * 0.075,
            backgroundColor: '#fff',
          }}
        >
          <View>
            <Row>
              <Col>
                <Image
                  style={{ height: 28.4, width: 210, marginTop: 50 }}
                  source={require('../../assets/ls-logo.png')}
                />
              </Col>
            </Row>
            <Row style={{ marginTop: 30 }}>
              <Col>
                <Text
                  style={{
                    fontFamily: 'PT Sans',
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#5f5f5f',
                    letterSpacing: 1.3,
                  }}
                >
                  SIGN IN
                </Text>
              </Col>
              <Col style={{ paddingTop: 4 }}>
                <TouchableOpacity
                  onPress={() => props.navigation.push('Signup')}
                >
                  <Text
                    style={{
                      color: '#bababa',
                      textDecorationLine: 'underline',
                      fontSize: 14,
                      textAlign: 'right',
                    }}
                  >
                    doesn't have account?
                  </Text>
                </TouchableOpacity>
              </Col>
            </Row>
          </View>
          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <Row style={{ marginBottom: 13 }}>
              <Col>
                <Image
                  style={{
                    resizeMode: 'contain',
                    width: 16,
                    height: 16,
                    marginTop: 18,
                    marginLeft: 17,
                    position: 'absolute',
                  }}
                  source={require('../../assets/sssssEnvelope.png')}
                />
                <TextInput
                  style={styles.loginform}
                  onChangeText={email => this.setState({ email })}
                  placeholder="Email"
                  value={this.state.email}
                />
              </Col>
            </Row>
            <Row style={{ marginBottom: 13 }}>
              <Col>
                <Image
                  style={{
                    resizeMode: 'contain',
                    width: 16,
                    height: 16,
                    marginTop: 18,
                    marginLeft: 17,
                    position: 'absolute',
                  }}
                  source={require('../../assets/key.png')}
                />
                <TextInput
                  style={styles.loginform}
                  onChangeText={password => this.setState({ password })}
                  placeholder="Password"
                  secureTextEntry={true}
                  value={this.state.password}
                />
              </Col>
            </Row>
            <Row>
              <Col style={{ flex: 1 }}></Col>
              <Col style={{ flex: 8 }}>
                <TouchableOpacity
                  onPress={() => props.navigation.push('ForgotPassword')}
                >
                  <Text
                    style={{
                      color: '#bababa',
                      textDecorationLine: 'underline',
                      fontSize: 14,
                      paddingTop: 5,
                      textAlign: 'right',
                    }}
                  >
                    Forgot Password?
                  </Text>
                </TouchableOpacity>
              </Col>
            </Row>

            {/* BUTTON */}
            <TouchableOpacity onPress={this.dologin}>
              <Row
                style={{
                  marginTop: 30,
                  justifyContent: 'center',
                  backgroundColor: '#f3483c',
                  width: deviceWidth - 70,
                  borderRadius: 50,
                  paddingTop: 13,
                  paddingBottom: 13,
                }}
              >
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontFamily: 'PTSans WebRegular',
                    letterSpacing: 0.8,
                  }}
                >
                  {this.props.isFetching ? 'Loading..' : 'SIGN IN'}
                </Text>
              </Row>
            </TouchableOpacity>
            <Row style={{ marginTop: 20, marginBottom: 20 }}>
              <Col>
                <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>
                  OR
                </Text>
              </Col>
            </Row>
            <Row style={{ justifyContent: 'center' }}>
              <Button
                onPress={this.googleLogin}
                rounded
                style={{
                  justifyContent: 'center',
                  backgroundColor: '#ff001f',
                  width: deviceWidth - 70,
                }}
              >
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontFamily: 'PTSans WebRegular',
                    letterSpacing: 0.8,
                  }}
                >
                  {this.state.fetchingLoginGoogle
                    ? 'PLEASE WAIT..'
                    : 'SIGN IN USING GMAIL'}
                </Text>
              </Button>
            </Row>
            <Row style={{ marginTop: 15, justifyContent: 'center' }}>
              <Button
                onPress={this.facebookLogin}
                rounded
                style={{
                  justifyContent: 'center',
                  backgroundColor: '#4a90e2',
                  width: deviceWidth - 70,
                }}
              >
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontFamily: 'PTSans WebRegular',
                    letterSpacing: 0.8,
                  }}
                >
                  {this.state.fetchingLoginFB
                    ? 'PLEASE WAIT..'
                    : 'SIGN IN USING FACEBOOK'}
                </Text>
              </Button>
            </Row>
            {/* BUTTON */}
          </View>
        </Content>
        <Toast ref="toast" />
      </Container>
    );
  }
}

const window = Dimensions.get('window');
let deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  loginform: {
    height: 55,
    flex: 1,
    borderColor: '#DADADA',
    borderWidth: 1,
    borderRadius: 4,
    color: '#4D4D4D',
    paddingLeft: 50,
  },
  actionbutton: {
    backgroundColor: '#f3483c',
    borderRadius: 24,
    color: '#fff',
    fontWeight: 'bold',
    letterSpacing: 1,
  },
  username: {
    fontSize: 26,
    marginTop: deviceWidth * 0.05,
    color: '#444',
    fontWeight: 'bold',
    fontFamily: 'PT Sans',
  },
  mainbio: {
    fontSize: 19,
    color: '#141b26',
    fontFamily: 'RobotoSlab',
  },
  subbio: {
    fontSize: 14,
    color: '#404f68',
    fontFamily: 'RobotoSlab',
  },
  numberfollow: {
    color: 'color: rgb(20, 27, 38)',
    fontSize: 24,
    fontFamily: 'Roboto Slab',
  },
  expfollow: {
    fontSize: 12,
    color: 'color: rgb(64, 79, 104)',
    fontFamily: 'Roboto Slab',
    opacity: 0.5,
  },
  makingborder: {
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
    borderTopWidth: 1,
    borderTopColor: '#e7e7e7',
  },
  subtitlecat: {
    fontFamily: 'PT Sans',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#444',
  },
  littletittle: {
    color: '#fff',
    fontSize: 14,
    textTransform: 'uppercase',
    letterSpacing: 1.5,
    fontFamily: 'Raleway-Light',
    fontWeight: 'bold',
  },
  biggertitle: {
    color: '#fff',
    fontSize: 20,
    textTransform: 'capitalize',
    fontFamily: 'PT Sans',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowOffset: { width: 0, height: 2 },
    textShadowRadius: 4,
  },
});

function mapStateToProps(state, ownProps) {
  return {
    isFetching: state.login.fetchingLogin,
    login: state.login.data,
  };
}

export default connect(mapStateToProps)(Login);
