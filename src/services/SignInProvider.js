const googleAndroid = {
  appId:
    '456188491662-1qb0vcjpqekomthua2rcq36rv51dua10.apps.googleusercontent.com',
  callback: 'com.asitaaja:/oauth2redirect',
};
const googleIos = {
  appId:
    '1005543871813-1ri63qjqh8dbj3k2avf6p71iork49qc0.apps.googleusercontent.com',
  callback: 'com.solusiawan.asitaaja:/oauth2redirect',
};
const facebookConf = {
  appId: '223906638534092',
  callback: 'fb223906638534092://authorize',
  scope: 'user_friends', // you can override the default scope here
  fields: ['email', 'first_name', 'last_name'], // you can override the default fields here
};

export { googleAndroid, googleIos, facebookConf };
