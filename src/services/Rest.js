import axios from 'axios';

export const post = (url, data, token) => {
  let querystring = require('query-string');
  let params = querystring.stringify(data);

  return new Promise((resolve, reject) => {
    axios
      .post(url, params, { headers: { Authorization: 'Bearer ' + token } })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const postNoToken = (url, data) => {
  var headers = {
    'Content-Type': 'application/json',
  };

  return new Promise((resolve, reject) => {
    axios
      .post(url, data, { headers: headers })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const postWithHeader = (url, data, token) => {
  var headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: 'Bearer ' + token,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(url, data, { headers: headers })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const get = url => {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const getWithHeader = (url, token) => {
  var headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: 'Bearer ' + token,
  };

  return new Promise((resolve, reject) => {
    axios
      .get(url, { headers: headers })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err);
      });
  });
};
