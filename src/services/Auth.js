import { AsyncStorage } from 'react-native';
import { NEWLOADFLAG, USERDATACONFIG } from '../Const/Config';

export const newLoad = data =>
  AsyncStorage.setItem(NEWLOADFLAG, JSON.stringify(data));
export const userData = data =>
  AsyncStorage.setItem(USERDATACONFIG, JSON.stringify(data));
