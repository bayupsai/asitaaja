import React from 'react'
import { View, Linking } from 'react-native'
import { Provider } from 'react-redux'
import Route from './src/base/configs/routes'
import { DotIndicator } from 'react-native-indicators'
import { getPersistor } from '@rematch/persist'

import store from './src/redux/reducers/index'
import { PersistGate } from 'redux-persist/lib/integration/react'

class App extends React.Component {

  componentDidMount() {
    Linking.getInitialURL()
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={getPersistor()}
          loading={<View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: '#FFFFFF' }}><DotIndicator size={10} style={{ flex: 1, backgroundColor: '#FFFFFF' }} color="#008195" /></View>}>
          <Route />
        </PersistGate>
      </Provider>
    )
  }
}

export default App